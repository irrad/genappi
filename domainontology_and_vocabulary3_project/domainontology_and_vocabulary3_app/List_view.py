from django.db import models
from django.template.loader import render_to_string
from .models import *
from .forms import *
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseNotFound
from django.template import loader
from django.core.urlresolvers import reverse
import datetime
from .forms import *
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.core.files.storage import FileSystemStorage
from django.utils.safestring import mark_safe
from django.core import serializers


def BC10EventList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC10Event.objects.all())
    fields = BC10Event._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC10EventList.html",{'data':data, 'fields':new_fields})

def BC11PersonList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC11Person.objects.all())
    fields = BC11Person._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC11PersonList.html",{'data':data, 'fields':new_fields})

def BC12EcosystemList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC12Ecosystem.objects.all())
    fields = BC12Ecosystem._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC12EcosystemList.html",{'data':data, 'fields':new_fields})

def BC13OrganizationList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC13Organization.objects.all())
    fields = BC13Organization._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC13OrganizationList.html",{'data':data, 'fields':new_fields})

def BC14EcosystemEnvironmentList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC14EcosystemEnvironment.objects.all())
    fields = BC14EcosystemEnvironment._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC14EcosystemEnvironmentList.html",{'data':data, 'fields':new_fields})

def BC15WaterAreaList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC15WaterArea.objects.all())
    fields = BC15WaterArea._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC15WaterAreaList.html",{'data':data, 'fields':new_fields})

def BC16ManMadethingList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC16ManMadething.objects.all())
    fields = BC16ManMadething._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC16ManMadethingList.html",{'data':data, 'fields':new_fields})

def BC17ConceptualObjectList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC17ConceptualObject.objects.all())
    fields = BC17ConceptualObject._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC17ConceptualObjectList.html",{'data':data, 'fields':new_fields})

def BC18PropositionList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC18Proposition.objects.all())
    fields = BC18Proposition._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC18PropositionList.html",{'data':data, 'fields':new_fields})

def BC19TitleList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC19Title.objects.all())
    fields = BC19Title._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC19TitleList.html",{'data':data, 'fields':new_fields})

def BC1TLOEntityList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC1TLOEntity.objects.all())
    fields = BC1TLOEntity._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC1TLOEntityList.html",{'data':data, 'fields':new_fields})

def BC20DeclarativePlaceList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC20DeclarativePlace.objects.all())
    fields = BC20DeclarativePlace._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC20DeclarativePlaceList.html",{'data':data, 'fields':new_fields})

def BC21DataSetList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC21DataSet.objects.all())
    fields = BC21DataSet._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC21DataSetList.html",{'data':data, 'fields':new_fields})

def BC22EncounterEventList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC22EncounterEvent.objects.all())
    fields = BC22EncounterEvent._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC22EncounterEventList.html",{'data':data, 'fields':new_fields})

def BC23DigitalObjectList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC23DigitalObject.objects.all())
    fields = BC23DigitalObject._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC23DigitalObjectList.html",{'data':data, 'fields':new_fields})

def BC24RepositoryObjectList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC24RepositoryObject.objects.all())
    fields = BC24RepositoryObject._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC24RepositoryObjectList.html",{'data':data, 'fields':new_fields})

def BC25LinguisticObjectList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC25LinguisticObject.objects.all())
    fields = BC25LinguisticObject._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC25LinguisticObjectList.html",{'data':data, 'fields':new_fields})

def BC26PlaceNameList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC26PlaceName.objects.all())
    fields = BC26PlaceName._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC26PlaceNameList.html",{'data':data, 'fields':new_fields})

def BC27PublicationList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC27Publication.objects.all())
    fields = BC27Publication._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC27PublicationList.html",{'data':data, 'fields':new_fields})

def BC29SpatialCoordinateReferenceSystemList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC29SpatialCoordinateReferenceSystem.objects.all())
    fields = BC29SpatialCoordinateReferenceSystem._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC29SpatialCoordinateReferenceSystemList.html",{'data':data, 'fields':new_fields})

def BC2TimeSpanList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC2TimeSpan.objects.all())
    fields = BC2TimeSpan._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC2TimeSpanList.html",{'data':data, 'fields':new_fields})

def BC30AppellationList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC30Appellation.objects.all())
    fields = BC30Appellation._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC30AppellationList.html",{'data':data, 'fields':new_fields})

def BC31PlaceAppellationList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC31PlaceAppellation.objects.all())
    fields = BC31PlaceAppellation._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC31PlaceAppellationList.html",{'data':data, 'fields':new_fields})

def BC32IdentifierList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC32Identifier.objects.all())
    fields = BC32Identifier._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC32IdentifierList.html",{'data':data, 'fields':new_fields})

def BC33SpatialCoordinateList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC33SpatialCoordinate.objects.all())
    fields = BC33SpatialCoordinate._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC33SpatialCoordinateList.html",{'data':data, 'fields':new_fields})

def BC34GeometricPlaceExpressionList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC34GeometricPlaceExpression.objects.all())
    fields = BC34GeometricPlaceExpression._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC34GeometricPlaceExpressionList.html",{'data':data, 'fields':new_fields})

def BC35PhysicalThingList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC35PhysicalThing.objects.all())
    fields = BC35PhysicalThing._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC35PhysicalThingList.html",{'data':data, 'fields':new_fields})

def BC36AbioticElementList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC36AbioticElement.objects.all())
    fields = BC36AbioticElement._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC36AbioticElementList.html",{'data':data, 'fields':new_fields})

def BC37BiologicalObjectList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC37BiologicalObject.objects.all())
    fields = BC37BiologicalObject._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC37BiologicalObjectList.html",{'data':data, 'fields':new_fields})

def BC38BioticElementList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC38BioticElement.objects.all())
    fields = BC38BioticElement._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC38BioticElementList.html",{'data':data, 'fields':new_fields})

def BC39MarineAnimalList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC39MarineAnimal.objects.all())
    fields = BC39MarineAnimal._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC39MarineAnimalList.html",{'data':data, 'fields':new_fields})

def BC3PlaceList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC3Place.objects.all())
    fields = BC3Place._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC3PlaceList.html",{'data':data, 'fields':new_fields})

def BC40PhysicalManMadeThingList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC40PhysicalManMadeThing.objects.all())
    fields = BC40PhysicalManMadeThing._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC40PhysicalManMadeThingList.html",{'data':data, 'fields':new_fields})

def BC41ManMadeObjectList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC41ManMadeObject.objects.all())
    fields = BC41ManMadeObject._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC41ManMadeObjectList.html",{'data':data, 'fields':new_fields})

def BC42CollectionList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC42Collection.objects.all())
    fields = BC42Collection._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC42CollectionList.html",{'data':data, 'fields':new_fields})

def BC43ActivityList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC43Activity.objects.all())
    fields = BC43Activity._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC43ActivityList.html",{'data':data, 'fields':new_fields})

def BC44AttributeAssignmentList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC44AttributeAssignment.objects.all())
    fields = BC44AttributeAssignment._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC44AttributeAssignmentList.html",{'data':data, 'fields':new_fields})

def BC45ObservationList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC45Observation.objects.all())
    fields = BC45Observation._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC45ObservationList.html",{'data':data, 'fields':new_fields})

def BC46IdentifierAssignmentList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC46IdentifierAssignment.objects.all())
    fields = BC46IdentifierAssignment._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC46IdentifierAssignmentList.html",{'data':data, 'fields':new_fields})

def BC47ImageList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC47Image.objects.all())
    fields = BC47Image._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC47ImageList.html",{'data':data, 'fields':new_fields})

def BC48DatabaseList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC48Database.objects.all())
    fields = BC48Database._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC48DatabaseList.html",{'data':data, 'fields':new_fields})

def BC4TemporalPhenomenonList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC4TemporalPhenomenon.objects.all())
    fields = BC4TemporalPhenomenon._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC4TemporalPhenomenonList.html",{'data':data, 'fields':new_fields})

def BC51PhysicalObjectList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC51PhysicalObject.objects.all())
    fields = BC51PhysicalObject._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC51PhysicalObjectList.html",{'data':data, 'fields':new_fields})

def BC53SpecimenList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC53Specimen.objects.all())
    fields = BC53Specimen._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC53SpecimenList.html",{'data':data, 'fields':new_fields})

def BC54MeasurementList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC54Measurement.objects.all())
    fields = BC54Measurement._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC54MeasurementList.html",{'data':data, 'fields':new_fields})

def BC55MeasurementUnitList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC55MeasurementUnit.objects.all())
    fields = BC55MeasurementUnit._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC55MeasurementUnitList.html",{'data':data, 'fields':new_fields})

def BC56DigitalMeasurementEventList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC56DigitalMeasurementEvent.objects.all())
    fields = BC56DigitalMeasurementEvent._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC56DigitalMeasurementEventList.html",{'data':data, 'fields':new_fields})

def BC57CaptureList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC57Capture.objects.all())
    fields = BC57Capture._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC57CaptureList.html",{'data':data, 'fields':new_fields})

def BC58DigitalDeviceList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC58DigitalDevice.objects.all())
    fields = BC58DigitalDevice._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC58DigitalDeviceList.html",{'data':data, 'fields':new_fields})

def BC59SoftwareList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC59Software.objects.all())
    fields = BC59Software._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC59SoftwareList.html",{'data':data, 'fields':new_fields})

def BC5DimensionList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC5Dimension.objects.all())
    fields = BC5Dimension._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC5DimensionList.html",{'data':data, 'fields':new_fields})

def BC60SoftwareExecutionList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC60SoftwareExecution.objects.all())
    fields = BC60SoftwareExecution._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC60SoftwareExecutionList.html",{'data':data, 'fields':new_fields})

def BC61CaptureActivityList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC61CaptureActivity.objects.all())
    fields = BC61CaptureActivity._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC61CaptureActivityList.html",{'data':data, 'fields':new_fields})

def BC62StatisticIndicatorList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC62StatisticIndicator.objects.all())
    fields = BC62StatisticIndicator._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC62StatisticIndicatorList.html",{'data':data, 'fields':new_fields})

def BC63GlobalStatisticLandingList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC63GlobalStatisticLanding.objects.all())
    fields = BC63GlobalStatisticLanding._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC63GlobalStatisticLandingList.html",{'data':data, 'fields':new_fields})

def BC64DesignorProcedureList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC64DesignorProcedure.objects.all())
    fields = BC64DesignorProcedure._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC64DesignorProcedureList.html",{'data':data, 'fields':new_fields})

def BC6PersistentItemList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC6PersistentItem.objects.all())
    fields = BC6PersistentItem._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC6PersistentItemList.html",{'data':data, 'fields':new_fields})

def BC70GroupList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC70Group.objects.all())
    fields = BC70Group._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC70GroupList.html",{'data':data, 'fields':new_fields})

def BC71NameUseActivityList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC71NameUseActivity.objects.all())
    fields = BC71NameUseActivity._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC71NameUseActivityList.html",{'data':data, 'fields':new_fields})

def BC72SpaceTimeVolumeList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC72SpaceTimeVolume.objects.all())
    fields = BC72SpaceTimeVolume._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC72SpaceTimeVolumeList.html",{'data':data, 'fields':new_fields})

def BC75StockList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC75Stock.objects.all())
    fields = BC75Stock._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC75StockList.html",{'data':data, 'fields':new_fields})

def BC76StockFormationList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC76StockFormation.objects.all())
    fields = BC76StockFormation._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC76StockFormationList.html",{'data':data, 'fields':new_fields})

def BC77MatterRemovalList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC77MatterRemoval.objects.all())
    fields = BC77MatterRemoval._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC77MatterRemovalList.html",{'data':data, 'fields':new_fields})

def BC78AmountofMatterList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC78AmountofMatter.objects.all())
    fields = BC78AmountofMatter._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC78AmountofMatterList.html",{'data':data, 'fields':new_fields})

def BC79RightList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC79Right.objects.all())
    fields = BC79Right._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC79RightList.html",{'data':data, 'fields':new_fields})

def BC7ThingList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC7Thing.objects.all())
    fields = BC7Thing._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC7ThingList.html",{'data':data, 'fields':new_fields})

def BC8ActorList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC8Actor.objects.all())
    fields = BC8Actor._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC8ActorList.html",{'data':data, 'fields':new_fields})

def BC9ObservableEntityList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC9ObservableEntity.objects.all())
    fields = BC9ObservableEntity._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC9ObservableEntityList.html",{'data':data, 'fields':new_fields})

def BT10BioticElementTypeList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT10BioticElementType.objects.all())
    fields = BT10BioticElementType._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT10BioticElementTypeList.html",{'data':data, 'fields':new_fields})

def BT11EquipmentTypeList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT11EquipmentType.objects.all())
    fields = BT11EquipmentType._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT11EquipmentTypeList.html",{'data':data, 'fields':new_fields})

def BT12ScientificDataTypeList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT12ScientificDataType.objects.all())
    fields = BT12ScientificDataType._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT12ScientificDataTypeList.html",{'data':data, 'fields':new_fields})

def BT13DigitalObjectTypeList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT13DigitalObjectType.objects.all())
    fields = BT13DigitalObjectType._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT13DigitalObjectTypeList.html",{'data':data, 'fields':new_fields})

def BT14AppellationTypeList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT14AppellationType.objects.all())
    fields = BT14AppellationType._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT14AppellationTypeList.html",{'data':data, 'fields':new_fields})

def BT16VisualTypeList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT16VisualType.objects.all())
    fields = BT16VisualType._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT16VisualTypeList.html",{'data':data, 'fields':new_fields})

def BT17HumanActivityTypeList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT17HumanActivityType.objects.all())
    fields = BT17HumanActivityType._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT17HumanActivityTypeList.html",{'data':data, 'fields':new_fields})

def BT18KingdomList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT18Kingdom.objects.all())
    fields = BT18Kingdom._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT18KingdomList.html",{'data':data, 'fields':new_fields})

def BT19PhylumList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT19Phylum.objects.all())
    fields = BT19Phylum._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT19PhylumList.html",{'data':data, 'fields':new_fields})

def BT1TLOEntityTypeList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT1TLOEntityType.objects.all())
    fields = BT1TLOEntityType._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT1TLOEntityTypeList.html",{'data':data, 'fields':new_fields})

def BT20SubPhylumList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT20SubPhylum.objects.all())
    fields = BT20SubPhylum._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT20SubPhylumList.html",{'data':data, 'fields':new_fields})

def BT21SuperClassList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT21SuperClass.objects.all())
    fields = BT21SuperClass._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT21SuperClassList.html",{'data':data, 'fields':new_fields})

def BT22ClassList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT22Class.objects.all())
    fields = BT22Class._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT22ClassList.html",{'data':data, 'fields':new_fields})

def BT23SubClassList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT23SubClass.objects.all())
    fields = BT23SubClass._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT23SubClassList.html",{'data':data, 'fields':new_fields})

def BT24FamilyList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT24Family.objects.all())
    fields = BT24Family._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT24FamilyList.html",{'data':data, 'fields':new_fields})

def BT25SubFamilyList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT25SubFamily.objects.all())
    fields = BT25SubFamily._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT25SubFamilyList.html",{'data':data, 'fields':new_fields})

def BT26GenusList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT26Genus.objects.all())
    fields = BT26Genus._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT26GenusList.html",{'data':data, 'fields':new_fields})

def BT27SpeciesList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT27Species.objects.all())
    fields = BT27Species._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT27SpeciesList.html",{'data':data, 'fields':new_fields})

def BT28ScientificActivityTypeList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT28ScientificActivityType.objects.all())
    fields = BT28ScientificActivityType._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT28ScientificActivityTypeList.html",{'data':data, 'fields':new_fields})

def BT29IndustrialActivityTypeList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT29IndustrialActivityType.objects.all())
    fields = BT29IndustrialActivityType._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT29IndustrialActivityTypeList.html",{'data':data, 'fields':new_fields})

def BT2TemporalPhenomenonTypeList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT2TemporalPhenomenonType.objects.all())
    fields = BT2TemporalPhenomenonType._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT2TemporalPhenomenonTypeList.html",{'data':data, 'fields':new_fields})

def BT30IdentifierAssignmentTypeList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT30IdentifierAssignmentType.objects.all())
    fields = BT30IdentifierAssignmentType._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT30IdentifierAssignmentTypeList.html",{'data':data, 'fields':new_fields})

def BT31BiologicalPartTimeList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT31BiologicalPartTime.objects.all())
    fields = BT31BiologicalPartTime._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT31BiologicalPartTimeList.html",{'data':data, 'fields':new_fields})

def BT32PersistenTypeList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT32PersistenType.objects.all())
    fields = BT32PersistenType._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT32PersistenTypeList.html",{'data':data, 'fields':new_fields})

def BT32PersistentTypeList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT32PersistentType.objects.all())
    fields = BT32PersistentType._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT32PersistentTypeList.html",{'data':data, 'fields':new_fields})

def BT33MarineAnimalTypeList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT33MarineAnimalType.objects.all())
    fields = BT33MarineAnimalType._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT33MarineAnimalTypeList.html",{'data':data, 'fields':new_fields})

def BT34OrderList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT34Order.objects.all())
    fields = BT34Order._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT34OrderList.html",{'data':data, 'fields':new_fields})

def BT35PropertyTypeList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT35PropertyType.objects.all())
    fields = BT35PropertyType._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT35PropertyTypeList.html",{'data':data, 'fields':new_fields})

def BT36LanguageList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT36Language.objects.all())
    fields = BT36Language._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT36LanguageList.html",{'data':data, 'fields':new_fields})

def BT3PhysicalThingTypeList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT3PhysicalThingType.objects.all())
    fields = BT3PhysicalThingType._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT3PhysicalThingTypeList.html",{'data':data, 'fields':new_fields})

def BT4ConceptualObjectTypeList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT4ConceptualObjectType.objects.all())
    fields = BT4ConceptualObjectType._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT4ConceptualObjectTypeList.html",{'data':data, 'fields':new_fields})

def BT5LegislativeZoneTypeList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT5LegislativeZoneType.objects.all())
    fields = BT5LegislativeZoneType._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT5LegislativeZoneTypeList.html",{'data':data, 'fields':new_fields})

def BT6HumanEventTypeList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT6HumanEventType.objects.all())
    fields = BT6HumanEventType._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT6HumanEventTypeList.html",{'data':data, 'fields':new_fields})

def BT7EcosystemTypeList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT7EcosystemType.objects.all())
    fields = BT7EcosystemType._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT7EcosystemTypeList.html",{'data':data, 'fields':new_fields})

def BT8AbioticElementTypeList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT8AbioticElementType.objects.all())
    fields = BT8AbioticElementType._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT8AbioticElementTypeList.html",{'data':data, 'fields':new_fields})

def BT9ActorTypeList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT9ActorType.objects.all())
    fields = BT9ActorType._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT9ActorTypeList.html",{'data':data, 'fields':new_fields})

def PersonList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Person.objects.all())
    fields = Person._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"PersonList.html",{'data':data, 'fields':new_fields})
