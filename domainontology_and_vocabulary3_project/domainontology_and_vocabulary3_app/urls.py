from django.conf.urls import url, include
from . import Read_view, Create_view, Update_view, Delete_view, List_view, Home_view, preferences, registration

urlpatterns = [
    url(r'^home/$', Home_view.HomeView, name='HomeView'),

    url(r'^preferences/$', preferences.define_preferences, name='define_preferences'),

    url(r'^signup/$', registration.signup, name='signup'),    

    url(r'^BC10Event/create/$', Create_view.BC10EventCreate, name='BC10EventCreate'),

    url(r'^BC11Person/create/$', Create_view.BC11PersonCreate, name='BC11PersonCreate'),

    url(r'^BC12Ecosystem/create/$', Create_view.BC12EcosystemCreate, name='BC12EcosystemCreate'),

    url(r'^BC13Organization/create/$', Create_view.BC13OrganizationCreate, name='BC13OrganizationCreate'),

    url(r'^BC14EcosystemEnvironment/create/$', Create_view.BC14EcosystemEnvironmentCreate, name='BC14EcosystemEnvironmentCreate'),

    url(r'^BC15WaterArea/create/$', Create_view.BC15WaterAreaCreate, name='BC15WaterAreaCreate'),

    url(r'^BC16ManMadething/create/$', Create_view.BC16ManMadethingCreate, name='BC16ManMadethingCreate'),

    url(r'^BC17ConceptualObject/create/$', Create_view.BC17ConceptualObjectCreate, name='BC17ConceptualObjectCreate'),

    url(r'^BC18Proposition/create/$', Create_view.BC18PropositionCreate, name='BC18PropositionCreate'),

    url(r'^BC19Title/create/$', Create_view.BC19TitleCreate, name='BC19TitleCreate'),

    url(r'^BC1TLOEntity/create/$', Create_view.BC1TLOEntityCreate, name='BC1TLOEntityCreate'),

    url(r'^BC20DeclarativePlace/create/$', Create_view.BC20DeclarativePlaceCreate, name='BC20DeclarativePlaceCreate'),

    url(r'^BC21DataSet/create/$', Create_view.BC21DataSetCreate, name='BC21DataSetCreate'),

    url(r'^BC22EncounterEvent/create/$', Create_view.BC22EncounterEventCreate, name='BC22EncounterEventCreate'),

    url(r'^BC23DigitalObject/create/$', Create_view.BC23DigitalObjectCreate, name='BC23DigitalObjectCreate'),

    url(r'^BC24RepositoryObject/create/$', Create_view.BC24RepositoryObjectCreate, name='BC24RepositoryObjectCreate'),

    url(r'^BC25LinguisticObject/create/$', Create_view.BC25LinguisticObjectCreate, name='BC25LinguisticObjectCreate'),

    url(r'^BC26PlaceName/create/$', Create_view.BC26PlaceNameCreate, name='BC26PlaceNameCreate'),

    url(r'^BC27Publication/create/$', Create_view.BC27PublicationCreate, name='BC27PublicationCreate'),

    url(r'^BC29SpatialCoordinateReferenceSystem/create/$', Create_view.BC29SpatialCoordinateReferenceSystemCreate, name='BC29SpatialCoordinateReferenceSystemCreate'),

    url(r'^BC2TimeSpan/create/$', Create_view.BC2TimeSpanCreate, name='BC2TimeSpanCreate'),

    url(r'^BC30Appellation/create/$', Create_view.BC30AppellationCreate, name='BC30AppellationCreate'),

    url(r'^BC31PlaceAppellation/create/$', Create_view.BC31PlaceAppellationCreate, name='BC31PlaceAppellationCreate'),

    url(r'^BC32Identifier/create/$', Create_view.BC32IdentifierCreate, name='BC32IdentifierCreate'),

    url(r'^BC33SpatialCoordinate/create/$', Create_view.BC33SpatialCoordinateCreate, name='BC33SpatialCoordinateCreate'),

    url(r'^BC34GeometricPlaceExpression/create/$', Create_view.BC34GeometricPlaceExpressionCreate, name='BC34GeometricPlaceExpressionCreate'),

    url(r'^BC35PhysicalThing/create/$', Create_view.BC35PhysicalThingCreate, name='BC35PhysicalThingCreate'),

    url(r'^BC36AbioticElement/create/$', Create_view.BC36AbioticElementCreate, name='BC36AbioticElementCreate'),

    url(r'^BC37BiologicalObject/create/$', Create_view.BC37BiologicalObjectCreate, name='BC37BiologicalObjectCreate'),

    url(r'^BC38BioticElement/create/$', Create_view.BC38BioticElementCreate, name='BC38BioticElementCreate'),

    url(r'^BC39MarineAnimal/create/$', Create_view.BC39MarineAnimalCreate, name='BC39MarineAnimalCreate'),

    url(r'^BC3Place/create/$', Create_view.BC3PlaceCreate, name='BC3PlaceCreate'),

    url(r'^BC40PhysicalManMadeThing/create/$', Create_view.BC40PhysicalManMadeThingCreate, name='BC40PhysicalManMadeThingCreate'),

    url(r'^BC41ManMadeObject/create/$', Create_view.BC41ManMadeObjectCreate, name='BC41ManMadeObjectCreate'),

    url(r'^BC42Collection/create/$', Create_view.BC42CollectionCreate, name='BC42CollectionCreate'),

    url(r'^BC43Activity/create/$', Create_view.BC43ActivityCreate, name='BC43ActivityCreate'),

    url(r'^BC44AttributeAssignment/create/$', Create_view.BC44AttributeAssignmentCreate, name='BC44AttributeAssignmentCreate'),

    url(r'^BC45Observation/create/$', Create_view.BC45ObservationCreate, name='BC45ObservationCreate'),

    url(r'^BC46IdentifierAssignment/create/$', Create_view.BC46IdentifierAssignmentCreate, name='BC46IdentifierAssignmentCreate'),

    url(r'^BC47Image/create/$', Create_view.BC47ImageCreate, name='BC47ImageCreate'),

    url(r'^BC48Database/create/$', Create_view.BC48DatabaseCreate, name='BC48DatabaseCreate'),

    url(r'^BC4TemporalPhenomenon/create/$', Create_view.BC4TemporalPhenomenonCreate, name='BC4TemporalPhenomenonCreate'),

    url(r'^BC51PhysicalObject/create/$', Create_view.BC51PhysicalObjectCreate, name='BC51PhysicalObjectCreate'),

    url(r'^BC53Specimen/create/$', Create_view.BC53SpecimenCreate, name='BC53SpecimenCreate'),

    url(r'^BC54Measurement/create/$', Create_view.BC54MeasurementCreate, name='BC54MeasurementCreate'),

    url(r'^BC55MeasurementUnit/create/$', Create_view.BC55MeasurementUnitCreate, name='BC55MeasurementUnitCreate'),

    url(r'^BC56DigitalMeasurementEvent/create/$', Create_view.BC56DigitalMeasurementEventCreate, name='BC56DigitalMeasurementEventCreate'),

    url(r'^BC57Capture/create/$', Create_view.BC57CaptureCreate, name='BC57CaptureCreate'),

    url(r'^BC58DigitalDevice/create/$', Create_view.BC58DigitalDeviceCreate, name='BC58DigitalDeviceCreate'),

    url(r'^BC59Software/create/$', Create_view.BC59SoftwareCreate, name='BC59SoftwareCreate'),

    url(r'^BC5Dimension/create/$', Create_view.BC5DimensionCreate, name='BC5DimensionCreate'),

    url(r'^BC60SoftwareExecution/create/$', Create_view.BC60SoftwareExecutionCreate, name='BC60SoftwareExecutionCreate'),

    url(r'^BC61CaptureActivity/create/$', Create_view.BC61CaptureActivityCreate, name='BC61CaptureActivityCreate'),

    url(r'^BC62StatisticIndicator/create/$', Create_view.BC62StatisticIndicatorCreate, name='BC62StatisticIndicatorCreate'),

    url(r'^BC63GlobalStatisticLanding/create/$', Create_view.BC63GlobalStatisticLandingCreate, name='BC63GlobalStatisticLandingCreate'),

    url(r'^BC64DesignorProcedure/create/$', Create_view.BC64DesignorProcedureCreate, name='BC64DesignorProcedureCreate'),

    url(r'^BC6PersistentItem/create/$', Create_view.BC6PersistentItemCreate, name='BC6PersistentItemCreate'),

    url(r'^BC70Group/create/$', Create_view.BC70GroupCreate, name='BC70GroupCreate'),

    url(r'^BC71NameUseActivity/create/$', Create_view.BC71NameUseActivityCreate, name='BC71NameUseActivityCreate'),

    url(r'^BC72SpaceTimeVolume/create/$', Create_view.BC72SpaceTimeVolumeCreate, name='BC72SpaceTimeVolumeCreate'),

    url(r'^BC75Stock/create/$', Create_view.BC75StockCreate, name='BC75StockCreate'),

    url(r'^BC76StockFormation/create/$', Create_view.BC76StockFormationCreate, name='BC76StockFormationCreate'),

    url(r'^BC77MatterRemoval/create/$', Create_view.BC77MatterRemovalCreate, name='BC77MatterRemovalCreate'),

    url(r'^BC78AmountofMatter/create/$', Create_view.BC78AmountofMatterCreate, name='BC78AmountofMatterCreate'),

    url(r'^BC79Right/create/$', Create_view.BC79RightCreate, name='BC79RightCreate'),

    url(r'^BC7Thing/create/$', Create_view.BC7ThingCreate, name='BC7ThingCreate'),

    url(r'^BC8Actor/create/$', Create_view.BC8ActorCreate, name='BC8ActorCreate'),

    url(r'^BC9ObservableEntity/create/$', Create_view.BC9ObservableEntityCreate, name='BC9ObservableEntityCreate'),

    url(r'^BT10BioticElementType/create/$', Create_view.BT10BioticElementTypeCreate, name='BT10BioticElementTypeCreate'),

    url(r'^BT11EquipmentType/create/$', Create_view.BT11EquipmentTypeCreate, name='BT11EquipmentTypeCreate'),

    url(r'^BT12ScientificDataType/create/$', Create_view.BT12ScientificDataTypeCreate, name='BT12ScientificDataTypeCreate'),

    url(r'^BT13DigitalObjectType/create/$', Create_view.BT13DigitalObjectTypeCreate, name='BT13DigitalObjectTypeCreate'),

    url(r'^BT14AppellationType/create/$', Create_view.BT14AppellationTypeCreate, name='BT14AppellationTypeCreate'),

    url(r'^BT16VisualType/create/$', Create_view.BT16VisualTypeCreate, name='BT16VisualTypeCreate'),

    url(r'^BT17HumanActivityType/create/$', Create_view.BT17HumanActivityTypeCreate, name='BT17HumanActivityTypeCreate'),

    url(r'^BT18Kingdom/create/$', Create_view.BT18KingdomCreate, name='BT18KingdomCreate'),

    url(r'^BT19Phylum/create/$', Create_view.BT19PhylumCreate, name='BT19PhylumCreate'),

    url(r'^BT1TLOEntityType/create/$', Create_view.BT1TLOEntityTypeCreate, name='BT1TLOEntityTypeCreate'),

    url(r'^BT20SubPhylum/create/$', Create_view.BT20SubPhylumCreate, name='BT20SubPhylumCreate'),

    url(r'^BT21SuperClass/create/$', Create_view.BT21SuperClassCreate, name='BT21SuperClassCreate'),

    url(r'^BT22Class/create/$', Create_view.BT22ClassCreate, name='BT22ClassCreate'),

    url(r'^BT23SubClass/create/$', Create_view.BT23SubClassCreate, name='BT23SubClassCreate'),

    url(r'^BT24Family/create/$', Create_view.BT24FamilyCreate, name='BT24FamilyCreate'),

    url(r'^BT25SubFamily/create/$', Create_view.BT25SubFamilyCreate, name='BT25SubFamilyCreate'),

    url(r'^BT26Genus/create/$', Create_view.BT26GenusCreate, name='BT26GenusCreate'),

    url(r'^BT27Species/create/$', Create_view.BT27SpeciesCreate, name='BT27SpeciesCreate'),

    url(r'^BT28ScientificActivityType/create/$', Create_view.BT28ScientificActivityTypeCreate, name='BT28ScientificActivityTypeCreate'),

    url(r'^BT29IndustrialActivityType/create/$', Create_view.BT29IndustrialActivityTypeCreate, name='BT29IndustrialActivityTypeCreate'),

    url(r'^BT2TemporalPhenomenonType/create/$', Create_view.BT2TemporalPhenomenonTypeCreate, name='BT2TemporalPhenomenonTypeCreate'),

    url(r'^BT30IdentifierAssignmentType/create/$', Create_view.BT30IdentifierAssignmentTypeCreate, name='BT30IdentifierAssignmentTypeCreate'),

    url(r'^BT31BiologicalPartTime/create/$', Create_view.BT31BiologicalPartTimeCreate, name='BT31BiologicalPartTimeCreate'),

    url(r'^BT32PersistenType/create/$', Create_view.BT32PersistenTypeCreate, name='BT32PersistenTypeCreate'),

    url(r'^BT32PersistentType/create/$', Create_view.BT32PersistentTypeCreate, name='BT32PersistentTypeCreate'),

    url(r'^BT33MarineAnimalType/create/$', Create_view.BT33MarineAnimalTypeCreate, name='BT33MarineAnimalTypeCreate'),

    url(r'^BT34Order/create/$', Create_view.BT34OrderCreate, name='BT34OrderCreate'),

    url(r'^BT35PropertyType/create/$', Create_view.BT35PropertyTypeCreate, name='BT35PropertyTypeCreate'),

    url(r'^BT36Language/create/$', Create_view.BT36LanguageCreate, name='BT36LanguageCreate'),

    url(r'^BT3PhysicalThingType/create/$', Create_view.BT3PhysicalThingTypeCreate, name='BT3PhysicalThingTypeCreate'),

    url(r'^BT4ConceptualObjectType/create/$', Create_view.BT4ConceptualObjectTypeCreate, name='BT4ConceptualObjectTypeCreate'),

    url(r'^BT5LegislativeZoneType/create/$', Create_view.BT5LegislativeZoneTypeCreate, name='BT5LegislativeZoneTypeCreate'),

    url(r'^BT6HumanEventType/create/$', Create_view.BT6HumanEventTypeCreate, name='BT6HumanEventTypeCreate'),

    url(r'^BT7EcosystemType/create/$', Create_view.BT7EcosystemTypeCreate, name='BT7EcosystemTypeCreate'),

    url(r'^BT8AbioticElementType/create/$', Create_view.BT8AbioticElementTypeCreate, name='BT8AbioticElementTypeCreate'),

    url(r'^BT9ActorType/create/$', Create_view.BT9ActorTypeCreate, name='BT9ActorTypeCreate'),

    url(r'^Person/create/$', Create_view.PersonCreate, name='PersonCreate'),


    url(r'^BC10Event/(?P<pk>\d+)/read/$', Read_view.BC10EventRead, name='BC10EventRead'),

    url(r'^BC11Person/(?P<pk>\d+)/read/$', Read_view.BC11PersonRead, name='BC11PersonRead'),

    url(r'^BC12Ecosystem/(?P<pk>\d+)/read/$', Read_view.BC12EcosystemRead, name='BC12EcosystemRead'),

    url(r'^BC13Organization/(?P<pk>\d+)/read/$', Read_view.BC13OrganizationRead, name='BC13OrganizationRead'),

    url(r'^BC14EcosystemEnvironment/(?P<pk>\d+)/read/$', Read_view.BC14EcosystemEnvironmentRead, name='BC14EcosystemEnvironmentRead'),

    url(r'^BC15WaterArea/(?P<pk>\d+)/read/$', Read_view.BC15WaterAreaRead, name='BC15WaterAreaRead'),

    url(r'^BC16ManMadething/(?P<pk>\d+)/read/$', Read_view.BC16ManMadethingRead, name='BC16ManMadethingRead'),

    url(r'^BC17ConceptualObject/(?P<pk>\d+)/read/$', Read_view.BC17ConceptualObjectRead, name='BC17ConceptualObjectRead'),

    url(r'^BC18Proposition/(?P<pk>\d+)/read/$', Read_view.BC18PropositionRead, name='BC18PropositionRead'),

    url(r'^BC19Title/(?P<pk>\d+)/read/$', Read_view.BC19TitleRead, name='BC19TitleRead'),

    url(r'^BC1TLOEntity/(?P<pk>\d+)/read/$', Read_view.BC1TLOEntityRead, name='BC1TLOEntityRead'),

    url(r'^BC20DeclarativePlace/(?P<pk>\d+)/read/$', Read_view.BC20DeclarativePlaceRead, name='BC20DeclarativePlaceRead'),

    url(r'^BC21DataSet/(?P<pk>\d+)/read/$', Read_view.BC21DataSetRead, name='BC21DataSetRead'),

    url(r'^BC22EncounterEvent/(?P<pk>\d+)/read/$', Read_view.BC22EncounterEventRead, name='BC22EncounterEventRead'),

    url(r'^BC23DigitalObject/(?P<pk>\d+)/read/$', Read_view.BC23DigitalObjectRead, name='BC23DigitalObjectRead'),

    url(r'^BC24RepositoryObject/(?P<pk>\d+)/read/$', Read_view.BC24RepositoryObjectRead, name='BC24RepositoryObjectRead'),

    url(r'^BC25LinguisticObject/(?P<pk>\d+)/read/$', Read_view.BC25LinguisticObjectRead, name='BC25LinguisticObjectRead'),

    url(r'^BC26PlaceName/(?P<pk>\d+)/read/$', Read_view.BC26PlaceNameRead, name='BC26PlaceNameRead'),

    url(r'^BC27Publication/(?P<pk>\d+)/read/$', Read_view.BC27PublicationRead, name='BC27PublicationRead'),

    url(r'^BC29SpatialCoordinateReferenceSystem/(?P<pk>\d+)/read/$', Read_view.BC29SpatialCoordinateReferenceSystemRead, name='BC29SpatialCoordinateReferenceSystemRead'),

    url(r'^BC2TimeSpan/(?P<pk>\d+)/read/$', Read_view.BC2TimeSpanRead, name='BC2TimeSpanRead'),

    url(r'^BC30Appellation/(?P<pk>\d+)/read/$', Read_view.BC30AppellationRead, name='BC30AppellationRead'),

    url(r'^BC31PlaceAppellation/(?P<pk>\d+)/read/$', Read_view.BC31PlaceAppellationRead, name='BC31PlaceAppellationRead'),

    url(r'^BC32Identifier/(?P<pk>\d+)/read/$', Read_view.BC32IdentifierRead, name='BC32IdentifierRead'),

    url(r'^BC33SpatialCoordinate/(?P<pk>\d+)/read/$', Read_view.BC33SpatialCoordinateRead, name='BC33SpatialCoordinateRead'),

    url(r'^BC34GeometricPlaceExpression/(?P<pk>\d+)/read/$', Read_view.BC34GeometricPlaceExpressionRead, name='BC34GeometricPlaceExpressionRead'),

    url(r'^BC35PhysicalThing/(?P<pk>\d+)/read/$', Read_view.BC35PhysicalThingRead, name='BC35PhysicalThingRead'),

    url(r'^BC36AbioticElement/(?P<pk>\d+)/read/$', Read_view.BC36AbioticElementRead, name='BC36AbioticElementRead'),

    url(r'^BC37BiologicalObject/(?P<pk>\d+)/read/$', Read_view.BC37BiologicalObjectRead, name='BC37BiologicalObjectRead'),

    url(r'^BC38BioticElement/(?P<pk>\d+)/read/$', Read_view.BC38BioticElementRead, name='BC38BioticElementRead'),

    url(r'^BC39MarineAnimal/(?P<pk>\d+)/read/$', Read_view.BC39MarineAnimalRead, name='BC39MarineAnimalRead'),

    url(r'^BC3Place/(?P<pk>\d+)/read/$', Read_view.BC3PlaceRead, name='BC3PlaceRead'),

    url(r'^BC40PhysicalManMadeThing/(?P<pk>\d+)/read/$', Read_view.BC40PhysicalManMadeThingRead, name='BC40PhysicalManMadeThingRead'),

    url(r'^BC41ManMadeObject/(?P<pk>\d+)/read/$', Read_view.BC41ManMadeObjectRead, name='BC41ManMadeObjectRead'),

    url(r'^BC42Collection/(?P<pk>\d+)/read/$', Read_view.BC42CollectionRead, name='BC42CollectionRead'),

    url(r'^BC43Activity/(?P<pk>\d+)/read/$', Read_view.BC43ActivityRead, name='BC43ActivityRead'),

    url(r'^BC44AttributeAssignment/(?P<pk>\d+)/read/$', Read_view.BC44AttributeAssignmentRead, name='BC44AttributeAssignmentRead'),

    url(r'^BC45Observation/(?P<pk>\d+)/read/$', Read_view.BC45ObservationRead, name='BC45ObservationRead'),

    url(r'^BC46IdentifierAssignment/(?P<pk>\d+)/read/$', Read_view.BC46IdentifierAssignmentRead, name='BC46IdentifierAssignmentRead'),

    url(r'^BC47Image/(?P<pk>\d+)/read/$', Read_view.BC47ImageRead, name='BC47ImageRead'),

    url(r'^BC48Database/(?P<pk>\d+)/read/$', Read_view.BC48DatabaseRead, name='BC48DatabaseRead'),

    url(r'^BC4TemporalPhenomenon/(?P<pk>\d+)/read/$', Read_view.BC4TemporalPhenomenonRead, name='BC4TemporalPhenomenonRead'),

    url(r'^BC51PhysicalObject/(?P<pk>\d+)/read/$', Read_view.BC51PhysicalObjectRead, name='BC51PhysicalObjectRead'),

    url(r'^BC53Specimen/(?P<pk>\d+)/read/$', Read_view.BC53SpecimenRead, name='BC53SpecimenRead'),

    url(r'^BC54Measurement/(?P<pk>\d+)/read/$', Read_view.BC54MeasurementRead, name='BC54MeasurementRead'),

    url(r'^BC55MeasurementUnit/(?P<pk>\d+)/read/$', Read_view.BC55MeasurementUnitRead, name='BC55MeasurementUnitRead'),

    url(r'^BC56DigitalMeasurementEvent/(?P<pk>\d+)/read/$', Read_view.BC56DigitalMeasurementEventRead, name='BC56DigitalMeasurementEventRead'),

    url(r'^BC57Capture/(?P<pk>\d+)/read/$', Read_view.BC57CaptureRead, name='BC57CaptureRead'),

    url(r'^BC58DigitalDevice/(?P<pk>\d+)/read/$', Read_view.BC58DigitalDeviceRead, name='BC58DigitalDeviceRead'),

    url(r'^BC59Software/(?P<pk>\d+)/read/$', Read_view.BC59SoftwareRead, name='BC59SoftwareRead'),

    url(r'^BC5Dimension/(?P<pk>\d+)/read/$', Read_view.BC5DimensionRead, name='BC5DimensionRead'),

    url(r'^BC60SoftwareExecution/(?P<pk>\d+)/read/$', Read_view.BC60SoftwareExecutionRead, name='BC60SoftwareExecutionRead'),

    url(r'^BC61CaptureActivity/(?P<pk>\d+)/read/$', Read_view.BC61CaptureActivityRead, name='BC61CaptureActivityRead'),

    url(r'^BC62StatisticIndicator/(?P<pk>\d+)/read/$', Read_view.BC62StatisticIndicatorRead, name='BC62StatisticIndicatorRead'),

    url(r'^BC63GlobalStatisticLanding/(?P<pk>\d+)/read/$', Read_view.BC63GlobalStatisticLandingRead, name='BC63GlobalStatisticLandingRead'),

    url(r'^BC64DesignorProcedure/(?P<pk>\d+)/read/$', Read_view.BC64DesignorProcedureRead, name='BC64DesignorProcedureRead'),

    url(r'^BC6PersistentItem/(?P<pk>\d+)/read/$', Read_view.BC6PersistentItemRead, name='BC6PersistentItemRead'),

    url(r'^BC70Group/(?P<pk>\d+)/read/$', Read_view.BC70GroupRead, name='BC70GroupRead'),

    url(r'^BC71NameUseActivity/(?P<pk>\d+)/read/$', Read_view.BC71NameUseActivityRead, name='BC71NameUseActivityRead'),

    url(r'^BC72SpaceTimeVolume/(?P<pk>\d+)/read/$', Read_view.BC72SpaceTimeVolumeRead, name='BC72SpaceTimeVolumeRead'),

    url(r'^BC75Stock/(?P<pk>\d+)/read/$', Read_view.BC75StockRead, name='BC75StockRead'),

    url(r'^BC76StockFormation/(?P<pk>\d+)/read/$', Read_view.BC76StockFormationRead, name='BC76StockFormationRead'),

    url(r'^BC77MatterRemoval/(?P<pk>\d+)/read/$', Read_view.BC77MatterRemovalRead, name='BC77MatterRemovalRead'),

    url(r'^BC78AmountofMatter/(?P<pk>\d+)/read/$', Read_view.BC78AmountofMatterRead, name='BC78AmountofMatterRead'),

    url(r'^BC79Right/(?P<pk>\d+)/read/$', Read_view.BC79RightRead, name='BC79RightRead'),

    url(r'^BC7Thing/(?P<pk>\d+)/read/$', Read_view.BC7ThingRead, name='BC7ThingRead'),

    url(r'^BC8Actor/(?P<pk>\d+)/read/$', Read_view.BC8ActorRead, name='BC8ActorRead'),

    url(r'^BC9ObservableEntity/(?P<pk>\d+)/read/$', Read_view.BC9ObservableEntityRead, name='BC9ObservableEntityRead'),

    url(r'^BT10BioticElementType/(?P<pk>\d+)/read/$', Read_view.BT10BioticElementTypeRead, name='BT10BioticElementTypeRead'),

    url(r'^BT11EquipmentType/(?P<pk>\d+)/read/$', Read_view.BT11EquipmentTypeRead, name='BT11EquipmentTypeRead'),

    url(r'^BT12ScientificDataType/(?P<pk>\d+)/read/$', Read_view.BT12ScientificDataTypeRead, name='BT12ScientificDataTypeRead'),

    url(r'^BT13DigitalObjectType/(?P<pk>\d+)/read/$', Read_view.BT13DigitalObjectTypeRead, name='BT13DigitalObjectTypeRead'),

    url(r'^BT14AppellationType/(?P<pk>\d+)/read/$', Read_view.BT14AppellationTypeRead, name='BT14AppellationTypeRead'),

    url(r'^BT16VisualType/(?P<pk>\d+)/read/$', Read_view.BT16VisualTypeRead, name='BT16VisualTypeRead'),

    url(r'^BT17HumanActivityType/(?P<pk>\d+)/read/$', Read_view.BT17HumanActivityTypeRead, name='BT17HumanActivityTypeRead'),

    url(r'^BT18Kingdom/(?P<pk>\d+)/read/$', Read_view.BT18KingdomRead, name='BT18KingdomRead'),

    url(r'^BT19Phylum/(?P<pk>\d+)/read/$', Read_view.BT19PhylumRead, name='BT19PhylumRead'),

    url(r'^BT1TLOEntityType/(?P<pk>\d+)/read/$', Read_view.BT1TLOEntityTypeRead, name='BT1TLOEntityTypeRead'),

    url(r'^BT20SubPhylum/(?P<pk>\d+)/read/$', Read_view.BT20SubPhylumRead, name='BT20SubPhylumRead'),

    url(r'^BT21SuperClass/(?P<pk>\d+)/read/$', Read_view.BT21SuperClassRead, name='BT21SuperClassRead'),

    url(r'^BT22Class/(?P<pk>\d+)/read/$', Read_view.BT22ClassRead, name='BT22ClassRead'),

    url(r'^BT23SubClass/(?P<pk>\d+)/read/$', Read_view.BT23SubClassRead, name='BT23SubClassRead'),

    url(r'^BT24Family/(?P<pk>\d+)/read/$', Read_view.BT24FamilyRead, name='BT24FamilyRead'),

    url(r'^BT25SubFamily/(?P<pk>\d+)/read/$', Read_view.BT25SubFamilyRead, name='BT25SubFamilyRead'),

    url(r'^BT26Genus/(?P<pk>\d+)/read/$', Read_view.BT26GenusRead, name='BT26GenusRead'),

    url(r'^BT27Species/(?P<pk>\d+)/read/$', Read_view.BT27SpeciesRead, name='BT27SpeciesRead'),

    url(r'^BT28ScientificActivityType/(?P<pk>\d+)/read/$', Read_view.BT28ScientificActivityTypeRead, name='BT28ScientificActivityTypeRead'),

    url(r'^BT29IndustrialActivityType/(?P<pk>\d+)/read/$', Read_view.BT29IndustrialActivityTypeRead, name='BT29IndustrialActivityTypeRead'),

    url(r'^BT2TemporalPhenomenonType/(?P<pk>\d+)/read/$', Read_view.BT2TemporalPhenomenonTypeRead, name='BT2TemporalPhenomenonTypeRead'),

    url(r'^BT30IdentifierAssignmentType/(?P<pk>\d+)/read/$', Read_view.BT30IdentifierAssignmentTypeRead, name='BT30IdentifierAssignmentTypeRead'),

    url(r'^BT31BiologicalPartTime/(?P<pk>\d+)/read/$', Read_view.BT31BiologicalPartTimeRead, name='BT31BiologicalPartTimeRead'),

    url(r'^BT32PersistenType/(?P<pk>\d+)/read/$', Read_view.BT32PersistenTypeRead, name='BT32PersistenTypeRead'),

    url(r'^BT32PersistentType/(?P<pk>\d+)/read/$', Read_view.BT32PersistentTypeRead, name='BT32PersistentTypeRead'),

    url(r'^BT33MarineAnimalType/(?P<pk>\d+)/read/$', Read_view.BT33MarineAnimalTypeRead, name='BT33MarineAnimalTypeRead'),

    url(r'^BT34Order/(?P<pk>\d+)/read/$', Read_view.BT34OrderRead, name='BT34OrderRead'),

    url(r'^BT35PropertyType/(?P<pk>\d+)/read/$', Read_view.BT35PropertyTypeRead, name='BT35PropertyTypeRead'),

    url(r'^BT36Language/(?P<pk>\d+)/read/$', Read_view.BT36LanguageRead, name='BT36LanguageRead'),

    url(r'^BT3PhysicalThingType/(?P<pk>\d+)/read/$', Read_view.BT3PhysicalThingTypeRead, name='BT3PhysicalThingTypeRead'),

    url(r'^BT4ConceptualObjectType/(?P<pk>\d+)/read/$', Read_view.BT4ConceptualObjectTypeRead, name='BT4ConceptualObjectTypeRead'),

    url(r'^BT5LegislativeZoneType/(?P<pk>\d+)/read/$', Read_view.BT5LegislativeZoneTypeRead, name='BT5LegislativeZoneTypeRead'),

    url(r'^BT6HumanEventType/(?P<pk>\d+)/read/$', Read_view.BT6HumanEventTypeRead, name='BT6HumanEventTypeRead'),

    url(r'^BT7EcosystemType/(?P<pk>\d+)/read/$', Read_view.BT7EcosystemTypeRead, name='BT7EcosystemTypeRead'),

    url(r'^BT8AbioticElementType/(?P<pk>\d+)/read/$', Read_view.BT8AbioticElementTypeRead, name='BT8AbioticElementTypeRead'),

    url(r'^BT9ActorType/(?P<pk>\d+)/read/$', Read_view.BT9ActorTypeRead, name='BT9ActorTypeRead'),

    url(r'^Person/(?P<pk>\d+)/read/$', Read_view.PersonRead, name='PersonRead'),


    url(r'^BC10Event/list/$', List_view.BC10EventList, name='BC10EventList'),

    url(r'^BC11Person/list/$', List_view.BC11PersonList, name='BC11PersonList'),

    url(r'^BC12Ecosystem/list/$', List_view.BC12EcosystemList, name='BC12EcosystemList'),

    url(r'^BC13Organization/list/$', List_view.BC13OrganizationList, name='BC13OrganizationList'),

    url(r'^BC14EcosystemEnvironment/list/$', List_view.BC14EcosystemEnvironmentList, name='BC14EcosystemEnvironmentList'),

    url(r'^BC15WaterArea/list/$', List_view.BC15WaterAreaList, name='BC15WaterAreaList'),

    url(r'^BC16ManMadething/list/$', List_view.BC16ManMadethingList, name='BC16ManMadethingList'),

    url(r'^BC17ConceptualObject/list/$', List_view.BC17ConceptualObjectList, name='BC17ConceptualObjectList'),

    url(r'^BC18Proposition/list/$', List_view.BC18PropositionList, name='BC18PropositionList'),

    url(r'^BC19Title/list/$', List_view.BC19TitleList, name='BC19TitleList'),

    url(r'^BC1TLOEntity/list/$', List_view.BC1TLOEntityList, name='BC1TLOEntityList'),

    url(r'^BC20DeclarativePlace/list/$', List_view.BC20DeclarativePlaceList, name='BC20DeclarativePlaceList'),

    url(r'^BC21DataSet/list/$', List_view.BC21DataSetList, name='BC21DataSetList'),

    url(r'^BC22EncounterEvent/list/$', List_view.BC22EncounterEventList, name='BC22EncounterEventList'),

    url(r'^BC23DigitalObject/list/$', List_view.BC23DigitalObjectList, name='BC23DigitalObjectList'),

    url(r'^BC24RepositoryObject/list/$', List_view.BC24RepositoryObjectList, name='BC24RepositoryObjectList'),

    url(r'^BC25LinguisticObject/list/$', List_view.BC25LinguisticObjectList, name='BC25LinguisticObjectList'),

    url(r'^BC26PlaceName/list/$', List_view.BC26PlaceNameList, name='BC26PlaceNameList'),

    url(r'^BC27Publication/list/$', List_view.BC27PublicationList, name='BC27PublicationList'),

    url(r'^BC29SpatialCoordinateReferenceSystem/list/$', List_view.BC29SpatialCoordinateReferenceSystemList, name='BC29SpatialCoordinateReferenceSystemList'),

    url(r'^BC2TimeSpan/list/$', List_view.BC2TimeSpanList, name='BC2TimeSpanList'),

    url(r'^BC30Appellation/list/$', List_view.BC30AppellationList, name='BC30AppellationList'),

    url(r'^BC31PlaceAppellation/list/$', List_view.BC31PlaceAppellationList, name='BC31PlaceAppellationList'),

    url(r'^BC32Identifier/list/$', List_view.BC32IdentifierList, name='BC32IdentifierList'),

    url(r'^BC33SpatialCoordinate/list/$', List_view.BC33SpatialCoordinateList, name='BC33SpatialCoordinateList'),

    url(r'^BC34GeometricPlaceExpression/list/$', List_view.BC34GeometricPlaceExpressionList, name='BC34GeometricPlaceExpressionList'),

    url(r'^BC35PhysicalThing/list/$', List_view.BC35PhysicalThingList, name='BC35PhysicalThingList'),

    url(r'^BC36AbioticElement/list/$', List_view.BC36AbioticElementList, name='BC36AbioticElementList'),

    url(r'^BC37BiologicalObject/list/$', List_view.BC37BiologicalObjectList, name='BC37BiologicalObjectList'),

    url(r'^BC38BioticElement/list/$', List_view.BC38BioticElementList, name='BC38BioticElementList'),

    url(r'^BC39MarineAnimal/list/$', List_view.BC39MarineAnimalList, name='BC39MarineAnimalList'),

    url(r'^BC3Place/list/$', List_view.BC3PlaceList, name='BC3PlaceList'),

    url(r'^BC40PhysicalManMadeThing/list/$', List_view.BC40PhysicalManMadeThingList, name='BC40PhysicalManMadeThingList'),

    url(r'^BC41ManMadeObject/list/$', List_view.BC41ManMadeObjectList, name='BC41ManMadeObjectList'),

    url(r'^BC42Collection/list/$', List_view.BC42CollectionList, name='BC42CollectionList'),

    url(r'^BC43Activity/list/$', List_view.BC43ActivityList, name='BC43ActivityList'),

    url(r'^BC44AttributeAssignment/list/$', List_view.BC44AttributeAssignmentList, name='BC44AttributeAssignmentList'),

    url(r'^BC45Observation/list/$', List_view.BC45ObservationList, name='BC45ObservationList'),

    url(r'^BC46IdentifierAssignment/list/$', List_view.BC46IdentifierAssignmentList, name='BC46IdentifierAssignmentList'),

    url(r'^BC47Image/list/$', List_view.BC47ImageList, name='BC47ImageList'),

    url(r'^BC48Database/list/$', List_view.BC48DatabaseList, name='BC48DatabaseList'),

    url(r'^BC4TemporalPhenomenon/list/$', List_view.BC4TemporalPhenomenonList, name='BC4TemporalPhenomenonList'),

    url(r'^BC51PhysicalObject/list/$', List_view.BC51PhysicalObjectList, name='BC51PhysicalObjectList'),

    url(r'^BC53Specimen/list/$', List_view.BC53SpecimenList, name='BC53SpecimenList'),

    url(r'^BC54Measurement/list/$', List_view.BC54MeasurementList, name='BC54MeasurementList'),

    url(r'^BC55MeasurementUnit/list/$', List_view.BC55MeasurementUnitList, name='BC55MeasurementUnitList'),

    url(r'^BC56DigitalMeasurementEvent/list/$', List_view.BC56DigitalMeasurementEventList, name='BC56DigitalMeasurementEventList'),

    url(r'^BC57Capture/list/$', List_view.BC57CaptureList, name='BC57CaptureList'),

    url(r'^BC58DigitalDevice/list/$', List_view.BC58DigitalDeviceList, name='BC58DigitalDeviceList'),

    url(r'^BC59Software/list/$', List_view.BC59SoftwareList, name='BC59SoftwareList'),

    url(r'^BC5Dimension/list/$', List_view.BC5DimensionList, name='BC5DimensionList'),

    url(r'^BC60SoftwareExecution/list/$', List_view.BC60SoftwareExecutionList, name='BC60SoftwareExecutionList'),

    url(r'^BC61CaptureActivity/list/$', List_view.BC61CaptureActivityList, name='BC61CaptureActivityList'),

    url(r'^BC62StatisticIndicator/list/$', List_view.BC62StatisticIndicatorList, name='BC62StatisticIndicatorList'),

    url(r'^BC63GlobalStatisticLanding/list/$', List_view.BC63GlobalStatisticLandingList, name='BC63GlobalStatisticLandingList'),

    url(r'^BC64DesignorProcedure/list/$', List_view.BC64DesignorProcedureList, name='BC64DesignorProcedureList'),

    url(r'^BC6PersistentItem/list/$', List_view.BC6PersistentItemList, name='BC6PersistentItemList'),

    url(r'^BC70Group/list/$', List_view.BC70GroupList, name='BC70GroupList'),

    url(r'^BC71NameUseActivity/list/$', List_view.BC71NameUseActivityList, name='BC71NameUseActivityList'),

    url(r'^BC72SpaceTimeVolume/list/$', List_view.BC72SpaceTimeVolumeList, name='BC72SpaceTimeVolumeList'),

    url(r'^BC75Stock/list/$', List_view.BC75StockList, name='BC75StockList'),

    url(r'^BC76StockFormation/list/$', List_view.BC76StockFormationList, name='BC76StockFormationList'),

    url(r'^BC77MatterRemoval/list/$', List_view.BC77MatterRemovalList, name='BC77MatterRemovalList'),

    url(r'^BC78AmountofMatter/list/$', List_view.BC78AmountofMatterList, name='BC78AmountofMatterList'),

    url(r'^BC79Right/list/$', List_view.BC79RightList, name='BC79RightList'),

    url(r'^BC7Thing/list/$', List_view.BC7ThingList, name='BC7ThingList'),

    url(r'^BC8Actor/list/$', List_view.BC8ActorList, name='BC8ActorList'),

    url(r'^BC9ObservableEntity/list/$', List_view.BC9ObservableEntityList, name='BC9ObservableEntityList'),

    url(r'^BT10BioticElementType/list/$', List_view.BT10BioticElementTypeList, name='BT10BioticElementTypeList'),

    url(r'^BT11EquipmentType/list/$', List_view.BT11EquipmentTypeList, name='BT11EquipmentTypeList'),

    url(r'^BT12ScientificDataType/list/$', List_view.BT12ScientificDataTypeList, name='BT12ScientificDataTypeList'),

    url(r'^BT13DigitalObjectType/list/$', List_view.BT13DigitalObjectTypeList, name='BT13DigitalObjectTypeList'),

    url(r'^BT14AppellationType/list/$', List_view.BT14AppellationTypeList, name='BT14AppellationTypeList'),

    url(r'^BT16VisualType/list/$', List_view.BT16VisualTypeList, name='BT16VisualTypeList'),

    url(r'^BT17HumanActivityType/list/$', List_view.BT17HumanActivityTypeList, name='BT17HumanActivityTypeList'),

    url(r'^BT18Kingdom/list/$', List_view.BT18KingdomList, name='BT18KingdomList'),

    url(r'^BT19Phylum/list/$', List_view.BT19PhylumList, name='BT19PhylumList'),

    url(r'^BT1TLOEntityType/list/$', List_view.BT1TLOEntityTypeList, name='BT1TLOEntityTypeList'),

    url(r'^BT20SubPhylum/list/$', List_view.BT20SubPhylumList, name='BT20SubPhylumList'),

    url(r'^BT21SuperClass/list/$', List_view.BT21SuperClassList, name='BT21SuperClassList'),

    url(r'^BT22Class/list/$', List_view.BT22ClassList, name='BT22ClassList'),

    url(r'^BT23SubClass/list/$', List_view.BT23SubClassList, name='BT23SubClassList'),

    url(r'^BT24Family/list/$', List_view.BT24FamilyList, name='BT24FamilyList'),

    url(r'^BT25SubFamily/list/$', List_view.BT25SubFamilyList, name='BT25SubFamilyList'),

    url(r'^BT26Genus/list/$', List_view.BT26GenusList, name='BT26GenusList'),

    url(r'^BT27Species/list/$', List_view.BT27SpeciesList, name='BT27SpeciesList'),

    url(r'^BT28ScientificActivityType/list/$', List_view.BT28ScientificActivityTypeList, name='BT28ScientificActivityTypeList'),

    url(r'^BT29IndustrialActivityType/list/$', List_view.BT29IndustrialActivityTypeList, name='BT29IndustrialActivityTypeList'),

    url(r'^BT2TemporalPhenomenonType/list/$', List_view.BT2TemporalPhenomenonTypeList, name='BT2TemporalPhenomenonTypeList'),

    url(r'^BT30IdentifierAssignmentType/list/$', List_view.BT30IdentifierAssignmentTypeList, name='BT30IdentifierAssignmentTypeList'),

    url(r'^BT31BiologicalPartTime/list/$', List_view.BT31BiologicalPartTimeList, name='BT31BiologicalPartTimeList'),

    url(r'^BT32PersistenType/list/$', List_view.BT32PersistenTypeList, name='BT32PersistenTypeList'),

    url(r'^BT32PersistentType/list/$', List_view.BT32PersistentTypeList, name='BT32PersistentTypeList'),

    url(r'^BT33MarineAnimalType/list/$', List_view.BT33MarineAnimalTypeList, name='BT33MarineAnimalTypeList'),

    url(r'^BT34Order/list/$', List_view.BT34OrderList, name='BT34OrderList'),

    url(r'^BT35PropertyType/list/$', List_view.BT35PropertyTypeList, name='BT35PropertyTypeList'),

    url(r'^BT36Language/list/$', List_view.BT36LanguageList, name='BT36LanguageList'),

    url(r'^BT3PhysicalThingType/list/$', List_view.BT3PhysicalThingTypeList, name='BT3PhysicalThingTypeList'),

    url(r'^BT4ConceptualObjectType/list/$', List_view.BT4ConceptualObjectTypeList, name='BT4ConceptualObjectTypeList'),

    url(r'^BT5LegislativeZoneType/list/$', List_view.BT5LegislativeZoneTypeList, name='BT5LegislativeZoneTypeList'),

    url(r'^BT6HumanEventType/list/$', List_view.BT6HumanEventTypeList, name='BT6HumanEventTypeList'),

    url(r'^BT7EcosystemType/list/$', List_view.BT7EcosystemTypeList, name='BT7EcosystemTypeList'),

    url(r'^BT8AbioticElementType/list/$', List_view.BT8AbioticElementTypeList, name='BT8AbioticElementTypeList'),

    url(r'^BT9ActorType/list/$', List_view.BT9ActorTypeList, name='BT9ActorTypeList'),

    url(r'^Person/list/$', List_view.PersonList, name='PersonList'),


    url(r'^BC10Event/(?P<pk>\d+)/update/$', Update_view.BC10EventUpdate, name='BC10EventUpdate'),

    url(r'^BC11Person/(?P<pk>\d+)/update/$', Update_view.BC11PersonUpdate, name='BC11PersonUpdate'),

    url(r'^BC12Ecosystem/(?P<pk>\d+)/update/$', Update_view.BC12EcosystemUpdate, name='BC12EcosystemUpdate'),

    url(r'^BC13Organization/(?P<pk>\d+)/update/$', Update_view.BC13OrganizationUpdate, name='BC13OrganizationUpdate'),

    url(r'^BC14EcosystemEnvironment/(?P<pk>\d+)/update/$', Update_view.BC14EcosystemEnvironmentUpdate, name='BC14EcosystemEnvironmentUpdate'),

    url(r'^BC15WaterArea/(?P<pk>\d+)/update/$', Update_view.BC15WaterAreaUpdate, name='BC15WaterAreaUpdate'),

    url(r'^BC16ManMadething/(?P<pk>\d+)/update/$', Update_view.BC16ManMadethingUpdate, name='BC16ManMadethingUpdate'),

    url(r'^BC17ConceptualObject/(?P<pk>\d+)/update/$', Update_view.BC17ConceptualObjectUpdate, name='BC17ConceptualObjectUpdate'),

    url(r'^BC18Proposition/(?P<pk>\d+)/update/$', Update_view.BC18PropositionUpdate, name='BC18PropositionUpdate'),

    url(r'^BC19Title/(?P<pk>\d+)/update/$', Update_view.BC19TitleUpdate, name='BC19TitleUpdate'),

    url(r'^BC1TLOEntity/(?P<pk>\d+)/update/$', Update_view.BC1TLOEntityUpdate, name='BC1TLOEntityUpdate'),

    url(r'^BC20DeclarativePlace/(?P<pk>\d+)/update/$', Update_view.BC20DeclarativePlaceUpdate, name='BC20DeclarativePlaceUpdate'),

    url(r'^BC21DataSet/(?P<pk>\d+)/update/$', Update_view.BC21DataSetUpdate, name='BC21DataSetUpdate'),

    url(r'^BC22EncounterEvent/(?P<pk>\d+)/update/$', Update_view.BC22EncounterEventUpdate, name='BC22EncounterEventUpdate'),

    url(r'^BC23DigitalObject/(?P<pk>\d+)/update/$', Update_view.BC23DigitalObjectUpdate, name='BC23DigitalObjectUpdate'),

    url(r'^BC24RepositoryObject/(?P<pk>\d+)/update/$', Update_view.BC24RepositoryObjectUpdate, name='BC24RepositoryObjectUpdate'),

    url(r'^BC25LinguisticObject/(?P<pk>\d+)/update/$', Update_view.BC25LinguisticObjectUpdate, name='BC25LinguisticObjectUpdate'),

    url(r'^BC26PlaceName/(?P<pk>\d+)/update/$', Update_view.BC26PlaceNameUpdate, name='BC26PlaceNameUpdate'),

    url(r'^BC27Publication/(?P<pk>\d+)/update/$', Update_view.BC27PublicationUpdate, name='BC27PublicationUpdate'),

    url(r'^BC29SpatialCoordinateReferenceSystem/(?P<pk>\d+)/update/$', Update_view.BC29SpatialCoordinateReferenceSystemUpdate, name='BC29SpatialCoordinateReferenceSystemUpdate'),

    url(r'^BC2TimeSpan/(?P<pk>\d+)/update/$', Update_view.BC2TimeSpanUpdate, name='BC2TimeSpanUpdate'),

    url(r'^BC30Appellation/(?P<pk>\d+)/update/$', Update_view.BC30AppellationUpdate, name='BC30AppellationUpdate'),

    url(r'^BC31PlaceAppellation/(?P<pk>\d+)/update/$', Update_view.BC31PlaceAppellationUpdate, name='BC31PlaceAppellationUpdate'),

    url(r'^BC32Identifier/(?P<pk>\d+)/update/$', Update_view.BC32IdentifierUpdate, name='BC32IdentifierUpdate'),

    url(r'^BC33SpatialCoordinate/(?P<pk>\d+)/update/$', Update_view.BC33SpatialCoordinateUpdate, name='BC33SpatialCoordinateUpdate'),

    url(r'^BC34GeometricPlaceExpression/(?P<pk>\d+)/update/$', Update_view.BC34GeometricPlaceExpressionUpdate, name='BC34GeometricPlaceExpressionUpdate'),

    url(r'^BC35PhysicalThing/(?P<pk>\d+)/update/$', Update_view.BC35PhysicalThingUpdate, name='BC35PhysicalThingUpdate'),

    url(r'^BC36AbioticElement/(?P<pk>\d+)/update/$', Update_view.BC36AbioticElementUpdate, name='BC36AbioticElementUpdate'),

    url(r'^BC37BiologicalObject/(?P<pk>\d+)/update/$', Update_view.BC37BiologicalObjectUpdate, name='BC37BiologicalObjectUpdate'),

    url(r'^BC38BioticElement/(?P<pk>\d+)/update/$', Update_view.BC38BioticElementUpdate, name='BC38BioticElementUpdate'),

    url(r'^BC39MarineAnimal/(?P<pk>\d+)/update/$', Update_view.BC39MarineAnimalUpdate, name='BC39MarineAnimalUpdate'),

    url(r'^BC3Place/(?P<pk>\d+)/update/$', Update_view.BC3PlaceUpdate, name='BC3PlaceUpdate'),

    url(r'^BC40PhysicalManMadeThing/(?P<pk>\d+)/update/$', Update_view.BC40PhysicalManMadeThingUpdate, name='BC40PhysicalManMadeThingUpdate'),

    url(r'^BC41ManMadeObject/(?P<pk>\d+)/update/$', Update_view.BC41ManMadeObjectUpdate, name='BC41ManMadeObjectUpdate'),

    url(r'^BC42Collection/(?P<pk>\d+)/update/$', Update_view.BC42CollectionUpdate, name='BC42CollectionUpdate'),

    url(r'^BC43Activity/(?P<pk>\d+)/update/$', Update_view.BC43ActivityUpdate, name='BC43ActivityUpdate'),

    url(r'^BC44AttributeAssignment/(?P<pk>\d+)/update/$', Update_view.BC44AttributeAssignmentUpdate, name='BC44AttributeAssignmentUpdate'),

    url(r'^BC45Observation/(?P<pk>\d+)/update/$', Update_view.BC45ObservationUpdate, name='BC45ObservationUpdate'),

    url(r'^BC46IdentifierAssignment/(?P<pk>\d+)/update/$', Update_view.BC46IdentifierAssignmentUpdate, name='BC46IdentifierAssignmentUpdate'),

    url(r'^BC47Image/(?P<pk>\d+)/update/$', Update_view.BC47ImageUpdate, name='BC47ImageUpdate'),

    url(r'^BC48Database/(?P<pk>\d+)/update/$', Update_view.BC48DatabaseUpdate, name='BC48DatabaseUpdate'),

    url(r'^BC4TemporalPhenomenon/(?P<pk>\d+)/update/$', Update_view.BC4TemporalPhenomenonUpdate, name='BC4TemporalPhenomenonUpdate'),

    url(r'^BC51PhysicalObject/(?P<pk>\d+)/update/$', Update_view.BC51PhysicalObjectUpdate, name='BC51PhysicalObjectUpdate'),

    url(r'^BC53Specimen/(?P<pk>\d+)/update/$', Update_view.BC53SpecimenUpdate, name='BC53SpecimenUpdate'),

    url(r'^BC54Measurement/(?P<pk>\d+)/update/$', Update_view.BC54MeasurementUpdate, name='BC54MeasurementUpdate'),

    url(r'^BC55MeasurementUnit/(?P<pk>\d+)/update/$', Update_view.BC55MeasurementUnitUpdate, name='BC55MeasurementUnitUpdate'),

    url(r'^BC56DigitalMeasurementEvent/(?P<pk>\d+)/update/$', Update_view.BC56DigitalMeasurementEventUpdate, name='BC56DigitalMeasurementEventUpdate'),

    url(r'^BC57Capture/(?P<pk>\d+)/update/$', Update_view.BC57CaptureUpdate, name='BC57CaptureUpdate'),

    url(r'^BC58DigitalDevice/(?P<pk>\d+)/update/$', Update_view.BC58DigitalDeviceUpdate, name='BC58DigitalDeviceUpdate'),

    url(r'^BC59Software/(?P<pk>\d+)/update/$', Update_view.BC59SoftwareUpdate, name='BC59SoftwareUpdate'),

    url(r'^BC5Dimension/(?P<pk>\d+)/update/$', Update_view.BC5DimensionUpdate, name='BC5DimensionUpdate'),

    url(r'^BC60SoftwareExecution/(?P<pk>\d+)/update/$', Update_view.BC60SoftwareExecutionUpdate, name='BC60SoftwareExecutionUpdate'),

    url(r'^BC61CaptureActivity/(?P<pk>\d+)/update/$', Update_view.BC61CaptureActivityUpdate, name='BC61CaptureActivityUpdate'),

    url(r'^BC62StatisticIndicator/(?P<pk>\d+)/update/$', Update_view.BC62StatisticIndicatorUpdate, name='BC62StatisticIndicatorUpdate'),

    url(r'^BC63GlobalStatisticLanding/(?P<pk>\d+)/update/$', Update_view.BC63GlobalStatisticLandingUpdate, name='BC63GlobalStatisticLandingUpdate'),

    url(r'^BC64DesignorProcedure/(?P<pk>\d+)/update/$', Update_view.BC64DesignorProcedureUpdate, name='BC64DesignorProcedureUpdate'),

    url(r'^BC6PersistentItem/(?P<pk>\d+)/update/$', Update_view.BC6PersistentItemUpdate, name='BC6PersistentItemUpdate'),

    url(r'^BC70Group/(?P<pk>\d+)/update/$', Update_view.BC70GroupUpdate, name='BC70GroupUpdate'),

    url(r'^BC71NameUseActivity/(?P<pk>\d+)/update/$', Update_view.BC71NameUseActivityUpdate, name='BC71NameUseActivityUpdate'),

    url(r'^BC72SpaceTimeVolume/(?P<pk>\d+)/update/$', Update_view.BC72SpaceTimeVolumeUpdate, name='BC72SpaceTimeVolumeUpdate'),

    url(r'^BC75Stock/(?P<pk>\d+)/update/$', Update_view.BC75StockUpdate, name='BC75StockUpdate'),

    url(r'^BC76StockFormation/(?P<pk>\d+)/update/$', Update_view.BC76StockFormationUpdate, name='BC76StockFormationUpdate'),

    url(r'^BC77MatterRemoval/(?P<pk>\d+)/update/$', Update_view.BC77MatterRemovalUpdate, name='BC77MatterRemovalUpdate'),

    url(r'^BC78AmountofMatter/(?P<pk>\d+)/update/$', Update_view.BC78AmountofMatterUpdate, name='BC78AmountofMatterUpdate'),

    url(r'^BC79Right/(?P<pk>\d+)/update/$', Update_view.BC79RightUpdate, name='BC79RightUpdate'),

    url(r'^BC7Thing/(?P<pk>\d+)/update/$', Update_view.BC7ThingUpdate, name='BC7ThingUpdate'),

    url(r'^BC8Actor/(?P<pk>\d+)/update/$', Update_view.BC8ActorUpdate, name='BC8ActorUpdate'),

    url(r'^BC9ObservableEntity/(?P<pk>\d+)/update/$', Update_view.BC9ObservableEntityUpdate, name='BC9ObservableEntityUpdate'),

    url(r'^BT10BioticElementType/(?P<pk>\d+)/update/$', Update_view.BT10BioticElementTypeUpdate, name='BT10BioticElementTypeUpdate'),

    url(r'^BT11EquipmentType/(?P<pk>\d+)/update/$', Update_view.BT11EquipmentTypeUpdate, name='BT11EquipmentTypeUpdate'),

    url(r'^BT12ScientificDataType/(?P<pk>\d+)/update/$', Update_view.BT12ScientificDataTypeUpdate, name='BT12ScientificDataTypeUpdate'),

    url(r'^BT13DigitalObjectType/(?P<pk>\d+)/update/$', Update_view.BT13DigitalObjectTypeUpdate, name='BT13DigitalObjectTypeUpdate'),

    url(r'^BT14AppellationType/(?P<pk>\d+)/update/$', Update_view.BT14AppellationTypeUpdate, name='BT14AppellationTypeUpdate'),

    url(r'^BT16VisualType/(?P<pk>\d+)/update/$', Update_view.BT16VisualTypeUpdate, name='BT16VisualTypeUpdate'),

    url(r'^BT17HumanActivityType/(?P<pk>\d+)/update/$', Update_view.BT17HumanActivityTypeUpdate, name='BT17HumanActivityTypeUpdate'),

    url(r'^BT18Kingdom/(?P<pk>\d+)/update/$', Update_view.BT18KingdomUpdate, name='BT18KingdomUpdate'),

    url(r'^BT19Phylum/(?P<pk>\d+)/update/$', Update_view.BT19PhylumUpdate, name='BT19PhylumUpdate'),

    url(r'^BT1TLOEntityType/(?P<pk>\d+)/update/$', Update_view.BT1TLOEntityTypeUpdate, name='BT1TLOEntityTypeUpdate'),

    url(r'^BT20SubPhylum/(?P<pk>\d+)/update/$', Update_view.BT20SubPhylumUpdate, name='BT20SubPhylumUpdate'),

    url(r'^BT21SuperClass/(?P<pk>\d+)/update/$', Update_view.BT21SuperClassUpdate, name='BT21SuperClassUpdate'),

    url(r'^BT22Class/(?P<pk>\d+)/update/$', Update_view.BT22ClassUpdate, name='BT22ClassUpdate'),

    url(r'^BT23SubClass/(?P<pk>\d+)/update/$', Update_view.BT23SubClassUpdate, name='BT23SubClassUpdate'),

    url(r'^BT24Family/(?P<pk>\d+)/update/$', Update_view.BT24FamilyUpdate, name='BT24FamilyUpdate'),

    url(r'^BT25SubFamily/(?P<pk>\d+)/update/$', Update_view.BT25SubFamilyUpdate, name='BT25SubFamilyUpdate'),

    url(r'^BT26Genus/(?P<pk>\d+)/update/$', Update_view.BT26GenusUpdate, name='BT26GenusUpdate'),

    url(r'^BT27Species/(?P<pk>\d+)/update/$', Update_view.BT27SpeciesUpdate, name='BT27SpeciesUpdate'),

    url(r'^BT28ScientificActivityType/(?P<pk>\d+)/update/$', Update_view.BT28ScientificActivityTypeUpdate, name='BT28ScientificActivityTypeUpdate'),

    url(r'^BT29IndustrialActivityType/(?P<pk>\d+)/update/$', Update_view.BT29IndustrialActivityTypeUpdate, name='BT29IndustrialActivityTypeUpdate'),

    url(r'^BT2TemporalPhenomenonType/(?P<pk>\d+)/update/$', Update_view.BT2TemporalPhenomenonTypeUpdate, name='BT2TemporalPhenomenonTypeUpdate'),

    url(r'^BT30IdentifierAssignmentType/(?P<pk>\d+)/update/$', Update_view.BT30IdentifierAssignmentTypeUpdate, name='BT30IdentifierAssignmentTypeUpdate'),

    url(r'^BT31BiologicalPartTime/(?P<pk>\d+)/update/$', Update_view.BT31BiologicalPartTimeUpdate, name='BT31BiologicalPartTimeUpdate'),

    url(r'^BT32PersistenType/(?P<pk>\d+)/update/$', Update_view.BT32PersistenTypeUpdate, name='BT32PersistenTypeUpdate'),

    url(r'^BT32PersistentType/(?P<pk>\d+)/update/$', Update_view.BT32PersistentTypeUpdate, name='BT32PersistentTypeUpdate'),

    url(r'^BT33MarineAnimalType/(?P<pk>\d+)/update/$', Update_view.BT33MarineAnimalTypeUpdate, name='BT33MarineAnimalTypeUpdate'),

    url(r'^BT34Order/(?P<pk>\d+)/update/$', Update_view.BT34OrderUpdate, name='BT34OrderUpdate'),

    url(r'^BT35PropertyType/(?P<pk>\d+)/update/$', Update_view.BT35PropertyTypeUpdate, name='BT35PropertyTypeUpdate'),

    url(r'^BT36Language/(?P<pk>\d+)/update/$', Update_view.BT36LanguageUpdate, name='BT36LanguageUpdate'),

    url(r'^BT3PhysicalThingType/(?P<pk>\d+)/update/$', Update_view.BT3PhysicalThingTypeUpdate, name='BT3PhysicalThingTypeUpdate'),

    url(r'^BT4ConceptualObjectType/(?P<pk>\d+)/update/$', Update_view.BT4ConceptualObjectTypeUpdate, name='BT4ConceptualObjectTypeUpdate'),

    url(r'^BT5LegislativeZoneType/(?P<pk>\d+)/update/$', Update_view.BT5LegislativeZoneTypeUpdate, name='BT5LegislativeZoneTypeUpdate'),

    url(r'^BT6HumanEventType/(?P<pk>\d+)/update/$', Update_view.BT6HumanEventTypeUpdate, name='BT6HumanEventTypeUpdate'),

    url(r'^BT7EcosystemType/(?P<pk>\d+)/update/$', Update_view.BT7EcosystemTypeUpdate, name='BT7EcosystemTypeUpdate'),

    url(r'^BT8AbioticElementType/(?P<pk>\d+)/update/$', Update_view.BT8AbioticElementTypeUpdate, name='BT8AbioticElementTypeUpdate'),

    url(r'^BT9ActorType/(?P<pk>\d+)/update/$', Update_view.BT9ActorTypeUpdate, name='BT9ActorTypeUpdate'),

    url(r'^Person/(?P<pk>\d+)/update/$', Update_view.PersonUpdate, name='PersonUpdate'),


    url(r'^BC10Event/(?P<pk>\d+)/delete/$', Delete_view.BC10EventDelete, name='BC10EventDelete'),

    url(r'^BC11Person/(?P<pk>\d+)/delete/$', Delete_view.BC11PersonDelete, name='BC11PersonDelete'),

    url(r'^BC12Ecosystem/(?P<pk>\d+)/delete/$', Delete_view.BC12EcosystemDelete, name='BC12EcosystemDelete'),

    url(r'^BC13Organization/(?P<pk>\d+)/delete/$', Delete_view.BC13OrganizationDelete, name='BC13OrganizationDelete'),

    url(r'^BC14EcosystemEnvironment/(?P<pk>\d+)/delete/$', Delete_view.BC14EcosystemEnvironmentDelete, name='BC14EcosystemEnvironmentDelete'),

    url(r'^BC15WaterArea/(?P<pk>\d+)/delete/$', Delete_view.BC15WaterAreaDelete, name='BC15WaterAreaDelete'),

    url(r'^BC16ManMadething/(?P<pk>\d+)/delete/$', Delete_view.BC16ManMadethingDelete, name='BC16ManMadethingDelete'),

    url(r'^BC17ConceptualObject/(?P<pk>\d+)/delete/$', Delete_view.BC17ConceptualObjectDelete, name='BC17ConceptualObjectDelete'),

    url(r'^BC18Proposition/(?P<pk>\d+)/delete/$', Delete_view.BC18PropositionDelete, name='BC18PropositionDelete'),

    url(r'^BC19Title/(?P<pk>\d+)/delete/$', Delete_view.BC19TitleDelete, name='BC19TitleDelete'),

    url(r'^BC1TLOEntity/(?P<pk>\d+)/delete/$', Delete_view.BC1TLOEntityDelete, name='BC1TLOEntityDelete'),

    url(r'^BC20DeclarativePlace/(?P<pk>\d+)/delete/$', Delete_view.BC20DeclarativePlaceDelete, name='BC20DeclarativePlaceDelete'),

    url(r'^BC21DataSet/(?P<pk>\d+)/delete/$', Delete_view.BC21DataSetDelete, name='BC21DataSetDelete'),

    url(r'^BC22EncounterEvent/(?P<pk>\d+)/delete/$', Delete_view.BC22EncounterEventDelete, name='BC22EncounterEventDelete'),

    url(r'^BC23DigitalObject/(?P<pk>\d+)/delete/$', Delete_view.BC23DigitalObjectDelete, name='BC23DigitalObjectDelete'),

    url(r'^BC24RepositoryObject/(?P<pk>\d+)/delete/$', Delete_view.BC24RepositoryObjectDelete, name='BC24RepositoryObjectDelete'),

    url(r'^BC25LinguisticObject/(?P<pk>\d+)/delete/$', Delete_view.BC25LinguisticObjectDelete, name='BC25LinguisticObjectDelete'),

    url(r'^BC26PlaceName/(?P<pk>\d+)/delete/$', Delete_view.BC26PlaceNameDelete, name='BC26PlaceNameDelete'),

    url(r'^BC27Publication/(?P<pk>\d+)/delete/$', Delete_view.BC27PublicationDelete, name='BC27PublicationDelete'),

    url(r'^BC29SpatialCoordinateReferenceSystem/(?P<pk>\d+)/delete/$', Delete_view.BC29SpatialCoordinateReferenceSystemDelete, name='BC29SpatialCoordinateReferenceSystemDelete'),

    url(r'^BC2TimeSpan/(?P<pk>\d+)/delete/$', Delete_view.BC2TimeSpanDelete, name='BC2TimeSpanDelete'),

    url(r'^BC30Appellation/(?P<pk>\d+)/delete/$', Delete_view.BC30AppellationDelete, name='BC30AppellationDelete'),

    url(r'^BC31PlaceAppellation/(?P<pk>\d+)/delete/$', Delete_view.BC31PlaceAppellationDelete, name='BC31PlaceAppellationDelete'),

    url(r'^BC32Identifier/(?P<pk>\d+)/delete/$', Delete_view.BC32IdentifierDelete, name='BC32IdentifierDelete'),

    url(r'^BC33SpatialCoordinate/(?P<pk>\d+)/delete/$', Delete_view.BC33SpatialCoordinateDelete, name='BC33SpatialCoordinateDelete'),

    url(r'^BC34GeometricPlaceExpression/(?P<pk>\d+)/delete/$', Delete_view.BC34GeometricPlaceExpressionDelete, name='BC34GeometricPlaceExpressionDelete'),

    url(r'^BC35PhysicalThing/(?P<pk>\d+)/delete/$', Delete_view.BC35PhysicalThingDelete, name='BC35PhysicalThingDelete'),

    url(r'^BC36AbioticElement/(?P<pk>\d+)/delete/$', Delete_view.BC36AbioticElementDelete, name='BC36AbioticElementDelete'),

    url(r'^BC37BiologicalObject/(?P<pk>\d+)/delete/$', Delete_view.BC37BiologicalObjectDelete, name='BC37BiologicalObjectDelete'),

    url(r'^BC38BioticElement/(?P<pk>\d+)/delete/$', Delete_view.BC38BioticElementDelete, name='BC38BioticElementDelete'),

    url(r'^BC39MarineAnimal/(?P<pk>\d+)/delete/$', Delete_view.BC39MarineAnimalDelete, name='BC39MarineAnimalDelete'),

    url(r'^BC3Place/(?P<pk>\d+)/delete/$', Delete_view.BC3PlaceDelete, name='BC3PlaceDelete'),

    url(r'^BC40PhysicalManMadeThing/(?P<pk>\d+)/delete/$', Delete_view.BC40PhysicalManMadeThingDelete, name='BC40PhysicalManMadeThingDelete'),

    url(r'^BC41ManMadeObject/(?P<pk>\d+)/delete/$', Delete_view.BC41ManMadeObjectDelete, name='BC41ManMadeObjectDelete'),

    url(r'^BC42Collection/(?P<pk>\d+)/delete/$', Delete_view.BC42CollectionDelete, name='BC42CollectionDelete'),

    url(r'^BC43Activity/(?P<pk>\d+)/delete/$', Delete_view.BC43ActivityDelete, name='BC43ActivityDelete'),

    url(r'^BC44AttributeAssignment/(?P<pk>\d+)/delete/$', Delete_view.BC44AttributeAssignmentDelete, name='BC44AttributeAssignmentDelete'),

    url(r'^BC45Observation/(?P<pk>\d+)/delete/$', Delete_view.BC45ObservationDelete, name='BC45ObservationDelete'),

    url(r'^BC46IdentifierAssignment/(?P<pk>\d+)/delete/$', Delete_view.BC46IdentifierAssignmentDelete, name='BC46IdentifierAssignmentDelete'),

    url(r'^BC47Image/(?P<pk>\d+)/delete/$', Delete_view.BC47ImageDelete, name='BC47ImageDelete'),

    url(r'^BC48Database/(?P<pk>\d+)/delete/$', Delete_view.BC48DatabaseDelete, name='BC48DatabaseDelete'),

    url(r'^BC4TemporalPhenomenon/(?P<pk>\d+)/delete/$', Delete_view.BC4TemporalPhenomenonDelete, name='BC4TemporalPhenomenonDelete'),

    url(r'^BC51PhysicalObject/(?P<pk>\d+)/delete/$', Delete_view.BC51PhysicalObjectDelete, name='BC51PhysicalObjectDelete'),

    url(r'^BC53Specimen/(?P<pk>\d+)/delete/$', Delete_view.BC53SpecimenDelete, name='BC53SpecimenDelete'),

    url(r'^BC54Measurement/(?P<pk>\d+)/delete/$', Delete_view.BC54MeasurementDelete, name='BC54MeasurementDelete'),

    url(r'^BC55MeasurementUnit/(?P<pk>\d+)/delete/$', Delete_view.BC55MeasurementUnitDelete, name='BC55MeasurementUnitDelete'),

    url(r'^BC56DigitalMeasurementEvent/(?P<pk>\d+)/delete/$', Delete_view.BC56DigitalMeasurementEventDelete, name='BC56DigitalMeasurementEventDelete'),

    url(r'^BC57Capture/(?P<pk>\d+)/delete/$', Delete_view.BC57CaptureDelete, name='BC57CaptureDelete'),

    url(r'^BC58DigitalDevice/(?P<pk>\d+)/delete/$', Delete_view.BC58DigitalDeviceDelete, name='BC58DigitalDeviceDelete'),

    url(r'^BC59Software/(?P<pk>\d+)/delete/$', Delete_view.BC59SoftwareDelete, name='BC59SoftwareDelete'),

    url(r'^BC5Dimension/(?P<pk>\d+)/delete/$', Delete_view.BC5DimensionDelete, name='BC5DimensionDelete'),

    url(r'^BC60SoftwareExecution/(?P<pk>\d+)/delete/$', Delete_view.BC60SoftwareExecutionDelete, name='BC60SoftwareExecutionDelete'),

    url(r'^BC61CaptureActivity/(?P<pk>\d+)/delete/$', Delete_view.BC61CaptureActivityDelete, name='BC61CaptureActivityDelete'),

    url(r'^BC62StatisticIndicator/(?P<pk>\d+)/delete/$', Delete_view.BC62StatisticIndicatorDelete, name='BC62StatisticIndicatorDelete'),

    url(r'^BC63GlobalStatisticLanding/(?P<pk>\d+)/delete/$', Delete_view.BC63GlobalStatisticLandingDelete, name='BC63GlobalStatisticLandingDelete'),

    url(r'^BC64DesignorProcedure/(?P<pk>\d+)/delete/$', Delete_view.BC64DesignorProcedureDelete, name='BC64DesignorProcedureDelete'),

    url(r'^BC6PersistentItem/(?P<pk>\d+)/delete/$', Delete_view.BC6PersistentItemDelete, name='BC6PersistentItemDelete'),

    url(r'^BC70Group/(?P<pk>\d+)/delete/$', Delete_view.BC70GroupDelete, name='BC70GroupDelete'),

    url(r'^BC71NameUseActivity/(?P<pk>\d+)/delete/$', Delete_view.BC71NameUseActivityDelete, name='BC71NameUseActivityDelete'),

    url(r'^BC72SpaceTimeVolume/(?P<pk>\d+)/delete/$', Delete_view.BC72SpaceTimeVolumeDelete, name='BC72SpaceTimeVolumeDelete'),

    url(r'^BC75Stock/(?P<pk>\d+)/delete/$', Delete_view.BC75StockDelete, name='BC75StockDelete'),

    url(r'^BC76StockFormation/(?P<pk>\d+)/delete/$', Delete_view.BC76StockFormationDelete, name='BC76StockFormationDelete'),

    url(r'^BC77MatterRemoval/(?P<pk>\d+)/delete/$', Delete_view.BC77MatterRemovalDelete, name='BC77MatterRemovalDelete'),

    url(r'^BC78AmountofMatter/(?P<pk>\d+)/delete/$', Delete_view.BC78AmountofMatterDelete, name='BC78AmountofMatterDelete'),

    url(r'^BC79Right/(?P<pk>\d+)/delete/$', Delete_view.BC79RightDelete, name='BC79RightDelete'),

    url(r'^BC7Thing/(?P<pk>\d+)/delete/$', Delete_view.BC7ThingDelete, name='BC7ThingDelete'),

    url(r'^BC8Actor/(?P<pk>\d+)/delete/$', Delete_view.BC8ActorDelete, name='BC8ActorDelete'),

    url(r'^BC9ObservableEntity/(?P<pk>\d+)/delete/$', Delete_view.BC9ObservableEntityDelete, name='BC9ObservableEntityDelete'),

    url(r'^BT10BioticElementType/(?P<pk>\d+)/delete/$', Delete_view.BT10BioticElementTypeDelete, name='BT10BioticElementTypeDelete'),

    url(r'^BT11EquipmentType/(?P<pk>\d+)/delete/$', Delete_view.BT11EquipmentTypeDelete, name='BT11EquipmentTypeDelete'),

    url(r'^BT12ScientificDataType/(?P<pk>\d+)/delete/$', Delete_view.BT12ScientificDataTypeDelete, name='BT12ScientificDataTypeDelete'),

    url(r'^BT13DigitalObjectType/(?P<pk>\d+)/delete/$', Delete_view.BT13DigitalObjectTypeDelete, name='BT13DigitalObjectTypeDelete'),

    url(r'^BT14AppellationType/(?P<pk>\d+)/delete/$', Delete_view.BT14AppellationTypeDelete, name='BT14AppellationTypeDelete'),

    url(r'^BT16VisualType/(?P<pk>\d+)/delete/$', Delete_view.BT16VisualTypeDelete, name='BT16VisualTypeDelete'),

    url(r'^BT17HumanActivityType/(?P<pk>\d+)/delete/$', Delete_view.BT17HumanActivityTypeDelete, name='BT17HumanActivityTypeDelete'),

    url(r'^BT18Kingdom/(?P<pk>\d+)/delete/$', Delete_view.BT18KingdomDelete, name='BT18KingdomDelete'),

    url(r'^BT19Phylum/(?P<pk>\d+)/delete/$', Delete_view.BT19PhylumDelete, name='BT19PhylumDelete'),

    url(r'^BT1TLOEntityType/(?P<pk>\d+)/delete/$', Delete_view.BT1TLOEntityTypeDelete, name='BT1TLOEntityTypeDelete'),

    url(r'^BT20SubPhylum/(?P<pk>\d+)/delete/$', Delete_view.BT20SubPhylumDelete, name='BT20SubPhylumDelete'),

    url(r'^BT21SuperClass/(?P<pk>\d+)/delete/$', Delete_view.BT21SuperClassDelete, name='BT21SuperClassDelete'),

    url(r'^BT22Class/(?P<pk>\d+)/delete/$', Delete_view.BT22ClassDelete, name='BT22ClassDelete'),

    url(r'^BT23SubClass/(?P<pk>\d+)/delete/$', Delete_view.BT23SubClassDelete, name='BT23SubClassDelete'),

    url(r'^BT24Family/(?P<pk>\d+)/delete/$', Delete_view.BT24FamilyDelete, name='BT24FamilyDelete'),

    url(r'^BT25SubFamily/(?P<pk>\d+)/delete/$', Delete_view.BT25SubFamilyDelete, name='BT25SubFamilyDelete'),

    url(r'^BT26Genus/(?P<pk>\d+)/delete/$', Delete_view.BT26GenusDelete, name='BT26GenusDelete'),

    url(r'^BT27Species/(?P<pk>\d+)/delete/$', Delete_view.BT27SpeciesDelete, name='BT27SpeciesDelete'),

    url(r'^BT28ScientificActivityType/(?P<pk>\d+)/delete/$', Delete_view.BT28ScientificActivityTypeDelete, name='BT28ScientificActivityTypeDelete'),

    url(r'^BT29IndustrialActivityType/(?P<pk>\d+)/delete/$', Delete_view.BT29IndustrialActivityTypeDelete, name='BT29IndustrialActivityTypeDelete'),

    url(r'^BT2TemporalPhenomenonType/(?P<pk>\d+)/delete/$', Delete_view.BT2TemporalPhenomenonTypeDelete, name='BT2TemporalPhenomenonTypeDelete'),

    url(r'^BT30IdentifierAssignmentType/(?P<pk>\d+)/delete/$', Delete_view.BT30IdentifierAssignmentTypeDelete, name='BT30IdentifierAssignmentTypeDelete'),

    url(r'^BT31BiologicalPartTime/(?P<pk>\d+)/delete/$', Delete_view.BT31BiologicalPartTimeDelete, name='BT31BiologicalPartTimeDelete'),

    url(r'^BT32PersistenType/(?P<pk>\d+)/delete/$', Delete_view.BT32PersistenTypeDelete, name='BT32PersistenTypeDelete'),

    url(r'^BT32PersistentType/(?P<pk>\d+)/delete/$', Delete_view.BT32PersistentTypeDelete, name='BT32PersistentTypeDelete'),

    url(r'^BT33MarineAnimalType/(?P<pk>\d+)/delete/$', Delete_view.BT33MarineAnimalTypeDelete, name='BT33MarineAnimalTypeDelete'),

    url(r'^BT34Order/(?P<pk>\d+)/delete/$', Delete_view.BT34OrderDelete, name='BT34OrderDelete'),

    url(r'^BT35PropertyType/(?P<pk>\d+)/delete/$', Delete_view.BT35PropertyTypeDelete, name='BT35PropertyTypeDelete'),

    url(r'^BT36Language/(?P<pk>\d+)/delete/$', Delete_view.BT36LanguageDelete, name='BT36LanguageDelete'),

    url(r'^BT3PhysicalThingType/(?P<pk>\d+)/delete/$', Delete_view.BT3PhysicalThingTypeDelete, name='BT3PhysicalThingTypeDelete'),

    url(r'^BT4ConceptualObjectType/(?P<pk>\d+)/delete/$', Delete_view.BT4ConceptualObjectTypeDelete, name='BT4ConceptualObjectTypeDelete'),

    url(r'^BT5LegislativeZoneType/(?P<pk>\d+)/delete/$', Delete_view.BT5LegislativeZoneTypeDelete, name='BT5LegislativeZoneTypeDelete'),

    url(r'^BT6HumanEventType/(?P<pk>\d+)/delete/$', Delete_view.BT6HumanEventTypeDelete, name='BT6HumanEventTypeDelete'),

    url(r'^BT7EcosystemType/(?P<pk>\d+)/delete/$', Delete_view.BT7EcosystemTypeDelete, name='BT7EcosystemTypeDelete'),

    url(r'^BT8AbioticElementType/(?P<pk>\d+)/delete/$', Delete_view.BT8AbioticElementTypeDelete, name='BT8AbioticElementTypeDelete'),

    url(r'^BT9ActorType/(?P<pk>\d+)/delete/$', Delete_view.BT9ActorTypeDelete, name='BT9ActorTypeDelete'),

    url(r'^Person/(?P<pk>\d+)/delete/$', Delete_view.PersonDelete, name='PersonDelete'),

    ]