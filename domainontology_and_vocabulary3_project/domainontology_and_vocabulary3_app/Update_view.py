from owlready2 import *
from django.db import models
from django.template.loader import render_to_string
from .models import *
from .forms import *
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseNotFound
from django.template import loader
from django.core.urlresolvers import reverse
import datetime
from .forms import *
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.core.files.storage import FileSystemStorage
from django.utils.safestring import mark_safe
from django.conf import settings


def BC10EventUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC10Event, pk=pk)
    if request.method == 'POST':
        form = BC10EventForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC10Event(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC10Event/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC10EventForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC10EventUpdate.html",context)

def BC11PersonUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC11Person, pk=pk)
    if request.method == 'POST':
        form = BC11PersonForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC11Person(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC11Person/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC11PersonForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC11PersonUpdate.html",context)

def BC12EcosystemUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC12Ecosystem, pk=pk)
    if request.method == 'POST':
        form = BC12EcosystemForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC12Ecosystem(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC12Ecosystem/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC12EcosystemForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC12EcosystemUpdate.html",context)

def BC13OrganizationUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC13Organization, pk=pk)
    if request.method == 'POST':
        form = BC13OrganizationForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC13Organization(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC13Organization/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC13OrganizationForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC13OrganizationUpdate.html",context)

def BC14EcosystemEnvironmentUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC14EcosystemEnvironment, pk=pk)
    if request.method == 'POST':
        form = BC14EcosystemEnvironmentForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC14EcosystemEnvironment(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC14EcosystemEnvironment/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC14EcosystemEnvironmentForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC14EcosystemEnvironmentUpdate.html",context)

def BC15WaterAreaUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC15WaterArea, pk=pk)
    if request.method == 'POST':
        form = BC15WaterAreaForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC15WaterArea(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC15WaterArea/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC15WaterAreaForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC15WaterAreaUpdate.html",context)

def BC16ManMadethingUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC16ManMadething, pk=pk)
    if request.method == 'POST':
        form = BC16ManMadethingForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC16ManMadething(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC16ManMadething/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC16ManMadethingForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC16ManMadethingUpdate.html",context)

def BC17ConceptualObjectUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC17ConceptualObject, pk=pk)
    if request.method == 'POST':
        form = BC17ConceptualObjectForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC17ConceptualObject(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC17ConceptualObject/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC17ConceptualObjectForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC17ConceptualObjectUpdate.html",context)

def BC18PropositionUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC18Proposition, pk=pk)
    if request.method == 'POST':
        form = BC18PropositionForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC18Proposition(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC18Proposition/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC18PropositionForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC18PropositionUpdate.html",context)

def BC19TitleUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC19Title, pk=pk)
    if request.method == 'POST':
        form = BC19TitleForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC19Title(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC19Title/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC19TitleForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC19TitleUpdate.html",context)

def BC1TLOEntityUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC1TLOEntity, pk=pk)
    if request.method == 'POST':
        form = BC1TLOEntityForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC1TLOEntity(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC1TLOEntity/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC1TLOEntityForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC1TLOEntityUpdate.html",context)

def BC20DeclarativePlaceUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC20DeclarativePlace, pk=pk)
    if request.method == 'POST':
        form = BC20DeclarativePlaceForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC20DeclarativePlace(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC20DeclarativePlace/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC20DeclarativePlaceForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC20DeclarativePlaceUpdate.html",context)

def BC21DataSetUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC21DataSet, pk=pk)
    if request.method == 'POST':
        form = BC21DataSetForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC21DataSet(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC21DataSet/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC21DataSetForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC21DataSetUpdate.html",context)

def BC22EncounterEventUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC22EncounterEvent, pk=pk)
    if request.method == 'POST':
        form = BC22EncounterEventForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC22EncounterEvent(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC22EncounterEvent/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC22EncounterEventForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC22EncounterEventUpdate.html",context)

def BC23DigitalObjectUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC23DigitalObject, pk=pk)
    if request.method == 'POST':
        form = BC23DigitalObjectForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC23DigitalObject(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC23DigitalObject/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC23DigitalObjectForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC23DigitalObjectUpdate.html",context)

def BC24RepositoryObjectUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC24RepositoryObject, pk=pk)
    if request.method == 'POST':
        form = BC24RepositoryObjectForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC24RepositoryObject(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC24RepositoryObject/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC24RepositoryObjectForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC24RepositoryObjectUpdate.html",context)

def BC25LinguisticObjectUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC25LinguisticObject, pk=pk)
    if request.method == 'POST':
        form = BC25LinguisticObjectForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC25LinguisticObject(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC25LinguisticObject/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC25LinguisticObjectForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC25LinguisticObjectUpdate.html",context)

def BC26PlaceNameUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC26PlaceName, pk=pk)
    if request.method == 'POST':
        form = BC26PlaceNameForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC26PlaceName(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC26PlaceName/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC26PlaceNameForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC26PlaceNameUpdate.html",context)

def BC27PublicationUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC27Publication, pk=pk)
    if request.method == 'POST':
        form = BC27PublicationForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC27Publication(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC27Publication/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC27PublicationForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC27PublicationUpdate.html",context)

def BC29SpatialCoordinateReferenceSystemUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC29SpatialCoordinateReferenceSystem, pk=pk)
    if request.method == 'POST':
        form = BC29SpatialCoordinateReferenceSystemForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC29SpatialCoordinateReferenceSystem(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC29SpatialCoordinateReferenceSystem/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC29SpatialCoordinateReferenceSystemForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC29SpatialCoordinateReferenceSystemUpdate.html",context)

def BC2TimeSpanUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC2TimeSpan, pk=pk)
    if request.method == 'POST':
        form = BC2TimeSpanForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC2TimeSpan(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC2TimeSpan/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC2TimeSpanForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC2TimeSpanUpdate.html",context)

def BC30AppellationUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC30Appellation, pk=pk)
    if request.method == 'POST':
        form = BC30AppellationForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC30Appellation(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC30Appellation/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC30AppellationForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC30AppellationUpdate.html",context)

def BC31PlaceAppellationUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC31PlaceAppellation, pk=pk)
    if request.method == 'POST':
        form = BC31PlaceAppellationForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC31PlaceAppellation(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC31PlaceAppellation/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC31PlaceAppellationForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC31PlaceAppellationUpdate.html",context)

def BC32IdentifierUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC32Identifier, pk=pk)
    if request.method == 'POST':
        form = BC32IdentifierForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC32Identifier(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC32Identifier/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC32IdentifierForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC32IdentifierUpdate.html",context)

def BC33SpatialCoordinateUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC33SpatialCoordinate, pk=pk)
    if request.method == 'POST':
        form = BC33SpatialCoordinateForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC33SpatialCoordinate(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC33SpatialCoordinate/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC33SpatialCoordinateForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC33SpatialCoordinateUpdate.html",context)

def BC34GeometricPlaceExpressionUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC34GeometricPlaceExpression, pk=pk)
    if request.method == 'POST':
        form = BC34GeometricPlaceExpressionForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC34GeometricPlaceExpression(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC34GeometricPlaceExpression/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC34GeometricPlaceExpressionForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC34GeometricPlaceExpressionUpdate.html",context)

def BC35PhysicalThingUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC35PhysicalThing, pk=pk)
    if request.method == 'POST':
        form = BC35PhysicalThingForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC35PhysicalThing(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC35PhysicalThing/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC35PhysicalThingForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC35PhysicalThingUpdate.html",context)

def BC36AbioticElementUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC36AbioticElement, pk=pk)
    if request.method == 'POST':
        form = BC36AbioticElementForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC36AbioticElement(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC36AbioticElement/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC36AbioticElementForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC36AbioticElementUpdate.html",context)

def BC37BiologicalObjectUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC37BiologicalObject, pk=pk)
    if request.method == 'POST':
        form = BC37BiologicalObjectForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC37BiologicalObject(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC37BiologicalObject/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC37BiologicalObjectForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC37BiologicalObjectUpdate.html",context)

def BC38BioticElementUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC38BioticElement, pk=pk)
    if request.method == 'POST':
        form = BC38BioticElementForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC38BioticElement(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC38BioticElement/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC38BioticElementForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC38BioticElementUpdate.html",context)

def BC39MarineAnimalUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC39MarineAnimal, pk=pk)
    if request.method == 'POST':
        form = BC39MarineAnimalForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC39MarineAnimal(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC39MarineAnimal/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC39MarineAnimalForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC39MarineAnimalUpdate.html",context)

def BC3PlaceUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC3Place, pk=pk)
    if request.method == 'POST':
        form = BC3PlaceForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC3Place(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC3Place/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC3PlaceForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC3PlaceUpdate.html",context)

def BC40PhysicalManMadeThingUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC40PhysicalManMadeThing, pk=pk)
    if request.method == 'POST':
        form = BC40PhysicalManMadeThingForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC40PhysicalManMadeThing(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC40PhysicalManMadeThing/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC40PhysicalManMadeThingForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC40PhysicalManMadeThingUpdate.html",context)

def BC41ManMadeObjectUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC41ManMadeObject, pk=pk)
    if request.method == 'POST':
        form = BC41ManMadeObjectForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC41ManMadeObject(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC41ManMadeObject/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC41ManMadeObjectForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC41ManMadeObjectUpdate.html",context)

def BC42CollectionUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC42Collection, pk=pk)
    if request.method == 'POST':
        form = BC42CollectionForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC42Collection(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC42Collection/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC42CollectionForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC42CollectionUpdate.html",context)

def BC43ActivityUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC43Activity, pk=pk)
    if request.method == 'POST':
        form = BC43ActivityForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC43Activity(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC43Activity/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC43ActivityForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC43ActivityUpdate.html",context)

def BC44AttributeAssignmentUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC44AttributeAssignment, pk=pk)
    if request.method == 'POST':
        form = BC44AttributeAssignmentForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC44AttributeAssignment(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC44AttributeAssignment/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC44AttributeAssignmentForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC44AttributeAssignmentUpdate.html",context)

def BC45ObservationUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC45Observation, pk=pk)
    if request.method == 'POST':
        form = BC45ObservationForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC45Observation(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC45Observation/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC45ObservationForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC45ObservationUpdate.html",context)

def BC46IdentifierAssignmentUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC46IdentifierAssignment, pk=pk)
    if request.method == 'POST':
        form = BC46IdentifierAssignmentForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC46IdentifierAssignment(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC46IdentifierAssignment/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC46IdentifierAssignmentForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC46IdentifierAssignmentUpdate.html",context)

def BC47ImageUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC47Image, pk=pk)
    if request.method == 'POST':
        form = BC47ImageForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC47Image(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC47Image/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC47ImageForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC47ImageUpdate.html",context)

def BC48DatabaseUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC48Database, pk=pk)
    if request.method == 'POST':
        form = BC48DatabaseForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC48Database(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC48Database/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC48DatabaseForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC48DatabaseUpdate.html",context)

def BC4TemporalPhenomenonUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC4TemporalPhenomenon, pk=pk)
    if request.method == 'POST':
        form = BC4TemporalPhenomenonForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC4TemporalPhenomenon(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC4TemporalPhenomenon/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC4TemporalPhenomenonForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC4TemporalPhenomenonUpdate.html",context)

def BC51PhysicalObjectUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC51PhysicalObject, pk=pk)
    if request.method == 'POST':
        form = BC51PhysicalObjectForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC51PhysicalObject(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC51PhysicalObject/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC51PhysicalObjectForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC51PhysicalObjectUpdate.html",context)

def BC53SpecimenUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC53Specimen, pk=pk)
    if request.method == 'POST':
        form = BC53SpecimenForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC53Specimen(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC53Specimen/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC53SpecimenForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC53SpecimenUpdate.html",context)

def BC54MeasurementUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC54Measurement, pk=pk)
    if request.method == 'POST':
        form = BC54MeasurementForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC54Measurement(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC54Measurement/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC54MeasurementForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC54MeasurementUpdate.html",context)

def BC55MeasurementUnitUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC55MeasurementUnit, pk=pk)
    if request.method == 'POST':
        form = BC55MeasurementUnitForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC55MeasurementUnit(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC55MeasurementUnit/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC55MeasurementUnitForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC55MeasurementUnitUpdate.html",context)

def BC56DigitalMeasurementEventUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC56DigitalMeasurementEvent, pk=pk)
    if request.method == 'POST':
        form = BC56DigitalMeasurementEventForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC56DigitalMeasurementEvent(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC56DigitalMeasurementEvent/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC56DigitalMeasurementEventForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC56DigitalMeasurementEventUpdate.html",context)

def BC57CaptureUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC57Capture, pk=pk)
    if request.method == 'POST':
        form = BC57CaptureForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC57Capture(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC57Capture/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC57CaptureForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC57CaptureUpdate.html",context)

def BC58DigitalDeviceUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC58DigitalDevice, pk=pk)
    if request.method == 'POST':
        form = BC58DigitalDeviceForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC58DigitalDevice(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC58DigitalDevice/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC58DigitalDeviceForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC58DigitalDeviceUpdate.html",context)

def BC59SoftwareUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC59Software, pk=pk)
    if request.method == 'POST':
        form = BC59SoftwareForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC59Software(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC59Software/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC59SoftwareForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC59SoftwareUpdate.html",context)

def BC5DimensionUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC5Dimension, pk=pk)
    if request.method == 'POST':
        form = BC5DimensionForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC5Dimension(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC5Dimension/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC5DimensionForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC5DimensionUpdate.html",context)

def BC60SoftwareExecutionUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC60SoftwareExecution, pk=pk)
    if request.method == 'POST':
        form = BC60SoftwareExecutionForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC60SoftwareExecution(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC60SoftwareExecution/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC60SoftwareExecutionForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC60SoftwareExecutionUpdate.html",context)

def BC61CaptureActivityUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC61CaptureActivity, pk=pk)
    if request.method == 'POST':
        form = BC61CaptureActivityForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC61CaptureActivity(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC61CaptureActivity/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC61CaptureActivityForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC61CaptureActivityUpdate.html",context)

def BC62StatisticIndicatorUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC62StatisticIndicator, pk=pk)
    if request.method == 'POST':
        form = BC62StatisticIndicatorForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC62StatisticIndicator(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC62StatisticIndicator/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC62StatisticIndicatorForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC62StatisticIndicatorUpdate.html",context)

def BC63GlobalStatisticLandingUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC63GlobalStatisticLanding, pk=pk)
    if request.method == 'POST':
        form = BC63GlobalStatisticLandingForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC63GlobalStatisticLanding(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC63GlobalStatisticLanding/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC63GlobalStatisticLandingForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC63GlobalStatisticLandingUpdate.html",context)

def BC64DesignorProcedureUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC64DesignorProcedure, pk=pk)
    if request.method == 'POST':
        form = BC64DesignorProcedureForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC64DesignorProcedure(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC64DesignorProcedure/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC64DesignorProcedureForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC64DesignorProcedureUpdate.html",context)

def BC6PersistentItemUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC6PersistentItem, pk=pk)
    if request.method == 'POST':
        form = BC6PersistentItemForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC6PersistentItem(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC6PersistentItem/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC6PersistentItemForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC6PersistentItemUpdate.html",context)

def BC70GroupUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC70Group, pk=pk)
    if request.method == 'POST':
        form = BC70GroupForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC70Group(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC70Group/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC70GroupForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC70GroupUpdate.html",context)

def BC71NameUseActivityUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC71NameUseActivity, pk=pk)
    if request.method == 'POST':
        form = BC71NameUseActivityForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC71NameUseActivity(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC71NameUseActivity/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC71NameUseActivityForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC71NameUseActivityUpdate.html",context)

def BC72SpaceTimeVolumeUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC72SpaceTimeVolume, pk=pk)
    if request.method == 'POST':
        form = BC72SpaceTimeVolumeForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC72SpaceTimeVolume(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC72SpaceTimeVolume/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC72SpaceTimeVolumeForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC72SpaceTimeVolumeUpdate.html",context)

def BC75StockUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC75Stock, pk=pk)
    if request.method == 'POST':
        form = BC75StockForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC75Stock(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC75Stock/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC75StockForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC75StockUpdate.html",context)

def BC76StockFormationUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC76StockFormation, pk=pk)
    if request.method == 'POST':
        form = BC76StockFormationForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC76StockFormation(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC76StockFormation/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC76StockFormationForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC76StockFormationUpdate.html",context)

def BC77MatterRemovalUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC77MatterRemoval, pk=pk)
    if request.method == 'POST':
        form = BC77MatterRemovalForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC77MatterRemoval(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC77MatterRemoval/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC77MatterRemovalForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC77MatterRemovalUpdate.html",context)

def BC78AmountofMatterUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC78AmountofMatter, pk=pk)
    if request.method == 'POST':
        form = BC78AmountofMatterForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC78AmountofMatter(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC78AmountofMatter/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC78AmountofMatterForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC78AmountofMatterUpdate.html",context)

def BC79RightUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC79Right, pk=pk)
    if request.method == 'POST':
        form = BC79RightForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC79Right(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC79Right/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC79RightForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC79RightUpdate.html",context)

def BC7ThingUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC7Thing, pk=pk)
    if request.method == 'POST':
        form = BC7ThingForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC7Thing(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC7Thing/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC7ThingForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC7ThingUpdate.html",context)

def BC8ActorUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC8Actor, pk=pk)
    if request.method == 'POST':
        form = BC8ActorForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC8Actor(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC8Actor/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC8ActorForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC8ActorUpdate.html",context)

def BC9ObservableEntityUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC9ObservableEntity, pk=pk)
    if request.method == 'POST':
        form = BC9ObservableEntityForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BC9ObservableEntity(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BC9ObservableEntity/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC9ObservableEntityForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC9ObservableEntityUpdate.html",context)

def BT10BioticElementTypeUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT10BioticElementType, pk=pk)
    if request.method == 'POST':
        form = BT10BioticElementTypeForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BT10BioticElementType(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BT10BioticElementType/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT10BioticElementTypeForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT10BioticElementTypeUpdate.html",context)

def BT11EquipmentTypeUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT11EquipmentType, pk=pk)
    if request.method == 'POST':
        form = BT11EquipmentTypeForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BT11EquipmentType(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BT11EquipmentType/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT11EquipmentTypeForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT11EquipmentTypeUpdate.html",context)

def BT12ScientificDataTypeUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT12ScientificDataType, pk=pk)
    if request.method == 'POST':
        form = BT12ScientificDataTypeForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BT12ScientificDataType(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BT12ScientificDataType/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT12ScientificDataTypeForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT12ScientificDataTypeUpdate.html",context)

def BT13DigitalObjectTypeUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT13DigitalObjectType, pk=pk)
    if request.method == 'POST':
        form = BT13DigitalObjectTypeForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BT13DigitalObjectType(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BT13DigitalObjectType/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT13DigitalObjectTypeForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT13DigitalObjectTypeUpdate.html",context)

def BT14AppellationTypeUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT14AppellationType, pk=pk)
    if request.method == 'POST':
        form = BT14AppellationTypeForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BT14AppellationType(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BT14AppellationType/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT14AppellationTypeForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT14AppellationTypeUpdate.html",context)

def BT16VisualTypeUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT16VisualType, pk=pk)
    if request.method == 'POST':
        form = BT16VisualTypeForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BT16VisualType(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BT16VisualType/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT16VisualTypeForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT16VisualTypeUpdate.html",context)

def BT17HumanActivityTypeUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT17HumanActivityType, pk=pk)
    if request.method == 'POST':
        form = BT17HumanActivityTypeForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BT17HumanActivityType(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BT17HumanActivityType/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT17HumanActivityTypeForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT17HumanActivityTypeUpdate.html",context)

def BT18KingdomUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT18Kingdom, pk=pk)
    if request.method == 'POST':
        form = BT18KingdomForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BT18Kingdom(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BT18Kingdom/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT18KingdomForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT18KingdomUpdate.html",context)

def BT19PhylumUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT19Phylum, pk=pk)
    if request.method == 'POST':
        form = BT19PhylumForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BT19Phylum(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BT19Phylum/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT19PhylumForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT19PhylumUpdate.html",context)

def BT1TLOEntityTypeUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT1TLOEntityType, pk=pk)
    if request.method == 'POST':
        form = BT1TLOEntityTypeForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BT1TLOEntityType(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BT1TLOEntityType/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT1TLOEntityTypeForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT1TLOEntityTypeUpdate.html",context)

def BT20SubPhylumUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT20SubPhylum, pk=pk)
    if request.method == 'POST':
        form = BT20SubPhylumForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BT20SubPhylum(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BT20SubPhylum/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT20SubPhylumForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT20SubPhylumUpdate.html",context)

def BT21SuperClassUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT21SuperClass, pk=pk)
    if request.method == 'POST':
        form = BT21SuperClassForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BT21SuperClass(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BT21SuperClass/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT21SuperClassForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT21SuperClassUpdate.html",context)

def BT22ClassUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT22Class, pk=pk)
    if request.method == 'POST':
        form = BT22ClassForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BT22Class(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BT22Class/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT22ClassForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT22ClassUpdate.html",context)

def BT23SubClassUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT23SubClass, pk=pk)
    if request.method == 'POST':
        form = BT23SubClassForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BT23SubClass(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BT23SubClass/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT23SubClassForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT23SubClassUpdate.html",context)

def BT24FamilyUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT24Family, pk=pk)
    if request.method == 'POST':
        form = BT24FamilyForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BT24Family(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BT24Family/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT24FamilyForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT24FamilyUpdate.html",context)

def BT25SubFamilyUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT25SubFamily, pk=pk)
    if request.method == 'POST':
        form = BT25SubFamilyForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BT25SubFamily(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BT25SubFamily/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT25SubFamilyForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT25SubFamilyUpdate.html",context)

def BT26GenusUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT26Genus, pk=pk)
    if request.method == 'POST':
        form = BT26GenusForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BT26Genus(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BT26Genus/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT26GenusForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT26GenusUpdate.html",context)

def BT27SpeciesUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT27Species, pk=pk)
    if request.method == 'POST':
        form = BT27SpeciesForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BT27Species(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BT27Species/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT27SpeciesForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT27SpeciesUpdate.html",context)

def BT28ScientificActivityTypeUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT28ScientificActivityType, pk=pk)
    if request.method == 'POST':
        form = BT28ScientificActivityTypeForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BT28ScientificActivityType(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BT28ScientificActivityType/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT28ScientificActivityTypeForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT28ScientificActivityTypeUpdate.html",context)

def BT29IndustrialActivityTypeUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT29IndustrialActivityType, pk=pk)
    if request.method == 'POST':
        form = BT29IndustrialActivityTypeForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BT29IndustrialActivityType(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BT29IndustrialActivityType/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT29IndustrialActivityTypeForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT29IndustrialActivityTypeUpdate.html",context)

def BT2TemporalPhenomenonTypeUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT2TemporalPhenomenonType, pk=pk)
    if request.method == 'POST':
        form = BT2TemporalPhenomenonTypeForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BT2TemporalPhenomenonType(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BT2TemporalPhenomenonType/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT2TemporalPhenomenonTypeForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT2TemporalPhenomenonTypeUpdate.html",context)

def BT30IdentifierAssignmentTypeUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT30IdentifierAssignmentType, pk=pk)
    if request.method == 'POST':
        form = BT30IdentifierAssignmentTypeForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BT30IdentifierAssignmentType(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BT30IdentifierAssignmentType/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT30IdentifierAssignmentTypeForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT30IdentifierAssignmentTypeUpdate.html",context)

def BT31BiologicalPartTimeUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT31BiologicalPartTime, pk=pk)
    if request.method == 'POST':
        form = BT31BiologicalPartTimeForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BT31BiologicalPartTime(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BT31BiologicalPartTime/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT31BiologicalPartTimeForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT31BiologicalPartTimeUpdate.html",context)

def BT32PersistenTypeUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT32PersistenType, pk=pk)
    if request.method == 'POST':
        form = BT32PersistenTypeForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BT32PersistenType(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BT32PersistenType/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT32PersistenTypeForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT32PersistenTypeUpdate.html",context)

def BT32PersistentTypeUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT32PersistentType, pk=pk)
    if request.method == 'POST':
        form = BT32PersistentTypeForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BT32PersistentType(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BT32PersistentType/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT32PersistentTypeForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT32PersistentTypeUpdate.html",context)

def BT33MarineAnimalTypeUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT33MarineAnimalType, pk=pk)
    if request.method == 'POST':
        form = BT33MarineAnimalTypeForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BT33MarineAnimalType(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BT33MarineAnimalType/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT33MarineAnimalTypeForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT33MarineAnimalTypeUpdate.html",context)

def BT34OrderUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT34Order, pk=pk)
    if request.method == 'POST':
        form = BT34OrderForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BT34Order(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BT34Order/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT34OrderForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT34OrderUpdate.html",context)

def BT35PropertyTypeUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT35PropertyType, pk=pk)
    if request.method == 'POST':
        form = BT35PropertyTypeForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BT35PropertyType(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BT35PropertyType/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT35PropertyTypeForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT35PropertyTypeUpdate.html",context)

def BT36LanguageUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT36Language, pk=pk)
    if request.method == 'POST':
        form = BT36LanguageForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BT36Language(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BT36Language/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT36LanguageForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT36LanguageUpdate.html",context)

def BT3PhysicalThingTypeUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT3PhysicalThingType, pk=pk)
    if request.method == 'POST':
        form = BT3PhysicalThingTypeForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BT3PhysicalThingType(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BT3PhysicalThingType/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT3PhysicalThingTypeForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT3PhysicalThingTypeUpdate.html",context)

def BT4ConceptualObjectTypeUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT4ConceptualObjectType, pk=pk)
    if request.method == 'POST':
        form = BT4ConceptualObjectTypeForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BT4ConceptualObjectType(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BT4ConceptualObjectType/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT4ConceptualObjectTypeForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT4ConceptualObjectTypeUpdate.html",context)

def BT5LegislativeZoneTypeUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT5LegislativeZoneType, pk=pk)
    if request.method == 'POST':
        form = BT5LegislativeZoneTypeForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BT5LegislativeZoneType(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BT5LegislativeZoneType/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT5LegislativeZoneTypeForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT5LegislativeZoneTypeUpdate.html",context)

def BT6HumanEventTypeUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT6HumanEventType, pk=pk)
    if request.method == 'POST':
        form = BT6HumanEventTypeForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BT6HumanEventType(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BT6HumanEventType/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT6HumanEventTypeForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT6HumanEventTypeUpdate.html",context)

def BT7EcosystemTypeUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT7EcosystemType, pk=pk)
    if request.method == 'POST':
        form = BT7EcosystemTypeForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BT7EcosystemType(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BT7EcosystemType/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT7EcosystemTypeForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT7EcosystemTypeUpdate.html",context)

def BT8AbioticElementTypeUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT8AbioticElementType, pk=pk)
    if request.method == 'POST':
        form = BT8AbioticElementTypeForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BT8AbioticElementType(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BT8AbioticElementType/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT8AbioticElementTypeForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT8AbioticElementTypeUpdate.html",context)

def BT9ActorTypeUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT9ActorType, pk=pk)
    if request.method == 'POST':
        form = BT9ActorTypeForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.BT9ActorType(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/BT9ActorType/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT9ActorTypeForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT9ActorTypeUpdate.html",context)

def PersonUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Person, pk=pk)
    if request.method == 'POST':
        form = PersonForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto.Person(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/domainontology_and_vocabulary3_app/Person/list/"
            return HttpResponseRedirect(link)
    else:
        form = PersonForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "PersonUpdate.html",context)
