from django.db import models
from django.template.loader import render_to_string
from .models import *
from .forms import *
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseNotFound
from django.template import loader
from django.core.urlresolvers import reverse
import datetime
from .forms import *
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.core.files.storage import FileSystemStorage
from django.utils.safestring import mark_safe


def BC10EventDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC10Event,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC10Event/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC10EventForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC10EventDelete.html",context)

def BC11PersonDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC11Person,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC11Person/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC11PersonForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC11PersonDelete.html",context)

def BC12EcosystemDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC12Ecosystem,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC12Ecosystem/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC12EcosystemForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC12EcosystemDelete.html",context)

def BC13OrganizationDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC13Organization,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC13Organization/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC13OrganizationForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC13OrganizationDelete.html",context)

def BC14EcosystemEnvironmentDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC14EcosystemEnvironment,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC14EcosystemEnvironment/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC14EcosystemEnvironmentForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC14EcosystemEnvironmentDelete.html",context)

def BC15WaterAreaDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC15WaterArea,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC15WaterArea/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC15WaterAreaForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC15WaterAreaDelete.html",context)

def BC16ManMadethingDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC16ManMadething,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC16ManMadething/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC16ManMadethingForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC16ManMadethingDelete.html",context)

def BC17ConceptualObjectDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC17ConceptualObject,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC17ConceptualObject/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC17ConceptualObjectForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC17ConceptualObjectDelete.html",context)

def BC18PropositionDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC18Proposition,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC18Proposition/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC18PropositionForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC18PropositionDelete.html",context)

def BC19TitleDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC19Title,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC19Title/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC19TitleForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC19TitleDelete.html",context)

def BC1TLOEntityDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC1TLOEntity,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC1TLOEntity/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC1TLOEntityForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC1TLOEntityDelete.html",context)

def BC20DeclarativePlaceDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC20DeclarativePlace,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC20DeclarativePlace/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC20DeclarativePlaceForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC20DeclarativePlaceDelete.html",context)

def BC21DataSetDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC21DataSet,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC21DataSet/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC21DataSetForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC21DataSetDelete.html",context)

def BC22EncounterEventDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC22EncounterEvent,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC22EncounterEvent/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC22EncounterEventForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC22EncounterEventDelete.html",context)

def BC23DigitalObjectDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC23DigitalObject,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC23DigitalObject/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC23DigitalObjectForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC23DigitalObjectDelete.html",context)

def BC24RepositoryObjectDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC24RepositoryObject,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC24RepositoryObject/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC24RepositoryObjectForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC24RepositoryObjectDelete.html",context)

def BC25LinguisticObjectDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC25LinguisticObject,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC25LinguisticObject/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC25LinguisticObjectForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC25LinguisticObjectDelete.html",context)

def BC26PlaceNameDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC26PlaceName,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC26PlaceName/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC26PlaceNameForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC26PlaceNameDelete.html",context)

def BC27PublicationDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC27Publication,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC27Publication/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC27PublicationForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC27PublicationDelete.html",context)

def BC29SpatialCoordinateReferenceSystemDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC29SpatialCoordinateReferenceSystem,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC29SpatialCoordinateReferenceSystem/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC29SpatialCoordinateReferenceSystemForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC29SpatialCoordinateReferenceSystemDelete.html",context)

def BC2TimeSpanDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC2TimeSpan,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC2TimeSpan/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC2TimeSpanForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC2TimeSpanDelete.html",context)

def BC30AppellationDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC30Appellation,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC30Appellation/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC30AppellationForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC30AppellationDelete.html",context)

def BC31PlaceAppellationDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC31PlaceAppellation,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC31PlaceAppellation/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC31PlaceAppellationForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC31PlaceAppellationDelete.html",context)

def BC32IdentifierDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC32Identifier,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC32Identifier/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC32IdentifierForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC32IdentifierDelete.html",context)

def BC33SpatialCoordinateDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC33SpatialCoordinate,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC33SpatialCoordinate/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC33SpatialCoordinateForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC33SpatialCoordinateDelete.html",context)

def BC34GeometricPlaceExpressionDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC34GeometricPlaceExpression,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC34GeometricPlaceExpression/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC34GeometricPlaceExpressionForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC34GeometricPlaceExpressionDelete.html",context)

def BC35PhysicalThingDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC35PhysicalThing,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC35PhysicalThing/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC35PhysicalThingForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC35PhysicalThingDelete.html",context)

def BC36AbioticElementDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC36AbioticElement,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC36AbioticElement/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC36AbioticElementForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC36AbioticElementDelete.html",context)

def BC37BiologicalObjectDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC37BiologicalObject,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC37BiologicalObject/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC37BiologicalObjectForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC37BiologicalObjectDelete.html",context)

def BC38BioticElementDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC38BioticElement,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC38BioticElement/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC38BioticElementForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC38BioticElementDelete.html",context)

def BC39MarineAnimalDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC39MarineAnimal,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC39MarineAnimal/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC39MarineAnimalForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC39MarineAnimalDelete.html",context)

def BC3PlaceDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC3Place,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC3Place/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC3PlaceForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC3PlaceDelete.html",context)

def BC40PhysicalManMadeThingDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC40PhysicalManMadeThing,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC40PhysicalManMadeThing/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC40PhysicalManMadeThingForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC40PhysicalManMadeThingDelete.html",context)

def BC41ManMadeObjectDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC41ManMadeObject,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC41ManMadeObject/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC41ManMadeObjectForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC41ManMadeObjectDelete.html",context)

def BC42CollectionDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC42Collection,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC42Collection/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC42CollectionForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC42CollectionDelete.html",context)

def BC43ActivityDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC43Activity,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC43Activity/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC43ActivityForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC43ActivityDelete.html",context)

def BC44AttributeAssignmentDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC44AttributeAssignment,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC44AttributeAssignment/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC44AttributeAssignmentForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC44AttributeAssignmentDelete.html",context)

def BC45ObservationDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC45Observation,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC45Observation/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC45ObservationForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC45ObservationDelete.html",context)

def BC46IdentifierAssignmentDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC46IdentifierAssignment,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC46IdentifierAssignment/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC46IdentifierAssignmentForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC46IdentifierAssignmentDelete.html",context)

def BC47ImageDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC47Image,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC47Image/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC47ImageForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC47ImageDelete.html",context)

def BC48DatabaseDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC48Database,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC48Database/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC48DatabaseForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC48DatabaseDelete.html",context)

def BC4TemporalPhenomenonDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC4TemporalPhenomenon,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC4TemporalPhenomenon/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC4TemporalPhenomenonForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC4TemporalPhenomenonDelete.html",context)

def BC51PhysicalObjectDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC51PhysicalObject,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC51PhysicalObject/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC51PhysicalObjectForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC51PhysicalObjectDelete.html",context)

def BC53SpecimenDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC53Specimen,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC53Specimen/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC53SpecimenForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC53SpecimenDelete.html",context)

def BC54MeasurementDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC54Measurement,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC54Measurement/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC54MeasurementForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC54MeasurementDelete.html",context)

def BC55MeasurementUnitDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC55MeasurementUnit,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC55MeasurementUnit/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC55MeasurementUnitForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC55MeasurementUnitDelete.html",context)

def BC56DigitalMeasurementEventDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC56DigitalMeasurementEvent,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC56DigitalMeasurementEvent/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC56DigitalMeasurementEventForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC56DigitalMeasurementEventDelete.html",context)

def BC57CaptureDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC57Capture,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC57Capture/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC57CaptureForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC57CaptureDelete.html",context)

def BC58DigitalDeviceDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC58DigitalDevice,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC58DigitalDevice/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC58DigitalDeviceForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC58DigitalDeviceDelete.html",context)

def BC59SoftwareDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC59Software,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC59Software/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC59SoftwareForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC59SoftwareDelete.html",context)

def BC5DimensionDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC5Dimension,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC5Dimension/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC5DimensionForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC5DimensionDelete.html",context)

def BC60SoftwareExecutionDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC60SoftwareExecution,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC60SoftwareExecution/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC60SoftwareExecutionForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC60SoftwareExecutionDelete.html",context)

def BC61CaptureActivityDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC61CaptureActivity,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC61CaptureActivity/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC61CaptureActivityForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC61CaptureActivityDelete.html",context)

def BC62StatisticIndicatorDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC62StatisticIndicator,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC62StatisticIndicator/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC62StatisticIndicatorForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC62StatisticIndicatorDelete.html",context)

def BC63GlobalStatisticLandingDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC63GlobalStatisticLanding,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC63GlobalStatisticLanding/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC63GlobalStatisticLandingForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC63GlobalStatisticLandingDelete.html",context)

def BC64DesignorProcedureDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC64DesignorProcedure,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC64DesignorProcedure/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC64DesignorProcedureForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC64DesignorProcedureDelete.html",context)

def BC6PersistentItemDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC6PersistentItem,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC6PersistentItem/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC6PersistentItemForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC6PersistentItemDelete.html",context)

def BC70GroupDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC70Group,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC70Group/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC70GroupForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC70GroupDelete.html",context)

def BC71NameUseActivityDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC71NameUseActivity,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC71NameUseActivity/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC71NameUseActivityForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC71NameUseActivityDelete.html",context)

def BC72SpaceTimeVolumeDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC72SpaceTimeVolume,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC72SpaceTimeVolume/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC72SpaceTimeVolumeForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC72SpaceTimeVolumeDelete.html",context)

def BC75StockDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC75Stock,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC75Stock/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC75StockForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC75StockDelete.html",context)

def BC76StockFormationDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC76StockFormation,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC76StockFormation/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC76StockFormationForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC76StockFormationDelete.html",context)

def BC77MatterRemovalDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC77MatterRemoval,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC77MatterRemoval/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC77MatterRemovalForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC77MatterRemovalDelete.html",context)

def BC78AmountofMatterDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC78AmountofMatter,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC78AmountofMatter/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC78AmountofMatterForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC78AmountofMatterDelete.html",context)

def BC79RightDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC79Right,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC79Right/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC79RightForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC79RightDelete.html",context)

def BC7ThingDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC7Thing,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC7Thing/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC7ThingForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC7ThingDelete.html",context)

def BC8ActorDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC8Actor,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC8Actor/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC8ActorForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC8ActorDelete.html",context)

def BC9ObservableEntityDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC9ObservableEntity,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BC9ObservableEntity/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC9ObservableEntityForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC9ObservableEntityDelete.html",context)

def BT10BioticElementTypeDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT10BioticElementType,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BT10BioticElementType/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT10BioticElementTypeForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT10BioticElementTypeDelete.html",context)

def BT11EquipmentTypeDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT11EquipmentType,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BT11EquipmentType/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT11EquipmentTypeForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT11EquipmentTypeDelete.html",context)

def BT12ScientificDataTypeDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT12ScientificDataType,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BT12ScientificDataType/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT12ScientificDataTypeForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT12ScientificDataTypeDelete.html",context)

def BT13DigitalObjectTypeDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT13DigitalObjectType,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BT13DigitalObjectType/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT13DigitalObjectTypeForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT13DigitalObjectTypeDelete.html",context)

def BT14AppellationTypeDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT14AppellationType,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BT14AppellationType/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT14AppellationTypeForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT14AppellationTypeDelete.html",context)

def BT16VisualTypeDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT16VisualType,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BT16VisualType/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT16VisualTypeForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT16VisualTypeDelete.html",context)

def BT17HumanActivityTypeDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT17HumanActivityType,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BT17HumanActivityType/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT17HumanActivityTypeForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT17HumanActivityTypeDelete.html",context)

def BT18KingdomDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT18Kingdom,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BT18Kingdom/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT18KingdomForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT18KingdomDelete.html",context)

def BT19PhylumDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT19Phylum,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BT19Phylum/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT19PhylumForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT19PhylumDelete.html",context)

def BT1TLOEntityTypeDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT1TLOEntityType,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BT1TLOEntityType/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT1TLOEntityTypeForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT1TLOEntityTypeDelete.html",context)

def BT20SubPhylumDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT20SubPhylum,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BT20SubPhylum/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT20SubPhylumForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT20SubPhylumDelete.html",context)

def BT21SuperClassDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT21SuperClass,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BT21SuperClass/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT21SuperClassForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT21SuperClassDelete.html",context)

def BT22ClassDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT22Class,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BT22Class/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT22ClassForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT22ClassDelete.html",context)

def BT23SubClassDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT23SubClass,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BT23SubClass/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT23SubClassForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT23SubClassDelete.html",context)

def BT24FamilyDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT24Family,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BT24Family/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT24FamilyForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT24FamilyDelete.html",context)

def BT25SubFamilyDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT25SubFamily,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BT25SubFamily/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT25SubFamilyForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT25SubFamilyDelete.html",context)

def BT26GenusDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT26Genus,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BT26Genus/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT26GenusForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT26GenusDelete.html",context)

def BT27SpeciesDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT27Species,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BT27Species/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT27SpeciesForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT27SpeciesDelete.html",context)

def BT28ScientificActivityTypeDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT28ScientificActivityType,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BT28ScientificActivityType/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT28ScientificActivityTypeForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT28ScientificActivityTypeDelete.html",context)

def BT29IndustrialActivityTypeDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT29IndustrialActivityType,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BT29IndustrialActivityType/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT29IndustrialActivityTypeForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT29IndustrialActivityTypeDelete.html",context)

def BT2TemporalPhenomenonTypeDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT2TemporalPhenomenonType,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BT2TemporalPhenomenonType/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT2TemporalPhenomenonTypeForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT2TemporalPhenomenonTypeDelete.html",context)

def BT30IdentifierAssignmentTypeDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT30IdentifierAssignmentType,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BT30IdentifierAssignmentType/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT30IdentifierAssignmentTypeForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT30IdentifierAssignmentTypeDelete.html",context)

def BT31BiologicalPartTimeDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT31BiologicalPartTime,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BT31BiologicalPartTime/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT31BiologicalPartTimeForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT31BiologicalPartTimeDelete.html",context)

def BT32PersistenTypeDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT32PersistenType,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BT32PersistenType/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT32PersistenTypeForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT32PersistenTypeDelete.html",context)

def BT32PersistentTypeDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT32PersistentType,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BT32PersistentType/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT32PersistentTypeForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT32PersistentTypeDelete.html",context)

def BT33MarineAnimalTypeDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT33MarineAnimalType,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BT33MarineAnimalType/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT33MarineAnimalTypeForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT33MarineAnimalTypeDelete.html",context)

def BT34OrderDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT34Order,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BT34Order/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT34OrderForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT34OrderDelete.html",context)

def BT35PropertyTypeDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT35PropertyType,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BT35PropertyType/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT35PropertyTypeForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT35PropertyTypeDelete.html",context)

def BT36LanguageDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT36Language,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BT36Language/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT36LanguageForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT36LanguageDelete.html",context)

def BT3PhysicalThingTypeDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT3PhysicalThingType,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BT3PhysicalThingType/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT3PhysicalThingTypeForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT3PhysicalThingTypeDelete.html",context)

def BT4ConceptualObjectTypeDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT4ConceptualObjectType,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BT4ConceptualObjectType/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT4ConceptualObjectTypeForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT4ConceptualObjectTypeDelete.html",context)

def BT5LegislativeZoneTypeDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT5LegislativeZoneType,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BT5LegislativeZoneType/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT5LegislativeZoneTypeForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT5LegislativeZoneTypeDelete.html",context)

def BT6HumanEventTypeDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT6HumanEventType,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BT6HumanEventType/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT6HumanEventTypeForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT6HumanEventTypeDelete.html",context)

def BT7EcosystemTypeDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT7EcosystemType,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BT7EcosystemType/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT7EcosystemTypeForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT7EcosystemTypeDelete.html",context)

def BT8AbioticElementTypeDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT8AbioticElementType,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BT8AbioticElementType/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT8AbioticElementTypeForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT8AbioticElementTypeDelete.html",context)

def BT9ActorTypeDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT9ActorType,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/BT9ActorType/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT9ActorTypeForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT9ActorTypeDelete.html",context)

def PersonDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Person,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/domainontology_and_vocabulary3_app/Person/list/"
        return HttpResponseRedirect(link)
    else:
         form = PersonForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "PersonDelete.html",context)
