from django.db import models
from django.template.loader import render_to_string
from .models import *
from .forms import *
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseNotFound
from django.template import loader
from django.core.urlresolvers import reverse
import datetime
from .forms import *
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.core.files.storage import FileSystemStorage
from django.utils.safestring import mark_safe

def vowl(request):
    context = {}
    return render(request, "do_visualisation.html", context)

def HomeView(request):
    classes = []
    
    classes.append(str(BC10Event))
    
    classes.append(str(BC11Person))
    
    classes.append(str(BC12Ecosystem))
    
    classes.append(str(BC13Organization))
    
    classes.append(str(BC14EcosystemEnvironment))
    
    classes.append(str(BC15WaterArea))
    
    classes.append(str(BC16ManMadething))
    
    classes.append(str(BC17ConceptualObject))
    
    classes.append(str(BC18Proposition))
    
    classes.append(str(BC19Title))
    
    classes.append(str(BC1TLOEntity))
    
    classes.append(str(BC20DeclarativePlace))
    
    classes.append(str(BC21DataSet))
    
    classes.append(str(BC22EncounterEvent))
    
    classes.append(str(BC23DigitalObject))
    
    classes.append(str(BC24RepositoryObject))
    
    classes.append(str(BC25LinguisticObject))
    
    classes.append(str(BC26PlaceName))
    
    classes.append(str(BC27Publication))
    
    classes.append(str(BC29SpatialCoordinateReferenceSystem))
    
    classes.append(str(BC2TimeSpan))
    
    classes.append(str(BC30Appellation))
    
    classes.append(str(BC31PlaceAppellation))
    
    classes.append(str(BC32Identifier))
    
    classes.append(str(BC33SpatialCoordinate))
    
    classes.append(str(BC34GeometricPlaceExpression))
    
    classes.append(str(BC35PhysicalThing))
    
    classes.append(str(BC36AbioticElement))
    
    classes.append(str(BC37BiologicalObject))
    
    classes.append(str(BC38BioticElement))
    
    classes.append(str(BC39MarineAnimal))
    
    classes.append(str(BC3Place))
    
    classes.append(str(BC40PhysicalManMadeThing))
    
    classes.append(str(BC41ManMadeObject))
    
    classes.append(str(BC42Collection))
    
    classes.append(str(BC43Activity))
    
    classes.append(str(BC44AttributeAssignment))
    
    classes.append(str(BC45Observation))
    
    classes.append(str(BC46IdentifierAssignment))
    
    classes.append(str(BC47Image))
    
    classes.append(str(BC48Database))
    
    classes.append(str(BC4TemporalPhenomenon))
    
    classes.append(str(BC51PhysicalObject))
    
    classes.append(str(BC53Specimen))
    
    classes.append(str(BC54Measurement))
    
    classes.append(str(BC55MeasurementUnit))
    
    classes.append(str(BC56DigitalMeasurementEvent))
    
    classes.append(str(BC57Capture))
    
    classes.append(str(BC58DigitalDevice))
    
    classes.append(str(BC59Software))
    
    classes.append(str(BC5Dimension))
    
    classes.append(str(BC60SoftwareExecution))
    
    classes.append(str(BC61CaptureActivity))
    
    classes.append(str(BC62StatisticIndicator))
    
    classes.append(str(BC63GlobalStatisticLanding))
    
    classes.append(str(BC64DesignorProcedure))
    
    classes.append(str(BC6PersistentItem))
    
    classes.append(str(BC70Group))
    
    classes.append(str(BC71NameUseActivity))
    
    classes.append(str(BC72SpaceTimeVolume))
    
    classes.append(str(BC75Stock))
    
    classes.append(str(BC76StockFormation))
    
    classes.append(str(BC77MatterRemoval))
    
    classes.append(str(BC78AmountofMatter))
    
    classes.append(str(BC79Right))
    
    classes.append(str(BC7Thing))
    
    classes.append(str(BC8Actor))
    
    classes.append(str(BC9ObservableEntity))
    
    classes.append(str(BT10BioticElementType))
    
    classes.append(str(BT11EquipmentType))
    
    classes.append(str(BT12ScientificDataType))
    
    classes.append(str(BT13DigitalObjectType))
    
    classes.append(str(BT14AppellationType))
    
    classes.append(str(BT16VisualType))
    
    classes.append(str(BT17HumanActivityType))
    
    classes.append(str(BT18Kingdom))
    
    classes.append(str(BT19Phylum))
    
    classes.append(str(BT1TLOEntityType))
    
    classes.append(str(BT20SubPhylum))
    
    classes.append(str(BT21SuperClass))
    
    classes.append(str(BT22Class))
    
    classes.append(str(BT23SubClass))
    
    classes.append(str(BT24Family))
    
    classes.append(str(BT25SubFamily))
    
    classes.append(str(BT26Genus))
    
    classes.append(str(BT27Species))
    
    classes.append(str(BT28ScientificActivityType))
    
    classes.append(str(BT29IndustrialActivityType))
    
    classes.append(str(BT2TemporalPhenomenonType))
    
    classes.append(str(BT30IdentifierAssignmentType))
    
    classes.append(str(BT31BiologicalPartTime))
    
    classes.append(str(BT32PersistenType))
    
    classes.append(str(BT32PersistentType))
    
    classes.append(str(BT33MarineAnimalType))
    
    classes.append(str(BT34Order))
    
    classes.append(str(BT35PropertyType))
    
    classes.append(str(BT36Language))
    
    classes.append(str(BT3PhysicalThingType))
    
    classes.append(str(BT4ConceptualObjectType))
    
    classes.append(str(BT5LegislativeZoneType))
    
    classes.append(str(BT6HumanEventType))
    
    classes.append(str(BT7EcosystemType))
    
    classes.append(str(BT8AbioticElementType))
    
    classes.append(str(BT9ActorType))
    
    classes.append(str(Person))
    
    context = {'classes':classes}
    return render(request, "home.html",context)