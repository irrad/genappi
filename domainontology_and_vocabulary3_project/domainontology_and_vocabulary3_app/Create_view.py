from owlready2 import *
from django.db import models
from django.template.loader import render_to_string
from .models import *
from .forms import *
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseNotFound
from django.template import loader
from django.core.urlresolvers import reverse
import datetime
from .forms import *
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.core.files.storage import FileSystemStorage
from django.utils.safestring import mark_safe
from django.conf import settings


def BC10EventCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC10EventForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC10Event()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC10Event/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC10EventForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC10EventCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC11PersonCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC11PersonForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC11Person()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC11Person/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC11PersonForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC11PersonCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC12EcosystemCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC12EcosystemForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC12Ecosystem()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC12Ecosystem/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC12EcosystemForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC12EcosystemCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC13OrganizationCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC13OrganizationForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC13Organization()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC13Organization/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC13OrganizationForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC13OrganizationCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC14EcosystemEnvironmentCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC14EcosystemEnvironmentForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC14EcosystemEnvironment()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC14EcosystemEnvironment/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC14EcosystemEnvironmentForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC14EcosystemEnvironmentCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC15WaterAreaCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC15WaterAreaForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC15WaterArea()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC15WaterArea/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC15WaterAreaForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC15WaterAreaCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC16ManMadethingCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC16ManMadethingForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC16ManMadething()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC16ManMadething/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC16ManMadethingForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC16ManMadethingCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC17ConceptualObjectCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC17ConceptualObjectForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC17ConceptualObject()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC17ConceptualObject/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC17ConceptualObjectForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC17ConceptualObjectCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC18PropositionCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC18PropositionForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC18Proposition()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC18Proposition/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC18PropositionForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC18PropositionCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC19TitleCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC19TitleForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC19Title()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC19Title/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC19TitleForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC19TitleCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC1TLOEntityCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC1TLOEntityForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC1TLOEntity()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC1TLOEntity/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC1TLOEntityForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC1TLOEntityCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC20DeclarativePlaceCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC20DeclarativePlaceForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC20DeclarativePlace()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC20DeclarativePlace/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC20DeclarativePlaceForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC20DeclarativePlaceCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC21DataSetCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC21DataSetForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC21DataSet()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC21DataSet/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC21DataSetForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC21DataSetCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC22EncounterEventCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC22EncounterEventForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC22EncounterEvent()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC22EncounterEvent/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC22EncounterEventForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC22EncounterEventCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC23DigitalObjectCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC23DigitalObjectForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC23DigitalObject()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC23DigitalObject/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC23DigitalObjectForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC23DigitalObjectCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC24RepositoryObjectCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC24RepositoryObjectForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC24RepositoryObject()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC24RepositoryObject/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC24RepositoryObjectForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC24RepositoryObjectCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC25LinguisticObjectCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC25LinguisticObjectForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC25LinguisticObject()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC25LinguisticObject/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC25LinguisticObjectForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC25LinguisticObjectCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC26PlaceNameCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC26PlaceNameForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC26PlaceName()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC26PlaceName/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC26PlaceNameForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC26PlaceNameCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC27PublicationCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC27PublicationForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC27Publication()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC27Publication/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC27PublicationForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC27PublicationCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC29SpatialCoordinateReferenceSystemCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC29SpatialCoordinateReferenceSystemForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC29SpatialCoordinateReferenceSystem()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC29SpatialCoordinateReferenceSystem/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC29SpatialCoordinateReferenceSystemForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC29SpatialCoordinateReferenceSystemCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC2TimeSpanCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC2TimeSpanForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC2TimeSpan()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC2TimeSpan/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC2TimeSpanForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC2TimeSpanCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC30AppellationCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC30AppellationForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC30Appellation()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC30Appellation/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC30AppellationForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC30AppellationCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC31PlaceAppellationCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC31PlaceAppellationForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC31PlaceAppellation()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC31PlaceAppellation/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC31PlaceAppellationForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC31PlaceAppellationCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC32IdentifierCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC32IdentifierForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC32Identifier()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC32Identifier/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC32IdentifierForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC32IdentifierCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC33SpatialCoordinateCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC33SpatialCoordinateForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC33SpatialCoordinate()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC33SpatialCoordinate/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC33SpatialCoordinateForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC33SpatialCoordinateCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC34GeometricPlaceExpressionCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC34GeometricPlaceExpressionForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC34GeometricPlaceExpression()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC34GeometricPlaceExpression/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC34GeometricPlaceExpressionForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC34GeometricPlaceExpressionCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC35PhysicalThingCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC35PhysicalThingForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC35PhysicalThing()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC35PhysicalThing/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC35PhysicalThingForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC35PhysicalThingCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC36AbioticElementCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC36AbioticElementForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC36AbioticElement()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC36AbioticElement/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC36AbioticElementForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC36AbioticElementCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC37BiologicalObjectCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC37BiologicalObjectForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC37BiologicalObject()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC37BiologicalObject/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC37BiologicalObjectForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC37BiologicalObjectCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC38BioticElementCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC38BioticElementForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC38BioticElement()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC38BioticElement/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC38BioticElementForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC38BioticElementCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC39MarineAnimalCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC39MarineAnimalForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC39MarineAnimal()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC39MarineAnimal/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC39MarineAnimalForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC39MarineAnimalCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC3PlaceCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC3PlaceForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC3Place()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC3Place/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC3PlaceForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC3PlaceCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC40PhysicalManMadeThingCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC40PhysicalManMadeThingForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC40PhysicalManMadeThing()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC40PhysicalManMadeThing/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC40PhysicalManMadeThingForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC40PhysicalManMadeThingCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC41ManMadeObjectCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC41ManMadeObjectForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC41ManMadeObject()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC41ManMadeObject/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC41ManMadeObjectForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC41ManMadeObjectCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC42CollectionCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC42CollectionForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC42Collection()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC42Collection/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC42CollectionForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC42CollectionCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC43ActivityCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC43ActivityForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC43Activity()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC43Activity/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC43ActivityForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC43ActivityCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC44AttributeAssignmentCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC44AttributeAssignmentForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC44AttributeAssignment()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC44AttributeAssignment/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC44AttributeAssignmentForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC44AttributeAssignmentCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC45ObservationCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC45ObservationForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC45Observation()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC45Observation/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC45ObservationForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC45ObservationCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC46IdentifierAssignmentCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC46IdentifierAssignmentForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC46IdentifierAssignment()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC46IdentifierAssignment/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC46IdentifierAssignmentForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC46IdentifierAssignmentCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC47ImageCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC47ImageForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC47Image()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC47Image/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC47ImageForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC47ImageCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC48DatabaseCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC48DatabaseForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC48Database()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC48Database/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC48DatabaseForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC48DatabaseCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC4TemporalPhenomenonCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC4TemporalPhenomenonForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC4TemporalPhenomenon()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC4TemporalPhenomenon/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC4TemporalPhenomenonForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC4TemporalPhenomenonCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC51PhysicalObjectCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC51PhysicalObjectForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC51PhysicalObject()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC51PhysicalObject/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC51PhysicalObjectForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC51PhysicalObjectCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC53SpecimenCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC53SpecimenForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC53Specimen()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC53Specimen/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC53SpecimenForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC53SpecimenCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC54MeasurementCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC54MeasurementForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC54Measurement()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC54Measurement/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC54MeasurementForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC54MeasurementCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC55MeasurementUnitCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC55MeasurementUnitForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC55MeasurementUnit()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC55MeasurementUnit/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC55MeasurementUnitForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC55MeasurementUnitCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC56DigitalMeasurementEventCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC56DigitalMeasurementEventForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC56DigitalMeasurementEvent()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC56DigitalMeasurementEvent/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC56DigitalMeasurementEventForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC56DigitalMeasurementEventCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC57CaptureCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC57CaptureForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC57Capture()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC57Capture/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC57CaptureForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC57CaptureCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC58DigitalDeviceCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC58DigitalDeviceForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC58DigitalDevice()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC58DigitalDevice/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC58DigitalDeviceForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC58DigitalDeviceCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC59SoftwareCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC59SoftwareForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC59Software()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC59Software/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC59SoftwareForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC59SoftwareCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC5DimensionCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC5DimensionForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC5Dimension()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC5Dimension/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC5DimensionForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC5DimensionCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC60SoftwareExecutionCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC60SoftwareExecutionForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC60SoftwareExecution()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC60SoftwareExecution/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC60SoftwareExecutionForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC60SoftwareExecutionCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC61CaptureActivityCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC61CaptureActivityForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC61CaptureActivity()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC61CaptureActivity/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC61CaptureActivityForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC61CaptureActivityCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC62StatisticIndicatorCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC62StatisticIndicatorForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC62StatisticIndicator()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC62StatisticIndicator/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC62StatisticIndicatorForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC62StatisticIndicatorCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC63GlobalStatisticLandingCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC63GlobalStatisticLandingForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC63GlobalStatisticLanding()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC63GlobalStatisticLanding/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC63GlobalStatisticLandingForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC63GlobalStatisticLandingCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC64DesignorProcedureCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC64DesignorProcedureForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC64DesignorProcedure()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC64DesignorProcedure/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC64DesignorProcedureForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC64DesignorProcedureCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC6PersistentItemCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC6PersistentItemForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC6PersistentItem()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC6PersistentItem/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC6PersistentItemForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC6PersistentItemCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC70GroupCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC70GroupForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC70Group()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC70Group/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC70GroupForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC70GroupCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC71NameUseActivityCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC71NameUseActivityForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC71NameUseActivity()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC71NameUseActivity/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC71NameUseActivityForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC71NameUseActivityCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC72SpaceTimeVolumeCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC72SpaceTimeVolumeForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC72SpaceTimeVolume()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC72SpaceTimeVolume/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC72SpaceTimeVolumeForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC72SpaceTimeVolumeCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC75StockCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC75StockForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC75Stock()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC75Stock/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC75StockForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC75StockCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC76StockFormationCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC76StockFormationForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC76StockFormation()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC76StockFormation/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC76StockFormationForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC76StockFormationCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC77MatterRemovalCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC77MatterRemovalForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC77MatterRemoval()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC77MatterRemoval/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC77MatterRemovalForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC77MatterRemovalCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC78AmountofMatterCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC78AmountofMatterForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC78AmountofMatter()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC78AmountofMatter/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC78AmountofMatterForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC78AmountofMatterCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC79RightCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC79RightForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC79Right()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC79Right/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC79RightForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC79RightCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC7ThingCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC7ThingForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC7Thing()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC7Thing/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC7ThingForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC7ThingCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC8ActorCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC8ActorForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC8Actor()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC8Actor/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC8ActorForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC8ActorCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC9ObservableEntityCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BC9ObservableEntityForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BC9ObservableEntity()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BC9ObservableEntity/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC9ObservableEntityForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC9ObservableEntityCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT10BioticElementTypeCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BT10BioticElementTypeForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BT10BioticElementType()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BT10BioticElementType/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT10BioticElementTypeForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT10BioticElementTypeCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT11EquipmentTypeCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BT11EquipmentTypeForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BT11EquipmentType()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BT11EquipmentType/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT11EquipmentTypeForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT11EquipmentTypeCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT12ScientificDataTypeCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BT12ScientificDataTypeForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BT12ScientificDataType()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BT12ScientificDataType/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT12ScientificDataTypeForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT12ScientificDataTypeCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT13DigitalObjectTypeCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BT13DigitalObjectTypeForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BT13DigitalObjectType()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BT13DigitalObjectType/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT13DigitalObjectTypeForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT13DigitalObjectTypeCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT14AppellationTypeCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BT14AppellationTypeForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BT14AppellationType()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BT14AppellationType/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT14AppellationTypeForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT14AppellationTypeCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT16VisualTypeCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BT16VisualTypeForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BT16VisualType()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BT16VisualType/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT16VisualTypeForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT16VisualTypeCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT17HumanActivityTypeCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BT17HumanActivityTypeForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BT17HumanActivityType()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BT17HumanActivityType/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT17HumanActivityTypeForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT17HumanActivityTypeCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT18KingdomCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BT18KingdomForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BT18Kingdom()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BT18Kingdom/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT18KingdomForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT18KingdomCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT19PhylumCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BT19PhylumForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BT19Phylum()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BT19Phylum/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT19PhylumForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT19PhylumCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT1TLOEntityTypeCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BT1TLOEntityTypeForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BT1TLOEntityType()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BT1TLOEntityType/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT1TLOEntityTypeForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT1TLOEntityTypeCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT20SubPhylumCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BT20SubPhylumForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BT20SubPhylum()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BT20SubPhylum/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT20SubPhylumForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT20SubPhylumCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT21SuperClassCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BT21SuperClassForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BT21SuperClass()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BT21SuperClass/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT21SuperClassForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT21SuperClassCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT22ClassCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BT22ClassForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BT22Class()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BT22Class/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT22ClassForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT22ClassCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT23SubClassCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BT23SubClassForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BT23SubClass()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BT23SubClass/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT23SubClassForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT23SubClassCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT24FamilyCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BT24FamilyForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BT24Family()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BT24Family/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT24FamilyForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT24FamilyCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT25SubFamilyCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BT25SubFamilyForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BT25SubFamily()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BT25SubFamily/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT25SubFamilyForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT25SubFamilyCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT26GenusCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BT26GenusForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BT26Genus()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BT26Genus/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT26GenusForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT26GenusCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT27SpeciesCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BT27SpeciesForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BT27Species()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BT27Species/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT27SpeciesForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT27SpeciesCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT28ScientificActivityTypeCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BT28ScientificActivityTypeForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BT28ScientificActivityType()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BT28ScientificActivityType/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT28ScientificActivityTypeForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT28ScientificActivityTypeCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT29IndustrialActivityTypeCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BT29IndustrialActivityTypeForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BT29IndustrialActivityType()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BT29IndustrialActivityType/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT29IndustrialActivityTypeForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT29IndustrialActivityTypeCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT2TemporalPhenomenonTypeCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BT2TemporalPhenomenonTypeForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BT2TemporalPhenomenonType()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BT2TemporalPhenomenonType/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT2TemporalPhenomenonTypeForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT2TemporalPhenomenonTypeCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT30IdentifierAssignmentTypeCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BT30IdentifierAssignmentTypeForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BT30IdentifierAssignmentType()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BT30IdentifierAssignmentType/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT30IdentifierAssignmentTypeForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT30IdentifierAssignmentTypeCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT31BiologicalPartTimeCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BT31BiologicalPartTimeForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BT31BiologicalPartTime()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BT31BiologicalPartTime/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT31BiologicalPartTimeForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT31BiologicalPartTimeCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT32PersistenTypeCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BT32PersistenTypeForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BT32PersistenType()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BT32PersistenType/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT32PersistenTypeForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT32PersistenTypeCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT32PersistentTypeCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BT32PersistentTypeForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BT32PersistentType()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BT32PersistentType/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT32PersistentTypeForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT32PersistentTypeCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT33MarineAnimalTypeCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BT33MarineAnimalTypeForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BT33MarineAnimalType()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BT33MarineAnimalType/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT33MarineAnimalTypeForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT33MarineAnimalTypeCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT34OrderCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BT34OrderForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BT34Order()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BT34Order/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT34OrderForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT34OrderCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT35PropertyTypeCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BT35PropertyTypeForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BT35PropertyType()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BT35PropertyType/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT35PropertyTypeForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT35PropertyTypeCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT36LanguageCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BT36LanguageForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BT36Language()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BT36Language/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT36LanguageForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT36LanguageCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT3PhysicalThingTypeCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BT3PhysicalThingTypeForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BT3PhysicalThingType()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BT3PhysicalThingType/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT3PhysicalThingTypeForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT3PhysicalThingTypeCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT4ConceptualObjectTypeCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BT4ConceptualObjectTypeForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BT4ConceptualObjectType()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BT4ConceptualObjectType/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT4ConceptualObjectTypeForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT4ConceptualObjectTypeCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT5LegislativeZoneTypeCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BT5LegislativeZoneTypeForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BT5LegislativeZoneType()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BT5LegislativeZoneType/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT5LegislativeZoneTypeForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT5LegislativeZoneTypeCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT6HumanEventTypeCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BT6HumanEventTypeForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BT6HumanEventType()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BT6HumanEventType/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT6HumanEventTypeForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT6HumanEventTypeCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT7EcosystemTypeCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BT7EcosystemTypeForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BT7EcosystemType()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BT7EcosystemType/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT7EcosystemTypeForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT7EcosystemTypeCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT8AbioticElementTypeCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BT8AbioticElementTypeForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BT8AbioticElementType()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BT8AbioticElementType/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT8AbioticElementTypeForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT8AbioticElementTypeCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT9ActorTypeCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = BT9ActorTypeForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.BT9ActorType()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/BT9ActorType/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT9ActorTypeForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT9ActorTypeCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def PersonCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("iedm.owl")
    onto.load()

    

    if request.method == 'POST':
        form = PersonForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto.Person()
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/domainontology_and_vocabulary3_app/Person/list/"
            return HttpResponseRedirect(link)
    else:
        form = PersonForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "PersonCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})
