from django.db import models

class BC10Event(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC11Person(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC12Ecosystem(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC13Organization(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC14EcosystemEnvironment(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC15WaterArea(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC16ManMadething(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC17ConceptualObject(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC18Proposition(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC19Title(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC1TLOEntity(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC20DeclarativePlace(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC21DataSet(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC22EncounterEvent(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC23DigitalObject(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC24RepositoryObject(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC25LinguisticObject(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC26PlaceName(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC27Publication(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC29SpatialCoordinateReferenceSystem(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC2TimeSpan(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC30Appellation(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC31PlaceAppellation(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC32Identifier(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC33SpatialCoordinate(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC34GeometricPlaceExpression(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC35PhysicalThing(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC36AbioticElement(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC37BiologicalObject(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC38BioticElement(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC39MarineAnimal(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC3Place(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC40PhysicalManMadeThing(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC41ManMadeObject(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC42Collection(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC43Activity(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC44AttributeAssignment(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC45Observation(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC46IdentifierAssignment(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC47Image(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC48Database(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC4TemporalPhenomenon(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC51PhysicalObject(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC53Specimen(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC54Measurement(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC55MeasurementUnit(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC56DigitalMeasurementEvent(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC57Capture(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC58DigitalDevice(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC59Software(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC5Dimension(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC60SoftwareExecution(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC61CaptureActivity(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC62StatisticIndicator(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC63GlobalStatisticLanding(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC64DesignorProcedure(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC6PersistentItem(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC70Group(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC71NameUseActivity(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC72SpaceTimeVolume(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC75Stock(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC76StockFormation(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC77MatterRemoval(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC78AmountofMatter(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC79Right(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC7Thing(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC8Actor(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC9ObservableEntity(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT10BioticElementType(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT11EquipmentType(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT12ScientificDataType(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT13DigitalObjectType(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT14AppellationType(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT16VisualType(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT17HumanActivityType(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT18Kingdom(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT19Phylum(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT1TLOEntityType(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT20SubPhylum(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT21SuperClass(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT22Class(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT23SubClass(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT24Family(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT25SubFamily(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT26Genus(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT27Species(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT28ScientificActivityType(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT29IndustrialActivityType(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT2TemporalPhenomenonType(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT30IdentifierAssignmentType(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT31BiologicalPartTime(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT32PersistenType(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT32PersistentType(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT33MarineAnimalType(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT34Order(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT35PropertyType(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT36Language(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT3PhysicalThingType(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT4ConceptualObjectType(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT5LegislativeZoneType(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT6HumanEventType(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT7EcosystemType(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT8AbioticElementType(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT9ActorType(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Person(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name
