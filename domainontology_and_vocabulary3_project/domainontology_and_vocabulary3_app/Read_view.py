from django.db import models
from django.template.loader import render_to_string
from .models import *
from .forms import *
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseNotFound
from django.template import loader
from django.core.urlresolvers import reverse
import datetime
from .forms import *
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.core.files.storage import FileSystemStorage
from django.utils.safestring import mark_safe
from django.core import serializers


def BC10EventRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC10Event, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC10EventRead.html",{'instance_items': instance_items})

def BC11PersonRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC11Person, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC11PersonRead.html",{'instance_items': instance_items})

def BC12EcosystemRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC12Ecosystem, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC12EcosystemRead.html",{'instance_items': instance_items})

def BC13OrganizationRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC13Organization, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC13OrganizationRead.html",{'instance_items': instance_items})

def BC14EcosystemEnvironmentRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC14EcosystemEnvironment, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC14EcosystemEnvironmentRead.html",{'instance_items': instance_items})

def BC15WaterAreaRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC15WaterArea, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC15WaterAreaRead.html",{'instance_items': instance_items})

def BC16ManMadethingRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC16ManMadething, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC16ManMadethingRead.html",{'instance_items': instance_items})

def BC17ConceptualObjectRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC17ConceptualObject, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC17ConceptualObjectRead.html",{'instance_items': instance_items})

def BC18PropositionRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC18Proposition, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC18PropositionRead.html",{'instance_items': instance_items})

def BC19TitleRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC19Title, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC19TitleRead.html",{'instance_items': instance_items})

def BC1TLOEntityRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC1TLOEntity, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC1TLOEntityRead.html",{'instance_items': instance_items})

def BC20DeclarativePlaceRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC20DeclarativePlace, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC20DeclarativePlaceRead.html",{'instance_items': instance_items})

def BC21DataSetRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC21DataSet, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC21DataSetRead.html",{'instance_items': instance_items})

def BC22EncounterEventRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC22EncounterEvent, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC22EncounterEventRead.html",{'instance_items': instance_items})

def BC23DigitalObjectRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC23DigitalObject, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC23DigitalObjectRead.html",{'instance_items': instance_items})

def BC24RepositoryObjectRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC24RepositoryObject, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC24RepositoryObjectRead.html",{'instance_items': instance_items})

def BC25LinguisticObjectRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC25LinguisticObject, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC25LinguisticObjectRead.html",{'instance_items': instance_items})

def BC26PlaceNameRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC26PlaceName, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC26PlaceNameRead.html",{'instance_items': instance_items})

def BC27PublicationRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC27Publication, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC27PublicationRead.html",{'instance_items': instance_items})

def BC29SpatialCoordinateReferenceSystemRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC29SpatialCoordinateReferenceSystem, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC29SpatialCoordinateReferenceSystemRead.html",{'instance_items': instance_items})

def BC2TimeSpanRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC2TimeSpan, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC2TimeSpanRead.html",{'instance_items': instance_items})

def BC30AppellationRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC30Appellation, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC30AppellationRead.html",{'instance_items': instance_items})

def BC31PlaceAppellationRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC31PlaceAppellation, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC31PlaceAppellationRead.html",{'instance_items': instance_items})

def BC32IdentifierRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC32Identifier, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC32IdentifierRead.html",{'instance_items': instance_items})

def BC33SpatialCoordinateRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC33SpatialCoordinate, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC33SpatialCoordinateRead.html",{'instance_items': instance_items})

def BC34GeometricPlaceExpressionRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC34GeometricPlaceExpression, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC34GeometricPlaceExpressionRead.html",{'instance_items': instance_items})

def BC35PhysicalThingRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC35PhysicalThing, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC35PhysicalThingRead.html",{'instance_items': instance_items})

def BC36AbioticElementRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC36AbioticElement, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC36AbioticElementRead.html",{'instance_items': instance_items})

def BC37BiologicalObjectRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC37BiologicalObject, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC37BiologicalObjectRead.html",{'instance_items': instance_items})

def BC38BioticElementRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC38BioticElement, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC38BioticElementRead.html",{'instance_items': instance_items})

def BC39MarineAnimalRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC39MarineAnimal, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC39MarineAnimalRead.html",{'instance_items': instance_items})

def BC3PlaceRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC3Place, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC3PlaceRead.html",{'instance_items': instance_items})

def BC40PhysicalManMadeThingRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC40PhysicalManMadeThing, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC40PhysicalManMadeThingRead.html",{'instance_items': instance_items})

def BC41ManMadeObjectRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC41ManMadeObject, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC41ManMadeObjectRead.html",{'instance_items': instance_items})

def BC42CollectionRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC42Collection, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC42CollectionRead.html",{'instance_items': instance_items})

def BC43ActivityRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC43Activity, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC43ActivityRead.html",{'instance_items': instance_items})

def BC44AttributeAssignmentRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC44AttributeAssignment, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC44AttributeAssignmentRead.html",{'instance_items': instance_items})

def BC45ObservationRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC45Observation, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC45ObservationRead.html",{'instance_items': instance_items})

def BC46IdentifierAssignmentRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC46IdentifierAssignment, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC46IdentifierAssignmentRead.html",{'instance_items': instance_items})

def BC47ImageRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC47Image, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC47ImageRead.html",{'instance_items': instance_items})

def BC48DatabaseRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC48Database, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC48DatabaseRead.html",{'instance_items': instance_items})

def BC4TemporalPhenomenonRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC4TemporalPhenomenon, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC4TemporalPhenomenonRead.html",{'instance_items': instance_items})

def BC51PhysicalObjectRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC51PhysicalObject, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC51PhysicalObjectRead.html",{'instance_items': instance_items})

def BC53SpecimenRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC53Specimen, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC53SpecimenRead.html",{'instance_items': instance_items})

def BC54MeasurementRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC54Measurement, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC54MeasurementRead.html",{'instance_items': instance_items})

def BC55MeasurementUnitRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC55MeasurementUnit, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC55MeasurementUnitRead.html",{'instance_items': instance_items})

def BC56DigitalMeasurementEventRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC56DigitalMeasurementEvent, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC56DigitalMeasurementEventRead.html",{'instance_items': instance_items})

def BC57CaptureRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC57Capture, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC57CaptureRead.html",{'instance_items': instance_items})

def BC58DigitalDeviceRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC58DigitalDevice, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC58DigitalDeviceRead.html",{'instance_items': instance_items})

def BC59SoftwareRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC59Software, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC59SoftwareRead.html",{'instance_items': instance_items})

def BC5DimensionRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC5Dimension, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC5DimensionRead.html",{'instance_items': instance_items})

def BC60SoftwareExecutionRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC60SoftwareExecution, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC60SoftwareExecutionRead.html",{'instance_items': instance_items})

def BC61CaptureActivityRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC61CaptureActivity, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC61CaptureActivityRead.html",{'instance_items': instance_items})

def BC62StatisticIndicatorRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC62StatisticIndicator, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC62StatisticIndicatorRead.html",{'instance_items': instance_items})

def BC63GlobalStatisticLandingRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC63GlobalStatisticLanding, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC63GlobalStatisticLandingRead.html",{'instance_items': instance_items})

def BC64DesignorProcedureRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC64DesignorProcedure, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC64DesignorProcedureRead.html",{'instance_items': instance_items})

def BC6PersistentItemRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC6PersistentItem, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC6PersistentItemRead.html",{'instance_items': instance_items})

def BC70GroupRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC70Group, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC70GroupRead.html",{'instance_items': instance_items})

def BC71NameUseActivityRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC71NameUseActivity, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC71NameUseActivityRead.html",{'instance_items': instance_items})

def BC72SpaceTimeVolumeRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC72SpaceTimeVolume, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC72SpaceTimeVolumeRead.html",{'instance_items': instance_items})

def BC75StockRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC75Stock, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC75StockRead.html",{'instance_items': instance_items})

def BC76StockFormationRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC76StockFormation, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC76StockFormationRead.html",{'instance_items': instance_items})

def BC77MatterRemovalRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC77MatterRemoval, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC77MatterRemovalRead.html",{'instance_items': instance_items})

def BC78AmountofMatterRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC78AmountofMatter, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC78AmountofMatterRead.html",{'instance_items': instance_items})

def BC79RightRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC79Right, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC79RightRead.html",{'instance_items': instance_items})

def BC7ThingRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC7Thing, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC7ThingRead.html",{'instance_items': instance_items})

def BC8ActorRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC8Actor, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC8ActorRead.html",{'instance_items': instance_items})

def BC9ObservableEntityRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC9ObservableEntity, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC9ObservableEntityRead.html",{'instance_items': instance_items})

def BT10BioticElementTypeRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT10BioticElementType, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT10BioticElementTypeRead.html",{'instance_items': instance_items})

def BT11EquipmentTypeRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT11EquipmentType, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT11EquipmentTypeRead.html",{'instance_items': instance_items})

def BT12ScientificDataTypeRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT12ScientificDataType, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT12ScientificDataTypeRead.html",{'instance_items': instance_items})

def BT13DigitalObjectTypeRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT13DigitalObjectType, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT13DigitalObjectTypeRead.html",{'instance_items': instance_items})

def BT14AppellationTypeRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT14AppellationType, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT14AppellationTypeRead.html",{'instance_items': instance_items})

def BT16VisualTypeRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT16VisualType, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT16VisualTypeRead.html",{'instance_items': instance_items})

def BT17HumanActivityTypeRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT17HumanActivityType, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT17HumanActivityTypeRead.html",{'instance_items': instance_items})

def BT18KingdomRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT18Kingdom, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT18KingdomRead.html",{'instance_items': instance_items})

def BT19PhylumRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT19Phylum, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT19PhylumRead.html",{'instance_items': instance_items})

def BT1TLOEntityTypeRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT1TLOEntityType, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT1TLOEntityTypeRead.html",{'instance_items': instance_items})

def BT20SubPhylumRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT20SubPhylum, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT20SubPhylumRead.html",{'instance_items': instance_items})

def BT21SuperClassRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT21SuperClass, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT21SuperClassRead.html",{'instance_items': instance_items})

def BT22ClassRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT22Class, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT22ClassRead.html",{'instance_items': instance_items})

def BT23SubClassRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT23SubClass, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT23SubClassRead.html",{'instance_items': instance_items})

def BT24FamilyRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT24Family, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT24FamilyRead.html",{'instance_items': instance_items})

def BT25SubFamilyRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT25SubFamily, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT25SubFamilyRead.html",{'instance_items': instance_items})

def BT26GenusRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT26Genus, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT26GenusRead.html",{'instance_items': instance_items})

def BT27SpeciesRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT27Species, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT27SpeciesRead.html",{'instance_items': instance_items})

def BT28ScientificActivityTypeRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT28ScientificActivityType, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT28ScientificActivityTypeRead.html",{'instance_items': instance_items})

def BT29IndustrialActivityTypeRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT29IndustrialActivityType, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT29IndustrialActivityTypeRead.html",{'instance_items': instance_items})

def BT2TemporalPhenomenonTypeRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT2TemporalPhenomenonType, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT2TemporalPhenomenonTypeRead.html",{'instance_items': instance_items})

def BT30IdentifierAssignmentTypeRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT30IdentifierAssignmentType, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT30IdentifierAssignmentTypeRead.html",{'instance_items': instance_items})

def BT31BiologicalPartTimeRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT31BiologicalPartTime, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT31BiologicalPartTimeRead.html",{'instance_items': instance_items})

def BT32PersistenTypeRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT32PersistenType, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT32PersistenTypeRead.html",{'instance_items': instance_items})

def BT32PersistentTypeRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT32PersistentType, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT32PersistentTypeRead.html",{'instance_items': instance_items})

def BT33MarineAnimalTypeRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT33MarineAnimalType, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT33MarineAnimalTypeRead.html",{'instance_items': instance_items})

def BT34OrderRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT34Order, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT34OrderRead.html",{'instance_items': instance_items})

def BT35PropertyTypeRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT35PropertyType, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT35PropertyTypeRead.html",{'instance_items': instance_items})

def BT36LanguageRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT36Language, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT36LanguageRead.html",{'instance_items': instance_items})

def BT3PhysicalThingTypeRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT3PhysicalThingType, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT3PhysicalThingTypeRead.html",{'instance_items': instance_items})

def BT4ConceptualObjectTypeRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT4ConceptualObjectType, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT4ConceptualObjectTypeRead.html",{'instance_items': instance_items})

def BT5LegislativeZoneTypeRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT5LegislativeZoneType, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT5LegislativeZoneTypeRead.html",{'instance_items': instance_items})

def BT6HumanEventTypeRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT6HumanEventType, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT6HumanEventTypeRead.html",{'instance_items': instance_items})

def BT7EcosystemTypeRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT7EcosystemType, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT7EcosystemTypeRead.html",{'instance_items': instance_items})

def BT8AbioticElementTypeRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT8AbioticElementType, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT8AbioticElementTypeRead.html",{'instance_items': instance_items})

def BT9ActorTypeRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT9ActorType, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT9ActorTypeRead.html",{'instance_items': instance_items})

def PersonRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Person, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"PersonRead.html",{'instance_items': instance_items})
