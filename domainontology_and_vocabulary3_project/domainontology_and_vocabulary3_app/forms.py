from django import forms
from django.forms import ModelForm
from .models import *


class BC10EventForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC10EventForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC10Event
        exclude = ()

class BC11PersonForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC11PersonForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC11Person
        exclude = ()

class BC12EcosystemForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC12EcosystemForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC12Ecosystem
        exclude = ()

class BC13OrganizationForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC13OrganizationForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC13Organization
        exclude = ()

class BC14EcosystemEnvironmentForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC14EcosystemEnvironmentForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC14EcosystemEnvironment
        exclude = ()

class BC15WaterAreaForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC15WaterAreaForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC15WaterArea
        exclude = ()

class BC16ManMadethingForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC16ManMadethingForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC16ManMadething
        exclude = ()

class BC17ConceptualObjectForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC17ConceptualObjectForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC17ConceptualObject
        exclude = ()

class BC18PropositionForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC18PropositionForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC18Proposition
        exclude = ()

class BC19TitleForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC19TitleForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC19Title
        exclude = ()

class BC1TLOEntityForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC1TLOEntityForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC1TLOEntity
        exclude = ()

class BC20DeclarativePlaceForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC20DeclarativePlaceForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC20DeclarativePlace
        exclude = ()

class BC21DataSetForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC21DataSetForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC21DataSet
        exclude = ()

class BC22EncounterEventForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC22EncounterEventForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC22EncounterEvent
        exclude = ()

class BC23DigitalObjectForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC23DigitalObjectForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC23DigitalObject
        exclude = ()

class BC24RepositoryObjectForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC24RepositoryObjectForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC24RepositoryObject
        exclude = ()

class BC25LinguisticObjectForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC25LinguisticObjectForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC25LinguisticObject
        exclude = ()

class BC26PlaceNameForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC26PlaceNameForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC26PlaceName
        exclude = ()

class BC27PublicationForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC27PublicationForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC27Publication
        exclude = ()

class BC29SpatialCoordinateReferenceSystemForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC29SpatialCoordinateReferenceSystemForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC29SpatialCoordinateReferenceSystem
        exclude = ()

class BC2TimeSpanForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC2TimeSpanForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC2TimeSpan
        exclude = ()

class BC30AppellationForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC30AppellationForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC30Appellation
        exclude = ()

class BC31PlaceAppellationForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC31PlaceAppellationForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC31PlaceAppellation
        exclude = ()

class BC32IdentifierForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC32IdentifierForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC32Identifier
        exclude = ()

class BC33SpatialCoordinateForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC33SpatialCoordinateForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC33SpatialCoordinate
        exclude = ()

class BC34GeometricPlaceExpressionForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC34GeometricPlaceExpressionForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC34GeometricPlaceExpression
        exclude = ()

class BC35PhysicalThingForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC35PhysicalThingForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC35PhysicalThing
        exclude = ()

class BC36AbioticElementForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC36AbioticElementForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC36AbioticElement
        exclude = ()

class BC37BiologicalObjectForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC37BiologicalObjectForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC37BiologicalObject
        exclude = ()

class BC38BioticElementForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC38BioticElementForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC38BioticElement
        exclude = ()

class BC39MarineAnimalForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC39MarineAnimalForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC39MarineAnimal
        exclude = ()

class BC3PlaceForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC3PlaceForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC3Place
        exclude = ()

class BC40PhysicalManMadeThingForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC40PhysicalManMadeThingForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC40PhysicalManMadeThing
        exclude = ()

class BC41ManMadeObjectForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC41ManMadeObjectForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC41ManMadeObject
        exclude = ()

class BC42CollectionForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC42CollectionForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC42Collection
        exclude = ()

class BC43ActivityForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC43ActivityForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC43Activity
        exclude = ()

class BC44AttributeAssignmentForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC44AttributeAssignmentForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC44AttributeAssignment
        exclude = ()

class BC45ObservationForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC45ObservationForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC45Observation
        exclude = ()

class BC46IdentifierAssignmentForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC46IdentifierAssignmentForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC46IdentifierAssignment
        exclude = ()

class BC47ImageForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC47ImageForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC47Image
        exclude = ()

class BC48DatabaseForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC48DatabaseForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC48Database
        exclude = ()

class BC4TemporalPhenomenonForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC4TemporalPhenomenonForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC4TemporalPhenomenon
        exclude = ()

class BC51PhysicalObjectForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC51PhysicalObjectForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC51PhysicalObject
        exclude = ()

class BC53SpecimenForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC53SpecimenForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC53Specimen
        exclude = ()

class BC54MeasurementForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC54MeasurementForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC54Measurement
        exclude = ()

class BC55MeasurementUnitForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC55MeasurementUnitForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC55MeasurementUnit
        exclude = ()

class BC56DigitalMeasurementEventForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC56DigitalMeasurementEventForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC56DigitalMeasurementEvent
        exclude = ()

class BC57CaptureForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC57CaptureForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC57Capture
        exclude = ()

class BC58DigitalDeviceForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC58DigitalDeviceForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC58DigitalDevice
        exclude = ()

class BC59SoftwareForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC59SoftwareForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC59Software
        exclude = ()

class BC5DimensionForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC5DimensionForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC5Dimension
        exclude = ()

class BC60SoftwareExecutionForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC60SoftwareExecutionForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC60SoftwareExecution
        exclude = ()

class BC61CaptureActivityForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC61CaptureActivityForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC61CaptureActivity
        exclude = ()

class BC62StatisticIndicatorForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC62StatisticIndicatorForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC62StatisticIndicator
        exclude = ()

class BC63GlobalStatisticLandingForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC63GlobalStatisticLandingForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC63GlobalStatisticLanding
        exclude = ()

class BC64DesignorProcedureForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC64DesignorProcedureForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC64DesignorProcedure
        exclude = ()

class BC6PersistentItemForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC6PersistentItemForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC6PersistentItem
        exclude = ()

class BC70GroupForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC70GroupForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC70Group
        exclude = ()

class BC71NameUseActivityForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC71NameUseActivityForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC71NameUseActivity
        exclude = ()

class BC72SpaceTimeVolumeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC72SpaceTimeVolumeForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC72SpaceTimeVolume
        exclude = ()

class BC75StockForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC75StockForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC75Stock
        exclude = ()

class BC76StockFormationForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC76StockFormationForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC76StockFormation
        exclude = ()

class BC77MatterRemovalForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC77MatterRemovalForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC77MatterRemoval
        exclude = ()

class BC78AmountofMatterForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC78AmountofMatterForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC78AmountofMatter
        exclude = ()

class BC79RightForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC79RightForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC79Right
        exclude = ()

class BC7ThingForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC7ThingForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC7Thing
        exclude = ()

class BC8ActorForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC8ActorForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC8Actor
        exclude = ()

class BC9ObservableEntityForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC9ObservableEntityForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC9ObservableEntity
        exclude = ()

class BT10BioticElementTypeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT10BioticElementTypeForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT10BioticElementType
        exclude = ()

class BT11EquipmentTypeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT11EquipmentTypeForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT11EquipmentType
        exclude = ()

class BT12ScientificDataTypeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT12ScientificDataTypeForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT12ScientificDataType
        exclude = ()

class BT13DigitalObjectTypeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT13DigitalObjectTypeForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT13DigitalObjectType
        exclude = ()

class BT14AppellationTypeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT14AppellationTypeForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT14AppellationType
        exclude = ()

class BT16VisualTypeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT16VisualTypeForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT16VisualType
        exclude = ()

class BT17HumanActivityTypeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT17HumanActivityTypeForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT17HumanActivityType
        exclude = ()

class BT18KingdomForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT18KingdomForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT18Kingdom
        exclude = ()

class BT19PhylumForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT19PhylumForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT19Phylum
        exclude = ()

class BT1TLOEntityTypeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT1TLOEntityTypeForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT1TLOEntityType
        exclude = ()

class BT20SubPhylumForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT20SubPhylumForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT20SubPhylum
        exclude = ()

class BT21SuperClassForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT21SuperClassForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT21SuperClass
        exclude = ()

class BT22ClassForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT22ClassForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT22Class
        exclude = ()

class BT23SubClassForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT23SubClassForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT23SubClass
        exclude = ()

class BT24FamilyForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT24FamilyForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT24Family
        exclude = ()

class BT25SubFamilyForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT25SubFamilyForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT25SubFamily
        exclude = ()

class BT26GenusForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT26GenusForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT26Genus
        exclude = ()

class BT27SpeciesForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT27SpeciesForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT27Species
        exclude = ()

class BT28ScientificActivityTypeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT28ScientificActivityTypeForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT28ScientificActivityType
        exclude = ()

class BT29IndustrialActivityTypeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT29IndustrialActivityTypeForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT29IndustrialActivityType
        exclude = ()

class BT2TemporalPhenomenonTypeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT2TemporalPhenomenonTypeForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT2TemporalPhenomenonType
        exclude = ()

class BT30IdentifierAssignmentTypeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT30IdentifierAssignmentTypeForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT30IdentifierAssignmentType
        exclude = ()

class BT31BiologicalPartTimeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT31BiologicalPartTimeForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT31BiologicalPartTime
        exclude = ()

class BT32PersistenTypeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT32PersistenTypeForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT32PersistenType
        exclude = ()

class BT32PersistentTypeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT32PersistentTypeForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT32PersistentType
        exclude = ()

class BT33MarineAnimalTypeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT33MarineAnimalTypeForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT33MarineAnimalType
        exclude = ()

class BT34OrderForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT34OrderForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT34Order
        exclude = ()

class BT35PropertyTypeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT35PropertyTypeForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT35PropertyType
        exclude = ()

class BT36LanguageForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT36LanguageForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT36Language
        exclude = ()

class BT3PhysicalThingTypeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT3PhysicalThingTypeForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT3PhysicalThingType
        exclude = ()

class BT4ConceptualObjectTypeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT4ConceptualObjectTypeForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT4ConceptualObjectType
        exclude = ()

class BT5LegislativeZoneTypeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT5LegislativeZoneTypeForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT5LegislativeZoneType
        exclude = ()

class BT6HumanEventTypeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT6HumanEventTypeForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT6HumanEventType
        exclude = ()

class BT7EcosystemTypeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT7EcosystemTypeForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT7EcosystemType
        exclude = ()

class BT8AbioticElementTypeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT8AbioticElementTypeForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT8AbioticElementType
        exclude = ()

class BT9ActorTypeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT9ActorTypeForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT9ActorType
        exclude = ()

class PersonForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(PersonForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Person
        exclude = ()
