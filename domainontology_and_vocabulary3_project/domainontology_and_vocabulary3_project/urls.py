from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    # Examples:
    # url(r'^$', 'domainontology_and_vocabulary3_project.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^vowl', 'domainontology_and_vocabulary3_app.Home_view.vowl', name='vowl'),
    url(r'^domainontology_and_vocabulary3_app/', include('domainontology_and_vocabulary3_app.urls', namespace='domainontology_and_vocabulary3_app')),
    url(r'^$', 'domainontology_and_vocabulary3_app.Home_view.HomeView', name='HomeView'),    url(r'^accounts/',include('django.contrib.auth.urls')),]
