from django.db import models
from django.template.loader import render_to_string
from .models import *
from .forms import *
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseNotFound
from django.template import loader
from django.core.urlresolvers import reverse
import datetime
from .forms import *
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.core.files.storage import FileSystemStorage
from django.utils.safestring import mark_safe


def AgentDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Agent,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/sosa_app/Agent/list/"
        return HttpResponseRedirect(link)
    else:
         form = AgentForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AgentDelete.html",context)

def TemporalEntityDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(TemporalEntity,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/sosa_app/TemporalEntity/list/"
        return HttpResponseRedirect(link)
    else:
         form = TemporalEntityForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "TemporalEntityDelete.html",context)

def ProcedureDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Procedure,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/sosa_app/Procedure/list/"
        return HttpResponseRedirect(link)
    else:
         form = ProcedureForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ProcedureDelete.html",context)

def VocabularyDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Vocabulary,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/sosa_app/Vocabulary/list/"
        return HttpResponseRedirect(link)
    else:
         form = VocabularyForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "VocabularyDelete.html",context)

def ActuationDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Actuation,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/sosa_app/Actuation/list/"
        return HttpResponseRedirect(link)
    else:
         form = ActuationForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ActuationDelete.html",context)

def ActuatablePropertyDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ActuatableProperty,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/sosa_app/ActuatableProperty/list/"
        return HttpResponseRedirect(link)
    else:
         form = ActuatablePropertyForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ActuatablePropertyDelete.html",context)

def ObservationDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Observation,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/sosa_app/Observation/list/"
        return HttpResponseRedirect(link)
    else:
         form = ObservationForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ObservationDelete.html",context)

def SamplingDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Sampling,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/sosa_app/Sampling/list/"
        return HttpResponseRedirect(link)
    else:
         form = SamplingForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SamplingDelete.html",context)

def FeatureOfInterestDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(FeatureOfInterest,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/sosa_app/FeatureOfInterest/list/"
        return HttpResponseRedirect(link)
    else:
         form = FeatureOfInterestForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "FeatureOfInterestDelete.html",context)

def SampleDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Sample,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/sosa_app/Sample/list/"
        return HttpResponseRedirect(link)
    else:
         form = SampleForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SampleDelete.html",context)

def ResultDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Result,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/sosa_app/Result/list/"
        return HttpResponseRedirect(link)
    else:
         form = ResultForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ResultDelete.html",context)

def PlatformDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Platform,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/sosa_app/Platform/list/"
        return HttpResponseRedirect(link)
    else:
         form = PlatformForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "PlatformDelete.html",context)

def ActuatorDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Actuator,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/sosa_app/Actuator/list/"
        return HttpResponseRedirect(link)
    else:
         form = ActuatorForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ActuatorDelete.html",context)

def SamplerDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Sampler,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/sosa_app/Sampler/list/"
        return HttpResponseRedirect(link)
    else:
         form = SamplerForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SamplerDelete.html",context)

def SensorDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Sensor,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/sosa_app/Sensor/list/"
        return HttpResponseRedirect(link)
    else:
         form = SensorForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SensorDelete.html",context)

def ObservablePropertyDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ObservableProperty,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/sosa_app/ObservableProperty/list/"
        return HttpResponseRedirect(link)
    else:
         form = ObservablePropertyForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ObservablePropertyDelete.html",context)
