from owlready2 import *
from django.db import models
from django.template.loader import render_to_string
from .models import *
from .forms import *
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseNotFound
from django.template import loader
from django.core.urlresolvers import reverse
import datetime
from .forms import *
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.core.files.storage import FileSystemStorage
from django.utils.safestring import mark_safe
from django.conf import settings

onto_path.append(settings.BASE_DIR)
onto = get_ontology("raw.githubusercontent.com/w3c/sdw/gh-pages/ssn/integrated/sosa.rdf").load()
ontoclasses = onto.classes()
namespaces = dict() 
for clas in ontoclasses:
    namespaces[clas.name] = clas.namespace


def AgentUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Agent"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Agent, pk=pk)
    if request.method == 'POST':
        form = AgentForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()
            onto_instance = class_namespace.Agent(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/sosa_app/Agent/list/"
            return HttpResponseRedirect(link)
    else:
        form = AgentForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AgentUpdate.html",context)

def TemporalEntityUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["TemporalEntity"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(TemporalEntity, pk=pk)
    if request.method == 'POST':
        form = TemporalEntityForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.TemporalEntity(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/sosa_app/TemporalEntity/list/"
            return HttpResponseRedirect(link)
    else:
        form = TemporalEntityForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "TemporalEntityUpdate.html",context)

def ProcedureUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Procedure"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Procedure, pk=pk)
    if request.method == 'POST':
        form = ProcedureForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Procedure(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/sosa_app/Procedure/list/"
            return HttpResponseRedirect(link)
    else:
        form = ProcedureForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ProcedureUpdate.html",context)

def VocabularyUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Vocabulary"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Vocabulary, pk=pk)
    if request.method == 'POST':
        form = VocabularyForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()
            st = str(class_namespace) + "Voc3"
            print(st)
            inst = onto.search(iri = st)
            print(inst)
            inst[0].name = "hi3" 
            onto_instance = class_namespace.Vocabulary(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/sosa_app/Vocabulary/list/"
            return HttpResponseRedirect(link)
    else:
        form = VocabularyForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "VocabularyUpdate.html",context)

def ActuationUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Actuation"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Actuation, pk=pk)
    if request.method == 'POST':
        form = ActuationForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Actuation(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/sosa_app/Actuation/list/"
            return HttpResponseRedirect(link)
    else:
        form = ActuationForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ActuationUpdate.html",context)

def ActuatablePropertyUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ActuatableProperty"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ActuatableProperty, pk=pk)
    if request.method == 'POST':
        form = ActuatablePropertyForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ActuatableProperty(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/sosa_app/ActuatableProperty/list/"
            return HttpResponseRedirect(link)
    else:
        form = ActuatablePropertyForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ActuatablePropertyUpdate.html",context)

def ObservationUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Observation"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Observation, pk=pk)
    if request.method == 'POST':
        form = ObservationForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Observation(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/sosa_app/Observation/list/"
            return HttpResponseRedirect(link)
    else:
        form = ObservationForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ObservationUpdate.html",context)

def SamplingUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Sampling"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Sampling, pk=pk)
    if request.method == 'POST':
        form = SamplingForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Sampling(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/sosa_app/Sampling/list/"
            return HttpResponseRedirect(link)
    else:
        form = SamplingForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SamplingUpdate.html",context)

def FeatureOfInterestUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["FeatureOfInterest"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(FeatureOfInterest, pk=pk)
    if request.method == 'POST':
        form = FeatureOfInterestForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.FeatureOfInterest(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/sosa_app/FeatureOfInterest/list/"
            return HttpResponseRedirect(link)
    else:
        form = FeatureOfInterestForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "FeatureOfInterestUpdate.html",context)

def SampleUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Sample"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Sample, pk=pk)
    if request.method == 'POST':
        form = SampleForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Sample(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/sosa_app/Sample/list/"
            return HttpResponseRedirect(link)
    else:
        form = SampleForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SampleUpdate.html",context)

def ResultUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Result"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Result, pk=pk)
    if request.method == 'POST':
        form = ResultForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Result(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/sosa_app/Result/list/"
            return HttpResponseRedirect(link)
    else:
        form = ResultForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ResultUpdate.html",context)

def PlatformUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Platform"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Platform, pk=pk)
    if request.method == 'POST':
        form = PlatformForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Platform(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/sosa_app/Platform/list/"
            return HttpResponseRedirect(link)
    else:
        form = PlatformForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "PlatformUpdate.html",context)

def ActuatorUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Actuator"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Actuator, pk=pk)
    if request.method == 'POST':
        form = ActuatorForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Actuator(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/sosa_app/Actuator/list/"
            return HttpResponseRedirect(link)
    else:
        form = ActuatorForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ActuatorUpdate.html",context)

def SamplerUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Sampler"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Sampler, pk=pk)
    if request.method == 'POST':
        form = SamplerForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Sampler(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/sosa_app/Sampler/list/"
            return HttpResponseRedirect(link)
    else:
        form = SamplerForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SamplerUpdate.html",context)

def SensorUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Sensor"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Sensor, pk=pk)
    if request.method == 'POST':
        form = SensorForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Sensor(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/sosa_app/Sensor/list/"
            return HttpResponseRedirect(link)
    else:
        form = SensorForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SensorUpdate.html",context)

def ObservablePropertyUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ObservableProperty"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ObservableProperty, pk=pk)
    if request.method == 'POST':
        form = ObservablePropertyForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ObservableProperty(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/sosa_app/ObservableProperty/list/"
            return HttpResponseRedirect(link)
    else:
        form = ObservablePropertyForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ObservablePropertyUpdate.html",context)
