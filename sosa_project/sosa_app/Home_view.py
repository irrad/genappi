from django.db import models
from django.template.loader import render_to_string
from .models import *
from .forms import *
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseNotFound
from django.template import loader
from django.core.urlresolvers import reverse
import datetime
from .forms import *
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.core.files.storage import FileSystemStorage
from django.utils.safestring import mark_safe

def vowl(request):
    context = {}
    return render(request, "do_visualisation.html", context)

def HomeView(request):
    classes = []
    
    classes.append(str(Agent))
    
    classes.append(str(TemporalEntity))
    
    classes.append(str(Procedure))
    
    classes.append(str(Vocabulary))
    
    classes.append(str(Actuation))
    
    classes.append(str(ActuatableProperty))
    
    classes.append(str(Observation))
    
    classes.append(str(Sampling))
    
    classes.append(str(FeatureOfInterest))
    
    classes.append(str(Sample))
    
    classes.append(str(Result))
    
    classes.append(str(Platform))
    
    classes.append(str(Actuator))
    
    classes.append(str(Sampler))
    
    classes.append(str(Sensor))
    
    classes.append(str(ObservableProperty))
    
    context = {'classes':classes}
    return render(request, "home.html",context)