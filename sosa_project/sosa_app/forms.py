from django import forms
from django.forms import ModelForm
from .models import *


class AgentForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(AgentForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Agent
        exclude = ()

class TemporalEntityForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(TemporalEntityForm, self).__init__(*args, **kwargs)
    class Meta:
        model = TemporalEntity
        exclude = ()

class ProcedureForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ProcedureForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Procedure
        exclude = ()

class VocabularyForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(VocabularyForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Vocabulary
        exclude = ()

class ActuationForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ActuationForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Actuation
        exclude = ()

class ActuatablePropertyForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ActuatablePropertyForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ActuatableProperty
        exclude = ()

class ObservationForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ObservationForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Observation
        exclude = ()

class SamplingForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(SamplingForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Sampling
        exclude = ()

class FeatureOfInterestForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(FeatureOfInterestForm, self).__init__(*args, **kwargs)
    class Meta:
        model = FeatureOfInterest
        exclude = ()

class SampleForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(SampleForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Sample
        exclude = ()

class ResultForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ResultForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Result
        exclude = ()

class PlatformForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(PlatformForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Platform
        exclude = ()

class ActuatorForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ActuatorForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Actuator
        exclude = ()

class SamplerForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(SamplerForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Sampler
        exclude = ()

class SensorForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(SensorForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Sensor
        exclude = ()

class ObservablePropertyForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ObservablePropertyForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ObservableProperty
        exclude = ()
