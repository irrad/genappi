from django.db import models
from django.template.loader import render_to_string
from .models import *
from .forms import *
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseNotFound
from django.template import loader
from django.core.urlresolvers import reverse
import datetime
from .forms import *
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.core.files.storage import FileSystemStorage
from django.utils.safestring import mark_safe
from django.core import serializers


def AgentRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Agent, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"AgentRead.html",{'instance_items': instance_items})

def TemporalEntityRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(TemporalEntity, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"TemporalEntityRead.html",{'instance_items': instance_items})

def ProcedureRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Procedure, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ProcedureRead.html",{'instance_items': instance_items})

def VocabularyRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Vocabulary, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"VocabularyRead.html",{'instance_items': instance_items})

def ActuationRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Actuation, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ActuationRead.html",{'instance_items': instance_items})

def ActuatablePropertyRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ActuatableProperty, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ActuatablePropertyRead.html",{'instance_items': instance_items})

def ObservationRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Observation, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ObservationRead.html",{'instance_items': instance_items})

def SamplingRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Sampling, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"SamplingRead.html",{'instance_items': instance_items})

def FeatureOfInterestRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(FeatureOfInterest, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"FeatureOfInterestRead.html",{'instance_items': instance_items})

def SampleRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Sample, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"SampleRead.html",{'instance_items': instance_items})

def ResultRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Result, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ResultRead.html",{'instance_items': instance_items})

def PlatformRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Platform, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"PlatformRead.html",{'instance_items': instance_items})

def ActuatorRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Actuator, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ActuatorRead.html",{'instance_items': instance_items})

def SamplerRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Sampler, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"SamplerRead.html",{'instance_items': instance_items})

def SensorRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Sensor, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"SensorRead.html",{'instance_items': instance_items})

def ObservablePropertyRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ObservableProperty, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ObservablePropertyRead.html",{'instance_items': instance_items})
