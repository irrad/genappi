from django.conf.urls import url, include
from . import Read_view, Create_view, Update_view, Delete_view, List_view, Home_view, preferences, registration

urlpatterns = [
    url(r'^home/$', Home_view.HomeView, name='HomeView'),

    url(r'^preferences/$', preferences.define_preferences, name='define_preferences'),

    url(r'^signup/$', registration.signup, name='signup'),    

    url(r'^Agent/create/$', Create_view.AgentCreate, name='AgentCreate'),

    url(r'^TemporalEntity/create/$', Create_view.TemporalEntityCreate, name='TemporalEntityCreate'),

    url(r'^Procedure/create/$', Create_view.ProcedureCreate, name='ProcedureCreate'),

    url(r'^Vocabulary/create/$', Create_view.VocabularyCreate, name='VocabularyCreate'),

    url(r'^Actuation/create/$', Create_view.ActuationCreate, name='ActuationCreate'),

    url(r'^ActuatableProperty/create/$', Create_view.ActuatablePropertyCreate, name='ActuatablePropertyCreate'),

    url(r'^Observation/create/$', Create_view.ObservationCreate, name='ObservationCreate'),

    url(r'^Sampling/create/$', Create_view.SamplingCreate, name='SamplingCreate'),

    url(r'^FeatureOfInterest/create/$', Create_view.FeatureOfInterestCreate, name='FeatureOfInterestCreate'),

    url(r'^Sample/create/$', Create_view.SampleCreate, name='SampleCreate'),

    url(r'^Result/create/$', Create_view.ResultCreate, name='ResultCreate'),

    url(r'^Platform/create/$', Create_view.PlatformCreate, name='PlatformCreate'),

    url(r'^Actuator/create/$', Create_view.ActuatorCreate, name='ActuatorCreate'),

    url(r'^Sampler/create/$', Create_view.SamplerCreate, name='SamplerCreate'),

    url(r'^Sensor/create/$', Create_view.SensorCreate, name='SensorCreate'),

    url(r'^ObservableProperty/create/$', Create_view.ObservablePropertyCreate, name='ObservablePropertyCreate'),


    url(r'^Agent/(?P<pk>\d+)/read/$', Read_view.AgentRead, name='AgentRead'),

    url(r'^TemporalEntity/(?P<pk>\d+)/read/$', Read_view.TemporalEntityRead, name='TemporalEntityRead'),

    url(r'^Procedure/(?P<pk>\d+)/read/$', Read_view.ProcedureRead, name='ProcedureRead'),

    url(r'^Vocabulary/(?P<pk>\d+)/read/$', Read_view.VocabularyRead, name='VocabularyRead'),

    url(r'^Actuation/(?P<pk>\d+)/read/$', Read_view.ActuationRead, name='ActuationRead'),

    url(r'^ActuatableProperty/(?P<pk>\d+)/read/$', Read_view.ActuatablePropertyRead, name='ActuatablePropertyRead'),

    url(r'^Observation/(?P<pk>\d+)/read/$', Read_view.ObservationRead, name='ObservationRead'),

    url(r'^Sampling/(?P<pk>\d+)/read/$', Read_view.SamplingRead, name='SamplingRead'),

    url(r'^FeatureOfInterest/(?P<pk>\d+)/read/$', Read_view.FeatureOfInterestRead, name='FeatureOfInterestRead'),

    url(r'^Sample/(?P<pk>\d+)/read/$', Read_view.SampleRead, name='SampleRead'),

    url(r'^Result/(?P<pk>\d+)/read/$', Read_view.ResultRead, name='ResultRead'),

    url(r'^Platform/(?P<pk>\d+)/read/$', Read_view.PlatformRead, name='PlatformRead'),

    url(r'^Actuator/(?P<pk>\d+)/read/$', Read_view.ActuatorRead, name='ActuatorRead'),

    url(r'^Sampler/(?P<pk>\d+)/read/$', Read_view.SamplerRead, name='SamplerRead'),

    url(r'^Sensor/(?P<pk>\d+)/read/$', Read_view.SensorRead, name='SensorRead'),

    url(r'^ObservableProperty/(?P<pk>\d+)/read/$', Read_view.ObservablePropertyRead, name='ObservablePropertyRead'),


    url(r'^Agent/list/$', List_view.AgentList, name='AgentList'),

    url(r'^TemporalEntity/list/$', List_view.TemporalEntityList, name='TemporalEntityList'),

    url(r'^Procedure/list/$', List_view.ProcedureList, name='ProcedureList'),

    url(r'^Vocabulary/list/$', List_view.VocabularyList, name='VocabularyList'),

    url(r'^Actuation/list/$', List_view.ActuationList, name='ActuationList'),

    url(r'^ActuatableProperty/list/$', List_view.ActuatablePropertyList, name='ActuatablePropertyList'),

    url(r'^Observation/list/$', List_view.ObservationList, name='ObservationList'),

    url(r'^Sampling/list/$', List_view.SamplingList, name='SamplingList'),

    url(r'^FeatureOfInterest/list/$', List_view.FeatureOfInterestList, name='FeatureOfInterestList'),

    url(r'^Sample/list/$', List_view.SampleList, name='SampleList'),

    url(r'^Result/list/$', List_view.ResultList, name='ResultList'),

    url(r'^Platform/list/$', List_view.PlatformList, name='PlatformList'),

    url(r'^Actuator/list/$', List_view.ActuatorList, name='ActuatorList'),

    url(r'^Sampler/list/$', List_view.SamplerList, name='SamplerList'),

    url(r'^Sensor/list/$', List_view.SensorList, name='SensorList'),

    url(r'^ObservableProperty/list/$', List_view.ObservablePropertyList, name='ObservablePropertyList'),


    url(r'^Agent/(?P<pk>\d+)/update/$', Update_view.AgentUpdate, name='AgentUpdate'),

    url(r'^TemporalEntity/(?P<pk>\d+)/update/$', Update_view.TemporalEntityUpdate, name='TemporalEntityUpdate'),

    url(r'^Procedure/(?P<pk>\d+)/update/$', Update_view.ProcedureUpdate, name='ProcedureUpdate'),

    url(r'^Vocabulary/(?P<pk>\d+)/update/$', Update_view.VocabularyUpdate, name='VocabularyUpdate'),

    url(r'^Actuation/(?P<pk>\d+)/update/$', Update_view.ActuationUpdate, name='ActuationUpdate'),

    url(r'^ActuatableProperty/(?P<pk>\d+)/update/$', Update_view.ActuatablePropertyUpdate, name='ActuatablePropertyUpdate'),

    url(r'^Observation/(?P<pk>\d+)/update/$', Update_view.ObservationUpdate, name='ObservationUpdate'),

    url(r'^Sampling/(?P<pk>\d+)/update/$', Update_view.SamplingUpdate, name='SamplingUpdate'),

    url(r'^FeatureOfInterest/(?P<pk>\d+)/update/$', Update_view.FeatureOfInterestUpdate, name='FeatureOfInterestUpdate'),

    url(r'^Sample/(?P<pk>\d+)/update/$', Update_view.SampleUpdate, name='SampleUpdate'),

    url(r'^Result/(?P<pk>\d+)/update/$', Update_view.ResultUpdate, name='ResultUpdate'),

    url(r'^Platform/(?P<pk>\d+)/update/$', Update_view.PlatformUpdate, name='PlatformUpdate'),

    url(r'^Actuator/(?P<pk>\d+)/update/$', Update_view.ActuatorUpdate, name='ActuatorUpdate'),

    url(r'^Sampler/(?P<pk>\d+)/update/$', Update_view.SamplerUpdate, name='SamplerUpdate'),

    url(r'^Sensor/(?P<pk>\d+)/update/$', Update_view.SensorUpdate, name='SensorUpdate'),

    url(r'^ObservableProperty/(?P<pk>\d+)/update/$', Update_view.ObservablePropertyUpdate, name='ObservablePropertyUpdate'),


    url(r'^Agent/(?P<pk>\d+)/delete/$', Delete_view.AgentDelete, name='AgentDelete'),

    url(r'^TemporalEntity/(?P<pk>\d+)/delete/$', Delete_view.TemporalEntityDelete, name='TemporalEntityDelete'),

    url(r'^Procedure/(?P<pk>\d+)/delete/$', Delete_view.ProcedureDelete, name='ProcedureDelete'),

    url(r'^Vocabulary/(?P<pk>\d+)/delete/$', Delete_view.VocabularyDelete, name='VocabularyDelete'),

    url(r'^Actuation/(?P<pk>\d+)/delete/$', Delete_view.ActuationDelete, name='ActuationDelete'),

    url(r'^ActuatableProperty/(?P<pk>\d+)/delete/$', Delete_view.ActuatablePropertyDelete, name='ActuatablePropertyDelete'),

    url(r'^Observation/(?P<pk>\d+)/delete/$', Delete_view.ObservationDelete, name='ObservationDelete'),

    url(r'^Sampling/(?P<pk>\d+)/delete/$', Delete_view.SamplingDelete, name='SamplingDelete'),

    url(r'^FeatureOfInterest/(?P<pk>\d+)/delete/$', Delete_view.FeatureOfInterestDelete, name='FeatureOfInterestDelete'),

    url(r'^Sample/(?P<pk>\d+)/delete/$', Delete_view.SampleDelete, name='SampleDelete'),

    url(r'^Result/(?P<pk>\d+)/delete/$', Delete_view.ResultDelete, name='ResultDelete'),

    url(r'^Platform/(?P<pk>\d+)/delete/$', Delete_view.PlatformDelete, name='PlatformDelete'),

    url(r'^Actuator/(?P<pk>\d+)/delete/$', Delete_view.ActuatorDelete, name='ActuatorDelete'),

    url(r'^Sampler/(?P<pk>\d+)/delete/$', Delete_view.SamplerDelete, name='SamplerDelete'),

    url(r'^Sensor/(?P<pk>\d+)/delete/$', Delete_view.SensorDelete, name='SensorDelete'),

    url(r'^ObservableProperty/(?P<pk>\d+)/delete/$', Delete_view.ObservablePropertyDelete, name='ObservablePropertyDelete'),

    ]