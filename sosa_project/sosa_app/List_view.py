from django.db import models
from django.template.loader import render_to_string
from .models import *
from .forms import *
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseNotFound
from django.template import loader
from django.core.urlresolvers import reverse
import datetime
from .forms import *
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.core.files.storage import FileSystemStorage
from django.utils.safestring import mark_safe
from django.core import serializers


def AgentList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Agent.objects.all())
    fields = Agent._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"AgentList.html",{'data':data, 'fields':new_fields})

def TemporalEntityList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", TemporalEntity.objects.all())
    fields = TemporalEntity._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"TemporalEntityList.html",{'data':data, 'fields':new_fields})

def ProcedureList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Procedure.objects.all())
    fields = Procedure._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ProcedureList.html",{'data':data, 'fields':new_fields})

def VocabularyList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Vocabulary.objects.all())
    fields = Vocabulary._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"VocabularyList.html",{'data':data, 'fields':new_fields})

def ActuationList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Actuation.objects.all())
    fields = Actuation._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ActuationList.html",{'data':data, 'fields':new_fields})

def ActuatablePropertyList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ActuatableProperty.objects.all())
    fields = ActuatableProperty._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ActuatablePropertyList.html",{'data':data, 'fields':new_fields})

def ObservationList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Observation.objects.all())
    fields = Observation._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ObservationList.html",{'data':data, 'fields':new_fields})

def SamplingList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Sampling.objects.all())
    fields = Sampling._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"SamplingList.html",{'data':data, 'fields':new_fields})

def FeatureOfInterestList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", FeatureOfInterest.objects.all())
    fields = FeatureOfInterest._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"FeatureOfInterestList.html",{'data':data, 'fields':new_fields})

def SampleList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Sample.objects.all())
    fields = Sample._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"SampleList.html",{'data':data, 'fields':new_fields})

def ResultList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Result.objects.all())
    fields = Result._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ResultList.html",{'data':data, 'fields':new_fields})

def PlatformList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Platform.objects.all())
    fields = Platform._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"PlatformList.html",{'data':data, 'fields':new_fields})

def ActuatorList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Actuator.objects.all())
    fields = Actuator._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ActuatorList.html",{'data':data, 'fields':new_fields})

def SamplerList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Sampler.objects.all())
    fields = Sampler._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"SamplerList.html",{'data':data, 'fields':new_fields})

def SensorList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Sensor.objects.all())
    fields = Sensor._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"SensorList.html",{'data':data, 'fields':new_fields})

def ObservablePropertyList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ObservableProperty.objects.all())
    fields = ObservableProperty._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ObservablePropertyList.html",{'data':data, 'fields':new_fields})
