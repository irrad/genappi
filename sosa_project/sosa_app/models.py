from django.db import models

class Agent(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class TemporalEntity(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Procedure(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Vocabulary(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Actuation(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ActuatableProperty(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Observation(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Sampling(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class FeatureOfInterest(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Sample(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Result(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Platform(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Actuator(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Sampler(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Sensor(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ObservableProperty(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name
