from owlready2 import *
from django.db import models
from django.template.loader import render_to_string
from .models import *
from .forms import *
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseNotFound
from django.template import loader
from django.core.urlresolvers import reverse
import datetime
from .forms import *
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.core.files.storage import FileSystemStorage
from django.utils.safestring import mark_safe
from django.conf import settings

onto_path.append(settings.BASE_DIR)
onto = get_ontology("raw.githubusercontent.com/w3c/sdw/gh-pages/ssn/integrated/sosa.rdf").load()
ontoclasses = onto.classes()
namespaces = dict() 
for clas in ontoclasses:
    namespaces[clas.name] = clas.namespace


def AgentCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Agent"]

    

    if request.method == 'POST':
        form = AgentForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Agent(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/sosa_app/Agent/list/"
            return HttpResponseRedirect(link)
    else:
        form = AgentForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "AgentCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def TemporalEntityCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["TemporalEntity"]

    

    if request.method == 'POST':
        form = TemporalEntityForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.TemporalEntity(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/sosa_app/TemporalEntity/list/"
            return HttpResponseRedirect(link)
    else:
        form = TemporalEntityForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "TemporalEntityCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ProcedureCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Procedure"]

    

    if request.method == 'POST':
        form = ProcedureForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Procedure(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/sosa_app/Procedure/list/"
            return HttpResponseRedirect(link)
    else:
        form = ProcedureForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ProcedureCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def VocabularyCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Vocabulary"]

    

    if request.method == 'POST':
        form = VocabularyForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Vocabulary(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/sosa_app/Vocabulary/list/"
            return HttpResponseRedirect(link)
    else:
        form = VocabularyForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "VocabularyCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ActuationCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Actuation"]

    

    if request.method == 'POST':
        form = ActuationForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Actuation(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/sosa_app/Actuation/list/"
            return HttpResponseRedirect(link)
    else:
        form = ActuationForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ActuationCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ActuatablePropertyCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ActuatableProperty"]

    

    if request.method == 'POST':
        form = ActuatablePropertyForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ActuatableProperty(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/sosa_app/ActuatableProperty/list/"
            return HttpResponseRedirect(link)
    else:
        form = ActuatablePropertyForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ActuatablePropertyCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ObservationCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Observation"]

    

    if request.method == 'POST':
        form = ObservationForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Observation(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/sosa_app/Observation/list/"
            return HttpResponseRedirect(link)
    else:
        form = ObservationForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ObservationCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def SamplingCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Sampling"]

    

    if request.method == 'POST':
        form = SamplingForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Sampling(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/sosa_app/Sampling/list/"
            return HttpResponseRedirect(link)
    else:
        form = SamplingForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "SamplingCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def FeatureOfInterestCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["FeatureOfInterest"]

    

    if request.method == 'POST':
        form = FeatureOfInterestForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.FeatureOfInterest(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/sosa_app/FeatureOfInterest/list/"
            return HttpResponseRedirect(link)
    else:
        form = FeatureOfInterestForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "FeatureOfInterestCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def SampleCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Sample"]

    

    if request.method == 'POST':
        form = SampleForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Sample(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/sosa_app/Sample/list/"
            return HttpResponseRedirect(link)
    else:
        form = SampleForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "SampleCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ResultCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Result"]

    

    if request.method == 'POST':
        form = ResultForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Result(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/sosa_app/Result/list/"
            return HttpResponseRedirect(link)
    else:
        form = ResultForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ResultCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def PlatformCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Platform"]

    

    if request.method == 'POST':
        form = PlatformForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Platform(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/sosa_app/Platform/list/"
            return HttpResponseRedirect(link)
    else:
        form = PlatformForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "PlatformCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ActuatorCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Actuator"]

    

    if request.method == 'POST':
        form = ActuatorForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Actuator(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/sosa_app/Actuator/list/"
            return HttpResponseRedirect(link)
    else:
        form = ActuatorForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ActuatorCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def SamplerCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Sampler"]

    

    if request.method == 'POST':
        form = SamplerForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Sampler(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/sosa_app/Sampler/list/"
            return HttpResponseRedirect(link)
    else:
        form = SamplerForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "SamplerCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def SensorCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Sensor"]

    

    if request.method == 'POST':
        form = SensorForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Sensor(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/sosa_app/Sensor/list/"
            return HttpResponseRedirect(link)
    else:
        form = SensorForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "SensorCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ObservablePropertyCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ObservableProperty"]

    

    if request.method == 'POST':
        form = ObservablePropertyForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ObservableProperty(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/sosa_app/ObservableProperty/list/"
            return HttpResponseRedirect(link)
    else:
        form = ObservablePropertyForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ObservablePropertyCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})
