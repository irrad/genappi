from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    # Examples:
    # url(r'^$', 'sosa_project.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^vowl', 'sosa_app.Home_view.vowl', name='vowl'),
    url(r'^sosa_app/', include('sosa_app.urls', namespace='sosa_app')),
    url(r'^$', 'sosa_app.Home_view.HomeView', name='HomeView'),    url(r'^accounts/',include('django.contrib.auth.urls')),]
