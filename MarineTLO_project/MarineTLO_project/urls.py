from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    # Examples:
    # url(r'^$', 'MarineTLO_project.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^vowl', 'MarineTLO_app.Home_view.vowl', name='vowl'),
    url(r'^MarineTLO_app/', include('MarineTLO_app.urls', namespace='MarineTLO_app')),
    url(r'^$', 'MarineTLO_app.Home_view.HomeView', name='HomeView'),    url(r'^accounts/',include('django.contrib.auth.urls')),]
