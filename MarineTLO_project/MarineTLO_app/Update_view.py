from owlready2 import *
from django.db import models
from django.template.loader import render_to_string
from .models import *
from .forms import *
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseNotFound
from django.template import loader
from django.core.urlresolvers import reverse
import datetime
from .forms import *
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.core.files.storage import FileSystemStorage
from django.utils.safestring import mark_safe
from django.conf import settings


def BC35_Physical_ThingUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC35_Physical_Thing, pk=pk)
    if request.method == 'POST':
        form = BC35_Physical_ThingForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC35_Physical_Thing(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC35_Physical_Thing/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC35_Physical_ThingForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC35_Physical_ThingUpdate.html",context)

def BC10_EventUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC10_Event, pk=pk)
    if request.method == 'POST':
        form = BC10_EventForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC10_Event(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC10_Event/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC10_EventForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC10_EventUpdate.html",context)

def BC8_ActorUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC8_Actor, pk=pk)
    if request.method == 'POST':
        form = BC8_ActorForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC8_Actor(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC8_Actor/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC8_ActorForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC8_ActorUpdate.html",context)

def BC34_Geometric_Place_ExpressionUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC34_Geometric_Place_Expression, pk=pk)
    if request.method == 'POST':
        form = BC34_Geometric_Place_ExpressionForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC34_Geometric_Place_Expression(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC34_Geometric_Place_Expression/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC34_Geometric_Place_ExpressionForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC34_Geometric_Place_ExpressionUpdate.html",context)

def BC29_Spatial_Coordinate_Reference_SystemUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC29_Spatial_Coordinate_Reference_System, pk=pk)
    if request.method == 'POST':
        form = BC29_Spatial_Coordinate_Reference_SystemForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC29_Spatial_Coordinate_Reference_System(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC29_Spatial_Coordinate_Reference_System/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC29_Spatial_Coordinate_Reference_SystemForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC29_Spatial_Coordinate_Reference_SystemUpdate.html",context)

def BC56_Digital_Measurement_EventUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC56_Digital_Measurement_Event, pk=pk)
    if request.method == 'POST':
        form = BC56_Digital_Measurement_EventForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC56_Digital_Measurement_Event(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC56_Digital_Measurement_Event/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC56_Digital_Measurement_EventForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC56_Digital_Measurement_EventUpdate.html",context)

def BC23_Digital_ObjectUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC23_Digital_Object, pk=pk)
    if request.method == 'POST':
        form = BC23_Digital_ObjectForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC23_Digital_Object(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC23_Digital_Object/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC23_Digital_ObjectForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC23_Digital_ObjectUpdate.html",context)

def BC18_PropositionUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC18_Proposition, pk=pk)
    if request.method == 'POST':
        form = BC18_PropositionForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC18_Proposition(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC18_Proposition/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC18_PropositionForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC18_PropositionUpdate.html",context)

def BC59_SoftwareUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC59_Software, pk=pk)
    if request.method == 'POST':
        form = BC59_SoftwareForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC59_Software(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC59_Software/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC59_SoftwareForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC59_SoftwareUpdate.html",context)

def BC58_Digital_DeviceUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC58_Digital_Device, pk=pk)
    if request.method == 'POST':
        form = BC58_Digital_DeviceForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC58_Digital_Device(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC58_Digital_Device/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC58_Digital_DeviceForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC58_Digital_DeviceUpdate.html",context)

def BC61_Capture_ActivityUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC61_Capture_Activity, pk=pk)
    if request.method == 'POST':
        form = BC61_Capture_ActivityForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC61_Capture_Activity(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC61_Capture_Activity/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC61_Capture_ActivityForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC61_Capture_ActivityUpdate.html",context)

def BC57_CaptureUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC57_Capture, pk=pk)
    if request.method == 'POST':
        form = BC57_CaptureForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC57_Capture(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC57_Capture/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC57_CaptureForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC57_CaptureUpdate.html",context)

def BC54_MeasurementUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC54_Measurement, pk=pk)
    if request.method == 'POST':
        form = BC54_MeasurementForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC54_Measurement(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC54_Measurement/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC54_MeasurementForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC54_MeasurementUpdate.html",context)

def BC44_Attribute_AssignmentUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC44_Attribute_Assignment, pk=pk)
    if request.method == 'POST':
        form = BC44_Attribute_AssignmentForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC44_Attribute_Assignment(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC44_Attribute_Assignment/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC44_Attribute_AssignmentForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC44_Attribute_AssignmentUpdate.html",context)

def BC1_TLO_EntityUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC1_TLO_Entity, pk=pk)
    if request.method == 'POST':
        form = BC1_TLO_EntityForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC1_TLO_Entity(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC1_TLO_Entity/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC1_TLO_EntityForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC1_TLO_EntityUpdate.html",context)

def BC43_ActivityUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC43_Activity, pk=pk)
    if request.method == 'POST':
        form = BC43_ActivityForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC43_Activity(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC43_Activity/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC43_ActivityForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC43_ActivityUpdate.html",context)

def BC4_Temporal_PhenomenonUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC4_Temporal_Phenomenon, pk=pk)
    if request.method == 'POST':
        form = BC4_Temporal_PhenomenonForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC4_Temporal_Phenomenon(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC4_Temporal_Phenomenon/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC4_Temporal_PhenomenonForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC4_Temporal_PhenomenonUpdate.html",context)

def BC3_PlaceUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC3_Place, pk=pk)
    if request.method == 'POST':
        form = BC3_PlaceForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC3_Place(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC3_Place/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC3_PlaceForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC3_PlaceUpdate.html",context)

def BC45_ObservationUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC45_Observation, pk=pk)
    if request.method == 'POST':
        form = BC45_ObservationForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC45_Observation(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC45_Observation/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC45_ObservationForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC45_ObservationUpdate.html",context)

def BC9_Observable_EntityUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC9_Observable_Entity, pk=pk)
    if request.method == 'POST':
        form = BC9_Observable_EntityForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC9_Observable_Entity(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC9_Observable_Entity/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC9_Observable_EntityForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC9_Observable_EntityUpdate.html",context)

def BC7_ThingUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC7_Thing, pk=pk)
    if request.method == 'POST':
        form = BC7_ThingForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC7_Thing(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC7_Thing/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC7_ThingForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC7_ThingUpdate.html",context)

def BC5_DimensionUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC5_Dimension, pk=pk)
    if request.method == 'POST':
        form = BC5_DimensionForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC5_Dimension(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC5_Dimension/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC5_DimensionForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC5_DimensionUpdate.html",context)

def BC30_AppellationUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC30_Appellation, pk=pk)
    if request.method == 'POST':
        form = BC30_AppellationForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC30_Appellation(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC30_Appellation/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC30_AppellationForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC30_AppellationUpdate.html",context)

def BC55_Measurement_UnitUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC55_Measurement_Unit, pk=pk)
    if request.method == 'POST':
        form = BC55_Measurement_UnitForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC55_Measurement_Unit(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC55_Measurement_Unit/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC55_Measurement_UnitForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC55_Measurement_UnitUpdate.html",context)

def BC20_Declarative_PlaceUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC20_Declarative_Place, pk=pk)
    if request.method == 'POST':
        form = BC20_Declarative_PlaceForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC20_Declarative_Place(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC20_Declarative_Place/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC20_Declarative_PlaceForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC20_Declarative_PlaceUpdate.html",context)

def BC72_SpaceTime_VolumeUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC72_SpaceTime_Volume, pk=pk)
    if request.method == 'POST':
        form = BC72_SpaceTime_VolumeForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC72_SpaceTime_Volume(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC72_SpaceTime_Volume/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC72_SpaceTime_VolumeForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC72_SpaceTime_VolumeUpdate.html",context)

def BC2_Time_SpanUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC2_Time_Span, pk=pk)
    if request.method == 'POST':
        form = BC2_Time_SpanForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC2_Time_Span(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC2_Time_Span/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC2_Time_SpanForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC2_Time_SpanUpdate.html",context)

def BT14_Appellation_TypeUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT14_Appellation_Type, pk=pk)
    if request.method == 'POST':
        form = BT14_Appellation_TypeForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BT14_Appellation_Type(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BT14_Appellation_Type/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT14_Appellation_TypeForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT14_Appellation_TypeUpdate.html",context)

def BT2_Temporal_Phenomenon_TypeUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT2_Temporal_Phenomenon_Type, pk=pk)
    if request.method == 'POST':
        form = BT2_Temporal_Phenomenon_TypeForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BT2_Temporal_Phenomenon_Type(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BT2_Temporal_Phenomenon_Type/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT2_Temporal_Phenomenon_TypeForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT2_Temporal_Phenomenon_TypeUpdate.html",context)

def BT10_Biotic_Element_TypeUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT10_Biotic_Element_Type, pk=pk)
    if request.method == 'POST':
        form = BT10_Biotic_Element_TypeForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BT10_Biotic_Element_Type(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BT10_Biotic_Element_Type/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT10_Biotic_Element_TypeForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT10_Biotic_Element_TypeUpdate.html",context)

def BT7_Ecosystem_TypeUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT7_Ecosystem_Type, pk=pk)
    if request.method == 'POST':
        form = BT7_Ecosystem_TypeForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BT7_Ecosystem_Type(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BT7_Ecosystem_Type/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT7_Ecosystem_TypeForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT7_Ecosystem_TypeUpdate.html",context)

def BT4_Conceptual_Object_TypeUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT4_Conceptual_Object_Type, pk=pk)
    if request.method == 'POST':
        form = BT4_Conceptual_Object_TypeForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BT4_Conceptual_Object_Type(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BT4_Conceptual_Object_Type/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT4_Conceptual_Object_TypeForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT4_Conceptual_Object_TypeUpdate.html",context)

def BT1_TLO_Entity_TypeUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT1_TLO_Entity_Type, pk=pk)
    if request.method == 'POST':
        form = BT1_TLO_Entity_TypeForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BT1_TLO_Entity_Type(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BT1_TLO_Entity_Type/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT1_TLO_Entity_TypeForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT1_TLO_Entity_TypeUpdate.html",context)

def BT17_Human_Activity_TypeUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT17_Human_Activity_Type, pk=pk)
    if request.method == 'POST':
        form = BT17_Human_Activity_TypeForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BT17_Human_Activity_Type(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BT17_Human_Activity_Type/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT17_Human_Activity_TypeForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT17_Human_Activity_TypeUpdate.html",context)

def BT9_Actor_TypeUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT9_Actor_Type, pk=pk)
    if request.method == 'POST':
        form = BT9_Actor_TypeForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BT9_Actor_Type(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BT9_Actor_Type/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT9_Actor_TypeForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT9_Actor_TypeUpdate.html",context)

def BT32_Persistent_TypeUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT32_Persistent_Type, pk=pk)
    if request.method == 'POST':
        form = BT32_Persistent_TypeForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BT32_Persistent_Type(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BT32_Persistent_Type/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT32_Persistent_TypeForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT32_Persistent_TypeUpdate.html",context)

def BC32_IdentifierUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC32_Identifier, pk=pk)
    if request.method == 'POST':
        form = BC32_IdentifierForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC32_Identifier(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC32_Identifier/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC32_IdentifierForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC32_IdentifierUpdate.html",context)

def BC64_Design_or_ProcedureUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC64_Design_or_Procedure, pk=pk)
    if request.method == 'POST':
        form = BC64_Design_or_ProcedureForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC64_Design_or_Procedure(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC64_Design_or_Procedure/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC64_Design_or_ProcedureForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC64_Design_or_ProcedureUpdate.html",context)

def BC79_RightUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC79_Right, pk=pk)
    if request.method == 'POST':
        form = BC79_RightForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC79_Right(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC79_Right/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC79_RightForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC79_RightUpdate.html",context)

def BC6_Persistent_ItemUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC6_Persistent_Item, pk=pk)
    if request.method == 'POST':
        form = BC6_Persistent_ItemForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC6_Persistent_Item(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC6_Persistent_Item/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC6_Persistent_ItemForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC6_Persistent_ItemUpdate.html",context)

def BT3_Physical_Thing_TypeUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT3_Physical_Thing_Type, pk=pk)
    if request.method == 'POST':
        form = BT3_Physical_Thing_TypeForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BT3_Physical_Thing_Type(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BT3_Physical_Thing_Type/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT3_Physical_Thing_TypeForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT3_Physical_Thing_TypeUpdate.html",context)

def BC17_Conceptual_ObjectUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC17_Conceptual_Object, pk=pk)
    if request.method == 'POST':
        form = BC17_Conceptual_ObjectForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC17_Conceptual_Object(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC17_Conceptual_Object/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC17_Conceptual_ObjectForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC17_Conceptual_ObjectUpdate.html",context)

def BT32_Persisten_TypeUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT32_Persisten_Type, pk=pk)
    if request.method == 'POST':
        form = BT32_Persisten_TypeForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BT32_Persisten_Type(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BT32_Persisten_Type/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT32_Persisten_TypeForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT32_Persisten_TypeUpdate.html",context)

def BC39_Marine_AnimalUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC39_Marine_Animal, pk=pk)
    if request.method == 'POST':
        form = BC39_Marine_AnimalForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC39_Marine_Animal(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC39_Marine_Animal/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC39_Marine_AnimalForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC39_Marine_AnimalUpdate.html",context)

def BC40_Physical_ManMade_ThingUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC40_Physical_ManMade_Thing, pk=pk)
    if request.method == 'POST':
        form = BC40_Physical_ManMade_ThingForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC40_Physical_ManMade_Thing(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC40_Physical_ManMade_Thing/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC40_Physical_ManMade_ThingForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC40_Physical_ManMade_ThingUpdate.html",context)

def BC41_ManMade_ObjectUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC41_ManMade_Object, pk=pk)
    if request.method == 'POST':
        form = BC41_ManMade_ObjectForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC41_ManMade_Object(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC41_ManMade_Object/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC41_ManMade_ObjectForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC41_ManMade_ObjectUpdate.html",context)

def BC42_CollectionUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC42_Collection, pk=pk)
    if request.method == 'POST':
        form = BC42_CollectionForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC42_Collection(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC42_Collection/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC42_CollectionForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC42_CollectionUpdate.html",context)

def BC46_Identifier_AssignmentUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC46_Identifier_Assignment, pk=pk)
    if request.method == 'POST':
        form = BC46_Identifier_AssignmentForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC46_Identifier_Assignment(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC46_Identifier_Assignment/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC46_Identifier_AssignmentForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC46_Identifier_AssignmentUpdate.html",context)

def BC47_ImageUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC47_Image, pk=pk)
    if request.method == 'POST':
        form = BC47_ImageForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC47_Image(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC47_Image/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC47_ImageForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC47_ImageUpdate.html",context)

def BC48_DatabaseUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC48_Database, pk=pk)
    if request.method == 'POST':
        form = BC48_DatabaseForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC48_Database(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC48_Database/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC48_DatabaseForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC48_DatabaseUpdate.html",context)

def BC53_SpecimenUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC53_Specimen, pk=pk)
    if request.method == 'POST':
        form = BC53_SpecimenForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC53_Specimen(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC53_Specimen/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC53_SpecimenForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC53_SpecimenUpdate.html",context)

def BC60_Software_ExecutionUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC60_Software_Execution, pk=pk)
    if request.method == 'POST':
        form = BC60_Software_ExecutionForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC60_Software_Execution(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC60_Software_Execution/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC60_Software_ExecutionForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC60_Software_ExecutionUpdate.html",context)

def BC62_Statistic_IndicatorUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC62_Statistic_Indicator, pk=pk)
    if request.method == 'POST':
        form = BC62_Statistic_IndicatorForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC62_Statistic_Indicator(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC62_Statistic_Indicator/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC62_Statistic_IndicatorForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC62_Statistic_IndicatorUpdate.html",context)

def BC63_Global_Statistic_LandingUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC63_Global_Statistic_Landing, pk=pk)
    if request.method == 'POST':
        form = BC63_Global_Statistic_LandingForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC63_Global_Statistic_Landing(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC63_Global_Statistic_Landing/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC63_Global_Statistic_LandingForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC63_Global_Statistic_LandingUpdate.html",context)

def BC70_GroupUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC70_Group, pk=pk)
    if request.method == 'POST':
        form = BC70_GroupForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC70_Group(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC70_Group/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC70_GroupForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC70_GroupUpdate.html",context)

def BC71_Name_Use_ActivityUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC71_Name_Use_Activity, pk=pk)
    if request.method == 'POST':
        form = BC71_Name_Use_ActivityForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC71_Name_Use_Activity(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC71_Name_Use_Activity/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC71_Name_Use_ActivityForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC71_Name_Use_ActivityUpdate.html",context)

def BC75_StockUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC75_Stock, pk=pk)
    if request.method == 'POST':
        form = BC75_StockForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC75_Stock(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC75_Stock/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC75_StockForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC75_StockUpdate.html",context)

def BC76_Stock_FormationUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC76_Stock_Formation, pk=pk)
    if request.method == 'POST':
        form = BC76_Stock_FormationForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC76_Stock_Formation(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC76_Stock_Formation/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC76_Stock_FormationForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC76_Stock_FormationUpdate.html",context)

def BC77_Matter_RemovalUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC77_Matter_Removal, pk=pk)
    if request.method == 'POST':
        form = BC77_Matter_RemovalForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC77_Matter_Removal(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC77_Matter_Removal/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC77_Matter_RemovalForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC77_Matter_RemovalUpdate.html",context)

def BC78_Amount_of_MatterUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC78_Amount_of_Matter, pk=pk)
    if request.method == 'POST':
        form = BC78_Amount_of_MatterForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC78_Amount_of_Matter(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC78_Amount_of_Matter/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC78_Amount_of_MatterForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC78_Amount_of_MatterUpdate.html",context)

def BT11_Equipment_TypeUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT11_Equipment_Type, pk=pk)
    if request.method == 'POST':
        form = BT11_Equipment_TypeForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BT11_Equipment_Type(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BT11_Equipment_Type/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT11_Equipment_TypeForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT11_Equipment_TypeUpdate.html",context)

def BT12_Scientific_Data_TypeUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT12_Scientific_Data_Type, pk=pk)
    if request.method == 'POST':
        form = BT12_Scientific_Data_TypeForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BT12_Scientific_Data_Type(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BT12_Scientific_Data_Type/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT12_Scientific_Data_TypeForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT12_Scientific_Data_TypeUpdate.html",context)

def BT13_Digital_Object_TypeUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT13_Digital_Object_Type, pk=pk)
    if request.method == 'POST':
        form = BT13_Digital_Object_TypeForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BT13_Digital_Object_Type(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BT13_Digital_Object_Type/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT13_Digital_Object_TypeForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT13_Digital_Object_TypeUpdate.html",context)

def BT16_Visual_TypeUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT16_Visual_Type, pk=pk)
    if request.method == 'POST':
        form = BT16_Visual_TypeForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BT16_Visual_Type(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BT16_Visual_Type/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT16_Visual_TypeForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT16_Visual_TypeUpdate.html",context)

def BT6_Human_Event_TypeUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT6_Human_Event_Type, pk=pk)
    if request.method == 'POST':
        form = BT6_Human_Event_TypeForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BT6_Human_Event_Type(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BT6_Human_Event_Type/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT6_Human_Event_TypeForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT6_Human_Event_TypeUpdate.html",context)

def BT18_KingdomUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT18_Kingdom, pk=pk)
    if request.method == 'POST':
        form = BT18_KingdomForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BT18_Kingdom(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BT18_Kingdom/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT18_KingdomForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT18_KingdomUpdate.html",context)

def BT19_PhylumUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT19_Phylum, pk=pk)
    if request.method == 'POST':
        form = BT19_PhylumForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BT19_Phylum(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BT19_Phylum/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT19_PhylumForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT19_PhylumUpdate.html",context)

def BT20_SubPhylumUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT20_SubPhylum, pk=pk)
    if request.method == 'POST':
        form = BT20_SubPhylumForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BT20_SubPhylum(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BT20_SubPhylum/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT20_SubPhylumForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT20_SubPhylumUpdate.html",context)

def BT21_SuperClassUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT21_SuperClass, pk=pk)
    if request.method == 'POST':
        form = BT21_SuperClassForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BT21_SuperClass(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BT21_SuperClass/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT21_SuperClassForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT21_SuperClassUpdate.html",context)

def BT35_Property_TypeUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT35_Property_Type, pk=pk)
    if request.method == 'POST':
        form = BT35_Property_TypeForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BT35_Property_Type(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BT35_Property_Type/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT35_Property_TypeForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT35_Property_TypeUpdate.html",context)

def BC25_Linguistic_ObjectUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC25_Linguistic_Object, pk=pk)
    if request.method == 'POST':
        form = BC25_Linguistic_ObjectForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC25_Linguistic_Object(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC25_Linguistic_Object/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC25_Linguistic_ObjectForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC25_Linguistic_ObjectUpdate.html",context)

def BT36_LanguageUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT36_Language, pk=pk)
    if request.method == 'POST':
        form = BT36_LanguageForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BT36_Language(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BT36_Language/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT36_LanguageForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT36_LanguageUpdate.html",context)

def BC11_PersonUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC11_Person, pk=pk)
    if request.method == 'POST':
        form = BC11_PersonForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC11_Person(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC11_Person/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC11_PersonForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC11_PersonUpdate.html",context)

def BC12_EcosystemUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC12_Ecosystem, pk=pk)
    if request.method == 'POST':
        form = BC12_EcosystemForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC12_Ecosystem(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC12_Ecosystem/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC12_EcosystemForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC12_EcosystemUpdate.html",context)

def BC13_OrganizationUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC13_Organization, pk=pk)
    if request.method == 'POST':
        form = BC13_OrganizationForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC13_Organization(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC13_Organization/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC13_OrganizationForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC13_OrganizationUpdate.html",context)

def BC14_Ecosystem_EnvironmentUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC14_Ecosystem_Environment, pk=pk)
    if request.method == 'POST':
        form = BC14_Ecosystem_EnvironmentForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC14_Ecosystem_Environment(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC14_Ecosystem_Environment/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC14_Ecosystem_EnvironmentForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC14_Ecosystem_EnvironmentUpdate.html",context)

def BC15_Water_AreaUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC15_Water_Area, pk=pk)
    if request.method == 'POST':
        form = BC15_Water_AreaForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC15_Water_Area(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC15_Water_Area/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC15_Water_AreaForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC15_Water_AreaUpdate.html",context)

def BC16_ManMade_thingUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC16_ManMade_thing, pk=pk)
    if request.method == 'POST':
        form = BC16_ManMade_thingForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC16_ManMade_thing(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC16_ManMade_thing/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC16_ManMade_thingForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC16_ManMade_thingUpdate.html",context)

def BC19_TitleUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC19_Title, pk=pk)
    if request.method == 'POST':
        form = BC19_TitleForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC19_Title(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC19_Title/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC19_TitleForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC19_TitleUpdate.html",context)

def BC21_DataSetUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC21_DataSet, pk=pk)
    if request.method == 'POST':
        form = BC21_DataSetForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC21_DataSet(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC21_DataSet/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC21_DataSetForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC21_DataSetUpdate.html",context)

def BC22_Encounter_EventUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC22_Encounter_Event, pk=pk)
    if request.method == 'POST':
        form = BC22_Encounter_EventForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC22_Encounter_Event(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC22_Encounter_Event/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC22_Encounter_EventForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC22_Encounter_EventUpdate.html",context)

def BC24_Repository_ObjectUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC24_Repository_Object, pk=pk)
    if request.method == 'POST':
        form = BC24_Repository_ObjectForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC24_Repository_Object(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC24_Repository_Object/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC24_Repository_ObjectForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC24_Repository_ObjectUpdate.html",context)

def BC26_Place_NameUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC26_Place_Name, pk=pk)
    if request.method == 'POST':
        form = BC26_Place_NameForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC26_Place_Name(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC26_Place_Name/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC26_Place_NameForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC26_Place_NameUpdate.html",context)

def BC31_Place_AppellationUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC31_Place_Appellation, pk=pk)
    if request.method == 'POST':
        form = BC31_Place_AppellationForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC31_Place_Appellation(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC31_Place_Appellation/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC31_Place_AppellationForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC31_Place_AppellationUpdate.html",context)

def BC27_PublicationUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC27_Publication, pk=pk)
    if request.method == 'POST':
        form = BC27_PublicationForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC27_Publication(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC27_Publication/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC27_PublicationForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC27_PublicationUpdate.html",context)

def BC33_Spatial_CoordinateUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC33_Spatial_Coordinate, pk=pk)
    if request.method == 'POST':
        form = BC33_Spatial_CoordinateForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC33_Spatial_Coordinate(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC33_Spatial_Coordinate/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC33_Spatial_CoordinateForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC33_Spatial_CoordinateUpdate.html",context)

def BC36_Abiotic_ElementUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC36_Abiotic_Element, pk=pk)
    if request.method == 'POST':
        form = BC36_Abiotic_ElementForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC36_Abiotic_Element(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC36_Abiotic_Element/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC36_Abiotic_ElementForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC36_Abiotic_ElementUpdate.html",context)

def BC37_Biological_ObjectUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC37_Biological_Object, pk=pk)
    if request.method == 'POST':
        form = BC37_Biological_ObjectForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC37_Biological_Object(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC37_Biological_Object/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC37_Biological_ObjectForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC37_Biological_ObjectUpdate.html",context)

def BC51_Physical_ObjectUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC51_Physical_Object, pk=pk)
    if request.method == 'POST':
        form = BC51_Physical_ObjectForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC51_Physical_Object(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC51_Physical_Object/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC51_Physical_ObjectForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC51_Physical_ObjectUpdate.html",context)

def BC38_Biotic_ElementUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BC38_Biotic_Element, pk=pk)
    if request.method == 'POST':
        form = BC38_Biotic_ElementForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BC38_Biotic_Element(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BC38_Biotic_Element/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC38_Biotic_ElementForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC38_Biotic_ElementUpdate.html",context)

def BT22_ClassUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT22_Class, pk=pk)
    if request.method == 'POST':
        form = BT22_ClassForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BT22_Class(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BT22_Class/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT22_ClassForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT22_ClassUpdate.html",context)

def BT23_SubClassUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT23_SubClass, pk=pk)
    if request.method == 'POST':
        form = BT23_SubClassForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BT23_SubClass(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BT23_SubClass/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT23_SubClassForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT23_SubClassUpdate.html",context)

def BT24_FamilyUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT24_Family, pk=pk)
    if request.method == 'POST':
        form = BT24_FamilyForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BT24_Family(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BT24_Family/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT24_FamilyForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT24_FamilyUpdate.html",context)

def BT25_SubFamilyUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT25_SubFamily, pk=pk)
    if request.method == 'POST':
        form = BT25_SubFamilyForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BT25_SubFamily(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BT25_SubFamily/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT25_SubFamilyForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT25_SubFamilyUpdate.html",context)

def BT26_GenusUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT26_Genus, pk=pk)
    if request.method == 'POST':
        form = BT26_GenusForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BT26_Genus(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BT26_Genus/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT26_GenusForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT26_GenusUpdate.html",context)

def BT27_SpeciesUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT27_Species, pk=pk)
    if request.method == 'POST':
        form = BT27_SpeciesForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BT27_Species(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BT27_Species/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT27_SpeciesForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT27_SpeciesUpdate.html",context)

def BT28_Scientific_Activity_TypeUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT28_Scientific_Activity_Type, pk=pk)
    if request.method == 'POST':
        form = BT28_Scientific_Activity_TypeForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BT28_Scientific_Activity_Type(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BT28_Scientific_Activity_Type/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT28_Scientific_Activity_TypeForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT28_Scientific_Activity_TypeUpdate.html",context)

def BT29_Industrial_Activity_TypeUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT29_Industrial_Activity_Type, pk=pk)
    if request.method == 'POST':
        form = BT29_Industrial_Activity_TypeForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BT29_Industrial_Activity_Type(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BT29_Industrial_Activity_Type/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT29_Industrial_Activity_TypeForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT29_Industrial_Activity_TypeUpdate.html",context)

def BT30_Identifier_Assignment_TypeUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT30_Identifier_Assignment_Type, pk=pk)
    if request.method == 'POST':
        form = BT30_Identifier_Assignment_TypeForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BT30_Identifier_Assignment_Type(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BT30_Identifier_Assignment_Type/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT30_Identifier_Assignment_TypeForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT30_Identifier_Assignment_TypeUpdate.html",context)

def BT31_Biological_Part_TimeUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT31_Biological_Part_Time, pk=pk)
    if request.method == 'POST':
        form = BT31_Biological_Part_TimeForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BT31_Biological_Part_Time(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BT31_Biological_Part_Time/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT31_Biological_Part_TimeForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT31_Biological_Part_TimeUpdate.html",context)

def BT33_Marine_Animal_TypeUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT33_Marine_Animal_Type, pk=pk)
    if request.method == 'POST':
        form = BT33_Marine_Animal_TypeForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BT33_Marine_Animal_Type(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BT33_Marine_Animal_Type/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT33_Marine_Animal_TypeForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT33_Marine_Animal_TypeUpdate.html",context)

def BT34_OrderUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT34_Order, pk=pk)
    if request.method == 'POST':
        form = BT34_OrderForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BT34_Order(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BT34_Order/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT34_OrderForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT34_OrderUpdate.html",context)

def BT5_Legislative_Zone_TypeUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT5_Legislative_Zone_Type, pk=pk)
    if request.method == 'POST':
        form = BT5_Legislative_Zone_TypeForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BT5_Legislative_Zone_Type(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BT5_Legislative_Zone_Type/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT5_Legislative_Zone_TypeForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT5_Legislative_Zone_TypeUpdate.html",context)

def BT8_Abiotic_Element_TypeUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BT8_Abiotic_Element_Type, pk=pk)
    if request.method == 'POST':
        form = BT8_Abiotic_Element_TypeForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.BT8_Abiotic_Element_Type(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/BT8_Abiotic_Element_Type/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT8_Abiotic_Element_TypeForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT8_Abiotic_Element_TypeUpdate.html",context)

def PersonUpdate(request, pk):
    data = dict() 
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")
    
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Person, pk=pk)
    if request.method == 'POST':
        form = PersonForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = onto_namespace.Person(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/MarineTLO_app/Person/list/"
            return HttpResponseRedirect(link)
    else:
        form = PersonForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "PersonUpdate.html",context)
