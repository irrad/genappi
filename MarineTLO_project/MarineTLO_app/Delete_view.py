from django.db import models
from django.template.loader import render_to_string
from .models import *
from .forms import *
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseNotFound
from django.template import loader
from django.core.urlresolvers import reverse
import datetime
from .forms import *
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.core.files.storage import FileSystemStorage
from django.utils.safestring import mark_safe


def BC35_Physical_ThingDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC35_Physical_Thing,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC35_Physical_Thing/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC35_Physical_ThingForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC35_Physical_ThingDelete.html",context)

def BC10_EventDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC10_Event,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC10_Event/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC10_EventForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC10_EventDelete.html",context)

def BC8_ActorDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC8_Actor,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC8_Actor/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC8_ActorForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC8_ActorDelete.html",context)

def BC34_Geometric_Place_ExpressionDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC34_Geometric_Place_Expression,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC34_Geometric_Place_Expression/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC34_Geometric_Place_ExpressionForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC34_Geometric_Place_ExpressionDelete.html",context)

def BC29_Spatial_Coordinate_Reference_SystemDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC29_Spatial_Coordinate_Reference_System,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC29_Spatial_Coordinate_Reference_System/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC29_Spatial_Coordinate_Reference_SystemForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC29_Spatial_Coordinate_Reference_SystemDelete.html",context)

def BC56_Digital_Measurement_EventDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC56_Digital_Measurement_Event,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC56_Digital_Measurement_Event/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC56_Digital_Measurement_EventForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC56_Digital_Measurement_EventDelete.html",context)

def BC23_Digital_ObjectDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC23_Digital_Object,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC23_Digital_Object/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC23_Digital_ObjectForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC23_Digital_ObjectDelete.html",context)

def BC18_PropositionDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC18_Proposition,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC18_Proposition/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC18_PropositionForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC18_PropositionDelete.html",context)

def BC59_SoftwareDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC59_Software,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC59_Software/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC59_SoftwareForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC59_SoftwareDelete.html",context)

def BC58_Digital_DeviceDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC58_Digital_Device,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC58_Digital_Device/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC58_Digital_DeviceForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC58_Digital_DeviceDelete.html",context)

def BC61_Capture_ActivityDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC61_Capture_Activity,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC61_Capture_Activity/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC61_Capture_ActivityForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC61_Capture_ActivityDelete.html",context)

def BC57_CaptureDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC57_Capture,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC57_Capture/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC57_CaptureForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC57_CaptureDelete.html",context)

def BC54_MeasurementDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC54_Measurement,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC54_Measurement/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC54_MeasurementForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC54_MeasurementDelete.html",context)

def BC44_Attribute_AssignmentDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC44_Attribute_Assignment,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC44_Attribute_Assignment/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC44_Attribute_AssignmentForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC44_Attribute_AssignmentDelete.html",context)

def BC1_TLO_EntityDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC1_TLO_Entity,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC1_TLO_Entity/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC1_TLO_EntityForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC1_TLO_EntityDelete.html",context)

def BC43_ActivityDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC43_Activity,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC43_Activity/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC43_ActivityForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC43_ActivityDelete.html",context)

def BC4_Temporal_PhenomenonDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC4_Temporal_Phenomenon,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC4_Temporal_Phenomenon/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC4_Temporal_PhenomenonForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC4_Temporal_PhenomenonDelete.html",context)

def BC3_PlaceDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC3_Place,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC3_Place/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC3_PlaceForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC3_PlaceDelete.html",context)

def BC45_ObservationDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC45_Observation,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC45_Observation/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC45_ObservationForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC45_ObservationDelete.html",context)

def BC9_Observable_EntityDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC9_Observable_Entity,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC9_Observable_Entity/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC9_Observable_EntityForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC9_Observable_EntityDelete.html",context)

def BC7_ThingDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC7_Thing,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC7_Thing/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC7_ThingForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC7_ThingDelete.html",context)

def BC5_DimensionDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC5_Dimension,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC5_Dimension/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC5_DimensionForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC5_DimensionDelete.html",context)

def BC30_AppellationDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC30_Appellation,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC30_Appellation/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC30_AppellationForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC30_AppellationDelete.html",context)

def BC55_Measurement_UnitDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC55_Measurement_Unit,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC55_Measurement_Unit/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC55_Measurement_UnitForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC55_Measurement_UnitDelete.html",context)

def BC20_Declarative_PlaceDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC20_Declarative_Place,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC20_Declarative_Place/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC20_Declarative_PlaceForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC20_Declarative_PlaceDelete.html",context)

def BC72_SpaceTime_VolumeDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC72_SpaceTime_Volume,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC72_SpaceTime_Volume/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC72_SpaceTime_VolumeForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC72_SpaceTime_VolumeDelete.html",context)

def BC2_Time_SpanDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC2_Time_Span,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC2_Time_Span/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC2_Time_SpanForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC2_Time_SpanDelete.html",context)

def BT14_Appellation_TypeDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT14_Appellation_Type,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BT14_Appellation_Type/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT14_Appellation_TypeForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT14_Appellation_TypeDelete.html",context)

def BT2_Temporal_Phenomenon_TypeDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT2_Temporal_Phenomenon_Type,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BT2_Temporal_Phenomenon_Type/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT2_Temporal_Phenomenon_TypeForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT2_Temporal_Phenomenon_TypeDelete.html",context)

def BT10_Biotic_Element_TypeDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT10_Biotic_Element_Type,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BT10_Biotic_Element_Type/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT10_Biotic_Element_TypeForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT10_Biotic_Element_TypeDelete.html",context)

def BT7_Ecosystem_TypeDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT7_Ecosystem_Type,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BT7_Ecosystem_Type/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT7_Ecosystem_TypeForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT7_Ecosystem_TypeDelete.html",context)

def BT4_Conceptual_Object_TypeDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT4_Conceptual_Object_Type,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BT4_Conceptual_Object_Type/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT4_Conceptual_Object_TypeForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT4_Conceptual_Object_TypeDelete.html",context)

def BT1_TLO_Entity_TypeDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT1_TLO_Entity_Type,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BT1_TLO_Entity_Type/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT1_TLO_Entity_TypeForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT1_TLO_Entity_TypeDelete.html",context)

def BT17_Human_Activity_TypeDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT17_Human_Activity_Type,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BT17_Human_Activity_Type/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT17_Human_Activity_TypeForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT17_Human_Activity_TypeDelete.html",context)

def BT9_Actor_TypeDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT9_Actor_Type,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BT9_Actor_Type/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT9_Actor_TypeForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT9_Actor_TypeDelete.html",context)

def BT32_Persistent_TypeDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT32_Persistent_Type,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BT32_Persistent_Type/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT32_Persistent_TypeForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT32_Persistent_TypeDelete.html",context)

def BC32_IdentifierDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC32_Identifier,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC32_Identifier/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC32_IdentifierForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC32_IdentifierDelete.html",context)

def BC64_Design_or_ProcedureDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC64_Design_or_Procedure,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC64_Design_or_Procedure/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC64_Design_or_ProcedureForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC64_Design_or_ProcedureDelete.html",context)

def BC79_RightDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC79_Right,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC79_Right/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC79_RightForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC79_RightDelete.html",context)

def BC6_Persistent_ItemDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC6_Persistent_Item,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC6_Persistent_Item/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC6_Persistent_ItemForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC6_Persistent_ItemDelete.html",context)

def BT3_Physical_Thing_TypeDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT3_Physical_Thing_Type,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BT3_Physical_Thing_Type/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT3_Physical_Thing_TypeForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT3_Physical_Thing_TypeDelete.html",context)

def BC17_Conceptual_ObjectDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC17_Conceptual_Object,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC17_Conceptual_Object/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC17_Conceptual_ObjectForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC17_Conceptual_ObjectDelete.html",context)

def BT32_Persisten_TypeDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT32_Persisten_Type,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BT32_Persisten_Type/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT32_Persisten_TypeForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT32_Persisten_TypeDelete.html",context)

def BC39_Marine_AnimalDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC39_Marine_Animal,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC39_Marine_Animal/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC39_Marine_AnimalForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC39_Marine_AnimalDelete.html",context)

def BC40_Physical_ManMade_ThingDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC40_Physical_ManMade_Thing,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC40_Physical_ManMade_Thing/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC40_Physical_ManMade_ThingForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC40_Physical_ManMade_ThingDelete.html",context)

def BC41_ManMade_ObjectDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC41_ManMade_Object,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC41_ManMade_Object/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC41_ManMade_ObjectForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC41_ManMade_ObjectDelete.html",context)

def BC42_CollectionDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC42_Collection,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC42_Collection/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC42_CollectionForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC42_CollectionDelete.html",context)

def BC46_Identifier_AssignmentDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC46_Identifier_Assignment,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC46_Identifier_Assignment/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC46_Identifier_AssignmentForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC46_Identifier_AssignmentDelete.html",context)

def BC47_ImageDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC47_Image,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC47_Image/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC47_ImageForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC47_ImageDelete.html",context)

def BC48_DatabaseDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC48_Database,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC48_Database/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC48_DatabaseForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC48_DatabaseDelete.html",context)

def BC53_SpecimenDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC53_Specimen,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC53_Specimen/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC53_SpecimenForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC53_SpecimenDelete.html",context)

def BC60_Software_ExecutionDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC60_Software_Execution,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC60_Software_Execution/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC60_Software_ExecutionForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC60_Software_ExecutionDelete.html",context)

def BC62_Statistic_IndicatorDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC62_Statistic_Indicator,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC62_Statistic_Indicator/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC62_Statistic_IndicatorForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC62_Statistic_IndicatorDelete.html",context)

def BC63_Global_Statistic_LandingDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC63_Global_Statistic_Landing,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC63_Global_Statistic_Landing/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC63_Global_Statistic_LandingForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC63_Global_Statistic_LandingDelete.html",context)

def BC70_GroupDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC70_Group,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC70_Group/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC70_GroupForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC70_GroupDelete.html",context)

def BC71_Name_Use_ActivityDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC71_Name_Use_Activity,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC71_Name_Use_Activity/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC71_Name_Use_ActivityForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC71_Name_Use_ActivityDelete.html",context)

def BC75_StockDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC75_Stock,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC75_Stock/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC75_StockForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC75_StockDelete.html",context)

def BC76_Stock_FormationDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC76_Stock_Formation,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC76_Stock_Formation/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC76_Stock_FormationForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC76_Stock_FormationDelete.html",context)

def BC77_Matter_RemovalDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC77_Matter_Removal,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC77_Matter_Removal/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC77_Matter_RemovalForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC77_Matter_RemovalDelete.html",context)

def BC78_Amount_of_MatterDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC78_Amount_of_Matter,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC78_Amount_of_Matter/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC78_Amount_of_MatterForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC78_Amount_of_MatterDelete.html",context)

def BT11_Equipment_TypeDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT11_Equipment_Type,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BT11_Equipment_Type/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT11_Equipment_TypeForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT11_Equipment_TypeDelete.html",context)

def BT12_Scientific_Data_TypeDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT12_Scientific_Data_Type,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BT12_Scientific_Data_Type/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT12_Scientific_Data_TypeForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT12_Scientific_Data_TypeDelete.html",context)

def BT13_Digital_Object_TypeDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT13_Digital_Object_Type,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BT13_Digital_Object_Type/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT13_Digital_Object_TypeForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT13_Digital_Object_TypeDelete.html",context)

def BT16_Visual_TypeDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT16_Visual_Type,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BT16_Visual_Type/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT16_Visual_TypeForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT16_Visual_TypeDelete.html",context)

def BT6_Human_Event_TypeDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT6_Human_Event_Type,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BT6_Human_Event_Type/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT6_Human_Event_TypeForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT6_Human_Event_TypeDelete.html",context)

def BT18_KingdomDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT18_Kingdom,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BT18_Kingdom/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT18_KingdomForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT18_KingdomDelete.html",context)

def BT19_PhylumDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT19_Phylum,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BT19_Phylum/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT19_PhylumForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT19_PhylumDelete.html",context)

def BT20_SubPhylumDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT20_SubPhylum,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BT20_SubPhylum/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT20_SubPhylumForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT20_SubPhylumDelete.html",context)

def BT21_SuperClassDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT21_SuperClass,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BT21_SuperClass/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT21_SuperClassForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT21_SuperClassDelete.html",context)

def BT35_Property_TypeDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT35_Property_Type,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BT35_Property_Type/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT35_Property_TypeForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT35_Property_TypeDelete.html",context)

def BC25_Linguistic_ObjectDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC25_Linguistic_Object,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC25_Linguistic_Object/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC25_Linguistic_ObjectForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC25_Linguistic_ObjectDelete.html",context)

def BT36_LanguageDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT36_Language,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BT36_Language/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT36_LanguageForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT36_LanguageDelete.html",context)

def BC11_PersonDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC11_Person,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC11_Person/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC11_PersonForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC11_PersonDelete.html",context)

def BC12_EcosystemDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC12_Ecosystem,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC12_Ecosystem/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC12_EcosystemForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC12_EcosystemDelete.html",context)

def BC13_OrganizationDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC13_Organization,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC13_Organization/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC13_OrganizationForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC13_OrganizationDelete.html",context)

def BC14_Ecosystem_EnvironmentDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC14_Ecosystem_Environment,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC14_Ecosystem_Environment/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC14_Ecosystem_EnvironmentForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC14_Ecosystem_EnvironmentDelete.html",context)

def BC15_Water_AreaDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC15_Water_Area,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC15_Water_Area/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC15_Water_AreaForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC15_Water_AreaDelete.html",context)

def BC16_ManMade_thingDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC16_ManMade_thing,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC16_ManMade_thing/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC16_ManMade_thingForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC16_ManMade_thingDelete.html",context)

def BC19_TitleDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC19_Title,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC19_Title/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC19_TitleForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC19_TitleDelete.html",context)

def BC21_DataSetDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC21_DataSet,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC21_DataSet/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC21_DataSetForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC21_DataSetDelete.html",context)

def BC22_Encounter_EventDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC22_Encounter_Event,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC22_Encounter_Event/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC22_Encounter_EventForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC22_Encounter_EventDelete.html",context)

def BC24_Repository_ObjectDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC24_Repository_Object,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC24_Repository_Object/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC24_Repository_ObjectForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC24_Repository_ObjectDelete.html",context)

def BC26_Place_NameDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC26_Place_Name,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC26_Place_Name/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC26_Place_NameForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC26_Place_NameDelete.html",context)

def BC31_Place_AppellationDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC31_Place_Appellation,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC31_Place_Appellation/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC31_Place_AppellationForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC31_Place_AppellationDelete.html",context)

def BC27_PublicationDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC27_Publication,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC27_Publication/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC27_PublicationForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC27_PublicationDelete.html",context)

def BC33_Spatial_CoordinateDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC33_Spatial_Coordinate,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC33_Spatial_Coordinate/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC33_Spatial_CoordinateForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC33_Spatial_CoordinateDelete.html",context)

def BC36_Abiotic_ElementDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC36_Abiotic_Element,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC36_Abiotic_Element/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC36_Abiotic_ElementForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC36_Abiotic_ElementDelete.html",context)

def BC37_Biological_ObjectDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC37_Biological_Object,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC37_Biological_Object/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC37_Biological_ObjectForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC37_Biological_ObjectDelete.html",context)

def BC51_Physical_ObjectDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC51_Physical_Object,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC51_Physical_Object/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC51_Physical_ObjectForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC51_Physical_ObjectDelete.html",context)

def BC38_Biotic_ElementDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BC38_Biotic_Element,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BC38_Biotic_Element/list/"
        return HttpResponseRedirect(link)
    else:
         form = BC38_Biotic_ElementForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BC38_Biotic_ElementDelete.html",context)

def BT22_ClassDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT22_Class,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BT22_Class/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT22_ClassForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT22_ClassDelete.html",context)

def BT23_SubClassDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT23_SubClass,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BT23_SubClass/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT23_SubClassForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT23_SubClassDelete.html",context)

def BT24_FamilyDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT24_Family,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BT24_Family/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT24_FamilyForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT24_FamilyDelete.html",context)

def BT25_SubFamilyDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT25_SubFamily,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BT25_SubFamily/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT25_SubFamilyForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT25_SubFamilyDelete.html",context)

def BT26_GenusDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT26_Genus,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BT26_Genus/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT26_GenusForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT26_GenusDelete.html",context)

def BT27_SpeciesDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT27_Species,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BT27_Species/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT27_SpeciesForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT27_SpeciesDelete.html",context)

def BT28_Scientific_Activity_TypeDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT28_Scientific_Activity_Type,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BT28_Scientific_Activity_Type/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT28_Scientific_Activity_TypeForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT28_Scientific_Activity_TypeDelete.html",context)

def BT29_Industrial_Activity_TypeDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT29_Industrial_Activity_Type,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BT29_Industrial_Activity_Type/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT29_Industrial_Activity_TypeForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT29_Industrial_Activity_TypeDelete.html",context)

def BT30_Identifier_Assignment_TypeDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT30_Identifier_Assignment_Type,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BT30_Identifier_Assignment_Type/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT30_Identifier_Assignment_TypeForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT30_Identifier_Assignment_TypeDelete.html",context)

def BT31_Biological_Part_TimeDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT31_Biological_Part_Time,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BT31_Biological_Part_Time/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT31_Biological_Part_TimeForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT31_Biological_Part_TimeDelete.html",context)

def BT33_Marine_Animal_TypeDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT33_Marine_Animal_Type,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BT33_Marine_Animal_Type/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT33_Marine_Animal_TypeForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT33_Marine_Animal_TypeDelete.html",context)

def BT34_OrderDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT34_Order,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BT34_Order/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT34_OrderForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT34_OrderDelete.html",context)

def BT5_Legislative_Zone_TypeDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT5_Legislative_Zone_Type,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BT5_Legislative_Zone_Type/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT5_Legislative_Zone_TypeForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT5_Legislative_Zone_TypeDelete.html",context)

def BT8_Abiotic_Element_TypeDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BT8_Abiotic_Element_Type,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/BT8_Abiotic_Element_Type/list/"
        return HttpResponseRedirect(link)
    else:
         form = BT8_Abiotic_Element_TypeForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BT8_Abiotic_Element_TypeDelete.html",context)

def PersonDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Person,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/MarineTLO_app/Person/list/"
        return HttpResponseRedirect(link)
    else:
         form = PersonForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "PersonDelete.html",context)
