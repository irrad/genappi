from django.db import models
from django.template.loader import render_to_string
from .models import *
from .forms import *
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseNotFound
from django.template import loader
from django.core.urlresolvers import reverse
import datetime
from .forms import *
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.core.files.storage import FileSystemStorage
from django.utils.safestring import mark_safe

def vowl(request):
    context = {}
    return render(request, "do_visualisation.html", context)

def HomeView(request):
    classes = []
    
    classes.append(str(BC35_Physical_Thing))
    
    classes.append(str(BC10_Event))
    
    classes.append(str(BC8_Actor))
    
    classes.append(str(BC34_Geometric_Place_Expression))
    
    classes.append(str(BC29_Spatial_Coordinate_Reference_System))
    
    classes.append(str(BC56_Digital_Measurement_Event))
    
    classes.append(str(BC23_Digital_Object))
    
    classes.append(str(BC18_Proposition))
    
    classes.append(str(BC59_Software))
    
    classes.append(str(BC58_Digital_Device))
    
    classes.append(str(BC61_Capture_Activity))
    
    classes.append(str(BC57_Capture))
    
    classes.append(str(BC54_Measurement))
    
    classes.append(str(BC44_Attribute_Assignment))
    
    classes.append(str(BC1_TLO_Entity))
    
    classes.append(str(BC43_Activity))
    
    classes.append(str(BC4_Temporal_Phenomenon))
    
    classes.append(str(BC3_Place))
    
    classes.append(str(BC45_Observation))
    
    classes.append(str(BC9_Observable_Entity))
    
    classes.append(str(BC7_Thing))
    
    classes.append(str(BC5_Dimension))
    
    classes.append(str(BC30_Appellation))
    
    classes.append(str(BC55_Measurement_Unit))
    
    classes.append(str(BC20_Declarative_Place))
    
    classes.append(str(BC72_SpaceTime_Volume))
    
    classes.append(str(BC2_Time_Span))
    
    classes.append(str(BT14_Appellation_Type))
    
    classes.append(str(BT2_Temporal_Phenomenon_Type))
    
    classes.append(str(BT10_Biotic_Element_Type))
    
    classes.append(str(BT7_Ecosystem_Type))
    
    classes.append(str(BT4_Conceptual_Object_Type))
    
    classes.append(str(BT1_TLO_Entity_Type))
    
    classes.append(str(BT17_Human_Activity_Type))
    
    classes.append(str(BT9_Actor_Type))
    
    classes.append(str(BT32_Persistent_Type))
    
    classes.append(str(BC32_Identifier))
    
    classes.append(str(BC64_Design_or_Procedure))
    
    classes.append(str(BC79_Right))
    
    classes.append(str(BC6_Persistent_Item))
    
    classes.append(str(BT3_Physical_Thing_Type))
    
    classes.append(str(BC17_Conceptual_Object))
    
    classes.append(str(BT32_Persisten_Type))
    
    classes.append(str(BC39_Marine_Animal))
    
    classes.append(str(BC40_Physical_ManMade_Thing))
    
    classes.append(str(BC41_ManMade_Object))
    
    classes.append(str(BC42_Collection))
    
    classes.append(str(BC46_Identifier_Assignment))
    
    classes.append(str(BC47_Image))
    
    classes.append(str(BC48_Database))
    
    classes.append(str(BC53_Specimen))
    
    classes.append(str(BC60_Software_Execution))
    
    classes.append(str(BC62_Statistic_Indicator))
    
    classes.append(str(BC63_Global_Statistic_Landing))
    
    classes.append(str(BC70_Group))
    
    classes.append(str(BC71_Name_Use_Activity))
    
    classes.append(str(BC75_Stock))
    
    classes.append(str(BC76_Stock_Formation))
    
    classes.append(str(BC77_Matter_Removal))
    
    classes.append(str(BC78_Amount_of_Matter))
    
    classes.append(str(BT11_Equipment_Type))
    
    classes.append(str(BT12_Scientific_Data_Type))
    
    classes.append(str(BT13_Digital_Object_Type))
    
    classes.append(str(BT16_Visual_Type))
    
    classes.append(str(BT6_Human_Event_Type))
    
    classes.append(str(BT18_Kingdom))
    
    classes.append(str(BT19_Phylum))
    
    classes.append(str(BT20_SubPhylum))
    
    classes.append(str(BT21_SuperClass))
    
    classes.append(str(BT35_Property_Type))
    
    classes.append(str(BC25_Linguistic_Object))
    
    classes.append(str(BT36_Language))
    
    classes.append(str(BC11_Person))
    
    classes.append(str(BC12_Ecosystem))
    
    classes.append(str(BC13_Organization))
    
    classes.append(str(BC14_Ecosystem_Environment))
    
    classes.append(str(BC15_Water_Area))
    
    classes.append(str(BC16_ManMade_thing))
    
    classes.append(str(BC19_Title))
    
    classes.append(str(BC21_DataSet))
    
    classes.append(str(BC22_Encounter_Event))
    
    classes.append(str(BC24_Repository_Object))
    
    classes.append(str(BC26_Place_Name))
    
    classes.append(str(BC31_Place_Appellation))
    
    classes.append(str(BC27_Publication))
    
    classes.append(str(BC33_Spatial_Coordinate))
    
    classes.append(str(BC36_Abiotic_Element))
    
    classes.append(str(BC37_Biological_Object))
    
    classes.append(str(BC51_Physical_Object))
    
    classes.append(str(BC38_Biotic_Element))
    
    classes.append(str(BT22_Class))
    
    classes.append(str(BT23_SubClass))
    
    classes.append(str(BT24_Family))
    
    classes.append(str(BT25_SubFamily))
    
    classes.append(str(BT26_Genus))
    
    classes.append(str(BT27_Species))
    
    classes.append(str(BT28_Scientific_Activity_Type))
    
    classes.append(str(BT29_Industrial_Activity_Type))
    
    classes.append(str(BT30_Identifier_Assignment_Type))
    
    classes.append(str(BT31_Biological_Part_Time))
    
    classes.append(str(BT33_Marine_Animal_Type))
    
    classes.append(str(BT34_Order))
    
    classes.append(str(BT5_Legislative_Zone_Type))
    
    classes.append(str(BT8_Abiotic_Element_Type))
    
    classes.append(str(Person))
    
    context = {'classes':classes}
    return render(request, "home.html",context)