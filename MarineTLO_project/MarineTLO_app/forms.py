from django import forms
from django.forms import ModelForm
from .models import *


class BC35_Physical_ThingForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC35_Physical_ThingForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC35_Physical_Thing
        exclude = ()

class BC10_EventForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC10_EventForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC10_Event
        exclude = ()

class BC8_ActorForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC8_ActorForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC8_Actor
        exclude = ()

class BC34_Geometric_Place_ExpressionForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC34_Geometric_Place_ExpressionForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC34_Geometric_Place_Expression
        exclude = ()

class BC29_Spatial_Coordinate_Reference_SystemForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC29_Spatial_Coordinate_Reference_SystemForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC29_Spatial_Coordinate_Reference_System
        exclude = ()

class BC56_Digital_Measurement_EventForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC56_Digital_Measurement_EventForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC56_Digital_Measurement_Event
        exclude = ()

class BC23_Digital_ObjectForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC23_Digital_ObjectForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC23_Digital_Object
        exclude = ()

class BC18_PropositionForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC18_PropositionForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC18_Proposition
        exclude = ()

class BC59_SoftwareForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC59_SoftwareForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC59_Software
        exclude = ()

class BC58_Digital_DeviceForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC58_Digital_DeviceForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC58_Digital_Device
        exclude = ()

class BC61_Capture_ActivityForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC61_Capture_ActivityForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC61_Capture_Activity
        exclude = ()

class BC57_CaptureForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC57_CaptureForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC57_Capture
        exclude = ()

class BC54_MeasurementForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC54_MeasurementForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC54_Measurement
        exclude = ()

class BC44_Attribute_AssignmentForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC44_Attribute_AssignmentForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC44_Attribute_Assignment
        exclude = ()

class BC1_TLO_EntityForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC1_TLO_EntityForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC1_TLO_Entity
        exclude = ()

class BC43_ActivityForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC43_ActivityForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC43_Activity
        exclude = ()

class BC4_Temporal_PhenomenonForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC4_Temporal_PhenomenonForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC4_Temporal_Phenomenon
        exclude = ()

class BC3_PlaceForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC3_PlaceForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC3_Place
        exclude = ()

class BC45_ObservationForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC45_ObservationForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC45_Observation
        exclude = ()

class BC9_Observable_EntityForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC9_Observable_EntityForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC9_Observable_Entity
        exclude = ()

class BC7_ThingForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC7_ThingForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC7_Thing
        exclude = ()

class BC5_DimensionForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC5_DimensionForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC5_Dimension
        exclude = ()

class BC30_AppellationForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC30_AppellationForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC30_Appellation
        exclude = ()

class BC55_Measurement_UnitForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC55_Measurement_UnitForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC55_Measurement_Unit
        exclude = ()

class BC20_Declarative_PlaceForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC20_Declarative_PlaceForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC20_Declarative_Place
        exclude = ()

class BC72_SpaceTime_VolumeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC72_SpaceTime_VolumeForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC72_SpaceTime_Volume
        exclude = ()

class BC2_Time_SpanForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC2_Time_SpanForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC2_Time_Span
        exclude = ()

class BT14_Appellation_TypeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT14_Appellation_TypeForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT14_Appellation_Type
        exclude = ()

class BT2_Temporal_Phenomenon_TypeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT2_Temporal_Phenomenon_TypeForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT2_Temporal_Phenomenon_Type
        exclude = ()

class BT10_Biotic_Element_TypeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT10_Biotic_Element_TypeForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT10_Biotic_Element_Type
        exclude = ()

class BT7_Ecosystem_TypeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT7_Ecosystem_TypeForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT7_Ecosystem_Type
        exclude = ()

class BT4_Conceptual_Object_TypeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT4_Conceptual_Object_TypeForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT4_Conceptual_Object_Type
        exclude = ()

class BT1_TLO_Entity_TypeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT1_TLO_Entity_TypeForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT1_TLO_Entity_Type
        exclude = ()

class BT17_Human_Activity_TypeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT17_Human_Activity_TypeForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT17_Human_Activity_Type
        exclude = ()

class BT9_Actor_TypeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT9_Actor_TypeForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT9_Actor_Type
        exclude = ()

class BT32_Persistent_TypeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT32_Persistent_TypeForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT32_Persistent_Type
        exclude = ()

class BC32_IdentifierForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC32_IdentifierForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC32_Identifier
        exclude = ()

class BC64_Design_or_ProcedureForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC64_Design_or_ProcedureForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC64_Design_or_Procedure
        exclude = ()

class BC79_RightForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC79_RightForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC79_Right
        exclude = ()

class BC6_Persistent_ItemForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC6_Persistent_ItemForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC6_Persistent_Item
        exclude = ()

class BT3_Physical_Thing_TypeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT3_Physical_Thing_TypeForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT3_Physical_Thing_Type
        exclude = ()

class BC17_Conceptual_ObjectForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC17_Conceptual_ObjectForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC17_Conceptual_Object
        exclude = ()

class BT32_Persisten_TypeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT32_Persisten_TypeForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT32_Persisten_Type
        exclude = ()

class BC39_Marine_AnimalForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC39_Marine_AnimalForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC39_Marine_Animal
        exclude = ()

class BC40_Physical_ManMade_ThingForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC40_Physical_ManMade_ThingForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC40_Physical_ManMade_Thing
        exclude = ()

class BC41_ManMade_ObjectForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC41_ManMade_ObjectForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC41_ManMade_Object
        exclude = ()

class BC42_CollectionForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC42_CollectionForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC42_Collection
        exclude = ()

class BC46_Identifier_AssignmentForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC46_Identifier_AssignmentForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC46_Identifier_Assignment
        exclude = ()

class BC47_ImageForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC47_ImageForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC47_Image
        exclude = ()

class BC48_DatabaseForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC48_DatabaseForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC48_Database
        exclude = ()

class BC53_SpecimenForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC53_SpecimenForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC53_Specimen
        exclude = ()

class BC60_Software_ExecutionForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC60_Software_ExecutionForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC60_Software_Execution
        exclude = ()

class BC62_Statistic_IndicatorForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC62_Statistic_IndicatorForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC62_Statistic_Indicator
        exclude = ()

class BC63_Global_Statistic_LandingForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC63_Global_Statistic_LandingForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC63_Global_Statistic_Landing
        exclude = ()

class BC70_GroupForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC70_GroupForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC70_Group
        exclude = ()

class BC71_Name_Use_ActivityForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC71_Name_Use_ActivityForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC71_Name_Use_Activity
        exclude = ()

class BC75_StockForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC75_StockForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC75_Stock
        exclude = ()

class BC76_Stock_FormationForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC76_Stock_FormationForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC76_Stock_Formation
        exclude = ()

class BC77_Matter_RemovalForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC77_Matter_RemovalForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC77_Matter_Removal
        exclude = ()

class BC78_Amount_of_MatterForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC78_Amount_of_MatterForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC78_Amount_of_Matter
        exclude = ()

class BT11_Equipment_TypeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT11_Equipment_TypeForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT11_Equipment_Type
        exclude = ()

class BT12_Scientific_Data_TypeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT12_Scientific_Data_TypeForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT12_Scientific_Data_Type
        exclude = ()

class BT13_Digital_Object_TypeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT13_Digital_Object_TypeForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT13_Digital_Object_Type
        exclude = ()

class BT16_Visual_TypeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT16_Visual_TypeForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT16_Visual_Type
        exclude = ()

class BT6_Human_Event_TypeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT6_Human_Event_TypeForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT6_Human_Event_Type
        exclude = ()

class BT18_KingdomForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT18_KingdomForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT18_Kingdom
        exclude = ()

class BT19_PhylumForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT19_PhylumForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT19_Phylum
        exclude = ()

class BT20_SubPhylumForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT20_SubPhylumForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT20_SubPhylum
        exclude = ()

class BT21_SuperClassForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT21_SuperClassForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT21_SuperClass
        exclude = ()

class BT35_Property_TypeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT35_Property_TypeForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT35_Property_Type
        exclude = ()

class BC25_Linguistic_ObjectForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC25_Linguistic_ObjectForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC25_Linguistic_Object
        exclude = ()

class BT36_LanguageForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT36_LanguageForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT36_Language
        exclude = ()

class BC11_PersonForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC11_PersonForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC11_Person
        exclude = ()

class BC12_EcosystemForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC12_EcosystemForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC12_Ecosystem
        exclude = ()

class BC13_OrganizationForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC13_OrganizationForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC13_Organization
        exclude = ()

class BC14_Ecosystem_EnvironmentForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC14_Ecosystem_EnvironmentForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC14_Ecosystem_Environment
        exclude = ()

class BC15_Water_AreaForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC15_Water_AreaForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC15_Water_Area
        exclude = ()

class BC16_ManMade_thingForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC16_ManMade_thingForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC16_ManMade_thing
        exclude = ()

class BC19_TitleForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC19_TitleForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC19_Title
        exclude = ()

class BC21_DataSetForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC21_DataSetForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC21_DataSet
        exclude = ()

class BC22_Encounter_EventForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC22_Encounter_EventForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC22_Encounter_Event
        exclude = ()

class BC24_Repository_ObjectForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC24_Repository_ObjectForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC24_Repository_Object
        exclude = ()

class BC26_Place_NameForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC26_Place_NameForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC26_Place_Name
        exclude = ()

class BC31_Place_AppellationForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC31_Place_AppellationForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC31_Place_Appellation
        exclude = ()

class BC27_PublicationForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC27_PublicationForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC27_Publication
        exclude = ()

class BC33_Spatial_CoordinateForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC33_Spatial_CoordinateForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC33_Spatial_Coordinate
        exclude = ()

class BC36_Abiotic_ElementForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC36_Abiotic_ElementForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC36_Abiotic_Element
        exclude = ()

class BC37_Biological_ObjectForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC37_Biological_ObjectForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC37_Biological_Object
        exclude = ()

class BC51_Physical_ObjectForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC51_Physical_ObjectForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC51_Physical_Object
        exclude = ()

class BC38_Biotic_ElementForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BC38_Biotic_ElementForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BC38_Biotic_Element
        exclude = ()

class BT22_ClassForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT22_ClassForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT22_Class
        exclude = ()

class BT23_SubClassForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT23_SubClassForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT23_SubClass
        exclude = ()

class BT24_FamilyForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT24_FamilyForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT24_Family
        exclude = ()

class BT25_SubFamilyForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT25_SubFamilyForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT25_SubFamily
        exclude = ()

class BT26_GenusForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT26_GenusForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT26_Genus
        exclude = ()

class BT27_SpeciesForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT27_SpeciesForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT27_Species
        exclude = ()

class BT28_Scientific_Activity_TypeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT28_Scientific_Activity_TypeForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT28_Scientific_Activity_Type
        exclude = ()

class BT29_Industrial_Activity_TypeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT29_Industrial_Activity_TypeForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT29_Industrial_Activity_Type
        exclude = ()

class BT30_Identifier_Assignment_TypeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT30_Identifier_Assignment_TypeForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT30_Identifier_Assignment_Type
        exclude = ()

class BT31_Biological_Part_TimeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT31_Biological_Part_TimeForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT31_Biological_Part_Time
        exclude = ()

class BT33_Marine_Animal_TypeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT33_Marine_Animal_TypeForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT33_Marine_Animal_Type
        exclude = ()

class BT34_OrderForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT34_OrderForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT34_Order
        exclude = ()

class BT5_Legislative_Zone_TypeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT5_Legislative_Zone_TypeForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT5_Legislative_Zone_Type
        exclude = ()

class BT8_Abiotic_Element_TypeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BT8_Abiotic_Element_TypeForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BT8_Abiotic_Element_Type
        exclude = ()

class PersonForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(PersonForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Person
        exclude = ()
