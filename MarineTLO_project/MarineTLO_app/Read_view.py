from django.db import models
from django.template.loader import render_to_string
from .models import *
from .forms import *
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseNotFound
from django.template import loader
from django.core.urlresolvers import reverse
import datetime
from .forms import *
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.core.files.storage import FileSystemStorage
from django.utils.safestring import mark_safe
from django.core import serializers


def BC35_Physical_ThingRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC35_Physical_Thing, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC35_Physical_ThingRead.html",{'instance_items': instance_items})

def BC10_EventRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC10_Event, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC10_EventRead.html",{'instance_items': instance_items})

def BC8_ActorRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC8_Actor, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC8_ActorRead.html",{'instance_items': instance_items})

def BC34_Geometric_Place_ExpressionRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC34_Geometric_Place_Expression, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC34_Geometric_Place_ExpressionRead.html",{'instance_items': instance_items})

def BC29_Spatial_Coordinate_Reference_SystemRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC29_Spatial_Coordinate_Reference_System, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC29_Spatial_Coordinate_Reference_SystemRead.html",{'instance_items': instance_items})

def BC56_Digital_Measurement_EventRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC56_Digital_Measurement_Event, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC56_Digital_Measurement_EventRead.html",{'instance_items': instance_items})

def BC23_Digital_ObjectRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC23_Digital_Object, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC23_Digital_ObjectRead.html",{'instance_items': instance_items})

def BC18_PropositionRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC18_Proposition, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC18_PropositionRead.html",{'instance_items': instance_items})

def BC59_SoftwareRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC59_Software, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC59_SoftwareRead.html",{'instance_items': instance_items})

def BC58_Digital_DeviceRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC58_Digital_Device, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC58_Digital_DeviceRead.html",{'instance_items': instance_items})

def BC61_Capture_ActivityRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC61_Capture_Activity, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC61_Capture_ActivityRead.html",{'instance_items': instance_items})

def BC57_CaptureRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC57_Capture, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC57_CaptureRead.html",{'instance_items': instance_items})

def BC54_MeasurementRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC54_Measurement, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC54_MeasurementRead.html",{'instance_items': instance_items})

def BC44_Attribute_AssignmentRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC44_Attribute_Assignment, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC44_Attribute_AssignmentRead.html",{'instance_items': instance_items})

def BC1_TLO_EntityRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC1_TLO_Entity, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC1_TLO_EntityRead.html",{'instance_items': instance_items})

def BC43_ActivityRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC43_Activity, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC43_ActivityRead.html",{'instance_items': instance_items})

def BC4_Temporal_PhenomenonRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC4_Temporal_Phenomenon, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC4_Temporal_PhenomenonRead.html",{'instance_items': instance_items})

def BC3_PlaceRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC3_Place, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC3_PlaceRead.html",{'instance_items': instance_items})

def BC45_ObservationRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC45_Observation, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC45_ObservationRead.html",{'instance_items': instance_items})

def BC9_Observable_EntityRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC9_Observable_Entity, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC9_Observable_EntityRead.html",{'instance_items': instance_items})

def BC7_ThingRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC7_Thing, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC7_ThingRead.html",{'instance_items': instance_items})

def BC5_DimensionRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC5_Dimension, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC5_DimensionRead.html",{'instance_items': instance_items})

def BC30_AppellationRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC30_Appellation, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC30_AppellationRead.html",{'instance_items': instance_items})

def BC55_Measurement_UnitRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC55_Measurement_Unit, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC55_Measurement_UnitRead.html",{'instance_items': instance_items})

def BC20_Declarative_PlaceRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC20_Declarative_Place, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC20_Declarative_PlaceRead.html",{'instance_items': instance_items})

def BC72_SpaceTime_VolumeRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC72_SpaceTime_Volume, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC72_SpaceTime_VolumeRead.html",{'instance_items': instance_items})

def BC2_Time_SpanRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC2_Time_Span, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC2_Time_SpanRead.html",{'instance_items': instance_items})

def BT14_Appellation_TypeRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT14_Appellation_Type, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT14_Appellation_TypeRead.html",{'instance_items': instance_items})

def BT2_Temporal_Phenomenon_TypeRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT2_Temporal_Phenomenon_Type, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT2_Temporal_Phenomenon_TypeRead.html",{'instance_items': instance_items})

def BT10_Biotic_Element_TypeRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT10_Biotic_Element_Type, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT10_Biotic_Element_TypeRead.html",{'instance_items': instance_items})

def BT7_Ecosystem_TypeRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT7_Ecosystem_Type, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT7_Ecosystem_TypeRead.html",{'instance_items': instance_items})

def BT4_Conceptual_Object_TypeRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT4_Conceptual_Object_Type, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT4_Conceptual_Object_TypeRead.html",{'instance_items': instance_items})

def BT1_TLO_Entity_TypeRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT1_TLO_Entity_Type, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT1_TLO_Entity_TypeRead.html",{'instance_items': instance_items})

def BT17_Human_Activity_TypeRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT17_Human_Activity_Type, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT17_Human_Activity_TypeRead.html",{'instance_items': instance_items})

def BT9_Actor_TypeRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT9_Actor_Type, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT9_Actor_TypeRead.html",{'instance_items': instance_items})

def BT32_Persistent_TypeRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT32_Persistent_Type, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT32_Persistent_TypeRead.html",{'instance_items': instance_items})

def BC32_IdentifierRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC32_Identifier, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC32_IdentifierRead.html",{'instance_items': instance_items})

def BC64_Design_or_ProcedureRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC64_Design_or_Procedure, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC64_Design_or_ProcedureRead.html",{'instance_items': instance_items})

def BC79_RightRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC79_Right, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC79_RightRead.html",{'instance_items': instance_items})

def BC6_Persistent_ItemRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC6_Persistent_Item, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC6_Persistent_ItemRead.html",{'instance_items': instance_items})

def BT3_Physical_Thing_TypeRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT3_Physical_Thing_Type, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT3_Physical_Thing_TypeRead.html",{'instance_items': instance_items})

def BC17_Conceptual_ObjectRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC17_Conceptual_Object, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC17_Conceptual_ObjectRead.html",{'instance_items': instance_items})

def BT32_Persisten_TypeRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT32_Persisten_Type, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT32_Persisten_TypeRead.html",{'instance_items': instance_items})

def BC39_Marine_AnimalRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC39_Marine_Animal, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC39_Marine_AnimalRead.html",{'instance_items': instance_items})

def BC40_Physical_ManMade_ThingRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC40_Physical_ManMade_Thing, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC40_Physical_ManMade_ThingRead.html",{'instance_items': instance_items})

def BC41_ManMade_ObjectRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC41_ManMade_Object, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC41_ManMade_ObjectRead.html",{'instance_items': instance_items})

def BC42_CollectionRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC42_Collection, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC42_CollectionRead.html",{'instance_items': instance_items})

def BC46_Identifier_AssignmentRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC46_Identifier_Assignment, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC46_Identifier_AssignmentRead.html",{'instance_items': instance_items})

def BC47_ImageRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC47_Image, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC47_ImageRead.html",{'instance_items': instance_items})

def BC48_DatabaseRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC48_Database, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC48_DatabaseRead.html",{'instance_items': instance_items})

def BC53_SpecimenRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC53_Specimen, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC53_SpecimenRead.html",{'instance_items': instance_items})

def BC60_Software_ExecutionRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC60_Software_Execution, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC60_Software_ExecutionRead.html",{'instance_items': instance_items})

def BC62_Statistic_IndicatorRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC62_Statistic_Indicator, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC62_Statistic_IndicatorRead.html",{'instance_items': instance_items})

def BC63_Global_Statistic_LandingRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC63_Global_Statistic_Landing, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC63_Global_Statistic_LandingRead.html",{'instance_items': instance_items})

def BC70_GroupRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC70_Group, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC70_GroupRead.html",{'instance_items': instance_items})

def BC71_Name_Use_ActivityRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC71_Name_Use_Activity, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC71_Name_Use_ActivityRead.html",{'instance_items': instance_items})

def BC75_StockRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC75_Stock, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC75_StockRead.html",{'instance_items': instance_items})

def BC76_Stock_FormationRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC76_Stock_Formation, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC76_Stock_FormationRead.html",{'instance_items': instance_items})

def BC77_Matter_RemovalRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC77_Matter_Removal, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC77_Matter_RemovalRead.html",{'instance_items': instance_items})

def BC78_Amount_of_MatterRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC78_Amount_of_Matter, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC78_Amount_of_MatterRead.html",{'instance_items': instance_items})

def BT11_Equipment_TypeRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT11_Equipment_Type, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT11_Equipment_TypeRead.html",{'instance_items': instance_items})

def BT12_Scientific_Data_TypeRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT12_Scientific_Data_Type, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT12_Scientific_Data_TypeRead.html",{'instance_items': instance_items})

def BT13_Digital_Object_TypeRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT13_Digital_Object_Type, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT13_Digital_Object_TypeRead.html",{'instance_items': instance_items})

def BT16_Visual_TypeRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT16_Visual_Type, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT16_Visual_TypeRead.html",{'instance_items': instance_items})

def BT6_Human_Event_TypeRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT6_Human_Event_Type, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT6_Human_Event_TypeRead.html",{'instance_items': instance_items})

def BT18_KingdomRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT18_Kingdom, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT18_KingdomRead.html",{'instance_items': instance_items})

def BT19_PhylumRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT19_Phylum, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT19_PhylumRead.html",{'instance_items': instance_items})

def BT20_SubPhylumRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT20_SubPhylum, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT20_SubPhylumRead.html",{'instance_items': instance_items})

def BT21_SuperClassRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT21_SuperClass, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT21_SuperClassRead.html",{'instance_items': instance_items})

def BT35_Property_TypeRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT35_Property_Type, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT35_Property_TypeRead.html",{'instance_items': instance_items})

def BC25_Linguistic_ObjectRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC25_Linguistic_Object, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC25_Linguistic_ObjectRead.html",{'instance_items': instance_items})

def BT36_LanguageRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT36_Language, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT36_LanguageRead.html",{'instance_items': instance_items})

def BC11_PersonRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC11_Person, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC11_PersonRead.html",{'instance_items': instance_items})

def BC12_EcosystemRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC12_Ecosystem, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC12_EcosystemRead.html",{'instance_items': instance_items})

def BC13_OrganizationRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC13_Organization, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC13_OrganizationRead.html",{'instance_items': instance_items})

def BC14_Ecosystem_EnvironmentRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC14_Ecosystem_Environment, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC14_Ecosystem_EnvironmentRead.html",{'instance_items': instance_items})

def BC15_Water_AreaRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC15_Water_Area, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC15_Water_AreaRead.html",{'instance_items': instance_items})

def BC16_ManMade_thingRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC16_ManMade_thing, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC16_ManMade_thingRead.html",{'instance_items': instance_items})

def BC19_TitleRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC19_Title, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC19_TitleRead.html",{'instance_items': instance_items})

def BC21_DataSetRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC21_DataSet, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC21_DataSetRead.html",{'instance_items': instance_items})

def BC22_Encounter_EventRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC22_Encounter_Event, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC22_Encounter_EventRead.html",{'instance_items': instance_items})

def BC24_Repository_ObjectRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC24_Repository_Object, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC24_Repository_ObjectRead.html",{'instance_items': instance_items})

def BC26_Place_NameRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC26_Place_Name, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC26_Place_NameRead.html",{'instance_items': instance_items})

def BC31_Place_AppellationRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC31_Place_Appellation, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC31_Place_AppellationRead.html",{'instance_items': instance_items})

def BC27_PublicationRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC27_Publication, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC27_PublicationRead.html",{'instance_items': instance_items})

def BC33_Spatial_CoordinateRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC33_Spatial_Coordinate, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC33_Spatial_CoordinateRead.html",{'instance_items': instance_items})

def BC36_Abiotic_ElementRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC36_Abiotic_Element, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC36_Abiotic_ElementRead.html",{'instance_items': instance_items})

def BC37_Biological_ObjectRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC37_Biological_Object, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC37_Biological_ObjectRead.html",{'instance_items': instance_items})

def BC51_Physical_ObjectRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC51_Physical_Object, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC51_Physical_ObjectRead.html",{'instance_items': instance_items})

def BC38_Biotic_ElementRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BC38_Biotic_Element, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BC38_Biotic_ElementRead.html",{'instance_items': instance_items})

def BT22_ClassRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT22_Class, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT22_ClassRead.html",{'instance_items': instance_items})

def BT23_SubClassRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT23_SubClass, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT23_SubClassRead.html",{'instance_items': instance_items})

def BT24_FamilyRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT24_Family, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT24_FamilyRead.html",{'instance_items': instance_items})

def BT25_SubFamilyRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT25_SubFamily, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT25_SubFamilyRead.html",{'instance_items': instance_items})

def BT26_GenusRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT26_Genus, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT26_GenusRead.html",{'instance_items': instance_items})

def BT27_SpeciesRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT27_Species, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT27_SpeciesRead.html",{'instance_items': instance_items})

def BT28_Scientific_Activity_TypeRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT28_Scientific_Activity_Type, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT28_Scientific_Activity_TypeRead.html",{'instance_items': instance_items})

def BT29_Industrial_Activity_TypeRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT29_Industrial_Activity_Type, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT29_Industrial_Activity_TypeRead.html",{'instance_items': instance_items})

def BT30_Identifier_Assignment_TypeRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT30_Identifier_Assignment_Type, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT30_Identifier_Assignment_TypeRead.html",{'instance_items': instance_items})

def BT31_Biological_Part_TimeRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT31_Biological_Part_Time, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT31_Biological_Part_TimeRead.html",{'instance_items': instance_items})

def BT33_Marine_Animal_TypeRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT33_Marine_Animal_Type, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT33_Marine_Animal_TypeRead.html",{'instance_items': instance_items})

def BT34_OrderRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT34_Order, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT34_OrderRead.html",{'instance_items': instance_items})

def BT5_Legislative_Zone_TypeRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT5_Legislative_Zone_Type, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT5_Legislative_Zone_TypeRead.html",{'instance_items': instance_items})

def BT8_Abiotic_Element_TypeRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BT8_Abiotic_Element_Type, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BT8_Abiotic_Element_TypeRead.html",{'instance_items': instance_items})

def PersonRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Person, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"PersonRead.html",{'instance_items': instance_items})
