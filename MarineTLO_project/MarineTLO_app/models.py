from django.db import models

class BC35_Physical_Thing(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC10_Event(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC8_Actor(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC34_Geometric_Place_Expression(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC29_Spatial_Coordinate_Reference_System(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC56_Digital_Measurement_Event(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC23_Digital_Object(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC18_Proposition(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC59_Software(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC58_Digital_Device(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC61_Capture_Activity(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC57_Capture(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC54_Measurement(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC44_Attribute_Assignment(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC1_TLO_Entity(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC43_Activity(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC4_Temporal_Phenomenon(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC3_Place(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC45_Observation(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC9_Observable_Entity(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC7_Thing(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC5_Dimension(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC30_Appellation(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC55_Measurement_Unit(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC20_Declarative_Place(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC72_SpaceTime_Volume(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC2_Time_Span(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT14_Appellation_Type(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT2_Temporal_Phenomenon_Type(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT10_Biotic_Element_Type(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT7_Ecosystem_Type(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT4_Conceptual_Object_Type(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT1_TLO_Entity_Type(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT17_Human_Activity_Type(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT9_Actor_Type(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT32_Persistent_Type(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC32_Identifier(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC64_Design_or_Procedure(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC79_Right(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC6_Persistent_Item(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT3_Physical_Thing_Type(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC17_Conceptual_Object(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT32_Persisten_Type(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC39_Marine_Animal(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC40_Physical_ManMade_Thing(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC41_ManMade_Object(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC42_Collection(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC46_Identifier_Assignment(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC47_Image(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC48_Database(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC53_Specimen(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC60_Software_Execution(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC62_Statistic_Indicator(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC63_Global_Statistic_Landing(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC70_Group(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC71_Name_Use_Activity(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC75_Stock(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC76_Stock_Formation(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC77_Matter_Removal(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC78_Amount_of_Matter(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT11_Equipment_Type(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT12_Scientific_Data_Type(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT13_Digital_Object_Type(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT16_Visual_Type(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT6_Human_Event_Type(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT18_Kingdom(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT19_Phylum(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT20_SubPhylum(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT21_SuperClass(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT35_Property_Type(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC25_Linguistic_Object(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT36_Language(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC11_Person(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC12_Ecosystem(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC13_Organization(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC14_Ecosystem_Environment(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC15_Water_Area(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC16_ManMade_thing(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC19_Title(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC21_DataSet(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC22_Encounter_Event(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC24_Repository_Object(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC26_Place_Name(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC31_Place_Appellation(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC27_Publication(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC33_Spatial_Coordinate(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC36_Abiotic_Element(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC37_Biological_Object(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC51_Physical_Object(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BC38_Biotic_Element(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT22_Class(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT23_SubClass(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT24_Family(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT25_SubFamily(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT26_Genus(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT27_Species(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT28_Scientific_Activity_Type(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT29_Industrial_Activity_Type(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT30_Identifier_Assignment_Type(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT31_Biological_Part_Time(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT33_Marine_Animal_Type(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT34_Order(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT5_Legislative_Zone_Type(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BT8_Abiotic_Element_Type(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Person(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name
