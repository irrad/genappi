from django.conf.urls import url, include
from . import Read_view, Create_view, Update_view, Delete_view, List_view, Home_view, preferences, registration

urlpatterns = [
    url(r'^home/$', Home_view.HomeView, name='HomeView'),

    url(r'^preferences/$', preferences.define_preferences, name='define_preferences'),

    url(r'^signup/$', registration.signup, name='signup'),    

    url(r'^BC35_Physical_Thing/create/$', Create_view.BC35_Physical_ThingCreate, name='BC35_Physical_ThingCreate'),

    url(r'^BC10_Event/create/$', Create_view.BC10_EventCreate, name='BC10_EventCreate'),

    url(r'^BC8_Actor/create/$', Create_view.BC8_ActorCreate, name='BC8_ActorCreate'),

    url(r'^BC34_Geometric_Place_Expression/create/$', Create_view.BC34_Geometric_Place_ExpressionCreate, name='BC34_Geometric_Place_ExpressionCreate'),

    url(r'^BC29_Spatial_Coordinate_Reference_System/create/$', Create_view.BC29_Spatial_Coordinate_Reference_SystemCreate, name='BC29_Spatial_Coordinate_Reference_SystemCreate'),

    url(r'^BC56_Digital_Measurement_Event/create/$', Create_view.BC56_Digital_Measurement_EventCreate, name='BC56_Digital_Measurement_EventCreate'),

    url(r'^BC23_Digital_Object/create/$', Create_view.BC23_Digital_ObjectCreate, name='BC23_Digital_ObjectCreate'),

    url(r'^BC18_Proposition/create/$', Create_view.BC18_PropositionCreate, name='BC18_PropositionCreate'),

    url(r'^BC59_Software/create/$', Create_view.BC59_SoftwareCreate, name='BC59_SoftwareCreate'),

    url(r'^BC58_Digital_Device/create/$', Create_view.BC58_Digital_DeviceCreate, name='BC58_Digital_DeviceCreate'),

    url(r'^BC61_Capture_Activity/create/$', Create_view.BC61_Capture_ActivityCreate, name='BC61_Capture_ActivityCreate'),

    url(r'^BC57_Capture/create/$', Create_view.BC57_CaptureCreate, name='BC57_CaptureCreate'),

    url(r'^BC54_Measurement/create/$', Create_view.BC54_MeasurementCreate, name='BC54_MeasurementCreate'),

    url(r'^BC44_Attribute_Assignment/create/$', Create_view.BC44_Attribute_AssignmentCreate, name='BC44_Attribute_AssignmentCreate'),

    url(r'^BC1_TLO_Entity/create/$', Create_view.BC1_TLO_EntityCreate, name='BC1_TLO_EntityCreate'),

    url(r'^BC43_Activity/create/$', Create_view.BC43_ActivityCreate, name='BC43_ActivityCreate'),

    url(r'^BC4_Temporal_Phenomenon/create/$', Create_view.BC4_Temporal_PhenomenonCreate, name='BC4_Temporal_PhenomenonCreate'),

    url(r'^BC3_Place/create/$', Create_view.BC3_PlaceCreate, name='BC3_PlaceCreate'),

    url(r'^BC45_Observation/create/$', Create_view.BC45_ObservationCreate, name='BC45_ObservationCreate'),

    url(r'^BC9_Observable_Entity/create/$', Create_view.BC9_Observable_EntityCreate, name='BC9_Observable_EntityCreate'),

    url(r'^BC7_Thing/create/$', Create_view.BC7_ThingCreate, name='BC7_ThingCreate'),

    url(r'^BC5_Dimension/create/$', Create_view.BC5_DimensionCreate, name='BC5_DimensionCreate'),

    url(r'^BC30_Appellation/create/$', Create_view.BC30_AppellationCreate, name='BC30_AppellationCreate'),

    url(r'^BC55_Measurement_Unit/create/$', Create_view.BC55_Measurement_UnitCreate, name='BC55_Measurement_UnitCreate'),

    url(r'^BC20_Declarative_Place/create/$', Create_view.BC20_Declarative_PlaceCreate, name='BC20_Declarative_PlaceCreate'),

    url(r'^BC72_SpaceTime_Volume/create/$', Create_view.BC72_SpaceTime_VolumeCreate, name='BC72_SpaceTime_VolumeCreate'),

    url(r'^BC2_Time_Span/create/$', Create_view.BC2_Time_SpanCreate, name='BC2_Time_SpanCreate'),

    url(r'^BT14_Appellation_Type/create/$', Create_view.BT14_Appellation_TypeCreate, name='BT14_Appellation_TypeCreate'),

    url(r'^BT2_Temporal_Phenomenon_Type/create/$', Create_view.BT2_Temporal_Phenomenon_TypeCreate, name='BT2_Temporal_Phenomenon_TypeCreate'),

    url(r'^BT10_Biotic_Element_Type/create/$', Create_view.BT10_Biotic_Element_TypeCreate, name='BT10_Biotic_Element_TypeCreate'),

    url(r'^BT7_Ecosystem_Type/create/$', Create_view.BT7_Ecosystem_TypeCreate, name='BT7_Ecosystem_TypeCreate'),

    url(r'^BT4_Conceptual_Object_Type/create/$', Create_view.BT4_Conceptual_Object_TypeCreate, name='BT4_Conceptual_Object_TypeCreate'),

    url(r'^BT1_TLO_Entity_Type/create/$', Create_view.BT1_TLO_Entity_TypeCreate, name='BT1_TLO_Entity_TypeCreate'),

    url(r'^BT17_Human_Activity_Type/create/$', Create_view.BT17_Human_Activity_TypeCreate, name='BT17_Human_Activity_TypeCreate'),

    url(r'^BT9_Actor_Type/create/$', Create_view.BT9_Actor_TypeCreate, name='BT9_Actor_TypeCreate'),

    url(r'^BT32_Persistent_Type/create/$', Create_view.BT32_Persistent_TypeCreate, name='BT32_Persistent_TypeCreate'),

    url(r'^BC32_Identifier/create/$', Create_view.BC32_IdentifierCreate, name='BC32_IdentifierCreate'),

    url(r'^BC64_Design_or_Procedure/create/$', Create_view.BC64_Design_or_ProcedureCreate, name='BC64_Design_or_ProcedureCreate'),

    url(r'^BC79_Right/create/$', Create_view.BC79_RightCreate, name='BC79_RightCreate'),

    url(r'^BC6_Persistent_Item/create/$', Create_view.BC6_Persistent_ItemCreate, name='BC6_Persistent_ItemCreate'),

    url(r'^BT3_Physical_Thing_Type/create/$', Create_view.BT3_Physical_Thing_TypeCreate, name='BT3_Physical_Thing_TypeCreate'),

    url(r'^BC17_Conceptual_Object/create/$', Create_view.BC17_Conceptual_ObjectCreate, name='BC17_Conceptual_ObjectCreate'),

    url(r'^BT32_Persisten_Type/create/$', Create_view.BT32_Persisten_TypeCreate, name='BT32_Persisten_TypeCreate'),

    url(r'^BC39_Marine_Animal/create/$', Create_view.BC39_Marine_AnimalCreate, name='BC39_Marine_AnimalCreate'),

    url(r'^BC40_Physical_ManMade_Thing/create/$', Create_view.BC40_Physical_ManMade_ThingCreate, name='BC40_Physical_ManMade_ThingCreate'),

    url(r'^BC41_ManMade_Object/create/$', Create_view.BC41_ManMade_ObjectCreate, name='BC41_ManMade_ObjectCreate'),

    url(r'^BC42_Collection/create/$', Create_view.BC42_CollectionCreate, name='BC42_CollectionCreate'),

    url(r'^BC46_Identifier_Assignment/create/$', Create_view.BC46_Identifier_AssignmentCreate, name='BC46_Identifier_AssignmentCreate'),

    url(r'^BC47_Image/create/$', Create_view.BC47_ImageCreate, name='BC47_ImageCreate'),

    url(r'^BC48_Database/create/$', Create_view.BC48_DatabaseCreate, name='BC48_DatabaseCreate'),

    url(r'^BC53_Specimen/create/$', Create_view.BC53_SpecimenCreate, name='BC53_SpecimenCreate'),

    url(r'^BC60_Software_Execution/create/$', Create_view.BC60_Software_ExecutionCreate, name='BC60_Software_ExecutionCreate'),

    url(r'^BC62_Statistic_Indicator/create/$', Create_view.BC62_Statistic_IndicatorCreate, name='BC62_Statistic_IndicatorCreate'),

    url(r'^BC63_Global_Statistic_Landing/create/$', Create_view.BC63_Global_Statistic_LandingCreate, name='BC63_Global_Statistic_LandingCreate'),

    url(r'^BC70_Group/create/$', Create_view.BC70_GroupCreate, name='BC70_GroupCreate'),

    url(r'^BC71_Name_Use_Activity/create/$', Create_view.BC71_Name_Use_ActivityCreate, name='BC71_Name_Use_ActivityCreate'),

    url(r'^BC75_Stock/create/$', Create_view.BC75_StockCreate, name='BC75_StockCreate'),

    url(r'^BC76_Stock_Formation/create/$', Create_view.BC76_Stock_FormationCreate, name='BC76_Stock_FormationCreate'),

    url(r'^BC77_Matter_Removal/create/$', Create_view.BC77_Matter_RemovalCreate, name='BC77_Matter_RemovalCreate'),

    url(r'^BC78_Amount_of_Matter/create/$', Create_view.BC78_Amount_of_MatterCreate, name='BC78_Amount_of_MatterCreate'),

    url(r'^BT11_Equipment_Type/create/$', Create_view.BT11_Equipment_TypeCreate, name='BT11_Equipment_TypeCreate'),

    url(r'^BT12_Scientific_Data_Type/create/$', Create_view.BT12_Scientific_Data_TypeCreate, name='BT12_Scientific_Data_TypeCreate'),

    url(r'^BT13_Digital_Object_Type/create/$', Create_view.BT13_Digital_Object_TypeCreate, name='BT13_Digital_Object_TypeCreate'),

    url(r'^BT16_Visual_Type/create/$', Create_view.BT16_Visual_TypeCreate, name='BT16_Visual_TypeCreate'),

    url(r'^BT6_Human_Event_Type/create/$', Create_view.BT6_Human_Event_TypeCreate, name='BT6_Human_Event_TypeCreate'),

    url(r'^BT18_Kingdom/create/$', Create_view.BT18_KingdomCreate, name='BT18_KingdomCreate'),

    url(r'^BT19_Phylum/create/$', Create_view.BT19_PhylumCreate, name='BT19_PhylumCreate'),

    url(r'^BT20_SubPhylum/create/$', Create_view.BT20_SubPhylumCreate, name='BT20_SubPhylumCreate'),

    url(r'^BT21_SuperClass/create/$', Create_view.BT21_SuperClassCreate, name='BT21_SuperClassCreate'),

    url(r'^BT35_Property_Type/create/$', Create_view.BT35_Property_TypeCreate, name='BT35_Property_TypeCreate'),

    url(r'^BC25_Linguistic_Object/create/$', Create_view.BC25_Linguistic_ObjectCreate, name='BC25_Linguistic_ObjectCreate'),

    url(r'^BT36_Language/create/$', Create_view.BT36_LanguageCreate, name='BT36_LanguageCreate'),

    url(r'^BC11_Person/create/$', Create_view.BC11_PersonCreate, name='BC11_PersonCreate'),

    url(r'^BC12_Ecosystem/create/$', Create_view.BC12_EcosystemCreate, name='BC12_EcosystemCreate'),

    url(r'^BC13_Organization/create/$', Create_view.BC13_OrganizationCreate, name='BC13_OrganizationCreate'),

    url(r'^BC14_Ecosystem_Environment/create/$', Create_view.BC14_Ecosystem_EnvironmentCreate, name='BC14_Ecosystem_EnvironmentCreate'),

    url(r'^BC15_Water_Area/create/$', Create_view.BC15_Water_AreaCreate, name='BC15_Water_AreaCreate'),

    url(r'^BC16_ManMade_thing/create/$', Create_view.BC16_ManMade_thingCreate, name='BC16_ManMade_thingCreate'),

    url(r'^BC19_Title/create/$', Create_view.BC19_TitleCreate, name='BC19_TitleCreate'),

    url(r'^BC21_DataSet/create/$', Create_view.BC21_DataSetCreate, name='BC21_DataSetCreate'),

    url(r'^BC22_Encounter_Event/create/$', Create_view.BC22_Encounter_EventCreate, name='BC22_Encounter_EventCreate'),

    url(r'^BC24_Repository_Object/create/$', Create_view.BC24_Repository_ObjectCreate, name='BC24_Repository_ObjectCreate'),

    url(r'^BC26_Place_Name/create/$', Create_view.BC26_Place_NameCreate, name='BC26_Place_NameCreate'),

    url(r'^BC31_Place_Appellation/create/$', Create_view.BC31_Place_AppellationCreate, name='BC31_Place_AppellationCreate'),

    url(r'^BC27_Publication/create/$', Create_view.BC27_PublicationCreate, name='BC27_PublicationCreate'),

    url(r'^BC33_Spatial_Coordinate/create/$', Create_view.BC33_Spatial_CoordinateCreate, name='BC33_Spatial_CoordinateCreate'),

    url(r'^BC36_Abiotic_Element/create/$', Create_view.BC36_Abiotic_ElementCreate, name='BC36_Abiotic_ElementCreate'),

    url(r'^BC37_Biological_Object/create/$', Create_view.BC37_Biological_ObjectCreate, name='BC37_Biological_ObjectCreate'),

    url(r'^BC51_Physical_Object/create/$', Create_view.BC51_Physical_ObjectCreate, name='BC51_Physical_ObjectCreate'),

    url(r'^BC38_Biotic_Element/create/$', Create_view.BC38_Biotic_ElementCreate, name='BC38_Biotic_ElementCreate'),

    url(r'^BT22_Class/create/$', Create_view.BT22_ClassCreate, name='BT22_ClassCreate'),

    url(r'^BT23_SubClass/create/$', Create_view.BT23_SubClassCreate, name='BT23_SubClassCreate'),

    url(r'^BT24_Family/create/$', Create_view.BT24_FamilyCreate, name='BT24_FamilyCreate'),

    url(r'^BT25_SubFamily/create/$', Create_view.BT25_SubFamilyCreate, name='BT25_SubFamilyCreate'),

    url(r'^BT26_Genus/create/$', Create_view.BT26_GenusCreate, name='BT26_GenusCreate'),

    url(r'^BT27_Species/create/$', Create_view.BT27_SpeciesCreate, name='BT27_SpeciesCreate'),

    url(r'^BT28_Scientific_Activity_Type/create/$', Create_view.BT28_Scientific_Activity_TypeCreate, name='BT28_Scientific_Activity_TypeCreate'),

    url(r'^BT29_Industrial_Activity_Type/create/$', Create_view.BT29_Industrial_Activity_TypeCreate, name='BT29_Industrial_Activity_TypeCreate'),

    url(r'^BT30_Identifier_Assignment_Type/create/$', Create_view.BT30_Identifier_Assignment_TypeCreate, name='BT30_Identifier_Assignment_TypeCreate'),

    url(r'^BT31_Biological_Part_Time/create/$', Create_view.BT31_Biological_Part_TimeCreate, name='BT31_Biological_Part_TimeCreate'),

    url(r'^BT33_Marine_Animal_Type/create/$', Create_view.BT33_Marine_Animal_TypeCreate, name='BT33_Marine_Animal_TypeCreate'),

    url(r'^BT34_Order/create/$', Create_view.BT34_OrderCreate, name='BT34_OrderCreate'),

    url(r'^BT5_Legislative_Zone_Type/create/$', Create_view.BT5_Legislative_Zone_TypeCreate, name='BT5_Legislative_Zone_TypeCreate'),

    url(r'^BT8_Abiotic_Element_Type/create/$', Create_view.BT8_Abiotic_Element_TypeCreate, name='BT8_Abiotic_Element_TypeCreate'),

    url(r'^Person/create/$', Create_view.PersonCreate, name='PersonCreate'),


    url(r'^BC35_Physical_Thing/(?P<pk>\d+)/read/$', Read_view.BC35_Physical_ThingRead, name='BC35_Physical_ThingRead'),

    url(r'^BC10_Event/(?P<pk>\d+)/read/$', Read_view.BC10_EventRead, name='BC10_EventRead'),

    url(r'^BC8_Actor/(?P<pk>\d+)/read/$', Read_view.BC8_ActorRead, name='BC8_ActorRead'),

    url(r'^BC34_Geometric_Place_Expression/(?P<pk>\d+)/read/$', Read_view.BC34_Geometric_Place_ExpressionRead, name='BC34_Geometric_Place_ExpressionRead'),

    url(r'^BC29_Spatial_Coordinate_Reference_System/(?P<pk>\d+)/read/$', Read_view.BC29_Spatial_Coordinate_Reference_SystemRead, name='BC29_Spatial_Coordinate_Reference_SystemRead'),

    url(r'^BC56_Digital_Measurement_Event/(?P<pk>\d+)/read/$', Read_view.BC56_Digital_Measurement_EventRead, name='BC56_Digital_Measurement_EventRead'),

    url(r'^BC23_Digital_Object/(?P<pk>\d+)/read/$', Read_view.BC23_Digital_ObjectRead, name='BC23_Digital_ObjectRead'),

    url(r'^BC18_Proposition/(?P<pk>\d+)/read/$', Read_view.BC18_PropositionRead, name='BC18_PropositionRead'),

    url(r'^BC59_Software/(?P<pk>\d+)/read/$', Read_view.BC59_SoftwareRead, name='BC59_SoftwareRead'),

    url(r'^BC58_Digital_Device/(?P<pk>\d+)/read/$', Read_view.BC58_Digital_DeviceRead, name='BC58_Digital_DeviceRead'),

    url(r'^BC61_Capture_Activity/(?P<pk>\d+)/read/$', Read_view.BC61_Capture_ActivityRead, name='BC61_Capture_ActivityRead'),

    url(r'^BC57_Capture/(?P<pk>\d+)/read/$', Read_view.BC57_CaptureRead, name='BC57_CaptureRead'),

    url(r'^BC54_Measurement/(?P<pk>\d+)/read/$', Read_view.BC54_MeasurementRead, name='BC54_MeasurementRead'),

    url(r'^BC44_Attribute_Assignment/(?P<pk>\d+)/read/$', Read_view.BC44_Attribute_AssignmentRead, name='BC44_Attribute_AssignmentRead'),

    url(r'^BC1_TLO_Entity/(?P<pk>\d+)/read/$', Read_view.BC1_TLO_EntityRead, name='BC1_TLO_EntityRead'),

    url(r'^BC43_Activity/(?P<pk>\d+)/read/$', Read_view.BC43_ActivityRead, name='BC43_ActivityRead'),

    url(r'^BC4_Temporal_Phenomenon/(?P<pk>\d+)/read/$', Read_view.BC4_Temporal_PhenomenonRead, name='BC4_Temporal_PhenomenonRead'),

    url(r'^BC3_Place/(?P<pk>\d+)/read/$', Read_view.BC3_PlaceRead, name='BC3_PlaceRead'),

    url(r'^BC45_Observation/(?P<pk>\d+)/read/$', Read_view.BC45_ObservationRead, name='BC45_ObservationRead'),

    url(r'^BC9_Observable_Entity/(?P<pk>\d+)/read/$', Read_view.BC9_Observable_EntityRead, name='BC9_Observable_EntityRead'),

    url(r'^BC7_Thing/(?P<pk>\d+)/read/$', Read_view.BC7_ThingRead, name='BC7_ThingRead'),

    url(r'^BC5_Dimension/(?P<pk>\d+)/read/$', Read_view.BC5_DimensionRead, name='BC5_DimensionRead'),

    url(r'^BC30_Appellation/(?P<pk>\d+)/read/$', Read_view.BC30_AppellationRead, name='BC30_AppellationRead'),

    url(r'^BC55_Measurement_Unit/(?P<pk>\d+)/read/$', Read_view.BC55_Measurement_UnitRead, name='BC55_Measurement_UnitRead'),

    url(r'^BC20_Declarative_Place/(?P<pk>\d+)/read/$', Read_view.BC20_Declarative_PlaceRead, name='BC20_Declarative_PlaceRead'),

    url(r'^BC72_SpaceTime_Volume/(?P<pk>\d+)/read/$', Read_view.BC72_SpaceTime_VolumeRead, name='BC72_SpaceTime_VolumeRead'),

    url(r'^BC2_Time_Span/(?P<pk>\d+)/read/$', Read_view.BC2_Time_SpanRead, name='BC2_Time_SpanRead'),

    url(r'^BT14_Appellation_Type/(?P<pk>\d+)/read/$', Read_view.BT14_Appellation_TypeRead, name='BT14_Appellation_TypeRead'),

    url(r'^BT2_Temporal_Phenomenon_Type/(?P<pk>\d+)/read/$', Read_view.BT2_Temporal_Phenomenon_TypeRead, name='BT2_Temporal_Phenomenon_TypeRead'),

    url(r'^BT10_Biotic_Element_Type/(?P<pk>\d+)/read/$', Read_view.BT10_Biotic_Element_TypeRead, name='BT10_Biotic_Element_TypeRead'),

    url(r'^BT7_Ecosystem_Type/(?P<pk>\d+)/read/$', Read_view.BT7_Ecosystem_TypeRead, name='BT7_Ecosystem_TypeRead'),

    url(r'^BT4_Conceptual_Object_Type/(?P<pk>\d+)/read/$', Read_view.BT4_Conceptual_Object_TypeRead, name='BT4_Conceptual_Object_TypeRead'),

    url(r'^BT1_TLO_Entity_Type/(?P<pk>\d+)/read/$', Read_view.BT1_TLO_Entity_TypeRead, name='BT1_TLO_Entity_TypeRead'),

    url(r'^BT17_Human_Activity_Type/(?P<pk>\d+)/read/$', Read_view.BT17_Human_Activity_TypeRead, name='BT17_Human_Activity_TypeRead'),

    url(r'^BT9_Actor_Type/(?P<pk>\d+)/read/$', Read_view.BT9_Actor_TypeRead, name='BT9_Actor_TypeRead'),

    url(r'^BT32_Persistent_Type/(?P<pk>\d+)/read/$', Read_view.BT32_Persistent_TypeRead, name='BT32_Persistent_TypeRead'),

    url(r'^BC32_Identifier/(?P<pk>\d+)/read/$', Read_view.BC32_IdentifierRead, name='BC32_IdentifierRead'),

    url(r'^BC64_Design_or_Procedure/(?P<pk>\d+)/read/$', Read_view.BC64_Design_or_ProcedureRead, name='BC64_Design_or_ProcedureRead'),

    url(r'^BC79_Right/(?P<pk>\d+)/read/$', Read_view.BC79_RightRead, name='BC79_RightRead'),

    url(r'^BC6_Persistent_Item/(?P<pk>\d+)/read/$', Read_view.BC6_Persistent_ItemRead, name='BC6_Persistent_ItemRead'),

    url(r'^BT3_Physical_Thing_Type/(?P<pk>\d+)/read/$', Read_view.BT3_Physical_Thing_TypeRead, name='BT3_Physical_Thing_TypeRead'),

    url(r'^BC17_Conceptual_Object/(?P<pk>\d+)/read/$', Read_view.BC17_Conceptual_ObjectRead, name='BC17_Conceptual_ObjectRead'),

    url(r'^BT32_Persisten_Type/(?P<pk>\d+)/read/$', Read_view.BT32_Persisten_TypeRead, name='BT32_Persisten_TypeRead'),

    url(r'^BC39_Marine_Animal/(?P<pk>\d+)/read/$', Read_view.BC39_Marine_AnimalRead, name='BC39_Marine_AnimalRead'),

    url(r'^BC40_Physical_ManMade_Thing/(?P<pk>\d+)/read/$', Read_view.BC40_Physical_ManMade_ThingRead, name='BC40_Physical_ManMade_ThingRead'),

    url(r'^BC41_ManMade_Object/(?P<pk>\d+)/read/$', Read_view.BC41_ManMade_ObjectRead, name='BC41_ManMade_ObjectRead'),

    url(r'^BC42_Collection/(?P<pk>\d+)/read/$', Read_view.BC42_CollectionRead, name='BC42_CollectionRead'),

    url(r'^BC46_Identifier_Assignment/(?P<pk>\d+)/read/$', Read_view.BC46_Identifier_AssignmentRead, name='BC46_Identifier_AssignmentRead'),

    url(r'^BC47_Image/(?P<pk>\d+)/read/$', Read_view.BC47_ImageRead, name='BC47_ImageRead'),

    url(r'^BC48_Database/(?P<pk>\d+)/read/$', Read_view.BC48_DatabaseRead, name='BC48_DatabaseRead'),

    url(r'^BC53_Specimen/(?P<pk>\d+)/read/$', Read_view.BC53_SpecimenRead, name='BC53_SpecimenRead'),

    url(r'^BC60_Software_Execution/(?P<pk>\d+)/read/$', Read_view.BC60_Software_ExecutionRead, name='BC60_Software_ExecutionRead'),

    url(r'^BC62_Statistic_Indicator/(?P<pk>\d+)/read/$', Read_view.BC62_Statistic_IndicatorRead, name='BC62_Statistic_IndicatorRead'),

    url(r'^BC63_Global_Statistic_Landing/(?P<pk>\d+)/read/$', Read_view.BC63_Global_Statistic_LandingRead, name='BC63_Global_Statistic_LandingRead'),

    url(r'^BC70_Group/(?P<pk>\d+)/read/$', Read_view.BC70_GroupRead, name='BC70_GroupRead'),

    url(r'^BC71_Name_Use_Activity/(?P<pk>\d+)/read/$', Read_view.BC71_Name_Use_ActivityRead, name='BC71_Name_Use_ActivityRead'),

    url(r'^BC75_Stock/(?P<pk>\d+)/read/$', Read_view.BC75_StockRead, name='BC75_StockRead'),

    url(r'^BC76_Stock_Formation/(?P<pk>\d+)/read/$', Read_view.BC76_Stock_FormationRead, name='BC76_Stock_FormationRead'),

    url(r'^BC77_Matter_Removal/(?P<pk>\d+)/read/$', Read_view.BC77_Matter_RemovalRead, name='BC77_Matter_RemovalRead'),

    url(r'^BC78_Amount_of_Matter/(?P<pk>\d+)/read/$', Read_view.BC78_Amount_of_MatterRead, name='BC78_Amount_of_MatterRead'),

    url(r'^BT11_Equipment_Type/(?P<pk>\d+)/read/$', Read_view.BT11_Equipment_TypeRead, name='BT11_Equipment_TypeRead'),

    url(r'^BT12_Scientific_Data_Type/(?P<pk>\d+)/read/$', Read_view.BT12_Scientific_Data_TypeRead, name='BT12_Scientific_Data_TypeRead'),

    url(r'^BT13_Digital_Object_Type/(?P<pk>\d+)/read/$', Read_view.BT13_Digital_Object_TypeRead, name='BT13_Digital_Object_TypeRead'),

    url(r'^BT16_Visual_Type/(?P<pk>\d+)/read/$', Read_view.BT16_Visual_TypeRead, name='BT16_Visual_TypeRead'),

    url(r'^BT6_Human_Event_Type/(?P<pk>\d+)/read/$', Read_view.BT6_Human_Event_TypeRead, name='BT6_Human_Event_TypeRead'),

    url(r'^BT18_Kingdom/(?P<pk>\d+)/read/$', Read_view.BT18_KingdomRead, name='BT18_KingdomRead'),

    url(r'^BT19_Phylum/(?P<pk>\d+)/read/$', Read_view.BT19_PhylumRead, name='BT19_PhylumRead'),

    url(r'^BT20_SubPhylum/(?P<pk>\d+)/read/$', Read_view.BT20_SubPhylumRead, name='BT20_SubPhylumRead'),

    url(r'^BT21_SuperClass/(?P<pk>\d+)/read/$', Read_view.BT21_SuperClassRead, name='BT21_SuperClassRead'),

    url(r'^BT35_Property_Type/(?P<pk>\d+)/read/$', Read_view.BT35_Property_TypeRead, name='BT35_Property_TypeRead'),

    url(r'^BC25_Linguistic_Object/(?P<pk>\d+)/read/$', Read_view.BC25_Linguistic_ObjectRead, name='BC25_Linguistic_ObjectRead'),

    url(r'^BT36_Language/(?P<pk>\d+)/read/$', Read_view.BT36_LanguageRead, name='BT36_LanguageRead'),

    url(r'^BC11_Person/(?P<pk>\d+)/read/$', Read_view.BC11_PersonRead, name='BC11_PersonRead'),

    url(r'^BC12_Ecosystem/(?P<pk>\d+)/read/$', Read_view.BC12_EcosystemRead, name='BC12_EcosystemRead'),

    url(r'^BC13_Organization/(?P<pk>\d+)/read/$', Read_view.BC13_OrganizationRead, name='BC13_OrganizationRead'),

    url(r'^BC14_Ecosystem_Environment/(?P<pk>\d+)/read/$', Read_view.BC14_Ecosystem_EnvironmentRead, name='BC14_Ecosystem_EnvironmentRead'),

    url(r'^BC15_Water_Area/(?P<pk>\d+)/read/$', Read_view.BC15_Water_AreaRead, name='BC15_Water_AreaRead'),

    url(r'^BC16_ManMade_thing/(?P<pk>\d+)/read/$', Read_view.BC16_ManMade_thingRead, name='BC16_ManMade_thingRead'),

    url(r'^BC19_Title/(?P<pk>\d+)/read/$', Read_view.BC19_TitleRead, name='BC19_TitleRead'),

    url(r'^BC21_DataSet/(?P<pk>\d+)/read/$', Read_view.BC21_DataSetRead, name='BC21_DataSetRead'),

    url(r'^BC22_Encounter_Event/(?P<pk>\d+)/read/$', Read_view.BC22_Encounter_EventRead, name='BC22_Encounter_EventRead'),

    url(r'^BC24_Repository_Object/(?P<pk>\d+)/read/$', Read_view.BC24_Repository_ObjectRead, name='BC24_Repository_ObjectRead'),

    url(r'^BC26_Place_Name/(?P<pk>\d+)/read/$', Read_view.BC26_Place_NameRead, name='BC26_Place_NameRead'),

    url(r'^BC31_Place_Appellation/(?P<pk>\d+)/read/$', Read_view.BC31_Place_AppellationRead, name='BC31_Place_AppellationRead'),

    url(r'^BC27_Publication/(?P<pk>\d+)/read/$', Read_view.BC27_PublicationRead, name='BC27_PublicationRead'),

    url(r'^BC33_Spatial_Coordinate/(?P<pk>\d+)/read/$', Read_view.BC33_Spatial_CoordinateRead, name='BC33_Spatial_CoordinateRead'),

    url(r'^BC36_Abiotic_Element/(?P<pk>\d+)/read/$', Read_view.BC36_Abiotic_ElementRead, name='BC36_Abiotic_ElementRead'),

    url(r'^BC37_Biological_Object/(?P<pk>\d+)/read/$', Read_view.BC37_Biological_ObjectRead, name='BC37_Biological_ObjectRead'),

    url(r'^BC51_Physical_Object/(?P<pk>\d+)/read/$', Read_view.BC51_Physical_ObjectRead, name='BC51_Physical_ObjectRead'),

    url(r'^BC38_Biotic_Element/(?P<pk>\d+)/read/$', Read_view.BC38_Biotic_ElementRead, name='BC38_Biotic_ElementRead'),

    url(r'^BT22_Class/(?P<pk>\d+)/read/$', Read_view.BT22_ClassRead, name='BT22_ClassRead'),

    url(r'^BT23_SubClass/(?P<pk>\d+)/read/$', Read_view.BT23_SubClassRead, name='BT23_SubClassRead'),

    url(r'^BT24_Family/(?P<pk>\d+)/read/$', Read_view.BT24_FamilyRead, name='BT24_FamilyRead'),

    url(r'^BT25_SubFamily/(?P<pk>\d+)/read/$', Read_view.BT25_SubFamilyRead, name='BT25_SubFamilyRead'),

    url(r'^BT26_Genus/(?P<pk>\d+)/read/$', Read_view.BT26_GenusRead, name='BT26_GenusRead'),

    url(r'^BT27_Species/(?P<pk>\d+)/read/$', Read_view.BT27_SpeciesRead, name='BT27_SpeciesRead'),

    url(r'^BT28_Scientific_Activity_Type/(?P<pk>\d+)/read/$', Read_view.BT28_Scientific_Activity_TypeRead, name='BT28_Scientific_Activity_TypeRead'),

    url(r'^BT29_Industrial_Activity_Type/(?P<pk>\d+)/read/$', Read_view.BT29_Industrial_Activity_TypeRead, name='BT29_Industrial_Activity_TypeRead'),

    url(r'^BT30_Identifier_Assignment_Type/(?P<pk>\d+)/read/$', Read_view.BT30_Identifier_Assignment_TypeRead, name='BT30_Identifier_Assignment_TypeRead'),

    url(r'^BT31_Biological_Part_Time/(?P<pk>\d+)/read/$', Read_view.BT31_Biological_Part_TimeRead, name='BT31_Biological_Part_TimeRead'),

    url(r'^BT33_Marine_Animal_Type/(?P<pk>\d+)/read/$', Read_view.BT33_Marine_Animal_TypeRead, name='BT33_Marine_Animal_TypeRead'),

    url(r'^BT34_Order/(?P<pk>\d+)/read/$', Read_view.BT34_OrderRead, name='BT34_OrderRead'),

    url(r'^BT5_Legislative_Zone_Type/(?P<pk>\d+)/read/$', Read_view.BT5_Legislative_Zone_TypeRead, name='BT5_Legislative_Zone_TypeRead'),

    url(r'^BT8_Abiotic_Element_Type/(?P<pk>\d+)/read/$', Read_view.BT8_Abiotic_Element_TypeRead, name='BT8_Abiotic_Element_TypeRead'),

    url(r'^Person/(?P<pk>\d+)/read/$', Read_view.PersonRead, name='PersonRead'),


    url(r'^BC35_Physical_Thing/list/$', List_view.BC35_Physical_ThingList, name='BC35_Physical_ThingList'),

    url(r'^BC10_Event/list/$', List_view.BC10_EventList, name='BC10_EventList'),

    url(r'^BC8_Actor/list/$', List_view.BC8_ActorList, name='BC8_ActorList'),

    url(r'^BC34_Geometric_Place_Expression/list/$', List_view.BC34_Geometric_Place_ExpressionList, name='BC34_Geometric_Place_ExpressionList'),

    url(r'^BC29_Spatial_Coordinate_Reference_System/list/$', List_view.BC29_Spatial_Coordinate_Reference_SystemList, name='BC29_Spatial_Coordinate_Reference_SystemList'),

    url(r'^BC56_Digital_Measurement_Event/list/$', List_view.BC56_Digital_Measurement_EventList, name='BC56_Digital_Measurement_EventList'),

    url(r'^BC23_Digital_Object/list/$', List_view.BC23_Digital_ObjectList, name='BC23_Digital_ObjectList'),

    url(r'^BC18_Proposition/list/$', List_view.BC18_PropositionList, name='BC18_PropositionList'),

    url(r'^BC59_Software/list/$', List_view.BC59_SoftwareList, name='BC59_SoftwareList'),

    url(r'^BC58_Digital_Device/list/$', List_view.BC58_Digital_DeviceList, name='BC58_Digital_DeviceList'),

    url(r'^BC61_Capture_Activity/list/$', List_view.BC61_Capture_ActivityList, name='BC61_Capture_ActivityList'),

    url(r'^BC57_Capture/list/$', List_view.BC57_CaptureList, name='BC57_CaptureList'),

    url(r'^BC54_Measurement/list/$', List_view.BC54_MeasurementList, name='BC54_MeasurementList'),

    url(r'^BC44_Attribute_Assignment/list/$', List_view.BC44_Attribute_AssignmentList, name='BC44_Attribute_AssignmentList'),

    url(r'^BC1_TLO_Entity/list/$', List_view.BC1_TLO_EntityList, name='BC1_TLO_EntityList'),

    url(r'^BC43_Activity/list/$', List_view.BC43_ActivityList, name='BC43_ActivityList'),

    url(r'^BC4_Temporal_Phenomenon/list/$', List_view.BC4_Temporal_PhenomenonList, name='BC4_Temporal_PhenomenonList'),

    url(r'^BC3_Place/list/$', List_view.BC3_PlaceList, name='BC3_PlaceList'),

    url(r'^BC45_Observation/list/$', List_view.BC45_ObservationList, name='BC45_ObservationList'),

    url(r'^BC9_Observable_Entity/list/$', List_view.BC9_Observable_EntityList, name='BC9_Observable_EntityList'),

    url(r'^BC7_Thing/list/$', List_view.BC7_ThingList, name='BC7_ThingList'),

    url(r'^BC5_Dimension/list/$', List_view.BC5_DimensionList, name='BC5_DimensionList'),

    url(r'^BC30_Appellation/list/$', List_view.BC30_AppellationList, name='BC30_AppellationList'),

    url(r'^BC55_Measurement_Unit/list/$', List_view.BC55_Measurement_UnitList, name='BC55_Measurement_UnitList'),

    url(r'^BC20_Declarative_Place/list/$', List_view.BC20_Declarative_PlaceList, name='BC20_Declarative_PlaceList'),

    url(r'^BC72_SpaceTime_Volume/list/$', List_view.BC72_SpaceTime_VolumeList, name='BC72_SpaceTime_VolumeList'),

    url(r'^BC2_Time_Span/list/$', List_view.BC2_Time_SpanList, name='BC2_Time_SpanList'),

    url(r'^BT14_Appellation_Type/list/$', List_view.BT14_Appellation_TypeList, name='BT14_Appellation_TypeList'),

    url(r'^BT2_Temporal_Phenomenon_Type/list/$', List_view.BT2_Temporal_Phenomenon_TypeList, name='BT2_Temporal_Phenomenon_TypeList'),

    url(r'^BT10_Biotic_Element_Type/list/$', List_view.BT10_Biotic_Element_TypeList, name='BT10_Biotic_Element_TypeList'),

    url(r'^BT7_Ecosystem_Type/list/$', List_view.BT7_Ecosystem_TypeList, name='BT7_Ecosystem_TypeList'),

    url(r'^BT4_Conceptual_Object_Type/list/$', List_view.BT4_Conceptual_Object_TypeList, name='BT4_Conceptual_Object_TypeList'),

    url(r'^BT1_TLO_Entity_Type/list/$', List_view.BT1_TLO_Entity_TypeList, name='BT1_TLO_Entity_TypeList'),

    url(r'^BT17_Human_Activity_Type/list/$', List_view.BT17_Human_Activity_TypeList, name='BT17_Human_Activity_TypeList'),

    url(r'^BT9_Actor_Type/list/$', List_view.BT9_Actor_TypeList, name='BT9_Actor_TypeList'),

    url(r'^BT32_Persistent_Type/list/$', List_view.BT32_Persistent_TypeList, name='BT32_Persistent_TypeList'),

    url(r'^BC32_Identifier/list/$', List_view.BC32_IdentifierList, name='BC32_IdentifierList'),

    url(r'^BC64_Design_or_Procedure/list/$', List_view.BC64_Design_or_ProcedureList, name='BC64_Design_or_ProcedureList'),

    url(r'^BC79_Right/list/$', List_view.BC79_RightList, name='BC79_RightList'),

    url(r'^BC6_Persistent_Item/list/$', List_view.BC6_Persistent_ItemList, name='BC6_Persistent_ItemList'),

    url(r'^BT3_Physical_Thing_Type/list/$', List_view.BT3_Physical_Thing_TypeList, name='BT3_Physical_Thing_TypeList'),

    url(r'^BC17_Conceptual_Object/list/$', List_view.BC17_Conceptual_ObjectList, name='BC17_Conceptual_ObjectList'),

    url(r'^BT32_Persisten_Type/list/$', List_view.BT32_Persisten_TypeList, name='BT32_Persisten_TypeList'),

    url(r'^BC39_Marine_Animal/list/$', List_view.BC39_Marine_AnimalList, name='BC39_Marine_AnimalList'),

    url(r'^BC40_Physical_ManMade_Thing/list/$', List_view.BC40_Physical_ManMade_ThingList, name='BC40_Physical_ManMade_ThingList'),

    url(r'^BC41_ManMade_Object/list/$', List_view.BC41_ManMade_ObjectList, name='BC41_ManMade_ObjectList'),

    url(r'^BC42_Collection/list/$', List_view.BC42_CollectionList, name='BC42_CollectionList'),

    url(r'^BC46_Identifier_Assignment/list/$', List_view.BC46_Identifier_AssignmentList, name='BC46_Identifier_AssignmentList'),

    url(r'^BC47_Image/list/$', List_view.BC47_ImageList, name='BC47_ImageList'),

    url(r'^BC48_Database/list/$', List_view.BC48_DatabaseList, name='BC48_DatabaseList'),

    url(r'^BC53_Specimen/list/$', List_view.BC53_SpecimenList, name='BC53_SpecimenList'),

    url(r'^BC60_Software_Execution/list/$', List_view.BC60_Software_ExecutionList, name='BC60_Software_ExecutionList'),

    url(r'^BC62_Statistic_Indicator/list/$', List_view.BC62_Statistic_IndicatorList, name='BC62_Statistic_IndicatorList'),

    url(r'^BC63_Global_Statistic_Landing/list/$', List_view.BC63_Global_Statistic_LandingList, name='BC63_Global_Statistic_LandingList'),

    url(r'^BC70_Group/list/$', List_view.BC70_GroupList, name='BC70_GroupList'),

    url(r'^BC71_Name_Use_Activity/list/$', List_view.BC71_Name_Use_ActivityList, name='BC71_Name_Use_ActivityList'),

    url(r'^BC75_Stock/list/$', List_view.BC75_StockList, name='BC75_StockList'),

    url(r'^BC76_Stock_Formation/list/$', List_view.BC76_Stock_FormationList, name='BC76_Stock_FormationList'),

    url(r'^BC77_Matter_Removal/list/$', List_view.BC77_Matter_RemovalList, name='BC77_Matter_RemovalList'),

    url(r'^BC78_Amount_of_Matter/list/$', List_view.BC78_Amount_of_MatterList, name='BC78_Amount_of_MatterList'),

    url(r'^BT11_Equipment_Type/list/$', List_view.BT11_Equipment_TypeList, name='BT11_Equipment_TypeList'),

    url(r'^BT12_Scientific_Data_Type/list/$', List_view.BT12_Scientific_Data_TypeList, name='BT12_Scientific_Data_TypeList'),

    url(r'^BT13_Digital_Object_Type/list/$', List_view.BT13_Digital_Object_TypeList, name='BT13_Digital_Object_TypeList'),

    url(r'^BT16_Visual_Type/list/$', List_view.BT16_Visual_TypeList, name='BT16_Visual_TypeList'),

    url(r'^BT6_Human_Event_Type/list/$', List_view.BT6_Human_Event_TypeList, name='BT6_Human_Event_TypeList'),

    url(r'^BT18_Kingdom/list/$', List_view.BT18_KingdomList, name='BT18_KingdomList'),

    url(r'^BT19_Phylum/list/$', List_view.BT19_PhylumList, name='BT19_PhylumList'),

    url(r'^BT20_SubPhylum/list/$', List_view.BT20_SubPhylumList, name='BT20_SubPhylumList'),

    url(r'^BT21_SuperClass/list/$', List_view.BT21_SuperClassList, name='BT21_SuperClassList'),

    url(r'^BT35_Property_Type/list/$', List_view.BT35_Property_TypeList, name='BT35_Property_TypeList'),

    url(r'^BC25_Linguistic_Object/list/$', List_view.BC25_Linguistic_ObjectList, name='BC25_Linguistic_ObjectList'),

    url(r'^BT36_Language/list/$', List_view.BT36_LanguageList, name='BT36_LanguageList'),

    url(r'^BC11_Person/list/$', List_view.BC11_PersonList, name='BC11_PersonList'),

    url(r'^BC12_Ecosystem/list/$', List_view.BC12_EcosystemList, name='BC12_EcosystemList'),

    url(r'^BC13_Organization/list/$', List_view.BC13_OrganizationList, name='BC13_OrganizationList'),

    url(r'^BC14_Ecosystem_Environment/list/$', List_view.BC14_Ecosystem_EnvironmentList, name='BC14_Ecosystem_EnvironmentList'),

    url(r'^BC15_Water_Area/list/$', List_view.BC15_Water_AreaList, name='BC15_Water_AreaList'),

    url(r'^BC16_ManMade_thing/list/$', List_view.BC16_ManMade_thingList, name='BC16_ManMade_thingList'),

    url(r'^BC19_Title/list/$', List_view.BC19_TitleList, name='BC19_TitleList'),

    url(r'^BC21_DataSet/list/$', List_view.BC21_DataSetList, name='BC21_DataSetList'),

    url(r'^BC22_Encounter_Event/list/$', List_view.BC22_Encounter_EventList, name='BC22_Encounter_EventList'),

    url(r'^BC24_Repository_Object/list/$', List_view.BC24_Repository_ObjectList, name='BC24_Repository_ObjectList'),

    url(r'^BC26_Place_Name/list/$', List_view.BC26_Place_NameList, name='BC26_Place_NameList'),

    url(r'^BC31_Place_Appellation/list/$', List_view.BC31_Place_AppellationList, name='BC31_Place_AppellationList'),

    url(r'^BC27_Publication/list/$', List_view.BC27_PublicationList, name='BC27_PublicationList'),

    url(r'^BC33_Spatial_Coordinate/list/$', List_view.BC33_Spatial_CoordinateList, name='BC33_Spatial_CoordinateList'),

    url(r'^BC36_Abiotic_Element/list/$', List_view.BC36_Abiotic_ElementList, name='BC36_Abiotic_ElementList'),

    url(r'^BC37_Biological_Object/list/$', List_view.BC37_Biological_ObjectList, name='BC37_Biological_ObjectList'),

    url(r'^BC51_Physical_Object/list/$', List_view.BC51_Physical_ObjectList, name='BC51_Physical_ObjectList'),

    url(r'^BC38_Biotic_Element/list/$', List_view.BC38_Biotic_ElementList, name='BC38_Biotic_ElementList'),

    url(r'^BT22_Class/list/$', List_view.BT22_ClassList, name='BT22_ClassList'),

    url(r'^BT23_SubClass/list/$', List_view.BT23_SubClassList, name='BT23_SubClassList'),

    url(r'^BT24_Family/list/$', List_view.BT24_FamilyList, name='BT24_FamilyList'),

    url(r'^BT25_SubFamily/list/$', List_view.BT25_SubFamilyList, name='BT25_SubFamilyList'),

    url(r'^BT26_Genus/list/$', List_view.BT26_GenusList, name='BT26_GenusList'),

    url(r'^BT27_Species/list/$', List_view.BT27_SpeciesList, name='BT27_SpeciesList'),

    url(r'^BT28_Scientific_Activity_Type/list/$', List_view.BT28_Scientific_Activity_TypeList, name='BT28_Scientific_Activity_TypeList'),

    url(r'^BT29_Industrial_Activity_Type/list/$', List_view.BT29_Industrial_Activity_TypeList, name='BT29_Industrial_Activity_TypeList'),

    url(r'^BT30_Identifier_Assignment_Type/list/$', List_view.BT30_Identifier_Assignment_TypeList, name='BT30_Identifier_Assignment_TypeList'),

    url(r'^BT31_Biological_Part_Time/list/$', List_view.BT31_Biological_Part_TimeList, name='BT31_Biological_Part_TimeList'),

    url(r'^BT33_Marine_Animal_Type/list/$', List_view.BT33_Marine_Animal_TypeList, name='BT33_Marine_Animal_TypeList'),

    url(r'^BT34_Order/list/$', List_view.BT34_OrderList, name='BT34_OrderList'),

    url(r'^BT5_Legislative_Zone_Type/list/$', List_view.BT5_Legislative_Zone_TypeList, name='BT5_Legislative_Zone_TypeList'),

    url(r'^BT8_Abiotic_Element_Type/list/$', List_view.BT8_Abiotic_Element_TypeList, name='BT8_Abiotic_Element_TypeList'),

    url(r'^Person/list/$', List_view.PersonList, name='PersonList'),


    url(r'^BC35_Physical_Thing/(?P<pk>\d+)/update/$', Update_view.BC35_Physical_ThingUpdate, name='BC35_Physical_ThingUpdate'),

    url(r'^BC10_Event/(?P<pk>\d+)/update/$', Update_view.BC10_EventUpdate, name='BC10_EventUpdate'),

    url(r'^BC8_Actor/(?P<pk>\d+)/update/$', Update_view.BC8_ActorUpdate, name='BC8_ActorUpdate'),

    url(r'^BC34_Geometric_Place_Expression/(?P<pk>\d+)/update/$', Update_view.BC34_Geometric_Place_ExpressionUpdate, name='BC34_Geometric_Place_ExpressionUpdate'),

    url(r'^BC29_Spatial_Coordinate_Reference_System/(?P<pk>\d+)/update/$', Update_view.BC29_Spatial_Coordinate_Reference_SystemUpdate, name='BC29_Spatial_Coordinate_Reference_SystemUpdate'),

    url(r'^BC56_Digital_Measurement_Event/(?P<pk>\d+)/update/$', Update_view.BC56_Digital_Measurement_EventUpdate, name='BC56_Digital_Measurement_EventUpdate'),

    url(r'^BC23_Digital_Object/(?P<pk>\d+)/update/$', Update_view.BC23_Digital_ObjectUpdate, name='BC23_Digital_ObjectUpdate'),

    url(r'^BC18_Proposition/(?P<pk>\d+)/update/$', Update_view.BC18_PropositionUpdate, name='BC18_PropositionUpdate'),

    url(r'^BC59_Software/(?P<pk>\d+)/update/$', Update_view.BC59_SoftwareUpdate, name='BC59_SoftwareUpdate'),

    url(r'^BC58_Digital_Device/(?P<pk>\d+)/update/$', Update_view.BC58_Digital_DeviceUpdate, name='BC58_Digital_DeviceUpdate'),

    url(r'^BC61_Capture_Activity/(?P<pk>\d+)/update/$', Update_view.BC61_Capture_ActivityUpdate, name='BC61_Capture_ActivityUpdate'),

    url(r'^BC57_Capture/(?P<pk>\d+)/update/$', Update_view.BC57_CaptureUpdate, name='BC57_CaptureUpdate'),

    url(r'^BC54_Measurement/(?P<pk>\d+)/update/$', Update_view.BC54_MeasurementUpdate, name='BC54_MeasurementUpdate'),

    url(r'^BC44_Attribute_Assignment/(?P<pk>\d+)/update/$', Update_view.BC44_Attribute_AssignmentUpdate, name='BC44_Attribute_AssignmentUpdate'),

    url(r'^BC1_TLO_Entity/(?P<pk>\d+)/update/$', Update_view.BC1_TLO_EntityUpdate, name='BC1_TLO_EntityUpdate'),

    url(r'^BC43_Activity/(?P<pk>\d+)/update/$', Update_view.BC43_ActivityUpdate, name='BC43_ActivityUpdate'),

    url(r'^BC4_Temporal_Phenomenon/(?P<pk>\d+)/update/$', Update_view.BC4_Temporal_PhenomenonUpdate, name='BC4_Temporal_PhenomenonUpdate'),

    url(r'^BC3_Place/(?P<pk>\d+)/update/$', Update_view.BC3_PlaceUpdate, name='BC3_PlaceUpdate'),

    url(r'^BC45_Observation/(?P<pk>\d+)/update/$', Update_view.BC45_ObservationUpdate, name='BC45_ObservationUpdate'),

    url(r'^BC9_Observable_Entity/(?P<pk>\d+)/update/$', Update_view.BC9_Observable_EntityUpdate, name='BC9_Observable_EntityUpdate'),

    url(r'^BC7_Thing/(?P<pk>\d+)/update/$', Update_view.BC7_ThingUpdate, name='BC7_ThingUpdate'),

    url(r'^BC5_Dimension/(?P<pk>\d+)/update/$', Update_view.BC5_DimensionUpdate, name='BC5_DimensionUpdate'),

    url(r'^BC30_Appellation/(?P<pk>\d+)/update/$', Update_view.BC30_AppellationUpdate, name='BC30_AppellationUpdate'),

    url(r'^BC55_Measurement_Unit/(?P<pk>\d+)/update/$', Update_view.BC55_Measurement_UnitUpdate, name='BC55_Measurement_UnitUpdate'),

    url(r'^BC20_Declarative_Place/(?P<pk>\d+)/update/$', Update_view.BC20_Declarative_PlaceUpdate, name='BC20_Declarative_PlaceUpdate'),

    url(r'^BC72_SpaceTime_Volume/(?P<pk>\d+)/update/$', Update_view.BC72_SpaceTime_VolumeUpdate, name='BC72_SpaceTime_VolumeUpdate'),

    url(r'^BC2_Time_Span/(?P<pk>\d+)/update/$', Update_view.BC2_Time_SpanUpdate, name='BC2_Time_SpanUpdate'),

    url(r'^BT14_Appellation_Type/(?P<pk>\d+)/update/$', Update_view.BT14_Appellation_TypeUpdate, name='BT14_Appellation_TypeUpdate'),

    url(r'^BT2_Temporal_Phenomenon_Type/(?P<pk>\d+)/update/$', Update_view.BT2_Temporal_Phenomenon_TypeUpdate, name='BT2_Temporal_Phenomenon_TypeUpdate'),

    url(r'^BT10_Biotic_Element_Type/(?P<pk>\d+)/update/$', Update_view.BT10_Biotic_Element_TypeUpdate, name='BT10_Biotic_Element_TypeUpdate'),

    url(r'^BT7_Ecosystem_Type/(?P<pk>\d+)/update/$', Update_view.BT7_Ecosystem_TypeUpdate, name='BT7_Ecosystem_TypeUpdate'),

    url(r'^BT4_Conceptual_Object_Type/(?P<pk>\d+)/update/$', Update_view.BT4_Conceptual_Object_TypeUpdate, name='BT4_Conceptual_Object_TypeUpdate'),

    url(r'^BT1_TLO_Entity_Type/(?P<pk>\d+)/update/$', Update_view.BT1_TLO_Entity_TypeUpdate, name='BT1_TLO_Entity_TypeUpdate'),

    url(r'^BT17_Human_Activity_Type/(?P<pk>\d+)/update/$', Update_view.BT17_Human_Activity_TypeUpdate, name='BT17_Human_Activity_TypeUpdate'),

    url(r'^BT9_Actor_Type/(?P<pk>\d+)/update/$', Update_view.BT9_Actor_TypeUpdate, name='BT9_Actor_TypeUpdate'),

    url(r'^BT32_Persistent_Type/(?P<pk>\d+)/update/$', Update_view.BT32_Persistent_TypeUpdate, name='BT32_Persistent_TypeUpdate'),

    url(r'^BC32_Identifier/(?P<pk>\d+)/update/$', Update_view.BC32_IdentifierUpdate, name='BC32_IdentifierUpdate'),

    url(r'^BC64_Design_or_Procedure/(?P<pk>\d+)/update/$', Update_view.BC64_Design_or_ProcedureUpdate, name='BC64_Design_or_ProcedureUpdate'),

    url(r'^BC79_Right/(?P<pk>\d+)/update/$', Update_view.BC79_RightUpdate, name='BC79_RightUpdate'),

    url(r'^BC6_Persistent_Item/(?P<pk>\d+)/update/$', Update_view.BC6_Persistent_ItemUpdate, name='BC6_Persistent_ItemUpdate'),

    url(r'^BT3_Physical_Thing_Type/(?P<pk>\d+)/update/$', Update_view.BT3_Physical_Thing_TypeUpdate, name='BT3_Physical_Thing_TypeUpdate'),

    url(r'^BC17_Conceptual_Object/(?P<pk>\d+)/update/$', Update_view.BC17_Conceptual_ObjectUpdate, name='BC17_Conceptual_ObjectUpdate'),

    url(r'^BT32_Persisten_Type/(?P<pk>\d+)/update/$', Update_view.BT32_Persisten_TypeUpdate, name='BT32_Persisten_TypeUpdate'),

    url(r'^BC39_Marine_Animal/(?P<pk>\d+)/update/$', Update_view.BC39_Marine_AnimalUpdate, name='BC39_Marine_AnimalUpdate'),

    url(r'^BC40_Physical_ManMade_Thing/(?P<pk>\d+)/update/$', Update_view.BC40_Physical_ManMade_ThingUpdate, name='BC40_Physical_ManMade_ThingUpdate'),

    url(r'^BC41_ManMade_Object/(?P<pk>\d+)/update/$', Update_view.BC41_ManMade_ObjectUpdate, name='BC41_ManMade_ObjectUpdate'),

    url(r'^BC42_Collection/(?P<pk>\d+)/update/$', Update_view.BC42_CollectionUpdate, name='BC42_CollectionUpdate'),

    url(r'^BC46_Identifier_Assignment/(?P<pk>\d+)/update/$', Update_view.BC46_Identifier_AssignmentUpdate, name='BC46_Identifier_AssignmentUpdate'),

    url(r'^BC47_Image/(?P<pk>\d+)/update/$', Update_view.BC47_ImageUpdate, name='BC47_ImageUpdate'),

    url(r'^BC48_Database/(?P<pk>\d+)/update/$', Update_view.BC48_DatabaseUpdate, name='BC48_DatabaseUpdate'),

    url(r'^BC53_Specimen/(?P<pk>\d+)/update/$', Update_view.BC53_SpecimenUpdate, name='BC53_SpecimenUpdate'),

    url(r'^BC60_Software_Execution/(?P<pk>\d+)/update/$', Update_view.BC60_Software_ExecutionUpdate, name='BC60_Software_ExecutionUpdate'),

    url(r'^BC62_Statistic_Indicator/(?P<pk>\d+)/update/$', Update_view.BC62_Statistic_IndicatorUpdate, name='BC62_Statistic_IndicatorUpdate'),

    url(r'^BC63_Global_Statistic_Landing/(?P<pk>\d+)/update/$', Update_view.BC63_Global_Statistic_LandingUpdate, name='BC63_Global_Statistic_LandingUpdate'),

    url(r'^BC70_Group/(?P<pk>\d+)/update/$', Update_view.BC70_GroupUpdate, name='BC70_GroupUpdate'),

    url(r'^BC71_Name_Use_Activity/(?P<pk>\d+)/update/$', Update_view.BC71_Name_Use_ActivityUpdate, name='BC71_Name_Use_ActivityUpdate'),

    url(r'^BC75_Stock/(?P<pk>\d+)/update/$', Update_view.BC75_StockUpdate, name='BC75_StockUpdate'),

    url(r'^BC76_Stock_Formation/(?P<pk>\d+)/update/$', Update_view.BC76_Stock_FormationUpdate, name='BC76_Stock_FormationUpdate'),

    url(r'^BC77_Matter_Removal/(?P<pk>\d+)/update/$', Update_view.BC77_Matter_RemovalUpdate, name='BC77_Matter_RemovalUpdate'),

    url(r'^BC78_Amount_of_Matter/(?P<pk>\d+)/update/$', Update_view.BC78_Amount_of_MatterUpdate, name='BC78_Amount_of_MatterUpdate'),

    url(r'^BT11_Equipment_Type/(?P<pk>\d+)/update/$', Update_view.BT11_Equipment_TypeUpdate, name='BT11_Equipment_TypeUpdate'),

    url(r'^BT12_Scientific_Data_Type/(?P<pk>\d+)/update/$', Update_view.BT12_Scientific_Data_TypeUpdate, name='BT12_Scientific_Data_TypeUpdate'),

    url(r'^BT13_Digital_Object_Type/(?P<pk>\d+)/update/$', Update_view.BT13_Digital_Object_TypeUpdate, name='BT13_Digital_Object_TypeUpdate'),

    url(r'^BT16_Visual_Type/(?P<pk>\d+)/update/$', Update_view.BT16_Visual_TypeUpdate, name='BT16_Visual_TypeUpdate'),

    url(r'^BT6_Human_Event_Type/(?P<pk>\d+)/update/$', Update_view.BT6_Human_Event_TypeUpdate, name='BT6_Human_Event_TypeUpdate'),

    url(r'^BT18_Kingdom/(?P<pk>\d+)/update/$', Update_view.BT18_KingdomUpdate, name='BT18_KingdomUpdate'),

    url(r'^BT19_Phylum/(?P<pk>\d+)/update/$', Update_view.BT19_PhylumUpdate, name='BT19_PhylumUpdate'),

    url(r'^BT20_SubPhylum/(?P<pk>\d+)/update/$', Update_view.BT20_SubPhylumUpdate, name='BT20_SubPhylumUpdate'),

    url(r'^BT21_SuperClass/(?P<pk>\d+)/update/$', Update_view.BT21_SuperClassUpdate, name='BT21_SuperClassUpdate'),

    url(r'^BT35_Property_Type/(?P<pk>\d+)/update/$', Update_view.BT35_Property_TypeUpdate, name='BT35_Property_TypeUpdate'),

    url(r'^BC25_Linguistic_Object/(?P<pk>\d+)/update/$', Update_view.BC25_Linguistic_ObjectUpdate, name='BC25_Linguistic_ObjectUpdate'),

    url(r'^BT36_Language/(?P<pk>\d+)/update/$', Update_view.BT36_LanguageUpdate, name='BT36_LanguageUpdate'),

    url(r'^BC11_Person/(?P<pk>\d+)/update/$', Update_view.BC11_PersonUpdate, name='BC11_PersonUpdate'),

    url(r'^BC12_Ecosystem/(?P<pk>\d+)/update/$', Update_view.BC12_EcosystemUpdate, name='BC12_EcosystemUpdate'),

    url(r'^BC13_Organization/(?P<pk>\d+)/update/$', Update_view.BC13_OrganizationUpdate, name='BC13_OrganizationUpdate'),

    url(r'^BC14_Ecosystem_Environment/(?P<pk>\d+)/update/$', Update_view.BC14_Ecosystem_EnvironmentUpdate, name='BC14_Ecosystem_EnvironmentUpdate'),

    url(r'^BC15_Water_Area/(?P<pk>\d+)/update/$', Update_view.BC15_Water_AreaUpdate, name='BC15_Water_AreaUpdate'),

    url(r'^BC16_ManMade_thing/(?P<pk>\d+)/update/$', Update_view.BC16_ManMade_thingUpdate, name='BC16_ManMade_thingUpdate'),

    url(r'^BC19_Title/(?P<pk>\d+)/update/$', Update_view.BC19_TitleUpdate, name='BC19_TitleUpdate'),

    url(r'^BC21_DataSet/(?P<pk>\d+)/update/$', Update_view.BC21_DataSetUpdate, name='BC21_DataSetUpdate'),

    url(r'^BC22_Encounter_Event/(?P<pk>\d+)/update/$', Update_view.BC22_Encounter_EventUpdate, name='BC22_Encounter_EventUpdate'),

    url(r'^BC24_Repository_Object/(?P<pk>\d+)/update/$', Update_view.BC24_Repository_ObjectUpdate, name='BC24_Repository_ObjectUpdate'),

    url(r'^BC26_Place_Name/(?P<pk>\d+)/update/$', Update_view.BC26_Place_NameUpdate, name='BC26_Place_NameUpdate'),

    url(r'^BC31_Place_Appellation/(?P<pk>\d+)/update/$', Update_view.BC31_Place_AppellationUpdate, name='BC31_Place_AppellationUpdate'),

    url(r'^BC27_Publication/(?P<pk>\d+)/update/$', Update_view.BC27_PublicationUpdate, name='BC27_PublicationUpdate'),

    url(r'^BC33_Spatial_Coordinate/(?P<pk>\d+)/update/$', Update_view.BC33_Spatial_CoordinateUpdate, name='BC33_Spatial_CoordinateUpdate'),

    url(r'^BC36_Abiotic_Element/(?P<pk>\d+)/update/$', Update_view.BC36_Abiotic_ElementUpdate, name='BC36_Abiotic_ElementUpdate'),

    url(r'^BC37_Biological_Object/(?P<pk>\d+)/update/$', Update_view.BC37_Biological_ObjectUpdate, name='BC37_Biological_ObjectUpdate'),

    url(r'^BC51_Physical_Object/(?P<pk>\d+)/update/$', Update_view.BC51_Physical_ObjectUpdate, name='BC51_Physical_ObjectUpdate'),

    url(r'^BC38_Biotic_Element/(?P<pk>\d+)/update/$', Update_view.BC38_Biotic_ElementUpdate, name='BC38_Biotic_ElementUpdate'),

    url(r'^BT22_Class/(?P<pk>\d+)/update/$', Update_view.BT22_ClassUpdate, name='BT22_ClassUpdate'),

    url(r'^BT23_SubClass/(?P<pk>\d+)/update/$', Update_view.BT23_SubClassUpdate, name='BT23_SubClassUpdate'),

    url(r'^BT24_Family/(?P<pk>\d+)/update/$', Update_view.BT24_FamilyUpdate, name='BT24_FamilyUpdate'),

    url(r'^BT25_SubFamily/(?P<pk>\d+)/update/$', Update_view.BT25_SubFamilyUpdate, name='BT25_SubFamilyUpdate'),

    url(r'^BT26_Genus/(?P<pk>\d+)/update/$', Update_view.BT26_GenusUpdate, name='BT26_GenusUpdate'),

    url(r'^BT27_Species/(?P<pk>\d+)/update/$', Update_view.BT27_SpeciesUpdate, name='BT27_SpeciesUpdate'),

    url(r'^BT28_Scientific_Activity_Type/(?P<pk>\d+)/update/$', Update_view.BT28_Scientific_Activity_TypeUpdate, name='BT28_Scientific_Activity_TypeUpdate'),

    url(r'^BT29_Industrial_Activity_Type/(?P<pk>\d+)/update/$', Update_view.BT29_Industrial_Activity_TypeUpdate, name='BT29_Industrial_Activity_TypeUpdate'),

    url(r'^BT30_Identifier_Assignment_Type/(?P<pk>\d+)/update/$', Update_view.BT30_Identifier_Assignment_TypeUpdate, name='BT30_Identifier_Assignment_TypeUpdate'),

    url(r'^BT31_Biological_Part_Time/(?P<pk>\d+)/update/$', Update_view.BT31_Biological_Part_TimeUpdate, name='BT31_Biological_Part_TimeUpdate'),

    url(r'^BT33_Marine_Animal_Type/(?P<pk>\d+)/update/$', Update_view.BT33_Marine_Animal_TypeUpdate, name='BT33_Marine_Animal_TypeUpdate'),

    url(r'^BT34_Order/(?P<pk>\d+)/update/$', Update_view.BT34_OrderUpdate, name='BT34_OrderUpdate'),

    url(r'^BT5_Legislative_Zone_Type/(?P<pk>\d+)/update/$', Update_view.BT5_Legislative_Zone_TypeUpdate, name='BT5_Legislative_Zone_TypeUpdate'),

    url(r'^BT8_Abiotic_Element_Type/(?P<pk>\d+)/update/$', Update_view.BT8_Abiotic_Element_TypeUpdate, name='BT8_Abiotic_Element_TypeUpdate'),

    url(r'^Person/(?P<pk>\d+)/update/$', Update_view.PersonUpdate, name='PersonUpdate'),


    url(r'^BC35_Physical_Thing/(?P<pk>\d+)/delete/$', Delete_view.BC35_Physical_ThingDelete, name='BC35_Physical_ThingDelete'),

    url(r'^BC10_Event/(?P<pk>\d+)/delete/$', Delete_view.BC10_EventDelete, name='BC10_EventDelete'),

    url(r'^BC8_Actor/(?P<pk>\d+)/delete/$', Delete_view.BC8_ActorDelete, name='BC8_ActorDelete'),

    url(r'^BC34_Geometric_Place_Expression/(?P<pk>\d+)/delete/$', Delete_view.BC34_Geometric_Place_ExpressionDelete, name='BC34_Geometric_Place_ExpressionDelete'),

    url(r'^BC29_Spatial_Coordinate_Reference_System/(?P<pk>\d+)/delete/$', Delete_view.BC29_Spatial_Coordinate_Reference_SystemDelete, name='BC29_Spatial_Coordinate_Reference_SystemDelete'),

    url(r'^BC56_Digital_Measurement_Event/(?P<pk>\d+)/delete/$', Delete_view.BC56_Digital_Measurement_EventDelete, name='BC56_Digital_Measurement_EventDelete'),

    url(r'^BC23_Digital_Object/(?P<pk>\d+)/delete/$', Delete_view.BC23_Digital_ObjectDelete, name='BC23_Digital_ObjectDelete'),

    url(r'^BC18_Proposition/(?P<pk>\d+)/delete/$', Delete_view.BC18_PropositionDelete, name='BC18_PropositionDelete'),

    url(r'^BC59_Software/(?P<pk>\d+)/delete/$', Delete_view.BC59_SoftwareDelete, name='BC59_SoftwareDelete'),

    url(r'^BC58_Digital_Device/(?P<pk>\d+)/delete/$', Delete_view.BC58_Digital_DeviceDelete, name='BC58_Digital_DeviceDelete'),

    url(r'^BC61_Capture_Activity/(?P<pk>\d+)/delete/$', Delete_view.BC61_Capture_ActivityDelete, name='BC61_Capture_ActivityDelete'),

    url(r'^BC57_Capture/(?P<pk>\d+)/delete/$', Delete_view.BC57_CaptureDelete, name='BC57_CaptureDelete'),

    url(r'^BC54_Measurement/(?P<pk>\d+)/delete/$', Delete_view.BC54_MeasurementDelete, name='BC54_MeasurementDelete'),

    url(r'^BC44_Attribute_Assignment/(?P<pk>\d+)/delete/$', Delete_view.BC44_Attribute_AssignmentDelete, name='BC44_Attribute_AssignmentDelete'),

    url(r'^BC1_TLO_Entity/(?P<pk>\d+)/delete/$', Delete_view.BC1_TLO_EntityDelete, name='BC1_TLO_EntityDelete'),

    url(r'^BC43_Activity/(?P<pk>\d+)/delete/$', Delete_view.BC43_ActivityDelete, name='BC43_ActivityDelete'),

    url(r'^BC4_Temporal_Phenomenon/(?P<pk>\d+)/delete/$', Delete_view.BC4_Temporal_PhenomenonDelete, name='BC4_Temporal_PhenomenonDelete'),

    url(r'^BC3_Place/(?P<pk>\d+)/delete/$', Delete_view.BC3_PlaceDelete, name='BC3_PlaceDelete'),

    url(r'^BC45_Observation/(?P<pk>\d+)/delete/$', Delete_view.BC45_ObservationDelete, name='BC45_ObservationDelete'),

    url(r'^BC9_Observable_Entity/(?P<pk>\d+)/delete/$', Delete_view.BC9_Observable_EntityDelete, name='BC9_Observable_EntityDelete'),

    url(r'^BC7_Thing/(?P<pk>\d+)/delete/$', Delete_view.BC7_ThingDelete, name='BC7_ThingDelete'),

    url(r'^BC5_Dimension/(?P<pk>\d+)/delete/$', Delete_view.BC5_DimensionDelete, name='BC5_DimensionDelete'),

    url(r'^BC30_Appellation/(?P<pk>\d+)/delete/$', Delete_view.BC30_AppellationDelete, name='BC30_AppellationDelete'),

    url(r'^BC55_Measurement_Unit/(?P<pk>\d+)/delete/$', Delete_view.BC55_Measurement_UnitDelete, name='BC55_Measurement_UnitDelete'),

    url(r'^BC20_Declarative_Place/(?P<pk>\d+)/delete/$', Delete_view.BC20_Declarative_PlaceDelete, name='BC20_Declarative_PlaceDelete'),

    url(r'^BC72_SpaceTime_Volume/(?P<pk>\d+)/delete/$', Delete_view.BC72_SpaceTime_VolumeDelete, name='BC72_SpaceTime_VolumeDelete'),

    url(r'^BC2_Time_Span/(?P<pk>\d+)/delete/$', Delete_view.BC2_Time_SpanDelete, name='BC2_Time_SpanDelete'),

    url(r'^BT14_Appellation_Type/(?P<pk>\d+)/delete/$', Delete_view.BT14_Appellation_TypeDelete, name='BT14_Appellation_TypeDelete'),

    url(r'^BT2_Temporal_Phenomenon_Type/(?P<pk>\d+)/delete/$', Delete_view.BT2_Temporal_Phenomenon_TypeDelete, name='BT2_Temporal_Phenomenon_TypeDelete'),

    url(r'^BT10_Biotic_Element_Type/(?P<pk>\d+)/delete/$', Delete_view.BT10_Biotic_Element_TypeDelete, name='BT10_Biotic_Element_TypeDelete'),

    url(r'^BT7_Ecosystem_Type/(?P<pk>\d+)/delete/$', Delete_view.BT7_Ecosystem_TypeDelete, name='BT7_Ecosystem_TypeDelete'),

    url(r'^BT4_Conceptual_Object_Type/(?P<pk>\d+)/delete/$', Delete_view.BT4_Conceptual_Object_TypeDelete, name='BT4_Conceptual_Object_TypeDelete'),

    url(r'^BT1_TLO_Entity_Type/(?P<pk>\d+)/delete/$', Delete_view.BT1_TLO_Entity_TypeDelete, name='BT1_TLO_Entity_TypeDelete'),

    url(r'^BT17_Human_Activity_Type/(?P<pk>\d+)/delete/$', Delete_view.BT17_Human_Activity_TypeDelete, name='BT17_Human_Activity_TypeDelete'),

    url(r'^BT9_Actor_Type/(?P<pk>\d+)/delete/$', Delete_view.BT9_Actor_TypeDelete, name='BT9_Actor_TypeDelete'),

    url(r'^BT32_Persistent_Type/(?P<pk>\d+)/delete/$', Delete_view.BT32_Persistent_TypeDelete, name='BT32_Persistent_TypeDelete'),

    url(r'^BC32_Identifier/(?P<pk>\d+)/delete/$', Delete_view.BC32_IdentifierDelete, name='BC32_IdentifierDelete'),

    url(r'^BC64_Design_or_Procedure/(?P<pk>\d+)/delete/$', Delete_view.BC64_Design_or_ProcedureDelete, name='BC64_Design_or_ProcedureDelete'),

    url(r'^BC79_Right/(?P<pk>\d+)/delete/$', Delete_view.BC79_RightDelete, name='BC79_RightDelete'),

    url(r'^BC6_Persistent_Item/(?P<pk>\d+)/delete/$', Delete_view.BC6_Persistent_ItemDelete, name='BC6_Persistent_ItemDelete'),

    url(r'^BT3_Physical_Thing_Type/(?P<pk>\d+)/delete/$', Delete_view.BT3_Physical_Thing_TypeDelete, name='BT3_Physical_Thing_TypeDelete'),

    url(r'^BC17_Conceptual_Object/(?P<pk>\d+)/delete/$', Delete_view.BC17_Conceptual_ObjectDelete, name='BC17_Conceptual_ObjectDelete'),

    url(r'^BT32_Persisten_Type/(?P<pk>\d+)/delete/$', Delete_view.BT32_Persisten_TypeDelete, name='BT32_Persisten_TypeDelete'),

    url(r'^BC39_Marine_Animal/(?P<pk>\d+)/delete/$', Delete_view.BC39_Marine_AnimalDelete, name='BC39_Marine_AnimalDelete'),

    url(r'^BC40_Physical_ManMade_Thing/(?P<pk>\d+)/delete/$', Delete_view.BC40_Physical_ManMade_ThingDelete, name='BC40_Physical_ManMade_ThingDelete'),

    url(r'^BC41_ManMade_Object/(?P<pk>\d+)/delete/$', Delete_view.BC41_ManMade_ObjectDelete, name='BC41_ManMade_ObjectDelete'),

    url(r'^BC42_Collection/(?P<pk>\d+)/delete/$', Delete_view.BC42_CollectionDelete, name='BC42_CollectionDelete'),

    url(r'^BC46_Identifier_Assignment/(?P<pk>\d+)/delete/$', Delete_view.BC46_Identifier_AssignmentDelete, name='BC46_Identifier_AssignmentDelete'),

    url(r'^BC47_Image/(?P<pk>\d+)/delete/$', Delete_view.BC47_ImageDelete, name='BC47_ImageDelete'),

    url(r'^BC48_Database/(?P<pk>\d+)/delete/$', Delete_view.BC48_DatabaseDelete, name='BC48_DatabaseDelete'),

    url(r'^BC53_Specimen/(?P<pk>\d+)/delete/$', Delete_view.BC53_SpecimenDelete, name='BC53_SpecimenDelete'),

    url(r'^BC60_Software_Execution/(?P<pk>\d+)/delete/$', Delete_view.BC60_Software_ExecutionDelete, name='BC60_Software_ExecutionDelete'),

    url(r'^BC62_Statistic_Indicator/(?P<pk>\d+)/delete/$', Delete_view.BC62_Statistic_IndicatorDelete, name='BC62_Statistic_IndicatorDelete'),

    url(r'^BC63_Global_Statistic_Landing/(?P<pk>\d+)/delete/$', Delete_view.BC63_Global_Statistic_LandingDelete, name='BC63_Global_Statistic_LandingDelete'),

    url(r'^BC70_Group/(?P<pk>\d+)/delete/$', Delete_view.BC70_GroupDelete, name='BC70_GroupDelete'),

    url(r'^BC71_Name_Use_Activity/(?P<pk>\d+)/delete/$', Delete_view.BC71_Name_Use_ActivityDelete, name='BC71_Name_Use_ActivityDelete'),

    url(r'^BC75_Stock/(?P<pk>\d+)/delete/$', Delete_view.BC75_StockDelete, name='BC75_StockDelete'),

    url(r'^BC76_Stock_Formation/(?P<pk>\d+)/delete/$', Delete_view.BC76_Stock_FormationDelete, name='BC76_Stock_FormationDelete'),

    url(r'^BC77_Matter_Removal/(?P<pk>\d+)/delete/$', Delete_view.BC77_Matter_RemovalDelete, name='BC77_Matter_RemovalDelete'),

    url(r'^BC78_Amount_of_Matter/(?P<pk>\d+)/delete/$', Delete_view.BC78_Amount_of_MatterDelete, name='BC78_Amount_of_MatterDelete'),

    url(r'^BT11_Equipment_Type/(?P<pk>\d+)/delete/$', Delete_view.BT11_Equipment_TypeDelete, name='BT11_Equipment_TypeDelete'),

    url(r'^BT12_Scientific_Data_Type/(?P<pk>\d+)/delete/$', Delete_view.BT12_Scientific_Data_TypeDelete, name='BT12_Scientific_Data_TypeDelete'),

    url(r'^BT13_Digital_Object_Type/(?P<pk>\d+)/delete/$', Delete_view.BT13_Digital_Object_TypeDelete, name='BT13_Digital_Object_TypeDelete'),

    url(r'^BT16_Visual_Type/(?P<pk>\d+)/delete/$', Delete_view.BT16_Visual_TypeDelete, name='BT16_Visual_TypeDelete'),

    url(r'^BT6_Human_Event_Type/(?P<pk>\d+)/delete/$', Delete_view.BT6_Human_Event_TypeDelete, name='BT6_Human_Event_TypeDelete'),

    url(r'^BT18_Kingdom/(?P<pk>\d+)/delete/$', Delete_view.BT18_KingdomDelete, name='BT18_KingdomDelete'),

    url(r'^BT19_Phylum/(?P<pk>\d+)/delete/$', Delete_view.BT19_PhylumDelete, name='BT19_PhylumDelete'),

    url(r'^BT20_SubPhylum/(?P<pk>\d+)/delete/$', Delete_view.BT20_SubPhylumDelete, name='BT20_SubPhylumDelete'),

    url(r'^BT21_SuperClass/(?P<pk>\d+)/delete/$', Delete_view.BT21_SuperClassDelete, name='BT21_SuperClassDelete'),

    url(r'^BT35_Property_Type/(?P<pk>\d+)/delete/$', Delete_view.BT35_Property_TypeDelete, name='BT35_Property_TypeDelete'),

    url(r'^BC25_Linguistic_Object/(?P<pk>\d+)/delete/$', Delete_view.BC25_Linguistic_ObjectDelete, name='BC25_Linguistic_ObjectDelete'),

    url(r'^BT36_Language/(?P<pk>\d+)/delete/$', Delete_view.BT36_LanguageDelete, name='BT36_LanguageDelete'),

    url(r'^BC11_Person/(?P<pk>\d+)/delete/$', Delete_view.BC11_PersonDelete, name='BC11_PersonDelete'),

    url(r'^BC12_Ecosystem/(?P<pk>\d+)/delete/$', Delete_view.BC12_EcosystemDelete, name='BC12_EcosystemDelete'),

    url(r'^BC13_Organization/(?P<pk>\d+)/delete/$', Delete_view.BC13_OrganizationDelete, name='BC13_OrganizationDelete'),

    url(r'^BC14_Ecosystem_Environment/(?P<pk>\d+)/delete/$', Delete_view.BC14_Ecosystem_EnvironmentDelete, name='BC14_Ecosystem_EnvironmentDelete'),

    url(r'^BC15_Water_Area/(?P<pk>\d+)/delete/$', Delete_view.BC15_Water_AreaDelete, name='BC15_Water_AreaDelete'),

    url(r'^BC16_ManMade_thing/(?P<pk>\d+)/delete/$', Delete_view.BC16_ManMade_thingDelete, name='BC16_ManMade_thingDelete'),

    url(r'^BC19_Title/(?P<pk>\d+)/delete/$', Delete_view.BC19_TitleDelete, name='BC19_TitleDelete'),

    url(r'^BC21_DataSet/(?P<pk>\d+)/delete/$', Delete_view.BC21_DataSetDelete, name='BC21_DataSetDelete'),

    url(r'^BC22_Encounter_Event/(?P<pk>\d+)/delete/$', Delete_view.BC22_Encounter_EventDelete, name='BC22_Encounter_EventDelete'),

    url(r'^BC24_Repository_Object/(?P<pk>\d+)/delete/$', Delete_view.BC24_Repository_ObjectDelete, name='BC24_Repository_ObjectDelete'),

    url(r'^BC26_Place_Name/(?P<pk>\d+)/delete/$', Delete_view.BC26_Place_NameDelete, name='BC26_Place_NameDelete'),

    url(r'^BC31_Place_Appellation/(?P<pk>\d+)/delete/$', Delete_view.BC31_Place_AppellationDelete, name='BC31_Place_AppellationDelete'),

    url(r'^BC27_Publication/(?P<pk>\d+)/delete/$', Delete_view.BC27_PublicationDelete, name='BC27_PublicationDelete'),

    url(r'^BC33_Spatial_Coordinate/(?P<pk>\d+)/delete/$', Delete_view.BC33_Spatial_CoordinateDelete, name='BC33_Spatial_CoordinateDelete'),

    url(r'^BC36_Abiotic_Element/(?P<pk>\d+)/delete/$', Delete_view.BC36_Abiotic_ElementDelete, name='BC36_Abiotic_ElementDelete'),

    url(r'^BC37_Biological_Object/(?P<pk>\d+)/delete/$', Delete_view.BC37_Biological_ObjectDelete, name='BC37_Biological_ObjectDelete'),

    url(r'^BC51_Physical_Object/(?P<pk>\d+)/delete/$', Delete_view.BC51_Physical_ObjectDelete, name='BC51_Physical_ObjectDelete'),

    url(r'^BC38_Biotic_Element/(?P<pk>\d+)/delete/$', Delete_view.BC38_Biotic_ElementDelete, name='BC38_Biotic_ElementDelete'),

    url(r'^BT22_Class/(?P<pk>\d+)/delete/$', Delete_view.BT22_ClassDelete, name='BT22_ClassDelete'),

    url(r'^BT23_SubClass/(?P<pk>\d+)/delete/$', Delete_view.BT23_SubClassDelete, name='BT23_SubClassDelete'),

    url(r'^BT24_Family/(?P<pk>\d+)/delete/$', Delete_view.BT24_FamilyDelete, name='BT24_FamilyDelete'),

    url(r'^BT25_SubFamily/(?P<pk>\d+)/delete/$', Delete_view.BT25_SubFamilyDelete, name='BT25_SubFamilyDelete'),

    url(r'^BT26_Genus/(?P<pk>\d+)/delete/$', Delete_view.BT26_GenusDelete, name='BT26_GenusDelete'),

    url(r'^BT27_Species/(?P<pk>\d+)/delete/$', Delete_view.BT27_SpeciesDelete, name='BT27_SpeciesDelete'),

    url(r'^BT28_Scientific_Activity_Type/(?P<pk>\d+)/delete/$', Delete_view.BT28_Scientific_Activity_TypeDelete, name='BT28_Scientific_Activity_TypeDelete'),

    url(r'^BT29_Industrial_Activity_Type/(?P<pk>\d+)/delete/$', Delete_view.BT29_Industrial_Activity_TypeDelete, name='BT29_Industrial_Activity_TypeDelete'),

    url(r'^BT30_Identifier_Assignment_Type/(?P<pk>\d+)/delete/$', Delete_view.BT30_Identifier_Assignment_TypeDelete, name='BT30_Identifier_Assignment_TypeDelete'),

    url(r'^BT31_Biological_Part_Time/(?P<pk>\d+)/delete/$', Delete_view.BT31_Biological_Part_TimeDelete, name='BT31_Biological_Part_TimeDelete'),

    url(r'^BT33_Marine_Animal_Type/(?P<pk>\d+)/delete/$', Delete_view.BT33_Marine_Animal_TypeDelete, name='BT33_Marine_Animal_TypeDelete'),

    url(r'^BT34_Order/(?P<pk>\d+)/delete/$', Delete_view.BT34_OrderDelete, name='BT34_OrderDelete'),

    url(r'^BT5_Legislative_Zone_Type/(?P<pk>\d+)/delete/$', Delete_view.BT5_Legislative_Zone_TypeDelete, name='BT5_Legislative_Zone_TypeDelete'),

    url(r'^BT8_Abiotic_Element_Type/(?P<pk>\d+)/delete/$', Delete_view.BT8_Abiotic_Element_TypeDelete, name='BT8_Abiotic_Element_TypeDelete'),

    url(r'^Person/(?P<pk>\d+)/delete/$', Delete_view.PersonDelete, name='PersonDelete'),

    ]