from owlready2 import *
from django.db import models
from django.template.loader import render_to_string
from .models import *
from .forms import *
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseNotFound
from django.template import loader
from django.core.urlresolvers import reverse
import datetime
from .forms import *
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.core.files.storage import FileSystemStorage
from django.utils.safestring import mark_safe
from django.conf import settings


def BC35_Physical_ThingCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC35_Physical_ThingForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC35_Physical_Thing(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC35_Physical_Thing/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC35_Physical_ThingForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC35_Physical_ThingCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC10_EventCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC10_EventForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC10_Event(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC10_Event/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC10_EventForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC10_EventCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC8_ActorCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC8_ActorForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC8_Actor(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC8_Actor/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC8_ActorForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC8_ActorCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC34_Geometric_Place_ExpressionCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC34_Geometric_Place_ExpressionForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC34_Geometric_Place_Expression(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC34_Geometric_Place_Expression/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC34_Geometric_Place_ExpressionForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC34_Geometric_Place_ExpressionCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC29_Spatial_Coordinate_Reference_SystemCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC29_Spatial_Coordinate_Reference_SystemForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC29_Spatial_Coordinate_Reference_System(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC29_Spatial_Coordinate_Reference_System/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC29_Spatial_Coordinate_Reference_SystemForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC29_Spatial_Coordinate_Reference_SystemCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC56_Digital_Measurement_EventCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC56_Digital_Measurement_EventForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC56_Digital_Measurement_Event(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC56_Digital_Measurement_Event/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC56_Digital_Measurement_EventForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC56_Digital_Measurement_EventCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC23_Digital_ObjectCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC23_Digital_ObjectForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC23_Digital_Object(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC23_Digital_Object/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC23_Digital_ObjectForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC23_Digital_ObjectCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC18_PropositionCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC18_PropositionForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC18_Proposition(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC18_Proposition/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC18_PropositionForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC18_PropositionCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC59_SoftwareCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC59_SoftwareForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC59_Software(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC59_Software/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC59_SoftwareForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC59_SoftwareCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC58_Digital_DeviceCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC58_Digital_DeviceForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC58_Digital_Device(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC58_Digital_Device/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC58_Digital_DeviceForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC58_Digital_DeviceCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC61_Capture_ActivityCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC61_Capture_ActivityForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC61_Capture_Activity(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC61_Capture_Activity/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC61_Capture_ActivityForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC61_Capture_ActivityCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC57_CaptureCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC57_CaptureForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC57_Capture(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC57_Capture/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC57_CaptureForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC57_CaptureCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC54_MeasurementCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC54_MeasurementForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC54_Measurement(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC54_Measurement/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC54_MeasurementForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC54_MeasurementCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC44_Attribute_AssignmentCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC44_Attribute_AssignmentForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC44_Attribute_Assignment(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC44_Attribute_Assignment/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC44_Attribute_AssignmentForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC44_Attribute_AssignmentCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC1_TLO_EntityCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC1_TLO_EntityForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC1_TLO_Entity(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC1_TLO_Entity/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC1_TLO_EntityForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC1_TLO_EntityCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC43_ActivityCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC43_ActivityForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC43_Activity(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC43_Activity/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC43_ActivityForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC43_ActivityCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC4_Temporal_PhenomenonCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC4_Temporal_PhenomenonForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC4_Temporal_Phenomenon(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC4_Temporal_Phenomenon/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC4_Temporal_PhenomenonForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC4_Temporal_PhenomenonCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC3_PlaceCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC3_PlaceForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC3_Place(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC3_Place/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC3_PlaceForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC3_PlaceCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC45_ObservationCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC45_ObservationForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC45_Observation(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC45_Observation/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC45_ObservationForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC45_ObservationCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC9_Observable_EntityCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC9_Observable_EntityForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC9_Observable_Entity(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC9_Observable_Entity/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC9_Observable_EntityForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC9_Observable_EntityCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC7_ThingCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC7_ThingForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC7_Thing(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC7_Thing/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC7_ThingForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC7_ThingCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC5_DimensionCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC5_DimensionForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC5_Dimension(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC5_Dimension/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC5_DimensionForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC5_DimensionCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC30_AppellationCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC30_AppellationForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC30_Appellation(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC30_Appellation/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC30_AppellationForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC30_AppellationCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC55_Measurement_UnitCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC55_Measurement_UnitForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC55_Measurement_Unit(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC55_Measurement_Unit/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC55_Measurement_UnitForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC55_Measurement_UnitCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC20_Declarative_PlaceCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC20_Declarative_PlaceForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC20_Declarative_Place(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC20_Declarative_Place/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC20_Declarative_PlaceForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC20_Declarative_PlaceCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC72_SpaceTime_VolumeCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC72_SpaceTime_VolumeForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC72_SpaceTime_Volume(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC72_SpaceTime_Volume/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC72_SpaceTime_VolumeForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC72_SpaceTime_VolumeCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC2_Time_SpanCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC2_Time_SpanForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC2_Time_Span(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC2_Time_Span/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC2_Time_SpanForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC2_Time_SpanCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT14_Appellation_TypeCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BT14_Appellation_TypeForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BT14_Appellation_Type(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BT14_Appellation_Type/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT14_Appellation_TypeForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT14_Appellation_TypeCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT2_Temporal_Phenomenon_TypeCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BT2_Temporal_Phenomenon_TypeForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BT2_Temporal_Phenomenon_Type(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BT2_Temporal_Phenomenon_Type/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT2_Temporal_Phenomenon_TypeForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT2_Temporal_Phenomenon_TypeCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT10_Biotic_Element_TypeCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BT10_Biotic_Element_TypeForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BT10_Biotic_Element_Type(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BT10_Biotic_Element_Type/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT10_Biotic_Element_TypeForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT10_Biotic_Element_TypeCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT7_Ecosystem_TypeCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BT7_Ecosystem_TypeForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BT7_Ecosystem_Type(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BT7_Ecosystem_Type/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT7_Ecosystem_TypeForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT7_Ecosystem_TypeCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT4_Conceptual_Object_TypeCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BT4_Conceptual_Object_TypeForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BT4_Conceptual_Object_Type(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BT4_Conceptual_Object_Type/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT4_Conceptual_Object_TypeForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT4_Conceptual_Object_TypeCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT1_TLO_Entity_TypeCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BT1_TLO_Entity_TypeForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BT1_TLO_Entity_Type(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BT1_TLO_Entity_Type/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT1_TLO_Entity_TypeForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT1_TLO_Entity_TypeCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT17_Human_Activity_TypeCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BT17_Human_Activity_TypeForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BT17_Human_Activity_Type(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BT17_Human_Activity_Type/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT17_Human_Activity_TypeForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT17_Human_Activity_TypeCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT9_Actor_TypeCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BT9_Actor_TypeForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BT9_Actor_Type(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BT9_Actor_Type/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT9_Actor_TypeForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT9_Actor_TypeCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT32_Persistent_TypeCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BT32_Persistent_TypeForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BT32_Persistent_Type(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BT32_Persistent_Type/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT32_Persistent_TypeForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT32_Persistent_TypeCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC32_IdentifierCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC32_IdentifierForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC32_Identifier(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC32_Identifier/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC32_IdentifierForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC32_IdentifierCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC64_Design_or_ProcedureCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC64_Design_or_ProcedureForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC64_Design_or_Procedure(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC64_Design_or_Procedure/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC64_Design_or_ProcedureForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC64_Design_or_ProcedureCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC79_RightCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC79_RightForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC79_Right(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC79_Right/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC79_RightForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC79_RightCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC6_Persistent_ItemCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC6_Persistent_ItemForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC6_Persistent_Item(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC6_Persistent_Item/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC6_Persistent_ItemForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC6_Persistent_ItemCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT3_Physical_Thing_TypeCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BT3_Physical_Thing_TypeForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BT3_Physical_Thing_Type(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BT3_Physical_Thing_Type/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT3_Physical_Thing_TypeForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT3_Physical_Thing_TypeCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC17_Conceptual_ObjectCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC17_Conceptual_ObjectForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC17_Conceptual_Object(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC17_Conceptual_Object/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC17_Conceptual_ObjectForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC17_Conceptual_ObjectCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT32_Persisten_TypeCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BT32_Persisten_TypeForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BT32_Persisten_Type(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BT32_Persisten_Type/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT32_Persisten_TypeForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT32_Persisten_TypeCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC39_Marine_AnimalCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC39_Marine_AnimalForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC39_Marine_Animal(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC39_Marine_Animal/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC39_Marine_AnimalForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC39_Marine_AnimalCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC40_Physical_ManMade_ThingCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC40_Physical_ManMade_ThingForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC40_Physical_ManMade_Thing(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC40_Physical_ManMade_Thing/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC40_Physical_ManMade_ThingForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC40_Physical_ManMade_ThingCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC41_ManMade_ObjectCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC41_ManMade_ObjectForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC41_ManMade_Object(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC41_ManMade_Object/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC41_ManMade_ObjectForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC41_ManMade_ObjectCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC42_CollectionCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC42_CollectionForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC42_Collection(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC42_Collection/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC42_CollectionForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC42_CollectionCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC46_Identifier_AssignmentCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC46_Identifier_AssignmentForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC46_Identifier_Assignment(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC46_Identifier_Assignment/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC46_Identifier_AssignmentForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC46_Identifier_AssignmentCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC47_ImageCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC47_ImageForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC47_Image(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC47_Image/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC47_ImageForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC47_ImageCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC48_DatabaseCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC48_DatabaseForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC48_Database(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC48_Database/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC48_DatabaseForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC48_DatabaseCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC53_SpecimenCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC53_SpecimenForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC53_Specimen(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC53_Specimen/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC53_SpecimenForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC53_SpecimenCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC60_Software_ExecutionCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC60_Software_ExecutionForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC60_Software_Execution(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC60_Software_Execution/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC60_Software_ExecutionForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC60_Software_ExecutionCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC62_Statistic_IndicatorCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC62_Statistic_IndicatorForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC62_Statistic_Indicator(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC62_Statistic_Indicator/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC62_Statistic_IndicatorForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC62_Statistic_IndicatorCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC63_Global_Statistic_LandingCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC63_Global_Statistic_LandingForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC63_Global_Statistic_Landing(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC63_Global_Statistic_Landing/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC63_Global_Statistic_LandingForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC63_Global_Statistic_LandingCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC70_GroupCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC70_GroupForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC70_Group(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC70_Group/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC70_GroupForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC70_GroupCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC71_Name_Use_ActivityCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC71_Name_Use_ActivityForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC71_Name_Use_Activity(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC71_Name_Use_Activity/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC71_Name_Use_ActivityForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC71_Name_Use_ActivityCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC75_StockCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC75_StockForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC75_Stock(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC75_Stock/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC75_StockForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC75_StockCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC76_Stock_FormationCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC76_Stock_FormationForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC76_Stock_Formation(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC76_Stock_Formation/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC76_Stock_FormationForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC76_Stock_FormationCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC77_Matter_RemovalCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC77_Matter_RemovalForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC77_Matter_Removal(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC77_Matter_Removal/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC77_Matter_RemovalForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC77_Matter_RemovalCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC78_Amount_of_MatterCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC78_Amount_of_MatterForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC78_Amount_of_Matter(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC78_Amount_of_Matter/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC78_Amount_of_MatterForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC78_Amount_of_MatterCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT11_Equipment_TypeCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BT11_Equipment_TypeForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BT11_Equipment_Type(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BT11_Equipment_Type/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT11_Equipment_TypeForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT11_Equipment_TypeCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT12_Scientific_Data_TypeCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BT12_Scientific_Data_TypeForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BT12_Scientific_Data_Type(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BT12_Scientific_Data_Type/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT12_Scientific_Data_TypeForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT12_Scientific_Data_TypeCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT13_Digital_Object_TypeCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BT13_Digital_Object_TypeForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BT13_Digital_Object_Type(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BT13_Digital_Object_Type/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT13_Digital_Object_TypeForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT13_Digital_Object_TypeCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT16_Visual_TypeCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BT16_Visual_TypeForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BT16_Visual_Type(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BT16_Visual_Type/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT16_Visual_TypeForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT16_Visual_TypeCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT6_Human_Event_TypeCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BT6_Human_Event_TypeForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BT6_Human_Event_Type(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BT6_Human_Event_Type/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT6_Human_Event_TypeForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT6_Human_Event_TypeCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT18_KingdomCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BT18_KingdomForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BT18_Kingdom(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BT18_Kingdom/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT18_KingdomForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT18_KingdomCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT19_PhylumCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BT19_PhylumForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BT19_Phylum(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BT19_Phylum/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT19_PhylumForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT19_PhylumCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT20_SubPhylumCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BT20_SubPhylumForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BT20_SubPhylum(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BT20_SubPhylum/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT20_SubPhylumForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT20_SubPhylumCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT21_SuperClassCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BT21_SuperClassForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BT21_SuperClass(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BT21_SuperClass/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT21_SuperClassForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT21_SuperClassCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT35_Property_TypeCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BT35_Property_TypeForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BT35_Property_Type(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BT35_Property_Type/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT35_Property_TypeForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT35_Property_TypeCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC25_Linguistic_ObjectCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC25_Linguistic_ObjectForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC25_Linguistic_Object(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC25_Linguistic_Object/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC25_Linguistic_ObjectForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC25_Linguistic_ObjectCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT36_LanguageCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BT36_LanguageForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BT36_Language(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BT36_Language/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT36_LanguageForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT36_LanguageCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC11_PersonCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC11_PersonForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC11_Person(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC11_Person/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC11_PersonForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC11_PersonCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC12_EcosystemCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC12_EcosystemForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC12_Ecosystem(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC12_Ecosystem/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC12_EcosystemForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC12_EcosystemCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC13_OrganizationCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC13_OrganizationForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC13_Organization(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC13_Organization/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC13_OrganizationForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC13_OrganizationCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC14_Ecosystem_EnvironmentCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC14_Ecosystem_EnvironmentForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC14_Ecosystem_Environment(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC14_Ecosystem_Environment/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC14_Ecosystem_EnvironmentForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC14_Ecosystem_EnvironmentCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC15_Water_AreaCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC15_Water_AreaForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC15_Water_Area(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC15_Water_Area/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC15_Water_AreaForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC15_Water_AreaCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC16_ManMade_thingCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC16_ManMade_thingForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC16_ManMade_thing(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC16_ManMade_thing/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC16_ManMade_thingForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC16_ManMade_thingCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC19_TitleCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC19_TitleForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC19_Title(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC19_Title/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC19_TitleForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC19_TitleCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC21_DataSetCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC21_DataSetForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC21_DataSet(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC21_DataSet/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC21_DataSetForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC21_DataSetCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC22_Encounter_EventCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC22_Encounter_EventForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC22_Encounter_Event(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC22_Encounter_Event/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC22_Encounter_EventForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC22_Encounter_EventCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC24_Repository_ObjectCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC24_Repository_ObjectForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC24_Repository_Object(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC24_Repository_Object/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC24_Repository_ObjectForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC24_Repository_ObjectCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC26_Place_NameCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC26_Place_NameForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC26_Place_Name(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC26_Place_Name/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC26_Place_NameForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC26_Place_NameCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC31_Place_AppellationCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC31_Place_AppellationForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC31_Place_Appellation(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC31_Place_Appellation/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC31_Place_AppellationForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC31_Place_AppellationCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC27_PublicationCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC27_PublicationForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC27_Publication(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC27_Publication/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC27_PublicationForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC27_PublicationCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC33_Spatial_CoordinateCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC33_Spatial_CoordinateForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC33_Spatial_Coordinate(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC33_Spatial_Coordinate/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC33_Spatial_CoordinateForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC33_Spatial_CoordinateCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC36_Abiotic_ElementCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC36_Abiotic_ElementForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC36_Abiotic_Element(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC36_Abiotic_Element/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC36_Abiotic_ElementForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC36_Abiotic_ElementCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC37_Biological_ObjectCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC37_Biological_ObjectForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC37_Biological_Object(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC37_Biological_Object/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC37_Biological_ObjectForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC37_Biological_ObjectCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC51_Physical_ObjectCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC51_Physical_ObjectForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC51_Physical_Object(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC51_Physical_Object/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC51_Physical_ObjectForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC51_Physical_ObjectCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BC38_Biotic_ElementCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BC38_Biotic_ElementForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BC38_Biotic_Element(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BC38_Biotic_Element/list/"
            return HttpResponseRedirect(link)
    else:
        form = BC38_Biotic_ElementForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BC38_Biotic_ElementCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT22_ClassCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BT22_ClassForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BT22_Class(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BT22_Class/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT22_ClassForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT22_ClassCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT23_SubClassCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BT23_SubClassForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BT23_SubClass(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BT23_SubClass/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT23_SubClassForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT23_SubClassCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT24_FamilyCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BT24_FamilyForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BT24_Family(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BT24_Family/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT24_FamilyForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT24_FamilyCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT25_SubFamilyCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BT25_SubFamilyForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BT25_SubFamily(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BT25_SubFamily/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT25_SubFamilyForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT25_SubFamilyCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT26_GenusCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BT26_GenusForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BT26_Genus(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BT26_Genus/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT26_GenusForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT26_GenusCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT27_SpeciesCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BT27_SpeciesForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BT27_Species(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BT27_Species/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT27_SpeciesForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT27_SpeciesCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT28_Scientific_Activity_TypeCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BT28_Scientific_Activity_TypeForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BT28_Scientific_Activity_Type(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BT28_Scientific_Activity_Type/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT28_Scientific_Activity_TypeForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT28_Scientific_Activity_TypeCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT29_Industrial_Activity_TypeCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BT29_Industrial_Activity_TypeForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BT29_Industrial_Activity_Type(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BT29_Industrial_Activity_Type/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT29_Industrial_Activity_TypeForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT29_Industrial_Activity_TypeCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT30_Identifier_Assignment_TypeCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BT30_Identifier_Assignment_TypeForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BT30_Identifier_Assignment_Type(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BT30_Identifier_Assignment_Type/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT30_Identifier_Assignment_TypeForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT30_Identifier_Assignment_TypeCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT31_Biological_Part_TimeCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BT31_Biological_Part_TimeForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BT31_Biological_Part_Time(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BT31_Biological_Part_Time/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT31_Biological_Part_TimeForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT31_Biological_Part_TimeCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT33_Marine_Animal_TypeCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BT33_Marine_Animal_TypeForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BT33_Marine_Animal_Type(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BT33_Marine_Animal_Type/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT33_Marine_Animal_TypeForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT33_Marine_Animal_TypeCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT34_OrderCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BT34_OrderForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BT34_Order(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BT34_Order/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT34_OrderForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT34_OrderCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT5_Legislative_Zone_TypeCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BT5_Legislative_Zone_TypeForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BT5_Legislative_Zone_Type(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BT5_Legislative_Zone_Type/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT5_Legislative_Zone_TypeForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT5_Legislative_Zone_TypeCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BT8_Abiotic_Element_TypeCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = BT8_Abiotic_Element_TypeForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.BT8_Abiotic_Element_Type(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/BT8_Abiotic_Element_Type/list/"
            return HttpResponseRedirect(link)
    else:
        form = BT8_Abiotic_Element_TypeForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BT8_Abiotic_Element_TypeCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def PersonCreate(request):
    foreign_keys = dict()
    onto_path.append(settings.BASE_DIR)
    onto = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO.owl")
    onto.load()
    onto_namespace = get_ontology("http://www.ics.forth.gr/isl/ontology/MarineTLO/")

    

    if request.method == 'POST':
        form = PersonForm(request.POST)
        if form.is_valid():
            instance = form.save()
            onto_instance = onto_namespace.Person(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/MarineTLO_app/Person/list/"
            return HttpResponseRedirect(link)
    else:
        form = PersonForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "PersonCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})
