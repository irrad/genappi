from django.db import models
from django.template.loader import render_to_string
from .models import *
from .forms import *
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseNotFound
from django.template import loader
from django.core.urlresolvers import reverse
import datetime
from .forms import *
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.core.files.storage import FileSystemStorage
from django.utils.safestring import mark_safe
from django.core import serializers


def BC35_Physical_ThingList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC35_Physical_Thing.objects.all())
    fields = BC35_Physical_Thing._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC35_Physical_ThingList.html",{'data':data, 'fields':new_fields})

def BC10_EventList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC10_Event.objects.all())
    fields = BC10_Event._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC10_EventList.html",{'data':data, 'fields':new_fields})

def BC8_ActorList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC8_Actor.objects.all())
    fields = BC8_Actor._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC8_ActorList.html",{'data':data, 'fields':new_fields})

def BC34_Geometric_Place_ExpressionList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC34_Geometric_Place_Expression.objects.all())
    fields = BC34_Geometric_Place_Expression._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC34_Geometric_Place_ExpressionList.html",{'data':data, 'fields':new_fields})

def BC29_Spatial_Coordinate_Reference_SystemList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC29_Spatial_Coordinate_Reference_System.objects.all())
    fields = BC29_Spatial_Coordinate_Reference_System._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC29_Spatial_Coordinate_Reference_SystemList.html",{'data':data, 'fields':new_fields})

def BC56_Digital_Measurement_EventList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC56_Digital_Measurement_Event.objects.all())
    fields = BC56_Digital_Measurement_Event._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC56_Digital_Measurement_EventList.html",{'data':data, 'fields':new_fields})

def BC23_Digital_ObjectList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC23_Digital_Object.objects.all())
    fields = BC23_Digital_Object._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC23_Digital_ObjectList.html",{'data':data, 'fields':new_fields})

def BC18_PropositionList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC18_Proposition.objects.all())
    fields = BC18_Proposition._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC18_PropositionList.html",{'data':data, 'fields':new_fields})

def BC59_SoftwareList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC59_Software.objects.all())
    fields = BC59_Software._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC59_SoftwareList.html",{'data':data, 'fields':new_fields})

def BC58_Digital_DeviceList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC58_Digital_Device.objects.all())
    fields = BC58_Digital_Device._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC58_Digital_DeviceList.html",{'data':data, 'fields':new_fields})

def BC61_Capture_ActivityList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC61_Capture_Activity.objects.all())
    fields = BC61_Capture_Activity._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC61_Capture_ActivityList.html",{'data':data, 'fields':new_fields})

def BC57_CaptureList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC57_Capture.objects.all())
    fields = BC57_Capture._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC57_CaptureList.html",{'data':data, 'fields':new_fields})

def BC54_MeasurementList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC54_Measurement.objects.all())
    fields = BC54_Measurement._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC54_MeasurementList.html",{'data':data, 'fields':new_fields})

def BC44_Attribute_AssignmentList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC44_Attribute_Assignment.objects.all())
    fields = BC44_Attribute_Assignment._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC44_Attribute_AssignmentList.html",{'data':data, 'fields':new_fields})

def BC1_TLO_EntityList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC1_TLO_Entity.objects.all())
    fields = BC1_TLO_Entity._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC1_TLO_EntityList.html",{'data':data, 'fields':new_fields})

def BC43_ActivityList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC43_Activity.objects.all())
    fields = BC43_Activity._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC43_ActivityList.html",{'data':data, 'fields':new_fields})

def BC4_Temporal_PhenomenonList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC4_Temporal_Phenomenon.objects.all())
    fields = BC4_Temporal_Phenomenon._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC4_Temporal_PhenomenonList.html",{'data':data, 'fields':new_fields})

def BC3_PlaceList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC3_Place.objects.all())
    fields = BC3_Place._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC3_PlaceList.html",{'data':data, 'fields':new_fields})

def BC45_ObservationList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC45_Observation.objects.all())
    fields = BC45_Observation._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC45_ObservationList.html",{'data':data, 'fields':new_fields})

def BC9_Observable_EntityList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC9_Observable_Entity.objects.all())
    fields = BC9_Observable_Entity._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC9_Observable_EntityList.html",{'data':data, 'fields':new_fields})

def BC7_ThingList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC7_Thing.objects.all())
    fields = BC7_Thing._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC7_ThingList.html",{'data':data, 'fields':new_fields})

def BC5_DimensionList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC5_Dimension.objects.all())
    fields = BC5_Dimension._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC5_DimensionList.html",{'data':data, 'fields':new_fields})

def BC30_AppellationList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC30_Appellation.objects.all())
    fields = BC30_Appellation._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC30_AppellationList.html",{'data':data, 'fields':new_fields})

def BC55_Measurement_UnitList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC55_Measurement_Unit.objects.all())
    fields = BC55_Measurement_Unit._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC55_Measurement_UnitList.html",{'data':data, 'fields':new_fields})

def BC20_Declarative_PlaceList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC20_Declarative_Place.objects.all())
    fields = BC20_Declarative_Place._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC20_Declarative_PlaceList.html",{'data':data, 'fields':new_fields})

def BC72_SpaceTime_VolumeList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC72_SpaceTime_Volume.objects.all())
    fields = BC72_SpaceTime_Volume._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC72_SpaceTime_VolumeList.html",{'data':data, 'fields':new_fields})

def BC2_Time_SpanList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC2_Time_Span.objects.all())
    fields = BC2_Time_Span._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC2_Time_SpanList.html",{'data':data, 'fields':new_fields})

def BT14_Appellation_TypeList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT14_Appellation_Type.objects.all())
    fields = BT14_Appellation_Type._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT14_Appellation_TypeList.html",{'data':data, 'fields':new_fields})

def BT2_Temporal_Phenomenon_TypeList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT2_Temporal_Phenomenon_Type.objects.all())
    fields = BT2_Temporal_Phenomenon_Type._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT2_Temporal_Phenomenon_TypeList.html",{'data':data, 'fields':new_fields})

def BT10_Biotic_Element_TypeList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT10_Biotic_Element_Type.objects.all())
    fields = BT10_Biotic_Element_Type._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT10_Biotic_Element_TypeList.html",{'data':data, 'fields':new_fields})

def BT7_Ecosystem_TypeList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT7_Ecosystem_Type.objects.all())
    fields = BT7_Ecosystem_Type._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT7_Ecosystem_TypeList.html",{'data':data, 'fields':new_fields})

def BT4_Conceptual_Object_TypeList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT4_Conceptual_Object_Type.objects.all())
    fields = BT4_Conceptual_Object_Type._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT4_Conceptual_Object_TypeList.html",{'data':data, 'fields':new_fields})

def BT1_TLO_Entity_TypeList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT1_TLO_Entity_Type.objects.all())
    fields = BT1_TLO_Entity_Type._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT1_TLO_Entity_TypeList.html",{'data':data, 'fields':new_fields})

def BT17_Human_Activity_TypeList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT17_Human_Activity_Type.objects.all())
    fields = BT17_Human_Activity_Type._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT17_Human_Activity_TypeList.html",{'data':data, 'fields':new_fields})

def BT9_Actor_TypeList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT9_Actor_Type.objects.all())
    fields = BT9_Actor_Type._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT9_Actor_TypeList.html",{'data':data, 'fields':new_fields})

def BT32_Persistent_TypeList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT32_Persistent_Type.objects.all())
    fields = BT32_Persistent_Type._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT32_Persistent_TypeList.html",{'data':data, 'fields':new_fields})

def BC32_IdentifierList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC32_Identifier.objects.all())
    fields = BC32_Identifier._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC32_IdentifierList.html",{'data':data, 'fields':new_fields})

def BC64_Design_or_ProcedureList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC64_Design_or_Procedure.objects.all())
    fields = BC64_Design_or_Procedure._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC64_Design_or_ProcedureList.html",{'data':data, 'fields':new_fields})

def BC79_RightList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC79_Right.objects.all())
    fields = BC79_Right._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC79_RightList.html",{'data':data, 'fields':new_fields})

def BC6_Persistent_ItemList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC6_Persistent_Item.objects.all())
    fields = BC6_Persistent_Item._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC6_Persistent_ItemList.html",{'data':data, 'fields':new_fields})

def BT3_Physical_Thing_TypeList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT3_Physical_Thing_Type.objects.all())
    fields = BT3_Physical_Thing_Type._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT3_Physical_Thing_TypeList.html",{'data':data, 'fields':new_fields})

def BC17_Conceptual_ObjectList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC17_Conceptual_Object.objects.all())
    fields = BC17_Conceptual_Object._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC17_Conceptual_ObjectList.html",{'data':data, 'fields':new_fields})

def BT32_Persisten_TypeList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT32_Persisten_Type.objects.all())
    fields = BT32_Persisten_Type._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT32_Persisten_TypeList.html",{'data':data, 'fields':new_fields})

def BC39_Marine_AnimalList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC39_Marine_Animal.objects.all())
    fields = BC39_Marine_Animal._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC39_Marine_AnimalList.html",{'data':data, 'fields':new_fields})

def BC40_Physical_ManMade_ThingList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC40_Physical_ManMade_Thing.objects.all())
    fields = BC40_Physical_ManMade_Thing._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC40_Physical_ManMade_ThingList.html",{'data':data, 'fields':new_fields})

def BC41_ManMade_ObjectList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC41_ManMade_Object.objects.all())
    fields = BC41_ManMade_Object._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC41_ManMade_ObjectList.html",{'data':data, 'fields':new_fields})

def BC42_CollectionList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC42_Collection.objects.all())
    fields = BC42_Collection._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC42_CollectionList.html",{'data':data, 'fields':new_fields})

def BC46_Identifier_AssignmentList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC46_Identifier_Assignment.objects.all())
    fields = BC46_Identifier_Assignment._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC46_Identifier_AssignmentList.html",{'data':data, 'fields':new_fields})

def BC47_ImageList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC47_Image.objects.all())
    fields = BC47_Image._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC47_ImageList.html",{'data':data, 'fields':new_fields})

def BC48_DatabaseList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC48_Database.objects.all())
    fields = BC48_Database._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC48_DatabaseList.html",{'data':data, 'fields':new_fields})

def BC53_SpecimenList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC53_Specimen.objects.all())
    fields = BC53_Specimen._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC53_SpecimenList.html",{'data':data, 'fields':new_fields})

def BC60_Software_ExecutionList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC60_Software_Execution.objects.all())
    fields = BC60_Software_Execution._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC60_Software_ExecutionList.html",{'data':data, 'fields':new_fields})

def BC62_Statistic_IndicatorList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC62_Statistic_Indicator.objects.all())
    fields = BC62_Statistic_Indicator._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC62_Statistic_IndicatorList.html",{'data':data, 'fields':new_fields})

def BC63_Global_Statistic_LandingList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC63_Global_Statistic_Landing.objects.all())
    fields = BC63_Global_Statistic_Landing._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC63_Global_Statistic_LandingList.html",{'data':data, 'fields':new_fields})

def BC70_GroupList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC70_Group.objects.all())
    fields = BC70_Group._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC70_GroupList.html",{'data':data, 'fields':new_fields})

def BC71_Name_Use_ActivityList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC71_Name_Use_Activity.objects.all())
    fields = BC71_Name_Use_Activity._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC71_Name_Use_ActivityList.html",{'data':data, 'fields':new_fields})

def BC75_StockList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC75_Stock.objects.all())
    fields = BC75_Stock._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC75_StockList.html",{'data':data, 'fields':new_fields})

def BC76_Stock_FormationList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC76_Stock_Formation.objects.all())
    fields = BC76_Stock_Formation._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC76_Stock_FormationList.html",{'data':data, 'fields':new_fields})

def BC77_Matter_RemovalList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC77_Matter_Removal.objects.all())
    fields = BC77_Matter_Removal._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC77_Matter_RemovalList.html",{'data':data, 'fields':new_fields})

def BC78_Amount_of_MatterList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC78_Amount_of_Matter.objects.all())
    fields = BC78_Amount_of_Matter._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC78_Amount_of_MatterList.html",{'data':data, 'fields':new_fields})

def BT11_Equipment_TypeList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT11_Equipment_Type.objects.all())
    fields = BT11_Equipment_Type._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT11_Equipment_TypeList.html",{'data':data, 'fields':new_fields})

def BT12_Scientific_Data_TypeList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT12_Scientific_Data_Type.objects.all())
    fields = BT12_Scientific_Data_Type._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT12_Scientific_Data_TypeList.html",{'data':data, 'fields':new_fields})

def BT13_Digital_Object_TypeList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT13_Digital_Object_Type.objects.all())
    fields = BT13_Digital_Object_Type._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT13_Digital_Object_TypeList.html",{'data':data, 'fields':new_fields})

def BT16_Visual_TypeList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT16_Visual_Type.objects.all())
    fields = BT16_Visual_Type._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT16_Visual_TypeList.html",{'data':data, 'fields':new_fields})

def BT6_Human_Event_TypeList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT6_Human_Event_Type.objects.all())
    fields = BT6_Human_Event_Type._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT6_Human_Event_TypeList.html",{'data':data, 'fields':new_fields})

def BT18_KingdomList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT18_Kingdom.objects.all())
    fields = BT18_Kingdom._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT18_KingdomList.html",{'data':data, 'fields':new_fields})

def BT19_PhylumList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT19_Phylum.objects.all())
    fields = BT19_Phylum._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT19_PhylumList.html",{'data':data, 'fields':new_fields})

def BT20_SubPhylumList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT20_SubPhylum.objects.all())
    fields = BT20_SubPhylum._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT20_SubPhylumList.html",{'data':data, 'fields':new_fields})

def BT21_SuperClassList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT21_SuperClass.objects.all())
    fields = BT21_SuperClass._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT21_SuperClassList.html",{'data':data, 'fields':new_fields})

def BT35_Property_TypeList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT35_Property_Type.objects.all())
    fields = BT35_Property_Type._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT35_Property_TypeList.html",{'data':data, 'fields':new_fields})

def BC25_Linguistic_ObjectList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC25_Linguistic_Object.objects.all())
    fields = BC25_Linguistic_Object._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC25_Linguistic_ObjectList.html",{'data':data, 'fields':new_fields})

def BT36_LanguageList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT36_Language.objects.all())
    fields = BT36_Language._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT36_LanguageList.html",{'data':data, 'fields':new_fields})

def BC11_PersonList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC11_Person.objects.all())
    fields = BC11_Person._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC11_PersonList.html",{'data':data, 'fields':new_fields})

def BC12_EcosystemList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC12_Ecosystem.objects.all())
    fields = BC12_Ecosystem._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC12_EcosystemList.html",{'data':data, 'fields':new_fields})

def BC13_OrganizationList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC13_Organization.objects.all())
    fields = BC13_Organization._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC13_OrganizationList.html",{'data':data, 'fields':new_fields})

def BC14_Ecosystem_EnvironmentList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC14_Ecosystem_Environment.objects.all())
    fields = BC14_Ecosystem_Environment._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC14_Ecosystem_EnvironmentList.html",{'data':data, 'fields':new_fields})

def BC15_Water_AreaList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC15_Water_Area.objects.all())
    fields = BC15_Water_Area._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC15_Water_AreaList.html",{'data':data, 'fields':new_fields})

def BC16_ManMade_thingList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC16_ManMade_thing.objects.all())
    fields = BC16_ManMade_thing._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC16_ManMade_thingList.html",{'data':data, 'fields':new_fields})

def BC19_TitleList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC19_Title.objects.all())
    fields = BC19_Title._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC19_TitleList.html",{'data':data, 'fields':new_fields})

def BC21_DataSetList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC21_DataSet.objects.all())
    fields = BC21_DataSet._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC21_DataSetList.html",{'data':data, 'fields':new_fields})

def BC22_Encounter_EventList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC22_Encounter_Event.objects.all())
    fields = BC22_Encounter_Event._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC22_Encounter_EventList.html",{'data':data, 'fields':new_fields})

def BC24_Repository_ObjectList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC24_Repository_Object.objects.all())
    fields = BC24_Repository_Object._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC24_Repository_ObjectList.html",{'data':data, 'fields':new_fields})

def BC26_Place_NameList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC26_Place_Name.objects.all())
    fields = BC26_Place_Name._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC26_Place_NameList.html",{'data':data, 'fields':new_fields})

def BC31_Place_AppellationList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC31_Place_Appellation.objects.all())
    fields = BC31_Place_Appellation._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC31_Place_AppellationList.html",{'data':data, 'fields':new_fields})

def BC27_PublicationList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC27_Publication.objects.all())
    fields = BC27_Publication._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC27_PublicationList.html",{'data':data, 'fields':new_fields})

def BC33_Spatial_CoordinateList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC33_Spatial_Coordinate.objects.all())
    fields = BC33_Spatial_Coordinate._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC33_Spatial_CoordinateList.html",{'data':data, 'fields':new_fields})

def BC36_Abiotic_ElementList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC36_Abiotic_Element.objects.all())
    fields = BC36_Abiotic_Element._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC36_Abiotic_ElementList.html",{'data':data, 'fields':new_fields})

def BC37_Biological_ObjectList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC37_Biological_Object.objects.all())
    fields = BC37_Biological_Object._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC37_Biological_ObjectList.html",{'data':data, 'fields':new_fields})

def BC51_Physical_ObjectList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC51_Physical_Object.objects.all())
    fields = BC51_Physical_Object._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC51_Physical_ObjectList.html",{'data':data, 'fields':new_fields})

def BC38_Biotic_ElementList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BC38_Biotic_Element.objects.all())
    fields = BC38_Biotic_Element._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BC38_Biotic_ElementList.html",{'data':data, 'fields':new_fields})

def BT22_ClassList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT22_Class.objects.all())
    fields = BT22_Class._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT22_ClassList.html",{'data':data, 'fields':new_fields})

def BT23_SubClassList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT23_SubClass.objects.all())
    fields = BT23_SubClass._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT23_SubClassList.html",{'data':data, 'fields':new_fields})

def BT24_FamilyList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT24_Family.objects.all())
    fields = BT24_Family._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT24_FamilyList.html",{'data':data, 'fields':new_fields})

def BT25_SubFamilyList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT25_SubFamily.objects.all())
    fields = BT25_SubFamily._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT25_SubFamilyList.html",{'data':data, 'fields':new_fields})

def BT26_GenusList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT26_Genus.objects.all())
    fields = BT26_Genus._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT26_GenusList.html",{'data':data, 'fields':new_fields})

def BT27_SpeciesList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT27_Species.objects.all())
    fields = BT27_Species._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT27_SpeciesList.html",{'data':data, 'fields':new_fields})

def BT28_Scientific_Activity_TypeList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT28_Scientific_Activity_Type.objects.all())
    fields = BT28_Scientific_Activity_Type._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT28_Scientific_Activity_TypeList.html",{'data':data, 'fields':new_fields})

def BT29_Industrial_Activity_TypeList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT29_Industrial_Activity_Type.objects.all())
    fields = BT29_Industrial_Activity_Type._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT29_Industrial_Activity_TypeList.html",{'data':data, 'fields':new_fields})

def BT30_Identifier_Assignment_TypeList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT30_Identifier_Assignment_Type.objects.all())
    fields = BT30_Identifier_Assignment_Type._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT30_Identifier_Assignment_TypeList.html",{'data':data, 'fields':new_fields})

def BT31_Biological_Part_TimeList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT31_Biological_Part_Time.objects.all())
    fields = BT31_Biological_Part_Time._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT31_Biological_Part_TimeList.html",{'data':data, 'fields':new_fields})

def BT33_Marine_Animal_TypeList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT33_Marine_Animal_Type.objects.all())
    fields = BT33_Marine_Animal_Type._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT33_Marine_Animal_TypeList.html",{'data':data, 'fields':new_fields})

def BT34_OrderList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT34_Order.objects.all())
    fields = BT34_Order._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT34_OrderList.html",{'data':data, 'fields':new_fields})

def BT5_Legislative_Zone_TypeList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT5_Legislative_Zone_Type.objects.all())
    fields = BT5_Legislative_Zone_Type._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT5_Legislative_Zone_TypeList.html",{'data':data, 'fields':new_fields})

def BT8_Abiotic_Element_TypeList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BT8_Abiotic_Element_Type.objects.all())
    fields = BT8_Abiotic_Element_Type._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BT8_Abiotic_Element_TypeList.html",{'data':data, 'fields':new_fields})

def PersonList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Person.objects.all())
    fields = Person._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"PersonList.html",{'data':data, 'fields':new_fields})
