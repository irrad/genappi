from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    # Examples:
    # url(r'^$', 'iedm_project.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^vowl', 'iedm_app.Home_view.vowl', name='vowl'),
    url(r'^iedm_app/', include('iedm_app.urls', namespace='iedm_app')),
    url(r'^$', 'iedm_app.Home_view.HomeView', name='HomeView'),    url(r'^accounts/',include('django.contrib.auth.urls')),]
