from django.db import models
from django.template.loader import render_to_string
from .models import *
from .forms import *
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseNotFound
from django.template import loader
from django.core.urlresolvers import reverse
import datetime
from .forms import *
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.core.files.storage import FileSystemStorage
from django.utils.safestring import mark_safe
from django.core import serializers


def ThingRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Thing, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ThingRead.html",{'instance_items': instance_items})

def ObjectRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Object, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ObjectRead.html",{'instance_items': instance_items})

def UnaryFunctionRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(UnaryFunction, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"UnaryFunctionRead.html",{'instance_items': instance_items})

def UserRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(User, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"UserRead.html",{'instance_items': instance_items})

def IrradiationFacilityRoleRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(IrradiationFacilityRole, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"IrradiationFacilityRoleRead.html",{'instance_items': instance_items})

def IrradiationFacilityCoordinatorRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(IrradiationFacilityCoordinator, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"IrradiationFacilityCoordinatorRead.html",{'instance_items': instance_items})

def IrradiationFacilityManagerRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(IrradiationFacilityManager, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"IrradiationFacilityManagerRead.html",{'instance_items': instance_items})

def OperatorRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Operator, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"OperatorRead.html",{'instance_items': instance_items})

def ResponsiblePersonRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ResponsiblePerson, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ResponsiblePersonRead.html",{'instance_items': instance_items})

def DUTIrradiationRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(DUTIrradiation, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"DUTIrradiationRead.html",{'instance_items': instance_items})

def SampleRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Sample, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"SampleRead.html",{'instance_items': instance_items})

def DUTRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(DUT, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"DUTRead.html",{'instance_items': instance_items})

def LayerTableRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(LayerTable, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"LayerTableRead.html",{'instance_items': instance_items})

def IrradiationExperimentRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(IrradiationExperiment, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"IrradiationExperimentRead.html",{'instance_items': instance_items})

def LayerRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Layer, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"LayerRead.html",{'instance_items': instance_items})

def CompoundRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Compound, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"CompoundRead.html",{'instance_items': instance_items})

def CompoundWeightFractionTableRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(CompoundWeightFractionTable, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"CompoundWeightFractionTableRead.html",{'instance_items': instance_items})

def IrradiationFacilityRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(IrradiationFacility, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"IrradiationFacilityRead.html",{'instance_items': instance_items})

def PhysicalRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Physical, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"PhysicalRead.html",{'instance_items': instance_items})

def IrradiationFacilityUserRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(IrradiationFacilityUser, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"IrradiationFacilityUserRead.html",{'instance_items': instance_items})

def PredicateRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Predicate, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"PredicateRead.html",{'instance_items': instance_items})

def SentientAgentRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(SentientAgent, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"SentientAgentRead.html",{'instance_items': instance_items})

def AgentRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Agent, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"AgentRead.html",{'instance_items': instance_items})

def CompoundWeightFractionEntryRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(CompoundWeightFractionEntry, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"CompoundWeightFractionEntryRead.html",{'instance_items': instance_items})

def ElementRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Element, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ElementRead.html",{'instance_items': instance_items})

def UnaryFunctionEntryRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(UnaryFunctionEntry, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"UnaryFunctionEntryRead.html",{'instance_items': instance_items})

def CumulatedQuantityRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(CumulatedQuantity, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"CumulatedQuantityRead.html",{'instance_items': instance_items})

def IrradiationExperimentObjectRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(IrradiationExperimentObject, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"IrradiationExperimentObjectRead.html",{'instance_items': instance_items})

def InteractionLengthRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(InteractionLength, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"InteractionLengthRead.html",{'instance_items': instance_items})

def InteractionLengthOccupancyRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(InteractionLengthOccupancy, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"InteractionLengthOccupancyRead.html",{'instance_items': instance_items})

def DosimetricQuantityRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(DosimetricQuantity, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"DosimetricQuantityRead.html",{'instance_items': instance_items})

def DosimeterRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Dosimeter, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"DosimeterRead.html",{'instance_items': instance_items})

def DomainOfExperimentRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(DomainOfExperiment, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"DomainOfExperimentRead.html",{'instance_items': instance_items})

def ParticlePhysicsExperimentRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ParticlePhysicsExperiment, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ParticlePhysicsExperimentRead.html",{'instance_items': instance_items})

def RequirementsRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Requirements, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"RequirementsRead.html",{'instance_items': instance_items})

def MonitoringSystemRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(MonitoringSystem, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"MonitoringSystemRead.html",{'instance_items': instance_items})

def StorageAreaRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(StorageArea, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"StorageAreaRead.html",{'instance_items': instance_items})

def FieldOfStudyRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(FieldOfStudy, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"FieldOfStudyRead.html",{'instance_items': instance_items})

def IrradiationFacilityPostionRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(IrradiationFacilityPostion, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"IrradiationFacilityPostionRead.html",{'instance_items': instance_items})

def LengthRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Length, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"LengthRead.html",{'instance_items': instance_items})

def PercentageRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Percentage, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"PercentageRead.html",{'instance_items': instance_items})

def LayerEntryRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(LayerEntry, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"LayerEntryRead.html",{'instance_items': instance_items})

def RelationRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Relation, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"RelationRead.html",{'instance_items': instance_items})

def AbstractRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Abstract, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"AbstractRead.html",{'instance_items': instance_items})

def RegionRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Region, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"RegionRead.html",{'instance_items': instance_items})

def SubjectRoleRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(SubjectRole, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"SubjectRoleRead.html",{'instance_items': instance_items})

def ActorRoleRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ActorRole, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ActorRoleRead.html",{'instance_items': instance_items})

def AbsorbedDoseRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(AbsorbedDose, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"AbsorbedDoseRead.html",{'instance_items': instance_items})

def AbsorbedDoseInMediumRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(AbsorbedDoseInMedium, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"AbsorbedDoseInMediumRead.html",{'instance_items': instance_items})

def QuantityRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Quantity, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"QuantityRead.html",{'instance_items': instance_items})

def ActivityRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Activity, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ActivityRead.html",{'instance_items': instance_items})

def DosimetricRateRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(DosimetricRate, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"DosimetricRateRead.html",{'instance_items': instance_items})

def AtomicMassRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(AtomicMass, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"AtomicMassRead.html",{'instance_items': instance_items})

def MassRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Mass, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"MassRead.html",{'instance_items': instance_items})

def DensityRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Density, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"DensityRead.html",{'instance_items': instance_items})

def EnergyRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Energy, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"EnergyRead.html",{'instance_items': instance_items})

def HeightRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Height, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"HeightRead.html",{'instance_items': instance_items})

def NumberRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Number, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"NumberRead.html",{'instance_items': instance_items})

def RatioRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Ratio, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"RatioRead.html",{'instance_items': instance_items})

def TemperatureRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Temperature, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"TemperatureRead.html",{'instance_items': instance_items})

def ThicknessRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Thickness, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ThicknessRead.html",{'instance_items': instance_items})

def AbsorbedDoseInAirRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(AbsorbedDoseInAir, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"AbsorbedDoseInAirRead.html",{'instance_items': instance_items})

def AdminInfoIrradiationExperimentRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(AdminInfoIrradiationExperiment, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"AdminInfoIrradiationExperimentRead.html",{'instance_items': instance_items})

def AdminInfoExperimentRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(AdminInfoExperiment, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"AdminInfoExperimentRead.html",{'instance_items': instance_items})

def ActionrelatedRoleRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ActionrelatedRole, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ActionrelatedRoleRead.html",{'instance_items': instance_items})

def RoleRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Role, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"RoleRead.html",{'instance_items': instance_items})

def AdministrativeInformationRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(AdministrativeInformation, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"AdministrativeInformationRead.html",{'instance_items': instance_items})

def PropositionRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Proposition, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"PropositionRead.html",{'instance_items': instance_items})

def AdminInfoObjectOfExperimentRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(AdminInfoObjectOfExperiment, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"AdminInfoObjectOfExperimentRead.html",{'instance_items': instance_items})

def AdminInfoUserRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(AdminInfoUser, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"AdminInfoUserRead.html",{'instance_items': instance_items})

def ClassificationRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Classification, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ClassificationRead.html",{'instance_items': instance_items})

def ScientificTaskRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ScientificTask, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ScientificTaskRead.html",{'instance_items': instance_items})

def ClassificationByDomainRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ClassificationByDomain, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ClassificationByDomainRead.html",{'instance_items': instance_items})

def ClassificationOfExperimentsRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ClassificationOfExperiments, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ClassificationOfExperimentsRead.html",{'instance_items': instance_items})

def CorpuscularObjectRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(CorpuscularObject, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"CorpuscularObjectRead.html",{'instance_items': instance_items})

def SelfConnectedObjectRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(SelfConnectedObject, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"SelfConnectedObjectRead.html",{'instance_items': instance_items})

def ExperimentalRequirementsRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ExperimentalRequirements, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ExperimentalRequirementsRead.html",{'instance_items': instance_items})

def AdminInfoIrradiationExperimentUserRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(AdminInfoIrradiationExperimentUser, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"AdminInfoIrradiationExperimentUserRead.html",{'instance_items': instance_items})

def AdminInfoObjectOfIrradiationRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(AdminInfoObjectOfIrradiation, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"AdminInfoObjectOfIrradiationRead.html",{'instance_items': instance_items})

def AdminInfoObjectOfIrradiationExperimentRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(AdminInfoObjectOfIrradiationExperiment, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"AdminInfoObjectOfIrradiationExperimentRead.html",{'instance_items': instance_items})

def AtomicNumberRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(AtomicNumber, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"AtomicNumberRead.html",{'instance_items': instance_items})

def CollaborationRequirementsRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(CollaborationRequirements, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"CollaborationRequirementsRead.html",{'instance_items': instance_items})

def ControlRoomRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ControlRoom, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ControlRoomRead.html",{'instance_items': instance_items})

def ControlSystemRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ControlSystem, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ControlSystemRead.html",{'instance_items': instance_items})

def SystemRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(System, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"SystemRead.html",{'instance_items': instance_items})

def RadiationLengthRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(RadiationLength, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"RadiationLengthRead.html",{'instance_items': instance_items})

def RadiationLengthOccupancyRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(RadiationLengthOccupancy, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"RadiationLengthOccupancyRead.html",{'instance_items': instance_items})

def RadioprotectionRequirementsRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(RadioprotectionRequirements, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"RadioprotectionRequirementsRead.html",{'instance_items': instance_items})

def SingularFieldRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(SingularField, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"SingularFieldRead.html",{'instance_items': instance_items})

def RadiationFieldRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(RadiationField, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"RadiationFieldRead.html",{'instance_items': instance_items})

def TechnicalRequirementsRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(TechnicalRequirements, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"TechnicalRequirementsRead.html",{'instance_items': instance_items})

def IrradiationFacilityRequirementsRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(IrradiationFacilityRequirements, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"IrradiationFacilityRequirementsRead.html",{'instance_items': instance_items})

def FluenceRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Fluence, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"FluenceRead.html",{'instance_items': instance_items})

def NuclearCollisionLengthRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(NuclearCollisionLength, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"NuclearCollisionLengthRead.html",{'instance_items': instance_items})

def NuclearInteractionLengthRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(NuclearInteractionLength, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"NuclearInteractionLengthRead.html",{'instance_items': instance_items})

def DataManagementSystemRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(DataManagementSystem, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"DataManagementSystemRead.html",{'instance_items': instance_items})

def DoseRateRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(DoseRate, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"DoseRateRead.html",{'instance_items': instance_items})

def ExternalPositionRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ExternalPosition, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ExternalPositionRead.html",{'instance_items': instance_items})

def FluenceRateRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(FluenceRate, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"FluenceRateRead.html",{'instance_items': instance_items})

def InteractionLengthEntryRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(InteractionLengthEntry, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"InteractionLengthEntryRead.html",{'instance_items': instance_items})

def InteractionLengthTableRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(InteractionLengthTable, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"InteractionLengthTableRead.html",{'instance_items': instance_items})

def IrradiationPositionRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(IrradiationPosition, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"IrradiationPositionRead.html",{'instance_items': instance_items})

def IrradiationZoneRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(IrradiationZone, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"IrradiationZoneRead.html",{'instance_items': instance_items})

def LaboratoryRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Laboratory, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"LaboratoryRead.html",{'instance_items': instance_items})

def LegalRequirementsRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(LegalRequirements, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"LegalRequirementsRead.html",{'instance_items': instance_items})

def MinimumIonisationEntryRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(MinimumIonisationEntry, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"MinimumIonisationEntryRead.html",{'instance_items': instance_items})

def MinimumIonisationTableRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(MinimumIonisationTable, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"MinimumIonisationTableRead.html",{'instance_items': instance_items})

def MixedFieldRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(MixedField, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"MixedFieldRead.html",{'instance_items': instance_items})

def NuclearCollisionLengthOccupancyRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(NuclearCollisionLengthOccupancy, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"NuclearCollisionLengthOccupancyRead.html",{'instance_items': instance_items})

def NuclearInteractionLengthOccupancyRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(NuclearInteractionLengthOccupancy, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"NuclearInteractionLengthOccupancyRead.html",{'instance_items': instance_items})

def ParticleRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Particle, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ParticleRead.html",{'instance_items': instance_items})
