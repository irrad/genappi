from owlready2 import *
from django.db import models
from django.template.loader import render_to_string
from .models import *
from .forms import *
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseNotFound
from django.template import loader
from django.core.urlresolvers import reverse
import datetime
from .forms import *
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.core.files.storage import FileSystemStorage
from django.utils.safestring import mark_safe
from django.conf import settings

onto_path.append(settings.BASE_DIR)
onto = get_ontology("iedm.owl").load()
ontoclasses = onto.classes()
namespaces = dict() 
for clas in ontoclasses:
    namespaces[clas.name] = clas.namespace


def ThingUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Thing"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Thing, pk=pk)
    if request.method == 'POST':
        form = ThingForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Thing(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/Thing/list/"
            return HttpResponseRedirect(link)
    else:
        form = ThingForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ThingUpdate.html",context)

def ObjectUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Object"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Object, pk=pk)
    if request.method == 'POST':
        form = ObjectForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Object(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/Object/list/"
            return HttpResponseRedirect(link)
    else:
        form = ObjectForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ObjectUpdate.html",context)

def UnaryFunctionUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["UnaryFunction"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(UnaryFunction, pk=pk)
    if request.method == 'POST':
        form = UnaryFunctionForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.UnaryFunction(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/UnaryFunction/list/"
            return HttpResponseRedirect(link)
    else:
        form = UnaryFunctionForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "UnaryFunctionUpdate.html",context)

def UserUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["User"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(User, pk=pk)
    if request.method == 'POST':
        form = UserForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.User(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/User/list/"
            return HttpResponseRedirect(link)
    else:
        form = UserForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "UserUpdate.html",context)

def IrradiationFacilityRoleUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["IrradiationFacilityRole"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(IrradiationFacilityRole, pk=pk)
    if request.method == 'POST':
        form = IrradiationFacilityRoleForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.IrradiationFacilityRole(form.cleaned_data['name'])
            
            if IrradiationFacilityRole._meta.get_field('name').get_internal_type()  == 'IntegerField':
                onto_instance.name=int(form.cleaned_data['name'])
            elif IrradiationFacilityRole._meta.get_field('name').get_internal_type()  == 'DecimalField':
                onto_instance.name=float(form.cleaned_data['name'])
            else:
                onto_instance.name=str(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/IrradiationFacilityRole/list/"
            return HttpResponseRedirect(link)
    else:
        form = IrradiationFacilityRoleForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "IrradiationFacilityRoleUpdate.html",context)

def IrradiationFacilityCoordinatorUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["IrradiationFacilityCoordinator"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(IrradiationFacilityCoordinator, pk=pk)
    if request.method == 'POST':
        form = IrradiationFacilityCoordinatorForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.IrradiationFacilityCoordinator(form.cleaned_data['name'])
            
            if IrradiationFacilityCoordinator._meta.get_field('name').get_internal_type()  == 'IntegerField':
                onto_instance.name=int(form.cleaned_data['name'])
            elif IrradiationFacilityCoordinator._meta.get_field('name').get_internal_type()  == 'DecimalField':
                onto_instance.name=float(form.cleaned_data['name'])
            else:
                onto_instance.name=str(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/IrradiationFacilityCoordinator/list/"
            return HttpResponseRedirect(link)
    else:
        form = IrradiationFacilityCoordinatorForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "IrradiationFacilityCoordinatorUpdate.html",context)

def IrradiationFacilityManagerUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["IrradiationFacilityManager"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(IrradiationFacilityManager, pk=pk)
    if request.method == 'POST':
        form = IrradiationFacilityManagerForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.IrradiationFacilityManager(form.cleaned_data['name'])
            
            if IrradiationFacilityManager._meta.get_field('name').get_internal_type()  == 'IntegerField':
                onto_instance.name=int(form.cleaned_data['name'])
            elif IrradiationFacilityManager._meta.get_field('name').get_internal_type()  == 'DecimalField':
                onto_instance.name=float(form.cleaned_data['name'])
            else:
                onto_instance.name=str(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/IrradiationFacilityManager/list/"
            return HttpResponseRedirect(link)
    else:
        form = IrradiationFacilityManagerForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "IrradiationFacilityManagerUpdate.html",context)

def OperatorUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Operator"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Operator, pk=pk)
    if request.method == 'POST':
        form = OperatorForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Operator(form.cleaned_data['name'])
            
            if Operator._meta.get_field('name').get_internal_type()  == 'IntegerField':
                onto_instance.name=int(form.cleaned_data['name'])
            elif Operator._meta.get_field('name').get_internal_type()  == 'DecimalField':
                onto_instance.name=float(form.cleaned_data['name'])
            else:
                onto_instance.name=str(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/Operator/list/"
            return HttpResponseRedirect(link)
    else:
        form = OperatorForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "OperatorUpdate.html",context)

def ResponsiblePersonUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ResponsiblePerson"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ResponsiblePerson, pk=pk)
    if request.method == 'POST':
        form = ResponsiblePersonForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ResponsiblePerson(form.cleaned_data['name'])
            
            if ResponsiblePerson._meta.get_field('name').get_internal_type()  == 'IntegerField':
                onto_instance.name=int(form.cleaned_data['name'])
            elif ResponsiblePerson._meta.get_field('name').get_internal_type()  == 'DecimalField':
                onto_instance.name=float(form.cleaned_data['name'])
            else:
                onto_instance.name=str(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/ResponsiblePerson/list/"
            return HttpResponseRedirect(link)
    else:
        form = ResponsiblePersonForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ResponsiblePersonUpdate.html",context)

def DUTIrradiationUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["DUTIrradiation"]
    foreign_keys = dict()
    
    foreign_keys['HasCumulatedQuantity'] = 'iedm_app:CumulatedQuantityList'
    
    foreign_keys['HasDosimeter'] = 'iedm_app:IrradiationExperimentObjectList'
    
    foreign_keys['HasInteractionLength'] = 'iedm_app:InteractionLengthList'
    
    foreign_keys['HasInteractionLengthOccupancy'] = 'iedm_app:InteractionLengthOccupancyList'
    
    foreign_keys['HasMaximumTargetCumulatedQuantity'] = 'iedm_app:CumulatedQuantityList'
    
    foreign_keys['HasMinimumCumulatedQuantity'] = 'iedm_app:CumulatedQuantityList'
    
    foreign_keys['HasSample'] = 'iedm_app:IrradiationExperimentObjectList'
    
    updated_instance =  get_object_or_404(DUTIrradiation, pk=pk)
    if request.method == 'POST':
        form = DUTIrradiationForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.DUTIrradiation(form.cleaned_data['name'])
            
            if DUTIrradiation._meta.get_field('dateTimeEnd').get_internal_type()  == 'IntegerField':
                onto_instance.dateTimeEnd=int(form.cleaned_data['dateTimeEnd'])
            elif DUTIrradiation._meta.get_field('dateTimeEnd').get_internal_type()  == 'DecimalField':
                onto_instance.dateTimeEnd=float(form.cleaned_data['dateTimeEnd'])
            else:
                onto_instance.dateTimeEnd=str(form.cleaned_data['dateTimeEnd'])
            
            if DUTIrradiation._meta.get_field('dateTimeStart').get_internal_type()  == 'IntegerField':
                onto_instance.dateTimeStart=int(form.cleaned_data['dateTimeStart'])
            elif DUTIrradiation._meta.get_field('dateTimeStart').get_internal_type()  == 'DecimalField':
                onto_instance.dateTimeStart=float(form.cleaned_data['dateTimeStart'])
            else:
                onto_instance.dateTimeStart=str(form.cleaned_data['dateTimeStart'])
            
            if DUTIrradiation._meta.get_field('radiationField').get_internal_type()  == 'IntegerField':
                onto_instance.radiationField=int(form.cleaned_data['radiationField'])
            elif DUTIrradiation._meta.get_field('radiationField').get_internal_type()  == 'DecimalField':
                onto_instance.radiationField=float(form.cleaned_data['radiationField'])
            else:
                onto_instance.radiationField=str(form.cleaned_data['radiationField'])
            
            onto.save()

            
            fkstr = 'HasCumulatedQuantity'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.CumulatedQuantity(fkinstanceinput[0])
            onto.save()
            onto_instance.hasCumulatedQuantity.append(fkinstance) 
            
            fkstr = 'HasDosimeter'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.IrradiationExperimentObject(fkinstanceinput[0])
            onto.save()
            onto_instance.hasDosimeter.append(fkinstance) 
            
            fkstr = 'HasInteractionLength'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.InteractionLength(fkinstanceinput[0])
            onto.save()
            onto_instance.hasInteractionLength.append(fkinstance) 
            
            fkstr = 'HasInteractionLengthOccupancy'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.InteractionLengthOccupancy(fkinstanceinput[0])
            onto.save()
            onto_instance.hasInteractionLengthOccupancy.append(fkinstance) 
            
            fkstr = 'HasMaximumTargetCumulatedQuantity'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.CumulatedQuantity(fkinstanceinput[0])
            onto.save()
            onto_instance.hasMaximumTargetCumulatedQuantity.append(fkinstance) 
            
            fkstr = 'HasMinimumCumulatedQuantity'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.CumulatedQuantity(fkinstanceinput[0])
            onto.save()
            onto_instance.hasMinimumCumulatedQuantity.append(fkinstance) 
            
            fkstr = 'HasSample'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.IrradiationExperimentObject(fkinstanceinput[0])
            onto.save()
            onto_instance.hasSample.append(fkinstance) 
            
            onto.save()


            link = "/iedm_app/DUTIrradiation/list/"
            return HttpResponseRedirect(link)
    else:
        form = DUTIrradiationForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "DUTIrradiationUpdate.html",context)

def SampleUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Sample"]
    foreign_keys = dict()
    
    foreign_keys['CompositionDefinedBy'] = 'iedm_app:LayerTableList'
    
    updated_instance =  get_object_or_404(Sample, pk=pk)
    if request.method == 'POST':
        form = SampleForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Sample(form.cleaned_data['name'])
            
            onto.save()

            
            fkstr = 'CompositionDefinedBy'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.LayerTable(fkinstanceinput[0])
            onto.save()
            onto_instance.compositionDefinedBy.append(fkinstance) 
            
            onto.save()


            link = "/iedm_app/Sample/list/"
            return HttpResponseRedirect(link)
    else:
        form = SampleForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SampleUpdate.html",context)

def DUTUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["DUT"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(DUT, pk=pk)
    if request.method == 'POST':
        form = DUTForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.DUT(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/DUT/list/"
            return HttpResponseRedirect(link)
    else:
        form = DUTForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "DUTUpdate.html",context)

def LayerTableUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["LayerTable"]
    foreign_keys = dict()
    
    foreign_keys['HasEntries'] = 'iedm_app:LayerEntryList'
    
    updated_instance =  get_object_or_404(LayerTable, pk=pk)
    if request.method == 'POST':
        form = LayerTableForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.LayerTable(form.cleaned_data['name'])
            
            onto.save()

            
            fkstr = 'HasEntries'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.LayerEntry(fkinstanceinput[0])
            onto.save()
            onto_instance.hasEntries.append(fkinstance) 
            
            onto.save()


            link = "/iedm_app/LayerTable/list/"
            return HttpResponseRedirect(link)
    else:
        form = LayerTableForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "LayerTableUpdate.html",context)

def IrradiationExperimentUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["IrradiationExperiment"]
    foreign_keys = dict()
    
    foreign_keys['HasPart'] = 'iedm_app:RequirementsList'
    
    foreign_keys['HasPart'] = 'iedm_app:DUTIrradiationList'
    
    foreign_keys['HasPart'] = 'iedm_app:MonitoringSystemList'
    
    foreign_keys['CreatedBy'] = 'iedm_app:UserList'
    
    foreign_keys['HasFieldOfStudy'] = 'iedm_app:DomainOfExperimentList'
    
    foreign_keys['HasFinalStorageArea'] = 'iedm_app:StorageAreaList'
    
    foreign_keys['IsCoordinatedBy'] = 'iedm_app:IrradiationFacilityCoordinatorList'
    
    foreign_keys['IsOperatedBy'] = 'iedm_app:OperatorList'
    
    foreign_keys['IsPerformedIn'] = 'iedm_app:IrradiationFacilityList'
    
    foreign_keys['IsUsedBy'] = 'iedm_app:IrradiationFacilityUserList'
    
    foreign_keys['UpdatedBy'] = 'iedm_app:UserList'
    
    foreign_keys['HasResponsiblePerson'] = 'iedm_app:ResponsiblePersonList'
    
    updated_instance =  get_object_or_404(IrradiationExperiment, pk=pk)
    if request.method == 'POST':
        form = IrradiationExperimentForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.IrradiationExperiment(form.cleaned_data['name'])
            
            if IrradiationExperiment._meta.get_field('availability').get_internal_type()  == 'IntegerField':
                onto_instance.availability=int(form.cleaned_data['availability'])
            elif IrradiationExperiment._meta.get_field('availability').get_internal_type()  == 'DecimalField':
                onto_instance.availability=float(form.cleaned_data['availability'])
            else:
                onto_instance.availability=str(form.cleaned_data['availability'])
            
            onto.save()

            
            fkstr = 'HasPart'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Requirements(fkinstanceinput[0])
            onto.save()
            onto_instance.hasPart.append(fkinstance) 
            
            fkstr = 'HasPart'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.DUTIrradiation(fkinstanceinput[0])
            onto.save()
            onto_instance.hasPart.append(fkinstance) 
            
            fkstr = 'HasPart'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.MonitoringSystem(fkinstanceinput[0])
            onto.save()
            onto_instance.hasPart.append(fkinstance) 
            
            fkstr = 'CreatedBy'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.User(fkinstanceinput[0])
            onto.save()
            onto_instance.createdBy.append(fkinstance) 
            
            fkstr = 'HasFieldOfStudy'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.DomainOfExperiment(fkinstanceinput[0])
            onto.save()
            onto_instance.hasFieldOfStudy.append(fkinstance) 
            
            fkstr = 'HasFinalStorageArea'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.StorageArea(fkinstanceinput[0])
            onto.save()
            onto_instance.hasFinalStorageArea.append(fkinstance) 
            
            fkstr = 'IsCoordinatedBy'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.IrradiationFacilityCoordinator(fkinstanceinput[0])
            onto.save()
            onto_instance.isCoordinatedBy.append(fkinstance) 
            
            fkstr = 'IsOperatedBy'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Operator(fkinstanceinput[0])
            onto.save()
            onto_instance.isOperatedBy.append(fkinstance) 
            
            fkstr = 'IsPerformedIn'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.IrradiationFacility(fkinstanceinput[0])
            onto.save()
            onto_instance.isPerformedIn.append(fkinstance) 
            
            fkstr = 'IsUsedBy'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.IrradiationFacilityUser(fkinstanceinput[0])
            onto.save()
            onto_instance.isUsedBy.append(fkinstance) 
            
            fkstr = 'UpdatedBy'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.User(fkinstanceinput[0])
            onto.save()
            onto_instance.updatedBy.append(fkinstance) 
            
            fkstr = 'HasResponsiblePerson'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.ResponsiblePerson(fkinstanceinput[0])
            onto.save()
            onto_instance.hasResponsiblePerson.append(fkinstance) 
            
            onto.save()


            link = "/iedm_app/IrradiationExperiment/list/"
            return HttpResponseRedirect(link)
    else:
        form = IrradiationExperimentForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "IrradiationExperimentUpdate.html",context)

def LayerUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Layer"]
    foreign_keys = dict()
    
    foreign_keys['IsMadeOf'] = 'iedm_app:CompoundList'
    
    updated_instance =  get_object_or_404(Layer, pk=pk)
    if request.method == 'POST':
        form = LayerForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Layer(form.cleaned_data['name'])
            
            if Layer._meta.get_field('length').get_internal_type()  == 'IntegerField':
                onto_instance.length=int(form.cleaned_data['length'])
            elif Layer._meta.get_field('length').get_internal_type()  == 'DecimalField':
                onto_instance.length=float(form.cleaned_data['length'])
            else:
                onto_instance.length=str(form.cleaned_data['length'])
            
            onto.save()

            
            fkstr = 'IsMadeOf'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Compound(fkinstanceinput[0])
            onto.save()
            onto_instance.isMadeOf.append(fkinstance) 
            
            onto.save()


            link = "/iedm_app/Layer/list/"
            return HttpResponseRedirect(link)
    else:
        form = LayerForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "LayerUpdate.html",context)

def CompoundUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Compound"]
    foreign_keys = dict()
    
    foreign_keys['CompositionDefinedBy'] = 'iedm_app:CompoundWeightFractionTableList'
    
    updated_instance =  get_object_or_404(Compound, pk=pk)
    if request.method == 'POST':
        form = CompoundForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Compound(form.cleaned_data['name'])
            
            if Compound._meta.get_field('density').get_internal_type()  == 'IntegerField':
                onto_instance.density=int(form.cleaned_data['density'])
            elif Compound._meta.get_field('density').get_internal_type()  == 'DecimalField':
                onto_instance.density=float(form.cleaned_data['density'])
            else:
                onto_instance.density=str(form.cleaned_data['density'])
            
            onto.save()

            
            fkstr = 'CompositionDefinedBy'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.CompoundWeightFractionTable(fkinstanceinput[0])
            onto.save()
            onto_instance.compositionDefinedBy.append(fkinstance) 
            
            onto.save()


            link = "/iedm_app/Compound/list/"
            return HttpResponseRedirect(link)
    else:
        form = CompoundForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "CompoundUpdate.html",context)

def CompoundWeightFractionTableUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["CompoundWeightFractionTable"]
    foreign_keys = dict()
    
    foreign_keys['HasEntries'] = 'iedm_app:CompoundWeightFractionEntryList'
    
    updated_instance =  get_object_or_404(CompoundWeightFractionTable, pk=pk)
    if request.method == 'POST':
        form = CompoundWeightFractionTableForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.CompoundWeightFractionTable(form.cleaned_data['name'])
            
            onto.save()

            
            fkstr = 'HasEntries'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.CompoundWeightFractionEntry(fkinstanceinput[0])
            onto.save()
            onto_instance.hasEntries.append(fkinstance) 
            
            onto.save()


            link = "/iedm_app/CompoundWeightFractionTable/list/"
            return HttpResponseRedirect(link)
    else:
        form = CompoundWeightFractionTableForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "CompoundWeightFractionTableUpdate.html",context)

def IrradiationFacilityUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["IrradiationFacility"]
    foreign_keys = dict()
    
    foreign_keys['IsManagedBy'] = 'iedm_app:IrradiationFacilityManagerList'
    
    updated_instance =  get_object_or_404(IrradiationFacility, pk=pk)
    if request.method == 'POST':
        form = IrradiationFacilityForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.IrradiationFacility(form.cleaned_data['name'])
            
            if IrradiationFacility._meta.get_field('name').get_internal_type()  == 'IntegerField':
                onto_instance.name=int(form.cleaned_data['name'])
            elif IrradiationFacility._meta.get_field('name').get_internal_type()  == 'DecimalField':
                onto_instance.name=float(form.cleaned_data['name'])
            else:
                onto_instance.name=str(form.cleaned_data['name'])
            
            onto.save()

            
            fkstr = 'IsManagedBy'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.IrradiationFacilityManager(fkinstanceinput[0])
            onto.save()
            onto_instance.isManagedBy.append(fkinstance) 
            
            onto.save()


            link = "/iedm_app/IrradiationFacility/list/"
            return HttpResponseRedirect(link)
    else:
        form = IrradiationFacilityForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "IrradiationFacilityUpdate.html",context)

def PhysicalUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Physical"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Physical, pk=pk)
    if request.method == 'POST':
        form = PhysicalForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Physical(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/Physical/list/"
            return HttpResponseRedirect(link)
    else:
        form = PhysicalForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "PhysicalUpdate.html",context)

def IrradiationFacilityUserUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["IrradiationFacilityUser"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(IrradiationFacilityUser, pk=pk)
    if request.method == 'POST':
        form = IrradiationFacilityUserForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.IrradiationFacilityUser(form.cleaned_data['name'])
            
            if IrradiationFacilityUser._meta.get_field('name').get_internal_type()  == 'IntegerField':
                onto_instance.name=int(form.cleaned_data['name'])
            elif IrradiationFacilityUser._meta.get_field('name').get_internal_type()  == 'DecimalField':
                onto_instance.name=float(form.cleaned_data['name'])
            else:
                onto_instance.name=str(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/IrradiationFacilityUser/list/"
            return HttpResponseRedirect(link)
    else:
        form = IrradiationFacilityUserForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "IrradiationFacilityUserUpdate.html",context)

def PredicateUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Predicate"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Predicate, pk=pk)
    if request.method == 'POST':
        form = PredicateForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Predicate(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/Predicate/list/"
            return HttpResponseRedirect(link)
    else:
        form = PredicateForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "PredicateUpdate.html",context)

def SentientAgentUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["SentientAgent"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(SentientAgent, pk=pk)
    if request.method == 'POST':
        form = SentientAgentForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.SentientAgent(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/SentientAgent/list/"
            return HttpResponseRedirect(link)
    else:
        form = SentientAgentForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SentientAgentUpdate.html",context)

def AgentUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Agent"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Agent, pk=pk)
    if request.method == 'POST':
        form = AgentForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Agent(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/Agent/list/"
            return HttpResponseRedirect(link)
    else:
        form = AgentForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AgentUpdate.html",context)

def CompoundWeightFractionEntryUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["CompoundWeightFractionEntry"]
    foreign_keys = dict()
    
    foreign_keys['HasArgument'] = 'iedm_app:ElementList'
    
    updated_instance =  get_object_or_404(CompoundWeightFractionEntry, pk=pk)
    if request.method == 'POST':
        form = CompoundWeightFractionEntryForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.CompoundWeightFractionEntry(form.cleaned_data['name'])
            
            if CompoundWeightFractionEntry._meta.get_field('hasValue').get_internal_type()  == 'IntegerField':
                onto_instance.hasValue=int(form.cleaned_data['hasValue'])
            elif CompoundWeightFractionEntry._meta.get_field('hasValue').get_internal_type()  == 'DecimalField':
                onto_instance.hasValue=float(form.cleaned_data['hasValue'])
            else:
                onto_instance.hasValue=str(form.cleaned_data['hasValue'])
            
            onto.save()

            
            fkstr = 'HasArgument'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Element(fkinstanceinput[0])
            onto.save()
            onto_instance.hasArgument.append(fkinstance) 
            
            onto.save()


            link = "/iedm_app/CompoundWeightFractionEntry/list/"
            return HttpResponseRedirect(link)
    else:
        form = CompoundWeightFractionEntryForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "CompoundWeightFractionEntryUpdate.html",context)

def ElementUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Element"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Element, pk=pk)
    if request.method == 'POST':
        form = ElementForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Element(form.cleaned_data['name'])
            
            if Element._meta.get_field('name').get_internal_type()  == 'IntegerField':
                onto_instance.name=int(form.cleaned_data['name'])
            elif Element._meta.get_field('name').get_internal_type()  == 'DecimalField':
                onto_instance.name=float(form.cleaned_data['name'])
            else:
                onto_instance.name=str(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/Element/list/"
            return HttpResponseRedirect(link)
    else:
        form = ElementForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ElementUpdate.html",context)

def UnaryFunctionEntryUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["UnaryFunctionEntry"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(UnaryFunctionEntry, pk=pk)
    if request.method == 'POST':
        form = UnaryFunctionEntryForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.UnaryFunctionEntry(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/UnaryFunctionEntry/list/"
            return HttpResponseRedirect(link)
    else:
        form = UnaryFunctionEntryForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "UnaryFunctionEntryUpdate.html",context)

def CumulatedQuantityUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["CumulatedQuantity"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(CumulatedQuantity, pk=pk)
    if request.method == 'POST':
        form = CumulatedQuantityForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.CumulatedQuantity(form.cleaned_data['name'])
            
            if CumulatedQuantity._meta.get_field('hasValue').get_internal_type()  == 'IntegerField':
                onto_instance.hasValue=int(form.cleaned_data['hasValue'])
            elif CumulatedQuantity._meta.get_field('hasValue').get_internal_type()  == 'DecimalField':
                onto_instance.hasValue=float(form.cleaned_data['hasValue'])
            else:
                onto_instance.hasValue=str(form.cleaned_data['hasValue'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/CumulatedQuantity/list/"
            return HttpResponseRedirect(link)
    else:
        form = CumulatedQuantityForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "CumulatedQuantityUpdate.html",context)

def IrradiationExperimentObjectUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["IrradiationExperimentObject"]
    foreign_keys = dict()
    
    foreign_keys['UpdatedBy'] = 'iedm_app:UserList'
    
    foreign_keys['CreatedBy'] = 'iedm_app:UserList'
    
    updated_instance =  get_object_or_404(IrradiationExperimentObject, pk=pk)
    if request.method == 'POST':
        form = IrradiationExperimentObjectForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.IrradiationExperimentObject(form.cleaned_data['name'])
            
            if IrradiationExperimentObject._meta.get_field('height').get_internal_type()  == 'IntegerField':
                onto_instance.height=int(form.cleaned_data['height'])
            elif IrradiationExperimentObject._meta.get_field('height').get_internal_type()  == 'DecimalField':
                onto_instance.height=float(form.cleaned_data['height'])
            else:
                onto_instance.height=str(form.cleaned_data['height'])
            
            if IrradiationExperimentObject._meta.get_field('identification').get_internal_type()  == 'IntegerField':
                onto_instance.identification=int(form.cleaned_data['identification'])
            elif IrradiationExperimentObject._meta.get_field('identification').get_internal_type()  == 'DecimalField':
                onto_instance.identification=float(form.cleaned_data['identification'])
            else:
                onto_instance.identification=str(form.cleaned_data['identification'])
            
            if IrradiationExperimentObject._meta.get_field('location').get_internal_type()  == 'IntegerField':
                onto_instance.location=int(form.cleaned_data['location'])
            elif IrradiationExperimentObject._meta.get_field('location').get_internal_type()  == 'DecimalField':
                onto_instance.location=float(form.cleaned_data['location'])
            else:
                onto_instance.location=str(form.cleaned_data['location'])
            
            if IrradiationExperimentObject._meta.get_field('weight').get_internal_type()  == 'IntegerField':
                onto_instance.weight=int(form.cleaned_data['weight'])
            elif IrradiationExperimentObject._meta.get_field('weight').get_internal_type()  == 'DecimalField':
                onto_instance.weight=float(form.cleaned_data['weight'])
            else:
                onto_instance.weight=str(form.cleaned_data['weight'])
            
            if IrradiationExperimentObject._meta.get_field('width').get_internal_type()  == 'IntegerField':
                onto_instance.width=int(form.cleaned_data['width'])
            elif IrradiationExperimentObject._meta.get_field('width').get_internal_type()  == 'DecimalField':
                onto_instance.width=float(form.cleaned_data['width'])
            else:
                onto_instance.width=str(form.cleaned_data['width'])
            
            onto.save()

            
            fkstr = 'UpdatedBy'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.User(fkinstanceinput[0])
            onto.save()
            onto_instance.updatedBy.append(fkinstance) 
            
            fkstr = 'CreatedBy'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.User(fkinstanceinput[0])
            onto.save()
            onto_instance.createdBy.append(fkinstance) 
            
            onto.save()


            link = "/iedm_app/IrradiationExperimentObject/list/"
            return HttpResponseRedirect(link)
    else:
        form = IrradiationExperimentObjectForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "IrradiationExperimentObjectUpdate.html",context)

def InteractionLengthUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["InteractionLength"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(InteractionLength, pk=pk)
    if request.method == 'POST':
        form = InteractionLengthForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.InteractionLength(form.cleaned_data['name'])
            
            if InteractionLength._meta.get_field('length').get_internal_type()  == 'IntegerField':
                onto_instance.length=int(form.cleaned_data['length'])
            elif InteractionLength._meta.get_field('length').get_internal_type()  == 'DecimalField':
                onto_instance.length=float(form.cleaned_data['length'])
            else:
                onto_instance.length=str(form.cleaned_data['length'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/InteractionLength/list/"
            return HttpResponseRedirect(link)
    else:
        form = InteractionLengthForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "InteractionLengthUpdate.html",context)

def InteractionLengthOccupancyUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["InteractionLengthOccupancy"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(InteractionLengthOccupancy, pk=pk)
    if request.method == 'POST':
        form = InteractionLengthOccupancyForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.InteractionLengthOccupancy(form.cleaned_data['name'])
            
            if InteractionLengthOccupancy._meta.get_field('hasValue').get_internal_type()  == 'IntegerField':
                onto_instance.hasValue=int(form.cleaned_data['hasValue'])
            elif InteractionLengthOccupancy._meta.get_field('hasValue').get_internal_type()  == 'DecimalField':
                onto_instance.hasValue=float(form.cleaned_data['hasValue'])
            else:
                onto_instance.hasValue=str(form.cleaned_data['hasValue'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/InteractionLengthOccupancy/list/"
            return HttpResponseRedirect(link)
    else:
        form = InteractionLengthOccupancyForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "InteractionLengthOccupancyUpdate.html",context)

def DosimetricQuantityUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["DosimetricQuantity"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(DosimetricQuantity, pk=pk)
    if request.method == 'POST':
        form = DosimetricQuantityForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.DosimetricQuantity(form.cleaned_data['name'])
            
            if DosimetricQuantity._meta.get_field('hasValue').get_internal_type()  == 'IntegerField':
                onto_instance.hasValue=int(form.cleaned_data['hasValue'])
            elif DosimetricQuantity._meta.get_field('hasValue').get_internal_type()  == 'DecimalField':
                onto_instance.hasValue=float(form.cleaned_data['hasValue'])
            else:
                onto_instance.hasValue=str(form.cleaned_data['hasValue'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/DosimetricQuantity/list/"
            return HttpResponseRedirect(link)
    else:
        form = DosimetricQuantityForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "DosimetricQuantityUpdate.html",context)

def DosimeterUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Dosimeter"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Dosimeter, pk=pk)
    if request.method == 'POST':
        form = DosimeterForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Dosimeter(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/Dosimeter/list/"
            return HttpResponseRedirect(link)
    else:
        form = DosimeterForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "DosimeterUpdate.html",context)

def DomainOfExperimentUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["DomainOfExperiment"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(DomainOfExperiment, pk=pk)
    if request.method == 'POST':
        form = DomainOfExperimentForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.DomainOfExperiment(form.cleaned_data['name'])
            
            if DomainOfExperiment._meta.get_field('name').get_internal_type()  == 'IntegerField':
                onto_instance.name=int(form.cleaned_data['name'])
            elif DomainOfExperiment._meta.get_field('name').get_internal_type()  == 'DecimalField':
                onto_instance.name=float(form.cleaned_data['name'])
            else:
                onto_instance.name=str(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/DomainOfExperiment/list/"
            return HttpResponseRedirect(link)
    else:
        form = DomainOfExperimentForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "DomainOfExperimentUpdate.html",context)

def ParticlePhysicsExperimentUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ParticlePhysicsExperiment"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ParticlePhysicsExperiment, pk=pk)
    if request.method == 'POST':
        form = ParticlePhysicsExperimentForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ParticlePhysicsExperiment(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/ParticlePhysicsExperiment/list/"
            return HttpResponseRedirect(link)
    else:
        form = ParticlePhysicsExperimentForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ParticlePhysicsExperimentUpdate.html",context)

def RequirementsUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Requirements"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Requirements, pk=pk)
    if request.method == 'POST':
        form = RequirementsForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Requirements(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/Requirements/list/"
            return HttpResponseRedirect(link)
    else:
        form = RequirementsForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "RequirementsUpdate.html",context)

def MonitoringSystemUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["MonitoringSystem"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(MonitoringSystem, pk=pk)
    if request.method == 'POST':
        form = MonitoringSystemForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.MonitoringSystem(form.cleaned_data['name'])
            
            if MonitoringSystem._meta.get_field('name').get_internal_type()  == 'IntegerField':
                onto_instance.name=int(form.cleaned_data['name'])
            elif MonitoringSystem._meta.get_field('name').get_internal_type()  == 'DecimalField':
                onto_instance.name=float(form.cleaned_data['name'])
            else:
                onto_instance.name=str(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/MonitoringSystem/list/"
            return HttpResponseRedirect(link)
    else:
        form = MonitoringSystemForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "MonitoringSystemUpdate.html",context)

def StorageAreaUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["StorageArea"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(StorageArea, pk=pk)
    if request.method == 'POST':
        form = StorageAreaForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.StorageArea(form.cleaned_data['name'])
            
            if StorageArea._meta.get_field('name').get_internal_type()  == 'IntegerField':
                onto_instance.name=int(form.cleaned_data['name'])
            elif StorageArea._meta.get_field('name').get_internal_type()  == 'DecimalField':
                onto_instance.name=float(form.cleaned_data['name'])
            else:
                onto_instance.name=str(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/StorageArea/list/"
            return HttpResponseRedirect(link)
    else:
        form = StorageAreaForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "StorageAreaUpdate.html",context)

def FieldOfStudyUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["FieldOfStudy"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(FieldOfStudy, pk=pk)
    if request.method == 'POST':
        form = FieldOfStudyForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.FieldOfStudy(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/FieldOfStudy/list/"
            return HttpResponseRedirect(link)
    else:
        form = FieldOfStudyForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "FieldOfStudyUpdate.html",context)

def IrradiationFacilityPostionUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["IrradiationFacilityPostion"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(IrradiationFacilityPostion, pk=pk)
    if request.method == 'POST':
        form = IrradiationFacilityPostionForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.IrradiationFacilityPostion(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/IrradiationFacilityPostion/list/"
            return HttpResponseRedirect(link)
    else:
        form = IrradiationFacilityPostionForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "IrradiationFacilityPostionUpdate.html",context)

def LengthUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Length"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Length, pk=pk)
    if request.method == 'POST':
        form = LengthForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Length(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/Length/list/"
            return HttpResponseRedirect(link)
    else:
        form = LengthForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "LengthUpdate.html",context)

def PercentageUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Percentage"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Percentage, pk=pk)
    if request.method == 'POST':
        form = PercentageForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Percentage(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/Percentage/list/"
            return HttpResponseRedirect(link)
    else:
        form = PercentageForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "PercentageUpdate.html",context)

def LayerEntryUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["LayerEntry"]
    foreign_keys = dict()
    
    foreign_keys['HasEntries'] = 'iedm_app:LayerList'
    
    updated_instance =  get_object_or_404(LayerEntry, pk=pk)
    if request.method == 'POST':
        form = LayerEntryForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.LayerEntry(form.cleaned_data['name'])
            
            if LayerEntry._meta.get_field('ordinal').get_internal_type()  == 'IntegerField':
                onto_instance.ordinal=int(form.cleaned_data['ordinal'])
            elif LayerEntry._meta.get_field('ordinal').get_internal_type()  == 'DecimalField':
                onto_instance.ordinal=float(form.cleaned_data['ordinal'])
            else:
                onto_instance.ordinal=str(form.cleaned_data['ordinal'])
            
            onto.save()

            
            fkstr = 'HasEntries'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Layer(fkinstanceinput[0])
            onto.save()
            onto_instance.hasEntries.append(fkinstance) 
            
            onto.save()


            link = "/iedm_app/LayerEntry/list/"
            return HttpResponseRedirect(link)
    else:
        form = LayerEntryForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "LayerEntryUpdate.html",context)

def RelationUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Relation"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Relation, pk=pk)
    if request.method == 'POST':
        form = RelationForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Relation(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/Relation/list/"
            return HttpResponseRedirect(link)
    else:
        form = RelationForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "RelationUpdate.html",context)

def AbstractUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Abstract"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Abstract, pk=pk)
    if request.method == 'POST':
        form = AbstractForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Abstract(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/Abstract/list/"
            return HttpResponseRedirect(link)
    else:
        form = AbstractForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AbstractUpdate.html",context)

def RegionUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Region"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Region, pk=pk)
    if request.method == 'POST':
        form = RegionForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Region(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/Region/list/"
            return HttpResponseRedirect(link)
    else:
        form = RegionForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "RegionUpdate.html",context)

def SubjectRoleUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["SubjectRole"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(SubjectRole, pk=pk)
    if request.method == 'POST':
        form = SubjectRoleForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.SubjectRole(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/SubjectRole/list/"
            return HttpResponseRedirect(link)
    else:
        form = SubjectRoleForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SubjectRoleUpdate.html",context)

def ActorRoleUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ActorRole"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ActorRole, pk=pk)
    if request.method == 'POST':
        form = ActorRoleForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ActorRole(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/ActorRole/list/"
            return HttpResponseRedirect(link)
    else:
        form = ActorRoleForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ActorRoleUpdate.html",context)

def AbsorbedDoseUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["AbsorbedDose"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(AbsorbedDose, pk=pk)
    if request.method == 'POST':
        form = AbsorbedDoseForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.AbsorbedDose(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/AbsorbedDose/list/"
            return HttpResponseRedirect(link)
    else:
        form = AbsorbedDoseForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AbsorbedDoseUpdate.html",context)

def AbsorbedDoseInMediumUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["AbsorbedDoseInMedium"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(AbsorbedDoseInMedium, pk=pk)
    if request.method == 'POST':
        form = AbsorbedDoseInMediumForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.AbsorbedDoseInMedium(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/AbsorbedDoseInMedium/list/"
            return HttpResponseRedirect(link)
    else:
        form = AbsorbedDoseInMediumForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AbsorbedDoseInMediumUpdate.html",context)

def QuantityUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Quantity"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Quantity, pk=pk)
    if request.method == 'POST':
        form = QuantityForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Quantity(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/Quantity/list/"
            return HttpResponseRedirect(link)
    else:
        form = QuantityForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "QuantityUpdate.html",context)

def ActivityUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Activity"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Activity, pk=pk)
    if request.method == 'POST':
        form = ActivityForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Activity(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/Activity/list/"
            return HttpResponseRedirect(link)
    else:
        form = ActivityForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ActivityUpdate.html",context)

def DosimetricRateUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["DosimetricRate"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(DosimetricRate, pk=pk)
    if request.method == 'POST':
        form = DosimetricRateForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.DosimetricRate(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/DosimetricRate/list/"
            return HttpResponseRedirect(link)
    else:
        form = DosimetricRateForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "DosimetricRateUpdate.html",context)

def AtomicMassUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["AtomicMass"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(AtomicMass, pk=pk)
    if request.method == 'POST':
        form = AtomicMassForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.AtomicMass(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/AtomicMass/list/"
            return HttpResponseRedirect(link)
    else:
        form = AtomicMassForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AtomicMassUpdate.html",context)

def MassUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Mass"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Mass, pk=pk)
    if request.method == 'POST':
        form = MassForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Mass(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/Mass/list/"
            return HttpResponseRedirect(link)
    else:
        form = MassForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "MassUpdate.html",context)

def DensityUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Density"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Density, pk=pk)
    if request.method == 'POST':
        form = DensityForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Density(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/Density/list/"
            return HttpResponseRedirect(link)
    else:
        form = DensityForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "DensityUpdate.html",context)

def EnergyUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Energy"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Energy, pk=pk)
    if request.method == 'POST':
        form = EnergyForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Energy(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/Energy/list/"
            return HttpResponseRedirect(link)
    else:
        form = EnergyForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "EnergyUpdate.html",context)

def HeightUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Height"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Height, pk=pk)
    if request.method == 'POST':
        form = HeightForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Height(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/Height/list/"
            return HttpResponseRedirect(link)
    else:
        form = HeightForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "HeightUpdate.html",context)

def NumberUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Number"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Number, pk=pk)
    if request.method == 'POST':
        form = NumberForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Number(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/Number/list/"
            return HttpResponseRedirect(link)
    else:
        form = NumberForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "NumberUpdate.html",context)

def RatioUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Ratio"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Ratio, pk=pk)
    if request.method == 'POST':
        form = RatioForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Ratio(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/Ratio/list/"
            return HttpResponseRedirect(link)
    else:
        form = RatioForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "RatioUpdate.html",context)

def TemperatureUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Temperature"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Temperature, pk=pk)
    if request.method == 'POST':
        form = TemperatureForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Temperature(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/Temperature/list/"
            return HttpResponseRedirect(link)
    else:
        form = TemperatureForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "TemperatureUpdate.html",context)

def ThicknessUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Thickness"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Thickness, pk=pk)
    if request.method == 'POST':
        form = ThicknessForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Thickness(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/Thickness/list/"
            return HttpResponseRedirect(link)
    else:
        form = ThicknessForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ThicknessUpdate.html",context)

def AbsorbedDoseInAirUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["AbsorbedDoseInAir"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(AbsorbedDoseInAir, pk=pk)
    if request.method == 'POST':
        form = AbsorbedDoseInAirForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.AbsorbedDoseInAir(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/AbsorbedDoseInAir/list/"
            return HttpResponseRedirect(link)
    else:
        form = AbsorbedDoseInAirForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AbsorbedDoseInAirUpdate.html",context)

def AdminInfoIrradiationExperimentUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["AdminInfoIrradiationExperiment"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(AdminInfoIrradiationExperiment, pk=pk)
    if request.method == 'POST':
        form = AdminInfoIrradiationExperimentForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.AdminInfoIrradiationExperiment(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/AdminInfoIrradiationExperiment/list/"
            return HttpResponseRedirect(link)
    else:
        form = AdminInfoIrradiationExperimentForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AdminInfoIrradiationExperimentUpdate.html",context)

def AdminInfoExperimentUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["AdminInfoExperiment"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(AdminInfoExperiment, pk=pk)
    if request.method == 'POST':
        form = AdminInfoExperimentForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.AdminInfoExperiment(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/AdminInfoExperiment/list/"
            return HttpResponseRedirect(link)
    else:
        form = AdminInfoExperimentForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AdminInfoExperimentUpdate.html",context)

def ActionrelatedRoleUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ActionrelatedRole"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ActionrelatedRole, pk=pk)
    if request.method == 'POST':
        form = ActionrelatedRoleForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ActionrelatedRole(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/ActionrelatedRole/list/"
            return HttpResponseRedirect(link)
    else:
        form = ActionrelatedRoleForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ActionrelatedRoleUpdate.html",context)

def RoleUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Role"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Role, pk=pk)
    if request.method == 'POST':
        form = RoleForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Role(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/Role/list/"
            return HttpResponseRedirect(link)
    else:
        form = RoleForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "RoleUpdate.html",context)

def AdministrativeInformationUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["AdministrativeInformation"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(AdministrativeInformation, pk=pk)
    if request.method == 'POST':
        form = AdministrativeInformationForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.AdministrativeInformation(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/AdministrativeInformation/list/"
            return HttpResponseRedirect(link)
    else:
        form = AdministrativeInformationForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AdministrativeInformationUpdate.html",context)

def PropositionUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Proposition"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Proposition, pk=pk)
    if request.method == 'POST':
        form = PropositionForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Proposition(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/Proposition/list/"
            return HttpResponseRedirect(link)
    else:
        form = PropositionForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "PropositionUpdate.html",context)

def AdminInfoObjectOfExperimentUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["AdminInfoObjectOfExperiment"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(AdminInfoObjectOfExperiment, pk=pk)
    if request.method == 'POST':
        form = AdminInfoObjectOfExperimentForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.AdminInfoObjectOfExperiment(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/AdminInfoObjectOfExperiment/list/"
            return HttpResponseRedirect(link)
    else:
        form = AdminInfoObjectOfExperimentForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AdminInfoObjectOfExperimentUpdate.html",context)

def AdminInfoUserUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["AdminInfoUser"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(AdminInfoUser, pk=pk)
    if request.method == 'POST':
        form = AdminInfoUserForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.AdminInfoUser(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/AdminInfoUser/list/"
            return HttpResponseRedirect(link)
    else:
        form = AdminInfoUserForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AdminInfoUserUpdate.html",context)

def ClassificationUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Classification"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Classification, pk=pk)
    if request.method == 'POST':
        form = ClassificationForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Classification(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/Classification/list/"
            return HttpResponseRedirect(link)
    else:
        form = ClassificationForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ClassificationUpdate.html",context)

def ScientificTaskUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ScientificTask"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ScientificTask, pk=pk)
    if request.method == 'POST':
        form = ScientificTaskForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ScientificTask(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/ScientificTask/list/"
            return HttpResponseRedirect(link)
    else:
        form = ScientificTaskForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ScientificTaskUpdate.html",context)

def ClassificationByDomainUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ClassificationByDomain"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ClassificationByDomain, pk=pk)
    if request.method == 'POST':
        form = ClassificationByDomainForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ClassificationByDomain(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/ClassificationByDomain/list/"
            return HttpResponseRedirect(link)
    else:
        form = ClassificationByDomainForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ClassificationByDomainUpdate.html",context)

def ClassificationOfExperimentsUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ClassificationOfExperiments"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ClassificationOfExperiments, pk=pk)
    if request.method == 'POST':
        form = ClassificationOfExperimentsForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ClassificationOfExperiments(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/ClassificationOfExperiments/list/"
            return HttpResponseRedirect(link)
    else:
        form = ClassificationOfExperimentsForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ClassificationOfExperimentsUpdate.html",context)

def CorpuscularObjectUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["CorpuscularObject"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(CorpuscularObject, pk=pk)
    if request.method == 'POST':
        form = CorpuscularObjectForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.CorpuscularObject(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/CorpuscularObject/list/"
            return HttpResponseRedirect(link)
    else:
        form = CorpuscularObjectForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "CorpuscularObjectUpdate.html",context)

def SelfConnectedObjectUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["SelfConnectedObject"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(SelfConnectedObject, pk=pk)
    if request.method == 'POST':
        form = SelfConnectedObjectForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.SelfConnectedObject(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/SelfConnectedObject/list/"
            return HttpResponseRedirect(link)
    else:
        form = SelfConnectedObjectForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SelfConnectedObjectUpdate.html",context)

def ExperimentalRequirementsUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ExperimentalRequirements"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ExperimentalRequirements, pk=pk)
    if request.method == 'POST':
        form = ExperimentalRequirementsForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ExperimentalRequirements(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/ExperimentalRequirements/list/"
            return HttpResponseRedirect(link)
    else:
        form = ExperimentalRequirementsForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ExperimentalRequirementsUpdate.html",context)

def AdminInfoIrradiationExperimentUserUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["AdminInfoIrradiationExperimentUser"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(AdminInfoIrradiationExperimentUser, pk=pk)
    if request.method == 'POST':
        form = AdminInfoIrradiationExperimentUserForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.AdminInfoIrradiationExperimentUser(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/AdminInfoIrradiationExperimentUser/list/"
            return HttpResponseRedirect(link)
    else:
        form = AdminInfoIrradiationExperimentUserForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AdminInfoIrradiationExperimentUserUpdate.html",context)

def AdminInfoObjectOfIrradiationUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["AdminInfoObjectOfIrradiation"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(AdminInfoObjectOfIrradiation, pk=pk)
    if request.method == 'POST':
        form = AdminInfoObjectOfIrradiationForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.AdminInfoObjectOfIrradiation(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/AdminInfoObjectOfIrradiation/list/"
            return HttpResponseRedirect(link)
    else:
        form = AdminInfoObjectOfIrradiationForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AdminInfoObjectOfIrradiationUpdate.html",context)

def AdminInfoObjectOfIrradiationExperimentUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["AdminInfoObjectOfIrradiationExperiment"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(AdminInfoObjectOfIrradiationExperiment, pk=pk)
    if request.method == 'POST':
        form = AdminInfoObjectOfIrradiationExperimentForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.AdminInfoObjectOfIrradiationExperiment(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/AdminInfoObjectOfIrradiationExperiment/list/"
            return HttpResponseRedirect(link)
    else:
        form = AdminInfoObjectOfIrradiationExperimentForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AdminInfoObjectOfIrradiationExperimentUpdate.html",context)

def AtomicNumberUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["AtomicNumber"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(AtomicNumber, pk=pk)
    if request.method == 'POST':
        form = AtomicNumberForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.AtomicNumber(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/AtomicNumber/list/"
            return HttpResponseRedirect(link)
    else:
        form = AtomicNumberForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AtomicNumberUpdate.html",context)

def CollaborationRequirementsUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["CollaborationRequirements"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(CollaborationRequirements, pk=pk)
    if request.method == 'POST':
        form = CollaborationRequirementsForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.CollaborationRequirements(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/CollaborationRequirements/list/"
            return HttpResponseRedirect(link)
    else:
        form = CollaborationRequirementsForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "CollaborationRequirementsUpdate.html",context)

def ControlRoomUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ControlRoom"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ControlRoom, pk=pk)
    if request.method == 'POST':
        form = ControlRoomForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ControlRoom(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/ControlRoom/list/"
            return HttpResponseRedirect(link)
    else:
        form = ControlRoomForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ControlRoomUpdate.html",context)

def ControlSystemUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ControlSystem"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ControlSystem, pk=pk)
    if request.method == 'POST':
        form = ControlSystemForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ControlSystem(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/ControlSystem/list/"
            return HttpResponseRedirect(link)
    else:
        form = ControlSystemForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ControlSystemUpdate.html",context)

def SystemUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["System"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(System, pk=pk)
    if request.method == 'POST':
        form = SystemForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.System(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/System/list/"
            return HttpResponseRedirect(link)
    else:
        form = SystemForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SystemUpdate.html",context)

def RadiationLengthUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["RadiationLength"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(RadiationLength, pk=pk)
    if request.method == 'POST':
        form = RadiationLengthForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.RadiationLength(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/RadiationLength/list/"
            return HttpResponseRedirect(link)
    else:
        form = RadiationLengthForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "RadiationLengthUpdate.html",context)

def RadiationLengthOccupancyUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["RadiationLengthOccupancy"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(RadiationLengthOccupancy, pk=pk)
    if request.method == 'POST':
        form = RadiationLengthOccupancyForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.RadiationLengthOccupancy(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/RadiationLengthOccupancy/list/"
            return HttpResponseRedirect(link)
    else:
        form = RadiationLengthOccupancyForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "RadiationLengthOccupancyUpdate.html",context)

def RadioprotectionRequirementsUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["RadioprotectionRequirements"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(RadioprotectionRequirements, pk=pk)
    if request.method == 'POST':
        form = RadioprotectionRequirementsForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.RadioprotectionRequirements(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/RadioprotectionRequirements/list/"
            return HttpResponseRedirect(link)
    else:
        form = RadioprotectionRequirementsForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "RadioprotectionRequirementsUpdate.html",context)

def SingularFieldUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["SingularField"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(SingularField, pk=pk)
    if request.method == 'POST':
        form = SingularFieldForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.SingularField(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/SingularField/list/"
            return HttpResponseRedirect(link)
    else:
        form = SingularFieldForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SingularFieldUpdate.html",context)

def RadiationFieldUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["RadiationField"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(RadiationField, pk=pk)
    if request.method == 'POST':
        form = RadiationFieldForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.RadiationField(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/RadiationField/list/"
            return HttpResponseRedirect(link)
    else:
        form = RadiationFieldForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "RadiationFieldUpdate.html",context)

def TechnicalRequirementsUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["TechnicalRequirements"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(TechnicalRequirements, pk=pk)
    if request.method == 'POST':
        form = TechnicalRequirementsForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.TechnicalRequirements(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/TechnicalRequirements/list/"
            return HttpResponseRedirect(link)
    else:
        form = TechnicalRequirementsForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "TechnicalRequirementsUpdate.html",context)

def IrradiationFacilityRequirementsUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["IrradiationFacilityRequirements"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(IrradiationFacilityRequirements, pk=pk)
    if request.method == 'POST':
        form = IrradiationFacilityRequirementsForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.IrradiationFacilityRequirements(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/IrradiationFacilityRequirements/list/"
            return HttpResponseRedirect(link)
    else:
        form = IrradiationFacilityRequirementsForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "IrradiationFacilityRequirementsUpdate.html",context)

def FluenceUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Fluence"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Fluence, pk=pk)
    if request.method == 'POST':
        form = FluenceForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Fluence(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/Fluence/list/"
            return HttpResponseRedirect(link)
    else:
        form = FluenceForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "FluenceUpdate.html",context)

def NuclearCollisionLengthUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["NuclearCollisionLength"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(NuclearCollisionLength, pk=pk)
    if request.method == 'POST':
        form = NuclearCollisionLengthForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.NuclearCollisionLength(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/NuclearCollisionLength/list/"
            return HttpResponseRedirect(link)
    else:
        form = NuclearCollisionLengthForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "NuclearCollisionLengthUpdate.html",context)

def NuclearInteractionLengthUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["NuclearInteractionLength"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(NuclearInteractionLength, pk=pk)
    if request.method == 'POST':
        form = NuclearInteractionLengthForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.NuclearInteractionLength(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/NuclearInteractionLength/list/"
            return HttpResponseRedirect(link)
    else:
        form = NuclearInteractionLengthForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "NuclearInteractionLengthUpdate.html",context)

def DataManagementSystemUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["DataManagementSystem"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(DataManagementSystem, pk=pk)
    if request.method == 'POST':
        form = DataManagementSystemForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.DataManagementSystem(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/DataManagementSystem/list/"
            return HttpResponseRedirect(link)
    else:
        form = DataManagementSystemForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "DataManagementSystemUpdate.html",context)

def DoseRateUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["DoseRate"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(DoseRate, pk=pk)
    if request.method == 'POST':
        form = DoseRateForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.DoseRate(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/DoseRate/list/"
            return HttpResponseRedirect(link)
    else:
        form = DoseRateForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "DoseRateUpdate.html",context)

def ExternalPositionUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ExternalPosition"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ExternalPosition, pk=pk)
    if request.method == 'POST':
        form = ExternalPositionForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ExternalPosition(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/ExternalPosition/list/"
            return HttpResponseRedirect(link)
    else:
        form = ExternalPositionForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ExternalPositionUpdate.html",context)

def FluenceRateUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["FluenceRate"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(FluenceRate, pk=pk)
    if request.method == 'POST':
        form = FluenceRateForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.FluenceRate(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/FluenceRate/list/"
            return HttpResponseRedirect(link)
    else:
        form = FluenceRateForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "FluenceRateUpdate.html",context)

def InteractionLengthEntryUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["InteractionLengthEntry"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(InteractionLengthEntry, pk=pk)
    if request.method == 'POST':
        form = InteractionLengthEntryForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.InteractionLengthEntry(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/InteractionLengthEntry/list/"
            return HttpResponseRedirect(link)
    else:
        form = InteractionLengthEntryForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "InteractionLengthEntryUpdate.html",context)

def InteractionLengthTableUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["InteractionLengthTable"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(InteractionLengthTable, pk=pk)
    if request.method == 'POST':
        form = InteractionLengthTableForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.InteractionLengthTable(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/InteractionLengthTable/list/"
            return HttpResponseRedirect(link)
    else:
        form = InteractionLengthTableForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "InteractionLengthTableUpdate.html",context)

def IrradiationPositionUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["IrradiationPosition"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(IrradiationPosition, pk=pk)
    if request.method == 'POST':
        form = IrradiationPositionForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.IrradiationPosition(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/IrradiationPosition/list/"
            return HttpResponseRedirect(link)
    else:
        form = IrradiationPositionForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "IrradiationPositionUpdate.html",context)

def IrradiationZoneUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["IrradiationZone"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(IrradiationZone, pk=pk)
    if request.method == 'POST':
        form = IrradiationZoneForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.IrradiationZone(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/IrradiationZone/list/"
            return HttpResponseRedirect(link)
    else:
        form = IrradiationZoneForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "IrradiationZoneUpdate.html",context)

def LaboratoryUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Laboratory"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Laboratory, pk=pk)
    if request.method == 'POST':
        form = LaboratoryForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Laboratory(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/Laboratory/list/"
            return HttpResponseRedirect(link)
    else:
        form = LaboratoryForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "LaboratoryUpdate.html",context)

def LegalRequirementsUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["LegalRequirements"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(LegalRequirements, pk=pk)
    if request.method == 'POST':
        form = LegalRequirementsForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.LegalRequirements(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/LegalRequirements/list/"
            return HttpResponseRedirect(link)
    else:
        form = LegalRequirementsForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "LegalRequirementsUpdate.html",context)

def MinimumIonisationEntryUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["MinimumIonisationEntry"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(MinimumIonisationEntry, pk=pk)
    if request.method == 'POST':
        form = MinimumIonisationEntryForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.MinimumIonisationEntry(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/MinimumIonisationEntry/list/"
            return HttpResponseRedirect(link)
    else:
        form = MinimumIonisationEntryForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "MinimumIonisationEntryUpdate.html",context)

def MinimumIonisationTableUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["MinimumIonisationTable"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(MinimumIonisationTable, pk=pk)
    if request.method == 'POST':
        form = MinimumIonisationTableForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.MinimumIonisationTable(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/MinimumIonisationTable/list/"
            return HttpResponseRedirect(link)
    else:
        form = MinimumIonisationTableForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "MinimumIonisationTableUpdate.html",context)

def MixedFieldUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["MixedField"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(MixedField, pk=pk)
    if request.method == 'POST':
        form = MixedFieldForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.MixedField(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/MixedField/list/"
            return HttpResponseRedirect(link)
    else:
        form = MixedFieldForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "MixedFieldUpdate.html",context)

def NuclearCollisionLengthOccupancyUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["NuclearCollisionLengthOccupancy"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(NuclearCollisionLengthOccupancy, pk=pk)
    if request.method == 'POST':
        form = NuclearCollisionLengthOccupancyForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.NuclearCollisionLengthOccupancy(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/NuclearCollisionLengthOccupancy/list/"
            return HttpResponseRedirect(link)
    else:
        form = NuclearCollisionLengthOccupancyForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "NuclearCollisionLengthOccupancyUpdate.html",context)

def NuclearInteractionLengthOccupancyUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["NuclearInteractionLengthOccupancy"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(NuclearInteractionLengthOccupancy, pk=pk)
    if request.method == 'POST':
        form = NuclearInteractionLengthOccupancyForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.NuclearInteractionLengthOccupancy(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/NuclearInteractionLengthOccupancy/list/"
            return HttpResponseRedirect(link)
    else:
        form = NuclearInteractionLengthOccupancyForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "NuclearInteractionLengthOccupancyUpdate.html",context)

def ParticleUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Particle"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Particle, pk=pk)
    if request.method == 'POST':
        form = ParticleForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Particle(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/iedm_app/Particle/list/"
            return HttpResponseRedirect(link)
    else:
        form = ParticleForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ParticleUpdate.html",context)
