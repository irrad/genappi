from owlready2 import *
from django.db import models
from .models import *
from .forms import *
from django.shortcuts import get_object_or_404, render, redirect
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseNotFound
from django.template import loader
from django.core.urlresolvers import reverse
import datetime
from .forms import *
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.core.files.storage import FileSystemStorage
from django.utils.safestring import mark_safe
from django.conf import settings


def define_preferences(request):
    onto_path.append(settings.BASE_DIR)
    owao = get_ontology("owao.owl")
    owao.load()
    body_background = request.POST['body_background']
    font_color = request.POST['font_color']
    font = request.POST['font_size']
    user =  owao.User(request.user)
    owao.save()
    element_name = str(request.user)+"element" 
    preference = owao.UIPreference(element_name)
    owao.save()
    preference.fontsize.append(font)
    preference.color.append(font_color)
    preference.backgroundColor.append(body_background)
    user.hasUIPreference.append(preference)
    owao.save()
    data = dict()
    return redirect('HomeView')    