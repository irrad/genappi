from django.conf.urls import url, include
from . import Read_view, Create_view, Update_view, Delete_view, List_view, Home_view, preferences, registration

urlpatterns = [
    url(r'^home/$', Home_view.HomeView, name='HomeView'),

    url(r'^preferences/$', preferences.define_preferences, name='define_preferences'),

    url(r'^signup/$', registration.signup, name='signup'),    

    url(r'^Thing/create/$', Create_view.ThingCreate, name='ThingCreate'),

    url(r'^Object/create/$', Create_view.ObjectCreate, name='ObjectCreate'),

    url(r'^UnaryFunction/create/$', Create_view.UnaryFunctionCreate, name='UnaryFunctionCreate'),

    url(r'^User/create/$', Create_view.UserCreate, name='UserCreate'),

    url(r'^IrradiationFacilityRole/create/$', Create_view.IrradiationFacilityRoleCreate, name='IrradiationFacilityRoleCreate'),

    url(r'^IrradiationFacilityCoordinator/create/$', Create_view.IrradiationFacilityCoordinatorCreate, name='IrradiationFacilityCoordinatorCreate'),

    url(r'^IrradiationFacilityManager/create/$', Create_view.IrradiationFacilityManagerCreate, name='IrradiationFacilityManagerCreate'),

    url(r'^Operator/create/$', Create_view.OperatorCreate, name='OperatorCreate'),

    url(r'^ResponsiblePerson/create/$', Create_view.ResponsiblePersonCreate, name='ResponsiblePersonCreate'),

    url(r'^DUTIrradiation/create/$', Create_view.DUTIrradiationCreate, name='DUTIrradiationCreate'),

    url(r'^Sample/create/$', Create_view.SampleCreate, name='SampleCreate'),

    url(r'^DUT/create/$', Create_view.DUTCreate, name='DUTCreate'),

    url(r'^LayerTable/create/$', Create_view.LayerTableCreate, name='LayerTableCreate'),

    url(r'^IrradiationExperiment/create/$', Create_view.IrradiationExperimentCreate, name='IrradiationExperimentCreate'),

    url(r'^Layer/create/$', Create_view.LayerCreate, name='LayerCreate'),

    url(r'^Compound/create/$', Create_view.CompoundCreate, name='CompoundCreate'),

    url(r'^CompoundWeightFractionTable/create/$', Create_view.CompoundWeightFractionTableCreate, name='CompoundWeightFractionTableCreate'),

    url(r'^IrradiationFacility/create/$', Create_view.IrradiationFacilityCreate, name='IrradiationFacilityCreate'),

    url(r'^Physical/create/$', Create_view.PhysicalCreate, name='PhysicalCreate'),

    url(r'^IrradiationFacilityUser/create/$', Create_view.IrradiationFacilityUserCreate, name='IrradiationFacilityUserCreate'),

    url(r'^Predicate/create/$', Create_view.PredicateCreate, name='PredicateCreate'),

    url(r'^SentientAgent/create/$', Create_view.SentientAgentCreate, name='SentientAgentCreate'),

    url(r'^Agent/create/$', Create_view.AgentCreate, name='AgentCreate'),

    url(r'^CompoundWeightFractionEntry/create/$', Create_view.CompoundWeightFractionEntryCreate, name='CompoundWeightFractionEntryCreate'),

    url(r'^Element/create/$', Create_view.ElementCreate, name='ElementCreate'),

    url(r'^UnaryFunctionEntry/create/$', Create_view.UnaryFunctionEntryCreate, name='UnaryFunctionEntryCreate'),

    url(r'^CumulatedQuantity/create/$', Create_view.CumulatedQuantityCreate, name='CumulatedQuantityCreate'),

    url(r'^IrradiationExperimentObject/create/$', Create_view.IrradiationExperimentObjectCreate, name='IrradiationExperimentObjectCreate'),

    url(r'^InteractionLength/create/$', Create_view.InteractionLengthCreate, name='InteractionLengthCreate'),

    url(r'^InteractionLengthOccupancy/create/$', Create_view.InteractionLengthOccupancyCreate, name='InteractionLengthOccupancyCreate'),

    url(r'^DosimetricQuantity/create/$', Create_view.DosimetricQuantityCreate, name='DosimetricQuantityCreate'),

    url(r'^Dosimeter/create/$', Create_view.DosimeterCreate, name='DosimeterCreate'),

    url(r'^DomainOfExperiment/create/$', Create_view.DomainOfExperimentCreate, name='DomainOfExperimentCreate'),

    url(r'^ParticlePhysicsExperiment/create/$', Create_view.ParticlePhysicsExperimentCreate, name='ParticlePhysicsExperimentCreate'),

    url(r'^Requirements/create/$', Create_view.RequirementsCreate, name='RequirementsCreate'),

    url(r'^MonitoringSystem/create/$', Create_view.MonitoringSystemCreate, name='MonitoringSystemCreate'),

    url(r'^StorageArea/create/$', Create_view.StorageAreaCreate, name='StorageAreaCreate'),

    url(r'^FieldOfStudy/create/$', Create_view.FieldOfStudyCreate, name='FieldOfStudyCreate'),

    url(r'^IrradiationFacilityPostion/create/$', Create_view.IrradiationFacilityPostionCreate, name='IrradiationFacilityPostionCreate'),

    url(r'^Length/create/$', Create_view.LengthCreate, name='LengthCreate'),

    url(r'^Percentage/create/$', Create_view.PercentageCreate, name='PercentageCreate'),

    url(r'^LayerEntry/create/$', Create_view.LayerEntryCreate, name='LayerEntryCreate'),

    url(r'^Relation/create/$', Create_view.RelationCreate, name='RelationCreate'),

    url(r'^Abstract/create/$', Create_view.AbstractCreate, name='AbstractCreate'),

    url(r'^Region/create/$', Create_view.RegionCreate, name='RegionCreate'),

    url(r'^SubjectRole/create/$', Create_view.SubjectRoleCreate, name='SubjectRoleCreate'),

    url(r'^ActorRole/create/$', Create_view.ActorRoleCreate, name='ActorRoleCreate'),

    url(r'^AbsorbedDose/create/$', Create_view.AbsorbedDoseCreate, name='AbsorbedDoseCreate'),

    url(r'^AbsorbedDoseInMedium/create/$', Create_view.AbsorbedDoseInMediumCreate, name='AbsorbedDoseInMediumCreate'),

    url(r'^Quantity/create/$', Create_view.QuantityCreate, name='QuantityCreate'),

    url(r'^Activity/create/$', Create_view.ActivityCreate, name='ActivityCreate'),

    url(r'^DosimetricRate/create/$', Create_view.DosimetricRateCreate, name='DosimetricRateCreate'),

    url(r'^AtomicMass/create/$', Create_view.AtomicMassCreate, name='AtomicMassCreate'),

    url(r'^Mass/create/$', Create_view.MassCreate, name='MassCreate'),

    url(r'^Density/create/$', Create_view.DensityCreate, name='DensityCreate'),

    url(r'^Energy/create/$', Create_view.EnergyCreate, name='EnergyCreate'),

    url(r'^Height/create/$', Create_view.HeightCreate, name='HeightCreate'),

    url(r'^Number/create/$', Create_view.NumberCreate, name='NumberCreate'),

    url(r'^Ratio/create/$', Create_view.RatioCreate, name='RatioCreate'),

    url(r'^Temperature/create/$', Create_view.TemperatureCreate, name='TemperatureCreate'),

    url(r'^Thickness/create/$', Create_view.ThicknessCreate, name='ThicknessCreate'),

    url(r'^AbsorbedDoseInAir/create/$', Create_view.AbsorbedDoseInAirCreate, name='AbsorbedDoseInAirCreate'),

    url(r'^AdminInfoIrradiationExperiment/create/$', Create_view.AdminInfoIrradiationExperimentCreate, name='AdminInfoIrradiationExperimentCreate'),

    url(r'^AdminInfoExperiment/create/$', Create_view.AdminInfoExperimentCreate, name='AdminInfoExperimentCreate'),

    url(r'^ActionrelatedRole/create/$', Create_view.ActionrelatedRoleCreate, name='ActionrelatedRoleCreate'),

    url(r'^Role/create/$', Create_view.RoleCreate, name='RoleCreate'),

    url(r'^AdministrativeInformation/create/$', Create_view.AdministrativeInformationCreate, name='AdministrativeInformationCreate'),

    url(r'^Proposition/create/$', Create_view.PropositionCreate, name='PropositionCreate'),

    url(r'^AdminInfoObjectOfExperiment/create/$', Create_view.AdminInfoObjectOfExperimentCreate, name='AdminInfoObjectOfExperimentCreate'),

    url(r'^AdminInfoUser/create/$', Create_view.AdminInfoUserCreate, name='AdminInfoUserCreate'),

    url(r'^Classification/create/$', Create_view.ClassificationCreate, name='ClassificationCreate'),

    url(r'^ScientificTask/create/$', Create_view.ScientificTaskCreate, name='ScientificTaskCreate'),

    url(r'^ClassificationByDomain/create/$', Create_view.ClassificationByDomainCreate, name='ClassificationByDomainCreate'),

    url(r'^ClassificationOfExperiments/create/$', Create_view.ClassificationOfExperimentsCreate, name='ClassificationOfExperimentsCreate'),

    url(r'^CorpuscularObject/create/$', Create_view.CorpuscularObjectCreate, name='CorpuscularObjectCreate'),

    url(r'^SelfConnectedObject/create/$', Create_view.SelfConnectedObjectCreate, name='SelfConnectedObjectCreate'),

    url(r'^ExperimentalRequirements/create/$', Create_view.ExperimentalRequirementsCreate, name='ExperimentalRequirementsCreate'),

    url(r'^AdminInfoIrradiationExperimentUser/create/$', Create_view.AdminInfoIrradiationExperimentUserCreate, name='AdminInfoIrradiationExperimentUserCreate'),

    url(r'^AdminInfoObjectOfIrradiation/create/$', Create_view.AdminInfoObjectOfIrradiationCreate, name='AdminInfoObjectOfIrradiationCreate'),

    url(r'^AdminInfoObjectOfIrradiationExperiment/create/$', Create_view.AdminInfoObjectOfIrradiationExperimentCreate, name='AdminInfoObjectOfIrradiationExperimentCreate'),

    url(r'^AtomicNumber/create/$', Create_view.AtomicNumberCreate, name='AtomicNumberCreate'),

    url(r'^CollaborationRequirements/create/$', Create_view.CollaborationRequirementsCreate, name='CollaborationRequirementsCreate'),

    url(r'^ControlRoom/create/$', Create_view.ControlRoomCreate, name='ControlRoomCreate'),

    url(r'^ControlSystem/create/$', Create_view.ControlSystemCreate, name='ControlSystemCreate'),

    url(r'^System/create/$', Create_view.SystemCreate, name='SystemCreate'),

    url(r'^RadiationLength/create/$', Create_view.RadiationLengthCreate, name='RadiationLengthCreate'),

    url(r'^RadiationLengthOccupancy/create/$', Create_view.RadiationLengthOccupancyCreate, name='RadiationLengthOccupancyCreate'),

    url(r'^RadioprotectionRequirements/create/$', Create_view.RadioprotectionRequirementsCreate, name='RadioprotectionRequirementsCreate'),

    url(r'^SingularField/create/$', Create_view.SingularFieldCreate, name='SingularFieldCreate'),

    url(r'^RadiationField/create/$', Create_view.RadiationFieldCreate, name='RadiationFieldCreate'),

    url(r'^TechnicalRequirements/create/$', Create_view.TechnicalRequirementsCreate, name='TechnicalRequirementsCreate'),

    url(r'^IrradiationFacilityRequirements/create/$', Create_view.IrradiationFacilityRequirementsCreate, name='IrradiationFacilityRequirementsCreate'),

    url(r'^Fluence/create/$', Create_view.FluenceCreate, name='FluenceCreate'),

    url(r'^NuclearCollisionLength/create/$', Create_view.NuclearCollisionLengthCreate, name='NuclearCollisionLengthCreate'),

    url(r'^NuclearInteractionLength/create/$', Create_view.NuclearInteractionLengthCreate, name='NuclearInteractionLengthCreate'),

    url(r'^DataManagementSystem/create/$', Create_view.DataManagementSystemCreate, name='DataManagementSystemCreate'),

    url(r'^DoseRate/create/$', Create_view.DoseRateCreate, name='DoseRateCreate'),

    url(r'^ExternalPosition/create/$', Create_view.ExternalPositionCreate, name='ExternalPositionCreate'),

    url(r'^FluenceRate/create/$', Create_view.FluenceRateCreate, name='FluenceRateCreate'),

    url(r'^InteractionLengthEntry/create/$', Create_view.InteractionLengthEntryCreate, name='InteractionLengthEntryCreate'),

    url(r'^InteractionLengthTable/create/$', Create_view.InteractionLengthTableCreate, name='InteractionLengthTableCreate'),

    url(r'^IrradiationPosition/create/$', Create_view.IrradiationPositionCreate, name='IrradiationPositionCreate'),

    url(r'^IrradiationZone/create/$', Create_view.IrradiationZoneCreate, name='IrradiationZoneCreate'),

    url(r'^Laboratory/create/$', Create_view.LaboratoryCreate, name='LaboratoryCreate'),

    url(r'^LegalRequirements/create/$', Create_view.LegalRequirementsCreate, name='LegalRequirementsCreate'),

    url(r'^MinimumIonisationEntry/create/$', Create_view.MinimumIonisationEntryCreate, name='MinimumIonisationEntryCreate'),

    url(r'^MinimumIonisationTable/create/$', Create_view.MinimumIonisationTableCreate, name='MinimumIonisationTableCreate'),

    url(r'^MixedField/create/$', Create_view.MixedFieldCreate, name='MixedFieldCreate'),

    url(r'^NuclearCollisionLengthOccupancy/create/$', Create_view.NuclearCollisionLengthOccupancyCreate, name='NuclearCollisionLengthOccupancyCreate'),

    url(r'^NuclearInteractionLengthOccupancy/create/$', Create_view.NuclearInteractionLengthOccupancyCreate, name='NuclearInteractionLengthOccupancyCreate'),

    url(r'^Particle/create/$', Create_view.ParticleCreate, name='ParticleCreate'),


    url(r'^Thing/(?P<pk>\d+)/read/$', Read_view.ThingRead, name='ThingRead'),

    url(r'^Object/(?P<pk>\d+)/read/$', Read_view.ObjectRead, name='ObjectRead'),

    url(r'^UnaryFunction/(?P<pk>\d+)/read/$', Read_view.UnaryFunctionRead, name='UnaryFunctionRead'),

    url(r'^User/(?P<pk>\d+)/read/$', Read_view.UserRead, name='UserRead'),

    url(r'^IrradiationFacilityRole/(?P<pk>\d+)/read/$', Read_view.IrradiationFacilityRoleRead, name='IrradiationFacilityRoleRead'),

    url(r'^IrradiationFacilityCoordinator/(?P<pk>\d+)/read/$', Read_view.IrradiationFacilityCoordinatorRead, name='IrradiationFacilityCoordinatorRead'),

    url(r'^IrradiationFacilityManager/(?P<pk>\d+)/read/$', Read_view.IrradiationFacilityManagerRead, name='IrradiationFacilityManagerRead'),

    url(r'^Operator/(?P<pk>\d+)/read/$', Read_view.OperatorRead, name='OperatorRead'),

    url(r'^ResponsiblePerson/(?P<pk>\d+)/read/$', Read_view.ResponsiblePersonRead, name='ResponsiblePersonRead'),

    url(r'^DUTIrradiation/(?P<pk>\d+)/read/$', Read_view.DUTIrradiationRead, name='DUTIrradiationRead'),

    url(r'^Sample/(?P<pk>\d+)/read/$', Read_view.SampleRead, name='SampleRead'),

    url(r'^DUT/(?P<pk>\d+)/read/$', Read_view.DUTRead, name='DUTRead'),

    url(r'^LayerTable/(?P<pk>\d+)/read/$', Read_view.LayerTableRead, name='LayerTableRead'),

    url(r'^IrradiationExperiment/(?P<pk>\d+)/read/$', Read_view.IrradiationExperimentRead, name='IrradiationExperimentRead'),

    url(r'^Layer/(?P<pk>\d+)/read/$', Read_view.LayerRead, name='LayerRead'),

    url(r'^Compound/(?P<pk>\d+)/read/$', Read_view.CompoundRead, name='CompoundRead'),

    url(r'^CompoundWeightFractionTable/(?P<pk>\d+)/read/$', Read_view.CompoundWeightFractionTableRead, name='CompoundWeightFractionTableRead'),

    url(r'^IrradiationFacility/(?P<pk>\d+)/read/$', Read_view.IrradiationFacilityRead, name='IrradiationFacilityRead'),

    url(r'^Physical/(?P<pk>\d+)/read/$', Read_view.PhysicalRead, name='PhysicalRead'),

    url(r'^IrradiationFacilityUser/(?P<pk>\d+)/read/$', Read_view.IrradiationFacilityUserRead, name='IrradiationFacilityUserRead'),

    url(r'^Predicate/(?P<pk>\d+)/read/$', Read_view.PredicateRead, name='PredicateRead'),

    url(r'^SentientAgent/(?P<pk>\d+)/read/$', Read_view.SentientAgentRead, name='SentientAgentRead'),

    url(r'^Agent/(?P<pk>\d+)/read/$', Read_view.AgentRead, name='AgentRead'),

    url(r'^CompoundWeightFractionEntry/(?P<pk>\d+)/read/$', Read_view.CompoundWeightFractionEntryRead, name='CompoundWeightFractionEntryRead'),

    url(r'^Element/(?P<pk>\d+)/read/$', Read_view.ElementRead, name='ElementRead'),

    url(r'^UnaryFunctionEntry/(?P<pk>\d+)/read/$', Read_view.UnaryFunctionEntryRead, name='UnaryFunctionEntryRead'),

    url(r'^CumulatedQuantity/(?P<pk>\d+)/read/$', Read_view.CumulatedQuantityRead, name='CumulatedQuantityRead'),

    url(r'^IrradiationExperimentObject/(?P<pk>\d+)/read/$', Read_view.IrradiationExperimentObjectRead, name='IrradiationExperimentObjectRead'),

    url(r'^InteractionLength/(?P<pk>\d+)/read/$', Read_view.InteractionLengthRead, name='InteractionLengthRead'),

    url(r'^InteractionLengthOccupancy/(?P<pk>\d+)/read/$', Read_view.InteractionLengthOccupancyRead, name='InteractionLengthOccupancyRead'),

    url(r'^DosimetricQuantity/(?P<pk>\d+)/read/$', Read_view.DosimetricQuantityRead, name='DosimetricQuantityRead'),

    url(r'^Dosimeter/(?P<pk>\d+)/read/$', Read_view.DosimeterRead, name='DosimeterRead'),

    url(r'^DomainOfExperiment/(?P<pk>\d+)/read/$', Read_view.DomainOfExperimentRead, name='DomainOfExperimentRead'),

    url(r'^ParticlePhysicsExperiment/(?P<pk>\d+)/read/$', Read_view.ParticlePhysicsExperimentRead, name='ParticlePhysicsExperimentRead'),

    url(r'^Requirements/(?P<pk>\d+)/read/$', Read_view.RequirementsRead, name='RequirementsRead'),

    url(r'^MonitoringSystem/(?P<pk>\d+)/read/$', Read_view.MonitoringSystemRead, name='MonitoringSystemRead'),

    url(r'^StorageArea/(?P<pk>\d+)/read/$', Read_view.StorageAreaRead, name='StorageAreaRead'),

    url(r'^FieldOfStudy/(?P<pk>\d+)/read/$', Read_view.FieldOfStudyRead, name='FieldOfStudyRead'),

    url(r'^IrradiationFacilityPostion/(?P<pk>\d+)/read/$', Read_view.IrradiationFacilityPostionRead, name='IrradiationFacilityPostionRead'),

    url(r'^Length/(?P<pk>\d+)/read/$', Read_view.LengthRead, name='LengthRead'),

    url(r'^Percentage/(?P<pk>\d+)/read/$', Read_view.PercentageRead, name='PercentageRead'),

    url(r'^LayerEntry/(?P<pk>\d+)/read/$', Read_view.LayerEntryRead, name='LayerEntryRead'),

    url(r'^Relation/(?P<pk>\d+)/read/$', Read_view.RelationRead, name='RelationRead'),

    url(r'^Abstract/(?P<pk>\d+)/read/$', Read_view.AbstractRead, name='AbstractRead'),

    url(r'^Region/(?P<pk>\d+)/read/$', Read_view.RegionRead, name='RegionRead'),

    url(r'^SubjectRole/(?P<pk>\d+)/read/$', Read_view.SubjectRoleRead, name='SubjectRoleRead'),

    url(r'^ActorRole/(?P<pk>\d+)/read/$', Read_view.ActorRoleRead, name='ActorRoleRead'),

    url(r'^AbsorbedDose/(?P<pk>\d+)/read/$', Read_view.AbsorbedDoseRead, name='AbsorbedDoseRead'),

    url(r'^AbsorbedDoseInMedium/(?P<pk>\d+)/read/$', Read_view.AbsorbedDoseInMediumRead, name='AbsorbedDoseInMediumRead'),

    url(r'^Quantity/(?P<pk>\d+)/read/$', Read_view.QuantityRead, name='QuantityRead'),

    url(r'^Activity/(?P<pk>\d+)/read/$', Read_view.ActivityRead, name='ActivityRead'),

    url(r'^DosimetricRate/(?P<pk>\d+)/read/$', Read_view.DosimetricRateRead, name='DosimetricRateRead'),

    url(r'^AtomicMass/(?P<pk>\d+)/read/$', Read_view.AtomicMassRead, name='AtomicMassRead'),

    url(r'^Mass/(?P<pk>\d+)/read/$', Read_view.MassRead, name='MassRead'),

    url(r'^Density/(?P<pk>\d+)/read/$', Read_view.DensityRead, name='DensityRead'),

    url(r'^Energy/(?P<pk>\d+)/read/$', Read_view.EnergyRead, name='EnergyRead'),

    url(r'^Height/(?P<pk>\d+)/read/$', Read_view.HeightRead, name='HeightRead'),

    url(r'^Number/(?P<pk>\d+)/read/$', Read_view.NumberRead, name='NumberRead'),

    url(r'^Ratio/(?P<pk>\d+)/read/$', Read_view.RatioRead, name='RatioRead'),

    url(r'^Temperature/(?P<pk>\d+)/read/$', Read_view.TemperatureRead, name='TemperatureRead'),

    url(r'^Thickness/(?P<pk>\d+)/read/$', Read_view.ThicknessRead, name='ThicknessRead'),

    url(r'^AbsorbedDoseInAir/(?P<pk>\d+)/read/$', Read_view.AbsorbedDoseInAirRead, name='AbsorbedDoseInAirRead'),

    url(r'^AdminInfoIrradiationExperiment/(?P<pk>\d+)/read/$', Read_view.AdminInfoIrradiationExperimentRead, name='AdminInfoIrradiationExperimentRead'),

    url(r'^AdminInfoExperiment/(?P<pk>\d+)/read/$', Read_view.AdminInfoExperimentRead, name='AdminInfoExperimentRead'),

    url(r'^ActionrelatedRole/(?P<pk>\d+)/read/$', Read_view.ActionrelatedRoleRead, name='ActionrelatedRoleRead'),

    url(r'^Role/(?P<pk>\d+)/read/$', Read_view.RoleRead, name='RoleRead'),

    url(r'^AdministrativeInformation/(?P<pk>\d+)/read/$', Read_view.AdministrativeInformationRead, name='AdministrativeInformationRead'),

    url(r'^Proposition/(?P<pk>\d+)/read/$', Read_view.PropositionRead, name='PropositionRead'),

    url(r'^AdminInfoObjectOfExperiment/(?P<pk>\d+)/read/$', Read_view.AdminInfoObjectOfExperimentRead, name='AdminInfoObjectOfExperimentRead'),

    url(r'^AdminInfoUser/(?P<pk>\d+)/read/$', Read_view.AdminInfoUserRead, name='AdminInfoUserRead'),

    url(r'^Classification/(?P<pk>\d+)/read/$', Read_view.ClassificationRead, name='ClassificationRead'),

    url(r'^ScientificTask/(?P<pk>\d+)/read/$', Read_view.ScientificTaskRead, name='ScientificTaskRead'),

    url(r'^ClassificationByDomain/(?P<pk>\d+)/read/$', Read_view.ClassificationByDomainRead, name='ClassificationByDomainRead'),

    url(r'^ClassificationOfExperiments/(?P<pk>\d+)/read/$', Read_view.ClassificationOfExperimentsRead, name='ClassificationOfExperimentsRead'),

    url(r'^CorpuscularObject/(?P<pk>\d+)/read/$', Read_view.CorpuscularObjectRead, name='CorpuscularObjectRead'),

    url(r'^SelfConnectedObject/(?P<pk>\d+)/read/$', Read_view.SelfConnectedObjectRead, name='SelfConnectedObjectRead'),

    url(r'^ExperimentalRequirements/(?P<pk>\d+)/read/$', Read_view.ExperimentalRequirementsRead, name='ExperimentalRequirementsRead'),

    url(r'^AdminInfoIrradiationExperimentUser/(?P<pk>\d+)/read/$', Read_view.AdminInfoIrradiationExperimentUserRead, name='AdminInfoIrradiationExperimentUserRead'),

    url(r'^AdminInfoObjectOfIrradiation/(?P<pk>\d+)/read/$', Read_view.AdminInfoObjectOfIrradiationRead, name='AdminInfoObjectOfIrradiationRead'),

    url(r'^AdminInfoObjectOfIrradiationExperiment/(?P<pk>\d+)/read/$', Read_view.AdminInfoObjectOfIrradiationExperimentRead, name='AdminInfoObjectOfIrradiationExperimentRead'),

    url(r'^AtomicNumber/(?P<pk>\d+)/read/$', Read_view.AtomicNumberRead, name='AtomicNumberRead'),

    url(r'^CollaborationRequirements/(?P<pk>\d+)/read/$', Read_view.CollaborationRequirementsRead, name='CollaborationRequirementsRead'),

    url(r'^ControlRoom/(?P<pk>\d+)/read/$', Read_view.ControlRoomRead, name='ControlRoomRead'),

    url(r'^ControlSystem/(?P<pk>\d+)/read/$', Read_view.ControlSystemRead, name='ControlSystemRead'),

    url(r'^System/(?P<pk>\d+)/read/$', Read_view.SystemRead, name='SystemRead'),

    url(r'^RadiationLength/(?P<pk>\d+)/read/$', Read_view.RadiationLengthRead, name='RadiationLengthRead'),

    url(r'^RadiationLengthOccupancy/(?P<pk>\d+)/read/$', Read_view.RadiationLengthOccupancyRead, name='RadiationLengthOccupancyRead'),

    url(r'^RadioprotectionRequirements/(?P<pk>\d+)/read/$', Read_view.RadioprotectionRequirementsRead, name='RadioprotectionRequirementsRead'),

    url(r'^SingularField/(?P<pk>\d+)/read/$', Read_view.SingularFieldRead, name='SingularFieldRead'),

    url(r'^RadiationField/(?P<pk>\d+)/read/$', Read_view.RadiationFieldRead, name='RadiationFieldRead'),

    url(r'^TechnicalRequirements/(?P<pk>\d+)/read/$', Read_view.TechnicalRequirementsRead, name='TechnicalRequirementsRead'),

    url(r'^IrradiationFacilityRequirements/(?P<pk>\d+)/read/$', Read_view.IrradiationFacilityRequirementsRead, name='IrradiationFacilityRequirementsRead'),

    url(r'^Fluence/(?P<pk>\d+)/read/$', Read_view.FluenceRead, name='FluenceRead'),

    url(r'^NuclearCollisionLength/(?P<pk>\d+)/read/$', Read_view.NuclearCollisionLengthRead, name='NuclearCollisionLengthRead'),

    url(r'^NuclearInteractionLength/(?P<pk>\d+)/read/$', Read_view.NuclearInteractionLengthRead, name='NuclearInteractionLengthRead'),

    url(r'^DataManagementSystem/(?P<pk>\d+)/read/$', Read_view.DataManagementSystemRead, name='DataManagementSystemRead'),

    url(r'^DoseRate/(?P<pk>\d+)/read/$', Read_view.DoseRateRead, name='DoseRateRead'),

    url(r'^ExternalPosition/(?P<pk>\d+)/read/$', Read_view.ExternalPositionRead, name='ExternalPositionRead'),

    url(r'^FluenceRate/(?P<pk>\d+)/read/$', Read_view.FluenceRateRead, name='FluenceRateRead'),

    url(r'^InteractionLengthEntry/(?P<pk>\d+)/read/$', Read_view.InteractionLengthEntryRead, name='InteractionLengthEntryRead'),

    url(r'^InteractionLengthTable/(?P<pk>\d+)/read/$', Read_view.InteractionLengthTableRead, name='InteractionLengthTableRead'),

    url(r'^IrradiationPosition/(?P<pk>\d+)/read/$', Read_view.IrradiationPositionRead, name='IrradiationPositionRead'),

    url(r'^IrradiationZone/(?P<pk>\d+)/read/$', Read_view.IrradiationZoneRead, name='IrradiationZoneRead'),

    url(r'^Laboratory/(?P<pk>\d+)/read/$', Read_view.LaboratoryRead, name='LaboratoryRead'),

    url(r'^LegalRequirements/(?P<pk>\d+)/read/$', Read_view.LegalRequirementsRead, name='LegalRequirementsRead'),

    url(r'^MinimumIonisationEntry/(?P<pk>\d+)/read/$', Read_view.MinimumIonisationEntryRead, name='MinimumIonisationEntryRead'),

    url(r'^MinimumIonisationTable/(?P<pk>\d+)/read/$', Read_view.MinimumIonisationTableRead, name='MinimumIonisationTableRead'),

    url(r'^MixedField/(?P<pk>\d+)/read/$', Read_view.MixedFieldRead, name='MixedFieldRead'),

    url(r'^NuclearCollisionLengthOccupancy/(?P<pk>\d+)/read/$', Read_view.NuclearCollisionLengthOccupancyRead, name='NuclearCollisionLengthOccupancyRead'),

    url(r'^NuclearInteractionLengthOccupancy/(?P<pk>\d+)/read/$', Read_view.NuclearInteractionLengthOccupancyRead, name='NuclearInteractionLengthOccupancyRead'),

    url(r'^Particle/(?P<pk>\d+)/read/$', Read_view.ParticleRead, name='ParticleRead'),


    url(r'^Thing/list/$', List_view.ThingList, name='ThingList'),

    url(r'^Object/list/$', List_view.ObjectList, name='ObjectList'),

    url(r'^UnaryFunction/list/$', List_view.UnaryFunctionList, name='UnaryFunctionList'),

    url(r'^User/list/$', List_view.UserList, name='UserList'),

    url(r'^IrradiationFacilityRole/list/$', List_view.IrradiationFacilityRoleList, name='IrradiationFacilityRoleList'),

    url(r'^IrradiationFacilityCoordinator/list/$', List_view.IrradiationFacilityCoordinatorList, name='IrradiationFacilityCoordinatorList'),

    url(r'^IrradiationFacilityManager/list/$', List_view.IrradiationFacilityManagerList, name='IrradiationFacilityManagerList'),

    url(r'^Operator/list/$', List_view.OperatorList, name='OperatorList'),

    url(r'^ResponsiblePerson/list/$', List_view.ResponsiblePersonList, name='ResponsiblePersonList'),

    url(r'^DUTIrradiation/list/$', List_view.DUTIrradiationList, name='DUTIrradiationList'),

    url(r'^Sample/list/$', List_view.SampleList, name='SampleList'),

    url(r'^DUT/list/$', List_view.DUTList, name='DUTList'),

    url(r'^LayerTable/list/$', List_view.LayerTableList, name='LayerTableList'),

    url(r'^IrradiationExperiment/list/$', List_view.IrradiationExperimentList, name='IrradiationExperimentList'),

    url(r'^Layer/list/$', List_view.LayerList, name='LayerList'),

    url(r'^Compound/list/$', List_view.CompoundList, name='CompoundList'),

    url(r'^CompoundWeightFractionTable/list/$', List_view.CompoundWeightFractionTableList, name='CompoundWeightFractionTableList'),

    url(r'^IrradiationFacility/list/$', List_view.IrradiationFacilityList, name='IrradiationFacilityList'),

    url(r'^Physical/list/$', List_view.PhysicalList, name='PhysicalList'),

    url(r'^IrradiationFacilityUser/list/$', List_view.IrradiationFacilityUserList, name='IrradiationFacilityUserList'),

    url(r'^Predicate/list/$', List_view.PredicateList, name='PredicateList'),

    url(r'^SentientAgent/list/$', List_view.SentientAgentList, name='SentientAgentList'),

    url(r'^Agent/list/$', List_view.AgentList, name='AgentList'),

    url(r'^CompoundWeightFractionEntry/list/$', List_view.CompoundWeightFractionEntryList, name='CompoundWeightFractionEntryList'),

    url(r'^Element/list/$', List_view.ElementList, name='ElementList'),

    url(r'^UnaryFunctionEntry/list/$', List_view.UnaryFunctionEntryList, name='UnaryFunctionEntryList'),

    url(r'^CumulatedQuantity/list/$', List_view.CumulatedQuantityList, name='CumulatedQuantityList'),

    url(r'^IrradiationExperimentObject/list/$', List_view.IrradiationExperimentObjectList, name='IrradiationExperimentObjectList'),

    url(r'^InteractionLength/list/$', List_view.InteractionLengthList, name='InteractionLengthList'),

    url(r'^InteractionLengthOccupancy/list/$', List_view.InteractionLengthOccupancyList, name='InteractionLengthOccupancyList'),

    url(r'^DosimetricQuantity/list/$', List_view.DosimetricQuantityList, name='DosimetricQuantityList'),

    url(r'^Dosimeter/list/$', List_view.DosimeterList, name='DosimeterList'),

    url(r'^DomainOfExperiment/list/$', List_view.DomainOfExperimentList, name='DomainOfExperimentList'),

    url(r'^ParticlePhysicsExperiment/list/$', List_view.ParticlePhysicsExperimentList, name='ParticlePhysicsExperimentList'),

    url(r'^Requirements/list/$', List_view.RequirementsList, name='RequirementsList'),

    url(r'^MonitoringSystem/list/$', List_view.MonitoringSystemList, name='MonitoringSystemList'),

    url(r'^StorageArea/list/$', List_view.StorageAreaList, name='StorageAreaList'),

    url(r'^FieldOfStudy/list/$', List_view.FieldOfStudyList, name='FieldOfStudyList'),

    url(r'^IrradiationFacilityPostion/list/$', List_view.IrradiationFacilityPostionList, name='IrradiationFacilityPostionList'),

    url(r'^Length/list/$', List_view.LengthList, name='LengthList'),

    url(r'^Percentage/list/$', List_view.PercentageList, name='PercentageList'),

    url(r'^LayerEntry/list/$', List_view.LayerEntryList, name='LayerEntryList'),

    url(r'^Relation/list/$', List_view.RelationList, name='RelationList'),

    url(r'^Abstract/list/$', List_view.AbstractList, name='AbstractList'),

    url(r'^Region/list/$', List_view.RegionList, name='RegionList'),

    url(r'^SubjectRole/list/$', List_view.SubjectRoleList, name='SubjectRoleList'),

    url(r'^ActorRole/list/$', List_view.ActorRoleList, name='ActorRoleList'),

    url(r'^AbsorbedDose/list/$', List_view.AbsorbedDoseList, name='AbsorbedDoseList'),

    url(r'^AbsorbedDoseInMedium/list/$', List_view.AbsorbedDoseInMediumList, name='AbsorbedDoseInMediumList'),

    url(r'^Quantity/list/$', List_view.QuantityList, name='QuantityList'),

    url(r'^Activity/list/$', List_view.ActivityList, name='ActivityList'),

    url(r'^DosimetricRate/list/$', List_view.DosimetricRateList, name='DosimetricRateList'),

    url(r'^AtomicMass/list/$', List_view.AtomicMassList, name='AtomicMassList'),

    url(r'^Mass/list/$', List_view.MassList, name='MassList'),

    url(r'^Density/list/$', List_view.DensityList, name='DensityList'),

    url(r'^Energy/list/$', List_view.EnergyList, name='EnergyList'),

    url(r'^Height/list/$', List_view.HeightList, name='HeightList'),

    url(r'^Number/list/$', List_view.NumberList, name='NumberList'),

    url(r'^Ratio/list/$', List_view.RatioList, name='RatioList'),

    url(r'^Temperature/list/$', List_view.TemperatureList, name='TemperatureList'),

    url(r'^Thickness/list/$', List_view.ThicknessList, name='ThicknessList'),

    url(r'^AbsorbedDoseInAir/list/$', List_view.AbsorbedDoseInAirList, name='AbsorbedDoseInAirList'),

    url(r'^AdminInfoIrradiationExperiment/list/$', List_view.AdminInfoIrradiationExperimentList, name='AdminInfoIrradiationExperimentList'),

    url(r'^AdminInfoExperiment/list/$', List_view.AdminInfoExperimentList, name='AdminInfoExperimentList'),

    url(r'^ActionrelatedRole/list/$', List_view.ActionrelatedRoleList, name='ActionrelatedRoleList'),

    url(r'^Role/list/$', List_view.RoleList, name='RoleList'),

    url(r'^AdministrativeInformation/list/$', List_view.AdministrativeInformationList, name='AdministrativeInformationList'),

    url(r'^Proposition/list/$', List_view.PropositionList, name='PropositionList'),

    url(r'^AdminInfoObjectOfExperiment/list/$', List_view.AdminInfoObjectOfExperimentList, name='AdminInfoObjectOfExperimentList'),

    url(r'^AdminInfoUser/list/$', List_view.AdminInfoUserList, name='AdminInfoUserList'),

    url(r'^Classification/list/$', List_view.ClassificationList, name='ClassificationList'),

    url(r'^ScientificTask/list/$', List_view.ScientificTaskList, name='ScientificTaskList'),

    url(r'^ClassificationByDomain/list/$', List_view.ClassificationByDomainList, name='ClassificationByDomainList'),

    url(r'^ClassificationOfExperiments/list/$', List_view.ClassificationOfExperimentsList, name='ClassificationOfExperimentsList'),

    url(r'^CorpuscularObject/list/$', List_view.CorpuscularObjectList, name='CorpuscularObjectList'),

    url(r'^SelfConnectedObject/list/$', List_view.SelfConnectedObjectList, name='SelfConnectedObjectList'),

    url(r'^ExperimentalRequirements/list/$', List_view.ExperimentalRequirementsList, name='ExperimentalRequirementsList'),

    url(r'^AdminInfoIrradiationExperimentUser/list/$', List_view.AdminInfoIrradiationExperimentUserList, name='AdminInfoIrradiationExperimentUserList'),

    url(r'^AdminInfoObjectOfIrradiation/list/$', List_view.AdminInfoObjectOfIrradiationList, name='AdminInfoObjectOfIrradiationList'),

    url(r'^AdminInfoObjectOfIrradiationExperiment/list/$', List_view.AdminInfoObjectOfIrradiationExperimentList, name='AdminInfoObjectOfIrradiationExperimentList'),

    url(r'^AtomicNumber/list/$', List_view.AtomicNumberList, name='AtomicNumberList'),

    url(r'^CollaborationRequirements/list/$', List_view.CollaborationRequirementsList, name='CollaborationRequirementsList'),

    url(r'^ControlRoom/list/$', List_view.ControlRoomList, name='ControlRoomList'),

    url(r'^ControlSystem/list/$', List_view.ControlSystemList, name='ControlSystemList'),

    url(r'^System/list/$', List_view.SystemList, name='SystemList'),

    url(r'^RadiationLength/list/$', List_view.RadiationLengthList, name='RadiationLengthList'),

    url(r'^RadiationLengthOccupancy/list/$', List_view.RadiationLengthOccupancyList, name='RadiationLengthOccupancyList'),

    url(r'^RadioprotectionRequirements/list/$', List_view.RadioprotectionRequirementsList, name='RadioprotectionRequirementsList'),

    url(r'^SingularField/list/$', List_view.SingularFieldList, name='SingularFieldList'),

    url(r'^RadiationField/list/$', List_view.RadiationFieldList, name='RadiationFieldList'),

    url(r'^TechnicalRequirements/list/$', List_view.TechnicalRequirementsList, name='TechnicalRequirementsList'),

    url(r'^IrradiationFacilityRequirements/list/$', List_view.IrradiationFacilityRequirementsList, name='IrradiationFacilityRequirementsList'),

    url(r'^Fluence/list/$', List_view.FluenceList, name='FluenceList'),

    url(r'^NuclearCollisionLength/list/$', List_view.NuclearCollisionLengthList, name='NuclearCollisionLengthList'),

    url(r'^NuclearInteractionLength/list/$', List_view.NuclearInteractionLengthList, name='NuclearInteractionLengthList'),

    url(r'^DataManagementSystem/list/$', List_view.DataManagementSystemList, name='DataManagementSystemList'),

    url(r'^DoseRate/list/$', List_view.DoseRateList, name='DoseRateList'),

    url(r'^ExternalPosition/list/$', List_view.ExternalPositionList, name='ExternalPositionList'),

    url(r'^FluenceRate/list/$', List_view.FluenceRateList, name='FluenceRateList'),

    url(r'^InteractionLengthEntry/list/$', List_view.InteractionLengthEntryList, name='InteractionLengthEntryList'),

    url(r'^InteractionLengthTable/list/$', List_view.InteractionLengthTableList, name='InteractionLengthTableList'),

    url(r'^IrradiationPosition/list/$', List_view.IrradiationPositionList, name='IrradiationPositionList'),

    url(r'^IrradiationZone/list/$', List_view.IrradiationZoneList, name='IrradiationZoneList'),

    url(r'^Laboratory/list/$', List_view.LaboratoryList, name='LaboratoryList'),

    url(r'^LegalRequirements/list/$', List_view.LegalRequirementsList, name='LegalRequirementsList'),

    url(r'^MinimumIonisationEntry/list/$', List_view.MinimumIonisationEntryList, name='MinimumIonisationEntryList'),

    url(r'^MinimumIonisationTable/list/$', List_view.MinimumIonisationTableList, name='MinimumIonisationTableList'),

    url(r'^MixedField/list/$', List_view.MixedFieldList, name='MixedFieldList'),

    url(r'^NuclearCollisionLengthOccupancy/list/$', List_view.NuclearCollisionLengthOccupancyList, name='NuclearCollisionLengthOccupancyList'),

    url(r'^NuclearInteractionLengthOccupancy/list/$', List_view.NuclearInteractionLengthOccupancyList, name='NuclearInteractionLengthOccupancyList'),

    url(r'^Particle/list/$', List_view.ParticleList, name='ParticleList'),


    url(r'^Thing/(?P<pk>\d+)/update/$', Update_view.ThingUpdate, name='ThingUpdate'),

    url(r'^Object/(?P<pk>\d+)/update/$', Update_view.ObjectUpdate, name='ObjectUpdate'),

    url(r'^UnaryFunction/(?P<pk>\d+)/update/$', Update_view.UnaryFunctionUpdate, name='UnaryFunctionUpdate'),

    url(r'^User/(?P<pk>\d+)/update/$', Update_view.UserUpdate, name='UserUpdate'),

    url(r'^IrradiationFacilityRole/(?P<pk>\d+)/update/$', Update_view.IrradiationFacilityRoleUpdate, name='IrradiationFacilityRoleUpdate'),

    url(r'^IrradiationFacilityCoordinator/(?P<pk>\d+)/update/$', Update_view.IrradiationFacilityCoordinatorUpdate, name='IrradiationFacilityCoordinatorUpdate'),

    url(r'^IrradiationFacilityManager/(?P<pk>\d+)/update/$', Update_view.IrradiationFacilityManagerUpdate, name='IrradiationFacilityManagerUpdate'),

    url(r'^Operator/(?P<pk>\d+)/update/$', Update_view.OperatorUpdate, name='OperatorUpdate'),

    url(r'^ResponsiblePerson/(?P<pk>\d+)/update/$', Update_view.ResponsiblePersonUpdate, name='ResponsiblePersonUpdate'),

    url(r'^DUTIrradiation/(?P<pk>\d+)/update/$', Update_view.DUTIrradiationUpdate, name='DUTIrradiationUpdate'),

    url(r'^Sample/(?P<pk>\d+)/update/$', Update_view.SampleUpdate, name='SampleUpdate'),

    url(r'^DUT/(?P<pk>\d+)/update/$', Update_view.DUTUpdate, name='DUTUpdate'),

    url(r'^LayerTable/(?P<pk>\d+)/update/$', Update_view.LayerTableUpdate, name='LayerTableUpdate'),

    url(r'^IrradiationExperiment/(?P<pk>\d+)/update/$', Update_view.IrradiationExperimentUpdate, name='IrradiationExperimentUpdate'),

    url(r'^Layer/(?P<pk>\d+)/update/$', Update_view.LayerUpdate, name='LayerUpdate'),

    url(r'^Compound/(?P<pk>\d+)/update/$', Update_view.CompoundUpdate, name='CompoundUpdate'),

    url(r'^CompoundWeightFractionTable/(?P<pk>\d+)/update/$', Update_view.CompoundWeightFractionTableUpdate, name='CompoundWeightFractionTableUpdate'),

    url(r'^IrradiationFacility/(?P<pk>\d+)/update/$', Update_view.IrradiationFacilityUpdate, name='IrradiationFacilityUpdate'),

    url(r'^Physical/(?P<pk>\d+)/update/$', Update_view.PhysicalUpdate, name='PhysicalUpdate'),

    url(r'^IrradiationFacilityUser/(?P<pk>\d+)/update/$', Update_view.IrradiationFacilityUserUpdate, name='IrradiationFacilityUserUpdate'),

    url(r'^Predicate/(?P<pk>\d+)/update/$', Update_view.PredicateUpdate, name='PredicateUpdate'),

    url(r'^SentientAgent/(?P<pk>\d+)/update/$', Update_view.SentientAgentUpdate, name='SentientAgentUpdate'),

    url(r'^Agent/(?P<pk>\d+)/update/$', Update_view.AgentUpdate, name='AgentUpdate'),

    url(r'^CompoundWeightFractionEntry/(?P<pk>\d+)/update/$', Update_view.CompoundWeightFractionEntryUpdate, name='CompoundWeightFractionEntryUpdate'),

    url(r'^Element/(?P<pk>\d+)/update/$', Update_view.ElementUpdate, name='ElementUpdate'),

    url(r'^UnaryFunctionEntry/(?P<pk>\d+)/update/$', Update_view.UnaryFunctionEntryUpdate, name='UnaryFunctionEntryUpdate'),

    url(r'^CumulatedQuantity/(?P<pk>\d+)/update/$', Update_view.CumulatedQuantityUpdate, name='CumulatedQuantityUpdate'),

    url(r'^IrradiationExperimentObject/(?P<pk>\d+)/update/$', Update_view.IrradiationExperimentObjectUpdate, name='IrradiationExperimentObjectUpdate'),

    url(r'^InteractionLength/(?P<pk>\d+)/update/$', Update_view.InteractionLengthUpdate, name='InteractionLengthUpdate'),

    url(r'^InteractionLengthOccupancy/(?P<pk>\d+)/update/$', Update_view.InteractionLengthOccupancyUpdate, name='InteractionLengthOccupancyUpdate'),

    url(r'^DosimetricQuantity/(?P<pk>\d+)/update/$', Update_view.DosimetricQuantityUpdate, name='DosimetricQuantityUpdate'),

    url(r'^Dosimeter/(?P<pk>\d+)/update/$', Update_view.DosimeterUpdate, name='DosimeterUpdate'),

    url(r'^DomainOfExperiment/(?P<pk>\d+)/update/$', Update_view.DomainOfExperimentUpdate, name='DomainOfExperimentUpdate'),

    url(r'^ParticlePhysicsExperiment/(?P<pk>\d+)/update/$', Update_view.ParticlePhysicsExperimentUpdate, name='ParticlePhysicsExperimentUpdate'),

    url(r'^Requirements/(?P<pk>\d+)/update/$', Update_view.RequirementsUpdate, name='RequirementsUpdate'),

    url(r'^MonitoringSystem/(?P<pk>\d+)/update/$', Update_view.MonitoringSystemUpdate, name='MonitoringSystemUpdate'),

    url(r'^StorageArea/(?P<pk>\d+)/update/$', Update_view.StorageAreaUpdate, name='StorageAreaUpdate'),

    url(r'^FieldOfStudy/(?P<pk>\d+)/update/$', Update_view.FieldOfStudyUpdate, name='FieldOfStudyUpdate'),

    url(r'^IrradiationFacilityPostion/(?P<pk>\d+)/update/$', Update_view.IrradiationFacilityPostionUpdate, name='IrradiationFacilityPostionUpdate'),

    url(r'^Length/(?P<pk>\d+)/update/$', Update_view.LengthUpdate, name='LengthUpdate'),

    url(r'^Percentage/(?P<pk>\d+)/update/$', Update_view.PercentageUpdate, name='PercentageUpdate'),

    url(r'^LayerEntry/(?P<pk>\d+)/update/$', Update_view.LayerEntryUpdate, name='LayerEntryUpdate'),

    url(r'^Relation/(?P<pk>\d+)/update/$', Update_view.RelationUpdate, name='RelationUpdate'),

    url(r'^Abstract/(?P<pk>\d+)/update/$', Update_view.AbstractUpdate, name='AbstractUpdate'),

    url(r'^Region/(?P<pk>\d+)/update/$', Update_view.RegionUpdate, name='RegionUpdate'),

    url(r'^SubjectRole/(?P<pk>\d+)/update/$', Update_view.SubjectRoleUpdate, name='SubjectRoleUpdate'),

    url(r'^ActorRole/(?P<pk>\d+)/update/$', Update_view.ActorRoleUpdate, name='ActorRoleUpdate'),

    url(r'^AbsorbedDose/(?P<pk>\d+)/update/$', Update_view.AbsorbedDoseUpdate, name='AbsorbedDoseUpdate'),

    url(r'^AbsorbedDoseInMedium/(?P<pk>\d+)/update/$', Update_view.AbsorbedDoseInMediumUpdate, name='AbsorbedDoseInMediumUpdate'),

    url(r'^Quantity/(?P<pk>\d+)/update/$', Update_view.QuantityUpdate, name='QuantityUpdate'),

    url(r'^Activity/(?P<pk>\d+)/update/$', Update_view.ActivityUpdate, name='ActivityUpdate'),

    url(r'^DosimetricRate/(?P<pk>\d+)/update/$', Update_view.DosimetricRateUpdate, name='DosimetricRateUpdate'),

    url(r'^AtomicMass/(?P<pk>\d+)/update/$', Update_view.AtomicMassUpdate, name='AtomicMassUpdate'),

    url(r'^Mass/(?P<pk>\d+)/update/$', Update_view.MassUpdate, name='MassUpdate'),

    url(r'^Density/(?P<pk>\d+)/update/$', Update_view.DensityUpdate, name='DensityUpdate'),

    url(r'^Energy/(?P<pk>\d+)/update/$', Update_view.EnergyUpdate, name='EnergyUpdate'),

    url(r'^Height/(?P<pk>\d+)/update/$', Update_view.HeightUpdate, name='HeightUpdate'),

    url(r'^Number/(?P<pk>\d+)/update/$', Update_view.NumberUpdate, name='NumberUpdate'),

    url(r'^Ratio/(?P<pk>\d+)/update/$', Update_view.RatioUpdate, name='RatioUpdate'),

    url(r'^Temperature/(?P<pk>\d+)/update/$', Update_view.TemperatureUpdate, name='TemperatureUpdate'),

    url(r'^Thickness/(?P<pk>\d+)/update/$', Update_view.ThicknessUpdate, name='ThicknessUpdate'),

    url(r'^AbsorbedDoseInAir/(?P<pk>\d+)/update/$', Update_view.AbsorbedDoseInAirUpdate, name='AbsorbedDoseInAirUpdate'),

    url(r'^AdminInfoIrradiationExperiment/(?P<pk>\d+)/update/$', Update_view.AdminInfoIrradiationExperimentUpdate, name='AdminInfoIrradiationExperimentUpdate'),

    url(r'^AdminInfoExperiment/(?P<pk>\d+)/update/$', Update_view.AdminInfoExperimentUpdate, name='AdminInfoExperimentUpdate'),

    url(r'^ActionrelatedRole/(?P<pk>\d+)/update/$', Update_view.ActionrelatedRoleUpdate, name='ActionrelatedRoleUpdate'),

    url(r'^Role/(?P<pk>\d+)/update/$', Update_view.RoleUpdate, name='RoleUpdate'),

    url(r'^AdministrativeInformation/(?P<pk>\d+)/update/$', Update_view.AdministrativeInformationUpdate, name='AdministrativeInformationUpdate'),

    url(r'^Proposition/(?P<pk>\d+)/update/$', Update_view.PropositionUpdate, name='PropositionUpdate'),

    url(r'^AdminInfoObjectOfExperiment/(?P<pk>\d+)/update/$', Update_view.AdminInfoObjectOfExperimentUpdate, name='AdminInfoObjectOfExperimentUpdate'),

    url(r'^AdminInfoUser/(?P<pk>\d+)/update/$', Update_view.AdminInfoUserUpdate, name='AdminInfoUserUpdate'),

    url(r'^Classification/(?P<pk>\d+)/update/$', Update_view.ClassificationUpdate, name='ClassificationUpdate'),

    url(r'^ScientificTask/(?P<pk>\d+)/update/$', Update_view.ScientificTaskUpdate, name='ScientificTaskUpdate'),

    url(r'^ClassificationByDomain/(?P<pk>\d+)/update/$', Update_view.ClassificationByDomainUpdate, name='ClassificationByDomainUpdate'),

    url(r'^ClassificationOfExperiments/(?P<pk>\d+)/update/$', Update_view.ClassificationOfExperimentsUpdate, name='ClassificationOfExperimentsUpdate'),

    url(r'^CorpuscularObject/(?P<pk>\d+)/update/$', Update_view.CorpuscularObjectUpdate, name='CorpuscularObjectUpdate'),

    url(r'^SelfConnectedObject/(?P<pk>\d+)/update/$', Update_view.SelfConnectedObjectUpdate, name='SelfConnectedObjectUpdate'),

    url(r'^ExperimentalRequirements/(?P<pk>\d+)/update/$', Update_view.ExperimentalRequirementsUpdate, name='ExperimentalRequirementsUpdate'),

    url(r'^AdminInfoIrradiationExperimentUser/(?P<pk>\d+)/update/$', Update_view.AdminInfoIrradiationExperimentUserUpdate, name='AdminInfoIrradiationExperimentUserUpdate'),

    url(r'^AdminInfoObjectOfIrradiation/(?P<pk>\d+)/update/$', Update_view.AdminInfoObjectOfIrradiationUpdate, name='AdminInfoObjectOfIrradiationUpdate'),

    url(r'^AdminInfoObjectOfIrradiationExperiment/(?P<pk>\d+)/update/$', Update_view.AdminInfoObjectOfIrradiationExperimentUpdate, name='AdminInfoObjectOfIrradiationExperimentUpdate'),

    url(r'^AtomicNumber/(?P<pk>\d+)/update/$', Update_view.AtomicNumberUpdate, name='AtomicNumberUpdate'),

    url(r'^CollaborationRequirements/(?P<pk>\d+)/update/$', Update_view.CollaborationRequirementsUpdate, name='CollaborationRequirementsUpdate'),

    url(r'^ControlRoom/(?P<pk>\d+)/update/$', Update_view.ControlRoomUpdate, name='ControlRoomUpdate'),

    url(r'^ControlSystem/(?P<pk>\d+)/update/$', Update_view.ControlSystemUpdate, name='ControlSystemUpdate'),

    url(r'^System/(?P<pk>\d+)/update/$', Update_view.SystemUpdate, name='SystemUpdate'),

    url(r'^RadiationLength/(?P<pk>\d+)/update/$', Update_view.RadiationLengthUpdate, name='RadiationLengthUpdate'),

    url(r'^RadiationLengthOccupancy/(?P<pk>\d+)/update/$', Update_view.RadiationLengthOccupancyUpdate, name='RadiationLengthOccupancyUpdate'),

    url(r'^RadioprotectionRequirements/(?P<pk>\d+)/update/$', Update_view.RadioprotectionRequirementsUpdate, name='RadioprotectionRequirementsUpdate'),

    url(r'^SingularField/(?P<pk>\d+)/update/$', Update_view.SingularFieldUpdate, name='SingularFieldUpdate'),

    url(r'^RadiationField/(?P<pk>\d+)/update/$', Update_view.RadiationFieldUpdate, name='RadiationFieldUpdate'),

    url(r'^TechnicalRequirements/(?P<pk>\d+)/update/$', Update_view.TechnicalRequirementsUpdate, name='TechnicalRequirementsUpdate'),

    url(r'^IrradiationFacilityRequirements/(?P<pk>\d+)/update/$', Update_view.IrradiationFacilityRequirementsUpdate, name='IrradiationFacilityRequirementsUpdate'),

    url(r'^Fluence/(?P<pk>\d+)/update/$', Update_view.FluenceUpdate, name='FluenceUpdate'),

    url(r'^NuclearCollisionLength/(?P<pk>\d+)/update/$', Update_view.NuclearCollisionLengthUpdate, name='NuclearCollisionLengthUpdate'),

    url(r'^NuclearInteractionLength/(?P<pk>\d+)/update/$', Update_view.NuclearInteractionLengthUpdate, name='NuclearInteractionLengthUpdate'),

    url(r'^DataManagementSystem/(?P<pk>\d+)/update/$', Update_view.DataManagementSystemUpdate, name='DataManagementSystemUpdate'),

    url(r'^DoseRate/(?P<pk>\d+)/update/$', Update_view.DoseRateUpdate, name='DoseRateUpdate'),

    url(r'^ExternalPosition/(?P<pk>\d+)/update/$', Update_view.ExternalPositionUpdate, name='ExternalPositionUpdate'),

    url(r'^FluenceRate/(?P<pk>\d+)/update/$', Update_view.FluenceRateUpdate, name='FluenceRateUpdate'),

    url(r'^InteractionLengthEntry/(?P<pk>\d+)/update/$', Update_view.InteractionLengthEntryUpdate, name='InteractionLengthEntryUpdate'),

    url(r'^InteractionLengthTable/(?P<pk>\d+)/update/$', Update_view.InteractionLengthTableUpdate, name='InteractionLengthTableUpdate'),

    url(r'^IrradiationPosition/(?P<pk>\d+)/update/$', Update_view.IrradiationPositionUpdate, name='IrradiationPositionUpdate'),

    url(r'^IrradiationZone/(?P<pk>\d+)/update/$', Update_view.IrradiationZoneUpdate, name='IrradiationZoneUpdate'),

    url(r'^Laboratory/(?P<pk>\d+)/update/$', Update_view.LaboratoryUpdate, name='LaboratoryUpdate'),

    url(r'^LegalRequirements/(?P<pk>\d+)/update/$', Update_view.LegalRequirementsUpdate, name='LegalRequirementsUpdate'),

    url(r'^MinimumIonisationEntry/(?P<pk>\d+)/update/$', Update_view.MinimumIonisationEntryUpdate, name='MinimumIonisationEntryUpdate'),

    url(r'^MinimumIonisationTable/(?P<pk>\d+)/update/$', Update_view.MinimumIonisationTableUpdate, name='MinimumIonisationTableUpdate'),

    url(r'^MixedField/(?P<pk>\d+)/update/$', Update_view.MixedFieldUpdate, name='MixedFieldUpdate'),

    url(r'^NuclearCollisionLengthOccupancy/(?P<pk>\d+)/update/$', Update_view.NuclearCollisionLengthOccupancyUpdate, name='NuclearCollisionLengthOccupancyUpdate'),

    url(r'^NuclearInteractionLengthOccupancy/(?P<pk>\d+)/update/$', Update_view.NuclearInteractionLengthOccupancyUpdate, name='NuclearInteractionLengthOccupancyUpdate'),

    url(r'^Particle/(?P<pk>\d+)/update/$', Update_view.ParticleUpdate, name='ParticleUpdate'),


    url(r'^Thing/(?P<pk>\d+)/delete/$', Delete_view.ThingDelete, name='ThingDelete'),

    url(r'^Object/(?P<pk>\d+)/delete/$', Delete_view.ObjectDelete, name='ObjectDelete'),

    url(r'^UnaryFunction/(?P<pk>\d+)/delete/$', Delete_view.UnaryFunctionDelete, name='UnaryFunctionDelete'),

    url(r'^User/(?P<pk>\d+)/delete/$', Delete_view.UserDelete, name='UserDelete'),

    url(r'^IrradiationFacilityRole/(?P<pk>\d+)/delete/$', Delete_view.IrradiationFacilityRoleDelete, name='IrradiationFacilityRoleDelete'),

    url(r'^IrradiationFacilityCoordinator/(?P<pk>\d+)/delete/$', Delete_view.IrradiationFacilityCoordinatorDelete, name='IrradiationFacilityCoordinatorDelete'),

    url(r'^IrradiationFacilityManager/(?P<pk>\d+)/delete/$', Delete_view.IrradiationFacilityManagerDelete, name='IrradiationFacilityManagerDelete'),

    url(r'^Operator/(?P<pk>\d+)/delete/$', Delete_view.OperatorDelete, name='OperatorDelete'),

    url(r'^ResponsiblePerson/(?P<pk>\d+)/delete/$', Delete_view.ResponsiblePersonDelete, name='ResponsiblePersonDelete'),

    url(r'^DUTIrradiation/(?P<pk>\d+)/delete/$', Delete_view.DUTIrradiationDelete, name='DUTIrradiationDelete'),

    url(r'^Sample/(?P<pk>\d+)/delete/$', Delete_view.SampleDelete, name='SampleDelete'),

    url(r'^DUT/(?P<pk>\d+)/delete/$', Delete_view.DUTDelete, name='DUTDelete'),

    url(r'^LayerTable/(?P<pk>\d+)/delete/$', Delete_view.LayerTableDelete, name='LayerTableDelete'),

    url(r'^IrradiationExperiment/(?P<pk>\d+)/delete/$', Delete_view.IrradiationExperimentDelete, name='IrradiationExperimentDelete'),

    url(r'^Layer/(?P<pk>\d+)/delete/$', Delete_view.LayerDelete, name='LayerDelete'),

    url(r'^Compound/(?P<pk>\d+)/delete/$', Delete_view.CompoundDelete, name='CompoundDelete'),

    url(r'^CompoundWeightFractionTable/(?P<pk>\d+)/delete/$', Delete_view.CompoundWeightFractionTableDelete, name='CompoundWeightFractionTableDelete'),

    url(r'^IrradiationFacility/(?P<pk>\d+)/delete/$', Delete_view.IrradiationFacilityDelete, name='IrradiationFacilityDelete'),

    url(r'^Physical/(?P<pk>\d+)/delete/$', Delete_view.PhysicalDelete, name='PhysicalDelete'),

    url(r'^IrradiationFacilityUser/(?P<pk>\d+)/delete/$', Delete_view.IrradiationFacilityUserDelete, name='IrradiationFacilityUserDelete'),

    url(r'^Predicate/(?P<pk>\d+)/delete/$', Delete_view.PredicateDelete, name='PredicateDelete'),

    url(r'^SentientAgent/(?P<pk>\d+)/delete/$', Delete_view.SentientAgentDelete, name='SentientAgentDelete'),

    url(r'^Agent/(?P<pk>\d+)/delete/$', Delete_view.AgentDelete, name='AgentDelete'),

    url(r'^CompoundWeightFractionEntry/(?P<pk>\d+)/delete/$', Delete_view.CompoundWeightFractionEntryDelete, name='CompoundWeightFractionEntryDelete'),

    url(r'^Element/(?P<pk>\d+)/delete/$', Delete_view.ElementDelete, name='ElementDelete'),

    url(r'^UnaryFunctionEntry/(?P<pk>\d+)/delete/$', Delete_view.UnaryFunctionEntryDelete, name='UnaryFunctionEntryDelete'),

    url(r'^CumulatedQuantity/(?P<pk>\d+)/delete/$', Delete_view.CumulatedQuantityDelete, name='CumulatedQuantityDelete'),

    url(r'^IrradiationExperimentObject/(?P<pk>\d+)/delete/$', Delete_view.IrradiationExperimentObjectDelete, name='IrradiationExperimentObjectDelete'),

    url(r'^InteractionLength/(?P<pk>\d+)/delete/$', Delete_view.InteractionLengthDelete, name='InteractionLengthDelete'),

    url(r'^InteractionLengthOccupancy/(?P<pk>\d+)/delete/$', Delete_view.InteractionLengthOccupancyDelete, name='InteractionLengthOccupancyDelete'),

    url(r'^DosimetricQuantity/(?P<pk>\d+)/delete/$', Delete_view.DosimetricQuantityDelete, name='DosimetricQuantityDelete'),

    url(r'^Dosimeter/(?P<pk>\d+)/delete/$', Delete_view.DosimeterDelete, name='DosimeterDelete'),

    url(r'^DomainOfExperiment/(?P<pk>\d+)/delete/$', Delete_view.DomainOfExperimentDelete, name='DomainOfExperimentDelete'),

    url(r'^ParticlePhysicsExperiment/(?P<pk>\d+)/delete/$', Delete_view.ParticlePhysicsExperimentDelete, name='ParticlePhysicsExperimentDelete'),

    url(r'^Requirements/(?P<pk>\d+)/delete/$', Delete_view.RequirementsDelete, name='RequirementsDelete'),

    url(r'^MonitoringSystem/(?P<pk>\d+)/delete/$', Delete_view.MonitoringSystemDelete, name='MonitoringSystemDelete'),

    url(r'^StorageArea/(?P<pk>\d+)/delete/$', Delete_view.StorageAreaDelete, name='StorageAreaDelete'),

    url(r'^FieldOfStudy/(?P<pk>\d+)/delete/$', Delete_view.FieldOfStudyDelete, name='FieldOfStudyDelete'),

    url(r'^IrradiationFacilityPostion/(?P<pk>\d+)/delete/$', Delete_view.IrradiationFacilityPostionDelete, name='IrradiationFacilityPostionDelete'),

    url(r'^Length/(?P<pk>\d+)/delete/$', Delete_view.LengthDelete, name='LengthDelete'),

    url(r'^Percentage/(?P<pk>\d+)/delete/$', Delete_view.PercentageDelete, name='PercentageDelete'),

    url(r'^LayerEntry/(?P<pk>\d+)/delete/$', Delete_view.LayerEntryDelete, name='LayerEntryDelete'),

    url(r'^Relation/(?P<pk>\d+)/delete/$', Delete_view.RelationDelete, name='RelationDelete'),

    url(r'^Abstract/(?P<pk>\d+)/delete/$', Delete_view.AbstractDelete, name='AbstractDelete'),

    url(r'^Region/(?P<pk>\d+)/delete/$', Delete_view.RegionDelete, name='RegionDelete'),

    url(r'^SubjectRole/(?P<pk>\d+)/delete/$', Delete_view.SubjectRoleDelete, name='SubjectRoleDelete'),

    url(r'^ActorRole/(?P<pk>\d+)/delete/$', Delete_view.ActorRoleDelete, name='ActorRoleDelete'),

    url(r'^AbsorbedDose/(?P<pk>\d+)/delete/$', Delete_view.AbsorbedDoseDelete, name='AbsorbedDoseDelete'),

    url(r'^AbsorbedDoseInMedium/(?P<pk>\d+)/delete/$', Delete_view.AbsorbedDoseInMediumDelete, name='AbsorbedDoseInMediumDelete'),

    url(r'^Quantity/(?P<pk>\d+)/delete/$', Delete_view.QuantityDelete, name='QuantityDelete'),

    url(r'^Activity/(?P<pk>\d+)/delete/$', Delete_view.ActivityDelete, name='ActivityDelete'),

    url(r'^DosimetricRate/(?P<pk>\d+)/delete/$', Delete_view.DosimetricRateDelete, name='DosimetricRateDelete'),

    url(r'^AtomicMass/(?P<pk>\d+)/delete/$', Delete_view.AtomicMassDelete, name='AtomicMassDelete'),

    url(r'^Mass/(?P<pk>\d+)/delete/$', Delete_view.MassDelete, name='MassDelete'),

    url(r'^Density/(?P<pk>\d+)/delete/$', Delete_view.DensityDelete, name='DensityDelete'),

    url(r'^Energy/(?P<pk>\d+)/delete/$', Delete_view.EnergyDelete, name='EnergyDelete'),

    url(r'^Height/(?P<pk>\d+)/delete/$', Delete_view.HeightDelete, name='HeightDelete'),

    url(r'^Number/(?P<pk>\d+)/delete/$', Delete_view.NumberDelete, name='NumberDelete'),

    url(r'^Ratio/(?P<pk>\d+)/delete/$', Delete_view.RatioDelete, name='RatioDelete'),

    url(r'^Temperature/(?P<pk>\d+)/delete/$', Delete_view.TemperatureDelete, name='TemperatureDelete'),

    url(r'^Thickness/(?P<pk>\d+)/delete/$', Delete_view.ThicknessDelete, name='ThicknessDelete'),

    url(r'^AbsorbedDoseInAir/(?P<pk>\d+)/delete/$', Delete_view.AbsorbedDoseInAirDelete, name='AbsorbedDoseInAirDelete'),

    url(r'^AdminInfoIrradiationExperiment/(?P<pk>\d+)/delete/$', Delete_view.AdminInfoIrradiationExperimentDelete, name='AdminInfoIrradiationExperimentDelete'),

    url(r'^AdminInfoExperiment/(?P<pk>\d+)/delete/$', Delete_view.AdminInfoExperimentDelete, name='AdminInfoExperimentDelete'),

    url(r'^ActionrelatedRole/(?P<pk>\d+)/delete/$', Delete_view.ActionrelatedRoleDelete, name='ActionrelatedRoleDelete'),

    url(r'^Role/(?P<pk>\d+)/delete/$', Delete_view.RoleDelete, name='RoleDelete'),

    url(r'^AdministrativeInformation/(?P<pk>\d+)/delete/$', Delete_view.AdministrativeInformationDelete, name='AdministrativeInformationDelete'),

    url(r'^Proposition/(?P<pk>\d+)/delete/$', Delete_view.PropositionDelete, name='PropositionDelete'),

    url(r'^AdminInfoObjectOfExperiment/(?P<pk>\d+)/delete/$', Delete_view.AdminInfoObjectOfExperimentDelete, name='AdminInfoObjectOfExperimentDelete'),

    url(r'^AdminInfoUser/(?P<pk>\d+)/delete/$', Delete_view.AdminInfoUserDelete, name='AdminInfoUserDelete'),

    url(r'^Classification/(?P<pk>\d+)/delete/$', Delete_view.ClassificationDelete, name='ClassificationDelete'),

    url(r'^ScientificTask/(?P<pk>\d+)/delete/$', Delete_view.ScientificTaskDelete, name='ScientificTaskDelete'),

    url(r'^ClassificationByDomain/(?P<pk>\d+)/delete/$', Delete_view.ClassificationByDomainDelete, name='ClassificationByDomainDelete'),

    url(r'^ClassificationOfExperiments/(?P<pk>\d+)/delete/$', Delete_view.ClassificationOfExperimentsDelete, name='ClassificationOfExperimentsDelete'),

    url(r'^CorpuscularObject/(?P<pk>\d+)/delete/$', Delete_view.CorpuscularObjectDelete, name='CorpuscularObjectDelete'),

    url(r'^SelfConnectedObject/(?P<pk>\d+)/delete/$', Delete_view.SelfConnectedObjectDelete, name='SelfConnectedObjectDelete'),

    url(r'^ExperimentalRequirements/(?P<pk>\d+)/delete/$', Delete_view.ExperimentalRequirementsDelete, name='ExperimentalRequirementsDelete'),

    url(r'^AdminInfoIrradiationExperimentUser/(?P<pk>\d+)/delete/$', Delete_view.AdminInfoIrradiationExperimentUserDelete, name='AdminInfoIrradiationExperimentUserDelete'),

    url(r'^AdminInfoObjectOfIrradiation/(?P<pk>\d+)/delete/$', Delete_view.AdminInfoObjectOfIrradiationDelete, name='AdminInfoObjectOfIrradiationDelete'),

    url(r'^AdminInfoObjectOfIrradiationExperiment/(?P<pk>\d+)/delete/$', Delete_view.AdminInfoObjectOfIrradiationExperimentDelete, name='AdminInfoObjectOfIrradiationExperimentDelete'),

    url(r'^AtomicNumber/(?P<pk>\d+)/delete/$', Delete_view.AtomicNumberDelete, name='AtomicNumberDelete'),

    url(r'^CollaborationRequirements/(?P<pk>\d+)/delete/$', Delete_view.CollaborationRequirementsDelete, name='CollaborationRequirementsDelete'),

    url(r'^ControlRoom/(?P<pk>\d+)/delete/$', Delete_view.ControlRoomDelete, name='ControlRoomDelete'),

    url(r'^ControlSystem/(?P<pk>\d+)/delete/$', Delete_view.ControlSystemDelete, name='ControlSystemDelete'),

    url(r'^System/(?P<pk>\d+)/delete/$', Delete_view.SystemDelete, name='SystemDelete'),

    url(r'^RadiationLength/(?P<pk>\d+)/delete/$', Delete_view.RadiationLengthDelete, name='RadiationLengthDelete'),

    url(r'^RadiationLengthOccupancy/(?P<pk>\d+)/delete/$', Delete_view.RadiationLengthOccupancyDelete, name='RadiationLengthOccupancyDelete'),

    url(r'^RadioprotectionRequirements/(?P<pk>\d+)/delete/$', Delete_view.RadioprotectionRequirementsDelete, name='RadioprotectionRequirementsDelete'),

    url(r'^SingularField/(?P<pk>\d+)/delete/$', Delete_view.SingularFieldDelete, name='SingularFieldDelete'),

    url(r'^RadiationField/(?P<pk>\d+)/delete/$', Delete_view.RadiationFieldDelete, name='RadiationFieldDelete'),

    url(r'^TechnicalRequirements/(?P<pk>\d+)/delete/$', Delete_view.TechnicalRequirementsDelete, name='TechnicalRequirementsDelete'),

    url(r'^IrradiationFacilityRequirements/(?P<pk>\d+)/delete/$', Delete_view.IrradiationFacilityRequirementsDelete, name='IrradiationFacilityRequirementsDelete'),

    url(r'^Fluence/(?P<pk>\d+)/delete/$', Delete_view.FluenceDelete, name='FluenceDelete'),

    url(r'^NuclearCollisionLength/(?P<pk>\d+)/delete/$', Delete_view.NuclearCollisionLengthDelete, name='NuclearCollisionLengthDelete'),

    url(r'^NuclearInteractionLength/(?P<pk>\d+)/delete/$', Delete_view.NuclearInteractionLengthDelete, name='NuclearInteractionLengthDelete'),

    url(r'^DataManagementSystem/(?P<pk>\d+)/delete/$', Delete_view.DataManagementSystemDelete, name='DataManagementSystemDelete'),

    url(r'^DoseRate/(?P<pk>\d+)/delete/$', Delete_view.DoseRateDelete, name='DoseRateDelete'),

    url(r'^ExternalPosition/(?P<pk>\d+)/delete/$', Delete_view.ExternalPositionDelete, name='ExternalPositionDelete'),

    url(r'^FluenceRate/(?P<pk>\d+)/delete/$', Delete_view.FluenceRateDelete, name='FluenceRateDelete'),

    url(r'^InteractionLengthEntry/(?P<pk>\d+)/delete/$', Delete_view.InteractionLengthEntryDelete, name='InteractionLengthEntryDelete'),

    url(r'^InteractionLengthTable/(?P<pk>\d+)/delete/$', Delete_view.InteractionLengthTableDelete, name='InteractionLengthTableDelete'),

    url(r'^IrradiationPosition/(?P<pk>\d+)/delete/$', Delete_view.IrradiationPositionDelete, name='IrradiationPositionDelete'),

    url(r'^IrradiationZone/(?P<pk>\d+)/delete/$', Delete_view.IrradiationZoneDelete, name='IrradiationZoneDelete'),

    url(r'^Laboratory/(?P<pk>\d+)/delete/$', Delete_view.LaboratoryDelete, name='LaboratoryDelete'),

    url(r'^LegalRequirements/(?P<pk>\d+)/delete/$', Delete_view.LegalRequirementsDelete, name='LegalRequirementsDelete'),

    url(r'^MinimumIonisationEntry/(?P<pk>\d+)/delete/$', Delete_view.MinimumIonisationEntryDelete, name='MinimumIonisationEntryDelete'),

    url(r'^MinimumIonisationTable/(?P<pk>\d+)/delete/$', Delete_view.MinimumIonisationTableDelete, name='MinimumIonisationTableDelete'),

    url(r'^MixedField/(?P<pk>\d+)/delete/$', Delete_view.MixedFieldDelete, name='MixedFieldDelete'),

    url(r'^NuclearCollisionLengthOccupancy/(?P<pk>\d+)/delete/$', Delete_view.NuclearCollisionLengthOccupancyDelete, name='NuclearCollisionLengthOccupancyDelete'),

    url(r'^NuclearInteractionLengthOccupancy/(?P<pk>\d+)/delete/$', Delete_view.NuclearInteractionLengthOccupancyDelete, name='NuclearInteractionLengthOccupancyDelete'),

    url(r'^Particle/(?P<pk>\d+)/delete/$', Delete_view.ParticleDelete, name='ParticleDelete'),

    ]