from django.db import models
from django.template.loader import render_to_string
from .models import *
from .forms import *
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseNotFound
from django.template import loader
from django.core.urlresolvers import reverse
import datetime
from .forms import *
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.core.files.storage import FileSystemStorage
from django.utils.safestring import mark_safe
from django.core import serializers


def ThingList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Thing.objects.all())
    fields = Thing._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ThingList.html",{'data':data, 'fields':new_fields})

def ObjectList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Object.objects.all())
    fields = Object._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ObjectList.html",{'data':data, 'fields':new_fields})

def UnaryFunctionList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", UnaryFunction.objects.all())
    fields = UnaryFunction._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"UnaryFunctionList.html",{'data':data, 'fields':new_fields})

def UserList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", User.objects.all())
    fields = User._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"UserList.html",{'data':data, 'fields':new_fields})

def IrradiationFacilityRoleList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", IrradiationFacilityRole.objects.all())
    fields = IrradiationFacilityRole._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"IrradiationFacilityRoleList.html",{'data':data, 'fields':new_fields})

def IrradiationFacilityCoordinatorList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", IrradiationFacilityCoordinator.objects.all())
    fields = IrradiationFacilityCoordinator._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"IrradiationFacilityCoordinatorList.html",{'data':data, 'fields':new_fields})

def IrradiationFacilityManagerList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", IrradiationFacilityManager.objects.all())
    fields = IrradiationFacilityManager._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"IrradiationFacilityManagerList.html",{'data':data, 'fields':new_fields})

def OperatorList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Operator.objects.all())
    fields = Operator._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"OperatorList.html",{'data':data, 'fields':new_fields})

def ResponsiblePersonList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ResponsiblePerson.objects.all())
    fields = ResponsiblePerson._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ResponsiblePersonList.html",{'data':data, 'fields':new_fields})

def DUTIrradiationList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", DUTIrradiation.objects.all())
    fields = DUTIrradiation._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"DUTIrradiationList.html",{'data':data, 'fields':new_fields})

def SampleList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Sample.objects.all())
    fields = Sample._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"SampleList.html",{'data':data, 'fields':new_fields})

def DUTList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", DUT.objects.all())
    fields = DUT._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"DUTList.html",{'data':data, 'fields':new_fields})

def LayerTableList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", LayerTable.objects.all())
    fields = LayerTable._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"LayerTableList.html",{'data':data, 'fields':new_fields})

def IrradiationExperimentList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", IrradiationExperiment.objects.all())
    fields = IrradiationExperiment._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"IrradiationExperimentList.html",{'data':data, 'fields':new_fields})

def LayerList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Layer.objects.all())
    fields = Layer._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"LayerList.html",{'data':data, 'fields':new_fields})

def CompoundList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Compound.objects.all())
    fields = Compound._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"CompoundList.html",{'data':data, 'fields':new_fields})

def CompoundWeightFractionTableList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", CompoundWeightFractionTable.objects.all())
    fields = CompoundWeightFractionTable._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"CompoundWeightFractionTableList.html",{'data':data, 'fields':new_fields})

def IrradiationFacilityList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", IrradiationFacility.objects.all())
    fields = IrradiationFacility._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"IrradiationFacilityList.html",{'data':data, 'fields':new_fields})

def PhysicalList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Physical.objects.all())
    fields = Physical._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"PhysicalList.html",{'data':data, 'fields':new_fields})

def IrradiationFacilityUserList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", IrradiationFacilityUser.objects.all())
    fields = IrradiationFacilityUser._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"IrradiationFacilityUserList.html",{'data':data, 'fields':new_fields})

def PredicateList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Predicate.objects.all())
    fields = Predicate._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"PredicateList.html",{'data':data, 'fields':new_fields})

def SentientAgentList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", SentientAgent.objects.all())
    fields = SentientAgent._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"SentientAgentList.html",{'data':data, 'fields':new_fields})

def AgentList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Agent.objects.all())
    fields = Agent._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"AgentList.html",{'data':data, 'fields':new_fields})

def CompoundWeightFractionEntryList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", CompoundWeightFractionEntry.objects.all())
    fields = CompoundWeightFractionEntry._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"CompoundWeightFractionEntryList.html",{'data':data, 'fields':new_fields})

def ElementList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Element.objects.all())
    fields = Element._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ElementList.html",{'data':data, 'fields':new_fields})

def UnaryFunctionEntryList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", UnaryFunctionEntry.objects.all())
    fields = UnaryFunctionEntry._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"UnaryFunctionEntryList.html",{'data':data, 'fields':new_fields})

def CumulatedQuantityList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", CumulatedQuantity.objects.all())
    fields = CumulatedQuantity._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"CumulatedQuantityList.html",{'data':data, 'fields':new_fields})

def IrradiationExperimentObjectList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", IrradiationExperimentObject.objects.all())
    fields = IrradiationExperimentObject._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"IrradiationExperimentObjectList.html",{'data':data, 'fields':new_fields})

def InteractionLengthList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", InteractionLength.objects.all())
    fields = InteractionLength._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"InteractionLengthList.html",{'data':data, 'fields':new_fields})

def InteractionLengthOccupancyList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", InteractionLengthOccupancy.objects.all())
    fields = InteractionLengthOccupancy._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"InteractionLengthOccupancyList.html",{'data':data, 'fields':new_fields})

def DosimetricQuantityList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", DosimetricQuantity.objects.all())
    fields = DosimetricQuantity._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"DosimetricQuantityList.html",{'data':data, 'fields':new_fields})

def DosimeterList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Dosimeter.objects.all())
    fields = Dosimeter._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"DosimeterList.html",{'data':data, 'fields':new_fields})

def DomainOfExperimentList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", DomainOfExperiment.objects.all())
    fields = DomainOfExperiment._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"DomainOfExperimentList.html",{'data':data, 'fields':new_fields})

def ParticlePhysicsExperimentList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ParticlePhysicsExperiment.objects.all())
    fields = ParticlePhysicsExperiment._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ParticlePhysicsExperimentList.html",{'data':data, 'fields':new_fields})

def RequirementsList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Requirements.objects.all())
    fields = Requirements._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"RequirementsList.html",{'data':data, 'fields':new_fields})

def MonitoringSystemList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", MonitoringSystem.objects.all())
    fields = MonitoringSystem._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"MonitoringSystemList.html",{'data':data, 'fields':new_fields})

def StorageAreaList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", StorageArea.objects.all())
    fields = StorageArea._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"StorageAreaList.html",{'data':data, 'fields':new_fields})

def FieldOfStudyList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", FieldOfStudy.objects.all())
    fields = FieldOfStudy._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"FieldOfStudyList.html",{'data':data, 'fields':new_fields})

def IrradiationFacilityPostionList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", IrradiationFacilityPostion.objects.all())
    fields = IrradiationFacilityPostion._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"IrradiationFacilityPostionList.html",{'data':data, 'fields':new_fields})

def LengthList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Length.objects.all())
    fields = Length._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"LengthList.html",{'data':data, 'fields':new_fields})

def PercentageList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Percentage.objects.all())
    fields = Percentage._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"PercentageList.html",{'data':data, 'fields':new_fields})

def LayerEntryList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", LayerEntry.objects.all())
    fields = LayerEntry._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"LayerEntryList.html",{'data':data, 'fields':new_fields})

def RelationList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Relation.objects.all())
    fields = Relation._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"RelationList.html",{'data':data, 'fields':new_fields})

def AbstractList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Abstract.objects.all())
    fields = Abstract._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"AbstractList.html",{'data':data, 'fields':new_fields})

def RegionList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Region.objects.all())
    fields = Region._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"RegionList.html",{'data':data, 'fields':new_fields})

def SubjectRoleList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", SubjectRole.objects.all())
    fields = SubjectRole._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"SubjectRoleList.html",{'data':data, 'fields':new_fields})

def ActorRoleList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ActorRole.objects.all())
    fields = ActorRole._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ActorRoleList.html",{'data':data, 'fields':new_fields})

def AbsorbedDoseList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", AbsorbedDose.objects.all())
    fields = AbsorbedDose._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"AbsorbedDoseList.html",{'data':data, 'fields':new_fields})

def AbsorbedDoseInMediumList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", AbsorbedDoseInMedium.objects.all())
    fields = AbsorbedDoseInMedium._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"AbsorbedDoseInMediumList.html",{'data':data, 'fields':new_fields})

def QuantityList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Quantity.objects.all())
    fields = Quantity._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"QuantityList.html",{'data':data, 'fields':new_fields})

def ActivityList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Activity.objects.all())
    fields = Activity._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ActivityList.html",{'data':data, 'fields':new_fields})

def DosimetricRateList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", DosimetricRate.objects.all())
    fields = DosimetricRate._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"DosimetricRateList.html",{'data':data, 'fields':new_fields})

def AtomicMassList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", AtomicMass.objects.all())
    fields = AtomicMass._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"AtomicMassList.html",{'data':data, 'fields':new_fields})

def MassList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Mass.objects.all())
    fields = Mass._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"MassList.html",{'data':data, 'fields':new_fields})

def DensityList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Density.objects.all())
    fields = Density._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"DensityList.html",{'data':data, 'fields':new_fields})

def EnergyList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Energy.objects.all())
    fields = Energy._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"EnergyList.html",{'data':data, 'fields':new_fields})

def HeightList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Height.objects.all())
    fields = Height._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"HeightList.html",{'data':data, 'fields':new_fields})

def NumberList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Number.objects.all())
    fields = Number._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"NumberList.html",{'data':data, 'fields':new_fields})

def RatioList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Ratio.objects.all())
    fields = Ratio._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"RatioList.html",{'data':data, 'fields':new_fields})

def TemperatureList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Temperature.objects.all())
    fields = Temperature._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"TemperatureList.html",{'data':data, 'fields':new_fields})

def ThicknessList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Thickness.objects.all())
    fields = Thickness._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ThicknessList.html",{'data':data, 'fields':new_fields})

def AbsorbedDoseInAirList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", AbsorbedDoseInAir.objects.all())
    fields = AbsorbedDoseInAir._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"AbsorbedDoseInAirList.html",{'data':data, 'fields':new_fields})

def AdminInfoIrradiationExperimentList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", AdminInfoIrradiationExperiment.objects.all())
    fields = AdminInfoIrradiationExperiment._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"AdminInfoIrradiationExperimentList.html",{'data':data, 'fields':new_fields})

def AdminInfoExperimentList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", AdminInfoExperiment.objects.all())
    fields = AdminInfoExperiment._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"AdminInfoExperimentList.html",{'data':data, 'fields':new_fields})

def ActionrelatedRoleList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ActionrelatedRole.objects.all())
    fields = ActionrelatedRole._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ActionrelatedRoleList.html",{'data':data, 'fields':new_fields})

def RoleList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Role.objects.all())
    fields = Role._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"RoleList.html",{'data':data, 'fields':new_fields})

def AdministrativeInformationList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", AdministrativeInformation.objects.all())
    fields = AdministrativeInformation._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"AdministrativeInformationList.html",{'data':data, 'fields':new_fields})

def PropositionList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Proposition.objects.all())
    fields = Proposition._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"PropositionList.html",{'data':data, 'fields':new_fields})

def AdminInfoObjectOfExperimentList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", AdminInfoObjectOfExperiment.objects.all())
    fields = AdminInfoObjectOfExperiment._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"AdminInfoObjectOfExperimentList.html",{'data':data, 'fields':new_fields})

def AdminInfoUserList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", AdminInfoUser.objects.all())
    fields = AdminInfoUser._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"AdminInfoUserList.html",{'data':data, 'fields':new_fields})

def ClassificationList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Classification.objects.all())
    fields = Classification._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ClassificationList.html",{'data':data, 'fields':new_fields})

def ScientificTaskList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ScientificTask.objects.all())
    fields = ScientificTask._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ScientificTaskList.html",{'data':data, 'fields':new_fields})

def ClassificationByDomainList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ClassificationByDomain.objects.all())
    fields = ClassificationByDomain._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ClassificationByDomainList.html",{'data':data, 'fields':new_fields})

def ClassificationOfExperimentsList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ClassificationOfExperiments.objects.all())
    fields = ClassificationOfExperiments._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ClassificationOfExperimentsList.html",{'data':data, 'fields':new_fields})

def CorpuscularObjectList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", CorpuscularObject.objects.all())
    fields = CorpuscularObject._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"CorpuscularObjectList.html",{'data':data, 'fields':new_fields})

def SelfConnectedObjectList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", SelfConnectedObject.objects.all())
    fields = SelfConnectedObject._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"SelfConnectedObjectList.html",{'data':data, 'fields':new_fields})

def ExperimentalRequirementsList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ExperimentalRequirements.objects.all())
    fields = ExperimentalRequirements._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ExperimentalRequirementsList.html",{'data':data, 'fields':new_fields})

def AdminInfoIrradiationExperimentUserList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", AdminInfoIrradiationExperimentUser.objects.all())
    fields = AdminInfoIrradiationExperimentUser._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"AdminInfoIrradiationExperimentUserList.html",{'data':data, 'fields':new_fields})

def AdminInfoObjectOfIrradiationList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", AdminInfoObjectOfIrradiation.objects.all())
    fields = AdminInfoObjectOfIrradiation._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"AdminInfoObjectOfIrradiationList.html",{'data':data, 'fields':new_fields})

def AdminInfoObjectOfIrradiationExperimentList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", AdminInfoObjectOfIrradiationExperiment.objects.all())
    fields = AdminInfoObjectOfIrradiationExperiment._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"AdminInfoObjectOfIrradiationExperimentList.html",{'data':data, 'fields':new_fields})

def AtomicNumberList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", AtomicNumber.objects.all())
    fields = AtomicNumber._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"AtomicNumberList.html",{'data':data, 'fields':new_fields})

def CollaborationRequirementsList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", CollaborationRequirements.objects.all())
    fields = CollaborationRequirements._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"CollaborationRequirementsList.html",{'data':data, 'fields':new_fields})

def ControlRoomList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ControlRoom.objects.all())
    fields = ControlRoom._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ControlRoomList.html",{'data':data, 'fields':new_fields})

def ControlSystemList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ControlSystem.objects.all())
    fields = ControlSystem._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ControlSystemList.html",{'data':data, 'fields':new_fields})

def SystemList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", System.objects.all())
    fields = System._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"SystemList.html",{'data':data, 'fields':new_fields})

def RadiationLengthList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", RadiationLength.objects.all())
    fields = RadiationLength._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"RadiationLengthList.html",{'data':data, 'fields':new_fields})

def RadiationLengthOccupancyList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", RadiationLengthOccupancy.objects.all())
    fields = RadiationLengthOccupancy._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"RadiationLengthOccupancyList.html",{'data':data, 'fields':new_fields})

def RadioprotectionRequirementsList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", RadioprotectionRequirements.objects.all())
    fields = RadioprotectionRequirements._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"RadioprotectionRequirementsList.html",{'data':data, 'fields':new_fields})

def SingularFieldList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", SingularField.objects.all())
    fields = SingularField._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"SingularFieldList.html",{'data':data, 'fields':new_fields})

def RadiationFieldList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", RadiationField.objects.all())
    fields = RadiationField._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"RadiationFieldList.html",{'data':data, 'fields':new_fields})

def TechnicalRequirementsList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", TechnicalRequirements.objects.all())
    fields = TechnicalRequirements._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"TechnicalRequirementsList.html",{'data':data, 'fields':new_fields})

def IrradiationFacilityRequirementsList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", IrradiationFacilityRequirements.objects.all())
    fields = IrradiationFacilityRequirements._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"IrradiationFacilityRequirementsList.html",{'data':data, 'fields':new_fields})

def FluenceList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Fluence.objects.all())
    fields = Fluence._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"FluenceList.html",{'data':data, 'fields':new_fields})

def NuclearCollisionLengthList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", NuclearCollisionLength.objects.all())
    fields = NuclearCollisionLength._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"NuclearCollisionLengthList.html",{'data':data, 'fields':new_fields})

def NuclearInteractionLengthList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", NuclearInteractionLength.objects.all())
    fields = NuclearInteractionLength._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"NuclearInteractionLengthList.html",{'data':data, 'fields':new_fields})

def DataManagementSystemList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", DataManagementSystem.objects.all())
    fields = DataManagementSystem._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"DataManagementSystemList.html",{'data':data, 'fields':new_fields})

def DoseRateList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", DoseRate.objects.all())
    fields = DoseRate._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"DoseRateList.html",{'data':data, 'fields':new_fields})

def ExternalPositionList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ExternalPosition.objects.all())
    fields = ExternalPosition._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ExternalPositionList.html",{'data':data, 'fields':new_fields})

def FluenceRateList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", FluenceRate.objects.all())
    fields = FluenceRate._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"FluenceRateList.html",{'data':data, 'fields':new_fields})

def InteractionLengthEntryList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", InteractionLengthEntry.objects.all())
    fields = InteractionLengthEntry._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"InteractionLengthEntryList.html",{'data':data, 'fields':new_fields})

def InteractionLengthTableList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", InteractionLengthTable.objects.all())
    fields = InteractionLengthTable._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"InteractionLengthTableList.html",{'data':data, 'fields':new_fields})

def IrradiationPositionList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", IrradiationPosition.objects.all())
    fields = IrradiationPosition._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"IrradiationPositionList.html",{'data':data, 'fields':new_fields})

def IrradiationZoneList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", IrradiationZone.objects.all())
    fields = IrradiationZone._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"IrradiationZoneList.html",{'data':data, 'fields':new_fields})

def LaboratoryList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Laboratory.objects.all())
    fields = Laboratory._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"LaboratoryList.html",{'data':data, 'fields':new_fields})

def LegalRequirementsList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", LegalRequirements.objects.all())
    fields = LegalRequirements._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"LegalRequirementsList.html",{'data':data, 'fields':new_fields})

def MinimumIonisationEntryList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", MinimumIonisationEntry.objects.all())
    fields = MinimumIonisationEntry._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"MinimumIonisationEntryList.html",{'data':data, 'fields':new_fields})

def MinimumIonisationTableList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", MinimumIonisationTable.objects.all())
    fields = MinimumIonisationTable._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"MinimumIonisationTableList.html",{'data':data, 'fields':new_fields})

def MixedFieldList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", MixedField.objects.all())
    fields = MixedField._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"MixedFieldList.html",{'data':data, 'fields':new_fields})

def NuclearCollisionLengthOccupancyList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", NuclearCollisionLengthOccupancy.objects.all())
    fields = NuclearCollisionLengthOccupancy._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"NuclearCollisionLengthOccupancyList.html",{'data':data, 'fields':new_fields})

def NuclearInteractionLengthOccupancyList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", NuclearInteractionLengthOccupancy.objects.all())
    fields = NuclearInteractionLengthOccupancy._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"NuclearInteractionLengthOccupancyList.html",{'data':data, 'fields':new_fields})

def ParticleList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Particle.objects.all())
    fields = Particle._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ParticleList.html",{'data':data, 'fields':new_fields})
