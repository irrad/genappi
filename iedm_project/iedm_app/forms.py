from django import forms
from django.forms import ModelForm
from .models import *


class ThingForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ThingForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Thing
        exclude = ()

class ObjectForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ObjectForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Object
        exclude = ()

class UnaryFunctionForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(UnaryFunctionForm, self).__init__(*args, **kwargs)
    class Meta:
        model = UnaryFunction
        exclude = ()

class UserForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(UserForm, self).__init__(*args, **kwargs)
    class Meta:
        model = User
        exclude = ()

class IrradiationFacilityRoleForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(IrradiationFacilityRoleForm, self).__init__(*args, **kwargs)
    class Meta:
        model = IrradiationFacilityRole
        exclude = ()

class IrradiationFacilityCoordinatorForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(IrradiationFacilityCoordinatorForm, self).__init__(*args, **kwargs)
    class Meta:
        model = IrradiationFacilityCoordinator
        exclude = ()

class IrradiationFacilityManagerForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(IrradiationFacilityManagerForm, self).__init__(*args, **kwargs)
    class Meta:
        model = IrradiationFacilityManager
        exclude = ()

class OperatorForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(OperatorForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Operator
        exclude = ()

class ResponsiblePersonForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ResponsiblePersonForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ResponsiblePerson
        exclude = ()

class DUTIrradiationForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(DUTIrradiationForm, self).__init__(*args, **kwargs)
    class Meta:
        model = DUTIrradiation
        exclude = ()

class SampleForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(SampleForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Sample
        exclude = ()

class DUTForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(DUTForm, self).__init__(*args, **kwargs)
    class Meta:
        model = DUT
        exclude = ()

class LayerTableForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(LayerTableForm, self).__init__(*args, **kwargs)
    class Meta:
        model = LayerTable
        exclude = ()

class IrradiationExperimentForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(IrradiationExperimentForm, self).__init__(*args, **kwargs)
    class Meta:
        model = IrradiationExperiment
        exclude = ()

class LayerForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(LayerForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Layer
        exclude = ()

class CompoundForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(CompoundForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Compound
        exclude = ()

class CompoundWeightFractionTableForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(CompoundWeightFractionTableForm, self).__init__(*args, **kwargs)
    class Meta:
        model = CompoundWeightFractionTable
        exclude = ()

class IrradiationFacilityForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(IrradiationFacilityForm, self).__init__(*args, **kwargs)
    class Meta:
        model = IrradiationFacility
        exclude = ()

class PhysicalForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(PhysicalForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Physical
        exclude = ()

class IrradiationFacilityUserForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(IrradiationFacilityUserForm, self).__init__(*args, **kwargs)
    class Meta:
        model = IrradiationFacilityUser
        exclude = ()

class PredicateForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(PredicateForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Predicate
        exclude = ()

class SentientAgentForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(SentientAgentForm, self).__init__(*args, **kwargs)
    class Meta:
        model = SentientAgent
        exclude = ()

class AgentForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(AgentForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Agent
        exclude = ()

class CompoundWeightFractionEntryForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(CompoundWeightFractionEntryForm, self).__init__(*args, **kwargs)
    class Meta:
        model = CompoundWeightFractionEntry
        exclude = ()

class ElementForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ElementForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Element
        exclude = ()

class UnaryFunctionEntryForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(UnaryFunctionEntryForm, self).__init__(*args, **kwargs)
    class Meta:
        model = UnaryFunctionEntry
        exclude = ()

class CumulatedQuantityForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(CumulatedQuantityForm, self).__init__(*args, **kwargs)
    class Meta:
        model = CumulatedQuantity
        exclude = ()

class IrradiationExperimentObjectForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(IrradiationExperimentObjectForm, self).__init__(*args, **kwargs)
    class Meta:
        model = IrradiationExperimentObject
        exclude = ()

class InteractionLengthForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(InteractionLengthForm, self).__init__(*args, **kwargs)
    class Meta:
        model = InteractionLength
        exclude = ()

class InteractionLengthOccupancyForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(InteractionLengthOccupancyForm, self).__init__(*args, **kwargs)
    class Meta:
        model = InteractionLengthOccupancy
        exclude = ()

class DosimetricQuantityForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(DosimetricQuantityForm, self).__init__(*args, **kwargs)
    class Meta:
        model = DosimetricQuantity
        exclude = ()

class DosimeterForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(DosimeterForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Dosimeter
        exclude = ()

class DomainOfExperimentForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(DomainOfExperimentForm, self).__init__(*args, **kwargs)
    class Meta:
        model = DomainOfExperiment
        exclude = ()

class ParticlePhysicsExperimentForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ParticlePhysicsExperimentForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ParticlePhysicsExperiment
        exclude = ()

class RequirementsForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(RequirementsForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Requirements
        exclude = ()

class MonitoringSystemForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(MonitoringSystemForm, self).__init__(*args, **kwargs)
    class Meta:
        model = MonitoringSystem
        exclude = ()

class StorageAreaForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(StorageAreaForm, self).__init__(*args, **kwargs)
    class Meta:
        model = StorageArea
        exclude = ()

class FieldOfStudyForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(FieldOfStudyForm, self).__init__(*args, **kwargs)
    class Meta:
        model = FieldOfStudy
        exclude = ()

class IrradiationFacilityPostionForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(IrradiationFacilityPostionForm, self).__init__(*args, **kwargs)
    class Meta:
        model = IrradiationFacilityPostion
        exclude = ()

class LengthForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(LengthForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Length
        exclude = ()

class PercentageForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(PercentageForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Percentage
        exclude = ()

class LayerEntryForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(LayerEntryForm, self).__init__(*args, **kwargs)
    class Meta:
        model = LayerEntry
        exclude = ()

class RelationForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(RelationForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Relation
        exclude = ()

class AbstractForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(AbstractForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Abstract
        exclude = ()

class RegionForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(RegionForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Region
        exclude = ()

class SubjectRoleForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(SubjectRoleForm, self).__init__(*args, **kwargs)
    class Meta:
        model = SubjectRole
        exclude = ()

class ActorRoleForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ActorRoleForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ActorRole
        exclude = ()

class AbsorbedDoseForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(AbsorbedDoseForm, self).__init__(*args, **kwargs)
    class Meta:
        model = AbsorbedDose
        exclude = ()

class AbsorbedDoseInMediumForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(AbsorbedDoseInMediumForm, self).__init__(*args, **kwargs)
    class Meta:
        model = AbsorbedDoseInMedium
        exclude = ()

class QuantityForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(QuantityForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Quantity
        exclude = ()

class ActivityForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ActivityForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Activity
        exclude = ()

class DosimetricRateForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(DosimetricRateForm, self).__init__(*args, **kwargs)
    class Meta:
        model = DosimetricRate
        exclude = ()

class AtomicMassForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(AtomicMassForm, self).__init__(*args, **kwargs)
    class Meta:
        model = AtomicMass
        exclude = ()

class MassForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(MassForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Mass
        exclude = ()

class DensityForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(DensityForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Density
        exclude = ()

class EnergyForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(EnergyForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Energy
        exclude = ()

class HeightForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(HeightForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Height
        exclude = ()

class NumberForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(NumberForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Number
        exclude = ()

class RatioForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(RatioForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Ratio
        exclude = ()

class TemperatureForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(TemperatureForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Temperature
        exclude = ()

class ThicknessForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ThicknessForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Thickness
        exclude = ()

class AbsorbedDoseInAirForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(AbsorbedDoseInAirForm, self).__init__(*args, **kwargs)
    class Meta:
        model = AbsorbedDoseInAir
        exclude = ()

class AdminInfoIrradiationExperimentForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(AdminInfoIrradiationExperimentForm, self).__init__(*args, **kwargs)
    class Meta:
        model = AdminInfoIrradiationExperiment
        exclude = ()

class AdminInfoExperimentForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(AdminInfoExperimentForm, self).__init__(*args, **kwargs)
    class Meta:
        model = AdminInfoExperiment
        exclude = ()

class ActionrelatedRoleForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ActionrelatedRoleForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ActionrelatedRole
        exclude = ()

class RoleForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(RoleForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Role
        exclude = ()

class AdministrativeInformationForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(AdministrativeInformationForm, self).__init__(*args, **kwargs)
    class Meta:
        model = AdministrativeInformation
        exclude = ()

class PropositionForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(PropositionForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Proposition
        exclude = ()

class AdminInfoObjectOfExperimentForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(AdminInfoObjectOfExperimentForm, self).__init__(*args, **kwargs)
    class Meta:
        model = AdminInfoObjectOfExperiment
        exclude = ()

class AdminInfoUserForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(AdminInfoUserForm, self).__init__(*args, **kwargs)
    class Meta:
        model = AdminInfoUser
        exclude = ()

class ClassificationForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ClassificationForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Classification
        exclude = ()

class ScientificTaskForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ScientificTaskForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ScientificTask
        exclude = ()

class ClassificationByDomainForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ClassificationByDomainForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ClassificationByDomain
        exclude = ()

class ClassificationOfExperimentsForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ClassificationOfExperimentsForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ClassificationOfExperiments
        exclude = ()

class CorpuscularObjectForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(CorpuscularObjectForm, self).__init__(*args, **kwargs)
    class Meta:
        model = CorpuscularObject
        exclude = ()

class SelfConnectedObjectForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(SelfConnectedObjectForm, self).__init__(*args, **kwargs)
    class Meta:
        model = SelfConnectedObject
        exclude = ()

class ExperimentalRequirementsForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ExperimentalRequirementsForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ExperimentalRequirements
        exclude = ()

class AdminInfoIrradiationExperimentUserForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(AdminInfoIrradiationExperimentUserForm, self).__init__(*args, **kwargs)
    class Meta:
        model = AdminInfoIrradiationExperimentUser
        exclude = ()

class AdminInfoObjectOfIrradiationForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(AdminInfoObjectOfIrradiationForm, self).__init__(*args, **kwargs)
    class Meta:
        model = AdminInfoObjectOfIrradiation
        exclude = ()

class AdminInfoObjectOfIrradiationExperimentForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(AdminInfoObjectOfIrradiationExperimentForm, self).__init__(*args, **kwargs)
    class Meta:
        model = AdminInfoObjectOfIrradiationExperiment
        exclude = ()

class AtomicNumberForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(AtomicNumberForm, self).__init__(*args, **kwargs)
    class Meta:
        model = AtomicNumber
        exclude = ()

class CollaborationRequirementsForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(CollaborationRequirementsForm, self).__init__(*args, **kwargs)
    class Meta:
        model = CollaborationRequirements
        exclude = ()

class ControlRoomForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ControlRoomForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ControlRoom
        exclude = ()

class ControlSystemForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ControlSystemForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ControlSystem
        exclude = ()

class SystemForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(SystemForm, self).__init__(*args, **kwargs)
    class Meta:
        model = System
        exclude = ()

class RadiationLengthForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(RadiationLengthForm, self).__init__(*args, **kwargs)
    class Meta:
        model = RadiationLength
        exclude = ()

class RadiationLengthOccupancyForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(RadiationLengthOccupancyForm, self).__init__(*args, **kwargs)
    class Meta:
        model = RadiationLengthOccupancy
        exclude = ()

class RadioprotectionRequirementsForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(RadioprotectionRequirementsForm, self).__init__(*args, **kwargs)
    class Meta:
        model = RadioprotectionRequirements
        exclude = ()

class SingularFieldForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(SingularFieldForm, self).__init__(*args, **kwargs)
    class Meta:
        model = SingularField
        exclude = ()

class RadiationFieldForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(RadiationFieldForm, self).__init__(*args, **kwargs)
    class Meta:
        model = RadiationField
        exclude = ()

class TechnicalRequirementsForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(TechnicalRequirementsForm, self).__init__(*args, **kwargs)
    class Meta:
        model = TechnicalRequirements
        exclude = ()

class IrradiationFacilityRequirementsForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(IrradiationFacilityRequirementsForm, self).__init__(*args, **kwargs)
    class Meta:
        model = IrradiationFacilityRequirements
        exclude = ()

class FluenceForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(FluenceForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Fluence
        exclude = ()

class NuclearCollisionLengthForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(NuclearCollisionLengthForm, self).__init__(*args, **kwargs)
    class Meta:
        model = NuclearCollisionLength
        exclude = ()

class NuclearInteractionLengthForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(NuclearInteractionLengthForm, self).__init__(*args, **kwargs)
    class Meta:
        model = NuclearInteractionLength
        exclude = ()

class DataManagementSystemForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(DataManagementSystemForm, self).__init__(*args, **kwargs)
    class Meta:
        model = DataManagementSystem
        exclude = ()

class DoseRateForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(DoseRateForm, self).__init__(*args, **kwargs)
    class Meta:
        model = DoseRate
        exclude = ()

class ExternalPositionForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ExternalPositionForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ExternalPosition
        exclude = ()

class FluenceRateForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(FluenceRateForm, self).__init__(*args, **kwargs)
    class Meta:
        model = FluenceRate
        exclude = ()

class InteractionLengthEntryForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(InteractionLengthEntryForm, self).__init__(*args, **kwargs)
    class Meta:
        model = InteractionLengthEntry
        exclude = ()

class InteractionLengthTableForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(InteractionLengthTableForm, self).__init__(*args, **kwargs)
    class Meta:
        model = InteractionLengthTable
        exclude = ()

class IrradiationPositionForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(IrradiationPositionForm, self).__init__(*args, **kwargs)
    class Meta:
        model = IrradiationPosition
        exclude = ()

class IrradiationZoneForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(IrradiationZoneForm, self).__init__(*args, **kwargs)
    class Meta:
        model = IrradiationZone
        exclude = ()

class LaboratoryForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(LaboratoryForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Laboratory
        exclude = ()

class LegalRequirementsForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(LegalRequirementsForm, self).__init__(*args, **kwargs)
    class Meta:
        model = LegalRequirements
        exclude = ()

class MinimumIonisationEntryForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(MinimumIonisationEntryForm, self).__init__(*args, **kwargs)
    class Meta:
        model = MinimumIonisationEntry
        exclude = ()

class MinimumIonisationTableForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(MinimumIonisationTableForm, self).__init__(*args, **kwargs)
    class Meta:
        model = MinimumIonisationTable
        exclude = ()

class MixedFieldForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(MixedFieldForm, self).__init__(*args, **kwargs)
    class Meta:
        model = MixedField
        exclude = ()

class NuclearCollisionLengthOccupancyForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(NuclearCollisionLengthOccupancyForm, self).__init__(*args, **kwargs)
    class Meta:
        model = NuclearCollisionLengthOccupancy
        exclude = ()

class NuclearInteractionLengthOccupancyForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(NuclearInteractionLengthOccupancyForm, self).__init__(*args, **kwargs)
    class Meta:
        model = NuclearInteractionLengthOccupancy
        exclude = ()

class ParticleForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ParticleForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Particle
        exclude = ()
