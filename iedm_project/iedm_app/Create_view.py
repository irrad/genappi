from owlready2 import *
from django.db import models
from django.template.loader import render_to_string
from .models import *
from .forms import *
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseNotFound
from django.template import loader
from django.core.urlresolvers import reverse
import datetime
from .forms import *
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.core.files.storage import FileSystemStorage
from django.utils.safestring import mark_safe
from django.conf import settings

onto_path.append(settings.BASE_DIR)
onto = get_ontology("iedm.owl").load()
ontoclasses = onto.classes()
namespaces = dict() 
for clas in ontoclasses:
    namespaces[clas.name] = clas.namespace


def ThingCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Thing"]

    

    if request.method == 'POST':
        form = ThingForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Thing(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/Thing/list/"
            return HttpResponseRedirect(link)
    else:
        form = ThingForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ThingCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ObjectCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Object"]

    

    if request.method == 'POST':
        form = ObjectForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Object(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/Object/list/"
            return HttpResponseRedirect(link)
    else:
        form = ObjectForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ObjectCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def UnaryFunctionCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["UnaryFunction"]

    

    if request.method == 'POST':
        form = UnaryFunctionForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.UnaryFunction(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/UnaryFunction/list/"
            return HttpResponseRedirect(link)
    else:
        form = UnaryFunctionForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "UnaryFunctionCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def UserCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["User"]

    

    if request.method == 'POST':
        form = UserForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.User(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/User/list/"
            return HttpResponseRedirect(link)
    else:
        form = UserForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "UserCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def IrradiationFacilityRoleCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["IrradiationFacilityRole"]

    

    if request.method == 'POST':
        form = IrradiationFacilityRoleForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.IrradiationFacilityRole(instance.name)
            onto.save()
            

            
            if IrradiationFacilityRole._meta.get_field('name').get_internal_type()  == 'IntegerField':
                onto_instance.name=int(form.cleaned_data['name'])
            elif IrradiationFacilityRole._meta.get_field('name').get_internal_type()  == 'DecimalField':
                onto_instance.name=float(form.cleaned_data['name'])
            else:
                onto_instance.name=str(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()
            link = "/iedm_app/IrradiationFacilityRole/list/"
            return HttpResponseRedirect(link)
    else:
        form = IrradiationFacilityRoleForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "IrradiationFacilityRoleCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def IrradiationFacilityCoordinatorCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["IrradiationFacilityCoordinator"]

    

    if request.method == 'POST':
        form = IrradiationFacilityCoordinatorForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.IrradiationFacilityCoordinator(instance.name)
            onto.save()
            

            
            if IrradiationFacilityCoordinator._meta.get_field('name').get_internal_type()  == 'IntegerField':
                onto_instance.name=int(form.cleaned_data['name'])
            elif IrradiationFacilityCoordinator._meta.get_field('name').get_internal_type()  == 'DecimalField':
                onto_instance.name=float(form.cleaned_data['name'])
            else:
                onto_instance.name=str(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()
            link = "/iedm_app/IrradiationFacilityCoordinator/list/"
            return HttpResponseRedirect(link)
    else:
        form = IrradiationFacilityCoordinatorForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "IrradiationFacilityCoordinatorCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def IrradiationFacilityManagerCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["IrradiationFacilityManager"]

    

    if request.method == 'POST':
        form = IrradiationFacilityManagerForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.IrradiationFacilityManager(instance.name)
            onto.save()
            

            
            if IrradiationFacilityManager._meta.get_field('name').get_internal_type()  == 'IntegerField':
                onto_instance.name=int(form.cleaned_data['name'])
            elif IrradiationFacilityManager._meta.get_field('name').get_internal_type()  == 'DecimalField':
                onto_instance.name=float(form.cleaned_data['name'])
            else:
                onto_instance.name=str(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()
            link = "/iedm_app/IrradiationFacilityManager/list/"
            return HttpResponseRedirect(link)
    else:
        form = IrradiationFacilityManagerForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "IrradiationFacilityManagerCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def OperatorCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Operator"]

    

    if request.method == 'POST':
        form = OperatorForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Operator(instance.name)
            onto.save()
            

            
            if Operator._meta.get_field('name').get_internal_type()  == 'IntegerField':
                onto_instance.name=int(form.cleaned_data['name'])
            elif Operator._meta.get_field('name').get_internal_type()  == 'DecimalField':
                onto_instance.name=float(form.cleaned_data['name'])
            else:
                onto_instance.name=str(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()
            link = "/iedm_app/Operator/list/"
            return HttpResponseRedirect(link)
    else:
        form = OperatorForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "OperatorCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ResponsiblePersonCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ResponsiblePerson"]

    

    if request.method == 'POST':
        form = ResponsiblePersonForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ResponsiblePerson(instance.name)
            onto.save()
            

            
            if ResponsiblePerson._meta.get_field('name').get_internal_type()  == 'IntegerField':
                onto_instance.name=int(form.cleaned_data['name'])
            elif ResponsiblePerson._meta.get_field('name').get_internal_type()  == 'DecimalField':
                onto_instance.name=float(form.cleaned_data['name'])
            else:
                onto_instance.name=str(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()
            link = "/iedm_app/ResponsiblePerson/list/"
            return HttpResponseRedirect(link)
    else:
        form = ResponsiblePersonForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ResponsiblePersonCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def DUTIrradiationCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["DUTIrradiation"]

    
    foreign_keys['HasCumulatedQuantity'] = 'iedm_app:CumulatedQuantityList'
    
    foreign_keys['HasDosimeter'] = 'iedm_app:IrradiationExperimentObjectList'
    
    foreign_keys['HasInteractionLength'] = 'iedm_app:InteractionLengthList'
    
    foreign_keys['HasInteractionLengthOccupancy'] = 'iedm_app:InteractionLengthOccupancyList'
    
    foreign_keys['HasMaximumTargetCumulatedQuantity'] = 'iedm_app:CumulatedQuantityList'
    
    foreign_keys['HasMinimumCumulatedQuantity'] = 'iedm_app:CumulatedQuantityList'
    
    foreign_keys['HasSample'] = 'iedm_app:IrradiationExperimentObjectList'
    

    if request.method == 'POST':
        form = DUTIrradiationForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.DUTIrradiation(instance.name)
            onto.save()
            

            
            if DUTIrradiation._meta.get_field('dateTimeEnd').get_internal_type()  == 'IntegerField':
                onto_instance.dateTimeEnd=int(form.cleaned_data['dateTimeEnd'])
            elif DUTIrradiation._meta.get_field('dateTimeEnd').get_internal_type()  == 'DecimalField':
                onto_instance.dateTimeEnd=float(form.cleaned_data['dateTimeEnd'])
            else:
                onto_instance.dateTimeEnd=str(form.cleaned_data['dateTimeEnd'])
            
            if DUTIrradiation._meta.get_field('dateTimeStart').get_internal_type()  == 'IntegerField':
                onto_instance.dateTimeStart=int(form.cleaned_data['dateTimeStart'])
            elif DUTIrradiation._meta.get_field('dateTimeStart').get_internal_type()  == 'DecimalField':
                onto_instance.dateTimeStart=float(form.cleaned_data['dateTimeStart'])
            else:
                onto_instance.dateTimeStart=str(form.cleaned_data['dateTimeStart'])
            
            if DUTIrradiation._meta.get_field('radiationField').get_internal_type()  == 'IntegerField':
                onto_instance.radiationField=int(form.cleaned_data['radiationField'])
            elif DUTIrradiation._meta.get_field('radiationField').get_internal_type()  == 'DecimalField':
                onto_instance.radiationField=float(form.cleaned_data['radiationField'])
            else:
                onto_instance.radiationField=str(form.cleaned_data['radiationField'])
            
            onto.save()

            
            fkstr = 'HasCumulatedQuantity'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.CumulatedQuantity(fkinstanceinput[0])
            onto.save()
            onto_instance.hasCumulatedQuantity.append(fkinstance) 
            
            fkstr = 'HasDosimeter'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.IrradiationExperimentObject(fkinstanceinput[0])
            onto.save()
            onto_instance.hasDosimeter.append(fkinstance) 
            
            fkstr = 'HasInteractionLength'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.InteractionLength(fkinstanceinput[0])
            onto.save()
            onto_instance.hasInteractionLength.append(fkinstance) 
            
            fkstr = 'HasInteractionLengthOccupancy'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.InteractionLengthOccupancy(fkinstanceinput[0])
            onto.save()
            onto_instance.hasInteractionLengthOccupancy.append(fkinstance) 
            
            fkstr = 'HasMaximumTargetCumulatedQuantity'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.CumulatedQuantity(fkinstanceinput[0])
            onto.save()
            onto_instance.hasMaximumTargetCumulatedQuantity.append(fkinstance) 
            
            fkstr = 'HasMinimumCumulatedQuantity'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.CumulatedQuantity(fkinstanceinput[0])
            onto.save()
            onto_instance.hasMinimumCumulatedQuantity.append(fkinstance) 
            
            fkstr = 'HasSample'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.IrradiationExperimentObject(fkinstanceinput[0])
            onto.save()
            onto_instance.hasSample.append(fkinstance) 
            
            onto.save()
            link = "/iedm_app/DUTIrradiation/list/"
            return HttpResponseRedirect(link)
    else:
        form = DUTIrradiationForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "DUTIrradiationCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def SampleCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Sample"]

    
    foreign_keys['CompositionDefinedBy'] = 'iedm_app:LayerTableList'
    

    if request.method == 'POST':
        form = SampleForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Sample(instance.name)
            onto.save()
            

            
            onto.save()

            
            fkstr = 'CompositionDefinedBy'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.LayerTable(fkinstanceinput[0])
            onto.save()
            onto_instance.compositionDefinedBy.append(fkinstance) 
            
            onto.save()
            link = "/iedm_app/Sample/list/"
            return HttpResponseRedirect(link)
    else:
        form = SampleForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "SampleCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def DUTCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["DUT"]

    

    if request.method == 'POST':
        form = DUTForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.DUT(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/DUT/list/"
            return HttpResponseRedirect(link)
    else:
        form = DUTForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "DUTCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def LayerTableCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["LayerTable"]

    
    foreign_keys['HasEntries'] = 'iedm_app:LayerEntryList'
    

    if request.method == 'POST':
        form = LayerTableForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.LayerTable(instance.name)
            onto.save()
            

            
            onto.save()

            
            fkstr = 'HasEntries'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.LayerEntry(fkinstanceinput[0])
            onto.save()
            onto_instance.hasEntries.append(fkinstance) 
            
            onto.save()
            link = "/iedm_app/LayerTable/list/"
            return HttpResponseRedirect(link)
    else:
        form = LayerTableForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "LayerTableCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def IrradiationExperimentCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["IrradiationExperiment"]

    
    foreign_keys['HasPart'] = 'iedm_app:RequirementsList'
    
    foreign_keys['HasPart'] = 'iedm_app:DUTIrradiationList'
    
    foreign_keys['HasPart'] = 'iedm_app:MonitoringSystemList'
    
    foreign_keys['CreatedBy'] = 'iedm_app:UserList'
    
    foreign_keys['HasFieldOfStudy'] = 'iedm_app:DomainOfExperimentList'
    
    foreign_keys['HasFinalStorageArea'] = 'iedm_app:StorageAreaList'
    
    foreign_keys['IsCoordinatedBy'] = 'iedm_app:IrradiationFacilityCoordinatorList'
    
    foreign_keys['IsOperatedBy'] = 'iedm_app:OperatorList'
    
    foreign_keys['IsPerformedIn'] = 'iedm_app:IrradiationFacilityList'
    
    foreign_keys['IsUsedBy'] = 'iedm_app:IrradiationFacilityUserList'
    
    foreign_keys['UpdatedBy'] = 'iedm_app:UserList'
    
    foreign_keys['HasResponsiblePerson'] = 'iedm_app:ResponsiblePersonList'
    

    if request.method == 'POST':
        form = IrradiationExperimentForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.IrradiationExperiment(instance.name)
            onto.save()
            

            
            if IrradiationExperiment._meta.get_field('availability').get_internal_type()  == 'IntegerField':
                onto_instance.availability=int(form.cleaned_data['availability'])
            elif IrradiationExperiment._meta.get_field('availability').get_internal_type()  == 'DecimalField':
                onto_instance.availability=float(form.cleaned_data['availability'])
            else:
                onto_instance.availability=str(form.cleaned_data['availability'])
            
            onto.save()

            
            fkstr = 'HasPart'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Requirements(fkinstanceinput[0])
            onto.save()
            onto_instance.hasPart.append(fkinstance) 
            
            fkstr = 'HasPart'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.DUTIrradiation(fkinstanceinput[0])
            onto.save()
            onto_instance.hasPart.append(fkinstance) 
            
            fkstr = 'HasPart'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.MonitoringSystem(fkinstanceinput[0])
            onto.save()
            onto_instance.hasPart.append(fkinstance) 
            
            fkstr = 'CreatedBy'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.User(fkinstanceinput[0])
            onto.save()
            onto_instance.createdBy.append(fkinstance) 
            
            fkstr = 'HasFieldOfStudy'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.DomainOfExperiment(fkinstanceinput[0])
            onto.save()
            onto_instance.hasFieldOfStudy.append(fkinstance) 
            
            fkstr = 'HasFinalStorageArea'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.StorageArea(fkinstanceinput[0])
            onto.save()
            onto_instance.hasFinalStorageArea.append(fkinstance) 
            
            fkstr = 'IsCoordinatedBy'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.IrradiationFacilityCoordinator(fkinstanceinput[0])
            onto.save()
            onto_instance.isCoordinatedBy.append(fkinstance) 
            
            fkstr = 'IsOperatedBy'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Operator(fkinstanceinput[0])
            onto.save()
            onto_instance.isOperatedBy.append(fkinstance) 
            
            fkstr = 'IsPerformedIn'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.IrradiationFacility(fkinstanceinput[0])
            onto.save()
            onto_instance.isPerformedIn.append(fkinstance) 
            
            fkstr = 'IsUsedBy'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.IrradiationFacilityUser(fkinstanceinput[0])
            onto.save()
            onto_instance.isUsedBy.append(fkinstance) 
            
            fkstr = 'UpdatedBy'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.User(fkinstanceinput[0])
            onto.save()
            onto_instance.updatedBy.append(fkinstance) 
            
            fkstr = 'HasResponsiblePerson'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.ResponsiblePerson(fkinstanceinput[0])
            onto.save()
            onto_instance.hasResponsiblePerson.append(fkinstance) 
            
            onto.save()
            link = "/iedm_app/IrradiationExperiment/list/"
            return HttpResponseRedirect(link)
    else:
        form = IrradiationExperimentForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "IrradiationExperimentCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def LayerCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Layer"]

    
    foreign_keys['IsMadeOf'] = 'iedm_app:CompoundList'
    

    if request.method == 'POST':
        form = LayerForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Layer(instance.name)
            onto.save()
            

            
            if Layer._meta.get_field('length').get_internal_type()  == 'IntegerField':
                onto_instance.length=int(form.cleaned_data['length'])
            elif Layer._meta.get_field('length').get_internal_type()  == 'DecimalField':
                onto_instance.length=float(form.cleaned_data['length'])
            else:
                onto_instance.length=str(form.cleaned_data['length'])
            
            onto.save()

            
            fkstr = 'IsMadeOf'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Compound(fkinstanceinput[0])
            onto.save()
            onto_instance.isMadeOf.append(fkinstance) 
            
            onto.save()
            link = "/iedm_app/Layer/list/"
            return HttpResponseRedirect(link)
    else:
        form = LayerForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "LayerCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def CompoundCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Compound"]

    
    foreign_keys['CompositionDefinedBy'] = 'iedm_app:CompoundWeightFractionTableList'
    

    if request.method == 'POST':
        form = CompoundForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Compound(instance.name)
            onto.save()
            

            
            if Compound._meta.get_field('density').get_internal_type()  == 'IntegerField':
                onto_instance.density=int(form.cleaned_data['density'])
            elif Compound._meta.get_field('density').get_internal_type()  == 'DecimalField':
                onto_instance.density=float(form.cleaned_data['density'])
            else:
                onto_instance.density=str(form.cleaned_data['density'])
            
            onto.save()

            
            fkstr = 'CompositionDefinedBy'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.CompoundWeightFractionTable(fkinstanceinput[0])
            onto.save()
            onto_instance.compositionDefinedBy.append(fkinstance) 
            
            onto.save()
            link = "/iedm_app/Compound/list/"
            return HttpResponseRedirect(link)
    else:
        form = CompoundForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "CompoundCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def CompoundWeightFractionTableCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["CompoundWeightFractionTable"]

    
    foreign_keys['HasEntries'] = 'iedm_app:CompoundWeightFractionEntryList'
    

    if request.method == 'POST':
        form = CompoundWeightFractionTableForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.CompoundWeightFractionTable(instance.name)
            onto.save()
            

            
            onto.save()

            
            fkstr = 'HasEntries'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.CompoundWeightFractionEntry(fkinstanceinput[0])
            onto.save()
            onto_instance.hasEntries.append(fkinstance) 
            
            onto.save()
            link = "/iedm_app/CompoundWeightFractionTable/list/"
            return HttpResponseRedirect(link)
    else:
        form = CompoundWeightFractionTableForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "CompoundWeightFractionTableCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def IrradiationFacilityCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["IrradiationFacility"]

    
    foreign_keys['IsManagedBy'] = 'iedm_app:IrradiationFacilityManagerList'
    

    if request.method == 'POST':
        form = IrradiationFacilityForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.IrradiationFacility(instance.name)
            onto.save()
            

            
            if IrradiationFacility._meta.get_field('name').get_internal_type()  == 'IntegerField':
                onto_instance.name=int(form.cleaned_data['name'])
            elif IrradiationFacility._meta.get_field('name').get_internal_type()  == 'DecimalField':
                onto_instance.name=float(form.cleaned_data['name'])
            else:
                onto_instance.name=str(form.cleaned_data['name'])
            
            onto.save()

            
            fkstr = 'IsManagedBy'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.IrradiationFacilityManager(fkinstanceinput[0])
            onto.save()
            onto_instance.isManagedBy.append(fkinstance) 
            
            onto.save()
            link = "/iedm_app/IrradiationFacility/list/"
            return HttpResponseRedirect(link)
    else:
        form = IrradiationFacilityForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "IrradiationFacilityCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def PhysicalCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Physical"]

    

    if request.method == 'POST':
        form = PhysicalForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Physical(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/Physical/list/"
            return HttpResponseRedirect(link)
    else:
        form = PhysicalForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "PhysicalCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def IrradiationFacilityUserCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["IrradiationFacilityUser"]

    

    if request.method == 'POST':
        form = IrradiationFacilityUserForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.IrradiationFacilityUser(instance.name)
            onto.save()
            

            
            if IrradiationFacilityUser._meta.get_field('name').get_internal_type()  == 'IntegerField':
                onto_instance.name=int(form.cleaned_data['name'])
            elif IrradiationFacilityUser._meta.get_field('name').get_internal_type()  == 'DecimalField':
                onto_instance.name=float(form.cleaned_data['name'])
            else:
                onto_instance.name=str(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()
            link = "/iedm_app/IrradiationFacilityUser/list/"
            return HttpResponseRedirect(link)
    else:
        form = IrradiationFacilityUserForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "IrradiationFacilityUserCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def PredicateCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Predicate"]

    

    if request.method == 'POST':
        form = PredicateForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Predicate(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/Predicate/list/"
            return HttpResponseRedirect(link)
    else:
        form = PredicateForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "PredicateCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def SentientAgentCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["SentientAgent"]

    

    if request.method == 'POST':
        form = SentientAgentForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.SentientAgent(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/SentientAgent/list/"
            return HttpResponseRedirect(link)
    else:
        form = SentientAgentForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "SentientAgentCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def AgentCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Agent"]

    

    if request.method == 'POST':
        form = AgentForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Agent(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/Agent/list/"
            return HttpResponseRedirect(link)
    else:
        form = AgentForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "AgentCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def CompoundWeightFractionEntryCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["CompoundWeightFractionEntry"]

    
    foreign_keys['HasArgument'] = 'iedm_app:ElementList'
    

    if request.method == 'POST':
        form = CompoundWeightFractionEntryForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.CompoundWeightFractionEntry(instance.name)
            onto.save()
            

            
            if CompoundWeightFractionEntry._meta.get_field('hasValue').get_internal_type()  == 'IntegerField':
                onto_instance.hasValue=int(form.cleaned_data['hasValue'])
            elif CompoundWeightFractionEntry._meta.get_field('hasValue').get_internal_type()  == 'DecimalField':
                onto_instance.hasValue=float(form.cleaned_data['hasValue'])
            else:
                onto_instance.hasValue=str(form.cleaned_data['hasValue'])
            
            onto.save()

            
            fkstr = 'HasArgument'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Element(fkinstanceinput[0])
            onto.save()
            onto_instance.hasArgument.append(fkinstance) 
            
            onto.save()
            link = "/iedm_app/CompoundWeightFractionEntry/list/"
            return HttpResponseRedirect(link)
    else:
        form = CompoundWeightFractionEntryForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "CompoundWeightFractionEntryCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ElementCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Element"]

    

    if request.method == 'POST':
        form = ElementForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Element(instance.name)
            onto.save()
            

            
            if Element._meta.get_field('name').get_internal_type()  == 'IntegerField':
                onto_instance.name=int(form.cleaned_data['name'])
            elif Element._meta.get_field('name').get_internal_type()  == 'DecimalField':
                onto_instance.name=float(form.cleaned_data['name'])
            else:
                onto_instance.name=str(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()
            link = "/iedm_app/Element/list/"
            return HttpResponseRedirect(link)
    else:
        form = ElementForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ElementCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def UnaryFunctionEntryCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["UnaryFunctionEntry"]

    

    if request.method == 'POST':
        form = UnaryFunctionEntryForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.UnaryFunctionEntry(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/UnaryFunctionEntry/list/"
            return HttpResponseRedirect(link)
    else:
        form = UnaryFunctionEntryForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "UnaryFunctionEntryCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def CumulatedQuantityCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["CumulatedQuantity"]

    

    if request.method == 'POST':
        form = CumulatedQuantityForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.CumulatedQuantity(instance.name)
            onto.save()
            

            
            if CumulatedQuantity._meta.get_field('hasValue').get_internal_type()  == 'IntegerField':
                onto_instance.hasValue=int(form.cleaned_data['hasValue'])
            elif CumulatedQuantity._meta.get_field('hasValue').get_internal_type()  == 'DecimalField':
                onto_instance.hasValue=float(form.cleaned_data['hasValue'])
            else:
                onto_instance.hasValue=str(form.cleaned_data['hasValue'])
            
            onto.save()

            
            onto.save()
            link = "/iedm_app/CumulatedQuantity/list/"
            return HttpResponseRedirect(link)
    else:
        form = CumulatedQuantityForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "CumulatedQuantityCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def IrradiationExperimentObjectCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["IrradiationExperimentObject"]

    
    foreign_keys['UpdatedBy'] = 'iedm_app:UserList'
    
    foreign_keys['CreatedBy'] = 'iedm_app:UserList'
    

    if request.method == 'POST':
        form = IrradiationExperimentObjectForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.IrradiationExperimentObject(instance.name)
            onto.save()
            

            
            if IrradiationExperimentObject._meta.get_field('height').get_internal_type()  == 'IntegerField':
                onto_instance.height=int(form.cleaned_data['height'])
            elif IrradiationExperimentObject._meta.get_field('height').get_internal_type()  == 'DecimalField':
                onto_instance.height=float(form.cleaned_data['height'])
            else:
                onto_instance.height=str(form.cleaned_data['height'])
            
            if IrradiationExperimentObject._meta.get_field('identification').get_internal_type()  == 'IntegerField':
                onto_instance.identification=int(form.cleaned_data['identification'])
            elif IrradiationExperimentObject._meta.get_field('identification').get_internal_type()  == 'DecimalField':
                onto_instance.identification=float(form.cleaned_data['identification'])
            else:
                onto_instance.identification=str(form.cleaned_data['identification'])
            
            if IrradiationExperimentObject._meta.get_field('location').get_internal_type()  == 'IntegerField':
                onto_instance.location=int(form.cleaned_data['location'])
            elif IrradiationExperimentObject._meta.get_field('location').get_internal_type()  == 'DecimalField':
                onto_instance.location=float(form.cleaned_data['location'])
            else:
                onto_instance.location=str(form.cleaned_data['location'])
            
            if IrradiationExperimentObject._meta.get_field('weight').get_internal_type()  == 'IntegerField':
                onto_instance.weight=int(form.cleaned_data['weight'])
            elif IrradiationExperimentObject._meta.get_field('weight').get_internal_type()  == 'DecimalField':
                onto_instance.weight=float(form.cleaned_data['weight'])
            else:
                onto_instance.weight=str(form.cleaned_data['weight'])
            
            if IrradiationExperimentObject._meta.get_field('width').get_internal_type()  == 'IntegerField':
                onto_instance.width=int(form.cleaned_data['width'])
            elif IrradiationExperimentObject._meta.get_field('width').get_internal_type()  == 'DecimalField':
                onto_instance.width=float(form.cleaned_data['width'])
            else:
                onto_instance.width=str(form.cleaned_data['width'])
            
            onto.save()

            
            fkstr = 'UpdatedBy'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.User(fkinstanceinput[0])
            onto.save()
            onto_instance.updatedBy.append(fkinstance) 
            
            fkstr = 'CreatedBy'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.User(fkinstanceinput[0])
            onto.save()
            onto_instance.createdBy.append(fkinstance) 
            
            onto.save()
            link = "/iedm_app/IrradiationExperimentObject/list/"
            return HttpResponseRedirect(link)
    else:
        form = IrradiationExperimentObjectForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "IrradiationExperimentObjectCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def InteractionLengthCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["InteractionLength"]

    

    if request.method == 'POST':
        form = InteractionLengthForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.InteractionLength(instance.name)
            onto.save()
            

            
            if InteractionLength._meta.get_field('length').get_internal_type()  == 'IntegerField':
                onto_instance.length=int(form.cleaned_data['length'])
            elif InteractionLength._meta.get_field('length').get_internal_type()  == 'DecimalField':
                onto_instance.length=float(form.cleaned_data['length'])
            else:
                onto_instance.length=str(form.cleaned_data['length'])
            
            onto.save()

            
            onto.save()
            link = "/iedm_app/InteractionLength/list/"
            return HttpResponseRedirect(link)
    else:
        form = InteractionLengthForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "InteractionLengthCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def InteractionLengthOccupancyCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["InteractionLengthOccupancy"]

    

    if request.method == 'POST':
        form = InteractionLengthOccupancyForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.InteractionLengthOccupancy(instance.name)
            onto.save()
            

            
            if InteractionLengthOccupancy._meta.get_field('hasValue').get_internal_type()  == 'IntegerField':
                onto_instance.hasValue=int(form.cleaned_data['hasValue'])
            elif InteractionLengthOccupancy._meta.get_field('hasValue').get_internal_type()  == 'DecimalField':
                onto_instance.hasValue=float(form.cleaned_data['hasValue'])
            else:
                onto_instance.hasValue=str(form.cleaned_data['hasValue'])
            
            onto.save()

            
            onto.save()
            link = "/iedm_app/InteractionLengthOccupancy/list/"
            return HttpResponseRedirect(link)
    else:
        form = InteractionLengthOccupancyForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "InteractionLengthOccupancyCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def DosimetricQuantityCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["DosimetricQuantity"]

    

    if request.method == 'POST':
        form = DosimetricQuantityForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.DosimetricQuantity(instance.name)
            onto.save()
            

            
            if DosimetricQuantity._meta.get_field('hasValue').get_internal_type()  == 'IntegerField':
                onto_instance.hasValue=int(form.cleaned_data['hasValue'])
            elif DosimetricQuantity._meta.get_field('hasValue').get_internal_type()  == 'DecimalField':
                onto_instance.hasValue=float(form.cleaned_data['hasValue'])
            else:
                onto_instance.hasValue=str(form.cleaned_data['hasValue'])
            
            onto.save()

            
            onto.save()
            link = "/iedm_app/DosimetricQuantity/list/"
            return HttpResponseRedirect(link)
    else:
        form = DosimetricQuantityForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "DosimetricQuantityCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def DosimeterCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Dosimeter"]

    

    if request.method == 'POST':
        form = DosimeterForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Dosimeter(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/Dosimeter/list/"
            return HttpResponseRedirect(link)
    else:
        form = DosimeterForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "DosimeterCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def DomainOfExperimentCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["DomainOfExperiment"]

    

    if request.method == 'POST':
        form = DomainOfExperimentForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.DomainOfExperiment(instance.name)
            onto.save()
            

            
            if DomainOfExperiment._meta.get_field('name').get_internal_type()  == 'IntegerField':
                onto_instance.name=int(form.cleaned_data['name'])
            elif DomainOfExperiment._meta.get_field('name').get_internal_type()  == 'DecimalField':
                onto_instance.name=float(form.cleaned_data['name'])
            else:
                onto_instance.name=str(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()
            link = "/iedm_app/DomainOfExperiment/list/"
            return HttpResponseRedirect(link)
    else:
        form = DomainOfExperimentForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "DomainOfExperimentCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ParticlePhysicsExperimentCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ParticlePhysicsExperiment"]

    

    if request.method == 'POST':
        form = ParticlePhysicsExperimentForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ParticlePhysicsExperiment(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/ParticlePhysicsExperiment/list/"
            return HttpResponseRedirect(link)
    else:
        form = ParticlePhysicsExperimentForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ParticlePhysicsExperimentCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def RequirementsCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Requirements"]

    

    if request.method == 'POST':
        form = RequirementsForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Requirements(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/Requirements/list/"
            return HttpResponseRedirect(link)
    else:
        form = RequirementsForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "RequirementsCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def MonitoringSystemCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["MonitoringSystem"]

    

    if request.method == 'POST':
        form = MonitoringSystemForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.MonitoringSystem(instance.name)
            onto.save()
            

            
            if MonitoringSystem._meta.get_field('name').get_internal_type()  == 'IntegerField':
                onto_instance.name=int(form.cleaned_data['name'])
            elif MonitoringSystem._meta.get_field('name').get_internal_type()  == 'DecimalField':
                onto_instance.name=float(form.cleaned_data['name'])
            else:
                onto_instance.name=str(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()
            link = "/iedm_app/MonitoringSystem/list/"
            return HttpResponseRedirect(link)
    else:
        form = MonitoringSystemForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "MonitoringSystemCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def StorageAreaCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["StorageArea"]

    

    if request.method == 'POST':
        form = StorageAreaForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.StorageArea(instance.name)
            onto.save()
            

            
            if StorageArea._meta.get_field('name').get_internal_type()  == 'IntegerField':
                onto_instance.name=int(form.cleaned_data['name'])
            elif StorageArea._meta.get_field('name').get_internal_type()  == 'DecimalField':
                onto_instance.name=float(form.cleaned_data['name'])
            else:
                onto_instance.name=str(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()
            link = "/iedm_app/StorageArea/list/"
            return HttpResponseRedirect(link)
    else:
        form = StorageAreaForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "StorageAreaCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def FieldOfStudyCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["FieldOfStudy"]

    

    if request.method == 'POST':
        form = FieldOfStudyForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.FieldOfStudy(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/FieldOfStudy/list/"
            return HttpResponseRedirect(link)
    else:
        form = FieldOfStudyForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "FieldOfStudyCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def IrradiationFacilityPostionCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["IrradiationFacilityPostion"]

    

    if request.method == 'POST':
        form = IrradiationFacilityPostionForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.IrradiationFacilityPostion(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/IrradiationFacilityPostion/list/"
            return HttpResponseRedirect(link)
    else:
        form = IrradiationFacilityPostionForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "IrradiationFacilityPostionCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def LengthCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Length"]

    

    if request.method == 'POST':
        form = LengthForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Length(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/Length/list/"
            return HttpResponseRedirect(link)
    else:
        form = LengthForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "LengthCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def PercentageCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Percentage"]

    

    if request.method == 'POST':
        form = PercentageForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Percentage(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/Percentage/list/"
            return HttpResponseRedirect(link)
    else:
        form = PercentageForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "PercentageCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def LayerEntryCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["LayerEntry"]

    
    foreign_keys['HasEntries'] = 'iedm_app:LayerList'
    

    if request.method == 'POST':
        form = LayerEntryForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.LayerEntry(instance.name)
            onto.save()
            

            
            if LayerEntry._meta.get_field('ordinal').get_internal_type()  == 'IntegerField':
                onto_instance.ordinal=int(form.cleaned_data['ordinal'])
            elif LayerEntry._meta.get_field('ordinal').get_internal_type()  == 'DecimalField':
                onto_instance.ordinal=float(form.cleaned_data['ordinal'])
            else:
                onto_instance.ordinal=str(form.cleaned_data['ordinal'])
            
            onto.save()

            
            fkstr = 'HasEntries'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Layer(fkinstanceinput[0])
            onto.save()
            onto_instance.hasEntries.append(fkinstance) 
            
            onto.save()
            link = "/iedm_app/LayerEntry/list/"
            return HttpResponseRedirect(link)
    else:
        form = LayerEntryForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "LayerEntryCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def RelationCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Relation"]

    

    if request.method == 'POST':
        form = RelationForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Relation(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/Relation/list/"
            return HttpResponseRedirect(link)
    else:
        form = RelationForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "RelationCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def AbstractCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Abstract"]

    

    if request.method == 'POST':
        form = AbstractForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Abstract(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/Abstract/list/"
            return HttpResponseRedirect(link)
    else:
        form = AbstractForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "AbstractCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def RegionCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Region"]

    

    if request.method == 'POST':
        form = RegionForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Region(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/Region/list/"
            return HttpResponseRedirect(link)
    else:
        form = RegionForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "RegionCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def SubjectRoleCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["SubjectRole"]

    

    if request.method == 'POST':
        form = SubjectRoleForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.SubjectRole(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/SubjectRole/list/"
            return HttpResponseRedirect(link)
    else:
        form = SubjectRoleForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "SubjectRoleCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ActorRoleCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ActorRole"]

    

    if request.method == 'POST':
        form = ActorRoleForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ActorRole(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/ActorRole/list/"
            return HttpResponseRedirect(link)
    else:
        form = ActorRoleForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ActorRoleCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def AbsorbedDoseCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["AbsorbedDose"]

    

    if request.method == 'POST':
        form = AbsorbedDoseForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.AbsorbedDose(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/AbsorbedDose/list/"
            return HttpResponseRedirect(link)
    else:
        form = AbsorbedDoseForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "AbsorbedDoseCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def AbsorbedDoseInMediumCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["AbsorbedDoseInMedium"]

    

    if request.method == 'POST':
        form = AbsorbedDoseInMediumForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.AbsorbedDoseInMedium(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/AbsorbedDoseInMedium/list/"
            return HttpResponseRedirect(link)
    else:
        form = AbsorbedDoseInMediumForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "AbsorbedDoseInMediumCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def QuantityCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Quantity"]

    

    if request.method == 'POST':
        form = QuantityForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Quantity(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/Quantity/list/"
            return HttpResponseRedirect(link)
    else:
        form = QuantityForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "QuantityCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ActivityCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Activity"]

    

    if request.method == 'POST':
        form = ActivityForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Activity(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/Activity/list/"
            return HttpResponseRedirect(link)
    else:
        form = ActivityForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ActivityCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def DosimetricRateCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["DosimetricRate"]

    

    if request.method == 'POST':
        form = DosimetricRateForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.DosimetricRate(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/DosimetricRate/list/"
            return HttpResponseRedirect(link)
    else:
        form = DosimetricRateForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "DosimetricRateCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def AtomicMassCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["AtomicMass"]

    

    if request.method == 'POST':
        form = AtomicMassForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.AtomicMass(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/AtomicMass/list/"
            return HttpResponseRedirect(link)
    else:
        form = AtomicMassForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "AtomicMassCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def MassCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Mass"]

    

    if request.method == 'POST':
        form = MassForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Mass(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/Mass/list/"
            return HttpResponseRedirect(link)
    else:
        form = MassForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "MassCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def DensityCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Density"]

    

    if request.method == 'POST':
        form = DensityForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Density(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/Density/list/"
            return HttpResponseRedirect(link)
    else:
        form = DensityForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "DensityCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def EnergyCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Energy"]

    

    if request.method == 'POST':
        form = EnergyForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Energy(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/Energy/list/"
            return HttpResponseRedirect(link)
    else:
        form = EnergyForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "EnergyCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def HeightCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Height"]

    

    if request.method == 'POST':
        form = HeightForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Height(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/Height/list/"
            return HttpResponseRedirect(link)
    else:
        form = HeightForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "HeightCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def NumberCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Number"]

    

    if request.method == 'POST':
        form = NumberForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Number(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/Number/list/"
            return HttpResponseRedirect(link)
    else:
        form = NumberForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "NumberCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def RatioCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Ratio"]

    

    if request.method == 'POST':
        form = RatioForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Ratio(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/Ratio/list/"
            return HttpResponseRedirect(link)
    else:
        form = RatioForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "RatioCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def TemperatureCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Temperature"]

    

    if request.method == 'POST':
        form = TemperatureForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Temperature(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/Temperature/list/"
            return HttpResponseRedirect(link)
    else:
        form = TemperatureForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "TemperatureCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ThicknessCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Thickness"]

    

    if request.method == 'POST':
        form = ThicknessForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Thickness(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/Thickness/list/"
            return HttpResponseRedirect(link)
    else:
        form = ThicknessForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ThicknessCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def AbsorbedDoseInAirCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["AbsorbedDoseInAir"]

    

    if request.method == 'POST':
        form = AbsorbedDoseInAirForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.AbsorbedDoseInAir(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/AbsorbedDoseInAir/list/"
            return HttpResponseRedirect(link)
    else:
        form = AbsorbedDoseInAirForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "AbsorbedDoseInAirCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def AdminInfoIrradiationExperimentCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["AdminInfoIrradiationExperiment"]

    

    if request.method == 'POST':
        form = AdminInfoIrradiationExperimentForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.AdminInfoIrradiationExperiment(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/AdminInfoIrradiationExperiment/list/"
            return HttpResponseRedirect(link)
    else:
        form = AdminInfoIrradiationExperimentForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "AdminInfoIrradiationExperimentCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def AdminInfoExperimentCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["AdminInfoExperiment"]

    

    if request.method == 'POST':
        form = AdminInfoExperimentForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.AdminInfoExperiment(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/AdminInfoExperiment/list/"
            return HttpResponseRedirect(link)
    else:
        form = AdminInfoExperimentForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "AdminInfoExperimentCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ActionrelatedRoleCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ActionrelatedRole"]

    

    if request.method == 'POST':
        form = ActionrelatedRoleForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ActionrelatedRole(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/ActionrelatedRole/list/"
            return HttpResponseRedirect(link)
    else:
        form = ActionrelatedRoleForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ActionrelatedRoleCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def RoleCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Role"]

    

    if request.method == 'POST':
        form = RoleForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Role(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/Role/list/"
            return HttpResponseRedirect(link)
    else:
        form = RoleForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "RoleCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def AdministrativeInformationCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["AdministrativeInformation"]

    

    if request.method == 'POST':
        form = AdministrativeInformationForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.AdministrativeInformation(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/AdministrativeInformation/list/"
            return HttpResponseRedirect(link)
    else:
        form = AdministrativeInformationForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "AdministrativeInformationCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def PropositionCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Proposition"]

    

    if request.method == 'POST':
        form = PropositionForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Proposition(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/Proposition/list/"
            return HttpResponseRedirect(link)
    else:
        form = PropositionForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "PropositionCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def AdminInfoObjectOfExperimentCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["AdminInfoObjectOfExperiment"]

    

    if request.method == 'POST':
        form = AdminInfoObjectOfExperimentForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.AdminInfoObjectOfExperiment(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/AdminInfoObjectOfExperiment/list/"
            return HttpResponseRedirect(link)
    else:
        form = AdminInfoObjectOfExperimentForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "AdminInfoObjectOfExperimentCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def AdminInfoUserCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["AdminInfoUser"]

    

    if request.method == 'POST':
        form = AdminInfoUserForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.AdminInfoUser(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/AdminInfoUser/list/"
            return HttpResponseRedirect(link)
    else:
        form = AdminInfoUserForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "AdminInfoUserCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ClassificationCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Classification"]

    

    if request.method == 'POST':
        form = ClassificationForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Classification(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/Classification/list/"
            return HttpResponseRedirect(link)
    else:
        form = ClassificationForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ClassificationCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ScientificTaskCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ScientificTask"]

    

    if request.method == 'POST':
        form = ScientificTaskForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ScientificTask(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/ScientificTask/list/"
            return HttpResponseRedirect(link)
    else:
        form = ScientificTaskForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ScientificTaskCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ClassificationByDomainCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ClassificationByDomain"]

    

    if request.method == 'POST':
        form = ClassificationByDomainForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ClassificationByDomain(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/ClassificationByDomain/list/"
            return HttpResponseRedirect(link)
    else:
        form = ClassificationByDomainForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ClassificationByDomainCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ClassificationOfExperimentsCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ClassificationOfExperiments"]

    

    if request.method == 'POST':
        form = ClassificationOfExperimentsForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ClassificationOfExperiments(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/ClassificationOfExperiments/list/"
            return HttpResponseRedirect(link)
    else:
        form = ClassificationOfExperimentsForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ClassificationOfExperimentsCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def CorpuscularObjectCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["CorpuscularObject"]

    

    if request.method == 'POST':
        form = CorpuscularObjectForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.CorpuscularObject(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/CorpuscularObject/list/"
            return HttpResponseRedirect(link)
    else:
        form = CorpuscularObjectForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "CorpuscularObjectCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def SelfConnectedObjectCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["SelfConnectedObject"]

    

    if request.method == 'POST':
        form = SelfConnectedObjectForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.SelfConnectedObject(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/SelfConnectedObject/list/"
            return HttpResponseRedirect(link)
    else:
        form = SelfConnectedObjectForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "SelfConnectedObjectCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ExperimentalRequirementsCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ExperimentalRequirements"]

    

    if request.method == 'POST':
        form = ExperimentalRequirementsForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ExperimentalRequirements(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/ExperimentalRequirements/list/"
            return HttpResponseRedirect(link)
    else:
        form = ExperimentalRequirementsForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ExperimentalRequirementsCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def AdminInfoIrradiationExperimentUserCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["AdminInfoIrradiationExperimentUser"]

    

    if request.method == 'POST':
        form = AdminInfoIrradiationExperimentUserForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.AdminInfoIrradiationExperimentUser(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/AdminInfoIrradiationExperimentUser/list/"
            return HttpResponseRedirect(link)
    else:
        form = AdminInfoIrradiationExperimentUserForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "AdminInfoIrradiationExperimentUserCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def AdminInfoObjectOfIrradiationCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["AdminInfoObjectOfIrradiation"]

    

    if request.method == 'POST':
        form = AdminInfoObjectOfIrradiationForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.AdminInfoObjectOfIrradiation(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/AdminInfoObjectOfIrradiation/list/"
            return HttpResponseRedirect(link)
    else:
        form = AdminInfoObjectOfIrradiationForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "AdminInfoObjectOfIrradiationCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def AdminInfoObjectOfIrradiationExperimentCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["AdminInfoObjectOfIrradiationExperiment"]

    

    if request.method == 'POST':
        form = AdminInfoObjectOfIrradiationExperimentForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.AdminInfoObjectOfIrradiationExperiment(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/AdminInfoObjectOfIrradiationExperiment/list/"
            return HttpResponseRedirect(link)
    else:
        form = AdminInfoObjectOfIrradiationExperimentForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "AdminInfoObjectOfIrradiationExperimentCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def AtomicNumberCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["AtomicNumber"]

    

    if request.method == 'POST':
        form = AtomicNumberForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.AtomicNumber(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/AtomicNumber/list/"
            return HttpResponseRedirect(link)
    else:
        form = AtomicNumberForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "AtomicNumberCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def CollaborationRequirementsCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["CollaborationRequirements"]

    

    if request.method == 'POST':
        form = CollaborationRequirementsForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.CollaborationRequirements(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/CollaborationRequirements/list/"
            return HttpResponseRedirect(link)
    else:
        form = CollaborationRequirementsForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "CollaborationRequirementsCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ControlRoomCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ControlRoom"]

    

    if request.method == 'POST':
        form = ControlRoomForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ControlRoom(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/ControlRoom/list/"
            return HttpResponseRedirect(link)
    else:
        form = ControlRoomForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ControlRoomCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ControlSystemCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ControlSystem"]

    

    if request.method == 'POST':
        form = ControlSystemForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ControlSystem(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/ControlSystem/list/"
            return HttpResponseRedirect(link)
    else:
        form = ControlSystemForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ControlSystemCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def SystemCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["System"]

    

    if request.method == 'POST':
        form = SystemForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.System(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/System/list/"
            return HttpResponseRedirect(link)
    else:
        form = SystemForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "SystemCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def RadiationLengthCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["RadiationLength"]

    

    if request.method == 'POST':
        form = RadiationLengthForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.RadiationLength(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/RadiationLength/list/"
            return HttpResponseRedirect(link)
    else:
        form = RadiationLengthForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "RadiationLengthCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def RadiationLengthOccupancyCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["RadiationLengthOccupancy"]

    

    if request.method == 'POST':
        form = RadiationLengthOccupancyForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.RadiationLengthOccupancy(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/RadiationLengthOccupancy/list/"
            return HttpResponseRedirect(link)
    else:
        form = RadiationLengthOccupancyForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "RadiationLengthOccupancyCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def RadioprotectionRequirementsCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["RadioprotectionRequirements"]

    

    if request.method == 'POST':
        form = RadioprotectionRequirementsForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.RadioprotectionRequirements(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/RadioprotectionRequirements/list/"
            return HttpResponseRedirect(link)
    else:
        form = RadioprotectionRequirementsForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "RadioprotectionRequirementsCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def SingularFieldCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["SingularField"]

    

    if request.method == 'POST':
        form = SingularFieldForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.SingularField(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/SingularField/list/"
            return HttpResponseRedirect(link)
    else:
        form = SingularFieldForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "SingularFieldCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def RadiationFieldCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["RadiationField"]

    

    if request.method == 'POST':
        form = RadiationFieldForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.RadiationField(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/RadiationField/list/"
            return HttpResponseRedirect(link)
    else:
        form = RadiationFieldForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "RadiationFieldCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def TechnicalRequirementsCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["TechnicalRequirements"]

    

    if request.method == 'POST':
        form = TechnicalRequirementsForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.TechnicalRequirements(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/TechnicalRequirements/list/"
            return HttpResponseRedirect(link)
    else:
        form = TechnicalRequirementsForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "TechnicalRequirementsCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def IrradiationFacilityRequirementsCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["IrradiationFacilityRequirements"]

    

    if request.method == 'POST':
        form = IrradiationFacilityRequirementsForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.IrradiationFacilityRequirements(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/IrradiationFacilityRequirements/list/"
            return HttpResponseRedirect(link)
    else:
        form = IrradiationFacilityRequirementsForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "IrradiationFacilityRequirementsCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def FluenceCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Fluence"]

    

    if request.method == 'POST':
        form = FluenceForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Fluence(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/Fluence/list/"
            return HttpResponseRedirect(link)
    else:
        form = FluenceForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "FluenceCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def NuclearCollisionLengthCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["NuclearCollisionLength"]

    

    if request.method == 'POST':
        form = NuclearCollisionLengthForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.NuclearCollisionLength(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/NuclearCollisionLength/list/"
            return HttpResponseRedirect(link)
    else:
        form = NuclearCollisionLengthForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "NuclearCollisionLengthCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def NuclearInteractionLengthCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["NuclearInteractionLength"]

    

    if request.method == 'POST':
        form = NuclearInteractionLengthForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.NuclearInteractionLength(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/NuclearInteractionLength/list/"
            return HttpResponseRedirect(link)
    else:
        form = NuclearInteractionLengthForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "NuclearInteractionLengthCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def DataManagementSystemCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["DataManagementSystem"]

    

    if request.method == 'POST':
        form = DataManagementSystemForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.DataManagementSystem(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/DataManagementSystem/list/"
            return HttpResponseRedirect(link)
    else:
        form = DataManagementSystemForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "DataManagementSystemCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def DoseRateCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["DoseRate"]

    

    if request.method == 'POST':
        form = DoseRateForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.DoseRate(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/DoseRate/list/"
            return HttpResponseRedirect(link)
    else:
        form = DoseRateForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "DoseRateCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ExternalPositionCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ExternalPosition"]

    

    if request.method == 'POST':
        form = ExternalPositionForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ExternalPosition(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/ExternalPosition/list/"
            return HttpResponseRedirect(link)
    else:
        form = ExternalPositionForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ExternalPositionCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def FluenceRateCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["FluenceRate"]

    

    if request.method == 'POST':
        form = FluenceRateForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.FluenceRate(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/FluenceRate/list/"
            return HttpResponseRedirect(link)
    else:
        form = FluenceRateForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "FluenceRateCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def InteractionLengthEntryCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["InteractionLengthEntry"]

    

    if request.method == 'POST':
        form = InteractionLengthEntryForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.InteractionLengthEntry(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/InteractionLengthEntry/list/"
            return HttpResponseRedirect(link)
    else:
        form = InteractionLengthEntryForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "InteractionLengthEntryCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def InteractionLengthTableCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["InteractionLengthTable"]

    

    if request.method == 'POST':
        form = InteractionLengthTableForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.InteractionLengthTable(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/InteractionLengthTable/list/"
            return HttpResponseRedirect(link)
    else:
        form = InteractionLengthTableForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "InteractionLengthTableCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def IrradiationPositionCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["IrradiationPosition"]

    

    if request.method == 'POST':
        form = IrradiationPositionForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.IrradiationPosition(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/IrradiationPosition/list/"
            return HttpResponseRedirect(link)
    else:
        form = IrradiationPositionForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "IrradiationPositionCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def IrradiationZoneCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["IrradiationZone"]

    

    if request.method == 'POST':
        form = IrradiationZoneForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.IrradiationZone(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/IrradiationZone/list/"
            return HttpResponseRedirect(link)
    else:
        form = IrradiationZoneForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "IrradiationZoneCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def LaboratoryCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Laboratory"]

    

    if request.method == 'POST':
        form = LaboratoryForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Laboratory(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/Laboratory/list/"
            return HttpResponseRedirect(link)
    else:
        form = LaboratoryForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "LaboratoryCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def LegalRequirementsCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["LegalRequirements"]

    

    if request.method == 'POST':
        form = LegalRequirementsForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.LegalRequirements(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/LegalRequirements/list/"
            return HttpResponseRedirect(link)
    else:
        form = LegalRequirementsForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "LegalRequirementsCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def MinimumIonisationEntryCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["MinimumIonisationEntry"]

    

    if request.method == 'POST':
        form = MinimumIonisationEntryForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.MinimumIonisationEntry(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/MinimumIonisationEntry/list/"
            return HttpResponseRedirect(link)
    else:
        form = MinimumIonisationEntryForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "MinimumIonisationEntryCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def MinimumIonisationTableCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["MinimumIonisationTable"]

    

    if request.method == 'POST':
        form = MinimumIonisationTableForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.MinimumIonisationTable(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/MinimumIonisationTable/list/"
            return HttpResponseRedirect(link)
    else:
        form = MinimumIonisationTableForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "MinimumIonisationTableCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def MixedFieldCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["MixedField"]

    

    if request.method == 'POST':
        form = MixedFieldForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.MixedField(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/MixedField/list/"
            return HttpResponseRedirect(link)
    else:
        form = MixedFieldForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "MixedFieldCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def NuclearCollisionLengthOccupancyCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["NuclearCollisionLengthOccupancy"]

    

    if request.method == 'POST':
        form = NuclearCollisionLengthOccupancyForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.NuclearCollisionLengthOccupancy(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/NuclearCollisionLengthOccupancy/list/"
            return HttpResponseRedirect(link)
    else:
        form = NuclearCollisionLengthOccupancyForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "NuclearCollisionLengthOccupancyCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def NuclearInteractionLengthOccupancyCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["NuclearInteractionLengthOccupancy"]

    

    if request.method == 'POST':
        form = NuclearInteractionLengthOccupancyForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.NuclearInteractionLengthOccupancy(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/NuclearInteractionLengthOccupancy/list/"
            return HttpResponseRedirect(link)
    else:
        form = NuclearInteractionLengthOccupancyForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "NuclearInteractionLengthOccupancyCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ParticleCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Particle"]

    

    if request.method == 'POST':
        form = ParticleForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Particle(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/iedm_app/Particle/list/"
            return HttpResponseRedirect(link)
    else:
        form = ParticleForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ParticleCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})
