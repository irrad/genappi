from django.db import models

class Thing(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Object(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class UnaryFunction(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class User(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class IrradiationFacilityRole(models.Model):
    
    name=models.CharField(max_length = 50)
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class IrradiationFacilityCoordinator(models.Model):
    
    name=models.CharField(max_length = 50)
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class IrradiationFacilityManager(models.Model):
    
    name=models.CharField(max_length = 50)
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Operator(models.Model):
    
    name=models.CharField(max_length = 50)
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ResponsiblePerson(models.Model):
    
    name=models.CharField(max_length = 50)
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class DUTIrradiation(models.Model):
    
    hasCumulatedQuantity=models.ManyToManyField("CumulatedQuantity", related_name = "%(class)s_hasCumulatedQuantity")
    
    hasDosimeter=models.ManyToManyField("IrradiationExperimentObject", related_name = "%(class)s_hasDosimeter")
    
    hasInteractionLength=models.ManyToManyField("InteractionLength", related_name = "%(class)s_hasInteractionLength")
    
    hasInteractionLengthOccupancy=models.ManyToManyField("InteractionLengthOccupancy", related_name = "%(class)s_hasInteractionLengthOccupancy")
    
    hasMaximumTargetCumulatedQuantity=models.ManyToManyField("CumulatedQuantity", related_name = "%(class)s_hasMaximumTargetCumulatedQuantity")
    
    hasMinimumCumulatedQuantity=models.ManyToManyField("CumulatedQuantity", related_name = "%(class)s_hasMinimumCumulatedQuantity")
    
    hasSample=models.ManyToManyField("IrradiationExperimentObject", related_name = "%(class)s_hasSample")
    
    dateTimeEnd=models.DateTimeField()
    
    dateTimeStart=models.DateTimeField()
    
    radiationField=models.CharField(max_length = 50)
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Sample(models.Model):
    
    compositionDefinedBy=models.ForeignKey("LayerTable", related_name = "%(class)s_compositionDefinedBy")
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class DUT(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class LayerTable(models.Model):
    
    hasEntries=models.ManyToManyField("LayerEntry", related_name = "%(class)s_hasEntries")
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class IrradiationExperiment(models.Model):
    
    hasPart=models.ManyToManyField("Requirements", related_name = "%(class)s_hasPart")
    
    hasPart=models.ManyToManyField("DUTIrradiation", related_name = "%(class)s_hasPart")
    
    hasPart=models.ManyToManyField("MonitoringSystem", related_name = "%(class)s_hasPart")
    
    createdBy=models.ManyToManyField("User", related_name = "%(class)s_createdBy")
    
    hasFieldOfStudy=models.ManyToManyField("DomainOfExperiment", related_name = "%(class)s_hasFieldOfStudy")
    
    hasFinalStorageArea=models.ManyToManyField("StorageArea", related_name = "%(class)s_hasFinalStorageArea")
    
    isCoordinatedBy=models.ManyToManyField("IrradiationFacilityCoordinator", related_name = "%(class)s_isCoordinatedBy")
    
    isOperatedBy=models.ManyToManyField("Operator", related_name = "%(class)s_isOperatedBy")
    
    isPerformedIn=models.ManyToManyField("IrradiationFacility", related_name = "%(class)s_isPerformedIn")
    
    isUsedBy=models.ManyToManyField("IrradiationFacilityUser", related_name = "%(class)s_isUsedBy")
    
    updatedBy=models.ManyToManyField("User", related_name = "%(class)s_updatedBy")
    
    hasResponsiblePerson=models.ForeignKey("ResponsiblePerson", related_name = "%(class)s_hasResponsiblePerson")
    
    availability=models.DateTimeField()
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Layer(models.Model):
    
    isMadeOf=models.ForeignKey("Compound", related_name = "%(class)s_isMadeOf")
    
    length=models.DecimalField(max_digits=12,decimal_places=6)
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Compound(models.Model):
    
    compositionDefinedBy=models.ManyToManyField("CompoundWeightFractionTable", related_name = "%(class)s_compositionDefinedBy")
    
    density=models.DecimalField(max_digits=12,decimal_places=6)
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class CompoundWeightFractionTable(models.Model):
    
    hasEntries=models.ManyToManyField("CompoundWeightFractionEntry", related_name = "%(class)s_hasEntries")
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class IrradiationFacility(models.Model):
    
    isManagedBy=models.ManyToManyField("IrradiationFacilityManager", related_name = "%(class)s_isManagedBy")
    
    name=models.CharField(max_length = 50)
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Physical(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class IrradiationFacilityUser(models.Model):
    
    name=models.CharField(max_length = 50)
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Predicate(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class SentientAgent(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Agent(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class CompoundWeightFractionEntry(models.Model):
    
    hasArgument=models.ForeignKey("Element", related_name = "%(class)s_hasArgument")
    
    hasValue=models.DecimalField(max_digits=12,decimal_places=6)
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Element(models.Model):
    
    name=models.CharField(max_length = 50)
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class UnaryFunctionEntry(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class CumulatedQuantity(models.Model):
    
    hasValue=models.DecimalField(max_digits=12,decimal_places=6)
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class IrradiationExperimentObject(models.Model):
    
    updatedBy=models.ManyToManyField("User", related_name = "%(class)s_updatedBy")
    
    createdBy=models.ForeignKey("User", related_name = "%(class)s_createdBy")
    
    height=models.DecimalField(max_digits=12,decimal_places=6)
    
    identification=models.CharField(max_length = 50)
    
    location=models.CharField(max_length = 50)
    
    weight=models.DecimalField(max_digits=12,decimal_places=6)
    
    width=models.DecimalField(max_digits=12,decimal_places=6)
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class InteractionLength(models.Model):
    
    length=models.DecimalField(max_digits=12,decimal_places=6)
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class InteractionLengthOccupancy(models.Model):
    
    hasValue=models.DecimalField(max_digits=12,decimal_places=6)
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class DosimetricQuantity(models.Model):
    
    hasValue=models.DecimalField(max_digits=12,decimal_places=6)
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Dosimeter(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class DomainOfExperiment(models.Model):
    
    name=models.CharField(max_length = 50)
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ParticlePhysicsExperiment(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Requirements(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class MonitoringSystem(models.Model):
    
    name=models.CharField(max_length = 50)
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class StorageArea(models.Model):
    
    name=models.CharField(max_length = 50)
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class FieldOfStudy(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class IrradiationFacilityPostion(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Length(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Percentage(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class LayerEntry(models.Model):
    
    hasEntries=models.ForeignKey("Layer", related_name = "%(class)s_hasEntries")
    
    ordinal=models.IntegerField()
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Relation(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Abstract(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Region(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class SubjectRole(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ActorRole(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class AbsorbedDose(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class AbsorbedDoseInMedium(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Quantity(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Activity(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class DosimetricRate(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class AtomicMass(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Mass(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Density(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Energy(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Height(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Number(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Ratio(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Temperature(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Thickness(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class AbsorbedDoseInAir(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class AdminInfoIrradiationExperiment(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class AdminInfoExperiment(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ActionrelatedRole(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Role(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class AdministrativeInformation(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Proposition(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class AdminInfoObjectOfExperiment(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class AdminInfoUser(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Classification(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ScientificTask(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ClassificationByDomain(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ClassificationOfExperiments(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class CorpuscularObject(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class SelfConnectedObject(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ExperimentalRequirements(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class AdminInfoIrradiationExperimentUser(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class AdminInfoObjectOfIrradiation(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class AdminInfoObjectOfIrradiationExperiment(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class AtomicNumber(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class CollaborationRequirements(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ControlRoom(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ControlSystem(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class System(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class RadiationLength(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class RadiationLengthOccupancy(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class RadioprotectionRequirements(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class SingularField(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class RadiationField(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class TechnicalRequirements(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class IrradiationFacilityRequirements(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Fluence(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class NuclearCollisionLength(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class NuclearInteractionLength(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class DataManagementSystem(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class DoseRate(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ExternalPosition(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class FluenceRate(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class InteractionLengthEntry(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class InteractionLengthTable(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class IrradiationPosition(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class IrradiationZone(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Laboratory(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class LegalRequirements(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class MinimumIonisationEntry(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class MinimumIonisationTable(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class MixedField(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class NuclearCollisionLengthOccupancy(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class NuclearInteractionLengthOccupancy(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Particle(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name
