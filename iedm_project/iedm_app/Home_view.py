from django.db import models
from django.template.loader import render_to_string
from .models import *
from .forms import *
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseNotFound
from django.template import loader
from django.core.urlresolvers import reverse
import datetime
from .forms import *
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.core.files.storage import FileSystemStorage
from django.utils.safestring import mark_safe

def vowl(request):
    context = {}
    return render(request, "do_visualisation.html", context)

def HomeView(request):
    classes = []
    
    classes.append(str(Thing))
    
    classes.append(str(Object))
    
    classes.append(str(UnaryFunction))
    
    classes.append(str(User))
    
    classes.append(str(IrradiationFacilityRole))
    
    classes.append(str(IrradiationFacilityCoordinator))
    
    classes.append(str(IrradiationFacilityManager))
    
    classes.append(str(Operator))
    
    classes.append(str(ResponsiblePerson))
    
    classes.append(str(DUTIrradiation))
    
    classes.append(str(Sample))
    
    classes.append(str(DUT))
    
    classes.append(str(LayerTable))
    
    classes.append(str(IrradiationExperiment))
    
    classes.append(str(Layer))
    
    classes.append(str(Compound))
    
    classes.append(str(CompoundWeightFractionTable))
    
    classes.append(str(IrradiationFacility))
    
    classes.append(str(Physical))
    
    classes.append(str(IrradiationFacilityUser))
    
    classes.append(str(Predicate))
    
    classes.append(str(SentientAgent))
    
    classes.append(str(Agent))
    
    classes.append(str(CompoundWeightFractionEntry))
    
    classes.append(str(Element))
    
    classes.append(str(UnaryFunctionEntry))
    
    classes.append(str(CumulatedQuantity))
    
    classes.append(str(IrradiationExperimentObject))
    
    classes.append(str(InteractionLength))
    
    classes.append(str(InteractionLengthOccupancy))
    
    classes.append(str(DosimetricQuantity))
    
    classes.append(str(Dosimeter))
    
    classes.append(str(DomainOfExperiment))
    
    classes.append(str(ParticlePhysicsExperiment))
    
    classes.append(str(Requirements))
    
    classes.append(str(MonitoringSystem))
    
    classes.append(str(StorageArea))
    
    classes.append(str(FieldOfStudy))
    
    classes.append(str(IrradiationFacilityPostion))
    
    classes.append(str(Length))
    
    classes.append(str(Percentage))
    
    classes.append(str(LayerEntry))
    
    classes.append(str(Relation))
    
    classes.append(str(Abstract))
    
    classes.append(str(Region))
    
    classes.append(str(SubjectRole))
    
    classes.append(str(ActorRole))
    
    classes.append(str(AbsorbedDose))
    
    classes.append(str(AbsorbedDoseInMedium))
    
    classes.append(str(Quantity))
    
    classes.append(str(Activity))
    
    classes.append(str(DosimetricRate))
    
    classes.append(str(AtomicMass))
    
    classes.append(str(Mass))
    
    classes.append(str(Density))
    
    classes.append(str(Energy))
    
    classes.append(str(Height))
    
    classes.append(str(Number))
    
    classes.append(str(Ratio))
    
    classes.append(str(Temperature))
    
    classes.append(str(Thickness))
    
    classes.append(str(AbsorbedDoseInAir))
    
    classes.append(str(AdminInfoIrradiationExperiment))
    
    classes.append(str(AdminInfoExperiment))
    
    classes.append(str(ActionrelatedRole))
    
    classes.append(str(Role))
    
    classes.append(str(AdministrativeInformation))
    
    classes.append(str(Proposition))
    
    classes.append(str(AdminInfoObjectOfExperiment))
    
    classes.append(str(AdminInfoUser))
    
    classes.append(str(Classification))
    
    classes.append(str(ScientificTask))
    
    classes.append(str(ClassificationByDomain))
    
    classes.append(str(ClassificationOfExperiments))
    
    classes.append(str(CorpuscularObject))
    
    classes.append(str(SelfConnectedObject))
    
    classes.append(str(ExperimentalRequirements))
    
    classes.append(str(AdminInfoIrradiationExperimentUser))
    
    classes.append(str(AdminInfoObjectOfIrradiation))
    
    classes.append(str(AdminInfoObjectOfIrradiationExperiment))
    
    classes.append(str(AtomicNumber))
    
    classes.append(str(CollaborationRequirements))
    
    classes.append(str(ControlRoom))
    
    classes.append(str(ControlSystem))
    
    classes.append(str(System))
    
    classes.append(str(RadiationLength))
    
    classes.append(str(RadiationLengthOccupancy))
    
    classes.append(str(RadioprotectionRequirements))
    
    classes.append(str(SingularField))
    
    classes.append(str(RadiationField))
    
    classes.append(str(TechnicalRequirements))
    
    classes.append(str(IrradiationFacilityRequirements))
    
    classes.append(str(Fluence))
    
    classes.append(str(NuclearCollisionLength))
    
    classes.append(str(NuclearInteractionLength))
    
    classes.append(str(DataManagementSystem))
    
    classes.append(str(DoseRate))
    
    classes.append(str(ExternalPosition))
    
    classes.append(str(FluenceRate))
    
    classes.append(str(InteractionLengthEntry))
    
    classes.append(str(InteractionLengthTable))
    
    classes.append(str(IrradiationPosition))
    
    classes.append(str(IrradiationZone))
    
    classes.append(str(Laboratory))
    
    classes.append(str(LegalRequirements))
    
    classes.append(str(MinimumIonisationEntry))
    
    classes.append(str(MinimumIonisationTable))
    
    classes.append(str(MixedField))
    
    classes.append(str(NuclearCollisionLengthOccupancy))
    
    classes.append(str(NuclearInteractionLengthOccupancy))
    
    classes.append(str(Particle))
    
    context = {'classes':classes}
    return render(request, "home.html",context)