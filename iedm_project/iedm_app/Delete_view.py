from django.db import models
from django.template.loader import render_to_string
from .models import *
from .forms import *
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseNotFound
from django.template import loader
from django.core.urlresolvers import reverse
import datetime
from .forms import *
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.core.files.storage import FileSystemStorage
from django.utils.safestring import mark_safe


def ThingDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Thing,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/Thing/list/"
        return HttpResponseRedirect(link)
    else:
         form = ThingForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ThingDelete.html",context)

def ObjectDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Object,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/Object/list/"
        return HttpResponseRedirect(link)
    else:
         form = ObjectForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ObjectDelete.html",context)

def UnaryFunctionDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(UnaryFunction,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/UnaryFunction/list/"
        return HttpResponseRedirect(link)
    else:
         form = UnaryFunctionForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "UnaryFunctionDelete.html",context)

def UserDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(User,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/User/list/"
        return HttpResponseRedirect(link)
    else:
         form = UserForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "UserDelete.html",context)

def IrradiationFacilityRoleDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(IrradiationFacilityRole,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/IrradiationFacilityRole/list/"
        return HttpResponseRedirect(link)
    else:
         form = IrradiationFacilityRoleForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "IrradiationFacilityRoleDelete.html",context)

def IrradiationFacilityCoordinatorDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(IrradiationFacilityCoordinator,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/IrradiationFacilityCoordinator/list/"
        return HttpResponseRedirect(link)
    else:
         form = IrradiationFacilityCoordinatorForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "IrradiationFacilityCoordinatorDelete.html",context)

def IrradiationFacilityManagerDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(IrradiationFacilityManager,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/IrradiationFacilityManager/list/"
        return HttpResponseRedirect(link)
    else:
         form = IrradiationFacilityManagerForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "IrradiationFacilityManagerDelete.html",context)

def OperatorDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Operator,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/Operator/list/"
        return HttpResponseRedirect(link)
    else:
         form = OperatorForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "OperatorDelete.html",context)

def ResponsiblePersonDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ResponsiblePerson,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/ResponsiblePerson/list/"
        return HttpResponseRedirect(link)
    else:
         form = ResponsiblePersonForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ResponsiblePersonDelete.html",context)

def DUTIrradiationDelete(request, pk):
    foreign_keys = dict()
    
    foreign_keys['HasCumulatedQuantity'] = 'iedm_app:CumulatedQuantityList'
    
    foreign_keys['HasDosimeter'] = 'iedm_app:IrradiationExperimentObjectList'
    
    foreign_keys['HasInteractionLength'] = 'iedm_app:InteractionLengthList'
    
    foreign_keys['HasInteractionLengthOccupancy'] = 'iedm_app:InteractionLengthOccupancyList'
    
    foreign_keys['HasMaximumTargetCumulatedQuantity'] = 'iedm_app:CumulatedQuantityList'
    
    foreign_keys['HasMinimumCumulatedQuantity'] = 'iedm_app:CumulatedQuantityList'
    
    foreign_keys['HasSample'] = 'iedm_app:IrradiationExperimentObjectList'
    
    object_to_delete = get_object_or_404(DUTIrradiation,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/DUTIrradiation/list/"
        return HttpResponseRedirect(link)
    else:
         form = DUTIrradiationForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "DUTIrradiationDelete.html",context)

def SampleDelete(request, pk):
    foreign_keys = dict()
    
    foreign_keys['CompositionDefinedBy'] = 'iedm_app:LayerTableList'
    
    object_to_delete = get_object_or_404(Sample,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/Sample/list/"
        return HttpResponseRedirect(link)
    else:
         form = SampleForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SampleDelete.html",context)

def DUTDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(DUT,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/DUT/list/"
        return HttpResponseRedirect(link)
    else:
         form = DUTForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "DUTDelete.html",context)

def LayerTableDelete(request, pk):
    foreign_keys = dict()
    
    foreign_keys['HasEntries'] = 'iedm_app:LayerEntryList'
    
    object_to_delete = get_object_or_404(LayerTable,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/LayerTable/list/"
        return HttpResponseRedirect(link)
    else:
         form = LayerTableForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "LayerTableDelete.html",context)

def IrradiationExperimentDelete(request, pk):
    foreign_keys = dict()
    
    foreign_keys['HasPart'] = 'iedm_app:RequirementsList'
    
    foreign_keys['HasPart'] = 'iedm_app:DUTIrradiationList'
    
    foreign_keys['HasPart'] = 'iedm_app:MonitoringSystemList'
    
    foreign_keys['CreatedBy'] = 'iedm_app:UserList'
    
    foreign_keys['HasFieldOfStudy'] = 'iedm_app:DomainOfExperimentList'
    
    foreign_keys['HasFinalStorageArea'] = 'iedm_app:StorageAreaList'
    
    foreign_keys['IsCoordinatedBy'] = 'iedm_app:IrradiationFacilityCoordinatorList'
    
    foreign_keys['IsOperatedBy'] = 'iedm_app:OperatorList'
    
    foreign_keys['IsPerformedIn'] = 'iedm_app:IrradiationFacilityList'
    
    foreign_keys['IsUsedBy'] = 'iedm_app:IrradiationFacilityUserList'
    
    foreign_keys['UpdatedBy'] = 'iedm_app:UserList'
    
    foreign_keys['HasResponsiblePerson'] = 'iedm_app:ResponsiblePersonList'
    
    object_to_delete = get_object_or_404(IrradiationExperiment,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/IrradiationExperiment/list/"
        return HttpResponseRedirect(link)
    else:
         form = IrradiationExperimentForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "IrradiationExperimentDelete.html",context)

def LayerDelete(request, pk):
    foreign_keys = dict()
    
    foreign_keys['IsMadeOf'] = 'iedm_app:CompoundList'
    
    object_to_delete = get_object_or_404(Layer,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/Layer/list/"
        return HttpResponseRedirect(link)
    else:
         form = LayerForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "LayerDelete.html",context)

def CompoundDelete(request, pk):
    foreign_keys = dict()
    
    foreign_keys['CompositionDefinedBy'] = 'iedm_app:CompoundWeightFractionTableList'
    
    object_to_delete = get_object_or_404(Compound,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/Compound/list/"
        return HttpResponseRedirect(link)
    else:
         form = CompoundForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "CompoundDelete.html",context)

def CompoundWeightFractionTableDelete(request, pk):
    foreign_keys = dict()
    
    foreign_keys['HasEntries'] = 'iedm_app:CompoundWeightFractionEntryList'
    
    object_to_delete = get_object_or_404(CompoundWeightFractionTable,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/CompoundWeightFractionTable/list/"
        return HttpResponseRedirect(link)
    else:
         form = CompoundWeightFractionTableForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "CompoundWeightFractionTableDelete.html",context)

def IrradiationFacilityDelete(request, pk):
    foreign_keys = dict()
    
    foreign_keys['IsManagedBy'] = 'iedm_app:IrradiationFacilityManagerList'
    
    object_to_delete = get_object_or_404(IrradiationFacility,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/IrradiationFacility/list/"
        return HttpResponseRedirect(link)
    else:
         form = IrradiationFacilityForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "IrradiationFacilityDelete.html",context)

def PhysicalDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Physical,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/Physical/list/"
        return HttpResponseRedirect(link)
    else:
         form = PhysicalForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "PhysicalDelete.html",context)

def IrradiationFacilityUserDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(IrradiationFacilityUser,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/IrradiationFacilityUser/list/"
        return HttpResponseRedirect(link)
    else:
         form = IrradiationFacilityUserForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "IrradiationFacilityUserDelete.html",context)

def PredicateDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Predicate,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/Predicate/list/"
        return HttpResponseRedirect(link)
    else:
         form = PredicateForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "PredicateDelete.html",context)

def SentientAgentDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(SentientAgent,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/SentientAgent/list/"
        return HttpResponseRedirect(link)
    else:
         form = SentientAgentForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SentientAgentDelete.html",context)

def AgentDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Agent,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/Agent/list/"
        return HttpResponseRedirect(link)
    else:
         form = AgentForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AgentDelete.html",context)

def CompoundWeightFractionEntryDelete(request, pk):
    foreign_keys = dict()
    
    foreign_keys['HasArgument'] = 'iedm_app:ElementList'
    
    object_to_delete = get_object_or_404(CompoundWeightFractionEntry,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/CompoundWeightFractionEntry/list/"
        return HttpResponseRedirect(link)
    else:
         form = CompoundWeightFractionEntryForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "CompoundWeightFractionEntryDelete.html",context)

def ElementDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Element,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/Element/list/"
        return HttpResponseRedirect(link)
    else:
         form = ElementForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ElementDelete.html",context)

def UnaryFunctionEntryDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(UnaryFunctionEntry,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/UnaryFunctionEntry/list/"
        return HttpResponseRedirect(link)
    else:
         form = UnaryFunctionEntryForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "UnaryFunctionEntryDelete.html",context)

def CumulatedQuantityDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(CumulatedQuantity,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/CumulatedQuantity/list/"
        return HttpResponseRedirect(link)
    else:
         form = CumulatedQuantityForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "CumulatedQuantityDelete.html",context)

def IrradiationExperimentObjectDelete(request, pk):
    foreign_keys = dict()
    
    foreign_keys['UpdatedBy'] = 'iedm_app:UserList'
    
    foreign_keys['CreatedBy'] = 'iedm_app:UserList'
    
    object_to_delete = get_object_or_404(IrradiationExperimentObject,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/IrradiationExperimentObject/list/"
        return HttpResponseRedirect(link)
    else:
         form = IrradiationExperimentObjectForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "IrradiationExperimentObjectDelete.html",context)

def InteractionLengthDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(InteractionLength,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/InteractionLength/list/"
        return HttpResponseRedirect(link)
    else:
         form = InteractionLengthForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "InteractionLengthDelete.html",context)

def InteractionLengthOccupancyDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(InteractionLengthOccupancy,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/InteractionLengthOccupancy/list/"
        return HttpResponseRedirect(link)
    else:
         form = InteractionLengthOccupancyForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "InteractionLengthOccupancyDelete.html",context)

def DosimetricQuantityDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(DosimetricQuantity,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/DosimetricQuantity/list/"
        return HttpResponseRedirect(link)
    else:
         form = DosimetricQuantityForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "DosimetricQuantityDelete.html",context)

def DosimeterDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Dosimeter,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/Dosimeter/list/"
        return HttpResponseRedirect(link)
    else:
         form = DosimeterForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "DosimeterDelete.html",context)

def DomainOfExperimentDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(DomainOfExperiment,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/DomainOfExperiment/list/"
        return HttpResponseRedirect(link)
    else:
         form = DomainOfExperimentForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "DomainOfExperimentDelete.html",context)

def ParticlePhysicsExperimentDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ParticlePhysicsExperiment,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/ParticlePhysicsExperiment/list/"
        return HttpResponseRedirect(link)
    else:
         form = ParticlePhysicsExperimentForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ParticlePhysicsExperimentDelete.html",context)

def RequirementsDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Requirements,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/Requirements/list/"
        return HttpResponseRedirect(link)
    else:
         form = RequirementsForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "RequirementsDelete.html",context)

def MonitoringSystemDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(MonitoringSystem,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/MonitoringSystem/list/"
        return HttpResponseRedirect(link)
    else:
         form = MonitoringSystemForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "MonitoringSystemDelete.html",context)

def StorageAreaDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(StorageArea,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/StorageArea/list/"
        return HttpResponseRedirect(link)
    else:
         form = StorageAreaForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "StorageAreaDelete.html",context)

def FieldOfStudyDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(FieldOfStudy,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/FieldOfStudy/list/"
        return HttpResponseRedirect(link)
    else:
         form = FieldOfStudyForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "FieldOfStudyDelete.html",context)

def IrradiationFacilityPostionDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(IrradiationFacilityPostion,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/IrradiationFacilityPostion/list/"
        return HttpResponseRedirect(link)
    else:
         form = IrradiationFacilityPostionForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "IrradiationFacilityPostionDelete.html",context)

def LengthDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Length,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/Length/list/"
        return HttpResponseRedirect(link)
    else:
         form = LengthForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "LengthDelete.html",context)

def PercentageDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Percentage,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/Percentage/list/"
        return HttpResponseRedirect(link)
    else:
         form = PercentageForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "PercentageDelete.html",context)

def LayerEntryDelete(request, pk):
    foreign_keys = dict()
    
    foreign_keys['HasEntries'] = 'iedm_app:LayerList'
    
    object_to_delete = get_object_or_404(LayerEntry,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/LayerEntry/list/"
        return HttpResponseRedirect(link)
    else:
         form = LayerEntryForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "LayerEntryDelete.html",context)

def RelationDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Relation,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/Relation/list/"
        return HttpResponseRedirect(link)
    else:
         form = RelationForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "RelationDelete.html",context)

def AbstractDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Abstract,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/Abstract/list/"
        return HttpResponseRedirect(link)
    else:
         form = AbstractForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AbstractDelete.html",context)

def RegionDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Region,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/Region/list/"
        return HttpResponseRedirect(link)
    else:
         form = RegionForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "RegionDelete.html",context)

def SubjectRoleDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(SubjectRole,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/SubjectRole/list/"
        return HttpResponseRedirect(link)
    else:
         form = SubjectRoleForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SubjectRoleDelete.html",context)

def ActorRoleDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ActorRole,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/ActorRole/list/"
        return HttpResponseRedirect(link)
    else:
         form = ActorRoleForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ActorRoleDelete.html",context)

def AbsorbedDoseDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(AbsorbedDose,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/AbsorbedDose/list/"
        return HttpResponseRedirect(link)
    else:
         form = AbsorbedDoseForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AbsorbedDoseDelete.html",context)

def AbsorbedDoseInMediumDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(AbsorbedDoseInMedium,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/AbsorbedDoseInMedium/list/"
        return HttpResponseRedirect(link)
    else:
         form = AbsorbedDoseInMediumForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AbsorbedDoseInMediumDelete.html",context)

def QuantityDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Quantity,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/Quantity/list/"
        return HttpResponseRedirect(link)
    else:
         form = QuantityForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "QuantityDelete.html",context)

def ActivityDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Activity,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/Activity/list/"
        return HttpResponseRedirect(link)
    else:
         form = ActivityForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ActivityDelete.html",context)

def DosimetricRateDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(DosimetricRate,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/DosimetricRate/list/"
        return HttpResponseRedirect(link)
    else:
         form = DosimetricRateForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "DosimetricRateDelete.html",context)

def AtomicMassDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(AtomicMass,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/AtomicMass/list/"
        return HttpResponseRedirect(link)
    else:
         form = AtomicMassForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AtomicMassDelete.html",context)

def MassDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Mass,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/Mass/list/"
        return HttpResponseRedirect(link)
    else:
         form = MassForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "MassDelete.html",context)

def DensityDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Density,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/Density/list/"
        return HttpResponseRedirect(link)
    else:
         form = DensityForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "DensityDelete.html",context)

def EnergyDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Energy,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/Energy/list/"
        return HttpResponseRedirect(link)
    else:
         form = EnergyForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "EnergyDelete.html",context)

def HeightDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Height,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/Height/list/"
        return HttpResponseRedirect(link)
    else:
         form = HeightForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "HeightDelete.html",context)

def NumberDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Number,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/Number/list/"
        return HttpResponseRedirect(link)
    else:
         form = NumberForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "NumberDelete.html",context)

def RatioDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Ratio,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/Ratio/list/"
        return HttpResponseRedirect(link)
    else:
         form = RatioForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "RatioDelete.html",context)

def TemperatureDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Temperature,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/Temperature/list/"
        return HttpResponseRedirect(link)
    else:
         form = TemperatureForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "TemperatureDelete.html",context)

def ThicknessDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Thickness,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/Thickness/list/"
        return HttpResponseRedirect(link)
    else:
         form = ThicknessForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ThicknessDelete.html",context)

def AbsorbedDoseInAirDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(AbsorbedDoseInAir,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/AbsorbedDoseInAir/list/"
        return HttpResponseRedirect(link)
    else:
         form = AbsorbedDoseInAirForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AbsorbedDoseInAirDelete.html",context)

def AdminInfoIrradiationExperimentDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(AdminInfoIrradiationExperiment,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/AdminInfoIrradiationExperiment/list/"
        return HttpResponseRedirect(link)
    else:
         form = AdminInfoIrradiationExperimentForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AdminInfoIrradiationExperimentDelete.html",context)

def AdminInfoExperimentDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(AdminInfoExperiment,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/AdminInfoExperiment/list/"
        return HttpResponseRedirect(link)
    else:
         form = AdminInfoExperimentForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AdminInfoExperimentDelete.html",context)

def ActionrelatedRoleDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ActionrelatedRole,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/ActionrelatedRole/list/"
        return HttpResponseRedirect(link)
    else:
         form = ActionrelatedRoleForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ActionrelatedRoleDelete.html",context)

def RoleDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Role,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/Role/list/"
        return HttpResponseRedirect(link)
    else:
         form = RoleForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "RoleDelete.html",context)

def AdministrativeInformationDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(AdministrativeInformation,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/AdministrativeInformation/list/"
        return HttpResponseRedirect(link)
    else:
         form = AdministrativeInformationForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AdministrativeInformationDelete.html",context)

def PropositionDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Proposition,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/Proposition/list/"
        return HttpResponseRedirect(link)
    else:
         form = PropositionForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "PropositionDelete.html",context)

def AdminInfoObjectOfExperimentDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(AdminInfoObjectOfExperiment,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/AdminInfoObjectOfExperiment/list/"
        return HttpResponseRedirect(link)
    else:
         form = AdminInfoObjectOfExperimentForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AdminInfoObjectOfExperimentDelete.html",context)

def AdminInfoUserDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(AdminInfoUser,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/AdminInfoUser/list/"
        return HttpResponseRedirect(link)
    else:
         form = AdminInfoUserForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AdminInfoUserDelete.html",context)

def ClassificationDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Classification,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/Classification/list/"
        return HttpResponseRedirect(link)
    else:
         form = ClassificationForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ClassificationDelete.html",context)

def ScientificTaskDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ScientificTask,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/ScientificTask/list/"
        return HttpResponseRedirect(link)
    else:
         form = ScientificTaskForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ScientificTaskDelete.html",context)

def ClassificationByDomainDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ClassificationByDomain,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/ClassificationByDomain/list/"
        return HttpResponseRedirect(link)
    else:
         form = ClassificationByDomainForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ClassificationByDomainDelete.html",context)

def ClassificationOfExperimentsDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ClassificationOfExperiments,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/ClassificationOfExperiments/list/"
        return HttpResponseRedirect(link)
    else:
         form = ClassificationOfExperimentsForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ClassificationOfExperimentsDelete.html",context)

def CorpuscularObjectDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(CorpuscularObject,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/CorpuscularObject/list/"
        return HttpResponseRedirect(link)
    else:
         form = CorpuscularObjectForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "CorpuscularObjectDelete.html",context)

def SelfConnectedObjectDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(SelfConnectedObject,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/SelfConnectedObject/list/"
        return HttpResponseRedirect(link)
    else:
         form = SelfConnectedObjectForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SelfConnectedObjectDelete.html",context)

def ExperimentalRequirementsDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ExperimentalRequirements,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/ExperimentalRequirements/list/"
        return HttpResponseRedirect(link)
    else:
         form = ExperimentalRequirementsForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ExperimentalRequirementsDelete.html",context)

def AdminInfoIrradiationExperimentUserDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(AdminInfoIrradiationExperimentUser,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/AdminInfoIrradiationExperimentUser/list/"
        return HttpResponseRedirect(link)
    else:
         form = AdminInfoIrradiationExperimentUserForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AdminInfoIrradiationExperimentUserDelete.html",context)

def AdminInfoObjectOfIrradiationDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(AdminInfoObjectOfIrradiation,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/AdminInfoObjectOfIrradiation/list/"
        return HttpResponseRedirect(link)
    else:
         form = AdminInfoObjectOfIrradiationForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AdminInfoObjectOfIrradiationDelete.html",context)

def AdminInfoObjectOfIrradiationExperimentDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(AdminInfoObjectOfIrradiationExperiment,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/AdminInfoObjectOfIrradiationExperiment/list/"
        return HttpResponseRedirect(link)
    else:
         form = AdminInfoObjectOfIrradiationExperimentForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AdminInfoObjectOfIrradiationExperimentDelete.html",context)

def AtomicNumberDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(AtomicNumber,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/AtomicNumber/list/"
        return HttpResponseRedirect(link)
    else:
         form = AtomicNumberForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AtomicNumberDelete.html",context)

def CollaborationRequirementsDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(CollaborationRequirements,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/CollaborationRequirements/list/"
        return HttpResponseRedirect(link)
    else:
         form = CollaborationRequirementsForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "CollaborationRequirementsDelete.html",context)

def ControlRoomDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ControlRoom,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/ControlRoom/list/"
        return HttpResponseRedirect(link)
    else:
         form = ControlRoomForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ControlRoomDelete.html",context)

def ControlSystemDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ControlSystem,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/ControlSystem/list/"
        return HttpResponseRedirect(link)
    else:
         form = ControlSystemForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ControlSystemDelete.html",context)

def SystemDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(System,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/System/list/"
        return HttpResponseRedirect(link)
    else:
         form = SystemForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SystemDelete.html",context)

def RadiationLengthDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(RadiationLength,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/RadiationLength/list/"
        return HttpResponseRedirect(link)
    else:
         form = RadiationLengthForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "RadiationLengthDelete.html",context)

def RadiationLengthOccupancyDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(RadiationLengthOccupancy,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/RadiationLengthOccupancy/list/"
        return HttpResponseRedirect(link)
    else:
         form = RadiationLengthOccupancyForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "RadiationLengthOccupancyDelete.html",context)

def RadioprotectionRequirementsDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(RadioprotectionRequirements,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/RadioprotectionRequirements/list/"
        return HttpResponseRedirect(link)
    else:
         form = RadioprotectionRequirementsForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "RadioprotectionRequirementsDelete.html",context)

def SingularFieldDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(SingularField,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/SingularField/list/"
        return HttpResponseRedirect(link)
    else:
         form = SingularFieldForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SingularFieldDelete.html",context)

def RadiationFieldDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(RadiationField,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/RadiationField/list/"
        return HttpResponseRedirect(link)
    else:
         form = RadiationFieldForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "RadiationFieldDelete.html",context)

def TechnicalRequirementsDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(TechnicalRequirements,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/TechnicalRequirements/list/"
        return HttpResponseRedirect(link)
    else:
         form = TechnicalRequirementsForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "TechnicalRequirementsDelete.html",context)

def IrradiationFacilityRequirementsDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(IrradiationFacilityRequirements,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/IrradiationFacilityRequirements/list/"
        return HttpResponseRedirect(link)
    else:
         form = IrradiationFacilityRequirementsForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "IrradiationFacilityRequirementsDelete.html",context)

def FluenceDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Fluence,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/Fluence/list/"
        return HttpResponseRedirect(link)
    else:
         form = FluenceForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "FluenceDelete.html",context)

def NuclearCollisionLengthDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(NuclearCollisionLength,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/NuclearCollisionLength/list/"
        return HttpResponseRedirect(link)
    else:
         form = NuclearCollisionLengthForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "NuclearCollisionLengthDelete.html",context)

def NuclearInteractionLengthDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(NuclearInteractionLength,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/NuclearInteractionLength/list/"
        return HttpResponseRedirect(link)
    else:
         form = NuclearInteractionLengthForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "NuclearInteractionLengthDelete.html",context)

def DataManagementSystemDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(DataManagementSystem,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/DataManagementSystem/list/"
        return HttpResponseRedirect(link)
    else:
         form = DataManagementSystemForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "DataManagementSystemDelete.html",context)

def DoseRateDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(DoseRate,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/DoseRate/list/"
        return HttpResponseRedirect(link)
    else:
         form = DoseRateForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "DoseRateDelete.html",context)

def ExternalPositionDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ExternalPosition,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/ExternalPosition/list/"
        return HttpResponseRedirect(link)
    else:
         form = ExternalPositionForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ExternalPositionDelete.html",context)

def FluenceRateDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(FluenceRate,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/FluenceRate/list/"
        return HttpResponseRedirect(link)
    else:
         form = FluenceRateForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "FluenceRateDelete.html",context)

def InteractionLengthEntryDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(InteractionLengthEntry,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/InteractionLengthEntry/list/"
        return HttpResponseRedirect(link)
    else:
         form = InteractionLengthEntryForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "InteractionLengthEntryDelete.html",context)

def InteractionLengthTableDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(InteractionLengthTable,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/InteractionLengthTable/list/"
        return HttpResponseRedirect(link)
    else:
         form = InteractionLengthTableForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "InteractionLengthTableDelete.html",context)

def IrradiationPositionDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(IrradiationPosition,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/IrradiationPosition/list/"
        return HttpResponseRedirect(link)
    else:
         form = IrradiationPositionForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "IrradiationPositionDelete.html",context)

def IrradiationZoneDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(IrradiationZone,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/IrradiationZone/list/"
        return HttpResponseRedirect(link)
    else:
         form = IrradiationZoneForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "IrradiationZoneDelete.html",context)

def LaboratoryDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Laboratory,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/Laboratory/list/"
        return HttpResponseRedirect(link)
    else:
         form = LaboratoryForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "LaboratoryDelete.html",context)

def LegalRequirementsDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(LegalRequirements,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/LegalRequirements/list/"
        return HttpResponseRedirect(link)
    else:
         form = LegalRequirementsForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "LegalRequirementsDelete.html",context)

def MinimumIonisationEntryDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(MinimumIonisationEntry,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/MinimumIonisationEntry/list/"
        return HttpResponseRedirect(link)
    else:
         form = MinimumIonisationEntryForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "MinimumIonisationEntryDelete.html",context)

def MinimumIonisationTableDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(MinimumIonisationTable,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/MinimumIonisationTable/list/"
        return HttpResponseRedirect(link)
    else:
         form = MinimumIonisationTableForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "MinimumIonisationTableDelete.html",context)

def MixedFieldDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(MixedField,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/MixedField/list/"
        return HttpResponseRedirect(link)
    else:
         form = MixedFieldForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "MixedFieldDelete.html",context)

def NuclearCollisionLengthOccupancyDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(NuclearCollisionLengthOccupancy,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/NuclearCollisionLengthOccupancy/list/"
        return HttpResponseRedirect(link)
    else:
         form = NuclearCollisionLengthOccupancyForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "NuclearCollisionLengthOccupancyDelete.html",context)

def NuclearInteractionLengthOccupancyDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(NuclearInteractionLengthOccupancy,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/NuclearInteractionLengthOccupancy/list/"
        return HttpResponseRedirect(link)
    else:
         form = NuclearInteractionLengthOccupancyForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "NuclearInteractionLengthOccupancyDelete.html",context)

def ParticleDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Particle,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/iedm_app/Particle/list/"
        return HttpResponseRedirect(link)
    else:
         form = ParticleForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ParticleDelete.html",context)
