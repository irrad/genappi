from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    # Examples:
    # url(r'^$', 'EXPO_project.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^vowl', 'EXPO_app.Home_view.vowl', name='vowl'),
    url(r'^EXPO_app/', include('EXPO_app.urls', namespace='EXPO_app')),
    url(r'^$', 'EXPO_app.Home_view.HomeView', name='HomeView'),    url(r'^accounts/',include('django.contrib.auth.urls')),]
