from django import forms
from django.forms import ModelForm
from .models import *


class ThingForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ThingForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Thing
        exclude = ()

class RepresentationForm1(ModelForm):
    def __init__(self, *args, **kwargs):
        super(RepresentationForm1, self).__init__(*args, **kwargs)
    class Meta:
        model = RepresentationForm
        exclude = ()

class ArtifactForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ArtifactForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Artifact
        exclude = ()

class SynonymousExternalConceptForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(SynonymousExternalConceptForm, self).__init__(*args, **kwargs)
    class Meta:
        model = SynonymousExternalConcept
        exclude = ()

class NameForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(NameForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Name
        exclude = ()

class ScientificExperimentForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ScientificExperimentForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ScientificExperiment
        exclude = ()

class ExperimentalDesignForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ExperimentalDesignForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ExperimentalDesign
        exclude = ()

class AdminInfoExperimentForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(AdminInfoExperimentForm, self).__init__(*args, **kwargs)
    class Meta:
        model = AdminInfoExperiment
        exclude = ()

class IDExperimentForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(IDExperimentForm, self).__init__(*args, **kwargs)
    class Meta:
        model = IDExperiment
        exclude = ()

class DomainOfExperimentForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(DomainOfExperimentForm, self).__init__(*args, **kwargs)
    class Meta:
        model = DomainOfExperiment
        exclude = ()

class ExperimentalObservationForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ExperimentalObservationForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ExperimentalObservation
        exclude = ()

class ResultsInterpretationForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ResultsInterpretationForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ResultsInterpretation
        exclude = ()

class ExperimentalActionForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ExperimentalActionForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ExperimentalAction
        exclude = ()

class ActionNameForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ActionNameForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ActionName
        exclude = ()

class ActionComplexityForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ActionComplexityForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ActionComplexity
        exclude = ()

class HumanForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(HumanForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Human
        exclude = ()

class OrganizationForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(OrganizationForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Organization
        exclude = ()

class AdminInfoAuthorForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(AdminInfoAuthorForm, self).__init__(*args, **kwargs)
    class Meta:
        model = AdminInfoAuthor
        exclude = ()

class AdminInfoObjectOfExperimentForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(AdminInfoObjectOfExperimentForm, self).__init__(*args, **kwargs)
    class Meta:
        model = AdminInfoObjectOfExperiment
        exclude = ()

class AdminInfoProviderForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(AdminInfoProviderForm, self).__init__(*args, **kwargs)
    class Meta:
        model = AdminInfoProvider
        exclude = ()

class AdminInfoSubjectOfExperimentForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(AdminInfoSubjectOfExperimentForm, self).__init__(*args, **kwargs)
    class Meta:
        model = AdminInfoSubjectOfExperiment
        exclude = ()

class AdminInfoSubmitterForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(AdminInfoSubmitterForm, self).__init__(*args, **kwargs)
    class Meta:
        model = AdminInfoSubmitter
        exclude = ()

class AdminInfoUserForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(AdminInfoUserForm, self).__init__(*args, **kwargs)
    class Meta:
        model = AdminInfoUser
        exclude = ()

class ExperimentalMethodForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ExperimentalMethodForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ExperimentalMethod
        exclude = ()

class MethodApplicabilityConditionForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(MethodApplicabilityConditionForm, self).__init__(*args, **kwargs)
    class Meta:
        model = MethodApplicabilityCondition
        exclude = ()

class StandardOperatingProcedureForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(StandardOperatingProcedureForm, self).__init__(*args, **kwargs)
    class Meta:
        model = StandardOperatingProcedure
        exclude = ()

class AuthorForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(AuthorForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Author
        exclude = ()

class ClassificationOfDomainsForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ClassificationOfDomainsForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ClassificationOfDomains
        exclude = ()

class ClassificationOfExperimentsForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ClassificationOfExperimentsForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ClassificationOfExperiments
        exclude = ()

class ExperimentalHypothesisForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ExperimentalHypothesisForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ExperimentalHypothesis
        exclude = ()

class HypothesisComplexityForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(HypothesisComplexityForm, self).__init__(*args, **kwargs)
    class Meta:
        model = HypothesisComplexity
        exclude = ()

class RepresentationForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(RepresentationForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Representation
        exclude = ()

class PropositionForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(PropositionForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Proposition
        exclude = ()

class ExperimentalEquipmentForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ExperimentalEquipmentForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ExperimentalEquipment
        exclude = ()

class TechnicalDescriptionForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(TechnicalDescriptionForm, self).__init__(*args, **kwargs)
    class Meta:
        model = TechnicalDescription
        exclude = ()

class ExperimentalResultsForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ExperimentalResultsForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ExperimentalResults
        exclude = ()

class ObservationalErrorForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ObservationalErrorForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ObservationalError
        exclude = ()

class ResultErrorForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ResultErrorForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ResultError
        exclude = ()

class ResultsEvaluationForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ResultsEvaluationForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ResultsEvaluation
        exclude = ()

class ScientificInvestigationForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ScientificInvestigationForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ScientificInvestigation
        exclude = ()

class ExperimentalModelForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ExperimentalModelForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ExperimentalModel
        exclude = ()

class ExperimentalFactorForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ExperimentalFactorForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ExperimentalFactor
        exclude = ()

class ExperimentalTechnologyForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ExperimentalTechnologyForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ExperimentalTechnology
        exclude = ()

class ExperimentalRequirementsForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ExperimentalRequirementsForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ExperimentalRequirements
        exclude = ()

class HypothesisExplicitnessForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(HypothesisExplicitnessForm, self).__init__(*args, **kwargs)
    class Meta:
        model = HypothesisExplicitness
        exclude = ()

class LinguisticExpressionForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(LinguisticExpressionForm, self).__init__(*args, **kwargs)
    class Meta:
        model = LinguisticExpression
        exclude = ()

class FactRejectResearchHypothesisForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(FactRejectResearchHypothesisForm, self).__init__(*args, **kwargs)
    class Meta:
        model = FactRejectResearchHypothesis
        exclude = ()

class FactSupportResearchHypothesisForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(FactSupportResearchHypothesisForm, self).__init__(*args, **kwargs)
    class Meta:
        model = FactSupportResearchHypothesis
        exclude = ()

class NumberForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(NumberForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Number
        exclude = ()

class GeneralityOfHypothesisForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(GeneralityOfHypothesisForm, self).__init__(*args, **kwargs)
    class Meta:
        model = GeneralityOfHypothesis
        exclude = ()

class ActionGoalForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ActionGoalForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ActionGoal
        exclude = ()

class ExperimentalGoalForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ExperimentalGoalForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ExperimentalGoal
        exclude = ()

class MethodGoalForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(MethodGoalForm, self).__init__(*args, **kwargs)
    class Meta:
        model = MethodGoal
        exclude = ()

class HypothesisFormationForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(HypothesisFormationForm, self).__init__(*args, **kwargs)
    class Meta:
        model = HypothesisFormation
        exclude = ()

class HypothesisRepresentationForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(HypothesisRepresentationForm, self).__init__(*args, **kwargs)
    class Meta:
        model = HypothesisRepresentation
        exclude = ()

class InformationGetheringForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(InformationGetheringForm, self).__init__(*args, **kwargs)
    class Meta:
        model = InformationGethering
        exclude = ()

class SampleForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(SampleForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Sample
        exclude = ()

class VariabilityForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(VariabilityForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Variability
        exclude = ()

class TimePointForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(TimePointForm, self).__init__(*args, **kwargs)
    class Meta:
        model = TimePoint
        exclude = ()

class UserForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(UserForm, self).__init__(*args, **kwargs)
    class Meta:
        model = User
        exclude = ()

class LoginNameForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(LoginNameForm, self).__init__(*args, **kwargs)
    class Meta:
        model = LoginName
        exclude = ()

class DomainModelForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(DomainModelForm, self).__init__(*args, **kwargs)
    class Meta:
        model = DomainModel
        exclude = ()

class GroupExperimentalObjectForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(GroupExperimentalObjectForm, self).__init__(*args, **kwargs)
    class Meta:
        model = GroupExperimentalObject
        exclude = ()

class ObjectOfExperimentForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ObjectOfExperimentForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ObjectOfExperiment
        exclude = ()

class ClassificationByModelForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ClassificationByModelForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ClassificationByModel
        exclude = ()

class StatisticsCharacteristicForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(StatisticsCharacteristicForm, self).__init__(*args, **kwargs)
    class Meta:
        model = StatisticsCharacteristic
        exclude = ()

class ParentGroupForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ParentGroupForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ParentGroup
        exclude = ()

class PasswordForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(PasswordForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Password
        exclude = ()

class ProcedureExecuteExperimentForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ProcedureExecuteExperimentForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ProcedureExecuteExperiment
        exclude = ()

class PlanOfExperimentalActionsForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(PlanOfExperimentalActionsForm, self).__init__(*args, **kwargs)
    class Meta:
        model = PlanOfExperimentalActions
        exclude = ()

class PoblemAnalysisForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(PoblemAnalysisForm, self).__init__(*args, **kwargs)
    class Meta:
        model = PoblemAnalysis
        exclude = ()

class SubjectQualificationForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(SubjectQualificationForm, self).__init__(*args, **kwargs)
    class Meta:
        model = SubjectQualification
        exclude = ()

class BiblioReferenceForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BiblioReferenceForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BiblioReference
        exclude = ()

class ModelForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ModelForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Model
        exclude = ()

class ModelRepresentationForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ModelRepresentationForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ModelRepresentation
        exclude = ()

class RepresentationExperimentalExecutionProcedureForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(RepresentationExperimentalExecutionProcedureForm, self).__init__(*args, **kwargs)
    class Meta:
        model = RepresentationExperimentalExecutionProcedure
        exclude = ()

class RepresentationExperimentalGoalForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(RepresentationExperimentalGoalForm, self).__init__(*args, **kwargs)
    class Meta:
        model = RepresentationExperimentalGoal
        exclude = ()

class SampleRepresentativenessForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(SampleRepresentativenessForm, self).__init__(*args, **kwargs)
    class Meta:
        model = SampleRepresentativeness
        exclude = ()

class SampleSizeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(SampleSizeForm, self).__init__(*args, **kwargs)
    class Meta:
        model = SampleSize
        exclude = ()

class SamplingMethodForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(SamplingMethodForm, self).__init__(*args, **kwargs)
    class Meta:
        model = SamplingMethod
        exclude = ()

class SystematicSamplingForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(SystematicSamplingForm, self).__init__(*args, **kwargs)
    class Meta:
        model = SystematicSampling
        exclude = ()

class SamplingRuleForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(SamplingRuleForm, self).__init__(*args, **kwargs)
    class Meta:
        model = SamplingRule
        exclude = ()

class ExperimentalStandardForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ExperimentalStandardForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ExperimentalStandard
        exclude = ()

class ErrorForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ErrorForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Error
        exclude = ()

class DispersionForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(DispersionForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Dispersion
        exclude = ()

class LevelOfSignificanceForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(LevelOfSignificanceForm, self).__init__(*args, **kwargs)
    class Meta:
        model = LevelOfSignificance
        exclude = ()

class StatusExperimentalDocumentForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(StatusExperimentalDocumentForm, self).__init__(*args, **kwargs)
    class Meta:
        model = StatusExperimentalDocument
        exclude = ()

class ExperimentalDesignStrategyForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ExperimentalDesignStrategyForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ExperimentalDesignStrategy
        exclude = ()

class ExperimentalSubgoalForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ExperimentalSubgoalForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ExperimentalSubgoal
        exclude = ()

class SubjectMethodForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(SubjectMethodForm, self).__init__(*args, **kwargs)
    class Meta:
        model = SubjectMethod
        exclude = ()

class BaconianExperimentForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BaconianExperimentForm, self).__init__(*args, **kwargs)
    class Meta:
        model = BaconianExperiment
        exclude = ()

class TargetVariableForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(TargetVariableForm, self).__init__(*args, **kwargs)
    class Meta:
        model = TargetVariable
        exclude = ()

class TitleForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(TitleForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Title
        exclude = ()

class ValueEstimateForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ValueEstimateForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ValueEstimate
        exclude = ()

class ValueOfVariableForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ValueOfVariableForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ValueOfVariable
        exclude = ()

class RoleForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(RoleForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Role
        exclude = ()

class CorpuscularObjectForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(CorpuscularObjectForm, self).__init__(*args, **kwargs)
    class Meta:
        model = CorpuscularObject
        exclude = ()

class AttributeRoleForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(AttributeRoleForm, self).__init__(*args, **kwargs)
    class Meta:
        model = AttributeRole
        exclude = ()

class AttributeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(AttributeForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Attribute
        exclude = ()

class ProcedureForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ProcedureForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Procedure
        exclude = ()

class AdministrativeInformationForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(AdministrativeInformationForm, self).__init__(*args, **kwargs)
    class Meta:
        model = AdministrativeInformation
        exclude = ()

class TitleExperimentForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(TitleExperimentForm, self).__init__(*args, **kwargs)
    class Meta:
        model = TitleExperiment
        exclude = ()

class NameEquipmentForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(NameEquipmentForm, self).__init__(*args, **kwargs)
    class Meta:
        model = NameEquipment
        exclude = ()

class PersonalNameForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(PersonalNameForm, self).__init__(*args, **kwargs)
    class Meta:
        model = PersonalName
        exclude = ()

class FieldOfStudyForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(FieldOfStudyForm, self).__init__(*args, **kwargs)
    class Meta:
        model = FieldOfStudy
        exclude = ()

class RelatedDomainForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(RelatedDomainForm, self).__init__(*args, **kwargs)
    class Meta:
        model = RelatedDomain
        exclude = ()

class ProductRoleForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ProductRoleForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ProductRole
        exclude = ()

class ScientificTaskForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ScientificTaskForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ScientificTask
        exclude = ()

class ExecutionOfExperimentForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ExecutionOfExperimentForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ExecutionOfExperiment
        exclude = ()

class CorporateNameForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(CorporateNameForm, self).__init__(*args, **kwargs)
    class Meta:
        model = CorporateName
        exclude = ()

class AttributeOfActionForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(AttributeOfActionForm, self).__init__(*args, **kwargs)
    class Meta:
        model = AttributeOfAction
        exclude = ()

class SentientAgentForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(SentientAgentForm, self).__init__(*args, **kwargs)
    class Meta:
        model = SentientAgent
        exclude = ()

class RobotForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(RobotForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Robot
        exclude = ()

class GroupForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(GroupForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Group
        exclude = ()

class EntityForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(EntityForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Entity
        exclude = ()

class ResearchMethodForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ResearchMethodForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ResearchMethod
        exclude = ()

class RequirementsForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(RequirementsForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Requirements
        exclude = ()

class AuthorSOPForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(AuthorSOPForm, self).__init__(*args, **kwargs)
    class Meta:
        model = AuthorSOP
        exclude = ()

class SubjectRoleForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(SubjectRoleForm, self).__init__(*args, **kwargs)
    class Meta:
        model = SubjectRole
        exclude = ()

class ClassificationForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ClassificationForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Classification
        exclude = ()

class FactForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(FactForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Fact
        exclude = ()

class ModelAssumptionForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ModelAssumptionForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ModelAssumption
        exclude = ()

class VariableForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(VariableForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Variable
        exclude = ()

class AttributeOfHypothesisForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(AttributeOfHypothesisForm, self).__init__(*args, **kwargs)
    class Meta:
        model = AttributeOfHypothesis
        exclude = ()

class ContentBearingObjectForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ContentBearingObjectForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ContentBearingObject
        exclude = ()

class AbstractForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(AbstractForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Abstract
        exclude = ()

class QuantityForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(QuantityForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Quantity
        exclude = ()

class RelationForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(RelationForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Relation
        exclude = ()

class ProcessrelatedRoleForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ProcessrelatedRoleForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ProcessrelatedRole
        exclude = ()

class ContentBearingPhysicalForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ContentBearingPhysicalForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ContentBearingPhysical
        exclude = ()

class PhysicalQuantityForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(PhysicalQuantityForm, self).__init__(*args, **kwargs)
    class Meta:
        model = PhysicalQuantity
        exclude = ()

class GoalForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(GoalForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Goal
        exclude = ()

class RepresentationExperimentalObservationForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(RepresentationExperimentalObservationForm, self).__init__(*args, **kwargs)
    class Meta:
        model = RepresentationExperimentalObservation
        exclude = ()

class RepresentationExperimentalResultsForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(RepresentationExperimentalResultsForm, self).__init__(*args, **kwargs)
    class Meta:
        model = RepresentationExperimentalResults
        exclude = ()

class AttributeGroupForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(AttributeGroupForm, self).__init__(*args, **kwargs)
    class Meta:
        model = AttributeGroup
        exclude = ()

class TimePositionForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(TimePositionForm, self).__init__(*args, **kwargs)
    class Meta:
        model = TimePosition
        exclude = ()

class ActorRoleForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ActorRoleForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ActorRole
        exclude = ()

class DocumentStageForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(DocumentStageForm, self).__init__(*args, **kwargs)
    class Meta:
        model = DocumentStage
        exclude = ()

class PermissionStatusForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(PermissionStatusForm, self).__init__(*args, **kwargs)
    class Meta:
        model = PermissionStatus
        exclude = ()

class PlanForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(PlanForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Plan
        exclude = ()

class ReferenceForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ReferenceForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Reference
        exclude = ()

class AttributeSampleForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(AttributeSampleForm, self).__init__(*args, **kwargs)
    class Meta:
        model = AttributeSample
        exclude = ()

class ExperimentalRuleForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ExperimentalRuleForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ExperimentalRule
        exclude = ()

class RobustnessForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(RobustnessForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Robustness
        exclude = ()

class ValidityForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ValidityForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Validity
        exclude = ()

class AttributeOfDocumentForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(AttributeOfDocumentForm, self).__init__(*args, **kwargs)
    class Meta:
        model = AttributeOfDocument
        exclude = ()

class PhysicalExperimentForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(PhysicalExperimentForm, self).__init__(*args, **kwargs)
    class Meta:
        model = PhysicalExperiment
        exclude = ()

class ComputationalDataForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ComputationalDataForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ComputationalData
        exclude = ()

class PredicateForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(PredicateForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Predicate
        exclude = ()

class SelfConnectedObjectForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(SelfConnectedObjectForm, self).__init__(*args, **kwargs)
    class Meta:
        model = SelfConnectedObject
        exclude = ()

class ScientificActivityForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ScientificActivityForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ScientificActivity
        exclude = ()

class FormingClassificationSystemForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(FormingClassificationSystemForm, self).__init__(*args, **kwargs)
    class Meta:
        model = FormingClassificationSystem
        exclude = ()

class HypothesisFormingForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(HypothesisFormingForm, self).__init__(*args, **kwargs)
    class Meta:
        model = HypothesisForming
        exclude = ()

class InterpretingResultForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(InterpretingResultForm, self).__init__(*args, **kwargs)
    class Meta:
        model = InterpretingResult
        exclude = ()

class ProcessProblemAnalysisForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ProcessProblemAnalysisForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ProcessProblemAnalysis
        exclude = ()

class ResultEvaluatingForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ResultEvaluatingForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ResultEvaluating
        exclude = ()

class AttributeOfModelForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(AttributeOfModelForm, self).__init__(*args, **kwargs)
    class Meta:
        model = AttributeOfModel
        exclude = ()

class AttributeOfVariableForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(AttributeOfVariableForm, self).__init__(*args, **kwargs)
    class Meta:
        model = AttributeOfVariable
        exclude = ()

class AgentForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(AgentForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Agent
        exclude = ()

class CollectionForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(CollectionForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Collection
        exclude = ()

class EperimentalDesignTaskForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(EperimentalDesignTaskForm, self).__init__(*args, **kwargs)
    class Meta:
        model = EperimentalDesignTask
        exclude = ()

class PhysicalForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(PhysicalForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Physical
        exclude = ()

class ObjectForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ObjectForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Object
        exclude = ()

class ProcessForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ProcessForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Process
        exclude = ()

class TaskRoleForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(TaskRoleForm, self).__init__(*args, **kwargs)
    class Meta:
        model = TaskRole
        exclude = ()

class TimeMeasureForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(TimeMeasureForm, self).__init__(*args, **kwargs)
    class Meta:
        model = TimeMeasure
        exclude = ()

class ActionrelatedRoleForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ActionrelatedRoleForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ActionrelatedRole
        exclude = ()

class AbductiveHypothesisForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(AbductiveHypothesisForm, self).__init__(*args, **kwargs)
    class Meta:
        model = AbductiveHypothesis
        exclude = ()

class InductiveHypothesisForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(InductiveHypothesisForm, self).__init__(*args, **kwargs)
    class Meta:
        model = InductiveHypothesis
        exclude = ()

class AdequacyForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(AdequacyForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Adequacy
        exclude = ()

class AlternativeHypothesisForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(AlternativeHypothesisForm, self).__init__(*args, **kwargs)
    class Meta:
        model = AlternativeHypothesis
        exclude = ()

class NullHypothesisForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(NullHypothesisForm, self).__init__(*args, **kwargs)
    class Meta:
        model = NullHypothesis
        exclude = ()

class ResearchHypothesisForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ResearchHypothesisForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ResearchHypothesis
        exclude = ()

class ArticleForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ArticleForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Article
        exclude = ()

class TextForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(TextForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Text
        exclude = ()

class ArtificialLanguageForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ArtificialLanguageForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ArtificialLanguage
        exclude = ()

class LanguageForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(LanguageForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Language
        exclude = ()

class HumanLanguageForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(HumanLanguageForm, self).__init__(*args, **kwargs)
    class Meta:
        model = HumanLanguage
        exclude = ()

class SentenceForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(SentenceForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Sentence
        exclude = ()

class AtomicActionForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(AtomicActionForm, self).__init__(*args, **kwargs)
    class Meta:
        model = AtomicAction
        exclude = ()

class ComplexActionForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ComplexActionForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ComplexAction
        exclude = ()

class AuthorProtocolForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(AuthorProtocolForm, self).__init__(*args, **kwargs)
    class Meta:
        model = AuthorProtocol
        exclude = ()

class BookForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BookForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Book
        exclude = ()

class CalculableVariableForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(CalculableVariableForm, self).__init__(*args, **kwargs)
    class Meta:
        model = CalculableVariable
        exclude = ()

class CapacityRequirementsForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(CapacityRequirementsForm, self).__init__(*args, **kwargs)
    class Meta:
        model = CapacityRequirements
        exclude = ()

class EnvironmentalRequirementsForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(EnvironmentalRequirementsForm, self).__init__(*args, **kwargs)
    class Meta:
        model = EnvironmentalRequirements
        exclude = ()

class FinancialRequirementsForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(FinancialRequirementsForm, self).__init__(*args, **kwargs)
    class Meta:
        model = FinancialRequirements
        exclude = ()

class ClassificationByDomainForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ClassificationByDomainForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ClassificationByDomain
        exclude = ()

class ClassifyingForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ClassifyingForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Classifying
        exclude = ()

class DesigningExperimentForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(DesigningExperimentForm, self).__init__(*args, **kwargs)
    class Meta:
        model = DesigningExperiment
        exclude = ()

class CoarseErrorForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(CoarseErrorForm, self).__init__(*args, **kwargs)
    class Meta:
        model = CoarseError
        exclude = ()

class MeasurementErrorForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(MeasurementErrorForm, self).__init__(*args, **kwargs)
    class Meta:
        model = MeasurementError
        exclude = ()

class ComparisonControl_TargetGroupsForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ComparisonControl_TargetGroupsForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ComparisonControl_TargetGroups
        exclude = ()

class PairedComparisonForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(PairedComparisonForm, self).__init__(*args, **kwargs)
    class Meta:
        model = PairedComparison
        exclude = ()

class PairedComparisonOfMatchingGroupsForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(PairedComparisonOfMatchingGroupsForm, self).__init__(*args, **kwargs)
    class Meta:
        model = PairedComparisonOfMatchingGroups
        exclude = ()

class PairedComparisonOfSingleSampleGroupsForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(PairedComparisonOfSingleSampleGroupsForm, self).__init__(*args, **kwargs)
    class Meta:
        model = PairedComparisonOfSingleSampleGroups
        exclude = ()

class QualityControlForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(QualityControlForm, self).__init__(*args, **kwargs)
    class Meta:
        model = QualityControl
        exclude = ()

class ComplexHypothesisForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ComplexHypothesisForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ComplexHypothesis
        exclude = ()

class SingleHypothesisForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(SingleHypothesisForm, self).__init__(*args, **kwargs)
    class Meta:
        model = SingleHypothesis
        exclude = ()

class ComputationalExperimentForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ComputationalExperimentForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ComputationalExperiment
        exclude = ()

class ComputeGoalForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ComputeGoalForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ComputeGoal
        exclude = ()

class ConfirmGoalForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ConfirmGoalForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ConfirmGoal
        exclude = ()

class ExplainGoalForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ExplainGoalForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ExplainGoal
        exclude = ()

class InvestigateGoalForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(InvestigateGoalForm, self).__init__(*args, **kwargs)
    class Meta:
        model = InvestigateGoal
        exclude = ()

class ComputerSimulationForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ComputerSimulationForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ComputerSimulation
        exclude = ()

class ConstantQuantityForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ConstantQuantityForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ConstantQuantity
        exclude = ()

class ControllabilityForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ControllabilityForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Controllability
        exclude = ()

class IndependenceForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(IndependenceForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Independence
        exclude = ()

class DBReferenceForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(DBReferenceForm, self).__init__(*args, **kwargs)
    class Meta:
        model = DBReference
        exclude = ()

class DDC_Dewey_ClassificationForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(DDC_Dewey_ClassificationForm, self).__init__(*args, **kwargs)
    class Meta:
        model = DDC_Dewey_Classification
        exclude = ()

class LibraryOfCongressClassificationForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(LibraryOfCongressClassificationForm, self).__init__(*args, **kwargs)
    class Meta:
        model = LibraryOfCongressClassification
        exclude = ()

class NLMClassificationForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(NLMClassificationForm, self).__init__(*args, **kwargs)
    class Meta:
        model = NLMClassification
        exclude = ()

class ResearchCouncilsUKClassificationForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ResearchCouncilsUKClassificationForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ResearchCouncilsUKClassification
        exclude = ()

class DataRepresentationStandardForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(DataRepresentationStandardForm, self).__init__(*args, **kwargs)
    class Meta:
        model = DataRepresentationStandard
        exclude = ()

class DegreeOfModelForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(DegreeOfModelForm, self).__init__(*args, **kwargs)
    class Meta:
        model = DegreeOfModel
        exclude = ()

class DynamismOfModelForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(DynamismOfModelForm, self).__init__(*args, **kwargs)
    class Meta:
        model = DynamismOfModel
        exclude = ()

class DependentVariableForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(DependentVariableForm, self).__init__(*args, **kwargs)
    class Meta:
        model = DependentVariable
        exclude = ()

class DeviceForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(DeviceForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Device
        exclude = ()

class DomainActionForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(DomainActionForm, self).__init__(*args, **kwargs)
    class Meta:
        model = DomainAction
        exclude = ()

class DoseResponseForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(DoseResponseForm, self).__init__(*args, **kwargs)
    class Meta:
        model = DoseResponse
        exclude = ()

class GeneKnockinForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(GeneKnockinForm, self).__init__(*args, **kwargs)
    class Meta:
        model = GeneKnockin
        exclude = ()

class GeneKnockoutForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(GeneKnockoutForm, self).__init__(*args, **kwargs)
    class Meta:
        model = GeneKnockout
        exclude = ()

class Normal_DiseaseForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(Normal_DiseaseForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Normal_Disease
        exclude = ()

class TimeCourseForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(TimeCourseForm, self).__init__(*args, **kwargs)
    class Meta:
        model = TimeCourse
        exclude = ()

class Treated_UntreatedForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(Treated_UntreatedForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Treated_Untreated
        exclude = ()

class DraftStatusForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(DraftStatusForm, self).__init__(*args, **kwargs)
    class Meta:
        model = DraftStatus
        exclude = ()

class DuhemEffectForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(DuhemEffectForm, self).__init__(*args, **kwargs)
    class Meta:
        model = DuhemEffect
        exclude = ()

class ExperimentalDesignEffectForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ExperimentalDesignEffectForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ExperimentalDesignEffect
        exclude = ()

class InstrumentationEffectForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(InstrumentationEffectForm, self).__init__(*args, **kwargs)
    class Meta:
        model = InstrumentationEffect
        exclude = ()

class ObjectEffectForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ObjectEffectForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ObjectEffect
        exclude = ()

class SubjectEffectForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(SubjectEffectForm, self).__init__(*args, **kwargs)
    class Meta:
        model = SubjectEffect
        exclude = ()

class TimeEffectForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(TimeEffectForm, self).__init__(*args, **kwargs)
    class Meta:
        model = TimeEffect
        exclude = ()

class DynamicModelForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(DynamicModelForm, self).__init__(*args, **kwargs)
    class Meta:
        model = DynamicModel
        exclude = ()

class StaticModelForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(StaticModelForm, self).__init__(*args, **kwargs)
    class Meta:
        model = StaticModel
        exclude = ()

class ErrorOfConclusionForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ErrorOfConclusionForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ErrorOfConclusion
        exclude = ()

class ExperimentalActionsPlanningForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ExperimentalActionsPlanningForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ExperimentalActionsPlanning
        exclude = ()

class ExperimentalEquipmentSelectingForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ExperimentalEquipmentSelectingForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ExperimentalEquipmentSelecting
        exclude = ()

class ExperimentalModelDesignForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ExperimentalModelDesignForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ExperimentalModelDesign
        exclude = ()

class ExperimentalObjectSelectingForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ExperimentalObjectSelectingForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ExperimentalObjectSelecting
        exclude = ()

class ExperimentalConclusionForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ExperimentalConclusionForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ExperimentalConclusion
        exclude = ()

class ExperimentalProtocolForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ExperimentalProtocolForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ExperimentalProtocol
        exclude = ()

class ExperimenteeBiasForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ExperimenteeBiasForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ExperimenteeBias
        exclude = ()

class HawthorneEffectForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(HawthorneEffectForm, self).__init__(*args, **kwargs)
    class Meta:
        model = HawthorneEffect
        exclude = ()

class MortalityEffectForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(MortalityEffectForm, self).__init__(*args, **kwargs)
    class Meta:
        model = MortalityEffect
        exclude = ()

class ExperimenterBiasForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ExperimenterBiasForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ExperimenterBias
        exclude = ()

class TestingEffectForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(TestingEffectForm, self).__init__(*args, **kwargs)
    class Meta:
        model = TestingEffect
        exclude = ()

class ExplicitHypothesisForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ExplicitHypothesisForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ExplicitHypothesis
        exclude = ()

class ImplicitHypothesisForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ImplicitHypothesisForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ImplicitHypothesis
        exclude = ()

class FactorLevelForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(FactorLevelForm, self).__init__(*args, **kwargs)
    class Meta:
        model = FactorLevel
        exclude = ()

class FactorialDesignForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(FactorialDesignForm, self).__init__(*args, **kwargs)
    class Meta:
        model = FactorialDesign
        exclude = ()

class FalseNegativeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(FalseNegativeForm, self).__init__(*args, **kwargs)
    class Meta:
        model = FalseNegative
        exclude = ()

class HypothesisAcceptanceMistakeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(HypothesisAcceptanceMistakeForm, self).__init__(*args, **kwargs)
    class Meta:
        model = HypothesisAcceptanceMistake
        exclude = ()

class FalsePpositiveForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(FalsePpositiveForm, self).__init__(*args, **kwargs)
    class Meta:
        model = FalsePpositive
        exclude = ()

class FaultyComparisonForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(FaultyComparisonForm, self).__init__(*args, **kwargs)
    class Meta:
        model = FaultyComparison
        exclude = ()

class FormulaForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(FormulaForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Formula
        exclude = ()

class GalileanExperimentForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(GalileanExperimentForm, self).__init__(*args, **kwargs)
    class Meta:
        model = GalileanExperiment
        exclude = ()

class HandToolForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(HandToolForm, self).__init__(*args, **kwargs)
    class Meta:
        model = HandTool
        exclude = ()

class ToolForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ToolForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Tool
        exclude = ()

class HardwareForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(HardwareForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Hardware
        exclude = ()

class HistoryEffectForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(HistoryEffectForm, self).__init__(*args, **kwargs)
    class Meta:
        model = HistoryEffect
        exclude = ()

class MaturationEffectForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(MaturationEffectForm, self).__init__(*args, **kwargs)
    class Meta:
        model = MaturationEffect
        exclude = ()

class HypothesisdrivenExperimentForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(HypothesisdrivenExperimentForm, self).__init__(*args, **kwargs)
    class Meta:
        model = HypothesisdrivenExperiment
        exclude = ()

class HypothesisformingExperimentForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(HypothesisformingExperimentForm, self).__init__(*args, **kwargs)
    class Meta:
        model = HypothesisformingExperiment
        exclude = ()

class ImageForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ImageForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Image
        exclude = ()

class ImproperSamplingForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ImproperSamplingForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ImproperSampling
        exclude = ()

class IncompleteDataErrorForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(IncompleteDataErrorForm, self).__init__(*args, **kwargs)
    class Meta:
        model = IncompleteDataError
        exclude = ()

class IndependentVariableForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(IndependentVariableForm, self).__init__(*args, **kwargs)
    class Meta:
        model = IndependentVariable
        exclude = ()

class InferableVariableForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(InferableVariableForm, self).__init__(*args, **kwargs)
    class Meta:
        model = InferableVariable
        exclude = ()

class InternalStatusForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(InternalStatusForm, self).__init__(*args, **kwargs)
    class Meta:
        model = InternalStatus
        exclude = ()

class RestrictedStatusForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(RestrictedStatusForm, self).__init__(*args, **kwargs)
    class Meta:
        model = RestrictedStatus
        exclude = ()

class JournalForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(JournalForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Journal
        exclude = ()

class LinearModelForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(LinearModelForm, self).__init__(*args, **kwargs)
    class Meta:
        model = LinearModel
        exclude = ()

class NonlinearModelForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(NonlinearModelForm, self).__init__(*args, **kwargs)
    class Meta:
        model = NonlinearModel
        exclude = ()

class LogicalModelRepresentationForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(LogicalModelRepresentationForm, self).__init__(*args, **kwargs)
    class Meta:
        model = LogicalModelRepresentation
        exclude = ()

class MachineForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(MachineForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Machine
        exclude = ()

class MachineToolForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(MachineToolForm, self).__init__(*args, **kwargs)
    class Meta:
        model = MachineTool
        exclude = ()

class MagazineForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(MagazineForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Magazine
        exclude = ()

class MaterialsForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(MaterialsForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Materials
        exclude = ()

class MathematicalModelRepresentationForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(MathematicalModelRepresentationForm, self).__init__(*args, **kwargs)
    class Meta:
        model = MathematicalModelRepresentation
        exclude = ()

class MetabolomicExperimentForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(MetabolomicExperimentForm, self).__init__(*args, **kwargs)
    class Meta:
        model = MetabolomicExperiment
        exclude = ()

class MethodsComparisonForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(MethodsComparisonForm, self).__init__(*args, **kwargs)
    class Meta:
        model = MethodsComparison
        exclude = ()

class SubjectsComparisonForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(SubjectsComparisonForm, self).__init__(*args, **kwargs)
    class Meta:
        model = SubjectsComparison
        exclude = ()

class MicroarrayExperimentForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(MicroarrayExperimentForm, self).__init__(*args, **kwargs)
    class Meta:
        model = MicroarrayExperiment
        exclude = ()

class MultifactorExperimentForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(MultifactorExperimentForm, self).__init__(*args, **kwargs)
    class Meta:
        model = MultifactorExperiment
        exclude = ()

class OnefactorExperimentForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(OnefactorExperimentForm, self).__init__(*args, **kwargs)
    class Meta:
        model = OnefactorExperiment
        exclude = ()

class TwofactorExperimentForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(TwofactorExperimentForm, self).__init__(*args, **kwargs)
    class Meta:
        model = TwofactorExperiment
        exclude = ()

class NaturalLanguageForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(NaturalLanguageForm, self).__init__(*args, **kwargs)
    class Meta:
        model = NaturalLanguage
        exclude = ()

class NonRestrictedStatusForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(NonRestrictedStatusForm, self).__init__(*args, **kwargs)
    class Meta:
        model = NonRestrictedStatus
        exclude = ()

class NormalizationStrategyForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(NormalizationStrategyForm, self).__init__(*args, **kwargs)
    class Meta:
        model = NormalizationStrategy
        exclude = ()

class ObjectMethodForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ObjectMethodForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ObjectMethod
        exclude = ()

class ObjectOfActionForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ObjectOfActionForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ObjectOfAction
        exclude = ()

class ObservableVariableForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ObservableVariableForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ObservableVariable
        exclude = ()

class PaperForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(PaperForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Paper
        exclude = ()

class ParticlePhysicsExperimentForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ParticlePhysicsExperimentForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ParticlePhysicsExperiment
        exclude = ()

class PresentingSamplingForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(PresentingSamplingForm, self).__init__(*args, **kwargs)
    class Meta:
        model = PresentingSampling
        exclude = ()

class ProceedingsForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ProceedingsForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Proceedings
        exclude = ()

class ProgramForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ProgramForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Program
        exclude = ()

class ProteomicExperimentForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ProteomicExperimentForm, self).__init__(*args, **kwargs)
    class Meta:
        model = ProteomicExperiment
        exclude = ()

class ProviderForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ProviderForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Provider
        exclude = ()

class PublicAcademicStatusForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(PublicAcademicStatusForm, self).__init__(*args, **kwargs)
    class Meta:
        model = PublicAcademicStatus
        exclude = ()

class PublicStatusForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(PublicStatusForm, self).__init__(*args, **kwargs)
    class Meta:
        model = PublicStatus
        exclude = ()

class RandomErrorForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(RandomErrorForm, self).__init__(*args, **kwargs)
    class Meta:
        model = RandomError
        exclude = ()

class RandomSamplingForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(RandomSamplingForm, self).__init__(*args, **kwargs)
    class Meta:
        model = RandomSampling
        exclude = ()

class RecommendationSatusForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(RecommendationSatusForm, self).__init__(*args, **kwargs)
    class Meta:
        model = RecommendationSatus
        exclude = ()

class RecordingActionForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(RecordingActionForm, self).__init__(*args, **kwargs)
    class Meta:
        model = RecordingAction
        exclude = ()

class RegionForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(RegionForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Region
        exclude = ()

class RepresenationalMediumForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(RepresenationalMediumForm, self).__init__(*args, **kwargs)
    class Meta:
        model = RepresenationalMedium
        exclude = ()

class SUMOSynonymForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(SUMOSynonymForm, self).__init__(*args, **kwargs)
    class Meta:
        model = SUMOSynonym
        exclude = ()

class SampleFormingForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(SampleFormingForm, self).__init__(*args, **kwargs)
    class Meta:
        model = SampleForming
        exclude = ()

class SequentialDesignForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(SequentialDesignForm, self).__init__(*args, **kwargs)
    class Meta:
        model = SequentialDesign
        exclude = ()

class SoftwareForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(SoftwareForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Software
        exclude = ()

class SoundForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(SoundForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Sound
        exclude = ()

class SplitSamplesForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(SplitSamplesForm, self).__init__(*args, **kwargs)
    class Meta:
        model = SplitSamples
        exclude = ()

class SymmetricalMatchingForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(SymmetricalMatchingForm, self).__init__(*args, **kwargs)
    class Meta:
        model = SymmetricalMatching
        exclude = ()

class StatisticalErrorForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(StatisticalErrorForm, self).__init__(*args, **kwargs)
    class Meta:
        model = StatisticalError
        exclude = ()

class StratifiedSamplingForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(StratifiedSamplingForm, self).__init__(*args, **kwargs)
    class Meta:
        model = StratifiedSampling
        exclude = ()

class SubjectOfActionForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(SubjectOfActionForm, self).__init__(*args, **kwargs)
    class Meta:
        model = SubjectOfAction
        exclude = ()

class SubjectOfExperimentForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(SubjectOfExperimentForm, self).__init__(*args, **kwargs)
    class Meta:
        model = SubjectOfExperiment
        exclude = ()

class SubmitterForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(SubmitterForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Submitter
        exclude = ()

class SummaryForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(SummaryForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Summary
        exclude = ()

class SystematicErrorForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(SystematicErrorForm, self).__init__(*args, **kwargs)
    class Meta:
        model = SystematicError
        exclude = ()

class TechnicalReportForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(TechnicalReportForm, self).__init__(*args, **kwargs)
    class Meta:
        model = TechnicalReport
        exclude = ()

class TimeDurationForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(TimeDurationForm, self).__init__(*args, **kwargs)
    class Meta:
        model = TimeDuration
        exclude = ()

class TimeIntervalForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(TimeIntervalForm, self).__init__(*args, **kwargs)
    class Meta:
        model = TimeInterval
        exclude = ()

class URLForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(URLForm, self).__init__(*args, **kwargs)
    class Meta:
        model = URL
        exclude = ()
