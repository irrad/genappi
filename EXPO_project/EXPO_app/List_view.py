from django.db import models
from django.template.loader import render_to_string
from .models import *
from .forms import *
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseNotFound
from django.template import loader
from django.core.urlresolvers import reverse
import datetime
from .forms import *
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.core.files.storage import FileSystemStorage
from django.utils.safestring import mark_safe
from django.core import serializers


def ThingList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Thing.objects.all())
    fields = Thing._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ThingList.html",{'data':data, 'fields':new_fields})

def RepresentationFormList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", RepresentationForm.objects.all())
    fields = RepresentationForm._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"RepresentationFormList.html",{'data':data, 'fields':new_fields})

def ArtifactList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Artifact.objects.all())
    fields = Artifact._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ArtifactList.html",{'data':data, 'fields':new_fields})

def SynonymousExternalConceptList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", SynonymousExternalConcept.objects.all())
    fields = SynonymousExternalConcept._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"SynonymousExternalConceptList.html",{'data':data, 'fields':new_fields})

def NameList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Name.objects.all())
    fields = Name._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"NameList.html",{'data':data, 'fields':new_fields})

def ScientificExperimentList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ScientificExperiment.objects.all())
    fields = ScientificExperiment._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ScientificExperimentList.html",{'data':data, 'fields':new_fields})

def ExperimentalDesignList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ExperimentalDesign.objects.all())
    fields = ExperimentalDesign._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ExperimentalDesignList.html",{'data':data, 'fields':new_fields})

def AdminInfoExperimentList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", AdminInfoExperiment.objects.all())
    fields = AdminInfoExperiment._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"AdminInfoExperimentList.html",{'data':data, 'fields':new_fields})

def IDExperimentList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", IDExperiment.objects.all())
    fields = IDExperiment._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"IDExperimentList.html",{'data':data, 'fields':new_fields})

def DomainOfExperimentList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", DomainOfExperiment.objects.all())
    fields = DomainOfExperiment._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"DomainOfExperimentList.html",{'data':data, 'fields':new_fields})

def ExperimentalObservationList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ExperimentalObservation.objects.all())
    fields = ExperimentalObservation._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ExperimentalObservationList.html",{'data':data, 'fields':new_fields})

def ResultsInterpretationList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ResultsInterpretation.objects.all())
    fields = ResultsInterpretation._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ResultsInterpretationList.html",{'data':data, 'fields':new_fields})

def ExperimentalActionList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ExperimentalAction.objects.all())
    fields = ExperimentalAction._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ExperimentalActionList.html",{'data':data, 'fields':new_fields})

def ActionNameList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ActionName.objects.all())
    fields = ActionName._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ActionNameList.html",{'data':data, 'fields':new_fields})

def ActionComplexityList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ActionComplexity.objects.all())
    fields = ActionComplexity._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ActionComplexityList.html",{'data':data, 'fields':new_fields})

def HumanList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Human.objects.all())
    fields = Human._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"HumanList.html",{'data':data, 'fields':new_fields})

def OrganizationList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Organization.objects.all())
    fields = Organization._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"OrganizationList.html",{'data':data, 'fields':new_fields})

def AdminInfoAuthorList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", AdminInfoAuthor.objects.all())
    fields = AdminInfoAuthor._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"AdminInfoAuthorList.html",{'data':data, 'fields':new_fields})

def AdminInfoObjectOfExperimentList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", AdminInfoObjectOfExperiment.objects.all())
    fields = AdminInfoObjectOfExperiment._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"AdminInfoObjectOfExperimentList.html",{'data':data, 'fields':new_fields})

def AdminInfoProviderList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", AdminInfoProvider.objects.all())
    fields = AdminInfoProvider._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"AdminInfoProviderList.html",{'data':data, 'fields':new_fields})

def AdminInfoSubjectOfExperimentList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", AdminInfoSubjectOfExperiment.objects.all())
    fields = AdminInfoSubjectOfExperiment._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"AdminInfoSubjectOfExperimentList.html",{'data':data, 'fields':new_fields})

def AdminInfoSubmitterList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", AdminInfoSubmitter.objects.all())
    fields = AdminInfoSubmitter._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"AdminInfoSubmitterList.html",{'data':data, 'fields':new_fields})

def AdminInfoUserList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", AdminInfoUser.objects.all())
    fields = AdminInfoUser._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"AdminInfoUserList.html",{'data':data, 'fields':new_fields})

def ExperimentalMethodList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ExperimentalMethod.objects.all())
    fields = ExperimentalMethod._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ExperimentalMethodList.html",{'data':data, 'fields':new_fields})

def MethodApplicabilityConditionList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", MethodApplicabilityCondition.objects.all())
    fields = MethodApplicabilityCondition._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"MethodApplicabilityConditionList.html",{'data':data, 'fields':new_fields})

def StandardOperatingProcedureList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", StandardOperatingProcedure.objects.all())
    fields = StandardOperatingProcedure._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"StandardOperatingProcedureList.html",{'data':data, 'fields':new_fields})

def AuthorList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Author.objects.all())
    fields = Author._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"AuthorList.html",{'data':data, 'fields':new_fields})

def ClassificationOfDomainsList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ClassificationOfDomains.objects.all())
    fields = ClassificationOfDomains._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ClassificationOfDomainsList.html",{'data':data, 'fields':new_fields})

def ClassificationOfExperimentsList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ClassificationOfExperiments.objects.all())
    fields = ClassificationOfExperiments._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ClassificationOfExperimentsList.html",{'data':data, 'fields':new_fields})

def ExperimentalHypothesisList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ExperimentalHypothesis.objects.all())
    fields = ExperimentalHypothesis._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ExperimentalHypothesisList.html",{'data':data, 'fields':new_fields})

def HypothesisComplexityList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", HypothesisComplexity.objects.all())
    fields = HypothesisComplexity._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"HypothesisComplexityList.html",{'data':data, 'fields':new_fields})

def RepresentationList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Representation.objects.all())
    fields = Representation._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"RepresentationList.html",{'data':data, 'fields':new_fields})

def PropositionList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Proposition.objects.all())
    fields = Proposition._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"PropositionList.html",{'data':data, 'fields':new_fields})

def ExperimentalEquipmentList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ExperimentalEquipment.objects.all())
    fields = ExperimentalEquipment._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ExperimentalEquipmentList.html",{'data':data, 'fields':new_fields})

def TechnicalDescriptionList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", TechnicalDescription.objects.all())
    fields = TechnicalDescription._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"TechnicalDescriptionList.html",{'data':data, 'fields':new_fields})

def ExperimentalResultsList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ExperimentalResults.objects.all())
    fields = ExperimentalResults._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ExperimentalResultsList.html",{'data':data, 'fields':new_fields})

def ObservationalErrorList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ObservationalError.objects.all())
    fields = ObservationalError._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ObservationalErrorList.html",{'data':data, 'fields':new_fields})

def ResultErrorList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ResultError.objects.all())
    fields = ResultError._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ResultErrorList.html",{'data':data, 'fields':new_fields})

def ResultsEvaluationList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ResultsEvaluation.objects.all())
    fields = ResultsEvaluation._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ResultsEvaluationList.html",{'data':data, 'fields':new_fields})

def ScientificInvestigationList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ScientificInvestigation.objects.all())
    fields = ScientificInvestigation._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ScientificInvestigationList.html",{'data':data, 'fields':new_fields})

def ExperimentalModelList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ExperimentalModel.objects.all())
    fields = ExperimentalModel._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ExperimentalModelList.html",{'data':data, 'fields':new_fields})

def ExperimentalFactorList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ExperimentalFactor.objects.all())
    fields = ExperimentalFactor._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ExperimentalFactorList.html",{'data':data, 'fields':new_fields})

def ExperimentalTechnologyList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ExperimentalTechnology.objects.all())
    fields = ExperimentalTechnology._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ExperimentalTechnologyList.html",{'data':data, 'fields':new_fields})

def ExperimentalRequirementsList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ExperimentalRequirements.objects.all())
    fields = ExperimentalRequirements._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ExperimentalRequirementsList.html",{'data':data, 'fields':new_fields})

def HypothesisExplicitnessList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", HypothesisExplicitness.objects.all())
    fields = HypothesisExplicitness._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"HypothesisExplicitnessList.html",{'data':data, 'fields':new_fields})

def LinguisticExpressionList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", LinguisticExpression.objects.all())
    fields = LinguisticExpression._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"LinguisticExpressionList.html",{'data':data, 'fields':new_fields})

def FactRejectResearchHypothesisList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", FactRejectResearchHypothesis.objects.all())
    fields = FactRejectResearchHypothesis._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"FactRejectResearchHypothesisList.html",{'data':data, 'fields':new_fields})

def FactSupportResearchHypothesisList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", FactSupportResearchHypothesis.objects.all())
    fields = FactSupportResearchHypothesis._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"FactSupportResearchHypothesisList.html",{'data':data, 'fields':new_fields})

def NumberList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Number.objects.all())
    fields = Number._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"NumberList.html",{'data':data, 'fields':new_fields})

def GeneralityOfHypothesisList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", GeneralityOfHypothesis.objects.all())
    fields = GeneralityOfHypothesis._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"GeneralityOfHypothesisList.html",{'data':data, 'fields':new_fields})

def ActionGoalList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ActionGoal.objects.all())
    fields = ActionGoal._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ActionGoalList.html",{'data':data, 'fields':new_fields})

def ExperimentalGoalList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ExperimentalGoal.objects.all())
    fields = ExperimentalGoal._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ExperimentalGoalList.html",{'data':data, 'fields':new_fields})

def MethodGoalList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", MethodGoal.objects.all())
    fields = MethodGoal._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"MethodGoalList.html",{'data':data, 'fields':new_fields})

def HypothesisFormationList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", HypothesisFormation.objects.all())
    fields = HypothesisFormation._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"HypothesisFormationList.html",{'data':data, 'fields':new_fields})

def HypothesisRepresentationList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", HypothesisRepresentation.objects.all())
    fields = HypothesisRepresentation._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"HypothesisRepresentationList.html",{'data':data, 'fields':new_fields})

def InformationGetheringList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", InformationGethering.objects.all())
    fields = InformationGethering._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"InformationGetheringList.html",{'data':data, 'fields':new_fields})

def SampleList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Sample.objects.all())
    fields = Sample._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"SampleList.html",{'data':data, 'fields':new_fields})

def VariabilityList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Variability.objects.all())
    fields = Variability._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"VariabilityList.html",{'data':data, 'fields':new_fields})

def TimePointList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", TimePoint.objects.all())
    fields = TimePoint._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"TimePointList.html",{'data':data, 'fields':new_fields})

def UserList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", User.objects.all())
    fields = User._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"UserList.html",{'data':data, 'fields':new_fields})

def LoginNameList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", LoginName.objects.all())
    fields = LoginName._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"LoginNameList.html",{'data':data, 'fields':new_fields})

def DomainModelList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", DomainModel.objects.all())
    fields = DomainModel._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"DomainModelList.html",{'data':data, 'fields':new_fields})

def GroupExperimentalObjectList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", GroupExperimentalObject.objects.all())
    fields = GroupExperimentalObject._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"GroupExperimentalObjectList.html",{'data':data, 'fields':new_fields})

def ObjectOfExperimentList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ObjectOfExperiment.objects.all())
    fields = ObjectOfExperiment._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ObjectOfExperimentList.html",{'data':data, 'fields':new_fields})

def ClassificationByModelList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ClassificationByModel.objects.all())
    fields = ClassificationByModel._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ClassificationByModelList.html",{'data':data, 'fields':new_fields})

def StatisticsCharacteristicList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", StatisticsCharacteristic.objects.all())
    fields = StatisticsCharacteristic._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"StatisticsCharacteristicList.html",{'data':data, 'fields':new_fields})

def ParentGroupList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ParentGroup.objects.all())
    fields = ParentGroup._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ParentGroupList.html",{'data':data, 'fields':new_fields})

def PasswordList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Password.objects.all())
    fields = Password._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"PasswordList.html",{'data':data, 'fields':new_fields})

def ProcedureExecuteExperimentList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ProcedureExecuteExperiment.objects.all())
    fields = ProcedureExecuteExperiment._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ProcedureExecuteExperimentList.html",{'data':data, 'fields':new_fields})

def PlanOfExperimentalActionsList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", PlanOfExperimentalActions.objects.all())
    fields = PlanOfExperimentalActions._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"PlanOfExperimentalActionsList.html",{'data':data, 'fields':new_fields})

def PoblemAnalysisList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", PoblemAnalysis.objects.all())
    fields = PoblemAnalysis._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"PoblemAnalysisList.html",{'data':data, 'fields':new_fields})

def SubjectQualificationList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", SubjectQualification.objects.all())
    fields = SubjectQualification._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"SubjectQualificationList.html",{'data':data, 'fields':new_fields})

def BiblioReferenceList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BiblioReference.objects.all())
    fields = BiblioReference._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BiblioReferenceList.html",{'data':data, 'fields':new_fields})

def ModelList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Model.objects.all())
    fields = Model._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ModelList.html",{'data':data, 'fields':new_fields})

def ModelRepresentationList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ModelRepresentation.objects.all())
    fields = ModelRepresentation._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ModelRepresentationList.html",{'data':data, 'fields':new_fields})

def RepresentationExperimentalExecutionProcedureList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", RepresentationExperimentalExecutionProcedure.objects.all())
    fields = RepresentationExperimentalExecutionProcedure._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"RepresentationExperimentalExecutionProcedureList.html",{'data':data, 'fields':new_fields})

def RepresentationExperimentalGoalList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", RepresentationExperimentalGoal.objects.all())
    fields = RepresentationExperimentalGoal._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"RepresentationExperimentalGoalList.html",{'data':data, 'fields':new_fields})

def SampleRepresentativenessList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", SampleRepresentativeness.objects.all())
    fields = SampleRepresentativeness._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"SampleRepresentativenessList.html",{'data':data, 'fields':new_fields})

def SampleSizeList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", SampleSize.objects.all())
    fields = SampleSize._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"SampleSizeList.html",{'data':data, 'fields':new_fields})

def SamplingMethodList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", SamplingMethod.objects.all())
    fields = SamplingMethod._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"SamplingMethodList.html",{'data':data, 'fields':new_fields})

def SystematicSamplingList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", SystematicSampling.objects.all())
    fields = SystematicSampling._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"SystematicSamplingList.html",{'data':data, 'fields':new_fields})

def SamplingRuleList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", SamplingRule.objects.all())
    fields = SamplingRule._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"SamplingRuleList.html",{'data':data, 'fields':new_fields})

def ExperimentalStandardList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ExperimentalStandard.objects.all())
    fields = ExperimentalStandard._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ExperimentalStandardList.html",{'data':data, 'fields':new_fields})

def ErrorList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Error.objects.all())
    fields = Error._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ErrorList.html",{'data':data, 'fields':new_fields})

def DispersionList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Dispersion.objects.all())
    fields = Dispersion._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"DispersionList.html",{'data':data, 'fields':new_fields})

def LevelOfSignificanceList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", LevelOfSignificance.objects.all())
    fields = LevelOfSignificance._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"LevelOfSignificanceList.html",{'data':data, 'fields':new_fields})

def StatusExperimentalDocumentList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", StatusExperimentalDocument.objects.all())
    fields = StatusExperimentalDocument._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"StatusExperimentalDocumentList.html",{'data':data, 'fields':new_fields})

def ExperimentalDesignStrategyList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ExperimentalDesignStrategy.objects.all())
    fields = ExperimentalDesignStrategy._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ExperimentalDesignStrategyList.html",{'data':data, 'fields':new_fields})

def ExperimentalSubgoalList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ExperimentalSubgoal.objects.all())
    fields = ExperimentalSubgoal._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ExperimentalSubgoalList.html",{'data':data, 'fields':new_fields})

def SubjectMethodList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", SubjectMethod.objects.all())
    fields = SubjectMethod._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"SubjectMethodList.html",{'data':data, 'fields':new_fields})

def BaconianExperimentList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", BaconianExperiment.objects.all())
    fields = BaconianExperiment._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BaconianExperimentList.html",{'data':data, 'fields':new_fields})

def TargetVariableList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", TargetVariable.objects.all())
    fields = TargetVariable._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"TargetVariableList.html",{'data':data, 'fields':new_fields})

def TitleList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Title.objects.all())
    fields = Title._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"TitleList.html",{'data':data, 'fields':new_fields})

def ValueEstimateList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ValueEstimate.objects.all())
    fields = ValueEstimate._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ValueEstimateList.html",{'data':data, 'fields':new_fields})

def ValueOfVariableList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ValueOfVariable.objects.all())
    fields = ValueOfVariable._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ValueOfVariableList.html",{'data':data, 'fields':new_fields})

def RoleList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Role.objects.all())
    fields = Role._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"RoleList.html",{'data':data, 'fields':new_fields})

def CorpuscularObjectList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", CorpuscularObject.objects.all())
    fields = CorpuscularObject._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"CorpuscularObjectList.html",{'data':data, 'fields':new_fields})

def AttributeRoleList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", AttributeRole.objects.all())
    fields = AttributeRole._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"AttributeRoleList.html",{'data':data, 'fields':new_fields})

def AttributeList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Attribute.objects.all())
    fields = Attribute._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"AttributeList.html",{'data':data, 'fields':new_fields})

def ProcedureList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Procedure.objects.all())
    fields = Procedure._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ProcedureList.html",{'data':data, 'fields':new_fields})

def AdministrativeInformationList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", AdministrativeInformation.objects.all())
    fields = AdministrativeInformation._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"AdministrativeInformationList.html",{'data':data, 'fields':new_fields})

def TitleExperimentList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", TitleExperiment.objects.all())
    fields = TitleExperiment._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"TitleExperimentList.html",{'data':data, 'fields':new_fields})

def NameEquipmentList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", NameEquipment.objects.all())
    fields = NameEquipment._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"NameEquipmentList.html",{'data':data, 'fields':new_fields})

def PersonalNameList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", PersonalName.objects.all())
    fields = PersonalName._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"PersonalNameList.html",{'data':data, 'fields':new_fields})

def FieldOfStudyList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", FieldOfStudy.objects.all())
    fields = FieldOfStudy._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"FieldOfStudyList.html",{'data':data, 'fields':new_fields})

def RelatedDomainList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", RelatedDomain.objects.all())
    fields = RelatedDomain._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"RelatedDomainList.html",{'data':data, 'fields':new_fields})

def ProductRoleList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ProductRole.objects.all())
    fields = ProductRole._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ProductRoleList.html",{'data':data, 'fields':new_fields})

def ScientificTaskList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ScientificTask.objects.all())
    fields = ScientificTask._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ScientificTaskList.html",{'data':data, 'fields':new_fields})

def ExecutionOfExperimentList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ExecutionOfExperiment.objects.all())
    fields = ExecutionOfExperiment._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ExecutionOfExperimentList.html",{'data':data, 'fields':new_fields})

def CorporateNameList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", CorporateName.objects.all())
    fields = CorporateName._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"CorporateNameList.html",{'data':data, 'fields':new_fields})

def AttributeOfActionList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", AttributeOfAction.objects.all())
    fields = AttributeOfAction._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"AttributeOfActionList.html",{'data':data, 'fields':new_fields})

def SentientAgentList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", SentientAgent.objects.all())
    fields = SentientAgent._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"SentientAgentList.html",{'data':data, 'fields':new_fields})

def RobotList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Robot.objects.all())
    fields = Robot._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"RobotList.html",{'data':data, 'fields':new_fields})

def GroupList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Group.objects.all())
    fields = Group._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"GroupList.html",{'data':data, 'fields':new_fields})

def EntityList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Entity.objects.all())
    fields = Entity._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"EntityList.html",{'data':data, 'fields':new_fields})

def ResearchMethodList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ResearchMethod.objects.all())
    fields = ResearchMethod._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ResearchMethodList.html",{'data':data, 'fields':new_fields})

def RequirementsList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Requirements.objects.all())
    fields = Requirements._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"RequirementsList.html",{'data':data, 'fields':new_fields})

def AuthorSOPList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", AuthorSOP.objects.all())
    fields = AuthorSOP._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"AuthorSOPList.html",{'data':data, 'fields':new_fields})

def SubjectRoleList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", SubjectRole.objects.all())
    fields = SubjectRole._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"SubjectRoleList.html",{'data':data, 'fields':new_fields})

def ClassificationList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Classification.objects.all())
    fields = Classification._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ClassificationList.html",{'data':data, 'fields':new_fields})

def FactList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Fact.objects.all())
    fields = Fact._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"FactList.html",{'data':data, 'fields':new_fields})

def ModelAssumptionList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ModelAssumption.objects.all())
    fields = ModelAssumption._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ModelAssumptionList.html",{'data':data, 'fields':new_fields})

def VariableList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Variable.objects.all())
    fields = Variable._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"VariableList.html",{'data':data, 'fields':new_fields})

def AttributeOfHypothesisList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", AttributeOfHypothesis.objects.all())
    fields = AttributeOfHypothesis._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"AttributeOfHypothesisList.html",{'data':data, 'fields':new_fields})

def ContentBearingObjectList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ContentBearingObject.objects.all())
    fields = ContentBearingObject._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ContentBearingObjectList.html",{'data':data, 'fields':new_fields})

def AbstractList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Abstract.objects.all())
    fields = Abstract._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"AbstractList.html",{'data':data, 'fields':new_fields})

def QuantityList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Quantity.objects.all())
    fields = Quantity._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"QuantityList.html",{'data':data, 'fields':new_fields})

def RelationList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Relation.objects.all())
    fields = Relation._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"RelationList.html",{'data':data, 'fields':new_fields})

def ProcessrelatedRoleList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ProcessrelatedRole.objects.all())
    fields = ProcessrelatedRole._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ProcessrelatedRoleList.html",{'data':data, 'fields':new_fields})

def ContentBearingPhysicalList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ContentBearingPhysical.objects.all())
    fields = ContentBearingPhysical._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ContentBearingPhysicalList.html",{'data':data, 'fields':new_fields})

def PhysicalQuantityList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", PhysicalQuantity.objects.all())
    fields = PhysicalQuantity._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"PhysicalQuantityList.html",{'data':data, 'fields':new_fields})

def GoalList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Goal.objects.all())
    fields = Goal._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"GoalList.html",{'data':data, 'fields':new_fields})

def RepresentationExperimentalObservationList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", RepresentationExperimentalObservation.objects.all())
    fields = RepresentationExperimentalObservation._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"RepresentationExperimentalObservationList.html",{'data':data, 'fields':new_fields})

def RepresentationExperimentalResultsList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", RepresentationExperimentalResults.objects.all())
    fields = RepresentationExperimentalResults._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"RepresentationExperimentalResultsList.html",{'data':data, 'fields':new_fields})

def AttributeGroupList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", AttributeGroup.objects.all())
    fields = AttributeGroup._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"AttributeGroupList.html",{'data':data, 'fields':new_fields})

def TimePositionList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", TimePosition.objects.all())
    fields = TimePosition._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"TimePositionList.html",{'data':data, 'fields':new_fields})

def ActorRoleList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ActorRole.objects.all())
    fields = ActorRole._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ActorRoleList.html",{'data':data, 'fields':new_fields})

def DocumentStageList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", DocumentStage.objects.all())
    fields = DocumentStage._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"DocumentStageList.html",{'data':data, 'fields':new_fields})

def PermissionStatusList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", PermissionStatus.objects.all())
    fields = PermissionStatus._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"PermissionStatusList.html",{'data':data, 'fields':new_fields})

def PlanList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Plan.objects.all())
    fields = Plan._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"PlanList.html",{'data':data, 'fields':new_fields})

def ReferenceList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Reference.objects.all())
    fields = Reference._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ReferenceList.html",{'data':data, 'fields':new_fields})

def AttributeSampleList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", AttributeSample.objects.all())
    fields = AttributeSample._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"AttributeSampleList.html",{'data':data, 'fields':new_fields})

def ExperimentalRuleList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ExperimentalRule.objects.all())
    fields = ExperimentalRule._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ExperimentalRuleList.html",{'data':data, 'fields':new_fields})

def RobustnessList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Robustness.objects.all())
    fields = Robustness._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"RobustnessList.html",{'data':data, 'fields':new_fields})

def ValidityList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Validity.objects.all())
    fields = Validity._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ValidityList.html",{'data':data, 'fields':new_fields})

def AttributeOfDocumentList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", AttributeOfDocument.objects.all())
    fields = AttributeOfDocument._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"AttributeOfDocumentList.html",{'data':data, 'fields':new_fields})

def PhysicalExperimentList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", PhysicalExperiment.objects.all())
    fields = PhysicalExperiment._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"PhysicalExperimentList.html",{'data':data, 'fields':new_fields})

def ComputationalDataList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ComputationalData.objects.all())
    fields = ComputationalData._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ComputationalDataList.html",{'data':data, 'fields':new_fields})

def PredicateList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Predicate.objects.all())
    fields = Predicate._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"PredicateList.html",{'data':data, 'fields':new_fields})

def SelfConnectedObjectList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", SelfConnectedObject.objects.all())
    fields = SelfConnectedObject._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"SelfConnectedObjectList.html",{'data':data, 'fields':new_fields})

def ScientificActivityList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ScientificActivity.objects.all())
    fields = ScientificActivity._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ScientificActivityList.html",{'data':data, 'fields':new_fields})

def FormingClassificationSystemList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", FormingClassificationSystem.objects.all())
    fields = FormingClassificationSystem._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"FormingClassificationSystemList.html",{'data':data, 'fields':new_fields})

def HypothesisFormingList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", HypothesisForming.objects.all())
    fields = HypothesisForming._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"HypothesisFormingList.html",{'data':data, 'fields':new_fields})

def InterpretingResultList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", InterpretingResult.objects.all())
    fields = InterpretingResult._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"InterpretingResultList.html",{'data':data, 'fields':new_fields})

def ProcessProblemAnalysisList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ProcessProblemAnalysis.objects.all())
    fields = ProcessProblemAnalysis._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ProcessProblemAnalysisList.html",{'data':data, 'fields':new_fields})

def ResultEvaluatingList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ResultEvaluating.objects.all())
    fields = ResultEvaluating._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ResultEvaluatingList.html",{'data':data, 'fields':new_fields})

def AttributeOfModelList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", AttributeOfModel.objects.all())
    fields = AttributeOfModel._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"AttributeOfModelList.html",{'data':data, 'fields':new_fields})

def AttributeOfVariableList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", AttributeOfVariable.objects.all())
    fields = AttributeOfVariable._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"AttributeOfVariableList.html",{'data':data, 'fields':new_fields})

def AgentList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Agent.objects.all())
    fields = Agent._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"AgentList.html",{'data':data, 'fields':new_fields})

def CollectionList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Collection.objects.all())
    fields = Collection._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"CollectionList.html",{'data':data, 'fields':new_fields})

def EperimentalDesignTaskList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", EperimentalDesignTask.objects.all())
    fields = EperimentalDesignTask._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"EperimentalDesignTaskList.html",{'data':data, 'fields':new_fields})

def PhysicalList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Physical.objects.all())
    fields = Physical._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"PhysicalList.html",{'data':data, 'fields':new_fields})

def ObjectList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Object.objects.all())
    fields = Object._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ObjectList.html",{'data':data, 'fields':new_fields})

def ProcessList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Process.objects.all())
    fields = Process._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ProcessList.html",{'data':data, 'fields':new_fields})

def TaskRoleList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", TaskRole.objects.all())
    fields = TaskRole._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"TaskRoleList.html",{'data':data, 'fields':new_fields})

def TimeMeasureList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", TimeMeasure.objects.all())
    fields = TimeMeasure._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"TimeMeasureList.html",{'data':data, 'fields':new_fields})

def ActionrelatedRoleList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ActionrelatedRole.objects.all())
    fields = ActionrelatedRole._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ActionrelatedRoleList.html",{'data':data, 'fields':new_fields})

def AbductiveHypothesisList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", AbductiveHypothesis.objects.all())
    fields = AbductiveHypothesis._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"AbductiveHypothesisList.html",{'data':data, 'fields':new_fields})

def InductiveHypothesisList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", InductiveHypothesis.objects.all())
    fields = InductiveHypothesis._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"InductiveHypothesisList.html",{'data':data, 'fields':new_fields})

def AdequacyList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Adequacy.objects.all())
    fields = Adequacy._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"AdequacyList.html",{'data':data, 'fields':new_fields})

def AlternativeHypothesisList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", AlternativeHypothesis.objects.all())
    fields = AlternativeHypothesis._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"AlternativeHypothesisList.html",{'data':data, 'fields':new_fields})

def NullHypothesisList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", NullHypothesis.objects.all())
    fields = NullHypothesis._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"NullHypothesisList.html",{'data':data, 'fields':new_fields})

def ResearchHypothesisList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ResearchHypothesis.objects.all())
    fields = ResearchHypothesis._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ResearchHypothesisList.html",{'data':data, 'fields':new_fields})

def ArticleList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Article.objects.all())
    fields = Article._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ArticleList.html",{'data':data, 'fields':new_fields})

def TextList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Text.objects.all())
    fields = Text._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"TextList.html",{'data':data, 'fields':new_fields})

def ArtificialLanguageList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ArtificialLanguage.objects.all())
    fields = ArtificialLanguage._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ArtificialLanguageList.html",{'data':data, 'fields':new_fields})

def LanguageList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Language.objects.all())
    fields = Language._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"LanguageList.html",{'data':data, 'fields':new_fields})

def HumanLanguageList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", HumanLanguage.objects.all())
    fields = HumanLanguage._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"HumanLanguageList.html",{'data':data, 'fields':new_fields})

def SentenceList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Sentence.objects.all())
    fields = Sentence._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"SentenceList.html",{'data':data, 'fields':new_fields})

def AtomicActionList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", AtomicAction.objects.all())
    fields = AtomicAction._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"AtomicActionList.html",{'data':data, 'fields':new_fields})

def ComplexActionList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ComplexAction.objects.all())
    fields = ComplexAction._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ComplexActionList.html",{'data':data, 'fields':new_fields})

def AuthorProtocolList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", AuthorProtocol.objects.all())
    fields = AuthorProtocol._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"AuthorProtocolList.html",{'data':data, 'fields':new_fields})

def BookList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Book.objects.all())
    fields = Book._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"BookList.html",{'data':data, 'fields':new_fields})

def CalculableVariableList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", CalculableVariable.objects.all())
    fields = CalculableVariable._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"CalculableVariableList.html",{'data':data, 'fields':new_fields})

def CapacityRequirementsList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", CapacityRequirements.objects.all())
    fields = CapacityRequirements._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"CapacityRequirementsList.html",{'data':data, 'fields':new_fields})

def EnvironmentalRequirementsList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", EnvironmentalRequirements.objects.all())
    fields = EnvironmentalRequirements._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"EnvironmentalRequirementsList.html",{'data':data, 'fields':new_fields})

def FinancialRequirementsList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", FinancialRequirements.objects.all())
    fields = FinancialRequirements._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"FinancialRequirementsList.html",{'data':data, 'fields':new_fields})

def ClassificationByDomainList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ClassificationByDomain.objects.all())
    fields = ClassificationByDomain._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ClassificationByDomainList.html",{'data':data, 'fields':new_fields})

def ClassifyingList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Classifying.objects.all())
    fields = Classifying._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ClassifyingList.html",{'data':data, 'fields':new_fields})

def DesigningExperimentList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", DesigningExperiment.objects.all())
    fields = DesigningExperiment._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"DesigningExperimentList.html",{'data':data, 'fields':new_fields})

def CoarseErrorList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", CoarseError.objects.all())
    fields = CoarseError._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"CoarseErrorList.html",{'data':data, 'fields':new_fields})

def MeasurementErrorList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", MeasurementError.objects.all())
    fields = MeasurementError._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"MeasurementErrorList.html",{'data':data, 'fields':new_fields})

def ComparisonControl_TargetGroupsList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ComparisonControl_TargetGroups.objects.all())
    fields = ComparisonControl_TargetGroups._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ComparisonControl_TargetGroupsList.html",{'data':data, 'fields':new_fields})

def PairedComparisonList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", PairedComparison.objects.all())
    fields = PairedComparison._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"PairedComparisonList.html",{'data':data, 'fields':new_fields})

def PairedComparisonOfMatchingGroupsList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", PairedComparisonOfMatchingGroups.objects.all())
    fields = PairedComparisonOfMatchingGroups._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"PairedComparisonOfMatchingGroupsList.html",{'data':data, 'fields':new_fields})

def PairedComparisonOfSingleSampleGroupsList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", PairedComparisonOfSingleSampleGroups.objects.all())
    fields = PairedComparisonOfSingleSampleGroups._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"PairedComparisonOfSingleSampleGroupsList.html",{'data':data, 'fields':new_fields})

def QualityControlList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", QualityControl.objects.all())
    fields = QualityControl._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"QualityControlList.html",{'data':data, 'fields':new_fields})

def ComplexHypothesisList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ComplexHypothesis.objects.all())
    fields = ComplexHypothesis._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ComplexHypothesisList.html",{'data':data, 'fields':new_fields})

def SingleHypothesisList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", SingleHypothesis.objects.all())
    fields = SingleHypothesis._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"SingleHypothesisList.html",{'data':data, 'fields':new_fields})

def ComputationalExperimentList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ComputationalExperiment.objects.all())
    fields = ComputationalExperiment._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ComputationalExperimentList.html",{'data':data, 'fields':new_fields})

def ComputeGoalList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ComputeGoal.objects.all())
    fields = ComputeGoal._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ComputeGoalList.html",{'data':data, 'fields':new_fields})

def ConfirmGoalList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ConfirmGoal.objects.all())
    fields = ConfirmGoal._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ConfirmGoalList.html",{'data':data, 'fields':new_fields})

def ExplainGoalList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ExplainGoal.objects.all())
    fields = ExplainGoal._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ExplainGoalList.html",{'data':data, 'fields':new_fields})

def InvestigateGoalList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", InvestigateGoal.objects.all())
    fields = InvestigateGoal._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"InvestigateGoalList.html",{'data':data, 'fields':new_fields})

def ComputerSimulationList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ComputerSimulation.objects.all())
    fields = ComputerSimulation._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ComputerSimulationList.html",{'data':data, 'fields':new_fields})

def ConstantQuantityList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ConstantQuantity.objects.all())
    fields = ConstantQuantity._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ConstantQuantityList.html",{'data':data, 'fields':new_fields})

def ControllabilityList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Controllability.objects.all())
    fields = Controllability._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ControllabilityList.html",{'data':data, 'fields':new_fields})

def IndependenceList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Independence.objects.all())
    fields = Independence._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"IndependenceList.html",{'data':data, 'fields':new_fields})

def DBReferenceList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", DBReference.objects.all())
    fields = DBReference._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"DBReferenceList.html",{'data':data, 'fields':new_fields})

def DDC_Dewey_ClassificationList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", DDC_Dewey_Classification.objects.all())
    fields = DDC_Dewey_Classification._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"DDC_Dewey_ClassificationList.html",{'data':data, 'fields':new_fields})

def LibraryOfCongressClassificationList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", LibraryOfCongressClassification.objects.all())
    fields = LibraryOfCongressClassification._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"LibraryOfCongressClassificationList.html",{'data':data, 'fields':new_fields})

def NLMClassificationList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", NLMClassification.objects.all())
    fields = NLMClassification._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"NLMClassificationList.html",{'data':data, 'fields':new_fields})

def ResearchCouncilsUKClassificationList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ResearchCouncilsUKClassification.objects.all())
    fields = ResearchCouncilsUKClassification._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ResearchCouncilsUKClassificationList.html",{'data':data, 'fields':new_fields})

def DataRepresentationStandardList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", DataRepresentationStandard.objects.all())
    fields = DataRepresentationStandard._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"DataRepresentationStandardList.html",{'data':data, 'fields':new_fields})

def DegreeOfModelList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", DegreeOfModel.objects.all())
    fields = DegreeOfModel._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"DegreeOfModelList.html",{'data':data, 'fields':new_fields})

def DynamismOfModelList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", DynamismOfModel.objects.all())
    fields = DynamismOfModel._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"DynamismOfModelList.html",{'data':data, 'fields':new_fields})

def DependentVariableList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", DependentVariable.objects.all())
    fields = DependentVariable._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"DependentVariableList.html",{'data':data, 'fields':new_fields})

def DeviceList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Device.objects.all())
    fields = Device._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"DeviceList.html",{'data':data, 'fields':new_fields})

def DomainActionList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", DomainAction.objects.all())
    fields = DomainAction._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"DomainActionList.html",{'data':data, 'fields':new_fields})

def DoseResponseList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", DoseResponse.objects.all())
    fields = DoseResponse._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"DoseResponseList.html",{'data':data, 'fields':new_fields})

def GeneKnockinList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", GeneKnockin.objects.all())
    fields = GeneKnockin._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"GeneKnockinList.html",{'data':data, 'fields':new_fields})

def GeneKnockoutList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", GeneKnockout.objects.all())
    fields = GeneKnockout._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"GeneKnockoutList.html",{'data':data, 'fields':new_fields})

def Normal_DiseaseList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Normal_Disease.objects.all())
    fields = Normal_Disease._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"Normal_DiseaseList.html",{'data':data, 'fields':new_fields})

def TimeCourseList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", TimeCourse.objects.all())
    fields = TimeCourse._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"TimeCourseList.html",{'data':data, 'fields':new_fields})

def Treated_UntreatedList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Treated_Untreated.objects.all())
    fields = Treated_Untreated._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"Treated_UntreatedList.html",{'data':data, 'fields':new_fields})

def DraftStatusList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", DraftStatus.objects.all())
    fields = DraftStatus._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"DraftStatusList.html",{'data':data, 'fields':new_fields})

def DuhemEffectList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", DuhemEffect.objects.all())
    fields = DuhemEffect._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"DuhemEffectList.html",{'data':data, 'fields':new_fields})

def ExperimentalDesignEffectList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ExperimentalDesignEffect.objects.all())
    fields = ExperimentalDesignEffect._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ExperimentalDesignEffectList.html",{'data':data, 'fields':new_fields})

def InstrumentationEffectList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", InstrumentationEffect.objects.all())
    fields = InstrumentationEffect._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"InstrumentationEffectList.html",{'data':data, 'fields':new_fields})

def ObjectEffectList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ObjectEffect.objects.all())
    fields = ObjectEffect._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ObjectEffectList.html",{'data':data, 'fields':new_fields})

def SubjectEffectList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", SubjectEffect.objects.all())
    fields = SubjectEffect._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"SubjectEffectList.html",{'data':data, 'fields':new_fields})

def TimeEffectList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", TimeEffect.objects.all())
    fields = TimeEffect._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"TimeEffectList.html",{'data':data, 'fields':new_fields})

def DynamicModelList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", DynamicModel.objects.all())
    fields = DynamicModel._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"DynamicModelList.html",{'data':data, 'fields':new_fields})

def StaticModelList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", StaticModel.objects.all())
    fields = StaticModel._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"StaticModelList.html",{'data':data, 'fields':new_fields})

def ErrorOfConclusionList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ErrorOfConclusion.objects.all())
    fields = ErrorOfConclusion._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ErrorOfConclusionList.html",{'data':data, 'fields':new_fields})

def ExperimentalActionsPlanningList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ExperimentalActionsPlanning.objects.all())
    fields = ExperimentalActionsPlanning._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ExperimentalActionsPlanningList.html",{'data':data, 'fields':new_fields})

def ExperimentalEquipmentSelectingList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ExperimentalEquipmentSelecting.objects.all())
    fields = ExperimentalEquipmentSelecting._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ExperimentalEquipmentSelectingList.html",{'data':data, 'fields':new_fields})

def ExperimentalModelDesignList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ExperimentalModelDesign.objects.all())
    fields = ExperimentalModelDesign._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ExperimentalModelDesignList.html",{'data':data, 'fields':new_fields})

def ExperimentalObjectSelectingList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ExperimentalObjectSelecting.objects.all())
    fields = ExperimentalObjectSelecting._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ExperimentalObjectSelectingList.html",{'data':data, 'fields':new_fields})

def ExperimentalConclusionList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ExperimentalConclusion.objects.all())
    fields = ExperimentalConclusion._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ExperimentalConclusionList.html",{'data':data, 'fields':new_fields})

def ExperimentalProtocolList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ExperimentalProtocol.objects.all())
    fields = ExperimentalProtocol._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ExperimentalProtocolList.html",{'data':data, 'fields':new_fields})

def ExperimenteeBiasList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ExperimenteeBias.objects.all())
    fields = ExperimenteeBias._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ExperimenteeBiasList.html",{'data':data, 'fields':new_fields})

def HawthorneEffectList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", HawthorneEffect.objects.all())
    fields = HawthorneEffect._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"HawthorneEffectList.html",{'data':data, 'fields':new_fields})

def MortalityEffectList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", MortalityEffect.objects.all())
    fields = MortalityEffect._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"MortalityEffectList.html",{'data':data, 'fields':new_fields})

def ExperimenterBiasList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ExperimenterBias.objects.all())
    fields = ExperimenterBias._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ExperimenterBiasList.html",{'data':data, 'fields':new_fields})

def TestingEffectList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", TestingEffect.objects.all())
    fields = TestingEffect._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"TestingEffectList.html",{'data':data, 'fields':new_fields})

def ExplicitHypothesisList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ExplicitHypothesis.objects.all())
    fields = ExplicitHypothesis._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ExplicitHypothesisList.html",{'data':data, 'fields':new_fields})

def ImplicitHypothesisList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ImplicitHypothesis.objects.all())
    fields = ImplicitHypothesis._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ImplicitHypothesisList.html",{'data':data, 'fields':new_fields})

def FactorLevelList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", FactorLevel.objects.all())
    fields = FactorLevel._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"FactorLevelList.html",{'data':data, 'fields':new_fields})

def FactorialDesignList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", FactorialDesign.objects.all())
    fields = FactorialDesign._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"FactorialDesignList.html",{'data':data, 'fields':new_fields})

def FalseNegativeList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", FalseNegative.objects.all())
    fields = FalseNegative._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"FalseNegativeList.html",{'data':data, 'fields':new_fields})

def HypothesisAcceptanceMistakeList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", HypothesisAcceptanceMistake.objects.all())
    fields = HypothesisAcceptanceMistake._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"HypothesisAcceptanceMistakeList.html",{'data':data, 'fields':new_fields})

def FalsePpositiveList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", FalsePpositive.objects.all())
    fields = FalsePpositive._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"FalsePpositiveList.html",{'data':data, 'fields':new_fields})

def FaultyComparisonList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", FaultyComparison.objects.all())
    fields = FaultyComparison._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"FaultyComparisonList.html",{'data':data, 'fields':new_fields})

def FormulaList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Formula.objects.all())
    fields = Formula._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"FormulaList.html",{'data':data, 'fields':new_fields})

def GalileanExperimentList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", GalileanExperiment.objects.all())
    fields = GalileanExperiment._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"GalileanExperimentList.html",{'data':data, 'fields':new_fields})

def HandToolList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", HandTool.objects.all())
    fields = HandTool._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"HandToolList.html",{'data':data, 'fields':new_fields})

def ToolList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Tool.objects.all())
    fields = Tool._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ToolList.html",{'data':data, 'fields':new_fields})

def HardwareList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Hardware.objects.all())
    fields = Hardware._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"HardwareList.html",{'data':data, 'fields':new_fields})

def HistoryEffectList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", HistoryEffect.objects.all())
    fields = HistoryEffect._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"HistoryEffectList.html",{'data':data, 'fields':new_fields})

def MaturationEffectList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", MaturationEffect.objects.all())
    fields = MaturationEffect._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"MaturationEffectList.html",{'data':data, 'fields':new_fields})

def HypothesisdrivenExperimentList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", HypothesisdrivenExperiment.objects.all())
    fields = HypothesisdrivenExperiment._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"HypothesisdrivenExperimentList.html",{'data':data, 'fields':new_fields})

def HypothesisformingExperimentList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", HypothesisformingExperiment.objects.all())
    fields = HypothesisformingExperiment._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"HypothesisformingExperimentList.html",{'data':data, 'fields':new_fields})

def ImageList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Image.objects.all())
    fields = Image._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ImageList.html",{'data':data, 'fields':new_fields})

def ImproperSamplingList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ImproperSampling.objects.all())
    fields = ImproperSampling._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ImproperSamplingList.html",{'data':data, 'fields':new_fields})

def IncompleteDataErrorList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", IncompleteDataError.objects.all())
    fields = IncompleteDataError._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"IncompleteDataErrorList.html",{'data':data, 'fields':new_fields})

def IndependentVariableList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", IndependentVariable.objects.all())
    fields = IndependentVariable._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"IndependentVariableList.html",{'data':data, 'fields':new_fields})

def InferableVariableList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", InferableVariable.objects.all())
    fields = InferableVariable._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"InferableVariableList.html",{'data':data, 'fields':new_fields})

def InternalStatusList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", InternalStatus.objects.all())
    fields = InternalStatus._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"InternalStatusList.html",{'data':data, 'fields':new_fields})

def RestrictedStatusList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", RestrictedStatus.objects.all())
    fields = RestrictedStatus._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"RestrictedStatusList.html",{'data':data, 'fields':new_fields})

def JournalList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Journal.objects.all())
    fields = Journal._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"JournalList.html",{'data':data, 'fields':new_fields})

def LinearModelList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", LinearModel.objects.all())
    fields = LinearModel._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"LinearModelList.html",{'data':data, 'fields':new_fields})

def NonlinearModelList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", NonlinearModel.objects.all())
    fields = NonlinearModel._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"NonlinearModelList.html",{'data':data, 'fields':new_fields})

def LogicalModelRepresentationList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", LogicalModelRepresentation.objects.all())
    fields = LogicalModelRepresentation._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"LogicalModelRepresentationList.html",{'data':data, 'fields':new_fields})

def MachineList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Machine.objects.all())
    fields = Machine._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"MachineList.html",{'data':data, 'fields':new_fields})

def MachineToolList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", MachineTool.objects.all())
    fields = MachineTool._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"MachineToolList.html",{'data':data, 'fields':new_fields})

def MagazineList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Magazine.objects.all())
    fields = Magazine._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"MagazineList.html",{'data':data, 'fields':new_fields})

def MaterialsList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Materials.objects.all())
    fields = Materials._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"MaterialsList.html",{'data':data, 'fields':new_fields})

def MathematicalModelRepresentationList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", MathematicalModelRepresentation.objects.all())
    fields = MathematicalModelRepresentation._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"MathematicalModelRepresentationList.html",{'data':data, 'fields':new_fields})

def MetabolomicExperimentList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", MetabolomicExperiment.objects.all())
    fields = MetabolomicExperiment._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"MetabolomicExperimentList.html",{'data':data, 'fields':new_fields})

def MethodsComparisonList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", MethodsComparison.objects.all())
    fields = MethodsComparison._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"MethodsComparisonList.html",{'data':data, 'fields':new_fields})

def SubjectsComparisonList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", SubjectsComparison.objects.all())
    fields = SubjectsComparison._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"SubjectsComparisonList.html",{'data':data, 'fields':new_fields})

def MicroarrayExperimentList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", MicroarrayExperiment.objects.all())
    fields = MicroarrayExperiment._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"MicroarrayExperimentList.html",{'data':data, 'fields':new_fields})

def MultifactorExperimentList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", MultifactorExperiment.objects.all())
    fields = MultifactorExperiment._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"MultifactorExperimentList.html",{'data':data, 'fields':new_fields})

def OnefactorExperimentList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", OnefactorExperiment.objects.all())
    fields = OnefactorExperiment._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"OnefactorExperimentList.html",{'data':data, 'fields':new_fields})

def TwofactorExperimentList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", TwofactorExperiment.objects.all())
    fields = TwofactorExperiment._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"TwofactorExperimentList.html",{'data':data, 'fields':new_fields})

def NaturalLanguageList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", NaturalLanguage.objects.all())
    fields = NaturalLanguage._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"NaturalLanguageList.html",{'data':data, 'fields':new_fields})

def NonRestrictedStatusList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", NonRestrictedStatus.objects.all())
    fields = NonRestrictedStatus._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"NonRestrictedStatusList.html",{'data':data, 'fields':new_fields})

def NormalizationStrategyList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", NormalizationStrategy.objects.all())
    fields = NormalizationStrategy._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"NormalizationStrategyList.html",{'data':data, 'fields':new_fields})

def ObjectMethodList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ObjectMethod.objects.all())
    fields = ObjectMethod._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ObjectMethodList.html",{'data':data, 'fields':new_fields})

def ObjectOfActionList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ObjectOfAction.objects.all())
    fields = ObjectOfAction._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ObjectOfActionList.html",{'data':data, 'fields':new_fields})

def ObservableVariableList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ObservableVariable.objects.all())
    fields = ObservableVariable._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ObservableVariableList.html",{'data':data, 'fields':new_fields})

def PaperList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Paper.objects.all())
    fields = Paper._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"PaperList.html",{'data':data, 'fields':new_fields})

def ParticlePhysicsExperimentList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ParticlePhysicsExperiment.objects.all())
    fields = ParticlePhysicsExperiment._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ParticlePhysicsExperimentList.html",{'data':data, 'fields':new_fields})

def PresentingSamplingList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", PresentingSampling.objects.all())
    fields = PresentingSampling._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"PresentingSamplingList.html",{'data':data, 'fields':new_fields})

def ProceedingsList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Proceedings.objects.all())
    fields = Proceedings._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ProceedingsList.html",{'data':data, 'fields':new_fields})

def ProgramList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Program.objects.all())
    fields = Program._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ProgramList.html",{'data':data, 'fields':new_fields})

def ProteomicExperimentList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", ProteomicExperiment.objects.all())
    fields = ProteomicExperiment._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ProteomicExperimentList.html",{'data':data, 'fields':new_fields})

def ProviderList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Provider.objects.all())
    fields = Provider._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"ProviderList.html",{'data':data, 'fields':new_fields})

def PublicAcademicStatusList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", PublicAcademicStatus.objects.all())
    fields = PublicAcademicStatus._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"PublicAcademicStatusList.html",{'data':data, 'fields':new_fields})

def PublicStatusList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", PublicStatus.objects.all())
    fields = PublicStatus._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"PublicStatusList.html",{'data':data, 'fields':new_fields})

def RandomErrorList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", RandomError.objects.all())
    fields = RandomError._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"RandomErrorList.html",{'data':data, 'fields':new_fields})

def RandomSamplingList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", RandomSampling.objects.all())
    fields = RandomSampling._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"RandomSamplingList.html",{'data':data, 'fields':new_fields})

def RecommendationSatusList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", RecommendationSatus.objects.all())
    fields = RecommendationSatus._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"RecommendationSatusList.html",{'data':data, 'fields':new_fields})

def RecordingActionList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", RecordingAction.objects.all())
    fields = RecordingAction._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"RecordingActionList.html",{'data':data, 'fields':new_fields})

def RegionList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Region.objects.all())
    fields = Region._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"RegionList.html",{'data':data, 'fields':new_fields})

def RepresenationalMediumList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", RepresenationalMedium.objects.all())
    fields = RepresenationalMedium._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"RepresenationalMediumList.html",{'data':data, 'fields':new_fields})

def SUMOSynonymList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", SUMOSynonym.objects.all())
    fields = SUMOSynonym._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"SUMOSynonymList.html",{'data':data, 'fields':new_fields})

def SampleFormingList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", SampleForming.objects.all())
    fields = SampleForming._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"SampleFormingList.html",{'data':data, 'fields':new_fields})

def SequentialDesignList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", SequentialDesign.objects.all())
    fields = SequentialDesign._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"SequentialDesignList.html",{'data':data, 'fields':new_fields})

def SoftwareList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Software.objects.all())
    fields = Software._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"SoftwareList.html",{'data':data, 'fields':new_fields})

def SoundList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Sound.objects.all())
    fields = Sound._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"SoundList.html",{'data':data, 'fields':new_fields})

def SplitSamplesList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", SplitSamples.objects.all())
    fields = SplitSamples._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"SplitSamplesList.html",{'data':data, 'fields':new_fields})

def SymmetricalMatchingList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", SymmetricalMatching.objects.all())
    fields = SymmetricalMatching._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"SymmetricalMatchingList.html",{'data':data, 'fields':new_fields})

def StatisticalErrorList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", StatisticalError.objects.all())
    fields = StatisticalError._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"StatisticalErrorList.html",{'data':data, 'fields':new_fields})

def StratifiedSamplingList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", StratifiedSampling.objects.all())
    fields = StratifiedSampling._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"StratifiedSamplingList.html",{'data':data, 'fields':new_fields})

def SubjectOfActionList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", SubjectOfAction.objects.all())
    fields = SubjectOfAction._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"SubjectOfActionList.html",{'data':data, 'fields':new_fields})

def SubjectOfExperimentList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", SubjectOfExperiment.objects.all())
    fields = SubjectOfExperiment._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"SubjectOfExperimentList.html",{'data':data, 'fields':new_fields})

def SubmitterList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Submitter.objects.all())
    fields = Submitter._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"SubmitterList.html",{'data':data, 'fields':new_fields})

def SummaryList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", Summary.objects.all())
    fields = Summary._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"SummaryList.html",{'data':data, 'fields':new_fields})

def SystematicErrorList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", SystematicError.objects.all())
    fields = SystematicError._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"SystematicErrorList.html",{'data':data, 'fields':new_fields})

def TechnicalReportList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", TechnicalReport.objects.all())
    fields = TechnicalReport._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"TechnicalReportList.html",{'data':data, 'fields':new_fields})

def TimeDurationList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", TimeDuration.objects.all())
    fields = TimeDuration._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"TimeDurationList.html",{'data':data, 'fields':new_fields})

def TimeIntervalList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", TimeInterval.objects.all())
    fields = TimeInterval._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"TimeIntervalList.html",{'data':data, 'fields':new_fields})

def URLList(request):
    excluded_keys = 'id', '_state'
    data = serializers.serialize( "python", URL.objects.all())
    fields = URL._meta.get_all_field_names()
    new_fields = []
    if len(data)>0:
        instance = data[0]
        for field in instance['fields'].keys(): 
            new_fields.append(field)
    else:
        for field in fields:
            if field != 'id':
                new_fields.append(field)
    return render(request,"URLList.html",{'data':data, 'fields':new_fields})
