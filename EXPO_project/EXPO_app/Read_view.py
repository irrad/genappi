from django.db import models
from django.template.loader import render_to_string
from .models import *
from .forms import *
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseNotFound
from django.template import loader
from django.core.urlresolvers import reverse
import datetime
from .forms import *
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.core.files.storage import FileSystemStorage
from django.utils.safestring import mark_safe
from django.core import serializers


def ThingRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Thing, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ThingRead.html",{'instance_items': instance_items})

def RepresentationFormRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(RepresentationForm, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"RepresentationFormRead.html",{'instance_items': instance_items})

def ArtifactRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Artifact, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ArtifactRead.html",{'instance_items': instance_items})

def SynonymousExternalConceptRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(SynonymousExternalConcept, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"SynonymousExternalConceptRead.html",{'instance_items': instance_items})

def NameRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Name, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"NameRead.html",{'instance_items': instance_items})

def ScientificExperimentRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ScientificExperiment, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ScientificExperimentRead.html",{'instance_items': instance_items})

def ExperimentalDesignRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ExperimentalDesign, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ExperimentalDesignRead.html",{'instance_items': instance_items})

def AdminInfoExperimentRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(AdminInfoExperiment, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"AdminInfoExperimentRead.html",{'instance_items': instance_items})

def IDExperimentRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(IDExperiment, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"IDExperimentRead.html",{'instance_items': instance_items})

def DomainOfExperimentRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(DomainOfExperiment, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"DomainOfExperimentRead.html",{'instance_items': instance_items})

def ExperimentalObservationRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ExperimentalObservation, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ExperimentalObservationRead.html",{'instance_items': instance_items})

def ResultsInterpretationRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ResultsInterpretation, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ResultsInterpretationRead.html",{'instance_items': instance_items})

def ExperimentalActionRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ExperimentalAction, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ExperimentalActionRead.html",{'instance_items': instance_items})

def ActionNameRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ActionName, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ActionNameRead.html",{'instance_items': instance_items})

def ActionComplexityRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ActionComplexity, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ActionComplexityRead.html",{'instance_items': instance_items})

def HumanRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Human, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"HumanRead.html",{'instance_items': instance_items})

def OrganizationRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Organization, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"OrganizationRead.html",{'instance_items': instance_items})

def AdminInfoAuthorRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(AdminInfoAuthor, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"AdminInfoAuthorRead.html",{'instance_items': instance_items})

def AdminInfoObjectOfExperimentRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(AdminInfoObjectOfExperiment, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"AdminInfoObjectOfExperimentRead.html",{'instance_items': instance_items})

def AdminInfoProviderRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(AdminInfoProvider, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"AdminInfoProviderRead.html",{'instance_items': instance_items})

def AdminInfoSubjectOfExperimentRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(AdminInfoSubjectOfExperiment, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"AdminInfoSubjectOfExperimentRead.html",{'instance_items': instance_items})

def AdminInfoSubmitterRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(AdminInfoSubmitter, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"AdminInfoSubmitterRead.html",{'instance_items': instance_items})

def AdminInfoUserRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(AdminInfoUser, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"AdminInfoUserRead.html",{'instance_items': instance_items})

def ExperimentalMethodRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ExperimentalMethod, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ExperimentalMethodRead.html",{'instance_items': instance_items})

def MethodApplicabilityConditionRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(MethodApplicabilityCondition, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"MethodApplicabilityConditionRead.html",{'instance_items': instance_items})

def StandardOperatingProcedureRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(StandardOperatingProcedure, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"StandardOperatingProcedureRead.html",{'instance_items': instance_items})

def AuthorRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Author, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"AuthorRead.html",{'instance_items': instance_items})

def ClassificationOfDomainsRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ClassificationOfDomains, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ClassificationOfDomainsRead.html",{'instance_items': instance_items})

def ClassificationOfExperimentsRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ClassificationOfExperiments, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ClassificationOfExperimentsRead.html",{'instance_items': instance_items})

def ExperimentalHypothesisRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ExperimentalHypothesis, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ExperimentalHypothesisRead.html",{'instance_items': instance_items})

def HypothesisComplexityRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(HypothesisComplexity, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"HypothesisComplexityRead.html",{'instance_items': instance_items})

def RepresentationRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Representation, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"RepresentationRead.html",{'instance_items': instance_items})

def PropositionRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Proposition, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"PropositionRead.html",{'instance_items': instance_items})

def ExperimentalEquipmentRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ExperimentalEquipment, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ExperimentalEquipmentRead.html",{'instance_items': instance_items})

def TechnicalDescriptionRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(TechnicalDescription, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"TechnicalDescriptionRead.html",{'instance_items': instance_items})

def ExperimentalResultsRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ExperimentalResults, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ExperimentalResultsRead.html",{'instance_items': instance_items})

def ObservationalErrorRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ObservationalError, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ObservationalErrorRead.html",{'instance_items': instance_items})

def ResultErrorRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ResultError, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ResultErrorRead.html",{'instance_items': instance_items})

def ResultsEvaluationRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ResultsEvaluation, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ResultsEvaluationRead.html",{'instance_items': instance_items})

def ScientificInvestigationRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ScientificInvestigation, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ScientificInvestigationRead.html",{'instance_items': instance_items})

def ExperimentalModelRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ExperimentalModel, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ExperimentalModelRead.html",{'instance_items': instance_items})

def ExperimentalFactorRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ExperimentalFactor, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ExperimentalFactorRead.html",{'instance_items': instance_items})

def ExperimentalTechnologyRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ExperimentalTechnology, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ExperimentalTechnologyRead.html",{'instance_items': instance_items})

def ExperimentalRequirementsRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ExperimentalRequirements, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ExperimentalRequirementsRead.html",{'instance_items': instance_items})

def HypothesisExplicitnessRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(HypothesisExplicitness, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"HypothesisExplicitnessRead.html",{'instance_items': instance_items})

def LinguisticExpressionRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(LinguisticExpression, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"LinguisticExpressionRead.html",{'instance_items': instance_items})

def FactRejectResearchHypothesisRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(FactRejectResearchHypothesis, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"FactRejectResearchHypothesisRead.html",{'instance_items': instance_items})

def FactSupportResearchHypothesisRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(FactSupportResearchHypothesis, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"FactSupportResearchHypothesisRead.html",{'instance_items': instance_items})

def NumberRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Number, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"NumberRead.html",{'instance_items': instance_items})

def GeneralityOfHypothesisRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(GeneralityOfHypothesis, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"GeneralityOfHypothesisRead.html",{'instance_items': instance_items})

def ActionGoalRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ActionGoal, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ActionGoalRead.html",{'instance_items': instance_items})

def ExperimentalGoalRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ExperimentalGoal, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ExperimentalGoalRead.html",{'instance_items': instance_items})

def MethodGoalRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(MethodGoal, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"MethodGoalRead.html",{'instance_items': instance_items})

def HypothesisFormationRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(HypothesisFormation, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"HypothesisFormationRead.html",{'instance_items': instance_items})

def HypothesisRepresentationRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(HypothesisRepresentation, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"HypothesisRepresentationRead.html",{'instance_items': instance_items})

def InformationGetheringRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(InformationGethering, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"InformationGetheringRead.html",{'instance_items': instance_items})

def SampleRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Sample, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"SampleRead.html",{'instance_items': instance_items})

def VariabilityRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Variability, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"VariabilityRead.html",{'instance_items': instance_items})

def TimePointRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(TimePoint, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"TimePointRead.html",{'instance_items': instance_items})

def UserRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(User, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"UserRead.html",{'instance_items': instance_items})

def LoginNameRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(LoginName, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"LoginNameRead.html",{'instance_items': instance_items})

def DomainModelRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(DomainModel, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"DomainModelRead.html",{'instance_items': instance_items})

def GroupExperimentalObjectRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(GroupExperimentalObject, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"GroupExperimentalObjectRead.html",{'instance_items': instance_items})

def ObjectOfExperimentRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ObjectOfExperiment, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ObjectOfExperimentRead.html",{'instance_items': instance_items})

def ClassificationByModelRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ClassificationByModel, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ClassificationByModelRead.html",{'instance_items': instance_items})

def StatisticsCharacteristicRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(StatisticsCharacteristic, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"StatisticsCharacteristicRead.html",{'instance_items': instance_items})

def ParentGroupRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ParentGroup, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ParentGroupRead.html",{'instance_items': instance_items})

def PasswordRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Password, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"PasswordRead.html",{'instance_items': instance_items})

def ProcedureExecuteExperimentRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ProcedureExecuteExperiment, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ProcedureExecuteExperimentRead.html",{'instance_items': instance_items})

def PlanOfExperimentalActionsRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(PlanOfExperimentalActions, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"PlanOfExperimentalActionsRead.html",{'instance_items': instance_items})

def PoblemAnalysisRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(PoblemAnalysis, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"PoblemAnalysisRead.html",{'instance_items': instance_items})

def SubjectQualificationRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(SubjectQualification, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"SubjectQualificationRead.html",{'instance_items': instance_items})

def BiblioReferenceRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BiblioReference, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BiblioReferenceRead.html",{'instance_items': instance_items})

def ModelRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Model, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ModelRead.html",{'instance_items': instance_items})

def ModelRepresentationRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ModelRepresentation, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ModelRepresentationRead.html",{'instance_items': instance_items})

def RepresentationExperimentalExecutionProcedureRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(RepresentationExperimentalExecutionProcedure, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"RepresentationExperimentalExecutionProcedureRead.html",{'instance_items': instance_items})

def RepresentationExperimentalGoalRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(RepresentationExperimentalGoal, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"RepresentationExperimentalGoalRead.html",{'instance_items': instance_items})

def SampleRepresentativenessRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(SampleRepresentativeness, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"SampleRepresentativenessRead.html",{'instance_items': instance_items})

def SampleSizeRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(SampleSize, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"SampleSizeRead.html",{'instance_items': instance_items})

def SamplingMethodRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(SamplingMethod, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"SamplingMethodRead.html",{'instance_items': instance_items})

def SystematicSamplingRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(SystematicSampling, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"SystematicSamplingRead.html",{'instance_items': instance_items})

def SamplingRuleRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(SamplingRule, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"SamplingRuleRead.html",{'instance_items': instance_items})

def ExperimentalStandardRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ExperimentalStandard, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ExperimentalStandardRead.html",{'instance_items': instance_items})

def ErrorRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Error, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ErrorRead.html",{'instance_items': instance_items})

def DispersionRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Dispersion, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"DispersionRead.html",{'instance_items': instance_items})

def LevelOfSignificanceRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(LevelOfSignificance, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"LevelOfSignificanceRead.html",{'instance_items': instance_items})

def StatusExperimentalDocumentRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(StatusExperimentalDocument, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"StatusExperimentalDocumentRead.html",{'instance_items': instance_items})

def ExperimentalDesignStrategyRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ExperimentalDesignStrategy, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ExperimentalDesignStrategyRead.html",{'instance_items': instance_items})

def ExperimentalSubgoalRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ExperimentalSubgoal, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ExperimentalSubgoalRead.html",{'instance_items': instance_items})

def SubjectMethodRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(SubjectMethod, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"SubjectMethodRead.html",{'instance_items': instance_items})

def BaconianExperimentRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(BaconianExperiment, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BaconianExperimentRead.html",{'instance_items': instance_items})

def TargetVariableRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(TargetVariable, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"TargetVariableRead.html",{'instance_items': instance_items})

def TitleRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Title, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"TitleRead.html",{'instance_items': instance_items})

def ValueEstimateRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ValueEstimate, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ValueEstimateRead.html",{'instance_items': instance_items})

def ValueOfVariableRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ValueOfVariable, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ValueOfVariableRead.html",{'instance_items': instance_items})

def RoleRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Role, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"RoleRead.html",{'instance_items': instance_items})

def CorpuscularObjectRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(CorpuscularObject, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"CorpuscularObjectRead.html",{'instance_items': instance_items})

def AttributeRoleRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(AttributeRole, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"AttributeRoleRead.html",{'instance_items': instance_items})

def AttributeRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Attribute, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"AttributeRead.html",{'instance_items': instance_items})

def ProcedureRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Procedure, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ProcedureRead.html",{'instance_items': instance_items})

def AdministrativeInformationRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(AdministrativeInformation, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"AdministrativeInformationRead.html",{'instance_items': instance_items})

def TitleExperimentRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(TitleExperiment, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"TitleExperimentRead.html",{'instance_items': instance_items})

def NameEquipmentRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(NameEquipment, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"NameEquipmentRead.html",{'instance_items': instance_items})

def PersonalNameRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(PersonalName, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"PersonalNameRead.html",{'instance_items': instance_items})

def FieldOfStudyRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(FieldOfStudy, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"FieldOfStudyRead.html",{'instance_items': instance_items})

def RelatedDomainRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(RelatedDomain, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"RelatedDomainRead.html",{'instance_items': instance_items})

def ProductRoleRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ProductRole, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ProductRoleRead.html",{'instance_items': instance_items})

def ScientificTaskRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ScientificTask, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ScientificTaskRead.html",{'instance_items': instance_items})

def ExecutionOfExperimentRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ExecutionOfExperiment, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ExecutionOfExperimentRead.html",{'instance_items': instance_items})

def CorporateNameRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(CorporateName, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"CorporateNameRead.html",{'instance_items': instance_items})

def AttributeOfActionRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(AttributeOfAction, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"AttributeOfActionRead.html",{'instance_items': instance_items})

def SentientAgentRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(SentientAgent, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"SentientAgentRead.html",{'instance_items': instance_items})

def RobotRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Robot, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"RobotRead.html",{'instance_items': instance_items})

def GroupRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Group, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"GroupRead.html",{'instance_items': instance_items})

def EntityRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Entity, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"EntityRead.html",{'instance_items': instance_items})

def ResearchMethodRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ResearchMethod, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ResearchMethodRead.html",{'instance_items': instance_items})

def RequirementsRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Requirements, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"RequirementsRead.html",{'instance_items': instance_items})

def AuthorSOPRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(AuthorSOP, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"AuthorSOPRead.html",{'instance_items': instance_items})

def SubjectRoleRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(SubjectRole, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"SubjectRoleRead.html",{'instance_items': instance_items})

def ClassificationRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Classification, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ClassificationRead.html",{'instance_items': instance_items})

def FactRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Fact, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"FactRead.html",{'instance_items': instance_items})

def ModelAssumptionRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ModelAssumption, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ModelAssumptionRead.html",{'instance_items': instance_items})

def VariableRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Variable, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"VariableRead.html",{'instance_items': instance_items})

def AttributeOfHypothesisRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(AttributeOfHypothesis, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"AttributeOfHypothesisRead.html",{'instance_items': instance_items})

def ContentBearingObjectRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ContentBearingObject, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ContentBearingObjectRead.html",{'instance_items': instance_items})

def AbstractRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Abstract, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"AbstractRead.html",{'instance_items': instance_items})

def QuantityRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Quantity, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"QuantityRead.html",{'instance_items': instance_items})

def RelationRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Relation, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"RelationRead.html",{'instance_items': instance_items})

def ProcessrelatedRoleRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ProcessrelatedRole, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ProcessrelatedRoleRead.html",{'instance_items': instance_items})

def ContentBearingPhysicalRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ContentBearingPhysical, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ContentBearingPhysicalRead.html",{'instance_items': instance_items})

def PhysicalQuantityRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(PhysicalQuantity, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"PhysicalQuantityRead.html",{'instance_items': instance_items})

def GoalRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Goal, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"GoalRead.html",{'instance_items': instance_items})

def RepresentationExperimentalObservationRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(RepresentationExperimentalObservation, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"RepresentationExperimentalObservationRead.html",{'instance_items': instance_items})

def RepresentationExperimentalResultsRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(RepresentationExperimentalResults, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"RepresentationExperimentalResultsRead.html",{'instance_items': instance_items})

def AttributeGroupRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(AttributeGroup, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"AttributeGroupRead.html",{'instance_items': instance_items})

def TimePositionRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(TimePosition, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"TimePositionRead.html",{'instance_items': instance_items})

def ActorRoleRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ActorRole, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ActorRoleRead.html",{'instance_items': instance_items})

def DocumentStageRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(DocumentStage, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"DocumentStageRead.html",{'instance_items': instance_items})

def PermissionStatusRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(PermissionStatus, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"PermissionStatusRead.html",{'instance_items': instance_items})

def PlanRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Plan, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"PlanRead.html",{'instance_items': instance_items})

def ReferenceRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Reference, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ReferenceRead.html",{'instance_items': instance_items})

def AttributeSampleRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(AttributeSample, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"AttributeSampleRead.html",{'instance_items': instance_items})

def ExperimentalRuleRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ExperimentalRule, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ExperimentalRuleRead.html",{'instance_items': instance_items})

def RobustnessRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Robustness, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"RobustnessRead.html",{'instance_items': instance_items})

def ValidityRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Validity, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ValidityRead.html",{'instance_items': instance_items})

def AttributeOfDocumentRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(AttributeOfDocument, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"AttributeOfDocumentRead.html",{'instance_items': instance_items})

def PhysicalExperimentRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(PhysicalExperiment, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"PhysicalExperimentRead.html",{'instance_items': instance_items})

def ComputationalDataRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ComputationalData, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ComputationalDataRead.html",{'instance_items': instance_items})

def PredicateRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Predicate, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"PredicateRead.html",{'instance_items': instance_items})

def SelfConnectedObjectRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(SelfConnectedObject, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"SelfConnectedObjectRead.html",{'instance_items': instance_items})

def ScientificActivityRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ScientificActivity, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ScientificActivityRead.html",{'instance_items': instance_items})

def FormingClassificationSystemRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(FormingClassificationSystem, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"FormingClassificationSystemRead.html",{'instance_items': instance_items})

def HypothesisFormingRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(HypothesisForming, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"HypothesisFormingRead.html",{'instance_items': instance_items})

def InterpretingResultRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(InterpretingResult, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"InterpretingResultRead.html",{'instance_items': instance_items})

def ProcessProblemAnalysisRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ProcessProblemAnalysis, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ProcessProblemAnalysisRead.html",{'instance_items': instance_items})

def ResultEvaluatingRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ResultEvaluating, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ResultEvaluatingRead.html",{'instance_items': instance_items})

def AttributeOfModelRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(AttributeOfModel, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"AttributeOfModelRead.html",{'instance_items': instance_items})

def AttributeOfVariableRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(AttributeOfVariable, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"AttributeOfVariableRead.html",{'instance_items': instance_items})

def AgentRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Agent, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"AgentRead.html",{'instance_items': instance_items})

def CollectionRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Collection, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"CollectionRead.html",{'instance_items': instance_items})

def EperimentalDesignTaskRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(EperimentalDesignTask, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"EperimentalDesignTaskRead.html",{'instance_items': instance_items})

def PhysicalRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Physical, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"PhysicalRead.html",{'instance_items': instance_items})

def ObjectRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Object, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ObjectRead.html",{'instance_items': instance_items})

def ProcessRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Process, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ProcessRead.html",{'instance_items': instance_items})

def TaskRoleRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(TaskRole, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"TaskRoleRead.html",{'instance_items': instance_items})

def TimeMeasureRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(TimeMeasure, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"TimeMeasureRead.html",{'instance_items': instance_items})

def ActionrelatedRoleRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ActionrelatedRole, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ActionrelatedRoleRead.html",{'instance_items': instance_items})

def AbductiveHypothesisRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(AbductiveHypothesis, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"AbductiveHypothesisRead.html",{'instance_items': instance_items})

def InductiveHypothesisRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(InductiveHypothesis, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"InductiveHypothesisRead.html",{'instance_items': instance_items})

def AdequacyRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Adequacy, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"AdequacyRead.html",{'instance_items': instance_items})

def AlternativeHypothesisRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(AlternativeHypothesis, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"AlternativeHypothesisRead.html",{'instance_items': instance_items})

def NullHypothesisRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(NullHypothesis, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"NullHypothesisRead.html",{'instance_items': instance_items})

def ResearchHypothesisRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ResearchHypothesis, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ResearchHypothesisRead.html",{'instance_items': instance_items})

def ArticleRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Article, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ArticleRead.html",{'instance_items': instance_items})

def TextRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Text, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"TextRead.html",{'instance_items': instance_items})

def ArtificialLanguageRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ArtificialLanguage, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ArtificialLanguageRead.html",{'instance_items': instance_items})

def LanguageRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Language, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"LanguageRead.html",{'instance_items': instance_items})

def HumanLanguageRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(HumanLanguage, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"HumanLanguageRead.html",{'instance_items': instance_items})

def SentenceRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Sentence, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"SentenceRead.html",{'instance_items': instance_items})

def AtomicActionRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(AtomicAction, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"AtomicActionRead.html",{'instance_items': instance_items})

def ComplexActionRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ComplexAction, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ComplexActionRead.html",{'instance_items': instance_items})

def AuthorProtocolRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(AuthorProtocol, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"AuthorProtocolRead.html",{'instance_items': instance_items})

def BookRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Book, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"BookRead.html",{'instance_items': instance_items})

def CalculableVariableRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(CalculableVariable, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"CalculableVariableRead.html",{'instance_items': instance_items})

def CapacityRequirementsRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(CapacityRequirements, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"CapacityRequirementsRead.html",{'instance_items': instance_items})

def EnvironmentalRequirementsRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(EnvironmentalRequirements, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"EnvironmentalRequirementsRead.html",{'instance_items': instance_items})

def FinancialRequirementsRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(FinancialRequirements, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"FinancialRequirementsRead.html",{'instance_items': instance_items})

def ClassificationByDomainRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ClassificationByDomain, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ClassificationByDomainRead.html",{'instance_items': instance_items})

def ClassifyingRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Classifying, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ClassifyingRead.html",{'instance_items': instance_items})

def DesigningExperimentRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(DesigningExperiment, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"DesigningExperimentRead.html",{'instance_items': instance_items})

def CoarseErrorRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(CoarseError, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"CoarseErrorRead.html",{'instance_items': instance_items})

def MeasurementErrorRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(MeasurementError, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"MeasurementErrorRead.html",{'instance_items': instance_items})

def ComparisonControl_TargetGroupsRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ComparisonControl_TargetGroups, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ComparisonControl_TargetGroupsRead.html",{'instance_items': instance_items})

def PairedComparisonRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(PairedComparison, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"PairedComparisonRead.html",{'instance_items': instance_items})

def PairedComparisonOfMatchingGroupsRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(PairedComparisonOfMatchingGroups, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"PairedComparisonOfMatchingGroupsRead.html",{'instance_items': instance_items})

def PairedComparisonOfSingleSampleGroupsRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(PairedComparisonOfSingleSampleGroups, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"PairedComparisonOfSingleSampleGroupsRead.html",{'instance_items': instance_items})

def QualityControlRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(QualityControl, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"QualityControlRead.html",{'instance_items': instance_items})

def ComplexHypothesisRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ComplexHypothesis, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ComplexHypothesisRead.html",{'instance_items': instance_items})

def SingleHypothesisRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(SingleHypothesis, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"SingleHypothesisRead.html",{'instance_items': instance_items})

def ComputationalExperimentRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ComputationalExperiment, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ComputationalExperimentRead.html",{'instance_items': instance_items})

def ComputeGoalRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ComputeGoal, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ComputeGoalRead.html",{'instance_items': instance_items})

def ConfirmGoalRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ConfirmGoal, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ConfirmGoalRead.html",{'instance_items': instance_items})

def ExplainGoalRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ExplainGoal, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ExplainGoalRead.html",{'instance_items': instance_items})

def InvestigateGoalRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(InvestigateGoal, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"InvestigateGoalRead.html",{'instance_items': instance_items})

def ComputerSimulationRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ComputerSimulation, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ComputerSimulationRead.html",{'instance_items': instance_items})

def ConstantQuantityRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ConstantQuantity, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ConstantQuantityRead.html",{'instance_items': instance_items})

def ControllabilityRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Controllability, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ControllabilityRead.html",{'instance_items': instance_items})

def IndependenceRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Independence, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"IndependenceRead.html",{'instance_items': instance_items})

def DBReferenceRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(DBReference, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"DBReferenceRead.html",{'instance_items': instance_items})

def DDC_Dewey_ClassificationRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(DDC_Dewey_Classification, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"DDC_Dewey_ClassificationRead.html",{'instance_items': instance_items})

def LibraryOfCongressClassificationRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(LibraryOfCongressClassification, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"LibraryOfCongressClassificationRead.html",{'instance_items': instance_items})

def NLMClassificationRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(NLMClassification, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"NLMClassificationRead.html",{'instance_items': instance_items})

def ResearchCouncilsUKClassificationRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ResearchCouncilsUKClassification, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ResearchCouncilsUKClassificationRead.html",{'instance_items': instance_items})

def DataRepresentationStandardRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(DataRepresentationStandard, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"DataRepresentationStandardRead.html",{'instance_items': instance_items})

def DegreeOfModelRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(DegreeOfModel, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"DegreeOfModelRead.html",{'instance_items': instance_items})

def DynamismOfModelRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(DynamismOfModel, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"DynamismOfModelRead.html",{'instance_items': instance_items})

def DependentVariableRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(DependentVariable, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"DependentVariableRead.html",{'instance_items': instance_items})

def DeviceRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Device, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"DeviceRead.html",{'instance_items': instance_items})

def DomainActionRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(DomainAction, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"DomainActionRead.html",{'instance_items': instance_items})

def DoseResponseRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(DoseResponse, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"DoseResponseRead.html",{'instance_items': instance_items})

def GeneKnockinRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(GeneKnockin, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"GeneKnockinRead.html",{'instance_items': instance_items})

def GeneKnockoutRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(GeneKnockout, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"GeneKnockoutRead.html",{'instance_items': instance_items})

def Normal_DiseaseRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Normal_Disease, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"Normal_DiseaseRead.html",{'instance_items': instance_items})

def TimeCourseRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(TimeCourse, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"TimeCourseRead.html",{'instance_items': instance_items})

def Treated_UntreatedRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Treated_Untreated, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"Treated_UntreatedRead.html",{'instance_items': instance_items})

def DraftStatusRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(DraftStatus, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"DraftStatusRead.html",{'instance_items': instance_items})

def DuhemEffectRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(DuhemEffect, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"DuhemEffectRead.html",{'instance_items': instance_items})

def ExperimentalDesignEffectRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ExperimentalDesignEffect, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ExperimentalDesignEffectRead.html",{'instance_items': instance_items})

def InstrumentationEffectRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(InstrumentationEffect, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"InstrumentationEffectRead.html",{'instance_items': instance_items})

def ObjectEffectRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ObjectEffect, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ObjectEffectRead.html",{'instance_items': instance_items})

def SubjectEffectRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(SubjectEffect, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"SubjectEffectRead.html",{'instance_items': instance_items})

def TimeEffectRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(TimeEffect, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"TimeEffectRead.html",{'instance_items': instance_items})

def DynamicModelRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(DynamicModel, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"DynamicModelRead.html",{'instance_items': instance_items})

def StaticModelRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(StaticModel, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"StaticModelRead.html",{'instance_items': instance_items})

def ErrorOfConclusionRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ErrorOfConclusion, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ErrorOfConclusionRead.html",{'instance_items': instance_items})

def ExperimentalActionsPlanningRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ExperimentalActionsPlanning, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ExperimentalActionsPlanningRead.html",{'instance_items': instance_items})

def ExperimentalEquipmentSelectingRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ExperimentalEquipmentSelecting, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ExperimentalEquipmentSelectingRead.html",{'instance_items': instance_items})

def ExperimentalModelDesignRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ExperimentalModelDesign, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ExperimentalModelDesignRead.html",{'instance_items': instance_items})

def ExperimentalObjectSelectingRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ExperimentalObjectSelecting, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ExperimentalObjectSelectingRead.html",{'instance_items': instance_items})

def ExperimentalConclusionRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ExperimentalConclusion, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ExperimentalConclusionRead.html",{'instance_items': instance_items})

def ExperimentalProtocolRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ExperimentalProtocol, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ExperimentalProtocolRead.html",{'instance_items': instance_items})

def ExperimenteeBiasRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ExperimenteeBias, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ExperimenteeBiasRead.html",{'instance_items': instance_items})

def HawthorneEffectRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(HawthorneEffect, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"HawthorneEffectRead.html",{'instance_items': instance_items})

def MortalityEffectRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(MortalityEffect, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"MortalityEffectRead.html",{'instance_items': instance_items})

def ExperimenterBiasRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ExperimenterBias, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ExperimenterBiasRead.html",{'instance_items': instance_items})

def TestingEffectRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(TestingEffect, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"TestingEffectRead.html",{'instance_items': instance_items})

def ExplicitHypothesisRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ExplicitHypothesis, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ExplicitHypothesisRead.html",{'instance_items': instance_items})

def ImplicitHypothesisRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ImplicitHypothesis, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ImplicitHypothesisRead.html",{'instance_items': instance_items})

def FactorLevelRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(FactorLevel, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"FactorLevelRead.html",{'instance_items': instance_items})

def FactorialDesignRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(FactorialDesign, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"FactorialDesignRead.html",{'instance_items': instance_items})

def FalseNegativeRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(FalseNegative, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"FalseNegativeRead.html",{'instance_items': instance_items})

def HypothesisAcceptanceMistakeRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(HypothesisAcceptanceMistake, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"HypothesisAcceptanceMistakeRead.html",{'instance_items': instance_items})

def FalsePpositiveRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(FalsePpositive, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"FalsePpositiveRead.html",{'instance_items': instance_items})

def FaultyComparisonRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(FaultyComparison, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"FaultyComparisonRead.html",{'instance_items': instance_items})

def FormulaRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Formula, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"FormulaRead.html",{'instance_items': instance_items})

def GalileanExperimentRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(GalileanExperiment, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"GalileanExperimentRead.html",{'instance_items': instance_items})

def HandToolRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(HandTool, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"HandToolRead.html",{'instance_items': instance_items})

def ToolRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Tool, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ToolRead.html",{'instance_items': instance_items})

def HardwareRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Hardware, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"HardwareRead.html",{'instance_items': instance_items})

def HistoryEffectRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(HistoryEffect, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"HistoryEffectRead.html",{'instance_items': instance_items})

def MaturationEffectRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(MaturationEffect, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"MaturationEffectRead.html",{'instance_items': instance_items})

def HypothesisdrivenExperimentRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(HypothesisdrivenExperiment, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"HypothesisdrivenExperimentRead.html",{'instance_items': instance_items})

def HypothesisformingExperimentRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(HypothesisformingExperiment, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"HypothesisformingExperimentRead.html",{'instance_items': instance_items})

def ImageRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Image, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ImageRead.html",{'instance_items': instance_items})

def ImproperSamplingRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ImproperSampling, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ImproperSamplingRead.html",{'instance_items': instance_items})

def IncompleteDataErrorRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(IncompleteDataError, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"IncompleteDataErrorRead.html",{'instance_items': instance_items})

def IndependentVariableRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(IndependentVariable, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"IndependentVariableRead.html",{'instance_items': instance_items})

def InferableVariableRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(InferableVariable, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"InferableVariableRead.html",{'instance_items': instance_items})

def InternalStatusRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(InternalStatus, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"InternalStatusRead.html",{'instance_items': instance_items})

def RestrictedStatusRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(RestrictedStatus, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"RestrictedStatusRead.html",{'instance_items': instance_items})

def JournalRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Journal, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"JournalRead.html",{'instance_items': instance_items})

def LinearModelRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(LinearModel, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"LinearModelRead.html",{'instance_items': instance_items})

def NonlinearModelRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(NonlinearModel, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"NonlinearModelRead.html",{'instance_items': instance_items})

def LogicalModelRepresentationRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(LogicalModelRepresentation, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"LogicalModelRepresentationRead.html",{'instance_items': instance_items})

def MachineRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Machine, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"MachineRead.html",{'instance_items': instance_items})

def MachineToolRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(MachineTool, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"MachineToolRead.html",{'instance_items': instance_items})

def MagazineRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Magazine, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"MagazineRead.html",{'instance_items': instance_items})

def MaterialsRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Materials, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"MaterialsRead.html",{'instance_items': instance_items})

def MathematicalModelRepresentationRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(MathematicalModelRepresentation, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"MathematicalModelRepresentationRead.html",{'instance_items': instance_items})

def MetabolomicExperimentRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(MetabolomicExperiment, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"MetabolomicExperimentRead.html",{'instance_items': instance_items})

def MethodsComparisonRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(MethodsComparison, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"MethodsComparisonRead.html",{'instance_items': instance_items})

def SubjectsComparisonRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(SubjectsComparison, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"SubjectsComparisonRead.html",{'instance_items': instance_items})

def MicroarrayExperimentRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(MicroarrayExperiment, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"MicroarrayExperimentRead.html",{'instance_items': instance_items})

def MultifactorExperimentRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(MultifactorExperiment, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"MultifactorExperimentRead.html",{'instance_items': instance_items})

def OnefactorExperimentRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(OnefactorExperiment, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"OnefactorExperimentRead.html",{'instance_items': instance_items})

def TwofactorExperimentRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(TwofactorExperiment, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"TwofactorExperimentRead.html",{'instance_items': instance_items})

def NaturalLanguageRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(NaturalLanguage, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"NaturalLanguageRead.html",{'instance_items': instance_items})

def NonRestrictedStatusRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(NonRestrictedStatus, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"NonRestrictedStatusRead.html",{'instance_items': instance_items})

def NormalizationStrategyRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(NormalizationStrategy, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"NormalizationStrategyRead.html",{'instance_items': instance_items})

def ObjectMethodRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ObjectMethod, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ObjectMethodRead.html",{'instance_items': instance_items})

def ObjectOfActionRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ObjectOfAction, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ObjectOfActionRead.html",{'instance_items': instance_items})

def ObservableVariableRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ObservableVariable, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ObservableVariableRead.html",{'instance_items': instance_items})

def PaperRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Paper, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"PaperRead.html",{'instance_items': instance_items})

def ParticlePhysicsExperimentRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ParticlePhysicsExperiment, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ParticlePhysicsExperimentRead.html",{'instance_items': instance_items})

def PresentingSamplingRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(PresentingSampling, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"PresentingSamplingRead.html",{'instance_items': instance_items})

def ProceedingsRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Proceedings, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ProceedingsRead.html",{'instance_items': instance_items})

def ProgramRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Program, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ProgramRead.html",{'instance_items': instance_items})

def ProteomicExperimentRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(ProteomicExperiment, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ProteomicExperimentRead.html",{'instance_items': instance_items})

def ProviderRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Provider, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"ProviderRead.html",{'instance_items': instance_items})

def PublicAcademicStatusRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(PublicAcademicStatus, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"PublicAcademicStatusRead.html",{'instance_items': instance_items})

def PublicStatusRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(PublicStatus, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"PublicStatusRead.html",{'instance_items': instance_items})

def RandomErrorRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(RandomError, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"RandomErrorRead.html",{'instance_items': instance_items})

def RandomSamplingRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(RandomSampling, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"RandomSamplingRead.html",{'instance_items': instance_items})

def RecommendationSatusRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(RecommendationSatus, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"RecommendationSatusRead.html",{'instance_items': instance_items})

def RecordingActionRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(RecordingAction, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"RecordingActionRead.html",{'instance_items': instance_items})

def RegionRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Region, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"RegionRead.html",{'instance_items': instance_items})

def RepresenationalMediumRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(RepresenationalMedium, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"RepresenationalMediumRead.html",{'instance_items': instance_items})

def SUMOSynonymRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(SUMOSynonym, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"SUMOSynonymRead.html",{'instance_items': instance_items})

def SampleFormingRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(SampleForming, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"SampleFormingRead.html",{'instance_items': instance_items})

def SequentialDesignRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(SequentialDesign, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"SequentialDesignRead.html",{'instance_items': instance_items})

def SoftwareRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Software, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"SoftwareRead.html",{'instance_items': instance_items})

def SoundRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Sound, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"SoundRead.html",{'instance_items': instance_items})

def SplitSamplesRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(SplitSamples, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"SplitSamplesRead.html",{'instance_items': instance_items})

def SymmetricalMatchingRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(SymmetricalMatching, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"SymmetricalMatchingRead.html",{'instance_items': instance_items})

def StatisticalErrorRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(StatisticalError, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"StatisticalErrorRead.html",{'instance_items': instance_items})

def StratifiedSamplingRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(StratifiedSampling, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"StratifiedSamplingRead.html",{'instance_items': instance_items})

def SubjectOfActionRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(SubjectOfAction, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"SubjectOfActionRead.html",{'instance_items': instance_items})

def SubjectOfExperimentRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(SubjectOfExperiment, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"SubjectOfExperimentRead.html",{'instance_items': instance_items})

def SubmitterRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Submitter, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"SubmitterRead.html",{'instance_items': instance_items})

def SummaryRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(Summary, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"SummaryRead.html",{'instance_items': instance_items})

def SystematicErrorRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(SystematicError, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"SystematicErrorRead.html",{'instance_items': instance_items})

def TechnicalReportRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(TechnicalReport, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"TechnicalReportRead.html",{'instance_items': instance_items})

def TimeDurationRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(TimeDuration, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"TimeDurationRead.html",{'instance_items': instance_items})

def TimeIntervalRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(TimeInterval, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"TimeIntervalRead.html",{'instance_items': instance_items})

def URLRead(request, pk):
    excluded_keys = 'id', '_state'
    instance = get_object_or_404(URL, pk=pk)
    instance_key_values = instance.__dict__
    instance_items = []
    for key, value in instance_key_values.items():
        if key in excluded_keys:
            pass
        else:
            instance_items.append({'key': key, 'value': value})
    return render(request,"URLRead.html",{'instance_items': instance_items})
