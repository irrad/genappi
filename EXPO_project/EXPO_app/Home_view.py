from django.db import models
from django.template.loader import render_to_string
from .models import *
from .forms import *
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseNotFound
from django.template import loader
from django.core.urlresolvers import reverse
import datetime
from .forms import *
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.core.files.storage import FileSystemStorage
from django.utils.safestring import mark_safe

def vowl(request):
    context = {}
    return render(request, "do_visualisation.html", context)

def HomeView(request):
    classes = []
    
    classes.append(str(Thing))
    
    classes.append(str(RepresentationForm))
    
    classes.append(str(Artifact))
    
    classes.append(str(SynonymousExternalConcept))
    
    classes.append(str(Name))
    
    classes.append(str(ScientificExperiment))
    
    classes.append(str(ExperimentalDesign))
    
    classes.append(str(AdminInfoExperiment))
    
    classes.append(str(IDExperiment))
    
    classes.append(str(DomainOfExperiment))
    
    classes.append(str(ExperimentalObservation))
    
    classes.append(str(ResultsInterpretation))
    
    classes.append(str(ExperimentalAction))
    
    classes.append(str(ActionName))
    
    classes.append(str(ActionComplexity))
    
    classes.append(str(Human))
    
    classes.append(str(Organization))
    
    classes.append(str(AdminInfoAuthor))
    
    classes.append(str(AdminInfoObjectOfExperiment))
    
    classes.append(str(AdminInfoProvider))
    
    classes.append(str(AdminInfoSubjectOfExperiment))
    
    classes.append(str(AdminInfoSubmitter))
    
    classes.append(str(AdminInfoUser))
    
    classes.append(str(ExperimentalMethod))
    
    classes.append(str(MethodApplicabilityCondition))
    
    classes.append(str(StandardOperatingProcedure))
    
    classes.append(str(Author))
    
    classes.append(str(ClassificationOfDomains))
    
    classes.append(str(ClassificationOfExperiments))
    
    classes.append(str(ExperimentalHypothesis))
    
    classes.append(str(HypothesisComplexity))
    
    classes.append(str(Representation))
    
    classes.append(str(Proposition))
    
    classes.append(str(ExperimentalEquipment))
    
    classes.append(str(TechnicalDescription))
    
    classes.append(str(ExperimentalResults))
    
    classes.append(str(ObservationalError))
    
    classes.append(str(ResultError))
    
    classes.append(str(ResultsEvaluation))
    
    classes.append(str(ScientificInvestigation))
    
    classes.append(str(ExperimentalModel))
    
    classes.append(str(ExperimentalFactor))
    
    classes.append(str(ExperimentalTechnology))
    
    classes.append(str(ExperimentalRequirements))
    
    classes.append(str(HypothesisExplicitness))
    
    classes.append(str(LinguisticExpression))
    
    classes.append(str(FactRejectResearchHypothesis))
    
    classes.append(str(FactSupportResearchHypothesis))
    
    classes.append(str(Number))
    
    classes.append(str(GeneralityOfHypothesis))
    
    classes.append(str(ActionGoal))
    
    classes.append(str(ExperimentalGoal))
    
    classes.append(str(MethodGoal))
    
    classes.append(str(HypothesisFormation))
    
    classes.append(str(HypothesisRepresentation))
    
    classes.append(str(InformationGethering))
    
    classes.append(str(Sample))
    
    classes.append(str(Variability))
    
    classes.append(str(TimePoint))
    
    classes.append(str(User))
    
    classes.append(str(LoginName))
    
    classes.append(str(DomainModel))
    
    classes.append(str(GroupExperimentalObject))
    
    classes.append(str(ObjectOfExperiment))
    
    classes.append(str(ClassificationByModel))
    
    classes.append(str(StatisticsCharacteristic))
    
    classes.append(str(ParentGroup))
    
    classes.append(str(Password))
    
    classes.append(str(ProcedureExecuteExperiment))
    
    classes.append(str(PlanOfExperimentalActions))
    
    classes.append(str(PoblemAnalysis))
    
    classes.append(str(SubjectQualification))
    
    classes.append(str(BiblioReference))
    
    classes.append(str(Model))
    
    classes.append(str(ModelRepresentation))
    
    classes.append(str(RepresentationExperimentalExecutionProcedure))
    
    classes.append(str(RepresentationExperimentalGoal))
    
    classes.append(str(SampleRepresentativeness))
    
    classes.append(str(SampleSize))
    
    classes.append(str(SamplingMethod))
    
    classes.append(str(SystematicSampling))
    
    classes.append(str(SamplingRule))
    
    classes.append(str(ExperimentalStandard))
    
    classes.append(str(Error))
    
    classes.append(str(Dispersion))
    
    classes.append(str(LevelOfSignificance))
    
    classes.append(str(StatusExperimentalDocument))
    
    classes.append(str(ExperimentalDesignStrategy))
    
    classes.append(str(ExperimentalSubgoal))
    
    classes.append(str(SubjectMethod))
    
    classes.append(str(BaconianExperiment))
    
    classes.append(str(TargetVariable))
    
    classes.append(str(Title))
    
    classes.append(str(ValueEstimate))
    
    classes.append(str(ValueOfVariable))
    
    classes.append(str(Role))
    
    classes.append(str(CorpuscularObject))
    
    classes.append(str(AttributeRole))
    
    classes.append(str(Attribute))
    
    classes.append(str(Procedure))
    
    classes.append(str(AdministrativeInformation))
    
    classes.append(str(TitleExperiment))
    
    classes.append(str(NameEquipment))
    
    classes.append(str(PersonalName))
    
    classes.append(str(FieldOfStudy))
    
    classes.append(str(RelatedDomain))
    
    classes.append(str(ProductRole))
    
    classes.append(str(ScientificTask))
    
    classes.append(str(ExecutionOfExperiment))
    
    classes.append(str(CorporateName))
    
    classes.append(str(AttributeOfAction))
    
    classes.append(str(SentientAgent))
    
    classes.append(str(Robot))
    
    classes.append(str(Group))
    
    classes.append(str(Entity))
    
    classes.append(str(ResearchMethod))
    
    classes.append(str(Requirements))
    
    classes.append(str(AuthorSOP))
    
    classes.append(str(SubjectRole))
    
    classes.append(str(Classification))
    
    classes.append(str(Fact))
    
    classes.append(str(ModelAssumption))
    
    classes.append(str(Variable))
    
    classes.append(str(AttributeOfHypothesis))
    
    classes.append(str(ContentBearingObject))
    
    classes.append(str(Abstract))
    
    classes.append(str(Quantity))
    
    classes.append(str(Relation))
    
    classes.append(str(ProcessrelatedRole))
    
    classes.append(str(ContentBearingPhysical))
    
    classes.append(str(PhysicalQuantity))
    
    classes.append(str(Goal))
    
    classes.append(str(RepresentationExperimentalObservation))
    
    classes.append(str(RepresentationExperimentalResults))
    
    classes.append(str(AttributeGroup))
    
    classes.append(str(TimePosition))
    
    classes.append(str(ActorRole))
    
    classes.append(str(DocumentStage))
    
    classes.append(str(PermissionStatus))
    
    classes.append(str(Plan))
    
    classes.append(str(Reference))
    
    classes.append(str(AttributeSample))
    
    classes.append(str(ExperimentalRule))
    
    classes.append(str(Robustness))
    
    classes.append(str(Validity))
    
    classes.append(str(AttributeOfDocument))
    
    classes.append(str(PhysicalExperiment))
    
    classes.append(str(ComputationalData))
    
    classes.append(str(Predicate))
    
    classes.append(str(SelfConnectedObject))
    
    classes.append(str(ScientificActivity))
    
    classes.append(str(FormingClassificationSystem))
    
    classes.append(str(HypothesisForming))
    
    classes.append(str(InterpretingResult))
    
    classes.append(str(ProcessProblemAnalysis))
    
    classes.append(str(ResultEvaluating))
    
    classes.append(str(AttributeOfModel))
    
    classes.append(str(AttributeOfVariable))
    
    classes.append(str(Agent))
    
    classes.append(str(Collection))
    
    classes.append(str(EperimentalDesignTask))
    
    classes.append(str(Physical))
    
    classes.append(str(Object))
    
    classes.append(str(Process))
    
    classes.append(str(TaskRole))
    
    classes.append(str(TimeMeasure))
    
    classes.append(str(ActionrelatedRole))
    
    classes.append(str(AbductiveHypothesis))
    
    classes.append(str(InductiveHypothesis))
    
    classes.append(str(Adequacy))
    
    classes.append(str(AlternativeHypothesis))
    
    classes.append(str(NullHypothesis))
    
    classes.append(str(ResearchHypothesis))
    
    classes.append(str(Article))
    
    classes.append(str(Text))
    
    classes.append(str(ArtificialLanguage))
    
    classes.append(str(Language))
    
    classes.append(str(HumanLanguage))
    
    classes.append(str(Sentence))
    
    classes.append(str(AtomicAction))
    
    classes.append(str(ComplexAction))
    
    classes.append(str(AuthorProtocol))
    
    classes.append(str(Book))
    
    classes.append(str(CalculableVariable))
    
    classes.append(str(CapacityRequirements))
    
    classes.append(str(EnvironmentalRequirements))
    
    classes.append(str(FinancialRequirements))
    
    classes.append(str(ClassificationByDomain))
    
    classes.append(str(Classifying))
    
    classes.append(str(DesigningExperiment))
    
    classes.append(str(CoarseError))
    
    classes.append(str(MeasurementError))
    
    classes.append(str(ComparisonControl_TargetGroups))
    
    classes.append(str(PairedComparison))
    
    classes.append(str(PairedComparisonOfMatchingGroups))
    
    classes.append(str(PairedComparisonOfSingleSampleGroups))
    
    classes.append(str(QualityControl))
    
    classes.append(str(ComplexHypothesis))
    
    classes.append(str(SingleHypothesis))
    
    classes.append(str(ComputationalExperiment))
    
    classes.append(str(ComputeGoal))
    
    classes.append(str(ConfirmGoal))
    
    classes.append(str(ExplainGoal))
    
    classes.append(str(InvestigateGoal))
    
    classes.append(str(ComputerSimulation))
    
    classes.append(str(ConstantQuantity))
    
    classes.append(str(Controllability))
    
    classes.append(str(Independence))
    
    classes.append(str(DBReference))
    
    classes.append(str(DDC_Dewey_Classification))
    
    classes.append(str(LibraryOfCongressClassification))
    
    classes.append(str(NLMClassification))
    
    classes.append(str(ResearchCouncilsUKClassification))
    
    classes.append(str(DataRepresentationStandard))
    
    classes.append(str(DegreeOfModel))
    
    classes.append(str(DynamismOfModel))
    
    classes.append(str(DependentVariable))
    
    classes.append(str(Device))
    
    classes.append(str(DomainAction))
    
    classes.append(str(DoseResponse))
    
    classes.append(str(GeneKnockin))
    
    classes.append(str(GeneKnockout))
    
    classes.append(str(Normal_Disease))
    
    classes.append(str(TimeCourse))
    
    classes.append(str(Treated_Untreated))
    
    classes.append(str(DraftStatus))
    
    classes.append(str(DuhemEffect))
    
    classes.append(str(ExperimentalDesignEffect))
    
    classes.append(str(InstrumentationEffect))
    
    classes.append(str(ObjectEffect))
    
    classes.append(str(SubjectEffect))
    
    classes.append(str(TimeEffect))
    
    classes.append(str(DynamicModel))
    
    classes.append(str(StaticModel))
    
    classes.append(str(ErrorOfConclusion))
    
    classes.append(str(ExperimentalActionsPlanning))
    
    classes.append(str(ExperimentalEquipmentSelecting))
    
    classes.append(str(ExperimentalModelDesign))
    
    classes.append(str(ExperimentalObjectSelecting))
    
    classes.append(str(ExperimentalConclusion))
    
    classes.append(str(ExperimentalProtocol))
    
    classes.append(str(ExperimenteeBias))
    
    classes.append(str(HawthorneEffect))
    
    classes.append(str(MortalityEffect))
    
    classes.append(str(ExperimenterBias))
    
    classes.append(str(TestingEffect))
    
    classes.append(str(ExplicitHypothesis))
    
    classes.append(str(ImplicitHypothesis))
    
    classes.append(str(FactorLevel))
    
    classes.append(str(FactorialDesign))
    
    classes.append(str(FalseNegative))
    
    classes.append(str(HypothesisAcceptanceMistake))
    
    classes.append(str(FalsePpositive))
    
    classes.append(str(FaultyComparison))
    
    classes.append(str(Formula))
    
    classes.append(str(GalileanExperiment))
    
    classes.append(str(HandTool))
    
    classes.append(str(Tool))
    
    classes.append(str(Hardware))
    
    classes.append(str(HistoryEffect))
    
    classes.append(str(MaturationEffect))
    
    classes.append(str(HypothesisdrivenExperiment))
    
    classes.append(str(HypothesisformingExperiment))
    
    classes.append(str(Image))
    
    classes.append(str(ImproperSampling))
    
    classes.append(str(IncompleteDataError))
    
    classes.append(str(IndependentVariable))
    
    classes.append(str(InferableVariable))
    
    classes.append(str(InternalStatus))
    
    classes.append(str(RestrictedStatus))
    
    classes.append(str(Journal))
    
    classes.append(str(LinearModel))
    
    classes.append(str(NonlinearModel))
    
    classes.append(str(LogicalModelRepresentation))
    
    classes.append(str(Machine))
    
    classes.append(str(MachineTool))
    
    classes.append(str(Magazine))
    
    classes.append(str(Materials))
    
    classes.append(str(MathematicalModelRepresentation))
    
    classes.append(str(MetabolomicExperiment))
    
    classes.append(str(MethodsComparison))
    
    classes.append(str(SubjectsComparison))
    
    classes.append(str(MicroarrayExperiment))
    
    classes.append(str(MultifactorExperiment))
    
    classes.append(str(OnefactorExperiment))
    
    classes.append(str(TwofactorExperiment))
    
    classes.append(str(NaturalLanguage))
    
    classes.append(str(NonRestrictedStatus))
    
    classes.append(str(NormalizationStrategy))
    
    classes.append(str(ObjectMethod))
    
    classes.append(str(ObjectOfAction))
    
    classes.append(str(ObservableVariable))
    
    classes.append(str(Paper))
    
    classes.append(str(ParticlePhysicsExperiment))
    
    classes.append(str(PresentingSampling))
    
    classes.append(str(Proceedings))
    
    classes.append(str(Program))
    
    classes.append(str(ProteomicExperiment))
    
    classes.append(str(Provider))
    
    classes.append(str(PublicAcademicStatus))
    
    classes.append(str(PublicStatus))
    
    classes.append(str(RandomError))
    
    classes.append(str(RandomSampling))
    
    classes.append(str(RecommendationSatus))
    
    classes.append(str(RecordingAction))
    
    classes.append(str(Region))
    
    classes.append(str(RepresenationalMedium))
    
    classes.append(str(SUMOSynonym))
    
    classes.append(str(SampleForming))
    
    classes.append(str(SequentialDesign))
    
    classes.append(str(Software))
    
    classes.append(str(Sound))
    
    classes.append(str(SplitSamples))
    
    classes.append(str(SymmetricalMatching))
    
    classes.append(str(StatisticalError))
    
    classes.append(str(StratifiedSampling))
    
    classes.append(str(SubjectOfAction))
    
    classes.append(str(SubjectOfExperiment))
    
    classes.append(str(Submitter))
    
    classes.append(str(Summary))
    
    classes.append(str(SystematicError))
    
    classes.append(str(TechnicalReport))
    
    classes.append(str(TimeDuration))
    
    classes.append(str(TimeInterval))
    
    classes.append(str(URL))
    
    context = {'classes':classes}
    return render(request, "home.html",context)