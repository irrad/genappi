from django.conf.urls import url, include
from . import Read_view, Create_view, Update_view, Delete_view, List_view, Home_view, preferences, registration

urlpatterns = [
    url(r'^home/$', Home_view.HomeView, name='HomeView'),

    url(r'^preferences/$', preferences.define_preferences, name='define_preferences'),

    url(r'^signup/$', registration.signup, name='signup'),    

    url(r'^Thing/create/$', Create_view.ThingCreate, name='ThingCreate'),

    url(r'^RepresentationForm/create/$', Create_view.RepresentationFormCreate, name='RepresentationFormCreate'),

    url(r'^Artifact/create/$', Create_view.ArtifactCreate, name='ArtifactCreate'),

    url(r'^SynonymousExternalConcept/create/$', Create_view.SynonymousExternalConceptCreate, name='SynonymousExternalConceptCreate'),

    url(r'^Name/create/$', Create_view.NameCreate, name='NameCreate'),

    url(r'^ScientificExperiment/create/$', Create_view.ScientificExperimentCreate, name='ScientificExperimentCreate'),

    url(r'^ExperimentalDesign/create/$', Create_view.ExperimentalDesignCreate, name='ExperimentalDesignCreate'),

    url(r'^AdminInfoExperiment/create/$', Create_view.AdminInfoExperimentCreate, name='AdminInfoExperimentCreate'),

    url(r'^IDExperiment/create/$', Create_view.IDExperimentCreate, name='IDExperimentCreate'),

    url(r'^DomainOfExperiment/create/$', Create_view.DomainOfExperimentCreate, name='DomainOfExperimentCreate'),

    url(r'^ExperimentalObservation/create/$', Create_view.ExperimentalObservationCreate, name='ExperimentalObservationCreate'),

    url(r'^ResultsInterpretation/create/$', Create_view.ResultsInterpretationCreate, name='ResultsInterpretationCreate'),

    url(r'^ExperimentalAction/create/$', Create_view.ExperimentalActionCreate, name='ExperimentalActionCreate'),

    url(r'^ActionName/create/$', Create_view.ActionNameCreate, name='ActionNameCreate'),

    url(r'^ActionComplexity/create/$', Create_view.ActionComplexityCreate, name='ActionComplexityCreate'),

    url(r'^Human/create/$', Create_view.HumanCreate, name='HumanCreate'),

    url(r'^Organization/create/$', Create_view.OrganizationCreate, name='OrganizationCreate'),

    url(r'^AdminInfoAuthor/create/$', Create_view.AdminInfoAuthorCreate, name='AdminInfoAuthorCreate'),

    url(r'^AdminInfoObjectOfExperiment/create/$', Create_view.AdminInfoObjectOfExperimentCreate, name='AdminInfoObjectOfExperimentCreate'),

    url(r'^AdminInfoProvider/create/$', Create_view.AdminInfoProviderCreate, name='AdminInfoProviderCreate'),

    url(r'^AdminInfoSubjectOfExperiment/create/$', Create_view.AdminInfoSubjectOfExperimentCreate, name='AdminInfoSubjectOfExperimentCreate'),

    url(r'^AdminInfoSubmitter/create/$', Create_view.AdminInfoSubmitterCreate, name='AdminInfoSubmitterCreate'),

    url(r'^AdminInfoUser/create/$', Create_view.AdminInfoUserCreate, name='AdminInfoUserCreate'),

    url(r'^ExperimentalMethod/create/$', Create_view.ExperimentalMethodCreate, name='ExperimentalMethodCreate'),

    url(r'^MethodApplicabilityCondition/create/$', Create_view.MethodApplicabilityConditionCreate, name='MethodApplicabilityConditionCreate'),

    url(r'^StandardOperatingProcedure/create/$', Create_view.StandardOperatingProcedureCreate, name='StandardOperatingProcedureCreate'),

    url(r'^Author/create/$', Create_view.AuthorCreate, name='AuthorCreate'),

    url(r'^ClassificationOfDomains/create/$', Create_view.ClassificationOfDomainsCreate, name='ClassificationOfDomainsCreate'),

    url(r'^ClassificationOfExperiments/create/$', Create_view.ClassificationOfExperimentsCreate, name='ClassificationOfExperimentsCreate'),

    url(r'^ExperimentalHypothesis/create/$', Create_view.ExperimentalHypothesisCreate, name='ExperimentalHypothesisCreate'),

    url(r'^HypothesisComplexity/create/$', Create_view.HypothesisComplexityCreate, name='HypothesisComplexityCreate'),

    url(r'^Representation/create/$', Create_view.RepresentationCreate, name='RepresentationCreate'),

    url(r'^Proposition/create/$', Create_view.PropositionCreate, name='PropositionCreate'),

    url(r'^ExperimentalEquipment/create/$', Create_view.ExperimentalEquipmentCreate, name='ExperimentalEquipmentCreate'),

    url(r'^TechnicalDescription/create/$', Create_view.TechnicalDescriptionCreate, name='TechnicalDescriptionCreate'),

    url(r'^ExperimentalResults/create/$', Create_view.ExperimentalResultsCreate, name='ExperimentalResultsCreate'),

    url(r'^ObservationalError/create/$', Create_view.ObservationalErrorCreate, name='ObservationalErrorCreate'),

    url(r'^ResultError/create/$', Create_view.ResultErrorCreate, name='ResultErrorCreate'),

    url(r'^ResultsEvaluation/create/$', Create_view.ResultsEvaluationCreate, name='ResultsEvaluationCreate'),

    url(r'^ScientificInvestigation/create/$', Create_view.ScientificInvestigationCreate, name='ScientificInvestigationCreate'),

    url(r'^ExperimentalModel/create/$', Create_view.ExperimentalModelCreate, name='ExperimentalModelCreate'),

    url(r'^ExperimentalFactor/create/$', Create_view.ExperimentalFactorCreate, name='ExperimentalFactorCreate'),

    url(r'^ExperimentalTechnology/create/$', Create_view.ExperimentalTechnologyCreate, name='ExperimentalTechnologyCreate'),

    url(r'^ExperimentalRequirements/create/$', Create_view.ExperimentalRequirementsCreate, name='ExperimentalRequirementsCreate'),

    url(r'^HypothesisExplicitness/create/$', Create_view.HypothesisExplicitnessCreate, name='HypothesisExplicitnessCreate'),

    url(r'^LinguisticExpression/create/$', Create_view.LinguisticExpressionCreate, name='LinguisticExpressionCreate'),

    url(r'^FactRejectResearchHypothesis/create/$', Create_view.FactRejectResearchHypothesisCreate, name='FactRejectResearchHypothesisCreate'),

    url(r'^FactSupportResearchHypothesis/create/$', Create_view.FactSupportResearchHypothesisCreate, name='FactSupportResearchHypothesisCreate'),

    url(r'^Number/create/$', Create_view.NumberCreate, name='NumberCreate'),

    url(r'^GeneralityOfHypothesis/create/$', Create_view.GeneralityOfHypothesisCreate, name='GeneralityOfHypothesisCreate'),

    url(r'^ActionGoal/create/$', Create_view.ActionGoalCreate, name='ActionGoalCreate'),

    url(r'^ExperimentalGoal/create/$', Create_view.ExperimentalGoalCreate, name='ExperimentalGoalCreate'),

    url(r'^MethodGoal/create/$', Create_view.MethodGoalCreate, name='MethodGoalCreate'),

    url(r'^HypothesisFormation/create/$', Create_view.HypothesisFormationCreate, name='HypothesisFormationCreate'),

    url(r'^HypothesisRepresentation/create/$', Create_view.HypothesisRepresentationCreate, name='HypothesisRepresentationCreate'),

    url(r'^InformationGethering/create/$', Create_view.InformationGetheringCreate, name='InformationGetheringCreate'),

    url(r'^Sample/create/$', Create_view.SampleCreate, name='SampleCreate'),

    url(r'^Variability/create/$', Create_view.VariabilityCreate, name='VariabilityCreate'),

    url(r'^TimePoint/create/$', Create_view.TimePointCreate, name='TimePointCreate'),

    url(r'^User/create/$', Create_view.UserCreate, name='UserCreate'),

    url(r'^LoginName/create/$', Create_view.LoginNameCreate, name='LoginNameCreate'),

    url(r'^DomainModel/create/$', Create_view.DomainModelCreate, name='DomainModelCreate'),

    url(r'^GroupExperimentalObject/create/$', Create_view.GroupExperimentalObjectCreate, name='GroupExperimentalObjectCreate'),

    url(r'^ObjectOfExperiment/create/$', Create_view.ObjectOfExperimentCreate, name='ObjectOfExperimentCreate'),

    url(r'^ClassificationByModel/create/$', Create_view.ClassificationByModelCreate, name='ClassificationByModelCreate'),

    url(r'^StatisticsCharacteristic/create/$', Create_view.StatisticsCharacteristicCreate, name='StatisticsCharacteristicCreate'),

    url(r'^ParentGroup/create/$', Create_view.ParentGroupCreate, name='ParentGroupCreate'),

    url(r'^Password/create/$', Create_view.PasswordCreate, name='PasswordCreate'),

    url(r'^ProcedureExecuteExperiment/create/$', Create_view.ProcedureExecuteExperimentCreate, name='ProcedureExecuteExperimentCreate'),

    url(r'^PlanOfExperimentalActions/create/$', Create_view.PlanOfExperimentalActionsCreate, name='PlanOfExperimentalActionsCreate'),

    url(r'^PoblemAnalysis/create/$', Create_view.PoblemAnalysisCreate, name='PoblemAnalysisCreate'),

    url(r'^SubjectQualification/create/$', Create_view.SubjectQualificationCreate, name='SubjectQualificationCreate'),

    url(r'^BiblioReference/create/$', Create_view.BiblioReferenceCreate, name='BiblioReferenceCreate'),

    url(r'^Model/create/$', Create_view.ModelCreate, name='ModelCreate'),

    url(r'^ModelRepresentation/create/$', Create_view.ModelRepresentationCreate, name='ModelRepresentationCreate'),

    url(r'^RepresentationExperimentalExecutionProcedure/create/$', Create_view.RepresentationExperimentalExecutionProcedureCreate, name='RepresentationExperimentalExecutionProcedureCreate'),

    url(r'^RepresentationExperimentalGoal/create/$', Create_view.RepresentationExperimentalGoalCreate, name='RepresentationExperimentalGoalCreate'),

    url(r'^SampleRepresentativeness/create/$', Create_view.SampleRepresentativenessCreate, name='SampleRepresentativenessCreate'),

    url(r'^SampleSize/create/$', Create_view.SampleSizeCreate, name='SampleSizeCreate'),

    url(r'^SamplingMethod/create/$', Create_view.SamplingMethodCreate, name='SamplingMethodCreate'),

    url(r'^SystematicSampling/create/$', Create_view.SystematicSamplingCreate, name='SystematicSamplingCreate'),

    url(r'^SamplingRule/create/$', Create_view.SamplingRuleCreate, name='SamplingRuleCreate'),

    url(r'^ExperimentalStandard/create/$', Create_view.ExperimentalStandardCreate, name='ExperimentalStandardCreate'),

    url(r'^Error/create/$', Create_view.ErrorCreate, name='ErrorCreate'),

    url(r'^Dispersion/create/$', Create_view.DispersionCreate, name='DispersionCreate'),

    url(r'^LevelOfSignificance/create/$', Create_view.LevelOfSignificanceCreate, name='LevelOfSignificanceCreate'),

    url(r'^StatusExperimentalDocument/create/$', Create_view.StatusExperimentalDocumentCreate, name='StatusExperimentalDocumentCreate'),

    url(r'^ExperimentalDesignStrategy/create/$', Create_view.ExperimentalDesignStrategyCreate, name='ExperimentalDesignStrategyCreate'),

    url(r'^ExperimentalSubgoal/create/$', Create_view.ExperimentalSubgoalCreate, name='ExperimentalSubgoalCreate'),

    url(r'^SubjectMethod/create/$', Create_view.SubjectMethodCreate, name='SubjectMethodCreate'),

    url(r'^BaconianExperiment/create/$', Create_view.BaconianExperimentCreate, name='BaconianExperimentCreate'),

    url(r'^TargetVariable/create/$', Create_view.TargetVariableCreate, name='TargetVariableCreate'),

    url(r'^Title/create/$', Create_view.TitleCreate, name='TitleCreate'),

    url(r'^ValueEstimate/create/$', Create_view.ValueEstimateCreate, name='ValueEstimateCreate'),

    url(r'^ValueOfVariable/create/$', Create_view.ValueOfVariableCreate, name='ValueOfVariableCreate'),

    url(r'^Role/create/$', Create_view.RoleCreate, name='RoleCreate'),

    url(r'^CorpuscularObject/create/$', Create_view.CorpuscularObjectCreate, name='CorpuscularObjectCreate'),

    url(r'^AttributeRole/create/$', Create_view.AttributeRoleCreate, name='AttributeRoleCreate'),

    url(r'^Attribute/create/$', Create_view.AttributeCreate, name='AttributeCreate'),

    url(r'^Procedure/create/$', Create_view.ProcedureCreate, name='ProcedureCreate'),

    url(r'^AdministrativeInformation/create/$', Create_view.AdministrativeInformationCreate, name='AdministrativeInformationCreate'),

    url(r'^TitleExperiment/create/$', Create_view.TitleExperimentCreate, name='TitleExperimentCreate'),

    url(r'^NameEquipment/create/$', Create_view.NameEquipmentCreate, name='NameEquipmentCreate'),

    url(r'^PersonalName/create/$', Create_view.PersonalNameCreate, name='PersonalNameCreate'),

    url(r'^FieldOfStudy/create/$', Create_view.FieldOfStudyCreate, name='FieldOfStudyCreate'),

    url(r'^RelatedDomain/create/$', Create_view.RelatedDomainCreate, name='RelatedDomainCreate'),

    url(r'^ProductRole/create/$', Create_view.ProductRoleCreate, name='ProductRoleCreate'),

    url(r'^ScientificTask/create/$', Create_view.ScientificTaskCreate, name='ScientificTaskCreate'),

    url(r'^ExecutionOfExperiment/create/$', Create_view.ExecutionOfExperimentCreate, name='ExecutionOfExperimentCreate'),

    url(r'^CorporateName/create/$', Create_view.CorporateNameCreate, name='CorporateNameCreate'),

    url(r'^AttributeOfAction/create/$', Create_view.AttributeOfActionCreate, name='AttributeOfActionCreate'),

    url(r'^SentientAgent/create/$', Create_view.SentientAgentCreate, name='SentientAgentCreate'),

    url(r'^Robot/create/$', Create_view.RobotCreate, name='RobotCreate'),

    url(r'^Group/create/$', Create_view.GroupCreate, name='GroupCreate'),

    url(r'^Entity/create/$', Create_view.EntityCreate, name='EntityCreate'),

    url(r'^ResearchMethod/create/$', Create_view.ResearchMethodCreate, name='ResearchMethodCreate'),

    url(r'^Requirements/create/$', Create_view.RequirementsCreate, name='RequirementsCreate'),

    url(r'^AuthorSOP/create/$', Create_view.AuthorSOPCreate, name='AuthorSOPCreate'),

    url(r'^SubjectRole/create/$', Create_view.SubjectRoleCreate, name='SubjectRoleCreate'),

    url(r'^Classification/create/$', Create_view.ClassificationCreate, name='ClassificationCreate'),

    url(r'^Fact/create/$', Create_view.FactCreate, name='FactCreate'),

    url(r'^ModelAssumption/create/$', Create_view.ModelAssumptionCreate, name='ModelAssumptionCreate'),

    url(r'^Variable/create/$', Create_view.VariableCreate, name='VariableCreate'),

    url(r'^AttributeOfHypothesis/create/$', Create_view.AttributeOfHypothesisCreate, name='AttributeOfHypothesisCreate'),

    url(r'^ContentBearingObject/create/$', Create_view.ContentBearingObjectCreate, name='ContentBearingObjectCreate'),

    url(r'^Abstract/create/$', Create_view.AbstractCreate, name='AbstractCreate'),

    url(r'^Quantity/create/$', Create_view.QuantityCreate, name='QuantityCreate'),

    url(r'^Relation/create/$', Create_view.RelationCreate, name='RelationCreate'),

    url(r'^ProcessrelatedRole/create/$', Create_view.ProcessrelatedRoleCreate, name='ProcessrelatedRoleCreate'),

    url(r'^ContentBearingPhysical/create/$', Create_view.ContentBearingPhysicalCreate, name='ContentBearingPhysicalCreate'),

    url(r'^PhysicalQuantity/create/$', Create_view.PhysicalQuantityCreate, name='PhysicalQuantityCreate'),

    url(r'^Goal/create/$', Create_view.GoalCreate, name='GoalCreate'),

    url(r'^RepresentationExperimentalObservation/create/$', Create_view.RepresentationExperimentalObservationCreate, name='RepresentationExperimentalObservationCreate'),

    url(r'^RepresentationExperimentalResults/create/$', Create_view.RepresentationExperimentalResultsCreate, name='RepresentationExperimentalResultsCreate'),

    url(r'^AttributeGroup/create/$', Create_view.AttributeGroupCreate, name='AttributeGroupCreate'),

    url(r'^TimePosition/create/$', Create_view.TimePositionCreate, name='TimePositionCreate'),

    url(r'^ActorRole/create/$', Create_view.ActorRoleCreate, name='ActorRoleCreate'),

    url(r'^DocumentStage/create/$', Create_view.DocumentStageCreate, name='DocumentStageCreate'),

    url(r'^PermissionStatus/create/$', Create_view.PermissionStatusCreate, name='PermissionStatusCreate'),

    url(r'^Plan/create/$', Create_view.PlanCreate, name='PlanCreate'),

    url(r'^Reference/create/$', Create_view.ReferenceCreate, name='ReferenceCreate'),

    url(r'^AttributeSample/create/$', Create_view.AttributeSampleCreate, name='AttributeSampleCreate'),

    url(r'^ExperimentalRule/create/$', Create_view.ExperimentalRuleCreate, name='ExperimentalRuleCreate'),

    url(r'^Robustness/create/$', Create_view.RobustnessCreate, name='RobustnessCreate'),

    url(r'^Validity/create/$', Create_view.ValidityCreate, name='ValidityCreate'),

    url(r'^AttributeOfDocument/create/$', Create_view.AttributeOfDocumentCreate, name='AttributeOfDocumentCreate'),

    url(r'^PhysicalExperiment/create/$', Create_view.PhysicalExperimentCreate, name='PhysicalExperimentCreate'),

    url(r'^ComputationalData/create/$', Create_view.ComputationalDataCreate, name='ComputationalDataCreate'),

    url(r'^Predicate/create/$', Create_view.PredicateCreate, name='PredicateCreate'),

    url(r'^SelfConnectedObject/create/$', Create_view.SelfConnectedObjectCreate, name='SelfConnectedObjectCreate'),

    url(r'^ScientificActivity/create/$', Create_view.ScientificActivityCreate, name='ScientificActivityCreate'),

    url(r'^FormingClassificationSystem/create/$', Create_view.FormingClassificationSystemCreate, name='FormingClassificationSystemCreate'),

    url(r'^HypothesisForming/create/$', Create_view.HypothesisFormingCreate, name='HypothesisFormingCreate'),

    url(r'^InterpretingResult/create/$', Create_view.InterpretingResultCreate, name='InterpretingResultCreate'),

    url(r'^ProcessProblemAnalysis/create/$', Create_view.ProcessProblemAnalysisCreate, name='ProcessProblemAnalysisCreate'),

    url(r'^ResultEvaluating/create/$', Create_view.ResultEvaluatingCreate, name='ResultEvaluatingCreate'),

    url(r'^AttributeOfModel/create/$', Create_view.AttributeOfModelCreate, name='AttributeOfModelCreate'),

    url(r'^AttributeOfVariable/create/$', Create_view.AttributeOfVariableCreate, name='AttributeOfVariableCreate'),

    url(r'^Agent/create/$', Create_view.AgentCreate, name='AgentCreate'),

    url(r'^Collection/create/$', Create_view.CollectionCreate, name='CollectionCreate'),

    url(r'^EperimentalDesignTask/create/$', Create_view.EperimentalDesignTaskCreate, name='EperimentalDesignTaskCreate'),

    url(r'^Physical/create/$', Create_view.PhysicalCreate, name='PhysicalCreate'),

    url(r'^Object/create/$', Create_view.ObjectCreate, name='ObjectCreate'),

    url(r'^Process/create/$', Create_view.ProcessCreate, name='ProcessCreate'),

    url(r'^TaskRole/create/$', Create_view.TaskRoleCreate, name='TaskRoleCreate'),

    url(r'^TimeMeasure/create/$', Create_view.TimeMeasureCreate, name='TimeMeasureCreate'),

    url(r'^ActionrelatedRole/create/$', Create_view.ActionrelatedRoleCreate, name='ActionrelatedRoleCreate'),

    url(r'^AbductiveHypothesis/create/$', Create_view.AbductiveHypothesisCreate, name='AbductiveHypothesisCreate'),

    url(r'^InductiveHypothesis/create/$', Create_view.InductiveHypothesisCreate, name='InductiveHypothesisCreate'),

    url(r'^Adequacy/create/$', Create_view.AdequacyCreate, name='AdequacyCreate'),

    url(r'^AlternativeHypothesis/create/$', Create_view.AlternativeHypothesisCreate, name='AlternativeHypothesisCreate'),

    url(r'^NullHypothesis/create/$', Create_view.NullHypothesisCreate, name='NullHypothesisCreate'),

    url(r'^ResearchHypothesis/create/$', Create_view.ResearchHypothesisCreate, name='ResearchHypothesisCreate'),

    url(r'^Article/create/$', Create_view.ArticleCreate, name='ArticleCreate'),

    url(r'^Text/create/$', Create_view.TextCreate, name='TextCreate'),

    url(r'^ArtificialLanguage/create/$', Create_view.ArtificialLanguageCreate, name='ArtificialLanguageCreate'),

    url(r'^Language/create/$', Create_view.LanguageCreate, name='LanguageCreate'),

    url(r'^HumanLanguage/create/$', Create_view.HumanLanguageCreate, name='HumanLanguageCreate'),

    url(r'^Sentence/create/$', Create_view.SentenceCreate, name='SentenceCreate'),

    url(r'^AtomicAction/create/$', Create_view.AtomicActionCreate, name='AtomicActionCreate'),

    url(r'^ComplexAction/create/$', Create_view.ComplexActionCreate, name='ComplexActionCreate'),

    url(r'^AuthorProtocol/create/$', Create_view.AuthorProtocolCreate, name='AuthorProtocolCreate'),

    url(r'^Book/create/$', Create_view.BookCreate, name='BookCreate'),

    url(r'^CalculableVariable/create/$', Create_view.CalculableVariableCreate, name='CalculableVariableCreate'),

    url(r'^CapacityRequirements/create/$', Create_view.CapacityRequirementsCreate, name='CapacityRequirementsCreate'),

    url(r'^EnvironmentalRequirements/create/$', Create_view.EnvironmentalRequirementsCreate, name='EnvironmentalRequirementsCreate'),

    url(r'^FinancialRequirements/create/$', Create_view.FinancialRequirementsCreate, name='FinancialRequirementsCreate'),

    url(r'^ClassificationByDomain/create/$', Create_view.ClassificationByDomainCreate, name='ClassificationByDomainCreate'),

    url(r'^Classifying/create/$', Create_view.ClassifyingCreate, name='ClassifyingCreate'),

    url(r'^DesigningExperiment/create/$', Create_view.DesigningExperimentCreate, name='DesigningExperimentCreate'),

    url(r'^CoarseError/create/$', Create_view.CoarseErrorCreate, name='CoarseErrorCreate'),

    url(r'^MeasurementError/create/$', Create_view.MeasurementErrorCreate, name='MeasurementErrorCreate'),

    url(r'^ComparisonControl_TargetGroups/create/$', Create_view.ComparisonControl_TargetGroupsCreate, name='ComparisonControl_TargetGroupsCreate'),

    url(r'^PairedComparison/create/$', Create_view.PairedComparisonCreate, name='PairedComparisonCreate'),

    url(r'^PairedComparisonOfMatchingGroups/create/$', Create_view.PairedComparisonOfMatchingGroupsCreate, name='PairedComparisonOfMatchingGroupsCreate'),

    url(r'^PairedComparisonOfSingleSampleGroups/create/$', Create_view.PairedComparisonOfSingleSampleGroupsCreate, name='PairedComparisonOfSingleSampleGroupsCreate'),

    url(r'^QualityControl/create/$', Create_view.QualityControlCreate, name='QualityControlCreate'),

    url(r'^ComplexHypothesis/create/$', Create_view.ComplexHypothesisCreate, name='ComplexHypothesisCreate'),

    url(r'^SingleHypothesis/create/$', Create_view.SingleHypothesisCreate, name='SingleHypothesisCreate'),

    url(r'^ComputationalExperiment/create/$', Create_view.ComputationalExperimentCreate, name='ComputationalExperimentCreate'),

    url(r'^ComputeGoal/create/$', Create_view.ComputeGoalCreate, name='ComputeGoalCreate'),

    url(r'^ConfirmGoal/create/$', Create_view.ConfirmGoalCreate, name='ConfirmGoalCreate'),

    url(r'^ExplainGoal/create/$', Create_view.ExplainGoalCreate, name='ExplainGoalCreate'),

    url(r'^InvestigateGoal/create/$', Create_view.InvestigateGoalCreate, name='InvestigateGoalCreate'),

    url(r'^ComputerSimulation/create/$', Create_view.ComputerSimulationCreate, name='ComputerSimulationCreate'),

    url(r'^ConstantQuantity/create/$', Create_view.ConstantQuantityCreate, name='ConstantQuantityCreate'),

    url(r'^Controllability/create/$', Create_view.ControllabilityCreate, name='ControllabilityCreate'),

    url(r'^Independence/create/$', Create_view.IndependenceCreate, name='IndependenceCreate'),

    url(r'^DBReference/create/$', Create_view.DBReferenceCreate, name='DBReferenceCreate'),

    url(r'^DDC_Dewey_Classification/create/$', Create_view.DDC_Dewey_ClassificationCreate, name='DDC_Dewey_ClassificationCreate'),

    url(r'^LibraryOfCongressClassification/create/$', Create_view.LibraryOfCongressClassificationCreate, name='LibraryOfCongressClassificationCreate'),

    url(r'^NLMClassification/create/$', Create_view.NLMClassificationCreate, name='NLMClassificationCreate'),

    url(r'^ResearchCouncilsUKClassification/create/$', Create_view.ResearchCouncilsUKClassificationCreate, name='ResearchCouncilsUKClassificationCreate'),

    url(r'^DataRepresentationStandard/create/$', Create_view.DataRepresentationStandardCreate, name='DataRepresentationStandardCreate'),

    url(r'^DegreeOfModel/create/$', Create_view.DegreeOfModelCreate, name='DegreeOfModelCreate'),

    url(r'^DynamismOfModel/create/$', Create_view.DynamismOfModelCreate, name='DynamismOfModelCreate'),

    url(r'^DependentVariable/create/$', Create_view.DependentVariableCreate, name='DependentVariableCreate'),

    url(r'^Device/create/$', Create_view.DeviceCreate, name='DeviceCreate'),

    url(r'^DomainAction/create/$', Create_view.DomainActionCreate, name='DomainActionCreate'),

    url(r'^DoseResponse/create/$', Create_view.DoseResponseCreate, name='DoseResponseCreate'),

    url(r'^GeneKnockin/create/$', Create_view.GeneKnockinCreate, name='GeneKnockinCreate'),

    url(r'^GeneKnockout/create/$', Create_view.GeneKnockoutCreate, name='GeneKnockoutCreate'),

    url(r'^Normal_Disease/create/$', Create_view.Normal_DiseaseCreate, name='Normal_DiseaseCreate'),

    url(r'^TimeCourse/create/$', Create_view.TimeCourseCreate, name='TimeCourseCreate'),

    url(r'^Treated_Untreated/create/$', Create_view.Treated_UntreatedCreate, name='Treated_UntreatedCreate'),

    url(r'^DraftStatus/create/$', Create_view.DraftStatusCreate, name='DraftStatusCreate'),

    url(r'^DuhemEffect/create/$', Create_view.DuhemEffectCreate, name='DuhemEffectCreate'),

    url(r'^ExperimentalDesignEffect/create/$', Create_view.ExperimentalDesignEffectCreate, name='ExperimentalDesignEffectCreate'),

    url(r'^InstrumentationEffect/create/$', Create_view.InstrumentationEffectCreate, name='InstrumentationEffectCreate'),

    url(r'^ObjectEffect/create/$', Create_view.ObjectEffectCreate, name='ObjectEffectCreate'),

    url(r'^SubjectEffect/create/$', Create_view.SubjectEffectCreate, name='SubjectEffectCreate'),

    url(r'^TimeEffect/create/$', Create_view.TimeEffectCreate, name='TimeEffectCreate'),

    url(r'^DynamicModel/create/$', Create_view.DynamicModelCreate, name='DynamicModelCreate'),

    url(r'^StaticModel/create/$', Create_view.StaticModelCreate, name='StaticModelCreate'),

    url(r'^ErrorOfConclusion/create/$', Create_view.ErrorOfConclusionCreate, name='ErrorOfConclusionCreate'),

    url(r'^ExperimentalActionsPlanning/create/$', Create_view.ExperimentalActionsPlanningCreate, name='ExperimentalActionsPlanningCreate'),

    url(r'^ExperimentalEquipmentSelecting/create/$', Create_view.ExperimentalEquipmentSelectingCreate, name='ExperimentalEquipmentSelectingCreate'),

    url(r'^ExperimentalModelDesign/create/$', Create_view.ExperimentalModelDesignCreate, name='ExperimentalModelDesignCreate'),

    url(r'^ExperimentalObjectSelecting/create/$', Create_view.ExperimentalObjectSelectingCreate, name='ExperimentalObjectSelectingCreate'),

    url(r'^ExperimentalConclusion/create/$', Create_view.ExperimentalConclusionCreate, name='ExperimentalConclusionCreate'),

    url(r'^ExperimentalProtocol/create/$', Create_view.ExperimentalProtocolCreate, name='ExperimentalProtocolCreate'),

    url(r'^ExperimenteeBias/create/$', Create_view.ExperimenteeBiasCreate, name='ExperimenteeBiasCreate'),

    url(r'^HawthorneEffect/create/$', Create_view.HawthorneEffectCreate, name='HawthorneEffectCreate'),

    url(r'^MortalityEffect/create/$', Create_view.MortalityEffectCreate, name='MortalityEffectCreate'),

    url(r'^ExperimenterBias/create/$', Create_view.ExperimenterBiasCreate, name='ExperimenterBiasCreate'),

    url(r'^TestingEffect/create/$', Create_view.TestingEffectCreate, name='TestingEffectCreate'),

    url(r'^ExplicitHypothesis/create/$', Create_view.ExplicitHypothesisCreate, name='ExplicitHypothesisCreate'),

    url(r'^ImplicitHypothesis/create/$', Create_view.ImplicitHypothesisCreate, name='ImplicitHypothesisCreate'),

    url(r'^FactorLevel/create/$', Create_view.FactorLevelCreate, name='FactorLevelCreate'),

    url(r'^FactorialDesign/create/$', Create_view.FactorialDesignCreate, name='FactorialDesignCreate'),

    url(r'^FalseNegative/create/$', Create_view.FalseNegativeCreate, name='FalseNegativeCreate'),

    url(r'^HypothesisAcceptanceMistake/create/$', Create_view.HypothesisAcceptanceMistakeCreate, name='HypothesisAcceptanceMistakeCreate'),

    url(r'^FalsePpositive/create/$', Create_view.FalsePpositiveCreate, name='FalsePpositiveCreate'),

    url(r'^FaultyComparison/create/$', Create_view.FaultyComparisonCreate, name='FaultyComparisonCreate'),

    url(r'^Formula/create/$', Create_view.FormulaCreate, name='FormulaCreate'),

    url(r'^GalileanExperiment/create/$', Create_view.GalileanExperimentCreate, name='GalileanExperimentCreate'),

    url(r'^HandTool/create/$', Create_view.HandToolCreate, name='HandToolCreate'),

    url(r'^Tool/create/$', Create_view.ToolCreate, name='ToolCreate'),

    url(r'^Hardware/create/$', Create_view.HardwareCreate, name='HardwareCreate'),

    url(r'^HistoryEffect/create/$', Create_view.HistoryEffectCreate, name='HistoryEffectCreate'),

    url(r'^MaturationEffect/create/$', Create_view.MaturationEffectCreate, name='MaturationEffectCreate'),

    url(r'^HypothesisdrivenExperiment/create/$', Create_view.HypothesisdrivenExperimentCreate, name='HypothesisdrivenExperimentCreate'),

    url(r'^HypothesisformingExperiment/create/$', Create_view.HypothesisformingExperimentCreate, name='HypothesisformingExperimentCreate'),

    url(r'^Image/create/$', Create_view.ImageCreate, name='ImageCreate'),

    url(r'^ImproperSampling/create/$', Create_view.ImproperSamplingCreate, name='ImproperSamplingCreate'),

    url(r'^IncompleteDataError/create/$', Create_view.IncompleteDataErrorCreate, name='IncompleteDataErrorCreate'),

    url(r'^IndependentVariable/create/$', Create_view.IndependentVariableCreate, name='IndependentVariableCreate'),

    url(r'^InferableVariable/create/$', Create_view.InferableVariableCreate, name='InferableVariableCreate'),

    url(r'^InternalStatus/create/$', Create_view.InternalStatusCreate, name='InternalStatusCreate'),

    url(r'^RestrictedStatus/create/$', Create_view.RestrictedStatusCreate, name='RestrictedStatusCreate'),

    url(r'^Journal/create/$', Create_view.JournalCreate, name='JournalCreate'),

    url(r'^LinearModel/create/$', Create_view.LinearModelCreate, name='LinearModelCreate'),

    url(r'^NonlinearModel/create/$', Create_view.NonlinearModelCreate, name='NonlinearModelCreate'),

    url(r'^LogicalModelRepresentation/create/$', Create_view.LogicalModelRepresentationCreate, name='LogicalModelRepresentationCreate'),

    url(r'^Machine/create/$', Create_view.MachineCreate, name='MachineCreate'),

    url(r'^MachineTool/create/$', Create_view.MachineToolCreate, name='MachineToolCreate'),

    url(r'^Magazine/create/$', Create_view.MagazineCreate, name='MagazineCreate'),

    url(r'^Materials/create/$', Create_view.MaterialsCreate, name='MaterialsCreate'),

    url(r'^MathematicalModelRepresentation/create/$', Create_view.MathematicalModelRepresentationCreate, name='MathematicalModelRepresentationCreate'),

    url(r'^MetabolomicExperiment/create/$', Create_view.MetabolomicExperimentCreate, name='MetabolomicExperimentCreate'),

    url(r'^MethodsComparison/create/$', Create_view.MethodsComparisonCreate, name='MethodsComparisonCreate'),

    url(r'^SubjectsComparison/create/$', Create_view.SubjectsComparisonCreate, name='SubjectsComparisonCreate'),

    url(r'^MicroarrayExperiment/create/$', Create_view.MicroarrayExperimentCreate, name='MicroarrayExperimentCreate'),

    url(r'^MultifactorExperiment/create/$', Create_view.MultifactorExperimentCreate, name='MultifactorExperimentCreate'),

    url(r'^OnefactorExperiment/create/$', Create_view.OnefactorExperimentCreate, name='OnefactorExperimentCreate'),

    url(r'^TwofactorExperiment/create/$', Create_view.TwofactorExperimentCreate, name='TwofactorExperimentCreate'),

    url(r'^NaturalLanguage/create/$', Create_view.NaturalLanguageCreate, name='NaturalLanguageCreate'),

    url(r'^NonRestrictedStatus/create/$', Create_view.NonRestrictedStatusCreate, name='NonRestrictedStatusCreate'),

    url(r'^NormalizationStrategy/create/$', Create_view.NormalizationStrategyCreate, name='NormalizationStrategyCreate'),

    url(r'^ObjectMethod/create/$', Create_view.ObjectMethodCreate, name='ObjectMethodCreate'),

    url(r'^ObjectOfAction/create/$', Create_view.ObjectOfActionCreate, name='ObjectOfActionCreate'),

    url(r'^ObservableVariable/create/$', Create_view.ObservableVariableCreate, name='ObservableVariableCreate'),

    url(r'^Paper/create/$', Create_view.PaperCreate, name='PaperCreate'),

    url(r'^ParticlePhysicsExperiment/create/$', Create_view.ParticlePhysicsExperimentCreate, name='ParticlePhysicsExperimentCreate'),

    url(r'^PresentingSampling/create/$', Create_view.PresentingSamplingCreate, name='PresentingSamplingCreate'),

    url(r'^Proceedings/create/$', Create_view.ProceedingsCreate, name='ProceedingsCreate'),

    url(r'^Program/create/$', Create_view.ProgramCreate, name='ProgramCreate'),

    url(r'^ProteomicExperiment/create/$', Create_view.ProteomicExperimentCreate, name='ProteomicExperimentCreate'),

    url(r'^Provider/create/$', Create_view.ProviderCreate, name='ProviderCreate'),

    url(r'^PublicAcademicStatus/create/$', Create_view.PublicAcademicStatusCreate, name='PublicAcademicStatusCreate'),

    url(r'^PublicStatus/create/$', Create_view.PublicStatusCreate, name='PublicStatusCreate'),

    url(r'^RandomError/create/$', Create_view.RandomErrorCreate, name='RandomErrorCreate'),

    url(r'^RandomSampling/create/$', Create_view.RandomSamplingCreate, name='RandomSamplingCreate'),

    url(r'^RecommendationSatus/create/$', Create_view.RecommendationSatusCreate, name='RecommendationSatusCreate'),

    url(r'^RecordingAction/create/$', Create_view.RecordingActionCreate, name='RecordingActionCreate'),

    url(r'^Region/create/$', Create_view.RegionCreate, name='RegionCreate'),

    url(r'^RepresenationalMedium/create/$', Create_view.RepresenationalMediumCreate, name='RepresenationalMediumCreate'),

    url(r'^SUMOSynonym/create/$', Create_view.SUMOSynonymCreate, name='SUMOSynonymCreate'),

    url(r'^SampleForming/create/$', Create_view.SampleFormingCreate, name='SampleFormingCreate'),

    url(r'^SequentialDesign/create/$', Create_view.SequentialDesignCreate, name='SequentialDesignCreate'),

    url(r'^Software/create/$', Create_view.SoftwareCreate, name='SoftwareCreate'),

    url(r'^Sound/create/$', Create_view.SoundCreate, name='SoundCreate'),

    url(r'^SplitSamples/create/$', Create_view.SplitSamplesCreate, name='SplitSamplesCreate'),

    url(r'^SymmetricalMatching/create/$', Create_view.SymmetricalMatchingCreate, name='SymmetricalMatchingCreate'),

    url(r'^StatisticalError/create/$', Create_view.StatisticalErrorCreate, name='StatisticalErrorCreate'),

    url(r'^StratifiedSampling/create/$', Create_view.StratifiedSamplingCreate, name='StratifiedSamplingCreate'),

    url(r'^SubjectOfAction/create/$', Create_view.SubjectOfActionCreate, name='SubjectOfActionCreate'),

    url(r'^SubjectOfExperiment/create/$', Create_view.SubjectOfExperimentCreate, name='SubjectOfExperimentCreate'),

    url(r'^Submitter/create/$', Create_view.SubmitterCreate, name='SubmitterCreate'),

    url(r'^Summary/create/$', Create_view.SummaryCreate, name='SummaryCreate'),

    url(r'^SystematicError/create/$', Create_view.SystematicErrorCreate, name='SystematicErrorCreate'),

    url(r'^TechnicalReport/create/$', Create_view.TechnicalReportCreate, name='TechnicalReportCreate'),

    url(r'^TimeDuration/create/$', Create_view.TimeDurationCreate, name='TimeDurationCreate'),

    url(r'^TimeInterval/create/$', Create_view.TimeIntervalCreate, name='TimeIntervalCreate'),

    url(r'^URL/create/$', Create_view.URLCreate, name='URLCreate'),


    url(r'^Thing/(?P<pk>\d+)/read/$', Read_view.ThingRead, name='ThingRead'),

    url(r'^RepresentationForm/(?P<pk>\d+)/read/$', Read_view.RepresentationFormRead, name='RepresentationFormRead'),

    url(r'^Artifact/(?P<pk>\d+)/read/$', Read_view.ArtifactRead, name='ArtifactRead'),

    url(r'^SynonymousExternalConcept/(?P<pk>\d+)/read/$', Read_view.SynonymousExternalConceptRead, name='SynonymousExternalConceptRead'),

    url(r'^Name/(?P<pk>\d+)/read/$', Read_view.NameRead, name='NameRead'),

    url(r'^ScientificExperiment/(?P<pk>\d+)/read/$', Read_view.ScientificExperimentRead, name='ScientificExperimentRead'),

    url(r'^ExperimentalDesign/(?P<pk>\d+)/read/$', Read_view.ExperimentalDesignRead, name='ExperimentalDesignRead'),

    url(r'^AdminInfoExperiment/(?P<pk>\d+)/read/$', Read_view.AdminInfoExperimentRead, name='AdminInfoExperimentRead'),

    url(r'^IDExperiment/(?P<pk>\d+)/read/$', Read_view.IDExperimentRead, name='IDExperimentRead'),

    url(r'^DomainOfExperiment/(?P<pk>\d+)/read/$', Read_view.DomainOfExperimentRead, name='DomainOfExperimentRead'),

    url(r'^ExperimentalObservation/(?P<pk>\d+)/read/$', Read_view.ExperimentalObservationRead, name='ExperimentalObservationRead'),

    url(r'^ResultsInterpretation/(?P<pk>\d+)/read/$', Read_view.ResultsInterpretationRead, name='ResultsInterpretationRead'),

    url(r'^ExperimentalAction/(?P<pk>\d+)/read/$', Read_view.ExperimentalActionRead, name='ExperimentalActionRead'),

    url(r'^ActionName/(?P<pk>\d+)/read/$', Read_view.ActionNameRead, name='ActionNameRead'),

    url(r'^ActionComplexity/(?P<pk>\d+)/read/$', Read_view.ActionComplexityRead, name='ActionComplexityRead'),

    url(r'^Human/(?P<pk>\d+)/read/$', Read_view.HumanRead, name='HumanRead'),

    url(r'^Organization/(?P<pk>\d+)/read/$', Read_view.OrganizationRead, name='OrganizationRead'),

    url(r'^AdminInfoAuthor/(?P<pk>\d+)/read/$', Read_view.AdminInfoAuthorRead, name='AdminInfoAuthorRead'),

    url(r'^AdminInfoObjectOfExperiment/(?P<pk>\d+)/read/$', Read_view.AdminInfoObjectOfExperimentRead, name='AdminInfoObjectOfExperimentRead'),

    url(r'^AdminInfoProvider/(?P<pk>\d+)/read/$', Read_view.AdminInfoProviderRead, name='AdminInfoProviderRead'),

    url(r'^AdminInfoSubjectOfExperiment/(?P<pk>\d+)/read/$', Read_view.AdminInfoSubjectOfExperimentRead, name='AdminInfoSubjectOfExperimentRead'),

    url(r'^AdminInfoSubmitter/(?P<pk>\d+)/read/$', Read_view.AdminInfoSubmitterRead, name='AdminInfoSubmitterRead'),

    url(r'^AdminInfoUser/(?P<pk>\d+)/read/$', Read_view.AdminInfoUserRead, name='AdminInfoUserRead'),

    url(r'^ExperimentalMethod/(?P<pk>\d+)/read/$', Read_view.ExperimentalMethodRead, name='ExperimentalMethodRead'),

    url(r'^MethodApplicabilityCondition/(?P<pk>\d+)/read/$', Read_view.MethodApplicabilityConditionRead, name='MethodApplicabilityConditionRead'),

    url(r'^StandardOperatingProcedure/(?P<pk>\d+)/read/$', Read_view.StandardOperatingProcedureRead, name='StandardOperatingProcedureRead'),

    url(r'^Author/(?P<pk>\d+)/read/$', Read_view.AuthorRead, name='AuthorRead'),

    url(r'^ClassificationOfDomains/(?P<pk>\d+)/read/$', Read_view.ClassificationOfDomainsRead, name='ClassificationOfDomainsRead'),

    url(r'^ClassificationOfExperiments/(?P<pk>\d+)/read/$', Read_view.ClassificationOfExperimentsRead, name='ClassificationOfExperimentsRead'),

    url(r'^ExperimentalHypothesis/(?P<pk>\d+)/read/$', Read_view.ExperimentalHypothesisRead, name='ExperimentalHypothesisRead'),

    url(r'^HypothesisComplexity/(?P<pk>\d+)/read/$', Read_view.HypothesisComplexityRead, name='HypothesisComplexityRead'),

    url(r'^Representation/(?P<pk>\d+)/read/$', Read_view.RepresentationRead, name='RepresentationRead'),

    url(r'^Proposition/(?P<pk>\d+)/read/$', Read_view.PropositionRead, name='PropositionRead'),

    url(r'^ExperimentalEquipment/(?P<pk>\d+)/read/$', Read_view.ExperimentalEquipmentRead, name='ExperimentalEquipmentRead'),

    url(r'^TechnicalDescription/(?P<pk>\d+)/read/$', Read_view.TechnicalDescriptionRead, name='TechnicalDescriptionRead'),

    url(r'^ExperimentalResults/(?P<pk>\d+)/read/$', Read_view.ExperimentalResultsRead, name='ExperimentalResultsRead'),

    url(r'^ObservationalError/(?P<pk>\d+)/read/$', Read_view.ObservationalErrorRead, name='ObservationalErrorRead'),

    url(r'^ResultError/(?P<pk>\d+)/read/$', Read_view.ResultErrorRead, name='ResultErrorRead'),

    url(r'^ResultsEvaluation/(?P<pk>\d+)/read/$', Read_view.ResultsEvaluationRead, name='ResultsEvaluationRead'),

    url(r'^ScientificInvestigation/(?P<pk>\d+)/read/$', Read_view.ScientificInvestigationRead, name='ScientificInvestigationRead'),

    url(r'^ExperimentalModel/(?P<pk>\d+)/read/$', Read_view.ExperimentalModelRead, name='ExperimentalModelRead'),

    url(r'^ExperimentalFactor/(?P<pk>\d+)/read/$', Read_view.ExperimentalFactorRead, name='ExperimentalFactorRead'),

    url(r'^ExperimentalTechnology/(?P<pk>\d+)/read/$', Read_view.ExperimentalTechnologyRead, name='ExperimentalTechnologyRead'),

    url(r'^ExperimentalRequirements/(?P<pk>\d+)/read/$', Read_view.ExperimentalRequirementsRead, name='ExperimentalRequirementsRead'),

    url(r'^HypothesisExplicitness/(?P<pk>\d+)/read/$', Read_view.HypothesisExplicitnessRead, name='HypothesisExplicitnessRead'),

    url(r'^LinguisticExpression/(?P<pk>\d+)/read/$', Read_view.LinguisticExpressionRead, name='LinguisticExpressionRead'),

    url(r'^FactRejectResearchHypothesis/(?P<pk>\d+)/read/$', Read_view.FactRejectResearchHypothesisRead, name='FactRejectResearchHypothesisRead'),

    url(r'^FactSupportResearchHypothesis/(?P<pk>\d+)/read/$', Read_view.FactSupportResearchHypothesisRead, name='FactSupportResearchHypothesisRead'),

    url(r'^Number/(?P<pk>\d+)/read/$', Read_view.NumberRead, name='NumberRead'),

    url(r'^GeneralityOfHypothesis/(?P<pk>\d+)/read/$', Read_view.GeneralityOfHypothesisRead, name='GeneralityOfHypothesisRead'),

    url(r'^ActionGoal/(?P<pk>\d+)/read/$', Read_view.ActionGoalRead, name='ActionGoalRead'),

    url(r'^ExperimentalGoal/(?P<pk>\d+)/read/$', Read_view.ExperimentalGoalRead, name='ExperimentalGoalRead'),

    url(r'^MethodGoal/(?P<pk>\d+)/read/$', Read_view.MethodGoalRead, name='MethodGoalRead'),

    url(r'^HypothesisFormation/(?P<pk>\d+)/read/$', Read_view.HypothesisFormationRead, name='HypothesisFormationRead'),

    url(r'^HypothesisRepresentation/(?P<pk>\d+)/read/$', Read_view.HypothesisRepresentationRead, name='HypothesisRepresentationRead'),

    url(r'^InformationGethering/(?P<pk>\d+)/read/$', Read_view.InformationGetheringRead, name='InformationGetheringRead'),

    url(r'^Sample/(?P<pk>\d+)/read/$', Read_view.SampleRead, name='SampleRead'),

    url(r'^Variability/(?P<pk>\d+)/read/$', Read_view.VariabilityRead, name='VariabilityRead'),

    url(r'^TimePoint/(?P<pk>\d+)/read/$', Read_view.TimePointRead, name='TimePointRead'),

    url(r'^User/(?P<pk>\d+)/read/$', Read_view.UserRead, name='UserRead'),

    url(r'^LoginName/(?P<pk>\d+)/read/$', Read_view.LoginNameRead, name='LoginNameRead'),

    url(r'^DomainModel/(?P<pk>\d+)/read/$', Read_view.DomainModelRead, name='DomainModelRead'),

    url(r'^GroupExperimentalObject/(?P<pk>\d+)/read/$', Read_view.GroupExperimentalObjectRead, name='GroupExperimentalObjectRead'),

    url(r'^ObjectOfExperiment/(?P<pk>\d+)/read/$', Read_view.ObjectOfExperimentRead, name='ObjectOfExperimentRead'),

    url(r'^ClassificationByModel/(?P<pk>\d+)/read/$', Read_view.ClassificationByModelRead, name='ClassificationByModelRead'),

    url(r'^StatisticsCharacteristic/(?P<pk>\d+)/read/$', Read_view.StatisticsCharacteristicRead, name='StatisticsCharacteristicRead'),

    url(r'^ParentGroup/(?P<pk>\d+)/read/$', Read_view.ParentGroupRead, name='ParentGroupRead'),

    url(r'^Password/(?P<pk>\d+)/read/$', Read_view.PasswordRead, name='PasswordRead'),

    url(r'^ProcedureExecuteExperiment/(?P<pk>\d+)/read/$', Read_view.ProcedureExecuteExperimentRead, name='ProcedureExecuteExperimentRead'),

    url(r'^PlanOfExperimentalActions/(?P<pk>\d+)/read/$', Read_view.PlanOfExperimentalActionsRead, name='PlanOfExperimentalActionsRead'),

    url(r'^PoblemAnalysis/(?P<pk>\d+)/read/$', Read_view.PoblemAnalysisRead, name='PoblemAnalysisRead'),

    url(r'^SubjectQualification/(?P<pk>\d+)/read/$', Read_view.SubjectQualificationRead, name='SubjectQualificationRead'),

    url(r'^BiblioReference/(?P<pk>\d+)/read/$', Read_view.BiblioReferenceRead, name='BiblioReferenceRead'),

    url(r'^Model/(?P<pk>\d+)/read/$', Read_view.ModelRead, name='ModelRead'),

    url(r'^ModelRepresentation/(?P<pk>\d+)/read/$', Read_view.ModelRepresentationRead, name='ModelRepresentationRead'),

    url(r'^RepresentationExperimentalExecutionProcedure/(?P<pk>\d+)/read/$', Read_view.RepresentationExperimentalExecutionProcedureRead, name='RepresentationExperimentalExecutionProcedureRead'),

    url(r'^RepresentationExperimentalGoal/(?P<pk>\d+)/read/$', Read_view.RepresentationExperimentalGoalRead, name='RepresentationExperimentalGoalRead'),

    url(r'^SampleRepresentativeness/(?P<pk>\d+)/read/$', Read_view.SampleRepresentativenessRead, name='SampleRepresentativenessRead'),

    url(r'^SampleSize/(?P<pk>\d+)/read/$', Read_view.SampleSizeRead, name='SampleSizeRead'),

    url(r'^SamplingMethod/(?P<pk>\d+)/read/$', Read_view.SamplingMethodRead, name='SamplingMethodRead'),

    url(r'^SystematicSampling/(?P<pk>\d+)/read/$', Read_view.SystematicSamplingRead, name='SystematicSamplingRead'),

    url(r'^SamplingRule/(?P<pk>\d+)/read/$', Read_view.SamplingRuleRead, name='SamplingRuleRead'),

    url(r'^ExperimentalStandard/(?P<pk>\d+)/read/$', Read_view.ExperimentalStandardRead, name='ExperimentalStandardRead'),

    url(r'^Error/(?P<pk>\d+)/read/$', Read_view.ErrorRead, name='ErrorRead'),

    url(r'^Dispersion/(?P<pk>\d+)/read/$', Read_view.DispersionRead, name='DispersionRead'),

    url(r'^LevelOfSignificance/(?P<pk>\d+)/read/$', Read_view.LevelOfSignificanceRead, name='LevelOfSignificanceRead'),

    url(r'^StatusExperimentalDocument/(?P<pk>\d+)/read/$', Read_view.StatusExperimentalDocumentRead, name='StatusExperimentalDocumentRead'),

    url(r'^ExperimentalDesignStrategy/(?P<pk>\d+)/read/$', Read_view.ExperimentalDesignStrategyRead, name='ExperimentalDesignStrategyRead'),

    url(r'^ExperimentalSubgoal/(?P<pk>\d+)/read/$', Read_view.ExperimentalSubgoalRead, name='ExperimentalSubgoalRead'),

    url(r'^SubjectMethod/(?P<pk>\d+)/read/$', Read_view.SubjectMethodRead, name='SubjectMethodRead'),

    url(r'^BaconianExperiment/(?P<pk>\d+)/read/$', Read_view.BaconianExperimentRead, name='BaconianExperimentRead'),

    url(r'^TargetVariable/(?P<pk>\d+)/read/$', Read_view.TargetVariableRead, name='TargetVariableRead'),

    url(r'^Title/(?P<pk>\d+)/read/$', Read_view.TitleRead, name='TitleRead'),

    url(r'^ValueEstimate/(?P<pk>\d+)/read/$', Read_view.ValueEstimateRead, name='ValueEstimateRead'),

    url(r'^ValueOfVariable/(?P<pk>\d+)/read/$', Read_view.ValueOfVariableRead, name='ValueOfVariableRead'),

    url(r'^Role/(?P<pk>\d+)/read/$', Read_view.RoleRead, name='RoleRead'),

    url(r'^CorpuscularObject/(?P<pk>\d+)/read/$', Read_view.CorpuscularObjectRead, name='CorpuscularObjectRead'),

    url(r'^AttributeRole/(?P<pk>\d+)/read/$', Read_view.AttributeRoleRead, name='AttributeRoleRead'),

    url(r'^Attribute/(?P<pk>\d+)/read/$', Read_view.AttributeRead, name='AttributeRead'),

    url(r'^Procedure/(?P<pk>\d+)/read/$', Read_view.ProcedureRead, name='ProcedureRead'),

    url(r'^AdministrativeInformation/(?P<pk>\d+)/read/$', Read_view.AdministrativeInformationRead, name='AdministrativeInformationRead'),

    url(r'^TitleExperiment/(?P<pk>\d+)/read/$', Read_view.TitleExperimentRead, name='TitleExperimentRead'),

    url(r'^NameEquipment/(?P<pk>\d+)/read/$', Read_view.NameEquipmentRead, name='NameEquipmentRead'),

    url(r'^PersonalName/(?P<pk>\d+)/read/$', Read_view.PersonalNameRead, name='PersonalNameRead'),

    url(r'^FieldOfStudy/(?P<pk>\d+)/read/$', Read_view.FieldOfStudyRead, name='FieldOfStudyRead'),

    url(r'^RelatedDomain/(?P<pk>\d+)/read/$', Read_view.RelatedDomainRead, name='RelatedDomainRead'),

    url(r'^ProductRole/(?P<pk>\d+)/read/$', Read_view.ProductRoleRead, name='ProductRoleRead'),

    url(r'^ScientificTask/(?P<pk>\d+)/read/$', Read_view.ScientificTaskRead, name='ScientificTaskRead'),

    url(r'^ExecutionOfExperiment/(?P<pk>\d+)/read/$', Read_view.ExecutionOfExperimentRead, name='ExecutionOfExperimentRead'),

    url(r'^CorporateName/(?P<pk>\d+)/read/$', Read_view.CorporateNameRead, name='CorporateNameRead'),

    url(r'^AttributeOfAction/(?P<pk>\d+)/read/$', Read_view.AttributeOfActionRead, name='AttributeOfActionRead'),

    url(r'^SentientAgent/(?P<pk>\d+)/read/$', Read_view.SentientAgentRead, name='SentientAgentRead'),

    url(r'^Robot/(?P<pk>\d+)/read/$', Read_view.RobotRead, name='RobotRead'),

    url(r'^Group/(?P<pk>\d+)/read/$', Read_view.GroupRead, name='GroupRead'),

    url(r'^Entity/(?P<pk>\d+)/read/$', Read_view.EntityRead, name='EntityRead'),

    url(r'^ResearchMethod/(?P<pk>\d+)/read/$', Read_view.ResearchMethodRead, name='ResearchMethodRead'),

    url(r'^Requirements/(?P<pk>\d+)/read/$', Read_view.RequirementsRead, name='RequirementsRead'),

    url(r'^AuthorSOP/(?P<pk>\d+)/read/$', Read_view.AuthorSOPRead, name='AuthorSOPRead'),

    url(r'^SubjectRole/(?P<pk>\d+)/read/$', Read_view.SubjectRoleRead, name='SubjectRoleRead'),

    url(r'^Classification/(?P<pk>\d+)/read/$', Read_view.ClassificationRead, name='ClassificationRead'),

    url(r'^Fact/(?P<pk>\d+)/read/$', Read_view.FactRead, name='FactRead'),

    url(r'^ModelAssumption/(?P<pk>\d+)/read/$', Read_view.ModelAssumptionRead, name='ModelAssumptionRead'),

    url(r'^Variable/(?P<pk>\d+)/read/$', Read_view.VariableRead, name='VariableRead'),

    url(r'^AttributeOfHypothesis/(?P<pk>\d+)/read/$', Read_view.AttributeOfHypothesisRead, name='AttributeOfHypothesisRead'),

    url(r'^ContentBearingObject/(?P<pk>\d+)/read/$', Read_view.ContentBearingObjectRead, name='ContentBearingObjectRead'),

    url(r'^Abstract/(?P<pk>\d+)/read/$', Read_view.AbstractRead, name='AbstractRead'),

    url(r'^Quantity/(?P<pk>\d+)/read/$', Read_view.QuantityRead, name='QuantityRead'),

    url(r'^Relation/(?P<pk>\d+)/read/$', Read_view.RelationRead, name='RelationRead'),

    url(r'^ProcessrelatedRole/(?P<pk>\d+)/read/$', Read_view.ProcessrelatedRoleRead, name='ProcessrelatedRoleRead'),

    url(r'^ContentBearingPhysical/(?P<pk>\d+)/read/$', Read_view.ContentBearingPhysicalRead, name='ContentBearingPhysicalRead'),

    url(r'^PhysicalQuantity/(?P<pk>\d+)/read/$', Read_view.PhysicalQuantityRead, name='PhysicalQuantityRead'),

    url(r'^Goal/(?P<pk>\d+)/read/$', Read_view.GoalRead, name='GoalRead'),

    url(r'^RepresentationExperimentalObservation/(?P<pk>\d+)/read/$', Read_view.RepresentationExperimentalObservationRead, name='RepresentationExperimentalObservationRead'),

    url(r'^RepresentationExperimentalResults/(?P<pk>\d+)/read/$', Read_view.RepresentationExperimentalResultsRead, name='RepresentationExperimentalResultsRead'),

    url(r'^AttributeGroup/(?P<pk>\d+)/read/$', Read_view.AttributeGroupRead, name='AttributeGroupRead'),

    url(r'^TimePosition/(?P<pk>\d+)/read/$', Read_view.TimePositionRead, name='TimePositionRead'),

    url(r'^ActorRole/(?P<pk>\d+)/read/$', Read_view.ActorRoleRead, name='ActorRoleRead'),

    url(r'^DocumentStage/(?P<pk>\d+)/read/$', Read_view.DocumentStageRead, name='DocumentStageRead'),

    url(r'^PermissionStatus/(?P<pk>\d+)/read/$', Read_view.PermissionStatusRead, name='PermissionStatusRead'),

    url(r'^Plan/(?P<pk>\d+)/read/$', Read_view.PlanRead, name='PlanRead'),

    url(r'^Reference/(?P<pk>\d+)/read/$', Read_view.ReferenceRead, name='ReferenceRead'),

    url(r'^AttributeSample/(?P<pk>\d+)/read/$', Read_view.AttributeSampleRead, name='AttributeSampleRead'),

    url(r'^ExperimentalRule/(?P<pk>\d+)/read/$', Read_view.ExperimentalRuleRead, name='ExperimentalRuleRead'),

    url(r'^Robustness/(?P<pk>\d+)/read/$', Read_view.RobustnessRead, name='RobustnessRead'),

    url(r'^Validity/(?P<pk>\d+)/read/$', Read_view.ValidityRead, name='ValidityRead'),

    url(r'^AttributeOfDocument/(?P<pk>\d+)/read/$', Read_view.AttributeOfDocumentRead, name='AttributeOfDocumentRead'),

    url(r'^PhysicalExperiment/(?P<pk>\d+)/read/$', Read_view.PhysicalExperimentRead, name='PhysicalExperimentRead'),

    url(r'^ComputationalData/(?P<pk>\d+)/read/$', Read_view.ComputationalDataRead, name='ComputationalDataRead'),

    url(r'^Predicate/(?P<pk>\d+)/read/$', Read_view.PredicateRead, name='PredicateRead'),

    url(r'^SelfConnectedObject/(?P<pk>\d+)/read/$', Read_view.SelfConnectedObjectRead, name='SelfConnectedObjectRead'),

    url(r'^ScientificActivity/(?P<pk>\d+)/read/$', Read_view.ScientificActivityRead, name='ScientificActivityRead'),

    url(r'^FormingClassificationSystem/(?P<pk>\d+)/read/$', Read_view.FormingClassificationSystemRead, name='FormingClassificationSystemRead'),

    url(r'^HypothesisForming/(?P<pk>\d+)/read/$', Read_view.HypothesisFormingRead, name='HypothesisFormingRead'),

    url(r'^InterpretingResult/(?P<pk>\d+)/read/$', Read_view.InterpretingResultRead, name='InterpretingResultRead'),

    url(r'^ProcessProblemAnalysis/(?P<pk>\d+)/read/$', Read_view.ProcessProblemAnalysisRead, name='ProcessProblemAnalysisRead'),

    url(r'^ResultEvaluating/(?P<pk>\d+)/read/$', Read_view.ResultEvaluatingRead, name='ResultEvaluatingRead'),

    url(r'^AttributeOfModel/(?P<pk>\d+)/read/$', Read_view.AttributeOfModelRead, name='AttributeOfModelRead'),

    url(r'^AttributeOfVariable/(?P<pk>\d+)/read/$', Read_view.AttributeOfVariableRead, name='AttributeOfVariableRead'),

    url(r'^Agent/(?P<pk>\d+)/read/$', Read_view.AgentRead, name='AgentRead'),

    url(r'^Collection/(?P<pk>\d+)/read/$', Read_view.CollectionRead, name='CollectionRead'),

    url(r'^EperimentalDesignTask/(?P<pk>\d+)/read/$', Read_view.EperimentalDesignTaskRead, name='EperimentalDesignTaskRead'),

    url(r'^Physical/(?P<pk>\d+)/read/$', Read_view.PhysicalRead, name='PhysicalRead'),

    url(r'^Object/(?P<pk>\d+)/read/$', Read_view.ObjectRead, name='ObjectRead'),

    url(r'^Process/(?P<pk>\d+)/read/$', Read_view.ProcessRead, name='ProcessRead'),

    url(r'^TaskRole/(?P<pk>\d+)/read/$', Read_view.TaskRoleRead, name='TaskRoleRead'),

    url(r'^TimeMeasure/(?P<pk>\d+)/read/$', Read_view.TimeMeasureRead, name='TimeMeasureRead'),

    url(r'^ActionrelatedRole/(?P<pk>\d+)/read/$', Read_view.ActionrelatedRoleRead, name='ActionrelatedRoleRead'),

    url(r'^AbductiveHypothesis/(?P<pk>\d+)/read/$', Read_view.AbductiveHypothesisRead, name='AbductiveHypothesisRead'),

    url(r'^InductiveHypothesis/(?P<pk>\d+)/read/$', Read_view.InductiveHypothesisRead, name='InductiveHypothesisRead'),

    url(r'^Adequacy/(?P<pk>\d+)/read/$', Read_view.AdequacyRead, name='AdequacyRead'),

    url(r'^AlternativeHypothesis/(?P<pk>\d+)/read/$', Read_view.AlternativeHypothesisRead, name='AlternativeHypothesisRead'),

    url(r'^NullHypothesis/(?P<pk>\d+)/read/$', Read_view.NullHypothesisRead, name='NullHypothesisRead'),

    url(r'^ResearchHypothesis/(?P<pk>\d+)/read/$', Read_view.ResearchHypothesisRead, name='ResearchHypothesisRead'),

    url(r'^Article/(?P<pk>\d+)/read/$', Read_view.ArticleRead, name='ArticleRead'),

    url(r'^Text/(?P<pk>\d+)/read/$', Read_view.TextRead, name='TextRead'),

    url(r'^ArtificialLanguage/(?P<pk>\d+)/read/$', Read_view.ArtificialLanguageRead, name='ArtificialLanguageRead'),

    url(r'^Language/(?P<pk>\d+)/read/$', Read_view.LanguageRead, name='LanguageRead'),

    url(r'^HumanLanguage/(?P<pk>\d+)/read/$', Read_view.HumanLanguageRead, name='HumanLanguageRead'),

    url(r'^Sentence/(?P<pk>\d+)/read/$', Read_view.SentenceRead, name='SentenceRead'),

    url(r'^AtomicAction/(?P<pk>\d+)/read/$', Read_view.AtomicActionRead, name='AtomicActionRead'),

    url(r'^ComplexAction/(?P<pk>\d+)/read/$', Read_view.ComplexActionRead, name='ComplexActionRead'),

    url(r'^AuthorProtocol/(?P<pk>\d+)/read/$', Read_view.AuthorProtocolRead, name='AuthorProtocolRead'),

    url(r'^Book/(?P<pk>\d+)/read/$', Read_view.BookRead, name='BookRead'),

    url(r'^CalculableVariable/(?P<pk>\d+)/read/$', Read_view.CalculableVariableRead, name='CalculableVariableRead'),

    url(r'^CapacityRequirements/(?P<pk>\d+)/read/$', Read_view.CapacityRequirementsRead, name='CapacityRequirementsRead'),

    url(r'^EnvironmentalRequirements/(?P<pk>\d+)/read/$', Read_view.EnvironmentalRequirementsRead, name='EnvironmentalRequirementsRead'),

    url(r'^FinancialRequirements/(?P<pk>\d+)/read/$', Read_view.FinancialRequirementsRead, name='FinancialRequirementsRead'),

    url(r'^ClassificationByDomain/(?P<pk>\d+)/read/$', Read_view.ClassificationByDomainRead, name='ClassificationByDomainRead'),

    url(r'^Classifying/(?P<pk>\d+)/read/$', Read_view.ClassifyingRead, name='ClassifyingRead'),

    url(r'^DesigningExperiment/(?P<pk>\d+)/read/$', Read_view.DesigningExperimentRead, name='DesigningExperimentRead'),

    url(r'^CoarseError/(?P<pk>\d+)/read/$', Read_view.CoarseErrorRead, name='CoarseErrorRead'),

    url(r'^MeasurementError/(?P<pk>\d+)/read/$', Read_view.MeasurementErrorRead, name='MeasurementErrorRead'),

    url(r'^ComparisonControl_TargetGroups/(?P<pk>\d+)/read/$', Read_view.ComparisonControl_TargetGroupsRead, name='ComparisonControl_TargetGroupsRead'),

    url(r'^PairedComparison/(?P<pk>\d+)/read/$', Read_view.PairedComparisonRead, name='PairedComparisonRead'),

    url(r'^PairedComparisonOfMatchingGroups/(?P<pk>\d+)/read/$', Read_view.PairedComparisonOfMatchingGroupsRead, name='PairedComparisonOfMatchingGroupsRead'),

    url(r'^PairedComparisonOfSingleSampleGroups/(?P<pk>\d+)/read/$', Read_view.PairedComparisonOfSingleSampleGroupsRead, name='PairedComparisonOfSingleSampleGroupsRead'),

    url(r'^QualityControl/(?P<pk>\d+)/read/$', Read_view.QualityControlRead, name='QualityControlRead'),

    url(r'^ComplexHypothesis/(?P<pk>\d+)/read/$', Read_view.ComplexHypothesisRead, name='ComplexHypothesisRead'),

    url(r'^SingleHypothesis/(?P<pk>\d+)/read/$', Read_view.SingleHypothesisRead, name='SingleHypothesisRead'),

    url(r'^ComputationalExperiment/(?P<pk>\d+)/read/$', Read_view.ComputationalExperimentRead, name='ComputationalExperimentRead'),

    url(r'^ComputeGoal/(?P<pk>\d+)/read/$', Read_view.ComputeGoalRead, name='ComputeGoalRead'),

    url(r'^ConfirmGoal/(?P<pk>\d+)/read/$', Read_view.ConfirmGoalRead, name='ConfirmGoalRead'),

    url(r'^ExplainGoal/(?P<pk>\d+)/read/$', Read_view.ExplainGoalRead, name='ExplainGoalRead'),

    url(r'^InvestigateGoal/(?P<pk>\d+)/read/$', Read_view.InvestigateGoalRead, name='InvestigateGoalRead'),

    url(r'^ComputerSimulation/(?P<pk>\d+)/read/$', Read_view.ComputerSimulationRead, name='ComputerSimulationRead'),

    url(r'^ConstantQuantity/(?P<pk>\d+)/read/$', Read_view.ConstantQuantityRead, name='ConstantQuantityRead'),

    url(r'^Controllability/(?P<pk>\d+)/read/$', Read_view.ControllabilityRead, name='ControllabilityRead'),

    url(r'^Independence/(?P<pk>\d+)/read/$', Read_view.IndependenceRead, name='IndependenceRead'),

    url(r'^DBReference/(?P<pk>\d+)/read/$', Read_view.DBReferenceRead, name='DBReferenceRead'),

    url(r'^DDC_Dewey_Classification/(?P<pk>\d+)/read/$', Read_view.DDC_Dewey_ClassificationRead, name='DDC_Dewey_ClassificationRead'),

    url(r'^LibraryOfCongressClassification/(?P<pk>\d+)/read/$', Read_view.LibraryOfCongressClassificationRead, name='LibraryOfCongressClassificationRead'),

    url(r'^NLMClassification/(?P<pk>\d+)/read/$', Read_view.NLMClassificationRead, name='NLMClassificationRead'),

    url(r'^ResearchCouncilsUKClassification/(?P<pk>\d+)/read/$', Read_view.ResearchCouncilsUKClassificationRead, name='ResearchCouncilsUKClassificationRead'),

    url(r'^DataRepresentationStandard/(?P<pk>\d+)/read/$', Read_view.DataRepresentationStandardRead, name='DataRepresentationStandardRead'),

    url(r'^DegreeOfModel/(?P<pk>\d+)/read/$', Read_view.DegreeOfModelRead, name='DegreeOfModelRead'),

    url(r'^DynamismOfModel/(?P<pk>\d+)/read/$', Read_view.DynamismOfModelRead, name='DynamismOfModelRead'),

    url(r'^DependentVariable/(?P<pk>\d+)/read/$', Read_view.DependentVariableRead, name='DependentVariableRead'),

    url(r'^Device/(?P<pk>\d+)/read/$', Read_view.DeviceRead, name='DeviceRead'),

    url(r'^DomainAction/(?P<pk>\d+)/read/$', Read_view.DomainActionRead, name='DomainActionRead'),

    url(r'^DoseResponse/(?P<pk>\d+)/read/$', Read_view.DoseResponseRead, name='DoseResponseRead'),

    url(r'^GeneKnockin/(?P<pk>\d+)/read/$', Read_view.GeneKnockinRead, name='GeneKnockinRead'),

    url(r'^GeneKnockout/(?P<pk>\d+)/read/$', Read_view.GeneKnockoutRead, name='GeneKnockoutRead'),

    url(r'^Normal_Disease/(?P<pk>\d+)/read/$', Read_view.Normal_DiseaseRead, name='Normal_DiseaseRead'),

    url(r'^TimeCourse/(?P<pk>\d+)/read/$', Read_view.TimeCourseRead, name='TimeCourseRead'),

    url(r'^Treated_Untreated/(?P<pk>\d+)/read/$', Read_view.Treated_UntreatedRead, name='Treated_UntreatedRead'),

    url(r'^DraftStatus/(?P<pk>\d+)/read/$', Read_view.DraftStatusRead, name='DraftStatusRead'),

    url(r'^DuhemEffect/(?P<pk>\d+)/read/$', Read_view.DuhemEffectRead, name='DuhemEffectRead'),

    url(r'^ExperimentalDesignEffect/(?P<pk>\d+)/read/$', Read_view.ExperimentalDesignEffectRead, name='ExperimentalDesignEffectRead'),

    url(r'^InstrumentationEffect/(?P<pk>\d+)/read/$', Read_view.InstrumentationEffectRead, name='InstrumentationEffectRead'),

    url(r'^ObjectEffect/(?P<pk>\d+)/read/$', Read_view.ObjectEffectRead, name='ObjectEffectRead'),

    url(r'^SubjectEffect/(?P<pk>\d+)/read/$', Read_view.SubjectEffectRead, name='SubjectEffectRead'),

    url(r'^TimeEffect/(?P<pk>\d+)/read/$', Read_view.TimeEffectRead, name='TimeEffectRead'),

    url(r'^DynamicModel/(?P<pk>\d+)/read/$', Read_view.DynamicModelRead, name='DynamicModelRead'),

    url(r'^StaticModel/(?P<pk>\d+)/read/$', Read_view.StaticModelRead, name='StaticModelRead'),

    url(r'^ErrorOfConclusion/(?P<pk>\d+)/read/$', Read_view.ErrorOfConclusionRead, name='ErrorOfConclusionRead'),

    url(r'^ExperimentalActionsPlanning/(?P<pk>\d+)/read/$', Read_view.ExperimentalActionsPlanningRead, name='ExperimentalActionsPlanningRead'),

    url(r'^ExperimentalEquipmentSelecting/(?P<pk>\d+)/read/$', Read_view.ExperimentalEquipmentSelectingRead, name='ExperimentalEquipmentSelectingRead'),

    url(r'^ExperimentalModelDesign/(?P<pk>\d+)/read/$', Read_view.ExperimentalModelDesignRead, name='ExperimentalModelDesignRead'),

    url(r'^ExperimentalObjectSelecting/(?P<pk>\d+)/read/$', Read_view.ExperimentalObjectSelectingRead, name='ExperimentalObjectSelectingRead'),

    url(r'^ExperimentalConclusion/(?P<pk>\d+)/read/$', Read_view.ExperimentalConclusionRead, name='ExperimentalConclusionRead'),

    url(r'^ExperimentalProtocol/(?P<pk>\d+)/read/$', Read_view.ExperimentalProtocolRead, name='ExperimentalProtocolRead'),

    url(r'^ExperimenteeBias/(?P<pk>\d+)/read/$', Read_view.ExperimenteeBiasRead, name='ExperimenteeBiasRead'),

    url(r'^HawthorneEffect/(?P<pk>\d+)/read/$', Read_view.HawthorneEffectRead, name='HawthorneEffectRead'),

    url(r'^MortalityEffect/(?P<pk>\d+)/read/$', Read_view.MortalityEffectRead, name='MortalityEffectRead'),

    url(r'^ExperimenterBias/(?P<pk>\d+)/read/$', Read_view.ExperimenterBiasRead, name='ExperimenterBiasRead'),

    url(r'^TestingEffect/(?P<pk>\d+)/read/$', Read_view.TestingEffectRead, name='TestingEffectRead'),

    url(r'^ExplicitHypothesis/(?P<pk>\d+)/read/$', Read_view.ExplicitHypothesisRead, name='ExplicitHypothesisRead'),

    url(r'^ImplicitHypothesis/(?P<pk>\d+)/read/$', Read_view.ImplicitHypothesisRead, name='ImplicitHypothesisRead'),

    url(r'^FactorLevel/(?P<pk>\d+)/read/$', Read_view.FactorLevelRead, name='FactorLevelRead'),

    url(r'^FactorialDesign/(?P<pk>\d+)/read/$', Read_view.FactorialDesignRead, name='FactorialDesignRead'),

    url(r'^FalseNegative/(?P<pk>\d+)/read/$', Read_view.FalseNegativeRead, name='FalseNegativeRead'),

    url(r'^HypothesisAcceptanceMistake/(?P<pk>\d+)/read/$', Read_view.HypothesisAcceptanceMistakeRead, name='HypothesisAcceptanceMistakeRead'),

    url(r'^FalsePpositive/(?P<pk>\d+)/read/$', Read_view.FalsePpositiveRead, name='FalsePpositiveRead'),

    url(r'^FaultyComparison/(?P<pk>\d+)/read/$', Read_view.FaultyComparisonRead, name='FaultyComparisonRead'),

    url(r'^Formula/(?P<pk>\d+)/read/$', Read_view.FormulaRead, name='FormulaRead'),

    url(r'^GalileanExperiment/(?P<pk>\d+)/read/$', Read_view.GalileanExperimentRead, name='GalileanExperimentRead'),

    url(r'^HandTool/(?P<pk>\d+)/read/$', Read_view.HandToolRead, name='HandToolRead'),

    url(r'^Tool/(?P<pk>\d+)/read/$', Read_view.ToolRead, name='ToolRead'),

    url(r'^Hardware/(?P<pk>\d+)/read/$', Read_view.HardwareRead, name='HardwareRead'),

    url(r'^HistoryEffect/(?P<pk>\d+)/read/$', Read_view.HistoryEffectRead, name='HistoryEffectRead'),

    url(r'^MaturationEffect/(?P<pk>\d+)/read/$', Read_view.MaturationEffectRead, name='MaturationEffectRead'),

    url(r'^HypothesisdrivenExperiment/(?P<pk>\d+)/read/$', Read_view.HypothesisdrivenExperimentRead, name='HypothesisdrivenExperimentRead'),

    url(r'^HypothesisformingExperiment/(?P<pk>\d+)/read/$', Read_view.HypothesisformingExperimentRead, name='HypothesisformingExperimentRead'),

    url(r'^Image/(?P<pk>\d+)/read/$', Read_view.ImageRead, name='ImageRead'),

    url(r'^ImproperSampling/(?P<pk>\d+)/read/$', Read_view.ImproperSamplingRead, name='ImproperSamplingRead'),

    url(r'^IncompleteDataError/(?P<pk>\d+)/read/$', Read_view.IncompleteDataErrorRead, name='IncompleteDataErrorRead'),

    url(r'^IndependentVariable/(?P<pk>\d+)/read/$', Read_view.IndependentVariableRead, name='IndependentVariableRead'),

    url(r'^InferableVariable/(?P<pk>\d+)/read/$', Read_view.InferableVariableRead, name='InferableVariableRead'),

    url(r'^InternalStatus/(?P<pk>\d+)/read/$', Read_view.InternalStatusRead, name='InternalStatusRead'),

    url(r'^RestrictedStatus/(?P<pk>\d+)/read/$', Read_view.RestrictedStatusRead, name='RestrictedStatusRead'),

    url(r'^Journal/(?P<pk>\d+)/read/$', Read_view.JournalRead, name='JournalRead'),

    url(r'^LinearModel/(?P<pk>\d+)/read/$', Read_view.LinearModelRead, name='LinearModelRead'),

    url(r'^NonlinearModel/(?P<pk>\d+)/read/$', Read_view.NonlinearModelRead, name='NonlinearModelRead'),

    url(r'^LogicalModelRepresentation/(?P<pk>\d+)/read/$', Read_view.LogicalModelRepresentationRead, name='LogicalModelRepresentationRead'),

    url(r'^Machine/(?P<pk>\d+)/read/$', Read_view.MachineRead, name='MachineRead'),

    url(r'^MachineTool/(?P<pk>\d+)/read/$', Read_view.MachineToolRead, name='MachineToolRead'),

    url(r'^Magazine/(?P<pk>\d+)/read/$', Read_view.MagazineRead, name='MagazineRead'),

    url(r'^Materials/(?P<pk>\d+)/read/$', Read_view.MaterialsRead, name='MaterialsRead'),

    url(r'^MathematicalModelRepresentation/(?P<pk>\d+)/read/$', Read_view.MathematicalModelRepresentationRead, name='MathematicalModelRepresentationRead'),

    url(r'^MetabolomicExperiment/(?P<pk>\d+)/read/$', Read_view.MetabolomicExperimentRead, name='MetabolomicExperimentRead'),

    url(r'^MethodsComparison/(?P<pk>\d+)/read/$', Read_view.MethodsComparisonRead, name='MethodsComparisonRead'),

    url(r'^SubjectsComparison/(?P<pk>\d+)/read/$', Read_view.SubjectsComparisonRead, name='SubjectsComparisonRead'),

    url(r'^MicroarrayExperiment/(?P<pk>\d+)/read/$', Read_view.MicroarrayExperimentRead, name='MicroarrayExperimentRead'),

    url(r'^MultifactorExperiment/(?P<pk>\d+)/read/$', Read_view.MultifactorExperimentRead, name='MultifactorExperimentRead'),

    url(r'^OnefactorExperiment/(?P<pk>\d+)/read/$', Read_view.OnefactorExperimentRead, name='OnefactorExperimentRead'),

    url(r'^TwofactorExperiment/(?P<pk>\d+)/read/$', Read_view.TwofactorExperimentRead, name='TwofactorExperimentRead'),

    url(r'^NaturalLanguage/(?P<pk>\d+)/read/$', Read_view.NaturalLanguageRead, name='NaturalLanguageRead'),

    url(r'^NonRestrictedStatus/(?P<pk>\d+)/read/$', Read_view.NonRestrictedStatusRead, name='NonRestrictedStatusRead'),

    url(r'^NormalizationStrategy/(?P<pk>\d+)/read/$', Read_view.NormalizationStrategyRead, name='NormalizationStrategyRead'),

    url(r'^ObjectMethod/(?P<pk>\d+)/read/$', Read_view.ObjectMethodRead, name='ObjectMethodRead'),

    url(r'^ObjectOfAction/(?P<pk>\d+)/read/$', Read_view.ObjectOfActionRead, name='ObjectOfActionRead'),

    url(r'^ObservableVariable/(?P<pk>\d+)/read/$', Read_view.ObservableVariableRead, name='ObservableVariableRead'),

    url(r'^Paper/(?P<pk>\d+)/read/$', Read_view.PaperRead, name='PaperRead'),

    url(r'^ParticlePhysicsExperiment/(?P<pk>\d+)/read/$', Read_view.ParticlePhysicsExperimentRead, name='ParticlePhysicsExperimentRead'),

    url(r'^PresentingSampling/(?P<pk>\d+)/read/$', Read_view.PresentingSamplingRead, name='PresentingSamplingRead'),

    url(r'^Proceedings/(?P<pk>\d+)/read/$', Read_view.ProceedingsRead, name='ProceedingsRead'),

    url(r'^Program/(?P<pk>\d+)/read/$', Read_view.ProgramRead, name='ProgramRead'),

    url(r'^ProteomicExperiment/(?P<pk>\d+)/read/$', Read_view.ProteomicExperimentRead, name='ProteomicExperimentRead'),

    url(r'^Provider/(?P<pk>\d+)/read/$', Read_view.ProviderRead, name='ProviderRead'),

    url(r'^PublicAcademicStatus/(?P<pk>\d+)/read/$', Read_view.PublicAcademicStatusRead, name='PublicAcademicStatusRead'),

    url(r'^PublicStatus/(?P<pk>\d+)/read/$', Read_view.PublicStatusRead, name='PublicStatusRead'),

    url(r'^RandomError/(?P<pk>\d+)/read/$', Read_view.RandomErrorRead, name='RandomErrorRead'),

    url(r'^RandomSampling/(?P<pk>\d+)/read/$', Read_view.RandomSamplingRead, name='RandomSamplingRead'),

    url(r'^RecommendationSatus/(?P<pk>\d+)/read/$', Read_view.RecommendationSatusRead, name='RecommendationSatusRead'),

    url(r'^RecordingAction/(?P<pk>\d+)/read/$', Read_view.RecordingActionRead, name='RecordingActionRead'),

    url(r'^Region/(?P<pk>\d+)/read/$', Read_view.RegionRead, name='RegionRead'),

    url(r'^RepresenationalMedium/(?P<pk>\d+)/read/$', Read_view.RepresenationalMediumRead, name='RepresenationalMediumRead'),

    url(r'^SUMOSynonym/(?P<pk>\d+)/read/$', Read_view.SUMOSynonymRead, name='SUMOSynonymRead'),

    url(r'^SampleForming/(?P<pk>\d+)/read/$', Read_view.SampleFormingRead, name='SampleFormingRead'),

    url(r'^SequentialDesign/(?P<pk>\d+)/read/$', Read_view.SequentialDesignRead, name='SequentialDesignRead'),

    url(r'^Software/(?P<pk>\d+)/read/$', Read_view.SoftwareRead, name='SoftwareRead'),

    url(r'^Sound/(?P<pk>\d+)/read/$', Read_view.SoundRead, name='SoundRead'),

    url(r'^SplitSamples/(?P<pk>\d+)/read/$', Read_view.SplitSamplesRead, name='SplitSamplesRead'),

    url(r'^SymmetricalMatching/(?P<pk>\d+)/read/$', Read_view.SymmetricalMatchingRead, name='SymmetricalMatchingRead'),

    url(r'^StatisticalError/(?P<pk>\d+)/read/$', Read_view.StatisticalErrorRead, name='StatisticalErrorRead'),

    url(r'^StratifiedSampling/(?P<pk>\d+)/read/$', Read_view.StratifiedSamplingRead, name='StratifiedSamplingRead'),

    url(r'^SubjectOfAction/(?P<pk>\d+)/read/$', Read_view.SubjectOfActionRead, name='SubjectOfActionRead'),

    url(r'^SubjectOfExperiment/(?P<pk>\d+)/read/$', Read_view.SubjectOfExperimentRead, name='SubjectOfExperimentRead'),

    url(r'^Submitter/(?P<pk>\d+)/read/$', Read_view.SubmitterRead, name='SubmitterRead'),

    url(r'^Summary/(?P<pk>\d+)/read/$', Read_view.SummaryRead, name='SummaryRead'),

    url(r'^SystematicError/(?P<pk>\d+)/read/$', Read_view.SystematicErrorRead, name='SystematicErrorRead'),

    url(r'^TechnicalReport/(?P<pk>\d+)/read/$', Read_view.TechnicalReportRead, name='TechnicalReportRead'),

    url(r'^TimeDuration/(?P<pk>\d+)/read/$', Read_view.TimeDurationRead, name='TimeDurationRead'),

    url(r'^TimeInterval/(?P<pk>\d+)/read/$', Read_view.TimeIntervalRead, name='TimeIntervalRead'),

    url(r'^URL/(?P<pk>\d+)/read/$', Read_view.URLRead, name='URLRead'),


    url(r'^Thing/list/$', List_view.ThingList, name='ThingList'),

    url(r'^RepresentationForm/list/$', List_view.RepresentationFormList, name='RepresentationFormList'),

    url(r'^Artifact/list/$', List_view.ArtifactList, name='ArtifactList'),

    url(r'^SynonymousExternalConcept/list/$', List_view.SynonymousExternalConceptList, name='SynonymousExternalConceptList'),

    url(r'^Name/list/$', List_view.NameList, name='NameList'),

    url(r'^ScientificExperiment/list/$', List_view.ScientificExperimentList, name='ScientificExperimentList'),

    url(r'^ExperimentalDesign/list/$', List_view.ExperimentalDesignList, name='ExperimentalDesignList'),

    url(r'^AdminInfoExperiment/list/$', List_view.AdminInfoExperimentList, name='AdminInfoExperimentList'),

    url(r'^IDExperiment/list/$', List_view.IDExperimentList, name='IDExperimentList'),

    url(r'^DomainOfExperiment/list/$', List_view.DomainOfExperimentList, name='DomainOfExperimentList'),

    url(r'^ExperimentalObservation/list/$', List_view.ExperimentalObservationList, name='ExperimentalObservationList'),

    url(r'^ResultsInterpretation/list/$', List_view.ResultsInterpretationList, name='ResultsInterpretationList'),

    url(r'^ExperimentalAction/list/$', List_view.ExperimentalActionList, name='ExperimentalActionList'),

    url(r'^ActionName/list/$', List_view.ActionNameList, name='ActionNameList'),

    url(r'^ActionComplexity/list/$', List_view.ActionComplexityList, name='ActionComplexityList'),

    url(r'^Human/list/$', List_view.HumanList, name='HumanList'),

    url(r'^Organization/list/$', List_view.OrganizationList, name='OrganizationList'),

    url(r'^AdminInfoAuthor/list/$', List_view.AdminInfoAuthorList, name='AdminInfoAuthorList'),

    url(r'^AdminInfoObjectOfExperiment/list/$', List_view.AdminInfoObjectOfExperimentList, name='AdminInfoObjectOfExperimentList'),

    url(r'^AdminInfoProvider/list/$', List_view.AdminInfoProviderList, name='AdminInfoProviderList'),

    url(r'^AdminInfoSubjectOfExperiment/list/$', List_view.AdminInfoSubjectOfExperimentList, name='AdminInfoSubjectOfExperimentList'),

    url(r'^AdminInfoSubmitter/list/$', List_view.AdminInfoSubmitterList, name='AdminInfoSubmitterList'),

    url(r'^AdminInfoUser/list/$', List_view.AdminInfoUserList, name='AdminInfoUserList'),

    url(r'^ExperimentalMethod/list/$', List_view.ExperimentalMethodList, name='ExperimentalMethodList'),

    url(r'^MethodApplicabilityCondition/list/$', List_view.MethodApplicabilityConditionList, name='MethodApplicabilityConditionList'),

    url(r'^StandardOperatingProcedure/list/$', List_view.StandardOperatingProcedureList, name='StandardOperatingProcedureList'),

    url(r'^Author/list/$', List_view.AuthorList, name='AuthorList'),

    url(r'^ClassificationOfDomains/list/$', List_view.ClassificationOfDomainsList, name='ClassificationOfDomainsList'),

    url(r'^ClassificationOfExperiments/list/$', List_view.ClassificationOfExperimentsList, name='ClassificationOfExperimentsList'),

    url(r'^ExperimentalHypothesis/list/$', List_view.ExperimentalHypothesisList, name='ExperimentalHypothesisList'),

    url(r'^HypothesisComplexity/list/$', List_view.HypothesisComplexityList, name='HypothesisComplexityList'),

    url(r'^Representation/list/$', List_view.RepresentationList, name='RepresentationList'),

    url(r'^Proposition/list/$', List_view.PropositionList, name='PropositionList'),

    url(r'^ExperimentalEquipment/list/$', List_view.ExperimentalEquipmentList, name='ExperimentalEquipmentList'),

    url(r'^TechnicalDescription/list/$', List_view.TechnicalDescriptionList, name='TechnicalDescriptionList'),

    url(r'^ExperimentalResults/list/$', List_view.ExperimentalResultsList, name='ExperimentalResultsList'),

    url(r'^ObservationalError/list/$', List_view.ObservationalErrorList, name='ObservationalErrorList'),

    url(r'^ResultError/list/$', List_view.ResultErrorList, name='ResultErrorList'),

    url(r'^ResultsEvaluation/list/$', List_view.ResultsEvaluationList, name='ResultsEvaluationList'),

    url(r'^ScientificInvestigation/list/$', List_view.ScientificInvestigationList, name='ScientificInvestigationList'),

    url(r'^ExperimentalModel/list/$', List_view.ExperimentalModelList, name='ExperimentalModelList'),

    url(r'^ExperimentalFactor/list/$', List_view.ExperimentalFactorList, name='ExperimentalFactorList'),

    url(r'^ExperimentalTechnology/list/$', List_view.ExperimentalTechnologyList, name='ExperimentalTechnologyList'),

    url(r'^ExperimentalRequirements/list/$', List_view.ExperimentalRequirementsList, name='ExperimentalRequirementsList'),

    url(r'^HypothesisExplicitness/list/$', List_view.HypothesisExplicitnessList, name='HypothesisExplicitnessList'),

    url(r'^LinguisticExpression/list/$', List_view.LinguisticExpressionList, name='LinguisticExpressionList'),

    url(r'^FactRejectResearchHypothesis/list/$', List_view.FactRejectResearchHypothesisList, name='FactRejectResearchHypothesisList'),

    url(r'^FactSupportResearchHypothesis/list/$', List_view.FactSupportResearchHypothesisList, name='FactSupportResearchHypothesisList'),

    url(r'^Number/list/$', List_view.NumberList, name='NumberList'),

    url(r'^GeneralityOfHypothesis/list/$', List_view.GeneralityOfHypothesisList, name='GeneralityOfHypothesisList'),

    url(r'^ActionGoal/list/$', List_view.ActionGoalList, name='ActionGoalList'),

    url(r'^ExperimentalGoal/list/$', List_view.ExperimentalGoalList, name='ExperimentalGoalList'),

    url(r'^MethodGoal/list/$', List_view.MethodGoalList, name='MethodGoalList'),

    url(r'^HypothesisFormation/list/$', List_view.HypothesisFormationList, name='HypothesisFormationList'),

    url(r'^HypothesisRepresentation/list/$', List_view.HypothesisRepresentationList, name='HypothesisRepresentationList'),

    url(r'^InformationGethering/list/$', List_view.InformationGetheringList, name='InformationGetheringList'),

    url(r'^Sample/list/$', List_view.SampleList, name='SampleList'),

    url(r'^Variability/list/$', List_view.VariabilityList, name='VariabilityList'),

    url(r'^TimePoint/list/$', List_view.TimePointList, name='TimePointList'),

    url(r'^User/list/$', List_view.UserList, name='UserList'),

    url(r'^LoginName/list/$', List_view.LoginNameList, name='LoginNameList'),

    url(r'^DomainModel/list/$', List_view.DomainModelList, name='DomainModelList'),

    url(r'^GroupExperimentalObject/list/$', List_view.GroupExperimentalObjectList, name='GroupExperimentalObjectList'),

    url(r'^ObjectOfExperiment/list/$', List_view.ObjectOfExperimentList, name='ObjectOfExperimentList'),

    url(r'^ClassificationByModel/list/$', List_view.ClassificationByModelList, name='ClassificationByModelList'),

    url(r'^StatisticsCharacteristic/list/$', List_view.StatisticsCharacteristicList, name='StatisticsCharacteristicList'),

    url(r'^ParentGroup/list/$', List_view.ParentGroupList, name='ParentGroupList'),

    url(r'^Password/list/$', List_view.PasswordList, name='PasswordList'),

    url(r'^ProcedureExecuteExperiment/list/$', List_view.ProcedureExecuteExperimentList, name='ProcedureExecuteExperimentList'),

    url(r'^PlanOfExperimentalActions/list/$', List_view.PlanOfExperimentalActionsList, name='PlanOfExperimentalActionsList'),

    url(r'^PoblemAnalysis/list/$', List_view.PoblemAnalysisList, name='PoblemAnalysisList'),

    url(r'^SubjectQualification/list/$', List_view.SubjectQualificationList, name='SubjectQualificationList'),

    url(r'^BiblioReference/list/$', List_view.BiblioReferenceList, name='BiblioReferenceList'),

    url(r'^Model/list/$', List_view.ModelList, name='ModelList'),

    url(r'^ModelRepresentation/list/$', List_view.ModelRepresentationList, name='ModelRepresentationList'),

    url(r'^RepresentationExperimentalExecutionProcedure/list/$', List_view.RepresentationExperimentalExecutionProcedureList, name='RepresentationExperimentalExecutionProcedureList'),

    url(r'^RepresentationExperimentalGoal/list/$', List_view.RepresentationExperimentalGoalList, name='RepresentationExperimentalGoalList'),

    url(r'^SampleRepresentativeness/list/$', List_view.SampleRepresentativenessList, name='SampleRepresentativenessList'),

    url(r'^SampleSize/list/$', List_view.SampleSizeList, name='SampleSizeList'),

    url(r'^SamplingMethod/list/$', List_view.SamplingMethodList, name='SamplingMethodList'),

    url(r'^SystematicSampling/list/$', List_view.SystematicSamplingList, name='SystematicSamplingList'),

    url(r'^SamplingRule/list/$', List_view.SamplingRuleList, name='SamplingRuleList'),

    url(r'^ExperimentalStandard/list/$', List_view.ExperimentalStandardList, name='ExperimentalStandardList'),

    url(r'^Error/list/$', List_view.ErrorList, name='ErrorList'),

    url(r'^Dispersion/list/$', List_view.DispersionList, name='DispersionList'),

    url(r'^LevelOfSignificance/list/$', List_view.LevelOfSignificanceList, name='LevelOfSignificanceList'),

    url(r'^StatusExperimentalDocument/list/$', List_view.StatusExperimentalDocumentList, name='StatusExperimentalDocumentList'),

    url(r'^ExperimentalDesignStrategy/list/$', List_view.ExperimentalDesignStrategyList, name='ExperimentalDesignStrategyList'),

    url(r'^ExperimentalSubgoal/list/$', List_view.ExperimentalSubgoalList, name='ExperimentalSubgoalList'),

    url(r'^SubjectMethod/list/$', List_view.SubjectMethodList, name='SubjectMethodList'),

    url(r'^BaconianExperiment/list/$', List_view.BaconianExperimentList, name='BaconianExperimentList'),

    url(r'^TargetVariable/list/$', List_view.TargetVariableList, name='TargetVariableList'),

    url(r'^Title/list/$', List_view.TitleList, name='TitleList'),

    url(r'^ValueEstimate/list/$', List_view.ValueEstimateList, name='ValueEstimateList'),

    url(r'^ValueOfVariable/list/$', List_view.ValueOfVariableList, name='ValueOfVariableList'),

    url(r'^Role/list/$', List_view.RoleList, name='RoleList'),

    url(r'^CorpuscularObject/list/$', List_view.CorpuscularObjectList, name='CorpuscularObjectList'),

    url(r'^AttributeRole/list/$', List_view.AttributeRoleList, name='AttributeRoleList'),

    url(r'^Attribute/list/$', List_view.AttributeList, name='AttributeList'),

    url(r'^Procedure/list/$', List_view.ProcedureList, name='ProcedureList'),

    url(r'^AdministrativeInformation/list/$', List_view.AdministrativeInformationList, name='AdministrativeInformationList'),

    url(r'^TitleExperiment/list/$', List_view.TitleExperimentList, name='TitleExperimentList'),

    url(r'^NameEquipment/list/$', List_view.NameEquipmentList, name='NameEquipmentList'),

    url(r'^PersonalName/list/$', List_view.PersonalNameList, name='PersonalNameList'),

    url(r'^FieldOfStudy/list/$', List_view.FieldOfStudyList, name='FieldOfStudyList'),

    url(r'^RelatedDomain/list/$', List_view.RelatedDomainList, name='RelatedDomainList'),

    url(r'^ProductRole/list/$', List_view.ProductRoleList, name='ProductRoleList'),

    url(r'^ScientificTask/list/$', List_view.ScientificTaskList, name='ScientificTaskList'),

    url(r'^ExecutionOfExperiment/list/$', List_view.ExecutionOfExperimentList, name='ExecutionOfExperimentList'),

    url(r'^CorporateName/list/$', List_view.CorporateNameList, name='CorporateNameList'),

    url(r'^AttributeOfAction/list/$', List_view.AttributeOfActionList, name='AttributeOfActionList'),

    url(r'^SentientAgent/list/$', List_view.SentientAgentList, name='SentientAgentList'),

    url(r'^Robot/list/$', List_view.RobotList, name='RobotList'),

    url(r'^Group/list/$', List_view.GroupList, name='GroupList'),

    url(r'^Entity/list/$', List_view.EntityList, name='EntityList'),

    url(r'^ResearchMethod/list/$', List_view.ResearchMethodList, name='ResearchMethodList'),

    url(r'^Requirements/list/$', List_view.RequirementsList, name='RequirementsList'),

    url(r'^AuthorSOP/list/$', List_view.AuthorSOPList, name='AuthorSOPList'),

    url(r'^SubjectRole/list/$', List_view.SubjectRoleList, name='SubjectRoleList'),

    url(r'^Classification/list/$', List_view.ClassificationList, name='ClassificationList'),

    url(r'^Fact/list/$', List_view.FactList, name='FactList'),

    url(r'^ModelAssumption/list/$', List_view.ModelAssumptionList, name='ModelAssumptionList'),

    url(r'^Variable/list/$', List_view.VariableList, name='VariableList'),

    url(r'^AttributeOfHypothesis/list/$', List_view.AttributeOfHypothesisList, name='AttributeOfHypothesisList'),

    url(r'^ContentBearingObject/list/$', List_view.ContentBearingObjectList, name='ContentBearingObjectList'),

    url(r'^Abstract/list/$', List_view.AbstractList, name='AbstractList'),

    url(r'^Quantity/list/$', List_view.QuantityList, name='QuantityList'),

    url(r'^Relation/list/$', List_view.RelationList, name='RelationList'),

    url(r'^ProcessrelatedRole/list/$', List_view.ProcessrelatedRoleList, name='ProcessrelatedRoleList'),

    url(r'^ContentBearingPhysical/list/$', List_view.ContentBearingPhysicalList, name='ContentBearingPhysicalList'),

    url(r'^PhysicalQuantity/list/$', List_view.PhysicalQuantityList, name='PhysicalQuantityList'),

    url(r'^Goal/list/$', List_view.GoalList, name='GoalList'),

    url(r'^RepresentationExperimentalObservation/list/$', List_view.RepresentationExperimentalObservationList, name='RepresentationExperimentalObservationList'),

    url(r'^RepresentationExperimentalResults/list/$', List_view.RepresentationExperimentalResultsList, name='RepresentationExperimentalResultsList'),

    url(r'^AttributeGroup/list/$', List_view.AttributeGroupList, name='AttributeGroupList'),

    url(r'^TimePosition/list/$', List_view.TimePositionList, name='TimePositionList'),

    url(r'^ActorRole/list/$', List_view.ActorRoleList, name='ActorRoleList'),

    url(r'^DocumentStage/list/$', List_view.DocumentStageList, name='DocumentStageList'),

    url(r'^PermissionStatus/list/$', List_view.PermissionStatusList, name='PermissionStatusList'),

    url(r'^Plan/list/$', List_view.PlanList, name='PlanList'),

    url(r'^Reference/list/$', List_view.ReferenceList, name='ReferenceList'),

    url(r'^AttributeSample/list/$', List_view.AttributeSampleList, name='AttributeSampleList'),

    url(r'^ExperimentalRule/list/$', List_view.ExperimentalRuleList, name='ExperimentalRuleList'),

    url(r'^Robustness/list/$', List_view.RobustnessList, name='RobustnessList'),

    url(r'^Validity/list/$', List_view.ValidityList, name='ValidityList'),

    url(r'^AttributeOfDocument/list/$', List_view.AttributeOfDocumentList, name='AttributeOfDocumentList'),

    url(r'^PhysicalExperiment/list/$', List_view.PhysicalExperimentList, name='PhysicalExperimentList'),

    url(r'^ComputationalData/list/$', List_view.ComputationalDataList, name='ComputationalDataList'),

    url(r'^Predicate/list/$', List_view.PredicateList, name='PredicateList'),

    url(r'^SelfConnectedObject/list/$', List_view.SelfConnectedObjectList, name='SelfConnectedObjectList'),

    url(r'^ScientificActivity/list/$', List_view.ScientificActivityList, name='ScientificActivityList'),

    url(r'^FormingClassificationSystem/list/$', List_view.FormingClassificationSystemList, name='FormingClassificationSystemList'),

    url(r'^HypothesisForming/list/$', List_view.HypothesisFormingList, name='HypothesisFormingList'),

    url(r'^InterpretingResult/list/$', List_view.InterpretingResultList, name='InterpretingResultList'),

    url(r'^ProcessProblemAnalysis/list/$', List_view.ProcessProblemAnalysisList, name='ProcessProblemAnalysisList'),

    url(r'^ResultEvaluating/list/$', List_view.ResultEvaluatingList, name='ResultEvaluatingList'),

    url(r'^AttributeOfModel/list/$', List_view.AttributeOfModelList, name='AttributeOfModelList'),

    url(r'^AttributeOfVariable/list/$', List_view.AttributeOfVariableList, name='AttributeOfVariableList'),

    url(r'^Agent/list/$', List_view.AgentList, name='AgentList'),

    url(r'^Collection/list/$', List_view.CollectionList, name='CollectionList'),

    url(r'^EperimentalDesignTask/list/$', List_view.EperimentalDesignTaskList, name='EperimentalDesignTaskList'),

    url(r'^Physical/list/$', List_view.PhysicalList, name='PhysicalList'),

    url(r'^Object/list/$', List_view.ObjectList, name='ObjectList'),

    url(r'^Process/list/$', List_view.ProcessList, name='ProcessList'),

    url(r'^TaskRole/list/$', List_view.TaskRoleList, name='TaskRoleList'),

    url(r'^TimeMeasure/list/$', List_view.TimeMeasureList, name='TimeMeasureList'),

    url(r'^ActionrelatedRole/list/$', List_view.ActionrelatedRoleList, name='ActionrelatedRoleList'),

    url(r'^AbductiveHypothesis/list/$', List_view.AbductiveHypothesisList, name='AbductiveHypothesisList'),

    url(r'^InductiveHypothesis/list/$', List_view.InductiveHypothesisList, name='InductiveHypothesisList'),

    url(r'^Adequacy/list/$', List_view.AdequacyList, name='AdequacyList'),

    url(r'^AlternativeHypothesis/list/$', List_view.AlternativeHypothesisList, name='AlternativeHypothesisList'),

    url(r'^NullHypothesis/list/$', List_view.NullHypothesisList, name='NullHypothesisList'),

    url(r'^ResearchHypothesis/list/$', List_view.ResearchHypothesisList, name='ResearchHypothesisList'),

    url(r'^Article/list/$', List_view.ArticleList, name='ArticleList'),

    url(r'^Text/list/$', List_view.TextList, name='TextList'),

    url(r'^ArtificialLanguage/list/$', List_view.ArtificialLanguageList, name='ArtificialLanguageList'),

    url(r'^Language/list/$', List_view.LanguageList, name='LanguageList'),

    url(r'^HumanLanguage/list/$', List_view.HumanLanguageList, name='HumanLanguageList'),

    url(r'^Sentence/list/$', List_view.SentenceList, name='SentenceList'),

    url(r'^AtomicAction/list/$', List_view.AtomicActionList, name='AtomicActionList'),

    url(r'^ComplexAction/list/$', List_view.ComplexActionList, name='ComplexActionList'),

    url(r'^AuthorProtocol/list/$', List_view.AuthorProtocolList, name='AuthorProtocolList'),

    url(r'^Book/list/$', List_view.BookList, name='BookList'),

    url(r'^CalculableVariable/list/$', List_view.CalculableVariableList, name='CalculableVariableList'),

    url(r'^CapacityRequirements/list/$', List_view.CapacityRequirementsList, name='CapacityRequirementsList'),

    url(r'^EnvironmentalRequirements/list/$', List_view.EnvironmentalRequirementsList, name='EnvironmentalRequirementsList'),

    url(r'^FinancialRequirements/list/$', List_view.FinancialRequirementsList, name='FinancialRequirementsList'),

    url(r'^ClassificationByDomain/list/$', List_view.ClassificationByDomainList, name='ClassificationByDomainList'),

    url(r'^Classifying/list/$', List_view.ClassifyingList, name='ClassifyingList'),

    url(r'^DesigningExperiment/list/$', List_view.DesigningExperimentList, name='DesigningExperimentList'),

    url(r'^CoarseError/list/$', List_view.CoarseErrorList, name='CoarseErrorList'),

    url(r'^MeasurementError/list/$', List_view.MeasurementErrorList, name='MeasurementErrorList'),

    url(r'^ComparisonControl_TargetGroups/list/$', List_view.ComparisonControl_TargetGroupsList, name='ComparisonControl_TargetGroupsList'),

    url(r'^PairedComparison/list/$', List_view.PairedComparisonList, name='PairedComparisonList'),

    url(r'^PairedComparisonOfMatchingGroups/list/$', List_view.PairedComparisonOfMatchingGroupsList, name='PairedComparisonOfMatchingGroupsList'),

    url(r'^PairedComparisonOfSingleSampleGroups/list/$', List_view.PairedComparisonOfSingleSampleGroupsList, name='PairedComparisonOfSingleSampleGroupsList'),

    url(r'^QualityControl/list/$', List_view.QualityControlList, name='QualityControlList'),

    url(r'^ComplexHypothesis/list/$', List_view.ComplexHypothesisList, name='ComplexHypothesisList'),

    url(r'^SingleHypothesis/list/$', List_view.SingleHypothesisList, name='SingleHypothesisList'),

    url(r'^ComputationalExperiment/list/$', List_view.ComputationalExperimentList, name='ComputationalExperimentList'),

    url(r'^ComputeGoal/list/$', List_view.ComputeGoalList, name='ComputeGoalList'),

    url(r'^ConfirmGoal/list/$', List_view.ConfirmGoalList, name='ConfirmGoalList'),

    url(r'^ExplainGoal/list/$', List_view.ExplainGoalList, name='ExplainGoalList'),

    url(r'^InvestigateGoal/list/$', List_view.InvestigateGoalList, name='InvestigateGoalList'),

    url(r'^ComputerSimulation/list/$', List_view.ComputerSimulationList, name='ComputerSimulationList'),

    url(r'^ConstantQuantity/list/$', List_view.ConstantQuantityList, name='ConstantQuantityList'),

    url(r'^Controllability/list/$', List_view.ControllabilityList, name='ControllabilityList'),

    url(r'^Independence/list/$', List_view.IndependenceList, name='IndependenceList'),

    url(r'^DBReference/list/$', List_view.DBReferenceList, name='DBReferenceList'),

    url(r'^DDC_Dewey_Classification/list/$', List_view.DDC_Dewey_ClassificationList, name='DDC_Dewey_ClassificationList'),

    url(r'^LibraryOfCongressClassification/list/$', List_view.LibraryOfCongressClassificationList, name='LibraryOfCongressClassificationList'),

    url(r'^NLMClassification/list/$', List_view.NLMClassificationList, name='NLMClassificationList'),

    url(r'^ResearchCouncilsUKClassification/list/$', List_view.ResearchCouncilsUKClassificationList, name='ResearchCouncilsUKClassificationList'),

    url(r'^DataRepresentationStandard/list/$', List_view.DataRepresentationStandardList, name='DataRepresentationStandardList'),

    url(r'^DegreeOfModel/list/$', List_view.DegreeOfModelList, name='DegreeOfModelList'),

    url(r'^DynamismOfModel/list/$', List_view.DynamismOfModelList, name='DynamismOfModelList'),

    url(r'^DependentVariable/list/$', List_view.DependentVariableList, name='DependentVariableList'),

    url(r'^Device/list/$', List_view.DeviceList, name='DeviceList'),

    url(r'^DomainAction/list/$', List_view.DomainActionList, name='DomainActionList'),

    url(r'^DoseResponse/list/$', List_view.DoseResponseList, name='DoseResponseList'),

    url(r'^GeneKnockin/list/$', List_view.GeneKnockinList, name='GeneKnockinList'),

    url(r'^GeneKnockout/list/$', List_view.GeneKnockoutList, name='GeneKnockoutList'),

    url(r'^Normal_Disease/list/$', List_view.Normal_DiseaseList, name='Normal_DiseaseList'),

    url(r'^TimeCourse/list/$', List_view.TimeCourseList, name='TimeCourseList'),

    url(r'^Treated_Untreated/list/$', List_view.Treated_UntreatedList, name='Treated_UntreatedList'),

    url(r'^DraftStatus/list/$', List_view.DraftStatusList, name='DraftStatusList'),

    url(r'^DuhemEffect/list/$', List_view.DuhemEffectList, name='DuhemEffectList'),

    url(r'^ExperimentalDesignEffect/list/$', List_view.ExperimentalDesignEffectList, name='ExperimentalDesignEffectList'),

    url(r'^InstrumentationEffect/list/$', List_view.InstrumentationEffectList, name='InstrumentationEffectList'),

    url(r'^ObjectEffect/list/$', List_view.ObjectEffectList, name='ObjectEffectList'),

    url(r'^SubjectEffect/list/$', List_view.SubjectEffectList, name='SubjectEffectList'),

    url(r'^TimeEffect/list/$', List_view.TimeEffectList, name='TimeEffectList'),

    url(r'^DynamicModel/list/$', List_view.DynamicModelList, name='DynamicModelList'),

    url(r'^StaticModel/list/$', List_view.StaticModelList, name='StaticModelList'),

    url(r'^ErrorOfConclusion/list/$', List_view.ErrorOfConclusionList, name='ErrorOfConclusionList'),

    url(r'^ExperimentalActionsPlanning/list/$', List_view.ExperimentalActionsPlanningList, name='ExperimentalActionsPlanningList'),

    url(r'^ExperimentalEquipmentSelecting/list/$', List_view.ExperimentalEquipmentSelectingList, name='ExperimentalEquipmentSelectingList'),

    url(r'^ExperimentalModelDesign/list/$', List_view.ExperimentalModelDesignList, name='ExperimentalModelDesignList'),

    url(r'^ExperimentalObjectSelecting/list/$', List_view.ExperimentalObjectSelectingList, name='ExperimentalObjectSelectingList'),

    url(r'^ExperimentalConclusion/list/$', List_view.ExperimentalConclusionList, name='ExperimentalConclusionList'),

    url(r'^ExperimentalProtocol/list/$', List_view.ExperimentalProtocolList, name='ExperimentalProtocolList'),

    url(r'^ExperimenteeBias/list/$', List_view.ExperimenteeBiasList, name='ExperimenteeBiasList'),

    url(r'^HawthorneEffect/list/$', List_view.HawthorneEffectList, name='HawthorneEffectList'),

    url(r'^MortalityEffect/list/$', List_view.MortalityEffectList, name='MortalityEffectList'),

    url(r'^ExperimenterBias/list/$', List_view.ExperimenterBiasList, name='ExperimenterBiasList'),

    url(r'^TestingEffect/list/$', List_view.TestingEffectList, name='TestingEffectList'),

    url(r'^ExplicitHypothesis/list/$', List_view.ExplicitHypothesisList, name='ExplicitHypothesisList'),

    url(r'^ImplicitHypothesis/list/$', List_view.ImplicitHypothesisList, name='ImplicitHypothesisList'),

    url(r'^FactorLevel/list/$', List_view.FactorLevelList, name='FactorLevelList'),

    url(r'^FactorialDesign/list/$', List_view.FactorialDesignList, name='FactorialDesignList'),

    url(r'^FalseNegative/list/$', List_view.FalseNegativeList, name='FalseNegativeList'),

    url(r'^HypothesisAcceptanceMistake/list/$', List_view.HypothesisAcceptanceMistakeList, name='HypothesisAcceptanceMistakeList'),

    url(r'^FalsePpositive/list/$', List_view.FalsePpositiveList, name='FalsePpositiveList'),

    url(r'^FaultyComparison/list/$', List_view.FaultyComparisonList, name='FaultyComparisonList'),

    url(r'^Formula/list/$', List_view.FormulaList, name='FormulaList'),

    url(r'^GalileanExperiment/list/$', List_view.GalileanExperimentList, name='GalileanExperimentList'),

    url(r'^HandTool/list/$', List_view.HandToolList, name='HandToolList'),

    url(r'^Tool/list/$', List_view.ToolList, name='ToolList'),

    url(r'^Hardware/list/$', List_view.HardwareList, name='HardwareList'),

    url(r'^HistoryEffect/list/$', List_view.HistoryEffectList, name='HistoryEffectList'),

    url(r'^MaturationEffect/list/$', List_view.MaturationEffectList, name='MaturationEffectList'),

    url(r'^HypothesisdrivenExperiment/list/$', List_view.HypothesisdrivenExperimentList, name='HypothesisdrivenExperimentList'),

    url(r'^HypothesisformingExperiment/list/$', List_view.HypothesisformingExperimentList, name='HypothesisformingExperimentList'),

    url(r'^Image/list/$', List_view.ImageList, name='ImageList'),

    url(r'^ImproperSampling/list/$', List_view.ImproperSamplingList, name='ImproperSamplingList'),

    url(r'^IncompleteDataError/list/$', List_view.IncompleteDataErrorList, name='IncompleteDataErrorList'),

    url(r'^IndependentVariable/list/$', List_view.IndependentVariableList, name='IndependentVariableList'),

    url(r'^InferableVariable/list/$', List_view.InferableVariableList, name='InferableVariableList'),

    url(r'^InternalStatus/list/$', List_view.InternalStatusList, name='InternalStatusList'),

    url(r'^RestrictedStatus/list/$', List_view.RestrictedStatusList, name='RestrictedStatusList'),

    url(r'^Journal/list/$', List_view.JournalList, name='JournalList'),

    url(r'^LinearModel/list/$', List_view.LinearModelList, name='LinearModelList'),

    url(r'^NonlinearModel/list/$', List_view.NonlinearModelList, name='NonlinearModelList'),

    url(r'^LogicalModelRepresentation/list/$', List_view.LogicalModelRepresentationList, name='LogicalModelRepresentationList'),

    url(r'^Machine/list/$', List_view.MachineList, name='MachineList'),

    url(r'^MachineTool/list/$', List_view.MachineToolList, name='MachineToolList'),

    url(r'^Magazine/list/$', List_view.MagazineList, name='MagazineList'),

    url(r'^Materials/list/$', List_view.MaterialsList, name='MaterialsList'),

    url(r'^MathematicalModelRepresentation/list/$', List_view.MathematicalModelRepresentationList, name='MathematicalModelRepresentationList'),

    url(r'^MetabolomicExperiment/list/$', List_view.MetabolomicExperimentList, name='MetabolomicExperimentList'),

    url(r'^MethodsComparison/list/$', List_view.MethodsComparisonList, name='MethodsComparisonList'),

    url(r'^SubjectsComparison/list/$', List_view.SubjectsComparisonList, name='SubjectsComparisonList'),

    url(r'^MicroarrayExperiment/list/$', List_view.MicroarrayExperimentList, name='MicroarrayExperimentList'),

    url(r'^MultifactorExperiment/list/$', List_view.MultifactorExperimentList, name='MultifactorExperimentList'),

    url(r'^OnefactorExperiment/list/$', List_view.OnefactorExperimentList, name='OnefactorExperimentList'),

    url(r'^TwofactorExperiment/list/$', List_view.TwofactorExperimentList, name='TwofactorExperimentList'),

    url(r'^NaturalLanguage/list/$', List_view.NaturalLanguageList, name='NaturalLanguageList'),

    url(r'^NonRestrictedStatus/list/$', List_view.NonRestrictedStatusList, name='NonRestrictedStatusList'),

    url(r'^NormalizationStrategy/list/$', List_view.NormalizationStrategyList, name='NormalizationStrategyList'),

    url(r'^ObjectMethod/list/$', List_view.ObjectMethodList, name='ObjectMethodList'),

    url(r'^ObjectOfAction/list/$', List_view.ObjectOfActionList, name='ObjectOfActionList'),

    url(r'^ObservableVariable/list/$', List_view.ObservableVariableList, name='ObservableVariableList'),

    url(r'^Paper/list/$', List_view.PaperList, name='PaperList'),

    url(r'^ParticlePhysicsExperiment/list/$', List_view.ParticlePhysicsExperimentList, name='ParticlePhysicsExperimentList'),

    url(r'^PresentingSampling/list/$', List_view.PresentingSamplingList, name='PresentingSamplingList'),

    url(r'^Proceedings/list/$', List_view.ProceedingsList, name='ProceedingsList'),

    url(r'^Program/list/$', List_view.ProgramList, name='ProgramList'),

    url(r'^ProteomicExperiment/list/$', List_view.ProteomicExperimentList, name='ProteomicExperimentList'),

    url(r'^Provider/list/$', List_view.ProviderList, name='ProviderList'),

    url(r'^PublicAcademicStatus/list/$', List_view.PublicAcademicStatusList, name='PublicAcademicStatusList'),

    url(r'^PublicStatus/list/$', List_view.PublicStatusList, name='PublicStatusList'),

    url(r'^RandomError/list/$', List_view.RandomErrorList, name='RandomErrorList'),

    url(r'^RandomSampling/list/$', List_view.RandomSamplingList, name='RandomSamplingList'),

    url(r'^RecommendationSatus/list/$', List_view.RecommendationSatusList, name='RecommendationSatusList'),

    url(r'^RecordingAction/list/$', List_view.RecordingActionList, name='RecordingActionList'),

    url(r'^Region/list/$', List_view.RegionList, name='RegionList'),

    url(r'^RepresenationalMedium/list/$', List_view.RepresenationalMediumList, name='RepresenationalMediumList'),

    url(r'^SUMOSynonym/list/$', List_view.SUMOSynonymList, name='SUMOSynonymList'),

    url(r'^SampleForming/list/$', List_view.SampleFormingList, name='SampleFormingList'),

    url(r'^SequentialDesign/list/$', List_view.SequentialDesignList, name='SequentialDesignList'),

    url(r'^Software/list/$', List_view.SoftwareList, name='SoftwareList'),

    url(r'^Sound/list/$', List_view.SoundList, name='SoundList'),

    url(r'^SplitSamples/list/$', List_view.SplitSamplesList, name='SplitSamplesList'),

    url(r'^SymmetricalMatching/list/$', List_view.SymmetricalMatchingList, name='SymmetricalMatchingList'),

    url(r'^StatisticalError/list/$', List_view.StatisticalErrorList, name='StatisticalErrorList'),

    url(r'^StratifiedSampling/list/$', List_view.StratifiedSamplingList, name='StratifiedSamplingList'),

    url(r'^SubjectOfAction/list/$', List_view.SubjectOfActionList, name='SubjectOfActionList'),

    url(r'^SubjectOfExperiment/list/$', List_view.SubjectOfExperimentList, name='SubjectOfExperimentList'),

    url(r'^Submitter/list/$', List_view.SubmitterList, name='SubmitterList'),

    url(r'^Summary/list/$', List_view.SummaryList, name='SummaryList'),

    url(r'^SystematicError/list/$', List_view.SystematicErrorList, name='SystematicErrorList'),

    url(r'^TechnicalReport/list/$', List_view.TechnicalReportList, name='TechnicalReportList'),

    url(r'^TimeDuration/list/$', List_view.TimeDurationList, name='TimeDurationList'),

    url(r'^TimeInterval/list/$', List_view.TimeIntervalList, name='TimeIntervalList'),

    url(r'^URL/list/$', List_view.URLList, name='URLList'),


    url(r'^Thing/(?P<pk>\d+)/update/$', Update_view.ThingUpdate, name='ThingUpdate'),

    url(r'^RepresentationForm/(?P<pk>\d+)/update/$', Update_view.RepresentationFormUpdate, name='RepresentationFormUpdate'),

    url(r'^Artifact/(?P<pk>\d+)/update/$', Update_view.ArtifactUpdate, name='ArtifactUpdate'),

    url(r'^SynonymousExternalConcept/(?P<pk>\d+)/update/$', Update_view.SynonymousExternalConceptUpdate, name='SynonymousExternalConceptUpdate'),

    url(r'^Name/(?P<pk>\d+)/update/$', Update_view.NameUpdate, name='NameUpdate'),

    url(r'^ScientificExperiment/(?P<pk>\d+)/update/$', Update_view.ScientificExperimentUpdate, name='ScientificExperimentUpdate'),

    url(r'^ExperimentalDesign/(?P<pk>\d+)/update/$', Update_view.ExperimentalDesignUpdate, name='ExperimentalDesignUpdate'),

    url(r'^AdminInfoExperiment/(?P<pk>\d+)/update/$', Update_view.AdminInfoExperimentUpdate, name='AdminInfoExperimentUpdate'),

    url(r'^IDExperiment/(?P<pk>\d+)/update/$', Update_view.IDExperimentUpdate, name='IDExperimentUpdate'),

    url(r'^DomainOfExperiment/(?P<pk>\d+)/update/$', Update_view.DomainOfExperimentUpdate, name='DomainOfExperimentUpdate'),

    url(r'^ExperimentalObservation/(?P<pk>\d+)/update/$', Update_view.ExperimentalObservationUpdate, name='ExperimentalObservationUpdate'),

    url(r'^ResultsInterpretation/(?P<pk>\d+)/update/$', Update_view.ResultsInterpretationUpdate, name='ResultsInterpretationUpdate'),

    url(r'^ExperimentalAction/(?P<pk>\d+)/update/$', Update_view.ExperimentalActionUpdate, name='ExperimentalActionUpdate'),

    url(r'^ActionName/(?P<pk>\d+)/update/$', Update_view.ActionNameUpdate, name='ActionNameUpdate'),

    url(r'^ActionComplexity/(?P<pk>\d+)/update/$', Update_view.ActionComplexityUpdate, name='ActionComplexityUpdate'),

    url(r'^Human/(?P<pk>\d+)/update/$', Update_view.HumanUpdate, name='HumanUpdate'),

    url(r'^Organization/(?P<pk>\d+)/update/$', Update_view.OrganizationUpdate, name='OrganizationUpdate'),

    url(r'^AdminInfoAuthor/(?P<pk>\d+)/update/$', Update_view.AdminInfoAuthorUpdate, name='AdminInfoAuthorUpdate'),

    url(r'^AdminInfoObjectOfExperiment/(?P<pk>\d+)/update/$', Update_view.AdminInfoObjectOfExperimentUpdate, name='AdminInfoObjectOfExperimentUpdate'),

    url(r'^AdminInfoProvider/(?P<pk>\d+)/update/$', Update_view.AdminInfoProviderUpdate, name='AdminInfoProviderUpdate'),

    url(r'^AdminInfoSubjectOfExperiment/(?P<pk>\d+)/update/$', Update_view.AdminInfoSubjectOfExperimentUpdate, name='AdminInfoSubjectOfExperimentUpdate'),

    url(r'^AdminInfoSubmitter/(?P<pk>\d+)/update/$', Update_view.AdminInfoSubmitterUpdate, name='AdminInfoSubmitterUpdate'),

    url(r'^AdminInfoUser/(?P<pk>\d+)/update/$', Update_view.AdminInfoUserUpdate, name='AdminInfoUserUpdate'),

    url(r'^ExperimentalMethod/(?P<pk>\d+)/update/$', Update_view.ExperimentalMethodUpdate, name='ExperimentalMethodUpdate'),

    url(r'^MethodApplicabilityCondition/(?P<pk>\d+)/update/$', Update_view.MethodApplicabilityConditionUpdate, name='MethodApplicabilityConditionUpdate'),

    url(r'^StandardOperatingProcedure/(?P<pk>\d+)/update/$', Update_view.StandardOperatingProcedureUpdate, name='StandardOperatingProcedureUpdate'),

    url(r'^Author/(?P<pk>\d+)/update/$', Update_view.AuthorUpdate, name='AuthorUpdate'),

    url(r'^ClassificationOfDomains/(?P<pk>\d+)/update/$', Update_view.ClassificationOfDomainsUpdate, name='ClassificationOfDomainsUpdate'),

    url(r'^ClassificationOfExperiments/(?P<pk>\d+)/update/$', Update_view.ClassificationOfExperimentsUpdate, name='ClassificationOfExperimentsUpdate'),

    url(r'^ExperimentalHypothesis/(?P<pk>\d+)/update/$', Update_view.ExperimentalHypothesisUpdate, name='ExperimentalHypothesisUpdate'),

    url(r'^HypothesisComplexity/(?P<pk>\d+)/update/$', Update_view.HypothesisComplexityUpdate, name='HypothesisComplexityUpdate'),

    url(r'^Representation/(?P<pk>\d+)/update/$', Update_view.RepresentationUpdate, name='RepresentationUpdate'),

    url(r'^Proposition/(?P<pk>\d+)/update/$', Update_view.PropositionUpdate, name='PropositionUpdate'),

    url(r'^ExperimentalEquipment/(?P<pk>\d+)/update/$', Update_view.ExperimentalEquipmentUpdate, name='ExperimentalEquipmentUpdate'),

    url(r'^TechnicalDescription/(?P<pk>\d+)/update/$', Update_view.TechnicalDescriptionUpdate, name='TechnicalDescriptionUpdate'),

    url(r'^ExperimentalResults/(?P<pk>\d+)/update/$', Update_view.ExperimentalResultsUpdate, name='ExperimentalResultsUpdate'),

    url(r'^ObservationalError/(?P<pk>\d+)/update/$', Update_view.ObservationalErrorUpdate, name='ObservationalErrorUpdate'),

    url(r'^ResultError/(?P<pk>\d+)/update/$', Update_view.ResultErrorUpdate, name='ResultErrorUpdate'),

    url(r'^ResultsEvaluation/(?P<pk>\d+)/update/$', Update_view.ResultsEvaluationUpdate, name='ResultsEvaluationUpdate'),

    url(r'^ScientificInvestigation/(?P<pk>\d+)/update/$', Update_view.ScientificInvestigationUpdate, name='ScientificInvestigationUpdate'),

    url(r'^ExperimentalModel/(?P<pk>\d+)/update/$', Update_view.ExperimentalModelUpdate, name='ExperimentalModelUpdate'),

    url(r'^ExperimentalFactor/(?P<pk>\d+)/update/$', Update_view.ExperimentalFactorUpdate, name='ExperimentalFactorUpdate'),

    url(r'^ExperimentalTechnology/(?P<pk>\d+)/update/$', Update_view.ExperimentalTechnologyUpdate, name='ExperimentalTechnologyUpdate'),

    url(r'^ExperimentalRequirements/(?P<pk>\d+)/update/$', Update_view.ExperimentalRequirementsUpdate, name='ExperimentalRequirementsUpdate'),

    url(r'^HypothesisExplicitness/(?P<pk>\d+)/update/$', Update_view.HypothesisExplicitnessUpdate, name='HypothesisExplicitnessUpdate'),

    url(r'^LinguisticExpression/(?P<pk>\d+)/update/$', Update_view.LinguisticExpressionUpdate, name='LinguisticExpressionUpdate'),

    url(r'^FactRejectResearchHypothesis/(?P<pk>\d+)/update/$', Update_view.FactRejectResearchHypothesisUpdate, name='FactRejectResearchHypothesisUpdate'),

    url(r'^FactSupportResearchHypothesis/(?P<pk>\d+)/update/$', Update_view.FactSupportResearchHypothesisUpdate, name='FactSupportResearchHypothesisUpdate'),

    url(r'^Number/(?P<pk>\d+)/update/$', Update_view.NumberUpdate, name='NumberUpdate'),

    url(r'^GeneralityOfHypothesis/(?P<pk>\d+)/update/$', Update_view.GeneralityOfHypothesisUpdate, name='GeneralityOfHypothesisUpdate'),

    url(r'^ActionGoal/(?P<pk>\d+)/update/$', Update_view.ActionGoalUpdate, name='ActionGoalUpdate'),

    url(r'^ExperimentalGoal/(?P<pk>\d+)/update/$', Update_view.ExperimentalGoalUpdate, name='ExperimentalGoalUpdate'),

    url(r'^MethodGoal/(?P<pk>\d+)/update/$', Update_view.MethodGoalUpdate, name='MethodGoalUpdate'),

    url(r'^HypothesisFormation/(?P<pk>\d+)/update/$', Update_view.HypothesisFormationUpdate, name='HypothesisFormationUpdate'),

    url(r'^HypothesisRepresentation/(?P<pk>\d+)/update/$', Update_view.HypothesisRepresentationUpdate, name='HypothesisRepresentationUpdate'),

    url(r'^InformationGethering/(?P<pk>\d+)/update/$', Update_view.InformationGetheringUpdate, name='InformationGetheringUpdate'),

    url(r'^Sample/(?P<pk>\d+)/update/$', Update_view.SampleUpdate, name='SampleUpdate'),

    url(r'^Variability/(?P<pk>\d+)/update/$', Update_view.VariabilityUpdate, name='VariabilityUpdate'),

    url(r'^TimePoint/(?P<pk>\d+)/update/$', Update_view.TimePointUpdate, name='TimePointUpdate'),

    url(r'^User/(?P<pk>\d+)/update/$', Update_view.UserUpdate, name='UserUpdate'),

    url(r'^LoginName/(?P<pk>\d+)/update/$', Update_view.LoginNameUpdate, name='LoginNameUpdate'),

    url(r'^DomainModel/(?P<pk>\d+)/update/$', Update_view.DomainModelUpdate, name='DomainModelUpdate'),

    url(r'^GroupExperimentalObject/(?P<pk>\d+)/update/$', Update_view.GroupExperimentalObjectUpdate, name='GroupExperimentalObjectUpdate'),

    url(r'^ObjectOfExperiment/(?P<pk>\d+)/update/$', Update_view.ObjectOfExperimentUpdate, name='ObjectOfExperimentUpdate'),

    url(r'^ClassificationByModel/(?P<pk>\d+)/update/$', Update_view.ClassificationByModelUpdate, name='ClassificationByModelUpdate'),

    url(r'^StatisticsCharacteristic/(?P<pk>\d+)/update/$', Update_view.StatisticsCharacteristicUpdate, name='StatisticsCharacteristicUpdate'),

    url(r'^ParentGroup/(?P<pk>\d+)/update/$', Update_view.ParentGroupUpdate, name='ParentGroupUpdate'),

    url(r'^Password/(?P<pk>\d+)/update/$', Update_view.PasswordUpdate, name='PasswordUpdate'),

    url(r'^ProcedureExecuteExperiment/(?P<pk>\d+)/update/$', Update_view.ProcedureExecuteExperimentUpdate, name='ProcedureExecuteExperimentUpdate'),

    url(r'^PlanOfExperimentalActions/(?P<pk>\d+)/update/$', Update_view.PlanOfExperimentalActionsUpdate, name='PlanOfExperimentalActionsUpdate'),

    url(r'^PoblemAnalysis/(?P<pk>\d+)/update/$', Update_view.PoblemAnalysisUpdate, name='PoblemAnalysisUpdate'),

    url(r'^SubjectQualification/(?P<pk>\d+)/update/$', Update_view.SubjectQualificationUpdate, name='SubjectQualificationUpdate'),

    url(r'^BiblioReference/(?P<pk>\d+)/update/$', Update_view.BiblioReferenceUpdate, name='BiblioReferenceUpdate'),

    url(r'^Model/(?P<pk>\d+)/update/$', Update_view.ModelUpdate, name='ModelUpdate'),

    url(r'^ModelRepresentation/(?P<pk>\d+)/update/$', Update_view.ModelRepresentationUpdate, name='ModelRepresentationUpdate'),

    url(r'^RepresentationExperimentalExecutionProcedure/(?P<pk>\d+)/update/$', Update_view.RepresentationExperimentalExecutionProcedureUpdate, name='RepresentationExperimentalExecutionProcedureUpdate'),

    url(r'^RepresentationExperimentalGoal/(?P<pk>\d+)/update/$', Update_view.RepresentationExperimentalGoalUpdate, name='RepresentationExperimentalGoalUpdate'),

    url(r'^SampleRepresentativeness/(?P<pk>\d+)/update/$', Update_view.SampleRepresentativenessUpdate, name='SampleRepresentativenessUpdate'),

    url(r'^SampleSize/(?P<pk>\d+)/update/$', Update_view.SampleSizeUpdate, name='SampleSizeUpdate'),

    url(r'^SamplingMethod/(?P<pk>\d+)/update/$', Update_view.SamplingMethodUpdate, name='SamplingMethodUpdate'),

    url(r'^SystematicSampling/(?P<pk>\d+)/update/$', Update_view.SystematicSamplingUpdate, name='SystematicSamplingUpdate'),

    url(r'^SamplingRule/(?P<pk>\d+)/update/$', Update_view.SamplingRuleUpdate, name='SamplingRuleUpdate'),

    url(r'^ExperimentalStandard/(?P<pk>\d+)/update/$', Update_view.ExperimentalStandardUpdate, name='ExperimentalStandardUpdate'),

    url(r'^Error/(?P<pk>\d+)/update/$', Update_view.ErrorUpdate, name='ErrorUpdate'),

    url(r'^Dispersion/(?P<pk>\d+)/update/$', Update_view.DispersionUpdate, name='DispersionUpdate'),

    url(r'^LevelOfSignificance/(?P<pk>\d+)/update/$', Update_view.LevelOfSignificanceUpdate, name='LevelOfSignificanceUpdate'),

    url(r'^StatusExperimentalDocument/(?P<pk>\d+)/update/$', Update_view.StatusExperimentalDocumentUpdate, name='StatusExperimentalDocumentUpdate'),

    url(r'^ExperimentalDesignStrategy/(?P<pk>\d+)/update/$', Update_view.ExperimentalDesignStrategyUpdate, name='ExperimentalDesignStrategyUpdate'),

    url(r'^ExperimentalSubgoal/(?P<pk>\d+)/update/$', Update_view.ExperimentalSubgoalUpdate, name='ExperimentalSubgoalUpdate'),

    url(r'^SubjectMethod/(?P<pk>\d+)/update/$', Update_view.SubjectMethodUpdate, name='SubjectMethodUpdate'),

    url(r'^BaconianExperiment/(?P<pk>\d+)/update/$', Update_view.BaconianExperimentUpdate, name='BaconianExperimentUpdate'),

    url(r'^TargetVariable/(?P<pk>\d+)/update/$', Update_view.TargetVariableUpdate, name='TargetVariableUpdate'),

    url(r'^Title/(?P<pk>\d+)/update/$', Update_view.TitleUpdate, name='TitleUpdate'),

    url(r'^ValueEstimate/(?P<pk>\d+)/update/$', Update_view.ValueEstimateUpdate, name='ValueEstimateUpdate'),

    url(r'^ValueOfVariable/(?P<pk>\d+)/update/$', Update_view.ValueOfVariableUpdate, name='ValueOfVariableUpdate'),

    url(r'^Role/(?P<pk>\d+)/update/$', Update_view.RoleUpdate, name='RoleUpdate'),

    url(r'^CorpuscularObject/(?P<pk>\d+)/update/$', Update_view.CorpuscularObjectUpdate, name='CorpuscularObjectUpdate'),

    url(r'^AttributeRole/(?P<pk>\d+)/update/$', Update_view.AttributeRoleUpdate, name='AttributeRoleUpdate'),

    url(r'^Attribute/(?P<pk>\d+)/update/$', Update_view.AttributeUpdate, name='AttributeUpdate'),

    url(r'^Procedure/(?P<pk>\d+)/update/$', Update_view.ProcedureUpdate, name='ProcedureUpdate'),

    url(r'^AdministrativeInformation/(?P<pk>\d+)/update/$', Update_view.AdministrativeInformationUpdate, name='AdministrativeInformationUpdate'),

    url(r'^TitleExperiment/(?P<pk>\d+)/update/$', Update_view.TitleExperimentUpdate, name='TitleExperimentUpdate'),

    url(r'^NameEquipment/(?P<pk>\d+)/update/$', Update_view.NameEquipmentUpdate, name='NameEquipmentUpdate'),

    url(r'^PersonalName/(?P<pk>\d+)/update/$', Update_view.PersonalNameUpdate, name='PersonalNameUpdate'),

    url(r'^FieldOfStudy/(?P<pk>\d+)/update/$', Update_view.FieldOfStudyUpdate, name='FieldOfStudyUpdate'),

    url(r'^RelatedDomain/(?P<pk>\d+)/update/$', Update_view.RelatedDomainUpdate, name='RelatedDomainUpdate'),

    url(r'^ProductRole/(?P<pk>\d+)/update/$', Update_view.ProductRoleUpdate, name='ProductRoleUpdate'),

    url(r'^ScientificTask/(?P<pk>\d+)/update/$', Update_view.ScientificTaskUpdate, name='ScientificTaskUpdate'),

    url(r'^ExecutionOfExperiment/(?P<pk>\d+)/update/$', Update_view.ExecutionOfExperimentUpdate, name='ExecutionOfExperimentUpdate'),

    url(r'^CorporateName/(?P<pk>\d+)/update/$', Update_view.CorporateNameUpdate, name='CorporateNameUpdate'),

    url(r'^AttributeOfAction/(?P<pk>\d+)/update/$', Update_view.AttributeOfActionUpdate, name='AttributeOfActionUpdate'),

    url(r'^SentientAgent/(?P<pk>\d+)/update/$', Update_view.SentientAgentUpdate, name='SentientAgentUpdate'),

    url(r'^Robot/(?P<pk>\d+)/update/$', Update_view.RobotUpdate, name='RobotUpdate'),

    url(r'^Group/(?P<pk>\d+)/update/$', Update_view.GroupUpdate, name='GroupUpdate'),

    url(r'^Entity/(?P<pk>\d+)/update/$', Update_view.EntityUpdate, name='EntityUpdate'),

    url(r'^ResearchMethod/(?P<pk>\d+)/update/$', Update_view.ResearchMethodUpdate, name='ResearchMethodUpdate'),

    url(r'^Requirements/(?P<pk>\d+)/update/$', Update_view.RequirementsUpdate, name='RequirementsUpdate'),

    url(r'^AuthorSOP/(?P<pk>\d+)/update/$', Update_view.AuthorSOPUpdate, name='AuthorSOPUpdate'),

    url(r'^SubjectRole/(?P<pk>\d+)/update/$', Update_view.SubjectRoleUpdate, name='SubjectRoleUpdate'),

    url(r'^Classification/(?P<pk>\d+)/update/$', Update_view.ClassificationUpdate, name='ClassificationUpdate'),

    url(r'^Fact/(?P<pk>\d+)/update/$', Update_view.FactUpdate, name='FactUpdate'),

    url(r'^ModelAssumption/(?P<pk>\d+)/update/$', Update_view.ModelAssumptionUpdate, name='ModelAssumptionUpdate'),

    url(r'^Variable/(?P<pk>\d+)/update/$', Update_view.VariableUpdate, name='VariableUpdate'),

    url(r'^AttributeOfHypothesis/(?P<pk>\d+)/update/$', Update_view.AttributeOfHypothesisUpdate, name='AttributeOfHypothesisUpdate'),

    url(r'^ContentBearingObject/(?P<pk>\d+)/update/$', Update_view.ContentBearingObjectUpdate, name='ContentBearingObjectUpdate'),

    url(r'^Abstract/(?P<pk>\d+)/update/$', Update_view.AbstractUpdate, name='AbstractUpdate'),

    url(r'^Quantity/(?P<pk>\d+)/update/$', Update_view.QuantityUpdate, name='QuantityUpdate'),

    url(r'^Relation/(?P<pk>\d+)/update/$', Update_view.RelationUpdate, name='RelationUpdate'),

    url(r'^ProcessrelatedRole/(?P<pk>\d+)/update/$', Update_view.ProcessrelatedRoleUpdate, name='ProcessrelatedRoleUpdate'),

    url(r'^ContentBearingPhysical/(?P<pk>\d+)/update/$', Update_view.ContentBearingPhysicalUpdate, name='ContentBearingPhysicalUpdate'),

    url(r'^PhysicalQuantity/(?P<pk>\d+)/update/$', Update_view.PhysicalQuantityUpdate, name='PhysicalQuantityUpdate'),

    url(r'^Goal/(?P<pk>\d+)/update/$', Update_view.GoalUpdate, name='GoalUpdate'),

    url(r'^RepresentationExperimentalObservation/(?P<pk>\d+)/update/$', Update_view.RepresentationExperimentalObservationUpdate, name='RepresentationExperimentalObservationUpdate'),

    url(r'^RepresentationExperimentalResults/(?P<pk>\d+)/update/$', Update_view.RepresentationExperimentalResultsUpdate, name='RepresentationExperimentalResultsUpdate'),

    url(r'^AttributeGroup/(?P<pk>\d+)/update/$', Update_view.AttributeGroupUpdate, name='AttributeGroupUpdate'),

    url(r'^TimePosition/(?P<pk>\d+)/update/$', Update_view.TimePositionUpdate, name='TimePositionUpdate'),

    url(r'^ActorRole/(?P<pk>\d+)/update/$', Update_view.ActorRoleUpdate, name='ActorRoleUpdate'),

    url(r'^DocumentStage/(?P<pk>\d+)/update/$', Update_view.DocumentStageUpdate, name='DocumentStageUpdate'),

    url(r'^PermissionStatus/(?P<pk>\d+)/update/$', Update_view.PermissionStatusUpdate, name='PermissionStatusUpdate'),

    url(r'^Plan/(?P<pk>\d+)/update/$', Update_view.PlanUpdate, name='PlanUpdate'),

    url(r'^Reference/(?P<pk>\d+)/update/$', Update_view.ReferenceUpdate, name='ReferenceUpdate'),

    url(r'^AttributeSample/(?P<pk>\d+)/update/$', Update_view.AttributeSampleUpdate, name='AttributeSampleUpdate'),

    url(r'^ExperimentalRule/(?P<pk>\d+)/update/$', Update_view.ExperimentalRuleUpdate, name='ExperimentalRuleUpdate'),

    url(r'^Robustness/(?P<pk>\d+)/update/$', Update_view.RobustnessUpdate, name='RobustnessUpdate'),

    url(r'^Validity/(?P<pk>\d+)/update/$', Update_view.ValidityUpdate, name='ValidityUpdate'),

    url(r'^AttributeOfDocument/(?P<pk>\d+)/update/$', Update_view.AttributeOfDocumentUpdate, name='AttributeOfDocumentUpdate'),

    url(r'^PhysicalExperiment/(?P<pk>\d+)/update/$', Update_view.PhysicalExperimentUpdate, name='PhysicalExperimentUpdate'),

    url(r'^ComputationalData/(?P<pk>\d+)/update/$', Update_view.ComputationalDataUpdate, name='ComputationalDataUpdate'),

    url(r'^Predicate/(?P<pk>\d+)/update/$', Update_view.PredicateUpdate, name='PredicateUpdate'),

    url(r'^SelfConnectedObject/(?P<pk>\d+)/update/$', Update_view.SelfConnectedObjectUpdate, name='SelfConnectedObjectUpdate'),

    url(r'^ScientificActivity/(?P<pk>\d+)/update/$', Update_view.ScientificActivityUpdate, name='ScientificActivityUpdate'),

    url(r'^FormingClassificationSystem/(?P<pk>\d+)/update/$', Update_view.FormingClassificationSystemUpdate, name='FormingClassificationSystemUpdate'),

    url(r'^HypothesisForming/(?P<pk>\d+)/update/$', Update_view.HypothesisFormingUpdate, name='HypothesisFormingUpdate'),

    url(r'^InterpretingResult/(?P<pk>\d+)/update/$', Update_view.InterpretingResultUpdate, name='InterpretingResultUpdate'),

    url(r'^ProcessProblemAnalysis/(?P<pk>\d+)/update/$', Update_view.ProcessProblemAnalysisUpdate, name='ProcessProblemAnalysisUpdate'),

    url(r'^ResultEvaluating/(?P<pk>\d+)/update/$', Update_view.ResultEvaluatingUpdate, name='ResultEvaluatingUpdate'),

    url(r'^AttributeOfModel/(?P<pk>\d+)/update/$', Update_view.AttributeOfModelUpdate, name='AttributeOfModelUpdate'),

    url(r'^AttributeOfVariable/(?P<pk>\d+)/update/$', Update_view.AttributeOfVariableUpdate, name='AttributeOfVariableUpdate'),

    url(r'^Agent/(?P<pk>\d+)/update/$', Update_view.AgentUpdate, name='AgentUpdate'),

    url(r'^Collection/(?P<pk>\d+)/update/$', Update_view.CollectionUpdate, name='CollectionUpdate'),

    url(r'^EperimentalDesignTask/(?P<pk>\d+)/update/$', Update_view.EperimentalDesignTaskUpdate, name='EperimentalDesignTaskUpdate'),

    url(r'^Physical/(?P<pk>\d+)/update/$', Update_view.PhysicalUpdate, name='PhysicalUpdate'),

    url(r'^Object/(?P<pk>\d+)/update/$', Update_view.ObjectUpdate, name='ObjectUpdate'),

    url(r'^Process/(?P<pk>\d+)/update/$', Update_view.ProcessUpdate, name='ProcessUpdate'),

    url(r'^TaskRole/(?P<pk>\d+)/update/$', Update_view.TaskRoleUpdate, name='TaskRoleUpdate'),

    url(r'^TimeMeasure/(?P<pk>\d+)/update/$', Update_view.TimeMeasureUpdate, name='TimeMeasureUpdate'),

    url(r'^ActionrelatedRole/(?P<pk>\d+)/update/$', Update_view.ActionrelatedRoleUpdate, name='ActionrelatedRoleUpdate'),

    url(r'^AbductiveHypothesis/(?P<pk>\d+)/update/$', Update_view.AbductiveHypothesisUpdate, name='AbductiveHypothesisUpdate'),

    url(r'^InductiveHypothesis/(?P<pk>\d+)/update/$', Update_view.InductiveHypothesisUpdate, name='InductiveHypothesisUpdate'),

    url(r'^Adequacy/(?P<pk>\d+)/update/$', Update_view.AdequacyUpdate, name='AdequacyUpdate'),

    url(r'^AlternativeHypothesis/(?P<pk>\d+)/update/$', Update_view.AlternativeHypothesisUpdate, name='AlternativeHypothesisUpdate'),

    url(r'^NullHypothesis/(?P<pk>\d+)/update/$', Update_view.NullHypothesisUpdate, name='NullHypothesisUpdate'),

    url(r'^ResearchHypothesis/(?P<pk>\d+)/update/$', Update_view.ResearchHypothesisUpdate, name='ResearchHypothesisUpdate'),

    url(r'^Article/(?P<pk>\d+)/update/$', Update_view.ArticleUpdate, name='ArticleUpdate'),

    url(r'^Text/(?P<pk>\d+)/update/$', Update_view.TextUpdate, name='TextUpdate'),

    url(r'^ArtificialLanguage/(?P<pk>\d+)/update/$', Update_view.ArtificialLanguageUpdate, name='ArtificialLanguageUpdate'),

    url(r'^Language/(?P<pk>\d+)/update/$', Update_view.LanguageUpdate, name='LanguageUpdate'),

    url(r'^HumanLanguage/(?P<pk>\d+)/update/$', Update_view.HumanLanguageUpdate, name='HumanLanguageUpdate'),

    url(r'^Sentence/(?P<pk>\d+)/update/$', Update_view.SentenceUpdate, name='SentenceUpdate'),

    url(r'^AtomicAction/(?P<pk>\d+)/update/$', Update_view.AtomicActionUpdate, name='AtomicActionUpdate'),

    url(r'^ComplexAction/(?P<pk>\d+)/update/$', Update_view.ComplexActionUpdate, name='ComplexActionUpdate'),

    url(r'^AuthorProtocol/(?P<pk>\d+)/update/$', Update_view.AuthorProtocolUpdate, name='AuthorProtocolUpdate'),

    url(r'^Book/(?P<pk>\d+)/update/$', Update_view.BookUpdate, name='BookUpdate'),

    url(r'^CalculableVariable/(?P<pk>\d+)/update/$', Update_view.CalculableVariableUpdate, name='CalculableVariableUpdate'),

    url(r'^CapacityRequirements/(?P<pk>\d+)/update/$', Update_view.CapacityRequirementsUpdate, name='CapacityRequirementsUpdate'),

    url(r'^EnvironmentalRequirements/(?P<pk>\d+)/update/$', Update_view.EnvironmentalRequirementsUpdate, name='EnvironmentalRequirementsUpdate'),

    url(r'^FinancialRequirements/(?P<pk>\d+)/update/$', Update_view.FinancialRequirementsUpdate, name='FinancialRequirementsUpdate'),

    url(r'^ClassificationByDomain/(?P<pk>\d+)/update/$', Update_view.ClassificationByDomainUpdate, name='ClassificationByDomainUpdate'),

    url(r'^Classifying/(?P<pk>\d+)/update/$', Update_view.ClassifyingUpdate, name='ClassifyingUpdate'),

    url(r'^DesigningExperiment/(?P<pk>\d+)/update/$', Update_view.DesigningExperimentUpdate, name='DesigningExperimentUpdate'),

    url(r'^CoarseError/(?P<pk>\d+)/update/$', Update_view.CoarseErrorUpdate, name='CoarseErrorUpdate'),

    url(r'^MeasurementError/(?P<pk>\d+)/update/$', Update_view.MeasurementErrorUpdate, name='MeasurementErrorUpdate'),

    url(r'^ComparisonControl_TargetGroups/(?P<pk>\d+)/update/$', Update_view.ComparisonControl_TargetGroupsUpdate, name='ComparisonControl_TargetGroupsUpdate'),

    url(r'^PairedComparison/(?P<pk>\d+)/update/$', Update_view.PairedComparisonUpdate, name='PairedComparisonUpdate'),

    url(r'^PairedComparisonOfMatchingGroups/(?P<pk>\d+)/update/$', Update_view.PairedComparisonOfMatchingGroupsUpdate, name='PairedComparisonOfMatchingGroupsUpdate'),

    url(r'^PairedComparisonOfSingleSampleGroups/(?P<pk>\d+)/update/$', Update_view.PairedComparisonOfSingleSampleGroupsUpdate, name='PairedComparisonOfSingleSampleGroupsUpdate'),

    url(r'^QualityControl/(?P<pk>\d+)/update/$', Update_view.QualityControlUpdate, name='QualityControlUpdate'),

    url(r'^ComplexHypothesis/(?P<pk>\d+)/update/$', Update_view.ComplexHypothesisUpdate, name='ComplexHypothesisUpdate'),

    url(r'^SingleHypothesis/(?P<pk>\d+)/update/$', Update_view.SingleHypothesisUpdate, name='SingleHypothesisUpdate'),

    url(r'^ComputationalExperiment/(?P<pk>\d+)/update/$', Update_view.ComputationalExperimentUpdate, name='ComputationalExperimentUpdate'),

    url(r'^ComputeGoal/(?P<pk>\d+)/update/$', Update_view.ComputeGoalUpdate, name='ComputeGoalUpdate'),

    url(r'^ConfirmGoal/(?P<pk>\d+)/update/$', Update_view.ConfirmGoalUpdate, name='ConfirmGoalUpdate'),

    url(r'^ExplainGoal/(?P<pk>\d+)/update/$', Update_view.ExplainGoalUpdate, name='ExplainGoalUpdate'),

    url(r'^InvestigateGoal/(?P<pk>\d+)/update/$', Update_view.InvestigateGoalUpdate, name='InvestigateGoalUpdate'),

    url(r'^ComputerSimulation/(?P<pk>\d+)/update/$', Update_view.ComputerSimulationUpdate, name='ComputerSimulationUpdate'),

    url(r'^ConstantQuantity/(?P<pk>\d+)/update/$', Update_view.ConstantQuantityUpdate, name='ConstantQuantityUpdate'),

    url(r'^Controllability/(?P<pk>\d+)/update/$', Update_view.ControllabilityUpdate, name='ControllabilityUpdate'),

    url(r'^Independence/(?P<pk>\d+)/update/$', Update_view.IndependenceUpdate, name='IndependenceUpdate'),

    url(r'^DBReference/(?P<pk>\d+)/update/$', Update_view.DBReferenceUpdate, name='DBReferenceUpdate'),

    url(r'^DDC_Dewey_Classification/(?P<pk>\d+)/update/$', Update_view.DDC_Dewey_ClassificationUpdate, name='DDC_Dewey_ClassificationUpdate'),

    url(r'^LibraryOfCongressClassification/(?P<pk>\d+)/update/$', Update_view.LibraryOfCongressClassificationUpdate, name='LibraryOfCongressClassificationUpdate'),

    url(r'^NLMClassification/(?P<pk>\d+)/update/$', Update_view.NLMClassificationUpdate, name='NLMClassificationUpdate'),

    url(r'^ResearchCouncilsUKClassification/(?P<pk>\d+)/update/$', Update_view.ResearchCouncilsUKClassificationUpdate, name='ResearchCouncilsUKClassificationUpdate'),

    url(r'^DataRepresentationStandard/(?P<pk>\d+)/update/$', Update_view.DataRepresentationStandardUpdate, name='DataRepresentationStandardUpdate'),

    url(r'^DegreeOfModel/(?P<pk>\d+)/update/$', Update_view.DegreeOfModelUpdate, name='DegreeOfModelUpdate'),

    url(r'^DynamismOfModel/(?P<pk>\d+)/update/$', Update_view.DynamismOfModelUpdate, name='DynamismOfModelUpdate'),

    url(r'^DependentVariable/(?P<pk>\d+)/update/$', Update_view.DependentVariableUpdate, name='DependentVariableUpdate'),

    url(r'^Device/(?P<pk>\d+)/update/$', Update_view.DeviceUpdate, name='DeviceUpdate'),

    url(r'^DomainAction/(?P<pk>\d+)/update/$', Update_view.DomainActionUpdate, name='DomainActionUpdate'),

    url(r'^DoseResponse/(?P<pk>\d+)/update/$', Update_view.DoseResponseUpdate, name='DoseResponseUpdate'),

    url(r'^GeneKnockin/(?P<pk>\d+)/update/$', Update_view.GeneKnockinUpdate, name='GeneKnockinUpdate'),

    url(r'^GeneKnockout/(?P<pk>\d+)/update/$', Update_view.GeneKnockoutUpdate, name='GeneKnockoutUpdate'),

    url(r'^Normal_Disease/(?P<pk>\d+)/update/$', Update_view.Normal_DiseaseUpdate, name='Normal_DiseaseUpdate'),

    url(r'^TimeCourse/(?P<pk>\d+)/update/$', Update_view.TimeCourseUpdate, name='TimeCourseUpdate'),

    url(r'^Treated_Untreated/(?P<pk>\d+)/update/$', Update_view.Treated_UntreatedUpdate, name='Treated_UntreatedUpdate'),

    url(r'^DraftStatus/(?P<pk>\d+)/update/$', Update_view.DraftStatusUpdate, name='DraftStatusUpdate'),

    url(r'^DuhemEffect/(?P<pk>\d+)/update/$', Update_view.DuhemEffectUpdate, name='DuhemEffectUpdate'),

    url(r'^ExperimentalDesignEffect/(?P<pk>\d+)/update/$', Update_view.ExperimentalDesignEffectUpdate, name='ExperimentalDesignEffectUpdate'),

    url(r'^InstrumentationEffect/(?P<pk>\d+)/update/$', Update_view.InstrumentationEffectUpdate, name='InstrumentationEffectUpdate'),

    url(r'^ObjectEffect/(?P<pk>\d+)/update/$', Update_view.ObjectEffectUpdate, name='ObjectEffectUpdate'),

    url(r'^SubjectEffect/(?P<pk>\d+)/update/$', Update_view.SubjectEffectUpdate, name='SubjectEffectUpdate'),

    url(r'^TimeEffect/(?P<pk>\d+)/update/$', Update_view.TimeEffectUpdate, name='TimeEffectUpdate'),

    url(r'^DynamicModel/(?P<pk>\d+)/update/$', Update_view.DynamicModelUpdate, name='DynamicModelUpdate'),

    url(r'^StaticModel/(?P<pk>\d+)/update/$', Update_view.StaticModelUpdate, name='StaticModelUpdate'),

    url(r'^ErrorOfConclusion/(?P<pk>\d+)/update/$', Update_view.ErrorOfConclusionUpdate, name='ErrorOfConclusionUpdate'),

    url(r'^ExperimentalActionsPlanning/(?P<pk>\d+)/update/$', Update_view.ExperimentalActionsPlanningUpdate, name='ExperimentalActionsPlanningUpdate'),

    url(r'^ExperimentalEquipmentSelecting/(?P<pk>\d+)/update/$', Update_view.ExperimentalEquipmentSelectingUpdate, name='ExperimentalEquipmentSelectingUpdate'),

    url(r'^ExperimentalModelDesign/(?P<pk>\d+)/update/$', Update_view.ExperimentalModelDesignUpdate, name='ExperimentalModelDesignUpdate'),

    url(r'^ExperimentalObjectSelecting/(?P<pk>\d+)/update/$', Update_view.ExperimentalObjectSelectingUpdate, name='ExperimentalObjectSelectingUpdate'),

    url(r'^ExperimentalConclusion/(?P<pk>\d+)/update/$', Update_view.ExperimentalConclusionUpdate, name='ExperimentalConclusionUpdate'),

    url(r'^ExperimentalProtocol/(?P<pk>\d+)/update/$', Update_view.ExperimentalProtocolUpdate, name='ExperimentalProtocolUpdate'),

    url(r'^ExperimenteeBias/(?P<pk>\d+)/update/$', Update_view.ExperimenteeBiasUpdate, name='ExperimenteeBiasUpdate'),

    url(r'^HawthorneEffect/(?P<pk>\d+)/update/$', Update_view.HawthorneEffectUpdate, name='HawthorneEffectUpdate'),

    url(r'^MortalityEffect/(?P<pk>\d+)/update/$', Update_view.MortalityEffectUpdate, name='MortalityEffectUpdate'),

    url(r'^ExperimenterBias/(?P<pk>\d+)/update/$', Update_view.ExperimenterBiasUpdate, name='ExperimenterBiasUpdate'),

    url(r'^TestingEffect/(?P<pk>\d+)/update/$', Update_view.TestingEffectUpdate, name='TestingEffectUpdate'),

    url(r'^ExplicitHypothesis/(?P<pk>\d+)/update/$', Update_view.ExplicitHypothesisUpdate, name='ExplicitHypothesisUpdate'),

    url(r'^ImplicitHypothesis/(?P<pk>\d+)/update/$', Update_view.ImplicitHypothesisUpdate, name='ImplicitHypothesisUpdate'),

    url(r'^FactorLevel/(?P<pk>\d+)/update/$', Update_view.FactorLevelUpdate, name='FactorLevelUpdate'),

    url(r'^FactorialDesign/(?P<pk>\d+)/update/$', Update_view.FactorialDesignUpdate, name='FactorialDesignUpdate'),

    url(r'^FalseNegative/(?P<pk>\d+)/update/$', Update_view.FalseNegativeUpdate, name='FalseNegativeUpdate'),

    url(r'^HypothesisAcceptanceMistake/(?P<pk>\d+)/update/$', Update_view.HypothesisAcceptanceMistakeUpdate, name='HypothesisAcceptanceMistakeUpdate'),

    url(r'^FalsePpositive/(?P<pk>\d+)/update/$', Update_view.FalsePpositiveUpdate, name='FalsePpositiveUpdate'),

    url(r'^FaultyComparison/(?P<pk>\d+)/update/$', Update_view.FaultyComparisonUpdate, name='FaultyComparisonUpdate'),

    url(r'^Formula/(?P<pk>\d+)/update/$', Update_view.FormulaUpdate, name='FormulaUpdate'),

    url(r'^GalileanExperiment/(?P<pk>\d+)/update/$', Update_view.GalileanExperimentUpdate, name='GalileanExperimentUpdate'),

    url(r'^HandTool/(?P<pk>\d+)/update/$', Update_view.HandToolUpdate, name='HandToolUpdate'),

    url(r'^Tool/(?P<pk>\d+)/update/$', Update_view.ToolUpdate, name='ToolUpdate'),

    url(r'^Hardware/(?P<pk>\d+)/update/$', Update_view.HardwareUpdate, name='HardwareUpdate'),

    url(r'^HistoryEffect/(?P<pk>\d+)/update/$', Update_view.HistoryEffectUpdate, name='HistoryEffectUpdate'),

    url(r'^MaturationEffect/(?P<pk>\d+)/update/$', Update_view.MaturationEffectUpdate, name='MaturationEffectUpdate'),

    url(r'^HypothesisdrivenExperiment/(?P<pk>\d+)/update/$', Update_view.HypothesisdrivenExperimentUpdate, name='HypothesisdrivenExperimentUpdate'),

    url(r'^HypothesisformingExperiment/(?P<pk>\d+)/update/$', Update_view.HypothesisformingExperimentUpdate, name='HypothesisformingExperimentUpdate'),

    url(r'^Image/(?P<pk>\d+)/update/$', Update_view.ImageUpdate, name='ImageUpdate'),

    url(r'^ImproperSampling/(?P<pk>\d+)/update/$', Update_view.ImproperSamplingUpdate, name='ImproperSamplingUpdate'),

    url(r'^IncompleteDataError/(?P<pk>\d+)/update/$', Update_view.IncompleteDataErrorUpdate, name='IncompleteDataErrorUpdate'),

    url(r'^IndependentVariable/(?P<pk>\d+)/update/$', Update_view.IndependentVariableUpdate, name='IndependentVariableUpdate'),

    url(r'^InferableVariable/(?P<pk>\d+)/update/$', Update_view.InferableVariableUpdate, name='InferableVariableUpdate'),

    url(r'^InternalStatus/(?P<pk>\d+)/update/$', Update_view.InternalStatusUpdate, name='InternalStatusUpdate'),

    url(r'^RestrictedStatus/(?P<pk>\d+)/update/$', Update_view.RestrictedStatusUpdate, name='RestrictedStatusUpdate'),

    url(r'^Journal/(?P<pk>\d+)/update/$', Update_view.JournalUpdate, name='JournalUpdate'),

    url(r'^LinearModel/(?P<pk>\d+)/update/$', Update_view.LinearModelUpdate, name='LinearModelUpdate'),

    url(r'^NonlinearModel/(?P<pk>\d+)/update/$', Update_view.NonlinearModelUpdate, name='NonlinearModelUpdate'),

    url(r'^LogicalModelRepresentation/(?P<pk>\d+)/update/$', Update_view.LogicalModelRepresentationUpdate, name='LogicalModelRepresentationUpdate'),

    url(r'^Machine/(?P<pk>\d+)/update/$', Update_view.MachineUpdate, name='MachineUpdate'),

    url(r'^MachineTool/(?P<pk>\d+)/update/$', Update_view.MachineToolUpdate, name='MachineToolUpdate'),

    url(r'^Magazine/(?P<pk>\d+)/update/$', Update_view.MagazineUpdate, name='MagazineUpdate'),

    url(r'^Materials/(?P<pk>\d+)/update/$', Update_view.MaterialsUpdate, name='MaterialsUpdate'),

    url(r'^MathematicalModelRepresentation/(?P<pk>\d+)/update/$', Update_view.MathematicalModelRepresentationUpdate, name='MathematicalModelRepresentationUpdate'),

    url(r'^MetabolomicExperiment/(?P<pk>\d+)/update/$', Update_view.MetabolomicExperimentUpdate, name='MetabolomicExperimentUpdate'),

    url(r'^MethodsComparison/(?P<pk>\d+)/update/$', Update_view.MethodsComparisonUpdate, name='MethodsComparisonUpdate'),

    url(r'^SubjectsComparison/(?P<pk>\d+)/update/$', Update_view.SubjectsComparisonUpdate, name='SubjectsComparisonUpdate'),

    url(r'^MicroarrayExperiment/(?P<pk>\d+)/update/$', Update_view.MicroarrayExperimentUpdate, name='MicroarrayExperimentUpdate'),

    url(r'^MultifactorExperiment/(?P<pk>\d+)/update/$', Update_view.MultifactorExperimentUpdate, name='MultifactorExperimentUpdate'),

    url(r'^OnefactorExperiment/(?P<pk>\d+)/update/$', Update_view.OnefactorExperimentUpdate, name='OnefactorExperimentUpdate'),

    url(r'^TwofactorExperiment/(?P<pk>\d+)/update/$', Update_view.TwofactorExperimentUpdate, name='TwofactorExperimentUpdate'),

    url(r'^NaturalLanguage/(?P<pk>\d+)/update/$', Update_view.NaturalLanguageUpdate, name='NaturalLanguageUpdate'),

    url(r'^NonRestrictedStatus/(?P<pk>\d+)/update/$', Update_view.NonRestrictedStatusUpdate, name='NonRestrictedStatusUpdate'),

    url(r'^NormalizationStrategy/(?P<pk>\d+)/update/$', Update_view.NormalizationStrategyUpdate, name='NormalizationStrategyUpdate'),

    url(r'^ObjectMethod/(?P<pk>\d+)/update/$', Update_view.ObjectMethodUpdate, name='ObjectMethodUpdate'),

    url(r'^ObjectOfAction/(?P<pk>\d+)/update/$', Update_view.ObjectOfActionUpdate, name='ObjectOfActionUpdate'),

    url(r'^ObservableVariable/(?P<pk>\d+)/update/$', Update_view.ObservableVariableUpdate, name='ObservableVariableUpdate'),

    url(r'^Paper/(?P<pk>\d+)/update/$', Update_view.PaperUpdate, name='PaperUpdate'),

    url(r'^ParticlePhysicsExperiment/(?P<pk>\d+)/update/$', Update_view.ParticlePhysicsExperimentUpdate, name='ParticlePhysicsExperimentUpdate'),

    url(r'^PresentingSampling/(?P<pk>\d+)/update/$', Update_view.PresentingSamplingUpdate, name='PresentingSamplingUpdate'),

    url(r'^Proceedings/(?P<pk>\d+)/update/$', Update_view.ProceedingsUpdate, name='ProceedingsUpdate'),

    url(r'^Program/(?P<pk>\d+)/update/$', Update_view.ProgramUpdate, name='ProgramUpdate'),

    url(r'^ProteomicExperiment/(?P<pk>\d+)/update/$', Update_view.ProteomicExperimentUpdate, name='ProteomicExperimentUpdate'),

    url(r'^Provider/(?P<pk>\d+)/update/$', Update_view.ProviderUpdate, name='ProviderUpdate'),

    url(r'^PublicAcademicStatus/(?P<pk>\d+)/update/$', Update_view.PublicAcademicStatusUpdate, name='PublicAcademicStatusUpdate'),

    url(r'^PublicStatus/(?P<pk>\d+)/update/$', Update_view.PublicStatusUpdate, name='PublicStatusUpdate'),

    url(r'^RandomError/(?P<pk>\d+)/update/$', Update_view.RandomErrorUpdate, name='RandomErrorUpdate'),

    url(r'^RandomSampling/(?P<pk>\d+)/update/$', Update_view.RandomSamplingUpdate, name='RandomSamplingUpdate'),

    url(r'^RecommendationSatus/(?P<pk>\d+)/update/$', Update_view.RecommendationSatusUpdate, name='RecommendationSatusUpdate'),

    url(r'^RecordingAction/(?P<pk>\d+)/update/$', Update_view.RecordingActionUpdate, name='RecordingActionUpdate'),

    url(r'^Region/(?P<pk>\d+)/update/$', Update_view.RegionUpdate, name='RegionUpdate'),

    url(r'^RepresenationalMedium/(?P<pk>\d+)/update/$', Update_view.RepresenationalMediumUpdate, name='RepresenationalMediumUpdate'),

    url(r'^SUMOSynonym/(?P<pk>\d+)/update/$', Update_view.SUMOSynonymUpdate, name='SUMOSynonymUpdate'),

    url(r'^SampleForming/(?P<pk>\d+)/update/$', Update_view.SampleFormingUpdate, name='SampleFormingUpdate'),

    url(r'^SequentialDesign/(?P<pk>\d+)/update/$', Update_view.SequentialDesignUpdate, name='SequentialDesignUpdate'),

    url(r'^Software/(?P<pk>\d+)/update/$', Update_view.SoftwareUpdate, name='SoftwareUpdate'),

    url(r'^Sound/(?P<pk>\d+)/update/$', Update_view.SoundUpdate, name='SoundUpdate'),

    url(r'^SplitSamples/(?P<pk>\d+)/update/$', Update_view.SplitSamplesUpdate, name='SplitSamplesUpdate'),

    url(r'^SymmetricalMatching/(?P<pk>\d+)/update/$', Update_view.SymmetricalMatchingUpdate, name='SymmetricalMatchingUpdate'),

    url(r'^StatisticalError/(?P<pk>\d+)/update/$', Update_view.StatisticalErrorUpdate, name='StatisticalErrorUpdate'),

    url(r'^StratifiedSampling/(?P<pk>\d+)/update/$', Update_view.StratifiedSamplingUpdate, name='StratifiedSamplingUpdate'),

    url(r'^SubjectOfAction/(?P<pk>\d+)/update/$', Update_view.SubjectOfActionUpdate, name='SubjectOfActionUpdate'),

    url(r'^SubjectOfExperiment/(?P<pk>\d+)/update/$', Update_view.SubjectOfExperimentUpdate, name='SubjectOfExperimentUpdate'),

    url(r'^Submitter/(?P<pk>\d+)/update/$', Update_view.SubmitterUpdate, name='SubmitterUpdate'),

    url(r'^Summary/(?P<pk>\d+)/update/$', Update_view.SummaryUpdate, name='SummaryUpdate'),

    url(r'^SystematicError/(?P<pk>\d+)/update/$', Update_view.SystematicErrorUpdate, name='SystematicErrorUpdate'),

    url(r'^TechnicalReport/(?P<pk>\d+)/update/$', Update_view.TechnicalReportUpdate, name='TechnicalReportUpdate'),

    url(r'^TimeDuration/(?P<pk>\d+)/update/$', Update_view.TimeDurationUpdate, name='TimeDurationUpdate'),

    url(r'^TimeInterval/(?P<pk>\d+)/update/$', Update_view.TimeIntervalUpdate, name='TimeIntervalUpdate'),

    url(r'^URL/(?P<pk>\d+)/update/$', Update_view.URLUpdate, name='URLUpdate'),


    url(r'^Thing/(?P<pk>\d+)/delete/$', Delete_view.ThingDelete, name='ThingDelete'),

    url(r'^RepresentationForm/(?P<pk>\d+)/delete/$', Delete_view.RepresentationFormDelete, name='RepresentationFormDelete'),

    url(r'^Artifact/(?P<pk>\d+)/delete/$', Delete_view.ArtifactDelete, name='ArtifactDelete'),

    url(r'^SynonymousExternalConcept/(?P<pk>\d+)/delete/$', Delete_view.SynonymousExternalConceptDelete, name='SynonymousExternalConceptDelete'),

    url(r'^Name/(?P<pk>\d+)/delete/$', Delete_view.NameDelete, name='NameDelete'),

    url(r'^ScientificExperiment/(?P<pk>\d+)/delete/$', Delete_view.ScientificExperimentDelete, name='ScientificExperimentDelete'),

    url(r'^ExperimentalDesign/(?P<pk>\d+)/delete/$', Delete_view.ExperimentalDesignDelete, name='ExperimentalDesignDelete'),

    url(r'^AdminInfoExperiment/(?P<pk>\d+)/delete/$', Delete_view.AdminInfoExperimentDelete, name='AdminInfoExperimentDelete'),

    url(r'^IDExperiment/(?P<pk>\d+)/delete/$', Delete_view.IDExperimentDelete, name='IDExperimentDelete'),

    url(r'^DomainOfExperiment/(?P<pk>\d+)/delete/$', Delete_view.DomainOfExperimentDelete, name='DomainOfExperimentDelete'),

    url(r'^ExperimentalObservation/(?P<pk>\d+)/delete/$', Delete_view.ExperimentalObservationDelete, name='ExperimentalObservationDelete'),

    url(r'^ResultsInterpretation/(?P<pk>\d+)/delete/$', Delete_view.ResultsInterpretationDelete, name='ResultsInterpretationDelete'),

    url(r'^ExperimentalAction/(?P<pk>\d+)/delete/$', Delete_view.ExperimentalActionDelete, name='ExperimentalActionDelete'),

    url(r'^ActionName/(?P<pk>\d+)/delete/$', Delete_view.ActionNameDelete, name='ActionNameDelete'),

    url(r'^ActionComplexity/(?P<pk>\d+)/delete/$', Delete_view.ActionComplexityDelete, name='ActionComplexityDelete'),

    url(r'^Human/(?P<pk>\d+)/delete/$', Delete_view.HumanDelete, name='HumanDelete'),

    url(r'^Organization/(?P<pk>\d+)/delete/$', Delete_view.OrganizationDelete, name='OrganizationDelete'),

    url(r'^AdminInfoAuthor/(?P<pk>\d+)/delete/$', Delete_view.AdminInfoAuthorDelete, name='AdminInfoAuthorDelete'),

    url(r'^AdminInfoObjectOfExperiment/(?P<pk>\d+)/delete/$', Delete_view.AdminInfoObjectOfExperimentDelete, name='AdminInfoObjectOfExperimentDelete'),

    url(r'^AdminInfoProvider/(?P<pk>\d+)/delete/$', Delete_view.AdminInfoProviderDelete, name='AdminInfoProviderDelete'),

    url(r'^AdminInfoSubjectOfExperiment/(?P<pk>\d+)/delete/$', Delete_view.AdminInfoSubjectOfExperimentDelete, name='AdminInfoSubjectOfExperimentDelete'),

    url(r'^AdminInfoSubmitter/(?P<pk>\d+)/delete/$', Delete_view.AdminInfoSubmitterDelete, name='AdminInfoSubmitterDelete'),

    url(r'^AdminInfoUser/(?P<pk>\d+)/delete/$', Delete_view.AdminInfoUserDelete, name='AdminInfoUserDelete'),

    url(r'^ExperimentalMethod/(?P<pk>\d+)/delete/$', Delete_view.ExperimentalMethodDelete, name='ExperimentalMethodDelete'),

    url(r'^MethodApplicabilityCondition/(?P<pk>\d+)/delete/$', Delete_view.MethodApplicabilityConditionDelete, name='MethodApplicabilityConditionDelete'),

    url(r'^StandardOperatingProcedure/(?P<pk>\d+)/delete/$', Delete_view.StandardOperatingProcedureDelete, name='StandardOperatingProcedureDelete'),

    url(r'^Author/(?P<pk>\d+)/delete/$', Delete_view.AuthorDelete, name='AuthorDelete'),

    url(r'^ClassificationOfDomains/(?P<pk>\d+)/delete/$', Delete_view.ClassificationOfDomainsDelete, name='ClassificationOfDomainsDelete'),

    url(r'^ClassificationOfExperiments/(?P<pk>\d+)/delete/$', Delete_view.ClassificationOfExperimentsDelete, name='ClassificationOfExperimentsDelete'),

    url(r'^ExperimentalHypothesis/(?P<pk>\d+)/delete/$', Delete_view.ExperimentalHypothesisDelete, name='ExperimentalHypothesisDelete'),

    url(r'^HypothesisComplexity/(?P<pk>\d+)/delete/$', Delete_view.HypothesisComplexityDelete, name='HypothesisComplexityDelete'),

    url(r'^Representation/(?P<pk>\d+)/delete/$', Delete_view.RepresentationDelete, name='RepresentationDelete'),

    url(r'^Proposition/(?P<pk>\d+)/delete/$', Delete_view.PropositionDelete, name='PropositionDelete'),

    url(r'^ExperimentalEquipment/(?P<pk>\d+)/delete/$', Delete_view.ExperimentalEquipmentDelete, name='ExperimentalEquipmentDelete'),

    url(r'^TechnicalDescription/(?P<pk>\d+)/delete/$', Delete_view.TechnicalDescriptionDelete, name='TechnicalDescriptionDelete'),

    url(r'^ExperimentalResults/(?P<pk>\d+)/delete/$', Delete_view.ExperimentalResultsDelete, name='ExperimentalResultsDelete'),

    url(r'^ObservationalError/(?P<pk>\d+)/delete/$', Delete_view.ObservationalErrorDelete, name='ObservationalErrorDelete'),

    url(r'^ResultError/(?P<pk>\d+)/delete/$', Delete_view.ResultErrorDelete, name='ResultErrorDelete'),

    url(r'^ResultsEvaluation/(?P<pk>\d+)/delete/$', Delete_view.ResultsEvaluationDelete, name='ResultsEvaluationDelete'),

    url(r'^ScientificInvestigation/(?P<pk>\d+)/delete/$', Delete_view.ScientificInvestigationDelete, name='ScientificInvestigationDelete'),

    url(r'^ExperimentalModel/(?P<pk>\d+)/delete/$', Delete_view.ExperimentalModelDelete, name='ExperimentalModelDelete'),

    url(r'^ExperimentalFactor/(?P<pk>\d+)/delete/$', Delete_view.ExperimentalFactorDelete, name='ExperimentalFactorDelete'),

    url(r'^ExperimentalTechnology/(?P<pk>\d+)/delete/$', Delete_view.ExperimentalTechnologyDelete, name='ExperimentalTechnologyDelete'),

    url(r'^ExperimentalRequirements/(?P<pk>\d+)/delete/$', Delete_view.ExperimentalRequirementsDelete, name='ExperimentalRequirementsDelete'),

    url(r'^HypothesisExplicitness/(?P<pk>\d+)/delete/$', Delete_view.HypothesisExplicitnessDelete, name='HypothesisExplicitnessDelete'),

    url(r'^LinguisticExpression/(?P<pk>\d+)/delete/$', Delete_view.LinguisticExpressionDelete, name='LinguisticExpressionDelete'),

    url(r'^FactRejectResearchHypothesis/(?P<pk>\d+)/delete/$', Delete_view.FactRejectResearchHypothesisDelete, name='FactRejectResearchHypothesisDelete'),

    url(r'^FactSupportResearchHypothesis/(?P<pk>\d+)/delete/$', Delete_view.FactSupportResearchHypothesisDelete, name='FactSupportResearchHypothesisDelete'),

    url(r'^Number/(?P<pk>\d+)/delete/$', Delete_view.NumberDelete, name='NumberDelete'),

    url(r'^GeneralityOfHypothesis/(?P<pk>\d+)/delete/$', Delete_view.GeneralityOfHypothesisDelete, name='GeneralityOfHypothesisDelete'),

    url(r'^ActionGoal/(?P<pk>\d+)/delete/$', Delete_view.ActionGoalDelete, name='ActionGoalDelete'),

    url(r'^ExperimentalGoal/(?P<pk>\d+)/delete/$', Delete_view.ExperimentalGoalDelete, name='ExperimentalGoalDelete'),

    url(r'^MethodGoal/(?P<pk>\d+)/delete/$', Delete_view.MethodGoalDelete, name='MethodGoalDelete'),

    url(r'^HypothesisFormation/(?P<pk>\d+)/delete/$', Delete_view.HypothesisFormationDelete, name='HypothesisFormationDelete'),

    url(r'^HypothesisRepresentation/(?P<pk>\d+)/delete/$', Delete_view.HypothesisRepresentationDelete, name='HypothesisRepresentationDelete'),

    url(r'^InformationGethering/(?P<pk>\d+)/delete/$', Delete_view.InformationGetheringDelete, name='InformationGetheringDelete'),

    url(r'^Sample/(?P<pk>\d+)/delete/$', Delete_view.SampleDelete, name='SampleDelete'),

    url(r'^Variability/(?P<pk>\d+)/delete/$', Delete_view.VariabilityDelete, name='VariabilityDelete'),

    url(r'^TimePoint/(?P<pk>\d+)/delete/$', Delete_view.TimePointDelete, name='TimePointDelete'),

    url(r'^User/(?P<pk>\d+)/delete/$', Delete_view.UserDelete, name='UserDelete'),

    url(r'^LoginName/(?P<pk>\d+)/delete/$', Delete_view.LoginNameDelete, name='LoginNameDelete'),

    url(r'^DomainModel/(?P<pk>\d+)/delete/$', Delete_view.DomainModelDelete, name='DomainModelDelete'),

    url(r'^GroupExperimentalObject/(?P<pk>\d+)/delete/$', Delete_view.GroupExperimentalObjectDelete, name='GroupExperimentalObjectDelete'),

    url(r'^ObjectOfExperiment/(?P<pk>\d+)/delete/$', Delete_view.ObjectOfExperimentDelete, name='ObjectOfExperimentDelete'),

    url(r'^ClassificationByModel/(?P<pk>\d+)/delete/$', Delete_view.ClassificationByModelDelete, name='ClassificationByModelDelete'),

    url(r'^StatisticsCharacteristic/(?P<pk>\d+)/delete/$', Delete_view.StatisticsCharacteristicDelete, name='StatisticsCharacteristicDelete'),

    url(r'^ParentGroup/(?P<pk>\d+)/delete/$', Delete_view.ParentGroupDelete, name='ParentGroupDelete'),

    url(r'^Password/(?P<pk>\d+)/delete/$', Delete_view.PasswordDelete, name='PasswordDelete'),

    url(r'^ProcedureExecuteExperiment/(?P<pk>\d+)/delete/$', Delete_view.ProcedureExecuteExperimentDelete, name='ProcedureExecuteExperimentDelete'),

    url(r'^PlanOfExperimentalActions/(?P<pk>\d+)/delete/$', Delete_view.PlanOfExperimentalActionsDelete, name='PlanOfExperimentalActionsDelete'),

    url(r'^PoblemAnalysis/(?P<pk>\d+)/delete/$', Delete_view.PoblemAnalysisDelete, name='PoblemAnalysisDelete'),

    url(r'^SubjectQualification/(?P<pk>\d+)/delete/$', Delete_view.SubjectQualificationDelete, name='SubjectQualificationDelete'),

    url(r'^BiblioReference/(?P<pk>\d+)/delete/$', Delete_view.BiblioReferenceDelete, name='BiblioReferenceDelete'),

    url(r'^Model/(?P<pk>\d+)/delete/$', Delete_view.ModelDelete, name='ModelDelete'),

    url(r'^ModelRepresentation/(?P<pk>\d+)/delete/$', Delete_view.ModelRepresentationDelete, name='ModelRepresentationDelete'),

    url(r'^RepresentationExperimentalExecutionProcedure/(?P<pk>\d+)/delete/$', Delete_view.RepresentationExperimentalExecutionProcedureDelete, name='RepresentationExperimentalExecutionProcedureDelete'),

    url(r'^RepresentationExperimentalGoal/(?P<pk>\d+)/delete/$', Delete_view.RepresentationExperimentalGoalDelete, name='RepresentationExperimentalGoalDelete'),

    url(r'^SampleRepresentativeness/(?P<pk>\d+)/delete/$', Delete_view.SampleRepresentativenessDelete, name='SampleRepresentativenessDelete'),

    url(r'^SampleSize/(?P<pk>\d+)/delete/$', Delete_view.SampleSizeDelete, name='SampleSizeDelete'),

    url(r'^SamplingMethod/(?P<pk>\d+)/delete/$', Delete_view.SamplingMethodDelete, name='SamplingMethodDelete'),

    url(r'^SystematicSampling/(?P<pk>\d+)/delete/$', Delete_view.SystematicSamplingDelete, name='SystematicSamplingDelete'),

    url(r'^SamplingRule/(?P<pk>\d+)/delete/$', Delete_view.SamplingRuleDelete, name='SamplingRuleDelete'),

    url(r'^ExperimentalStandard/(?P<pk>\d+)/delete/$', Delete_view.ExperimentalStandardDelete, name='ExperimentalStandardDelete'),

    url(r'^Error/(?P<pk>\d+)/delete/$', Delete_view.ErrorDelete, name='ErrorDelete'),

    url(r'^Dispersion/(?P<pk>\d+)/delete/$', Delete_view.DispersionDelete, name='DispersionDelete'),

    url(r'^LevelOfSignificance/(?P<pk>\d+)/delete/$', Delete_view.LevelOfSignificanceDelete, name='LevelOfSignificanceDelete'),

    url(r'^StatusExperimentalDocument/(?P<pk>\d+)/delete/$', Delete_view.StatusExperimentalDocumentDelete, name='StatusExperimentalDocumentDelete'),

    url(r'^ExperimentalDesignStrategy/(?P<pk>\d+)/delete/$', Delete_view.ExperimentalDesignStrategyDelete, name='ExperimentalDesignStrategyDelete'),

    url(r'^ExperimentalSubgoal/(?P<pk>\d+)/delete/$', Delete_view.ExperimentalSubgoalDelete, name='ExperimentalSubgoalDelete'),

    url(r'^SubjectMethod/(?P<pk>\d+)/delete/$', Delete_view.SubjectMethodDelete, name='SubjectMethodDelete'),

    url(r'^BaconianExperiment/(?P<pk>\d+)/delete/$', Delete_view.BaconianExperimentDelete, name='BaconianExperimentDelete'),

    url(r'^TargetVariable/(?P<pk>\d+)/delete/$', Delete_view.TargetVariableDelete, name='TargetVariableDelete'),

    url(r'^Title/(?P<pk>\d+)/delete/$', Delete_view.TitleDelete, name='TitleDelete'),

    url(r'^ValueEstimate/(?P<pk>\d+)/delete/$', Delete_view.ValueEstimateDelete, name='ValueEstimateDelete'),

    url(r'^ValueOfVariable/(?P<pk>\d+)/delete/$', Delete_view.ValueOfVariableDelete, name='ValueOfVariableDelete'),

    url(r'^Role/(?P<pk>\d+)/delete/$', Delete_view.RoleDelete, name='RoleDelete'),

    url(r'^CorpuscularObject/(?P<pk>\d+)/delete/$', Delete_view.CorpuscularObjectDelete, name='CorpuscularObjectDelete'),

    url(r'^AttributeRole/(?P<pk>\d+)/delete/$', Delete_view.AttributeRoleDelete, name='AttributeRoleDelete'),

    url(r'^Attribute/(?P<pk>\d+)/delete/$', Delete_view.AttributeDelete, name='AttributeDelete'),

    url(r'^Procedure/(?P<pk>\d+)/delete/$', Delete_view.ProcedureDelete, name='ProcedureDelete'),

    url(r'^AdministrativeInformation/(?P<pk>\d+)/delete/$', Delete_view.AdministrativeInformationDelete, name='AdministrativeInformationDelete'),

    url(r'^TitleExperiment/(?P<pk>\d+)/delete/$', Delete_view.TitleExperimentDelete, name='TitleExperimentDelete'),

    url(r'^NameEquipment/(?P<pk>\d+)/delete/$', Delete_view.NameEquipmentDelete, name='NameEquipmentDelete'),

    url(r'^PersonalName/(?P<pk>\d+)/delete/$', Delete_view.PersonalNameDelete, name='PersonalNameDelete'),

    url(r'^FieldOfStudy/(?P<pk>\d+)/delete/$', Delete_view.FieldOfStudyDelete, name='FieldOfStudyDelete'),

    url(r'^RelatedDomain/(?P<pk>\d+)/delete/$', Delete_view.RelatedDomainDelete, name='RelatedDomainDelete'),

    url(r'^ProductRole/(?P<pk>\d+)/delete/$', Delete_view.ProductRoleDelete, name='ProductRoleDelete'),

    url(r'^ScientificTask/(?P<pk>\d+)/delete/$', Delete_view.ScientificTaskDelete, name='ScientificTaskDelete'),

    url(r'^ExecutionOfExperiment/(?P<pk>\d+)/delete/$', Delete_view.ExecutionOfExperimentDelete, name='ExecutionOfExperimentDelete'),

    url(r'^CorporateName/(?P<pk>\d+)/delete/$', Delete_view.CorporateNameDelete, name='CorporateNameDelete'),

    url(r'^AttributeOfAction/(?P<pk>\d+)/delete/$', Delete_view.AttributeOfActionDelete, name='AttributeOfActionDelete'),

    url(r'^SentientAgent/(?P<pk>\d+)/delete/$', Delete_view.SentientAgentDelete, name='SentientAgentDelete'),

    url(r'^Robot/(?P<pk>\d+)/delete/$', Delete_view.RobotDelete, name='RobotDelete'),

    url(r'^Group/(?P<pk>\d+)/delete/$', Delete_view.GroupDelete, name='GroupDelete'),

    url(r'^Entity/(?P<pk>\d+)/delete/$', Delete_view.EntityDelete, name='EntityDelete'),

    url(r'^ResearchMethod/(?P<pk>\d+)/delete/$', Delete_view.ResearchMethodDelete, name='ResearchMethodDelete'),

    url(r'^Requirements/(?P<pk>\d+)/delete/$', Delete_view.RequirementsDelete, name='RequirementsDelete'),

    url(r'^AuthorSOP/(?P<pk>\d+)/delete/$', Delete_view.AuthorSOPDelete, name='AuthorSOPDelete'),

    url(r'^SubjectRole/(?P<pk>\d+)/delete/$', Delete_view.SubjectRoleDelete, name='SubjectRoleDelete'),

    url(r'^Classification/(?P<pk>\d+)/delete/$', Delete_view.ClassificationDelete, name='ClassificationDelete'),

    url(r'^Fact/(?P<pk>\d+)/delete/$', Delete_view.FactDelete, name='FactDelete'),

    url(r'^ModelAssumption/(?P<pk>\d+)/delete/$', Delete_view.ModelAssumptionDelete, name='ModelAssumptionDelete'),

    url(r'^Variable/(?P<pk>\d+)/delete/$', Delete_view.VariableDelete, name='VariableDelete'),

    url(r'^AttributeOfHypothesis/(?P<pk>\d+)/delete/$', Delete_view.AttributeOfHypothesisDelete, name='AttributeOfHypothesisDelete'),

    url(r'^ContentBearingObject/(?P<pk>\d+)/delete/$', Delete_view.ContentBearingObjectDelete, name='ContentBearingObjectDelete'),

    url(r'^Abstract/(?P<pk>\d+)/delete/$', Delete_view.AbstractDelete, name='AbstractDelete'),

    url(r'^Quantity/(?P<pk>\d+)/delete/$', Delete_view.QuantityDelete, name='QuantityDelete'),

    url(r'^Relation/(?P<pk>\d+)/delete/$', Delete_view.RelationDelete, name='RelationDelete'),

    url(r'^ProcessrelatedRole/(?P<pk>\d+)/delete/$', Delete_view.ProcessrelatedRoleDelete, name='ProcessrelatedRoleDelete'),

    url(r'^ContentBearingPhysical/(?P<pk>\d+)/delete/$', Delete_view.ContentBearingPhysicalDelete, name='ContentBearingPhysicalDelete'),

    url(r'^PhysicalQuantity/(?P<pk>\d+)/delete/$', Delete_view.PhysicalQuantityDelete, name='PhysicalQuantityDelete'),

    url(r'^Goal/(?P<pk>\d+)/delete/$', Delete_view.GoalDelete, name='GoalDelete'),

    url(r'^RepresentationExperimentalObservation/(?P<pk>\d+)/delete/$', Delete_view.RepresentationExperimentalObservationDelete, name='RepresentationExperimentalObservationDelete'),

    url(r'^RepresentationExperimentalResults/(?P<pk>\d+)/delete/$', Delete_view.RepresentationExperimentalResultsDelete, name='RepresentationExperimentalResultsDelete'),

    url(r'^AttributeGroup/(?P<pk>\d+)/delete/$', Delete_view.AttributeGroupDelete, name='AttributeGroupDelete'),

    url(r'^TimePosition/(?P<pk>\d+)/delete/$', Delete_view.TimePositionDelete, name='TimePositionDelete'),

    url(r'^ActorRole/(?P<pk>\d+)/delete/$', Delete_view.ActorRoleDelete, name='ActorRoleDelete'),

    url(r'^DocumentStage/(?P<pk>\d+)/delete/$', Delete_view.DocumentStageDelete, name='DocumentStageDelete'),

    url(r'^PermissionStatus/(?P<pk>\d+)/delete/$', Delete_view.PermissionStatusDelete, name='PermissionStatusDelete'),

    url(r'^Plan/(?P<pk>\d+)/delete/$', Delete_view.PlanDelete, name='PlanDelete'),

    url(r'^Reference/(?P<pk>\d+)/delete/$', Delete_view.ReferenceDelete, name='ReferenceDelete'),

    url(r'^AttributeSample/(?P<pk>\d+)/delete/$', Delete_view.AttributeSampleDelete, name='AttributeSampleDelete'),

    url(r'^ExperimentalRule/(?P<pk>\d+)/delete/$', Delete_view.ExperimentalRuleDelete, name='ExperimentalRuleDelete'),

    url(r'^Robustness/(?P<pk>\d+)/delete/$', Delete_view.RobustnessDelete, name='RobustnessDelete'),

    url(r'^Validity/(?P<pk>\d+)/delete/$', Delete_view.ValidityDelete, name='ValidityDelete'),

    url(r'^AttributeOfDocument/(?P<pk>\d+)/delete/$', Delete_view.AttributeOfDocumentDelete, name='AttributeOfDocumentDelete'),

    url(r'^PhysicalExperiment/(?P<pk>\d+)/delete/$', Delete_view.PhysicalExperimentDelete, name='PhysicalExperimentDelete'),

    url(r'^ComputationalData/(?P<pk>\d+)/delete/$', Delete_view.ComputationalDataDelete, name='ComputationalDataDelete'),

    url(r'^Predicate/(?P<pk>\d+)/delete/$', Delete_view.PredicateDelete, name='PredicateDelete'),

    url(r'^SelfConnectedObject/(?P<pk>\d+)/delete/$', Delete_view.SelfConnectedObjectDelete, name='SelfConnectedObjectDelete'),

    url(r'^ScientificActivity/(?P<pk>\d+)/delete/$', Delete_view.ScientificActivityDelete, name='ScientificActivityDelete'),

    url(r'^FormingClassificationSystem/(?P<pk>\d+)/delete/$', Delete_view.FormingClassificationSystemDelete, name='FormingClassificationSystemDelete'),

    url(r'^HypothesisForming/(?P<pk>\d+)/delete/$', Delete_view.HypothesisFormingDelete, name='HypothesisFormingDelete'),

    url(r'^InterpretingResult/(?P<pk>\d+)/delete/$', Delete_view.InterpretingResultDelete, name='InterpretingResultDelete'),

    url(r'^ProcessProblemAnalysis/(?P<pk>\d+)/delete/$', Delete_view.ProcessProblemAnalysisDelete, name='ProcessProblemAnalysisDelete'),

    url(r'^ResultEvaluating/(?P<pk>\d+)/delete/$', Delete_view.ResultEvaluatingDelete, name='ResultEvaluatingDelete'),

    url(r'^AttributeOfModel/(?P<pk>\d+)/delete/$', Delete_view.AttributeOfModelDelete, name='AttributeOfModelDelete'),

    url(r'^AttributeOfVariable/(?P<pk>\d+)/delete/$', Delete_view.AttributeOfVariableDelete, name='AttributeOfVariableDelete'),

    url(r'^Agent/(?P<pk>\d+)/delete/$', Delete_view.AgentDelete, name='AgentDelete'),

    url(r'^Collection/(?P<pk>\d+)/delete/$', Delete_view.CollectionDelete, name='CollectionDelete'),

    url(r'^EperimentalDesignTask/(?P<pk>\d+)/delete/$', Delete_view.EperimentalDesignTaskDelete, name='EperimentalDesignTaskDelete'),

    url(r'^Physical/(?P<pk>\d+)/delete/$', Delete_view.PhysicalDelete, name='PhysicalDelete'),

    url(r'^Object/(?P<pk>\d+)/delete/$', Delete_view.ObjectDelete, name='ObjectDelete'),

    url(r'^Process/(?P<pk>\d+)/delete/$', Delete_view.ProcessDelete, name='ProcessDelete'),

    url(r'^TaskRole/(?P<pk>\d+)/delete/$', Delete_view.TaskRoleDelete, name='TaskRoleDelete'),

    url(r'^TimeMeasure/(?P<pk>\d+)/delete/$', Delete_view.TimeMeasureDelete, name='TimeMeasureDelete'),

    url(r'^ActionrelatedRole/(?P<pk>\d+)/delete/$', Delete_view.ActionrelatedRoleDelete, name='ActionrelatedRoleDelete'),

    url(r'^AbductiveHypothesis/(?P<pk>\d+)/delete/$', Delete_view.AbductiveHypothesisDelete, name='AbductiveHypothesisDelete'),

    url(r'^InductiveHypothesis/(?P<pk>\d+)/delete/$', Delete_view.InductiveHypothesisDelete, name='InductiveHypothesisDelete'),

    url(r'^Adequacy/(?P<pk>\d+)/delete/$', Delete_view.AdequacyDelete, name='AdequacyDelete'),

    url(r'^AlternativeHypothesis/(?P<pk>\d+)/delete/$', Delete_view.AlternativeHypothesisDelete, name='AlternativeHypothesisDelete'),

    url(r'^NullHypothesis/(?P<pk>\d+)/delete/$', Delete_view.NullHypothesisDelete, name='NullHypothesisDelete'),

    url(r'^ResearchHypothesis/(?P<pk>\d+)/delete/$', Delete_view.ResearchHypothesisDelete, name='ResearchHypothesisDelete'),

    url(r'^Article/(?P<pk>\d+)/delete/$', Delete_view.ArticleDelete, name='ArticleDelete'),

    url(r'^Text/(?P<pk>\d+)/delete/$', Delete_view.TextDelete, name='TextDelete'),

    url(r'^ArtificialLanguage/(?P<pk>\d+)/delete/$', Delete_view.ArtificialLanguageDelete, name='ArtificialLanguageDelete'),

    url(r'^Language/(?P<pk>\d+)/delete/$', Delete_view.LanguageDelete, name='LanguageDelete'),

    url(r'^HumanLanguage/(?P<pk>\d+)/delete/$', Delete_view.HumanLanguageDelete, name='HumanLanguageDelete'),

    url(r'^Sentence/(?P<pk>\d+)/delete/$', Delete_view.SentenceDelete, name='SentenceDelete'),

    url(r'^AtomicAction/(?P<pk>\d+)/delete/$', Delete_view.AtomicActionDelete, name='AtomicActionDelete'),

    url(r'^ComplexAction/(?P<pk>\d+)/delete/$', Delete_view.ComplexActionDelete, name='ComplexActionDelete'),

    url(r'^AuthorProtocol/(?P<pk>\d+)/delete/$', Delete_view.AuthorProtocolDelete, name='AuthorProtocolDelete'),

    url(r'^Book/(?P<pk>\d+)/delete/$', Delete_view.BookDelete, name='BookDelete'),

    url(r'^CalculableVariable/(?P<pk>\d+)/delete/$', Delete_view.CalculableVariableDelete, name='CalculableVariableDelete'),

    url(r'^CapacityRequirements/(?P<pk>\d+)/delete/$', Delete_view.CapacityRequirementsDelete, name='CapacityRequirementsDelete'),

    url(r'^EnvironmentalRequirements/(?P<pk>\d+)/delete/$', Delete_view.EnvironmentalRequirementsDelete, name='EnvironmentalRequirementsDelete'),

    url(r'^FinancialRequirements/(?P<pk>\d+)/delete/$', Delete_view.FinancialRequirementsDelete, name='FinancialRequirementsDelete'),

    url(r'^ClassificationByDomain/(?P<pk>\d+)/delete/$', Delete_view.ClassificationByDomainDelete, name='ClassificationByDomainDelete'),

    url(r'^Classifying/(?P<pk>\d+)/delete/$', Delete_view.ClassifyingDelete, name='ClassifyingDelete'),

    url(r'^DesigningExperiment/(?P<pk>\d+)/delete/$', Delete_view.DesigningExperimentDelete, name='DesigningExperimentDelete'),

    url(r'^CoarseError/(?P<pk>\d+)/delete/$', Delete_view.CoarseErrorDelete, name='CoarseErrorDelete'),

    url(r'^MeasurementError/(?P<pk>\d+)/delete/$', Delete_view.MeasurementErrorDelete, name='MeasurementErrorDelete'),

    url(r'^ComparisonControl_TargetGroups/(?P<pk>\d+)/delete/$', Delete_view.ComparisonControl_TargetGroupsDelete, name='ComparisonControl_TargetGroupsDelete'),

    url(r'^PairedComparison/(?P<pk>\d+)/delete/$', Delete_view.PairedComparisonDelete, name='PairedComparisonDelete'),

    url(r'^PairedComparisonOfMatchingGroups/(?P<pk>\d+)/delete/$', Delete_view.PairedComparisonOfMatchingGroupsDelete, name='PairedComparisonOfMatchingGroupsDelete'),

    url(r'^PairedComparisonOfSingleSampleGroups/(?P<pk>\d+)/delete/$', Delete_view.PairedComparisonOfSingleSampleGroupsDelete, name='PairedComparisonOfSingleSampleGroupsDelete'),

    url(r'^QualityControl/(?P<pk>\d+)/delete/$', Delete_view.QualityControlDelete, name='QualityControlDelete'),

    url(r'^ComplexHypothesis/(?P<pk>\d+)/delete/$', Delete_view.ComplexHypothesisDelete, name='ComplexHypothesisDelete'),

    url(r'^SingleHypothesis/(?P<pk>\d+)/delete/$', Delete_view.SingleHypothesisDelete, name='SingleHypothesisDelete'),

    url(r'^ComputationalExperiment/(?P<pk>\d+)/delete/$', Delete_view.ComputationalExperimentDelete, name='ComputationalExperimentDelete'),

    url(r'^ComputeGoal/(?P<pk>\d+)/delete/$', Delete_view.ComputeGoalDelete, name='ComputeGoalDelete'),

    url(r'^ConfirmGoal/(?P<pk>\d+)/delete/$', Delete_view.ConfirmGoalDelete, name='ConfirmGoalDelete'),

    url(r'^ExplainGoal/(?P<pk>\d+)/delete/$', Delete_view.ExplainGoalDelete, name='ExplainGoalDelete'),

    url(r'^InvestigateGoal/(?P<pk>\d+)/delete/$', Delete_view.InvestigateGoalDelete, name='InvestigateGoalDelete'),

    url(r'^ComputerSimulation/(?P<pk>\d+)/delete/$', Delete_view.ComputerSimulationDelete, name='ComputerSimulationDelete'),

    url(r'^ConstantQuantity/(?P<pk>\d+)/delete/$', Delete_view.ConstantQuantityDelete, name='ConstantQuantityDelete'),

    url(r'^Controllability/(?P<pk>\d+)/delete/$', Delete_view.ControllabilityDelete, name='ControllabilityDelete'),

    url(r'^Independence/(?P<pk>\d+)/delete/$', Delete_view.IndependenceDelete, name='IndependenceDelete'),

    url(r'^DBReference/(?P<pk>\d+)/delete/$', Delete_view.DBReferenceDelete, name='DBReferenceDelete'),

    url(r'^DDC_Dewey_Classification/(?P<pk>\d+)/delete/$', Delete_view.DDC_Dewey_ClassificationDelete, name='DDC_Dewey_ClassificationDelete'),

    url(r'^LibraryOfCongressClassification/(?P<pk>\d+)/delete/$', Delete_view.LibraryOfCongressClassificationDelete, name='LibraryOfCongressClassificationDelete'),

    url(r'^NLMClassification/(?P<pk>\d+)/delete/$', Delete_view.NLMClassificationDelete, name='NLMClassificationDelete'),

    url(r'^ResearchCouncilsUKClassification/(?P<pk>\d+)/delete/$', Delete_view.ResearchCouncilsUKClassificationDelete, name='ResearchCouncilsUKClassificationDelete'),

    url(r'^DataRepresentationStandard/(?P<pk>\d+)/delete/$', Delete_view.DataRepresentationStandardDelete, name='DataRepresentationStandardDelete'),

    url(r'^DegreeOfModel/(?P<pk>\d+)/delete/$', Delete_view.DegreeOfModelDelete, name='DegreeOfModelDelete'),

    url(r'^DynamismOfModel/(?P<pk>\d+)/delete/$', Delete_view.DynamismOfModelDelete, name='DynamismOfModelDelete'),

    url(r'^DependentVariable/(?P<pk>\d+)/delete/$', Delete_view.DependentVariableDelete, name='DependentVariableDelete'),

    url(r'^Device/(?P<pk>\d+)/delete/$', Delete_view.DeviceDelete, name='DeviceDelete'),

    url(r'^DomainAction/(?P<pk>\d+)/delete/$', Delete_view.DomainActionDelete, name='DomainActionDelete'),

    url(r'^DoseResponse/(?P<pk>\d+)/delete/$', Delete_view.DoseResponseDelete, name='DoseResponseDelete'),

    url(r'^GeneKnockin/(?P<pk>\d+)/delete/$', Delete_view.GeneKnockinDelete, name='GeneKnockinDelete'),

    url(r'^GeneKnockout/(?P<pk>\d+)/delete/$', Delete_view.GeneKnockoutDelete, name='GeneKnockoutDelete'),

    url(r'^Normal_Disease/(?P<pk>\d+)/delete/$', Delete_view.Normal_DiseaseDelete, name='Normal_DiseaseDelete'),

    url(r'^TimeCourse/(?P<pk>\d+)/delete/$', Delete_view.TimeCourseDelete, name='TimeCourseDelete'),

    url(r'^Treated_Untreated/(?P<pk>\d+)/delete/$', Delete_view.Treated_UntreatedDelete, name='Treated_UntreatedDelete'),

    url(r'^DraftStatus/(?P<pk>\d+)/delete/$', Delete_view.DraftStatusDelete, name='DraftStatusDelete'),

    url(r'^DuhemEffect/(?P<pk>\d+)/delete/$', Delete_view.DuhemEffectDelete, name='DuhemEffectDelete'),

    url(r'^ExperimentalDesignEffect/(?P<pk>\d+)/delete/$', Delete_view.ExperimentalDesignEffectDelete, name='ExperimentalDesignEffectDelete'),

    url(r'^InstrumentationEffect/(?P<pk>\d+)/delete/$', Delete_view.InstrumentationEffectDelete, name='InstrumentationEffectDelete'),

    url(r'^ObjectEffect/(?P<pk>\d+)/delete/$', Delete_view.ObjectEffectDelete, name='ObjectEffectDelete'),

    url(r'^SubjectEffect/(?P<pk>\d+)/delete/$', Delete_view.SubjectEffectDelete, name='SubjectEffectDelete'),

    url(r'^TimeEffect/(?P<pk>\d+)/delete/$', Delete_view.TimeEffectDelete, name='TimeEffectDelete'),

    url(r'^DynamicModel/(?P<pk>\d+)/delete/$', Delete_view.DynamicModelDelete, name='DynamicModelDelete'),

    url(r'^StaticModel/(?P<pk>\d+)/delete/$', Delete_view.StaticModelDelete, name='StaticModelDelete'),

    url(r'^ErrorOfConclusion/(?P<pk>\d+)/delete/$', Delete_view.ErrorOfConclusionDelete, name='ErrorOfConclusionDelete'),

    url(r'^ExperimentalActionsPlanning/(?P<pk>\d+)/delete/$', Delete_view.ExperimentalActionsPlanningDelete, name='ExperimentalActionsPlanningDelete'),

    url(r'^ExperimentalEquipmentSelecting/(?P<pk>\d+)/delete/$', Delete_view.ExperimentalEquipmentSelectingDelete, name='ExperimentalEquipmentSelectingDelete'),

    url(r'^ExperimentalModelDesign/(?P<pk>\d+)/delete/$', Delete_view.ExperimentalModelDesignDelete, name='ExperimentalModelDesignDelete'),

    url(r'^ExperimentalObjectSelecting/(?P<pk>\d+)/delete/$', Delete_view.ExperimentalObjectSelectingDelete, name='ExperimentalObjectSelectingDelete'),

    url(r'^ExperimentalConclusion/(?P<pk>\d+)/delete/$', Delete_view.ExperimentalConclusionDelete, name='ExperimentalConclusionDelete'),

    url(r'^ExperimentalProtocol/(?P<pk>\d+)/delete/$', Delete_view.ExperimentalProtocolDelete, name='ExperimentalProtocolDelete'),

    url(r'^ExperimenteeBias/(?P<pk>\d+)/delete/$', Delete_view.ExperimenteeBiasDelete, name='ExperimenteeBiasDelete'),

    url(r'^HawthorneEffect/(?P<pk>\d+)/delete/$', Delete_view.HawthorneEffectDelete, name='HawthorneEffectDelete'),

    url(r'^MortalityEffect/(?P<pk>\d+)/delete/$', Delete_view.MortalityEffectDelete, name='MortalityEffectDelete'),

    url(r'^ExperimenterBias/(?P<pk>\d+)/delete/$', Delete_view.ExperimenterBiasDelete, name='ExperimenterBiasDelete'),

    url(r'^TestingEffect/(?P<pk>\d+)/delete/$', Delete_view.TestingEffectDelete, name='TestingEffectDelete'),

    url(r'^ExplicitHypothesis/(?P<pk>\d+)/delete/$', Delete_view.ExplicitHypothesisDelete, name='ExplicitHypothesisDelete'),

    url(r'^ImplicitHypothesis/(?P<pk>\d+)/delete/$', Delete_view.ImplicitHypothesisDelete, name='ImplicitHypothesisDelete'),

    url(r'^FactorLevel/(?P<pk>\d+)/delete/$', Delete_view.FactorLevelDelete, name='FactorLevelDelete'),

    url(r'^FactorialDesign/(?P<pk>\d+)/delete/$', Delete_view.FactorialDesignDelete, name='FactorialDesignDelete'),

    url(r'^FalseNegative/(?P<pk>\d+)/delete/$', Delete_view.FalseNegativeDelete, name='FalseNegativeDelete'),

    url(r'^HypothesisAcceptanceMistake/(?P<pk>\d+)/delete/$', Delete_view.HypothesisAcceptanceMistakeDelete, name='HypothesisAcceptanceMistakeDelete'),

    url(r'^FalsePpositive/(?P<pk>\d+)/delete/$', Delete_view.FalsePpositiveDelete, name='FalsePpositiveDelete'),

    url(r'^FaultyComparison/(?P<pk>\d+)/delete/$', Delete_view.FaultyComparisonDelete, name='FaultyComparisonDelete'),

    url(r'^Formula/(?P<pk>\d+)/delete/$', Delete_view.FormulaDelete, name='FormulaDelete'),

    url(r'^GalileanExperiment/(?P<pk>\d+)/delete/$', Delete_view.GalileanExperimentDelete, name='GalileanExperimentDelete'),

    url(r'^HandTool/(?P<pk>\d+)/delete/$', Delete_view.HandToolDelete, name='HandToolDelete'),

    url(r'^Tool/(?P<pk>\d+)/delete/$', Delete_view.ToolDelete, name='ToolDelete'),

    url(r'^Hardware/(?P<pk>\d+)/delete/$', Delete_view.HardwareDelete, name='HardwareDelete'),

    url(r'^HistoryEffect/(?P<pk>\d+)/delete/$', Delete_view.HistoryEffectDelete, name='HistoryEffectDelete'),

    url(r'^MaturationEffect/(?P<pk>\d+)/delete/$', Delete_view.MaturationEffectDelete, name='MaturationEffectDelete'),

    url(r'^HypothesisdrivenExperiment/(?P<pk>\d+)/delete/$', Delete_view.HypothesisdrivenExperimentDelete, name='HypothesisdrivenExperimentDelete'),

    url(r'^HypothesisformingExperiment/(?P<pk>\d+)/delete/$', Delete_view.HypothesisformingExperimentDelete, name='HypothesisformingExperimentDelete'),

    url(r'^Image/(?P<pk>\d+)/delete/$', Delete_view.ImageDelete, name='ImageDelete'),

    url(r'^ImproperSampling/(?P<pk>\d+)/delete/$', Delete_view.ImproperSamplingDelete, name='ImproperSamplingDelete'),

    url(r'^IncompleteDataError/(?P<pk>\d+)/delete/$', Delete_view.IncompleteDataErrorDelete, name='IncompleteDataErrorDelete'),

    url(r'^IndependentVariable/(?P<pk>\d+)/delete/$', Delete_view.IndependentVariableDelete, name='IndependentVariableDelete'),

    url(r'^InferableVariable/(?P<pk>\d+)/delete/$', Delete_view.InferableVariableDelete, name='InferableVariableDelete'),

    url(r'^InternalStatus/(?P<pk>\d+)/delete/$', Delete_view.InternalStatusDelete, name='InternalStatusDelete'),

    url(r'^RestrictedStatus/(?P<pk>\d+)/delete/$', Delete_view.RestrictedStatusDelete, name='RestrictedStatusDelete'),

    url(r'^Journal/(?P<pk>\d+)/delete/$', Delete_view.JournalDelete, name='JournalDelete'),

    url(r'^LinearModel/(?P<pk>\d+)/delete/$', Delete_view.LinearModelDelete, name='LinearModelDelete'),

    url(r'^NonlinearModel/(?P<pk>\d+)/delete/$', Delete_view.NonlinearModelDelete, name='NonlinearModelDelete'),

    url(r'^LogicalModelRepresentation/(?P<pk>\d+)/delete/$', Delete_view.LogicalModelRepresentationDelete, name='LogicalModelRepresentationDelete'),

    url(r'^Machine/(?P<pk>\d+)/delete/$', Delete_view.MachineDelete, name='MachineDelete'),

    url(r'^MachineTool/(?P<pk>\d+)/delete/$', Delete_view.MachineToolDelete, name='MachineToolDelete'),

    url(r'^Magazine/(?P<pk>\d+)/delete/$', Delete_view.MagazineDelete, name='MagazineDelete'),

    url(r'^Materials/(?P<pk>\d+)/delete/$', Delete_view.MaterialsDelete, name='MaterialsDelete'),

    url(r'^MathematicalModelRepresentation/(?P<pk>\d+)/delete/$', Delete_view.MathematicalModelRepresentationDelete, name='MathematicalModelRepresentationDelete'),

    url(r'^MetabolomicExperiment/(?P<pk>\d+)/delete/$', Delete_view.MetabolomicExperimentDelete, name='MetabolomicExperimentDelete'),

    url(r'^MethodsComparison/(?P<pk>\d+)/delete/$', Delete_view.MethodsComparisonDelete, name='MethodsComparisonDelete'),

    url(r'^SubjectsComparison/(?P<pk>\d+)/delete/$', Delete_view.SubjectsComparisonDelete, name='SubjectsComparisonDelete'),

    url(r'^MicroarrayExperiment/(?P<pk>\d+)/delete/$', Delete_view.MicroarrayExperimentDelete, name='MicroarrayExperimentDelete'),

    url(r'^MultifactorExperiment/(?P<pk>\d+)/delete/$', Delete_view.MultifactorExperimentDelete, name='MultifactorExperimentDelete'),

    url(r'^OnefactorExperiment/(?P<pk>\d+)/delete/$', Delete_view.OnefactorExperimentDelete, name='OnefactorExperimentDelete'),

    url(r'^TwofactorExperiment/(?P<pk>\d+)/delete/$', Delete_view.TwofactorExperimentDelete, name='TwofactorExperimentDelete'),

    url(r'^NaturalLanguage/(?P<pk>\d+)/delete/$', Delete_view.NaturalLanguageDelete, name='NaturalLanguageDelete'),

    url(r'^NonRestrictedStatus/(?P<pk>\d+)/delete/$', Delete_view.NonRestrictedStatusDelete, name='NonRestrictedStatusDelete'),

    url(r'^NormalizationStrategy/(?P<pk>\d+)/delete/$', Delete_view.NormalizationStrategyDelete, name='NormalizationStrategyDelete'),

    url(r'^ObjectMethod/(?P<pk>\d+)/delete/$', Delete_view.ObjectMethodDelete, name='ObjectMethodDelete'),

    url(r'^ObjectOfAction/(?P<pk>\d+)/delete/$', Delete_view.ObjectOfActionDelete, name='ObjectOfActionDelete'),

    url(r'^ObservableVariable/(?P<pk>\d+)/delete/$', Delete_view.ObservableVariableDelete, name='ObservableVariableDelete'),

    url(r'^Paper/(?P<pk>\d+)/delete/$', Delete_view.PaperDelete, name='PaperDelete'),

    url(r'^ParticlePhysicsExperiment/(?P<pk>\d+)/delete/$', Delete_view.ParticlePhysicsExperimentDelete, name='ParticlePhysicsExperimentDelete'),

    url(r'^PresentingSampling/(?P<pk>\d+)/delete/$', Delete_view.PresentingSamplingDelete, name='PresentingSamplingDelete'),

    url(r'^Proceedings/(?P<pk>\d+)/delete/$', Delete_view.ProceedingsDelete, name='ProceedingsDelete'),

    url(r'^Program/(?P<pk>\d+)/delete/$', Delete_view.ProgramDelete, name='ProgramDelete'),

    url(r'^ProteomicExperiment/(?P<pk>\d+)/delete/$', Delete_view.ProteomicExperimentDelete, name='ProteomicExperimentDelete'),

    url(r'^Provider/(?P<pk>\d+)/delete/$', Delete_view.ProviderDelete, name='ProviderDelete'),

    url(r'^PublicAcademicStatus/(?P<pk>\d+)/delete/$', Delete_view.PublicAcademicStatusDelete, name='PublicAcademicStatusDelete'),

    url(r'^PublicStatus/(?P<pk>\d+)/delete/$', Delete_view.PublicStatusDelete, name='PublicStatusDelete'),

    url(r'^RandomError/(?P<pk>\d+)/delete/$', Delete_view.RandomErrorDelete, name='RandomErrorDelete'),

    url(r'^RandomSampling/(?P<pk>\d+)/delete/$', Delete_view.RandomSamplingDelete, name='RandomSamplingDelete'),

    url(r'^RecommendationSatus/(?P<pk>\d+)/delete/$', Delete_view.RecommendationSatusDelete, name='RecommendationSatusDelete'),

    url(r'^RecordingAction/(?P<pk>\d+)/delete/$', Delete_view.RecordingActionDelete, name='RecordingActionDelete'),

    url(r'^Region/(?P<pk>\d+)/delete/$', Delete_view.RegionDelete, name='RegionDelete'),

    url(r'^RepresenationalMedium/(?P<pk>\d+)/delete/$', Delete_view.RepresenationalMediumDelete, name='RepresenationalMediumDelete'),

    url(r'^SUMOSynonym/(?P<pk>\d+)/delete/$', Delete_view.SUMOSynonymDelete, name='SUMOSynonymDelete'),

    url(r'^SampleForming/(?P<pk>\d+)/delete/$', Delete_view.SampleFormingDelete, name='SampleFormingDelete'),

    url(r'^SequentialDesign/(?P<pk>\d+)/delete/$', Delete_view.SequentialDesignDelete, name='SequentialDesignDelete'),

    url(r'^Software/(?P<pk>\d+)/delete/$', Delete_view.SoftwareDelete, name='SoftwareDelete'),

    url(r'^Sound/(?P<pk>\d+)/delete/$', Delete_view.SoundDelete, name='SoundDelete'),

    url(r'^SplitSamples/(?P<pk>\d+)/delete/$', Delete_view.SplitSamplesDelete, name='SplitSamplesDelete'),

    url(r'^SymmetricalMatching/(?P<pk>\d+)/delete/$', Delete_view.SymmetricalMatchingDelete, name='SymmetricalMatchingDelete'),

    url(r'^StatisticalError/(?P<pk>\d+)/delete/$', Delete_view.StatisticalErrorDelete, name='StatisticalErrorDelete'),

    url(r'^StratifiedSampling/(?P<pk>\d+)/delete/$', Delete_view.StratifiedSamplingDelete, name='StratifiedSamplingDelete'),

    url(r'^SubjectOfAction/(?P<pk>\d+)/delete/$', Delete_view.SubjectOfActionDelete, name='SubjectOfActionDelete'),

    url(r'^SubjectOfExperiment/(?P<pk>\d+)/delete/$', Delete_view.SubjectOfExperimentDelete, name='SubjectOfExperimentDelete'),

    url(r'^Submitter/(?P<pk>\d+)/delete/$', Delete_view.SubmitterDelete, name='SubmitterDelete'),

    url(r'^Summary/(?P<pk>\d+)/delete/$', Delete_view.SummaryDelete, name='SummaryDelete'),

    url(r'^SystematicError/(?P<pk>\d+)/delete/$', Delete_view.SystematicErrorDelete, name='SystematicErrorDelete'),

    url(r'^TechnicalReport/(?P<pk>\d+)/delete/$', Delete_view.TechnicalReportDelete, name='TechnicalReportDelete'),

    url(r'^TimeDuration/(?P<pk>\d+)/delete/$', Delete_view.TimeDurationDelete, name='TimeDurationDelete'),

    url(r'^TimeInterval/(?P<pk>\d+)/delete/$', Delete_view.TimeIntervalDelete, name='TimeIntervalDelete'),

    url(r'^URL/(?P<pk>\d+)/delete/$', Delete_view.URLDelete, name='URLDelete'),

    ]