from owlready2 import *
from django.db import models
from django.template.loader import render_to_string
from .models import *
from .forms import *
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseNotFound
from django.template import loader
from django.core.urlresolvers import reverse
import datetime
from .forms import *
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.core.files.storage import FileSystemStorage
from django.utils.safestring import mark_safe
from django.conf import settings

onto_path.append(settings.BASE_DIR)
onto = get_ontology("EXPO.owl").load()
ontoclasses = onto.classes()
namespaces = dict() 
for clas in ontoclasses:
    namespaces[clas.name] = clas.namespace


def ThingCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Thing"]

    

    if request.method == 'POST':
        form = ThingForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Thing(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/Thing/list/"
            return HttpResponseRedirect(link)
    else:
        form = ThingForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ThingCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def RepresentationFormCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["RepresentationForm"]

    
    foreign_keys['Has_'] = 'EXPO_app:ArtifactList'
    
    foreign_keys['Has_expression'] = 'EXPO_app:LinguisticExpressionList'
    

    if request.method == 'POST':
        form = RepresentationForm1(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.RepresentationForm(instance.name)
            onto.save()
            

            
            onto.save()

            
            fkstr = 'Has_'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Artifact(fkinstanceinput[0])
            onto.save()
            onto_instance.has_.append(fkinstance) 
            
            fkstr = 'Has_expression'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.LinguisticExpression(fkinstanceinput[0])
            onto.save()
            onto_instance.has_expression.append(fkinstance) 
            
            onto.save()
            link = "/EXPO_app/RepresentationForm/list/"
            return HttpResponseRedirect(link)
    else:
        form = RepresentationFormForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "RepresentationFormCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ArtifactCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Artifact"]

    

    if request.method == 'POST':
        form = ArtifactForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Artifact(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/Artifact/list/"
            return HttpResponseRedirect(link)
    else:
        form = ArtifactForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ArtifactCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def SynonymousExternalConceptCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["SynonymousExternalConcept"]

    
    foreign_keys['Has_EXPO_concept'] = 'EXPO_app:NameList'
    
    foreign_keys['Has_synonymousExternalConcept'] = 'EXPO_app:NameList'
    
    foreign_keys['Has_EXPO_concept'] = 'EXPO_app:ThingList'
    

    if request.method == 'POST':
        form = SynonymousExternalConceptForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.SynonymousExternalConcept(instance.name)
            onto.save()
            

            
            onto.save()

            
            fkstr = 'Has_EXPO_concept'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Name(fkinstanceinput[0])
            onto.save()
            onto_instance.has_EXPO_concept.append(fkinstance) 
            
            fkstr = 'Has_synonymousExternalConcept'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Name(fkinstanceinput[0])
            onto.save()
            onto_instance.has_synonymousExternalConcept.append(fkinstance) 
            
            fkstr = 'Has_EXPO_concept'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Thing(fkinstanceinput[0])
            onto.save()
            onto_instance.has_EXPO_concept.append(fkinstance) 
            
            onto.save()
            link = "/EXPO_app/SynonymousExternalConcept/list/"
            return HttpResponseRedirect(link)
    else:
        form = SynonymousExternalConceptForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "SynonymousExternalConceptCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def NameCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Name"]

    

    if request.method == 'POST':
        form = NameForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Name(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/Name/list/"
            return HttpResponseRedirect(link)
    else:
        form = NameForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "NameCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ScientificExperimentCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ScientificExperiment"]

    
    foreign_keys['Has_ExperimentalDesign'] = 'EXPO_app:ExperimentalDesignList'
    
    foreign_keys['Has_admin_info'] = 'EXPO_app:AdminInfoExperimentList'
    
    foreign_keys['Has_classification'] = 'EXPO_app:ClassificationOfExperimentsList'
    
    foreign_keys['Has_goal'] = 'EXPO_app:ExperimentalGoalList'
    

    if request.method == 'POST':
        form = ScientificExperimentForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ScientificExperiment(instance.name)
            onto.save()
            

            
            onto.save()

            
            fkstr = 'Has_ExperimentalDesign'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.ExperimentalDesign(fkinstanceinput[0])
            onto.save()
            onto_instance.has_ExperimentalDesign.append(fkinstance) 
            
            fkstr = 'Has_admin_info'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.AdminInfoExperiment(fkinstanceinput[0])
            onto.save()
            onto_instance.has_admin_info.append(fkinstance) 
            
            fkstr = 'Has_classification'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.ClassificationOfExperiments(fkinstanceinput[0])
            onto.save()
            onto_instance.has_classification.append(fkinstance) 
            
            fkstr = 'Has_goal'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.ExperimentalGoal(fkinstanceinput[0])
            onto.save()
            onto_instance.has_goal.append(fkinstance) 
            
            onto.save()
            link = "/EXPO_app/ScientificExperiment/list/"
            return HttpResponseRedirect(link)
    else:
        form = ScientificExperimentForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ScientificExperimentCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ExperimentalDesignCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ExperimentalDesign"]

    

    if request.method == 'POST':
        form = ExperimentalDesignForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ExperimentalDesign(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/ExperimentalDesign/list/"
            return HttpResponseRedirect(link)
    else:
        form = ExperimentalDesignForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ExperimentalDesignCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def AdminInfoExperimentCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["AdminInfoExperiment"]

    
    foreign_keys['Has_ID'] = 'EXPO_app:ThingList'
    
    foreign_keys['Has_ID'] = 'EXPO_app:IDExperimentList'
    
    foreign_keys['Has_author'] = 'EXPO_app:AuthorList'
    
    foreign_keys['Has_organization'] = 'EXPO_app:OrganizationList'
    
    foreign_keys['Has_reference'] = 'EXPO_app:BiblioReferenceList'
    
    foreign_keys['Has_status'] = 'EXPO_app:StatusExperimentalDocumentList'
    
    foreign_keys['Has_title'] = 'EXPO_app:TitleExperimentList'
    

    if request.method == 'POST':
        form = AdminInfoExperimentForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.AdminInfoExperiment(instance.name)
            onto.save()
            

            
            onto.save()

            
            fkstr = 'Has_ID'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Thing(fkinstanceinput[0])
            onto.save()
            onto_instance.has_ID.append(fkinstance) 
            
            fkstr = 'Has_ID'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.IDExperiment(fkinstanceinput[0])
            onto.save()
            onto_instance.has_ID.append(fkinstance) 
            
            fkstr = 'Has_author'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Author(fkinstanceinput[0])
            onto.save()
            onto_instance.has_author.append(fkinstance) 
            
            fkstr = 'Has_organization'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Organization(fkinstanceinput[0])
            onto.save()
            onto_instance.has_organization.append(fkinstance) 
            
            fkstr = 'Has_reference'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.BiblioReference(fkinstanceinput[0])
            onto.save()
            onto_instance.has_reference.append(fkinstance) 
            
            fkstr = 'Has_status'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.StatusExperimentalDocument(fkinstanceinput[0])
            onto.save()
            onto_instance.has_status.append(fkinstance) 
            
            fkstr = 'Has_title'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.TitleExperiment(fkinstanceinput[0])
            onto.save()
            onto_instance.has_title.append(fkinstance) 
            
            onto.save()
            link = "/EXPO_app/AdminInfoExperiment/list/"
            return HttpResponseRedirect(link)
    else:
        form = AdminInfoExperimentForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "AdminInfoExperimentCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def IDExperimentCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["IDExperiment"]

    

    if request.method == 'POST':
        form = IDExperimentForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.IDExperiment(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/IDExperiment/list/"
            return HttpResponseRedirect(link)
    else:
        form = IDExperimentForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "IDExperimentCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def DomainOfExperimentCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["DomainOfExperiment"]

    
    foreign_keys['Has_SUMO_sinonym'] = 'EXPO_app:SynonymousExternalConceptList'
    
    foreign_keys['Has_classification'] = 'EXPO_app:ClassificationOfDomainsList'
    
    foreign_keys['Has_model'] = 'EXPO_app:DomainModelList'
    

    if request.method == 'POST':
        form = DomainOfExperimentForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.DomainOfExperiment(instance.name)
            onto.save()
            

            
            onto.save()

            
            fkstr = 'Has_SUMO_sinonym'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.SynonymousExternalConcept(fkinstanceinput[0])
            onto.save()
            onto_instance.has_SUMO_sinonym.append(fkinstance) 
            
            fkstr = 'Has_classification'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.ClassificationOfDomains(fkinstanceinput[0])
            onto.save()
            onto_instance.has_classification.append(fkinstance) 
            
            fkstr = 'Has_model'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.DomainModel(fkinstanceinput[0])
            onto.save()
            onto_instance.has_model.append(fkinstance) 
            
            onto.save()
            link = "/EXPO_app/DomainOfExperiment/list/"
            return HttpResponseRedirect(link)
    else:
        form = DomainOfExperimentForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "DomainOfExperimentCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ExperimentalObservationCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ExperimentalObservation"]

    
    foreign_keys['Has__interpretation'] = 'EXPO_app:ResultsInterpretationList'
    
    foreign_keys['Has_error'] = 'EXPO_app:ObservationalErrorList'
    
    foreign_keys['Has_observation_characteristic'] = 'EXPO_app:StatisticsCharacteristicList'
    

    if request.method == 'POST':
        form = ExperimentalObservationForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ExperimentalObservation(instance.name)
            onto.save()
            

            
            onto.save()

            
            fkstr = 'Has__interpretation'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.ResultsInterpretation(fkinstanceinput[0])
            onto.save()
            onto_instance.has__interpretation.append(fkinstance) 
            
            fkstr = 'Has_error'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.ObservationalError(fkinstanceinput[0])
            onto.save()
            onto_instance.has_error.append(fkinstance) 
            
            fkstr = 'Has_observation_characteristic'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.StatisticsCharacteristic(fkinstanceinput[0])
            onto.save()
            onto_instance.has_observation_characteristic.append(fkinstance) 
            
            onto.save()
            link = "/EXPO_app/ExperimentalObservation/list/"
            return HttpResponseRedirect(link)
    else:
        form = ExperimentalObservationForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ExperimentalObservationCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ResultsInterpretationCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ResultsInterpretation"]

    

    if request.method == 'POST':
        form = ResultsInterpretationForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ResultsInterpretation(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/ResultsInterpretation/list/"
            return HttpResponseRedirect(link)
    else:
        form = ResultsInterpretationForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ResultsInterpretationCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ExperimentalActionCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ExperimentalAction"]

    
    foreign_keys['Has__name'] = 'EXPO_app:ActionNameList'
    
    foreign_keys['Has_action_complexity'] = 'EXPO_app:ActionComplexityList'
    
    foreign_keys['Has_goal'] = 'EXPO_app:ActionGoalList'
    
    foreign_keys['Has_instrument'] = 'EXPO_app:ExperimentalEquipmentList'
    
    foreign_keys['Has_method'] = 'EXPO_app:ExperimentalMethodList'
    
    foreign_keys['Has_timing'] = 'EXPO_app:TimePointList'
    
    foreign_keys['Has_action_complexity'] = 'EXPO_app:ThingList'
    

    if request.method == 'POST':
        form = ExperimentalActionForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ExperimentalAction(instance.name)
            onto.save()
            

            
            onto.save()

            
            fkstr = 'Has__name'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.ActionName(fkinstanceinput[0])
            onto.save()
            onto_instance.has__name.append(fkinstance) 
            
            fkstr = 'Has_action_complexity'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.ActionComplexity(fkinstanceinput[0])
            onto.save()
            onto_instance.has_action_complexity.append(fkinstance) 
            
            fkstr = 'Has_goal'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.ActionGoal(fkinstanceinput[0])
            onto.save()
            onto_instance.has_goal.append(fkinstance) 
            
            fkstr = 'Has_instrument'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.ExperimentalEquipment(fkinstanceinput[0])
            onto.save()
            onto_instance.has_instrument.append(fkinstance) 
            
            fkstr = 'Has_method'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.ExperimentalMethod(fkinstanceinput[0])
            onto.save()
            onto_instance.has_method.append(fkinstance) 
            
            fkstr = 'Has_timing'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.TimePoint(fkinstanceinput[0])
            onto.save()
            onto_instance.has_timing.append(fkinstance) 
            
            fkstr = 'Has_action_complexity'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Thing(fkinstanceinput[0])
            onto.save()
            onto_instance.has_action_complexity.append(fkinstance) 
            
            onto.save()
            link = "/EXPO_app/ExperimentalAction/list/"
            return HttpResponseRedirect(link)
    else:
        form = ExperimentalActionForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ExperimentalActionCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ActionNameCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ActionName"]

    

    if request.method == 'POST':
        form = ActionNameForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ActionName(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/ActionName/list/"
            return HttpResponseRedirect(link)
    else:
        form = ActionNameForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ActionNameCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ActionComplexityCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ActionComplexity"]

    

    if request.method == 'POST':
        form = ActionComplexityForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ActionComplexity(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/ActionComplexity/list/"
            return HttpResponseRedirect(link)
    else:
        form = ActionComplexityForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ActionComplexityCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def HumanCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Human"]

    
    foreign_keys['Has_organization'] = 'EXPO_app:OrganizationList'
    
    foreign_keys['Has_telephone_number'] = 'EXPO_app:NumberList'
    
    foreign_keys['Has_title'] = 'EXPO_app:TitleList'
    
    foreign_keys['Has_name'] = 'EXPO_app:ThingList'
    

    if request.method == 'POST':
        form = HumanForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Human(instance.name)
            onto.save()
            

            
            onto.save()

            
            fkstr = 'Has_organization'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Organization(fkinstanceinput[0])
            onto.save()
            onto_instance.has_organization.append(fkinstance) 
            
            fkstr = 'Has_telephone_number'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Number(fkinstanceinput[0])
            onto.save()
            onto_instance.has_telephone_number.append(fkinstance) 
            
            fkstr = 'Has_title'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Title(fkinstanceinput[0])
            onto.save()
            onto_instance.has_title.append(fkinstance) 
            
            fkstr = 'Has_name'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Thing(fkinstanceinput[0])
            onto.save()
            onto_instance.has_name.append(fkinstance) 
            
            onto.save()
            link = "/EXPO_app/Human/list/"
            return HttpResponseRedirect(link)
    else:
        form = HumanForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "HumanCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def OrganizationCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Organization"]

    
    foreign_keys['Has_fax_number'] = 'EXPO_app:NumberList'
    
    foreign_keys['Has_name'] = 'EXPO_app:NameList'
    
    foreign_keys['Has_telephone_number'] = 'EXPO_app:NumberList'
    
    foreign_keys['Has_name'] = 'EXPO_app:EntityList'
    

    if request.method == 'POST':
        form = OrganizationForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Organization(instance.name)
            onto.save()
            

            
            onto.save()

            
            fkstr = 'Has_fax_number'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Number(fkinstanceinput[0])
            onto.save()
            onto_instance.has_fax_number.append(fkinstance) 
            
            fkstr = 'Has_name'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Name(fkinstanceinput[0])
            onto.save()
            onto_instance.has_name.append(fkinstance) 
            
            fkstr = 'Has_telephone_number'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Number(fkinstanceinput[0])
            onto.save()
            onto_instance.has_telephone_number.append(fkinstance) 
            
            fkstr = 'Has_name'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Entity(fkinstanceinput[0])
            onto.save()
            onto_instance.has_name.append(fkinstance) 
            
            onto.save()
            link = "/EXPO_app/Organization/list/"
            return HttpResponseRedirect(link)
    else:
        form = OrganizationForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "OrganizationCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def AdminInfoAuthorCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["AdminInfoAuthor"]

    

    if request.method == 'POST':
        form = AdminInfoAuthorForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.AdminInfoAuthor(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/AdminInfoAuthor/list/"
            return HttpResponseRedirect(link)
    else:
        form = AdminInfoAuthorForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "AdminInfoAuthorCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def AdminInfoObjectOfExperimentCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["AdminInfoObjectOfExperiment"]

    

    if request.method == 'POST':
        form = AdminInfoObjectOfExperimentForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.AdminInfoObjectOfExperiment(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/AdminInfoObjectOfExperiment/list/"
            return HttpResponseRedirect(link)
    else:
        form = AdminInfoObjectOfExperimentForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "AdminInfoObjectOfExperimentCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def AdminInfoProviderCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["AdminInfoProvider"]

    

    if request.method == 'POST':
        form = AdminInfoProviderForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.AdminInfoProvider(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/AdminInfoProvider/list/"
            return HttpResponseRedirect(link)
    else:
        form = AdminInfoProviderForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "AdminInfoProviderCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def AdminInfoSubjectOfExperimentCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["AdminInfoSubjectOfExperiment"]

    
    foreign_keys['Has_name'] = 'EXPO_app:NameList'
    
    foreign_keys['Has_organization'] = 'EXPO_app:OrganizationList'
    
    foreign_keys['Has_qualification'] = 'EXPO_app:SubjectQualificationList'
    

    if request.method == 'POST':
        form = AdminInfoSubjectOfExperimentForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.AdminInfoSubjectOfExperiment(instance.name)
            onto.save()
            

            
            onto.save()

            
            fkstr = 'Has_name'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Name(fkinstanceinput[0])
            onto.save()
            onto_instance.has_name.append(fkinstance) 
            
            fkstr = 'Has_organization'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Organization(fkinstanceinput[0])
            onto.save()
            onto_instance.has_organization.append(fkinstance) 
            
            fkstr = 'Has_qualification'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.SubjectQualification(fkinstanceinput[0])
            onto.save()
            onto_instance.has_qualification.append(fkinstance) 
            
            onto.save()
            link = "/EXPO_app/AdminInfoSubjectOfExperiment/list/"
            return HttpResponseRedirect(link)
    else:
        form = AdminInfoSubjectOfExperimentForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "AdminInfoSubjectOfExperimentCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def AdminInfoSubmitterCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["AdminInfoSubmitter"]

    

    if request.method == 'POST':
        form = AdminInfoSubmitterForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.AdminInfoSubmitter(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/AdminInfoSubmitter/list/"
            return HttpResponseRedirect(link)
    else:
        form = AdminInfoSubmitterForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "AdminInfoSubmitterCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def AdminInfoUserCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["AdminInfoUser"]

    

    if request.method == 'POST':
        form = AdminInfoUserForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.AdminInfoUser(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/AdminInfoUser/list/"
            return HttpResponseRedirect(link)
    else:
        form = AdminInfoUserForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "AdminInfoUserCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ExperimentalMethodCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ExperimentalMethod"]

    
    foreign_keys['Has_applicability'] = 'EXPO_app:MethodApplicabilityConditionList'
    
    foreign_keys['Has_goal'] = 'EXPO_app:MethodGoalList'
    
    foreign_keys['Has_plan'] = 'EXPO_app:PlanOfExperimentalActionsList'
    
    foreign_keys['Has_subject'] = 'EXPO_app:SubjectMethodList'
    

    if request.method == 'POST':
        form = ExperimentalMethodForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ExperimentalMethod(instance.name)
            onto.save()
            

            
            onto.save()

            
            fkstr = 'Has_applicability'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.MethodApplicabilityCondition(fkinstanceinput[0])
            onto.save()
            onto_instance.has_applicability.append(fkinstance) 
            
            fkstr = 'Has_goal'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.MethodGoal(fkinstanceinput[0])
            onto.save()
            onto_instance.has_goal.append(fkinstance) 
            
            fkstr = 'Has_plan'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.PlanOfExperimentalActions(fkinstanceinput[0])
            onto.save()
            onto_instance.has_plan.append(fkinstance) 
            
            fkstr = 'Has_subject'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.SubjectMethod(fkinstanceinput[0])
            onto.save()
            onto_instance.has_subject.append(fkinstance) 
            
            onto.save()
            link = "/EXPO_app/ExperimentalMethod/list/"
            return HttpResponseRedirect(link)
    else:
        form = ExperimentalMethodForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ExperimentalMethodCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def MethodApplicabilityConditionCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["MethodApplicabilityCondition"]

    

    if request.method == 'POST':
        form = MethodApplicabilityConditionForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.MethodApplicabilityCondition(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/MethodApplicabilityCondition/list/"
            return HttpResponseRedirect(link)
    else:
        form = MethodApplicabilityConditionForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "MethodApplicabilityConditionCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def StandardOperatingProcedureCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["StandardOperatingProcedure"]

    
    foreign_keys['Has_author'] = 'EXPO_app:AuthorSOPList'
    
    foreign_keys['Has_latest_modification'] = 'EXPO_app:TimePointList'
    
    foreign_keys['Has_procedure'] = 'EXPO_app:ProcedureExecuteExperimentList'
    
    foreign_keys['Has_representation'] = 'EXPO_app:BiblioReferenceList'
    
    foreign_keys['Has_latest_modification'] = 'EXPO_app:ThingList'
    

    if request.method == 'POST':
        form = StandardOperatingProcedureForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.StandardOperatingProcedure(instance.name)
            onto.save()
            

            
            onto.save()

            
            fkstr = 'Has_author'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.AuthorSOP(fkinstanceinput[0])
            onto.save()
            onto_instance.has_author.append(fkinstance) 
            
            fkstr = 'Has_latest_modification'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.TimePoint(fkinstanceinput[0])
            onto.save()
            onto_instance.has_latest_modification.append(fkinstance) 
            
            fkstr = 'Has_procedure'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.ProcedureExecuteExperiment(fkinstanceinput[0])
            onto.save()
            onto_instance.has_procedure.append(fkinstance) 
            
            fkstr = 'Has_representation'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.BiblioReference(fkinstanceinput[0])
            onto.save()
            onto_instance.has_representation.append(fkinstance) 
            
            fkstr = 'Has_latest_modification'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Thing(fkinstanceinput[0])
            onto.save()
            onto_instance.has_latest_modification.append(fkinstance) 
            
            onto.save()
            link = "/EXPO_app/StandardOperatingProcedure/list/"
            return HttpResponseRedirect(link)
    else:
        form = StandardOperatingProcedureForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "StandardOperatingProcedureCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def AuthorCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Author"]

    
    foreign_keys['Has_admin_info'] = 'EXPO_app:ThingList'
    
    foreign_keys['Has_admin_info'] = 'EXPO_app:AdminInfoAuthorList'
    

    if request.method == 'POST':
        form = AuthorForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Author(instance.name)
            onto.save()
            

            
            onto.save()

            
            fkstr = 'Has_admin_info'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Thing(fkinstanceinput[0])
            onto.save()
            onto_instance.has_admin_info.append(fkinstance) 
            
            fkstr = 'Has_admin_info'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.AdminInfoAuthor(fkinstanceinput[0])
            onto.save()
            onto_instance.has_admin_info.append(fkinstance) 
            
            onto.save()
            link = "/EXPO_app/Author/list/"
            return HttpResponseRedirect(link)
    else:
        form = AuthorForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "AuthorCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ClassificationOfDomainsCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ClassificationOfDomains"]

    

    if request.method == 'POST':
        form = ClassificationOfDomainsForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ClassificationOfDomains(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/ClassificationOfDomains/list/"
            return HttpResponseRedirect(link)
    else:
        form = ClassificationOfDomainsForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ClassificationOfDomainsCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ClassificationOfExperimentsCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ClassificationOfExperiments"]

    

    if request.method == 'POST':
        form = ClassificationOfExperimentsForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ClassificationOfExperiments(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/ClassificationOfExperiments/list/"
            return HttpResponseRedirect(link)
    else:
        form = ClassificationOfExperimentsForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ClassificationOfExperimentsCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ExperimentalHypothesisCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ExperimentalHypothesis"]

    
    foreign_keys['Has_explicitness'] = 'EXPO_app:ThingList'
    
    foreign_keys['Has_complexity'] = 'EXPO_app:HypothesisComplexityList'
    
    foreign_keys['Has_explicitness'] = 'EXPO_app:HypothesisExplicitnessList'
    
    foreign_keys['Has_generality'] = 'EXPO_app:GeneralityOfHypothesisList'
    
    foreign_keys['Has_hypothesis_representation'] = 'EXPO_app:HypothesisRepresentationList'
    
    foreign_keys['Has_target_variable'] = 'EXPO_app:TargetVariableList'
    
    foreign_keys['Has_complexity'] = 'EXPO_app:ThingList'
    
    foreign_keys['Has_generality'] = 'EXPO_app:ThingList'
    

    if request.method == 'POST':
        form = ExperimentalHypothesisForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ExperimentalHypothesis(instance.name)
            onto.save()
            

            
            onto.save()

            
            fkstr = 'Has_explicitness'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Thing(fkinstanceinput[0])
            onto.save()
            onto_instance.has_explicitness.append(fkinstance) 
            
            fkstr = 'Has_complexity'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.HypothesisComplexity(fkinstanceinput[0])
            onto.save()
            onto_instance.has_complexity.append(fkinstance) 
            
            fkstr = 'Has_explicitness'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.HypothesisExplicitness(fkinstanceinput[0])
            onto.save()
            onto_instance.has_explicitness.append(fkinstance) 
            
            fkstr = 'Has_generality'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.GeneralityOfHypothesis(fkinstanceinput[0])
            onto.save()
            onto_instance.has_generality.append(fkinstance) 
            
            fkstr = 'Has_hypothesis_representation'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.HypothesisRepresentation(fkinstanceinput[0])
            onto.save()
            onto_instance.has_hypothesis_representation.append(fkinstance) 
            
            fkstr = 'Has_target_variable'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.TargetVariable(fkinstanceinput[0])
            onto.save()
            onto_instance.has_target_variable.append(fkinstance) 
            
            fkstr = 'Has_complexity'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Thing(fkinstanceinput[0])
            onto.save()
            onto_instance.has_complexity.append(fkinstance) 
            
            fkstr = 'Has_generality'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Thing(fkinstanceinput[0])
            onto.save()
            onto_instance.has_generality.append(fkinstance) 
            
            onto.save()
            link = "/EXPO_app/ExperimentalHypothesis/list/"
            return HttpResponseRedirect(link)
    else:
        form = ExperimentalHypothesisForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ExperimentalHypothesisCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def HypothesisComplexityCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["HypothesisComplexity"]

    

    if request.method == 'POST':
        form = HypothesisComplexityForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.HypothesisComplexity(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/HypothesisComplexity/list/"
            return HttpResponseRedirect(link)
    else:
        form = HypothesisComplexityForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "HypothesisComplexityCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def RepresentationCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Representation"]

    
    foreign_keys['Has_content'] = 'EXPO_app:PropositionList'
    
    foreign_keys['Has_representation_style'] = 'EXPO_app:RepresentationFormList'
    
    foreign_keys['Has_content'] = 'EXPO_app:ThingList'
    
    foreign_keys['Has_representation_style'] = 'EXPO_app:ThingList'
    

    if request.method == 'POST':
        form = RepresentationForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Representation(instance.name)
            onto.save()
            

            
            onto.save()

            
            fkstr = 'Has_content'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Proposition(fkinstanceinput[0])
            onto.save()
            onto_instance.has_content.append(fkinstance) 
            
            fkstr = 'Has_representation_style'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.RepresentationForm(fkinstanceinput[0])
            onto.save()
            onto_instance.has_representation_style.append(fkinstance) 
            
            fkstr = 'Has_content'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Thing(fkinstanceinput[0])
            onto.save()
            onto_instance.has_content.append(fkinstance) 
            
            fkstr = 'Has_representation_style'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Thing(fkinstanceinput[0])
            onto.save()
            onto_instance.has_representation_style.append(fkinstance) 
            
            onto.save()
            link = "/EXPO_app/Representation/list/"
            return HttpResponseRedirect(link)
    else:
        form = RepresentationForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "RepresentationCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def PropositionCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Proposition"]

    

    if request.method == 'POST':
        form = PropositionForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Proposition(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/Proposition/list/"
            return HttpResponseRedirect(link)
    else:
        form = PropositionForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "PropositionCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ExperimentalEquipmentCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ExperimentalEquipment"]

    
    foreign_keys['Has_description'] = 'EXPO_app:TechnicalDescriptionList'
    
    foreign_keys['Has_name'] = 'EXPO_app:NameEquipmentList'
    

    if request.method == 'POST':
        form = ExperimentalEquipmentForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ExperimentalEquipment(instance.name)
            onto.save()
            

            
            onto.save()

            
            fkstr = 'Has_description'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.TechnicalDescription(fkinstanceinput[0])
            onto.save()
            onto_instance.has_description.append(fkinstance) 
            
            fkstr = 'Has_name'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.NameEquipment(fkinstanceinput[0])
            onto.save()
            onto_instance.has_name.append(fkinstance) 
            
            onto.save()
            link = "/EXPO_app/ExperimentalEquipment/list/"
            return HttpResponseRedirect(link)
    else:
        form = ExperimentalEquipmentForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ExperimentalEquipmentCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def TechnicalDescriptionCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["TechnicalDescription"]

    

    if request.method == 'POST':
        form = TechnicalDescriptionForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.TechnicalDescription(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/TechnicalDescription/list/"
            return HttpResponseRedirect(link)
    else:
        form = TechnicalDescriptionForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "TechnicalDescriptionCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ExperimentalResultsCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ExperimentalResults"]

    
    foreign_keys['Has_error'] = 'EXPO_app:ResultErrorList'
    
    foreign_keys['Has_evaluation'] = 'EXPO_app:ResultsEvaluationList'
    
    foreign_keys['Has_fact'] = 'EXPO_app:FactRejectResearchHypothesisList'
    
    foreign_keys['Has_fact'] = 'EXPO_app:FactSupportResearchHypothesisList'
    

    if request.method == 'POST':
        form = ExperimentalResultsForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ExperimentalResults(instance.name)
            onto.save()
            

            
            onto.save()

            
            fkstr = 'Has_error'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.ResultError(fkinstanceinput[0])
            onto.save()
            onto_instance.has_error.append(fkinstance) 
            
            fkstr = 'Has_evaluation'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.ResultsEvaluation(fkinstanceinput[0])
            onto.save()
            onto_instance.has_evaluation.append(fkinstance) 
            
            fkstr = 'Has_fact'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.FactRejectResearchHypothesis(fkinstanceinput[0])
            onto.save()
            onto_instance.has_fact.append(fkinstance) 
            
            fkstr = 'Has_fact'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.FactSupportResearchHypothesis(fkinstanceinput[0])
            onto.save()
            onto_instance.has_fact.append(fkinstance) 
            
            onto.save()
            link = "/EXPO_app/ExperimentalResults/list/"
            return HttpResponseRedirect(link)
    else:
        form = ExperimentalResultsForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ExperimentalResultsCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ObservationalErrorCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ObservationalError"]

    

    if request.method == 'POST':
        form = ObservationalErrorForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ObservationalError(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/ObservationalError/list/"
            return HttpResponseRedirect(link)
    else:
        form = ObservationalErrorForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ObservationalErrorCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ResultErrorCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ResultError"]

    

    if request.method == 'POST':
        form = ResultErrorForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ResultError(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/ResultError/list/"
            return HttpResponseRedirect(link)
    else:
        form = ResultErrorForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ResultErrorCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ResultsEvaluationCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ResultsEvaluation"]

    

    if request.method == 'POST':
        form = ResultsEvaluationForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ResultsEvaluation(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/ResultsEvaluation/list/"
            return HttpResponseRedirect(link)
    else:
        form = ResultsEvaluationForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ResultsEvaluationCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ScientificInvestigationCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ScientificInvestigation"]

    
    foreign_keys['Has_experiment'] = 'EXPO_app:ScientificExperimentList'
    
    foreign_keys['Has_experimental_design'] = 'EXPO_app:ExperimentalDesignList'
    
    foreign_keys['Has_hypothesis_formation'] = 'EXPO_app:HypothesisFormationList'
    
    foreign_keys['Has_information_gethering'] = 'EXPO_app:InformationGetheringList'
    
    foreign_keys['Has_problem_analysis'] = 'EXPO_app:PoblemAnalysisList'
    
    foreign_keys['Has_result_evaluation'] = 'EXPO_app:ResultsEvaluationList'
    
    foreign_keys['Has_result_interpretation'] = 'EXPO_app:ResultsInterpretationList'
    

    if request.method == 'POST':
        form = ScientificInvestigationForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ScientificInvestigation(instance.name)
            onto.save()
            

            
            onto.save()

            
            fkstr = 'Has_experiment'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.ScientificExperiment(fkinstanceinput[0])
            onto.save()
            onto_instance.has_experiment.append(fkinstance) 
            
            fkstr = 'Has_experimental_design'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.ExperimentalDesign(fkinstanceinput[0])
            onto.save()
            onto_instance.has_experimental_design.append(fkinstance) 
            
            fkstr = 'Has_hypothesis_formation'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.HypothesisFormation(fkinstanceinput[0])
            onto.save()
            onto_instance.has_hypothesis_formation.append(fkinstance) 
            
            fkstr = 'Has_information_gethering'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.InformationGethering(fkinstanceinput[0])
            onto.save()
            onto_instance.has_information_gethering.append(fkinstance) 
            
            fkstr = 'Has_problem_analysis'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.PoblemAnalysis(fkinstanceinput[0])
            onto.save()
            onto_instance.has_problem_analysis.append(fkinstance) 
            
            fkstr = 'Has_result_evaluation'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.ResultsEvaluation(fkinstanceinput[0])
            onto.save()
            onto_instance.has_result_evaluation.append(fkinstance) 
            
            fkstr = 'Has_result_interpretation'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.ResultsInterpretation(fkinstanceinput[0])
            onto.save()
            onto_instance.has_result_interpretation.append(fkinstance) 
            
            onto.save()
            link = "/EXPO_app/ScientificInvestigation/list/"
            return HttpResponseRedirect(link)
    else:
        form = ScientificInvestigationForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ScientificInvestigationCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ExperimentalModelCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ExperimentalModel"]

    
    foreign_keys['Has_experimental_factor'] = 'EXPO_app:ExperimentalFactorList'
    
    foreign_keys['Has_unknown_variable'] = 'EXPO_app:TargetVariableList'
    

    if request.method == 'POST':
        form = ExperimentalModelForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ExperimentalModel(instance.name)
            onto.save()
            

            
            onto.save()

            
            fkstr = 'Has_experimental_factor'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.ExperimentalFactor(fkinstanceinput[0])
            onto.save()
            onto_instance.has_experimental_factor.append(fkinstance) 
            
            fkstr = 'Has_unknown_variable'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.TargetVariable(fkinstanceinput[0])
            onto.save()
            onto_instance.has_unknown_variable.append(fkinstance) 
            
            onto.save()
            link = "/EXPO_app/ExperimentalModel/list/"
            return HttpResponseRedirect(link)
    else:
        form = ExperimentalModelForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ExperimentalModelCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ExperimentalFactorCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ExperimentalFactor"]

    

    if request.method == 'POST':
        form = ExperimentalFactorForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ExperimentalFactor(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/ExperimentalFactor/list/"
            return HttpResponseRedirect(link)
    else:
        form = ExperimentalFactorForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ExperimentalFactorCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ExperimentalTechnologyCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ExperimentalTechnology"]

    
    foreign_keys['Has_experimental_requirements'] = 'EXPO_app:ExperimentalRequirementsList'
    
    foreign_keys['Has_standard'] = 'EXPO_app:ExperimentalStandardList'
    
    foreign_keys['Has_strategy'] = 'EXPO_app:ExperimentalDesignStrategyList'
    

    if request.method == 'POST':
        form = ExperimentalTechnologyForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ExperimentalTechnology(instance.name)
            onto.save()
            

            
            onto.save()

            
            fkstr = 'Has_experimental_requirements'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.ExperimentalRequirements(fkinstanceinput[0])
            onto.save()
            onto_instance.has_experimental_requirements.append(fkinstance) 
            
            fkstr = 'Has_standard'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.ExperimentalStandard(fkinstanceinput[0])
            onto.save()
            onto_instance.has_standard.append(fkinstance) 
            
            fkstr = 'Has_strategy'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.ExperimentalDesignStrategy(fkinstanceinput[0])
            onto.save()
            onto_instance.has_strategy.append(fkinstance) 
            
            onto.save()
            link = "/EXPO_app/ExperimentalTechnology/list/"
            return HttpResponseRedirect(link)
    else:
        form = ExperimentalTechnologyForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ExperimentalTechnologyCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ExperimentalRequirementsCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ExperimentalRequirements"]

    

    if request.method == 'POST':
        form = ExperimentalRequirementsForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ExperimentalRequirements(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/ExperimentalRequirements/list/"
            return HttpResponseRedirect(link)
    else:
        form = ExperimentalRequirementsForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ExperimentalRequirementsCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def HypothesisExplicitnessCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["HypothesisExplicitness"]

    

    if request.method == 'POST':
        form = HypothesisExplicitnessForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.HypothesisExplicitness(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/HypothesisExplicitness/list/"
            return HttpResponseRedirect(link)
    else:
        form = HypothesisExplicitnessForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "HypothesisExplicitnessCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def LinguisticExpressionCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["LinguisticExpression"]

    

    if request.method == 'POST':
        form = LinguisticExpressionForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.LinguisticExpression(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/LinguisticExpression/list/"
            return HttpResponseRedirect(link)
    else:
        form = LinguisticExpressionForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "LinguisticExpressionCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def FactRejectResearchHypothesisCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["FactRejectResearchHypothesis"]

    

    if request.method == 'POST':
        form = FactRejectResearchHypothesisForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.FactRejectResearchHypothesis(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/FactRejectResearchHypothesis/list/"
            return HttpResponseRedirect(link)
    else:
        form = FactRejectResearchHypothesisForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "FactRejectResearchHypothesisCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def FactSupportResearchHypothesisCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["FactSupportResearchHypothesis"]

    

    if request.method == 'POST':
        form = FactSupportResearchHypothesisForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.FactSupportResearchHypothesis(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/FactSupportResearchHypothesis/list/"
            return HttpResponseRedirect(link)
    else:
        form = FactSupportResearchHypothesisForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "FactSupportResearchHypothesisCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def NumberCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Number"]

    

    if request.method == 'POST':
        form = NumberForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Number(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/Number/list/"
            return HttpResponseRedirect(link)
    else:
        form = NumberForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "NumberCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def GeneralityOfHypothesisCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["GeneralityOfHypothesis"]

    

    if request.method == 'POST':
        form = GeneralityOfHypothesisForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.GeneralityOfHypothesis(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/GeneralityOfHypothesis/list/"
            return HttpResponseRedirect(link)
    else:
        form = GeneralityOfHypothesisForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "GeneralityOfHypothesisCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ActionGoalCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ActionGoal"]

    

    if request.method == 'POST':
        form = ActionGoalForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ActionGoal(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/ActionGoal/list/"
            return HttpResponseRedirect(link)
    else:
        form = ActionGoalForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ActionGoalCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ExperimentalGoalCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ExperimentalGoal"]

    
    foreign_keys['Has_representation'] = 'EXPO_app:RepresentationExperimentalGoalList'
    
    foreign_keys['Has_subgoal'] = 'EXPO_app:ExperimentalSubgoalList'
    

    if request.method == 'POST':
        form = ExperimentalGoalForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ExperimentalGoal(instance.name)
            onto.save()
            

            
            onto.save()

            
            fkstr = 'Has_representation'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.RepresentationExperimentalGoal(fkinstanceinput[0])
            onto.save()
            onto_instance.has_representation.append(fkinstance) 
            
            fkstr = 'Has_subgoal'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.ExperimentalSubgoal(fkinstanceinput[0])
            onto.save()
            onto_instance.has_subgoal.append(fkinstance) 
            
            onto.save()
            link = "/EXPO_app/ExperimentalGoal/list/"
            return HttpResponseRedirect(link)
    else:
        form = ExperimentalGoalForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ExperimentalGoalCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def MethodGoalCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["MethodGoal"]

    

    if request.method == 'POST':
        form = MethodGoalForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.MethodGoal(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/MethodGoal/list/"
            return HttpResponseRedirect(link)
    else:
        form = MethodGoalForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "MethodGoalCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def HypothesisFormationCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["HypothesisFormation"]

    

    if request.method == 'POST':
        form = HypothesisFormationForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.HypothesisFormation(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/HypothesisFormation/list/"
            return HttpResponseRedirect(link)
    else:
        form = HypothesisFormationForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "HypothesisFormationCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def HypothesisRepresentationCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["HypothesisRepresentation"]

    

    if request.method == 'POST':
        form = HypothesisRepresentationForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.HypothesisRepresentation(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/HypothesisRepresentation/list/"
            return HttpResponseRedirect(link)
    else:
        form = HypothesisRepresentationForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "HypothesisRepresentationCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def InformationGetheringCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["InformationGethering"]

    

    if request.method == 'POST':
        form = InformationGetheringForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.InformationGethering(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/InformationGethering/list/"
            return HttpResponseRedirect(link)
    else:
        form = InformationGetheringForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "InformationGetheringCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def SampleCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Sample"]

    

    if request.method == 'POST':
        form = SampleForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Sample(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/Sample/list/"
            return HttpResponseRedirect(link)
    else:
        form = SampleForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "SampleCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def VariabilityCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Variability"]

    

    if request.method == 'POST':
        form = VariabilityForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Variability(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/Variability/list/"
            return HttpResponseRedirect(link)
    else:
        form = VariabilityForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "VariabilityCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def TimePointCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["TimePoint"]

    

    if request.method == 'POST':
        form = TimePointForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.TimePoint(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/TimePoint/list/"
            return HttpResponseRedirect(link)
    else:
        form = TimePointForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "TimePointCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def UserCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["User"]

    
    foreign_keys['Has_admin_info'] = 'EXPO_app:AdminInfoUserList'
    
    foreign_keys['Has_login'] = 'EXPO_app:LoginNameList'
    
    foreign_keys['Has_pasword'] = 'EXPO_app:PasswordList'
    
    foreign_keys['Has_admin_info'] = 'EXPO_app:ThingList'
    

    if request.method == 'POST':
        form = UserForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.User(instance.name)
            onto.save()
            

            
            onto.save()

            
            fkstr = 'Has_admin_info'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.AdminInfoUser(fkinstanceinput[0])
            onto.save()
            onto_instance.has_admin_info.append(fkinstance) 
            
            fkstr = 'Has_login'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.LoginName(fkinstanceinput[0])
            onto.save()
            onto_instance.has_login.append(fkinstance) 
            
            fkstr = 'Has_pasword'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Password(fkinstanceinput[0])
            onto.save()
            onto_instance.has_pasword.append(fkinstance) 
            
            fkstr = 'Has_admin_info'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Thing(fkinstanceinput[0])
            onto.save()
            onto_instance.has_admin_info.append(fkinstance) 
            
            onto.save()
            link = "/EXPO_app/User/list/"
            return HttpResponseRedirect(link)
    else:
        form = UserForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "UserCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def LoginNameCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["LoginName"]

    

    if request.method == 'POST':
        form = LoginNameForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.LoginName(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/LoginName/list/"
            return HttpResponseRedirect(link)
    else:
        form = LoginNameForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "LoginNameCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def DomainModelCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["DomainModel"]

    

    if request.method == 'POST':
        form = DomainModelForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.DomainModel(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/DomainModel/list/"
            return HttpResponseRedirect(link)
    else:
        form = DomainModelForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "DomainModelCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def GroupExperimentalObjectCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["GroupExperimentalObject"]

    

    if request.method == 'POST':
        form = GroupExperimentalObjectForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.GroupExperimentalObject(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/GroupExperimentalObject/list/"
            return HttpResponseRedirect(link)
    else:
        form = GroupExperimentalObjectForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "GroupExperimentalObjectCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ObjectOfExperimentCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ObjectOfExperiment"]

    
    foreign_keys['Has_admin_info'] = 'EXPO_app:ThingList'
    

    if request.method == 'POST':
        form = ObjectOfExperimentForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ObjectOfExperiment(instance.name)
            onto.save()
            

            
            onto.save()

            
            fkstr = 'Has_admin_info'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Thing(fkinstanceinput[0])
            onto.save()
            onto_instance.has_admin_info.append(fkinstance) 
            
            onto.save()
            link = "/EXPO_app/ObjectOfExperiment/list/"
            return HttpResponseRedirect(link)
    else:
        form = ObjectOfExperimentForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ObjectOfExperimentCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ClassificationByModelCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ClassificationByModel"]

    
    foreign_keys['Has_number_of_factors'] = 'EXPO_app:NumberList'
    
    foreign_keys['Has_number_of_factors'] = 'EXPO_app:ThingList'
    

    if request.method == 'POST':
        form = ClassificationByModelForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ClassificationByModel(instance.name)
            onto.save()
            

            
            onto.save()

            
            fkstr = 'Has_number_of_factors'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Number(fkinstanceinput[0])
            onto.save()
            onto_instance.has_number_of_factors.append(fkinstance) 
            
            fkstr = 'Has_number_of_factors'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Thing(fkinstanceinput[0])
            onto.save()
            onto_instance.has_number_of_factors.append(fkinstance) 
            
            onto.save()
            link = "/EXPO_app/ClassificationByModel/list/"
            return HttpResponseRedirect(link)
    else:
        form = ClassificationByModelForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ClassificationByModelCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def StatisticsCharacteristicCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["StatisticsCharacteristic"]

    

    if request.method == 'POST':
        form = StatisticsCharacteristicForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.StatisticsCharacteristic(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/StatisticsCharacteristic/list/"
            return HttpResponseRedirect(link)
    else:
        form = StatisticsCharacteristicForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "StatisticsCharacteristicCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ParentGroupCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ParentGroup"]

    

    if request.method == 'POST':
        form = ParentGroupForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ParentGroup(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/ParentGroup/list/"
            return HttpResponseRedirect(link)
    else:
        form = ParentGroupForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ParentGroupCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def PasswordCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Password"]

    

    if request.method == 'POST':
        form = PasswordForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Password(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/Password/list/"
            return HttpResponseRedirect(link)
    else:
        form = PasswordForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "PasswordCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ProcedureExecuteExperimentCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ProcedureExecuteExperiment"]

    
    foreign_keys['Has_plan'] = 'EXPO_app:PlanOfExperimentalActionsList'
    
    foreign_keys['Has_representation'] = 'EXPO_app:RepresentationExperimentalExecutionProcedureList'
    
    foreign_keys['Has_status'] = 'EXPO_app:DocumentStageList'
    
    foreign_keys['Has_status'] = 'EXPO_app:PermissionStatusList'
    
    foreign_keys['Has_status'] = 'EXPO_app:ThingList'
    

    if request.method == 'POST':
        form = ProcedureExecuteExperimentForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ProcedureExecuteExperiment(instance.name)
            onto.save()
            

            
            onto.save()

            
            fkstr = 'Has_plan'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.PlanOfExperimentalActions(fkinstanceinput[0])
            onto.save()
            onto_instance.has_plan.append(fkinstance) 
            
            fkstr = 'Has_representation'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.RepresentationExperimentalExecutionProcedure(fkinstanceinput[0])
            onto.save()
            onto_instance.has_representation.append(fkinstance) 
            
            fkstr = 'Has_status'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.DocumentStage(fkinstanceinput[0])
            onto.save()
            onto_instance.has_status.append(fkinstance) 
            
            fkstr = 'Has_status'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.PermissionStatus(fkinstanceinput[0])
            onto.save()
            onto_instance.has_status.append(fkinstance) 
            
            fkstr = 'Has_status'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Thing(fkinstanceinput[0])
            onto.save()
            onto_instance.has_status.append(fkinstance) 
            
            onto.save()
            link = "/EXPO_app/ProcedureExecuteExperiment/list/"
            return HttpResponseRedirect(link)
    else:
        form = ProcedureExecuteExperimentForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ProcedureExecuteExperimentCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def PlanOfExperimentalActionsCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["PlanOfExperimentalActions"]

    

    if request.method == 'POST':
        form = PlanOfExperimentalActionsForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.PlanOfExperimentalActions(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/PlanOfExperimentalActions/list/"
            return HttpResponseRedirect(link)
    else:
        form = PlanOfExperimentalActionsForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "PlanOfExperimentalActionsCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def PoblemAnalysisCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["PoblemAnalysis"]

    

    if request.method == 'POST':
        form = PoblemAnalysisForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.PoblemAnalysis(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/PoblemAnalysis/list/"
            return HttpResponseRedirect(link)
    else:
        form = PoblemAnalysisForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "PoblemAnalysisCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def SubjectQualificationCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["SubjectQualification"]

    

    if request.method == 'POST':
        form = SubjectQualificationForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.SubjectQualification(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/SubjectQualification/list/"
            return HttpResponseRedirect(link)
    else:
        form = SubjectQualificationForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "SubjectQualificationCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BiblioReferenceCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["BiblioReference"]

    

    if request.method == 'POST':
        form = BiblioReferenceForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.BiblioReference(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/BiblioReference/list/"
            return HttpResponseRedirect(link)
    else:
        form = BiblioReferenceForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BiblioReferenceCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ModelCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Model"]

    
    foreign_keys['Has_representation'] = 'EXPO_app:ModelRepresentationList'
    

    if request.method == 'POST':
        form = ModelForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Model(instance.name)
            onto.save()
            

            
            onto.save()

            
            fkstr = 'Has_representation'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.ModelRepresentation(fkinstanceinput[0])
            onto.save()
            onto_instance.has_representation.append(fkinstance) 
            
            onto.save()
            link = "/EXPO_app/Model/list/"
            return HttpResponseRedirect(link)
    else:
        form = ModelForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ModelCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ModelRepresentationCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ModelRepresentation"]

    

    if request.method == 'POST':
        form = ModelRepresentationForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ModelRepresentation(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/ModelRepresentation/list/"
            return HttpResponseRedirect(link)
    else:
        form = ModelRepresentationForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ModelRepresentationCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def RepresentationExperimentalExecutionProcedureCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["RepresentationExperimentalExecutionProcedure"]

    

    if request.method == 'POST':
        form = RepresentationExperimentalExecutionProcedureForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.RepresentationExperimentalExecutionProcedure(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/RepresentationExperimentalExecutionProcedure/list/"
            return HttpResponseRedirect(link)
    else:
        form = RepresentationExperimentalExecutionProcedureForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "RepresentationExperimentalExecutionProcedureCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def RepresentationExperimentalGoalCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["RepresentationExperimentalGoal"]

    

    if request.method == 'POST':
        form = RepresentationExperimentalGoalForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.RepresentationExperimentalGoal(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/RepresentationExperimentalGoal/list/"
            return HttpResponseRedirect(link)
    else:
        form = RepresentationExperimentalGoalForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "RepresentationExperimentalGoalCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def SampleRepresentativenessCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["SampleRepresentativeness"]

    

    if request.method == 'POST':
        form = SampleRepresentativenessForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.SampleRepresentativeness(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/SampleRepresentativeness/list/"
            return HttpResponseRedirect(link)
    else:
        form = SampleRepresentativenessForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "SampleRepresentativenessCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def SampleSizeCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["SampleSize"]

    

    if request.method == 'POST':
        form = SampleSizeForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.SampleSize(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/SampleSize/list/"
            return HttpResponseRedirect(link)
    else:
        form = SampleSizeForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "SampleSizeCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def SamplingMethodCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["SamplingMethod"]

    

    if request.method == 'POST':
        form = SamplingMethodForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.SamplingMethod(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/SamplingMethod/list/"
            return HttpResponseRedirect(link)
    else:
        form = SamplingMethodForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "SamplingMethodCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def SystematicSamplingCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["SystematicSampling"]

    
    foreign_keys['Has_sampling_rule'] = 'EXPO_app:SamplingRuleList'
    

    if request.method == 'POST':
        form = SystematicSamplingForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.SystematicSampling(instance.name)
            onto.save()
            

            
            onto.save()

            
            fkstr = 'Has_sampling_rule'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.SamplingRule(fkinstanceinput[0])
            onto.save()
            onto_instance.has_sampling_rule.append(fkinstance) 
            
            onto.save()
            link = "/EXPO_app/SystematicSampling/list/"
            return HttpResponseRedirect(link)
    else:
        form = SystematicSamplingForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "SystematicSamplingCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def SamplingRuleCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["SamplingRule"]

    

    if request.method == 'POST':
        form = SamplingRuleForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.SamplingRule(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/SamplingRule/list/"
            return HttpResponseRedirect(link)
    else:
        form = SamplingRuleForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "SamplingRuleCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ExperimentalStandardCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ExperimentalStandard"]

    

    if request.method == 'POST':
        form = ExperimentalStandardForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ExperimentalStandard(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/ExperimentalStandard/list/"
            return HttpResponseRedirect(link)
    else:
        form = ExperimentalStandardForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ExperimentalStandardCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ErrorCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Error"]

    
    foreign_keys['Has_statistics_characteristic'] = 'EXPO_app:DispersionList'
    
    foreign_keys['Has_statistics_characteristic'] = 'EXPO_app:LevelOfSignificanceList'
    
    foreign_keys['Has_statistics_characteristic'] = 'EXPO_app:ThingList'
    

    if request.method == 'POST':
        form = ErrorForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Error(instance.name)
            onto.save()
            

            
            onto.save()

            
            fkstr = 'Has_statistics_characteristic'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Dispersion(fkinstanceinput[0])
            onto.save()
            onto_instance.has_statistics_characteristic.append(fkinstance) 
            
            fkstr = 'Has_statistics_characteristic'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.LevelOfSignificance(fkinstanceinput[0])
            onto.save()
            onto_instance.has_statistics_characteristic.append(fkinstance) 
            
            fkstr = 'Has_statistics_characteristic'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Thing(fkinstanceinput[0])
            onto.save()
            onto_instance.has_statistics_characteristic.append(fkinstance) 
            
            onto.save()
            link = "/EXPO_app/Error/list/"
            return HttpResponseRedirect(link)
    else:
        form = ErrorForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ErrorCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def DispersionCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Dispersion"]

    

    if request.method == 'POST':
        form = DispersionForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Dispersion(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/Dispersion/list/"
            return HttpResponseRedirect(link)
    else:
        form = DispersionForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "DispersionCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def LevelOfSignificanceCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["LevelOfSignificance"]

    

    if request.method == 'POST':
        form = LevelOfSignificanceForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.LevelOfSignificance(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/LevelOfSignificance/list/"
            return HttpResponseRedirect(link)
    else:
        form = LevelOfSignificanceForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "LevelOfSignificanceCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def StatusExperimentalDocumentCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["StatusExperimentalDocument"]

    

    if request.method == 'POST':
        form = StatusExperimentalDocumentForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.StatusExperimentalDocument(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/StatusExperimentalDocument/list/"
            return HttpResponseRedirect(link)
    else:
        form = StatusExperimentalDocumentForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "StatusExperimentalDocumentCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ExperimentalDesignStrategyCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ExperimentalDesignStrategy"]

    

    if request.method == 'POST':
        form = ExperimentalDesignStrategyForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ExperimentalDesignStrategy(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/ExperimentalDesignStrategy/list/"
            return HttpResponseRedirect(link)
    else:
        form = ExperimentalDesignStrategyForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ExperimentalDesignStrategyCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ExperimentalSubgoalCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ExperimentalSubgoal"]

    

    if request.method == 'POST':
        form = ExperimentalSubgoalForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ExperimentalSubgoal(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/ExperimentalSubgoal/list/"
            return HttpResponseRedirect(link)
    else:
        form = ExperimentalSubgoalForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ExperimentalSubgoalCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def SubjectMethodCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["SubjectMethod"]

    

    if request.method == 'POST':
        form = SubjectMethodForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.SubjectMethod(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/SubjectMethod/list/"
            return HttpResponseRedirect(link)
    else:
        form = SubjectMethodForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "SubjectMethodCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BaconianExperimentCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["BaconianExperiment"]

    
    foreign_keys['Has_synonym'] = 'EXPO_app:SynonymousExternalConceptList'
    
    foreign_keys['Has_synonym'] = 'EXPO_app:ThingList'
    

    if request.method == 'POST':
        form = BaconianExperimentForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.BaconianExperiment(instance.name)
            onto.save()
            

            
            onto.save()

            
            fkstr = 'Has_synonym'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.SynonymousExternalConcept(fkinstanceinput[0])
            onto.save()
            onto_instance.has_synonym.append(fkinstance) 
            
            fkstr = 'Has_synonym'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Thing(fkinstanceinput[0])
            onto.save()
            onto_instance.has_synonym.append(fkinstance) 
            
            onto.save()
            link = "/EXPO_app/BaconianExperiment/list/"
            return HttpResponseRedirect(link)
    else:
        form = BaconianExperimentForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BaconianExperimentCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def TargetVariableCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["TargetVariable"]

    

    if request.method == 'POST':
        form = TargetVariableForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.TargetVariable(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/TargetVariable/list/"
            return HttpResponseRedirect(link)
    else:
        form = TargetVariableForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "TargetVariableCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def TitleCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Title"]

    

    if request.method == 'POST':
        form = TitleForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Title(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/Title/list/"
            return HttpResponseRedirect(link)
    else:
        form = TitleForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "TitleCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ValueEstimateCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ValueEstimate"]

    
    foreign_keys['Has_value_of_variable_'] = 'EXPO_app:ValueOfVariableList'
    

    if request.method == 'POST':
        form = ValueEstimateForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ValueEstimate(instance.name)
            onto.save()
            

            
            onto.save()

            
            fkstr = 'Has_value_of_variable_'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.ValueOfVariable(fkinstanceinput[0])
            onto.save()
            onto_instance.has_value_of_variable_.append(fkinstance) 
            
            onto.save()
            link = "/EXPO_app/ValueEstimate/list/"
            return HttpResponseRedirect(link)
    else:
        form = ValueEstimateForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ValueEstimateCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ValueOfVariableCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ValueOfVariable"]

    

    if request.method == 'POST':
        form = ValueOfVariableForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ValueOfVariable(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/ValueOfVariable/list/"
            return HttpResponseRedirect(link)
    else:
        form = ValueOfVariableForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ValueOfVariableCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def RoleCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Role"]

    

    if request.method == 'POST':
        form = RoleForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Role(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/Role/list/"
            return HttpResponseRedirect(link)
    else:
        form = RoleForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "RoleCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def CorpuscularObjectCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["CorpuscularObject"]

    

    if request.method == 'POST':
        form = CorpuscularObjectForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.CorpuscularObject(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/CorpuscularObject/list/"
            return HttpResponseRedirect(link)
    else:
        form = CorpuscularObjectForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "CorpuscularObjectCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def AttributeRoleCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["AttributeRole"]

    

    if request.method == 'POST':
        form = AttributeRoleForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.AttributeRole(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/AttributeRole/list/"
            return HttpResponseRedirect(link)
    else:
        form = AttributeRoleForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "AttributeRoleCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def AttributeCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Attribute"]

    

    if request.method == 'POST':
        form = AttributeForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Attribute(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/Attribute/list/"
            return HttpResponseRedirect(link)
    else:
        form = AttributeForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "AttributeCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ProcedureCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Procedure"]

    

    if request.method == 'POST':
        form = ProcedureForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Procedure(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/Procedure/list/"
            return HttpResponseRedirect(link)
    else:
        form = ProcedureForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ProcedureCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def AdministrativeInformationCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["AdministrativeInformation"]

    

    if request.method == 'POST':
        form = AdministrativeInformationForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.AdministrativeInformation(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/AdministrativeInformation/list/"
            return HttpResponseRedirect(link)
    else:
        form = AdministrativeInformationForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "AdministrativeInformationCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def TitleExperimentCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["TitleExperiment"]

    

    if request.method == 'POST':
        form = TitleExperimentForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.TitleExperiment(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/TitleExperiment/list/"
            return HttpResponseRedirect(link)
    else:
        form = TitleExperimentForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "TitleExperimentCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def NameEquipmentCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["NameEquipment"]

    

    if request.method == 'POST':
        form = NameEquipmentForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.NameEquipment(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/NameEquipment/list/"
            return HttpResponseRedirect(link)
    else:
        form = NameEquipmentForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "NameEquipmentCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def PersonalNameCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["PersonalName"]

    

    if request.method == 'POST':
        form = PersonalNameForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.PersonalName(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/PersonalName/list/"
            return HttpResponseRedirect(link)
    else:
        form = PersonalNameForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "PersonalNameCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def FieldOfStudyCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["FieldOfStudy"]

    

    if request.method == 'POST':
        form = FieldOfStudyForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.FieldOfStudy(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/FieldOfStudy/list/"
            return HttpResponseRedirect(link)
    else:
        form = FieldOfStudyForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "FieldOfStudyCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def RelatedDomainCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["RelatedDomain"]

    

    if request.method == 'POST':
        form = RelatedDomainForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.RelatedDomain(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/RelatedDomain/list/"
            return HttpResponseRedirect(link)
    else:
        form = RelatedDomainForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "RelatedDomainCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ProductRoleCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ProductRole"]

    

    if request.method == 'POST':
        form = ProductRoleForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ProductRole(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/ProductRole/list/"
            return HttpResponseRedirect(link)
    else:
        form = ProductRoleForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ProductRoleCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ScientificTaskCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ScientificTask"]

    

    if request.method == 'POST':
        form = ScientificTaskForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ScientificTask(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/ScientificTask/list/"
            return HttpResponseRedirect(link)
    else:
        form = ScientificTaskForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ScientificTaskCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ExecutionOfExperimentCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ExecutionOfExperiment"]

    

    if request.method == 'POST':
        form = ExecutionOfExperimentForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ExecutionOfExperiment(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/ExecutionOfExperiment/list/"
            return HttpResponseRedirect(link)
    else:
        form = ExecutionOfExperimentForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ExecutionOfExperimentCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def CorporateNameCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["CorporateName"]

    

    if request.method == 'POST':
        form = CorporateNameForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.CorporateName(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/CorporateName/list/"
            return HttpResponseRedirect(link)
    else:
        form = CorporateNameForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "CorporateNameCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def AttributeOfActionCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["AttributeOfAction"]

    

    if request.method == 'POST':
        form = AttributeOfActionForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.AttributeOfAction(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/AttributeOfAction/list/"
            return HttpResponseRedirect(link)
    else:
        form = AttributeOfActionForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "AttributeOfActionCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def SentientAgentCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["SentientAgent"]

    

    if request.method == 'POST':
        form = SentientAgentForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.SentientAgent(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/SentientAgent/list/"
            return HttpResponseRedirect(link)
    else:
        form = SentientAgentForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "SentientAgentCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def RobotCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Robot"]

    

    if request.method == 'POST':
        form = RobotForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Robot(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/Robot/list/"
            return HttpResponseRedirect(link)
    else:
        form = RobotForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "RobotCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def GroupCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Group"]

    

    if request.method == 'POST':
        form = GroupForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Group(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/Group/list/"
            return HttpResponseRedirect(link)
    else:
        form = GroupForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "GroupCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def EntityCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Entity"]

    

    if request.method == 'POST':
        form = EntityForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Entity(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/Entity/list/"
            return HttpResponseRedirect(link)
    else:
        form = EntityForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "EntityCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ResearchMethodCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ResearchMethod"]

    

    if request.method == 'POST':
        form = ResearchMethodForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ResearchMethod(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/ResearchMethod/list/"
            return HttpResponseRedirect(link)
    else:
        form = ResearchMethodForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ResearchMethodCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def RequirementsCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Requirements"]

    

    if request.method == 'POST':
        form = RequirementsForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Requirements(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/Requirements/list/"
            return HttpResponseRedirect(link)
    else:
        form = RequirementsForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "RequirementsCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def AuthorSOPCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["AuthorSOP"]

    

    if request.method == 'POST':
        form = AuthorSOPForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.AuthorSOP(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/AuthorSOP/list/"
            return HttpResponseRedirect(link)
    else:
        form = AuthorSOPForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "AuthorSOPCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def SubjectRoleCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["SubjectRole"]

    

    if request.method == 'POST':
        form = SubjectRoleForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.SubjectRole(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/SubjectRole/list/"
            return HttpResponseRedirect(link)
    else:
        form = SubjectRoleForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "SubjectRoleCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ClassificationCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Classification"]

    

    if request.method == 'POST':
        form = ClassificationForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Classification(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/Classification/list/"
            return HttpResponseRedirect(link)
    else:
        form = ClassificationForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ClassificationCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def FactCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Fact"]

    

    if request.method == 'POST':
        form = FactForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Fact(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/Fact/list/"
            return HttpResponseRedirect(link)
    else:
        form = FactForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "FactCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ModelAssumptionCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ModelAssumption"]

    

    if request.method == 'POST':
        form = ModelAssumptionForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ModelAssumption(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/ModelAssumption/list/"
            return HttpResponseRedirect(link)
    else:
        form = ModelAssumptionForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ModelAssumptionCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def VariableCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Variable"]

    

    if request.method == 'POST':
        form = VariableForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Variable(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/Variable/list/"
            return HttpResponseRedirect(link)
    else:
        form = VariableForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "VariableCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def AttributeOfHypothesisCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["AttributeOfHypothesis"]

    

    if request.method == 'POST':
        form = AttributeOfHypothesisForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.AttributeOfHypothesis(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/AttributeOfHypothesis/list/"
            return HttpResponseRedirect(link)
    else:
        form = AttributeOfHypothesisForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "AttributeOfHypothesisCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ContentBearingObjectCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ContentBearingObject"]

    

    if request.method == 'POST':
        form = ContentBearingObjectForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ContentBearingObject(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/ContentBearingObject/list/"
            return HttpResponseRedirect(link)
    else:
        form = ContentBearingObjectForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ContentBearingObjectCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def AbstractCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Abstract"]

    

    if request.method == 'POST':
        form = AbstractForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Abstract(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/Abstract/list/"
            return HttpResponseRedirect(link)
    else:
        form = AbstractForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "AbstractCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def QuantityCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Quantity"]

    

    if request.method == 'POST':
        form = QuantityForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Quantity(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/Quantity/list/"
            return HttpResponseRedirect(link)
    else:
        form = QuantityForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "QuantityCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def RelationCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Relation"]

    

    if request.method == 'POST':
        form = RelationForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Relation(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/Relation/list/"
            return HttpResponseRedirect(link)
    else:
        form = RelationForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "RelationCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ProcessrelatedRoleCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ProcessrelatedRole"]

    

    if request.method == 'POST':
        form = ProcessrelatedRoleForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ProcessrelatedRole(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/ProcessrelatedRole/list/"
            return HttpResponseRedirect(link)
    else:
        form = ProcessrelatedRoleForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ProcessrelatedRoleCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ContentBearingPhysicalCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ContentBearingPhysical"]

    

    if request.method == 'POST':
        form = ContentBearingPhysicalForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ContentBearingPhysical(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/ContentBearingPhysical/list/"
            return HttpResponseRedirect(link)
    else:
        form = ContentBearingPhysicalForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ContentBearingPhysicalCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def PhysicalQuantityCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["PhysicalQuantity"]

    

    if request.method == 'POST':
        form = PhysicalQuantityForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.PhysicalQuantity(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/PhysicalQuantity/list/"
            return HttpResponseRedirect(link)
    else:
        form = PhysicalQuantityForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "PhysicalQuantityCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def GoalCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Goal"]

    

    if request.method == 'POST':
        form = GoalForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Goal(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/Goal/list/"
            return HttpResponseRedirect(link)
    else:
        form = GoalForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "GoalCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def RepresentationExperimentalObservationCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["RepresentationExperimentalObservation"]

    

    if request.method == 'POST':
        form = RepresentationExperimentalObservationForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.RepresentationExperimentalObservation(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/RepresentationExperimentalObservation/list/"
            return HttpResponseRedirect(link)
    else:
        form = RepresentationExperimentalObservationForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "RepresentationExperimentalObservationCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def RepresentationExperimentalResultsCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["RepresentationExperimentalResults"]

    

    if request.method == 'POST':
        form = RepresentationExperimentalResultsForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.RepresentationExperimentalResults(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/RepresentationExperimentalResults/list/"
            return HttpResponseRedirect(link)
    else:
        form = RepresentationExperimentalResultsForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "RepresentationExperimentalResultsCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def AttributeGroupCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["AttributeGroup"]

    

    if request.method == 'POST':
        form = AttributeGroupForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.AttributeGroup(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/AttributeGroup/list/"
            return HttpResponseRedirect(link)
    else:
        form = AttributeGroupForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "AttributeGroupCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def TimePositionCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["TimePosition"]

    

    if request.method == 'POST':
        form = TimePositionForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.TimePosition(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/TimePosition/list/"
            return HttpResponseRedirect(link)
    else:
        form = TimePositionForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "TimePositionCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ActorRoleCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ActorRole"]

    

    if request.method == 'POST':
        form = ActorRoleForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ActorRole(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/ActorRole/list/"
            return HttpResponseRedirect(link)
    else:
        form = ActorRoleForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ActorRoleCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def DocumentStageCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["DocumentStage"]

    

    if request.method == 'POST':
        form = DocumentStageForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.DocumentStage(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/DocumentStage/list/"
            return HttpResponseRedirect(link)
    else:
        form = DocumentStageForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "DocumentStageCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def PermissionStatusCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["PermissionStatus"]

    

    if request.method == 'POST':
        form = PermissionStatusForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.PermissionStatus(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/PermissionStatus/list/"
            return HttpResponseRedirect(link)
    else:
        form = PermissionStatusForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "PermissionStatusCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def PlanCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Plan"]

    

    if request.method == 'POST':
        form = PlanForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Plan(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/Plan/list/"
            return HttpResponseRedirect(link)
    else:
        form = PlanForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "PlanCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ReferenceCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Reference"]

    

    if request.method == 'POST':
        form = ReferenceForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Reference(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/Reference/list/"
            return HttpResponseRedirect(link)
    else:
        form = ReferenceForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ReferenceCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def AttributeSampleCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["AttributeSample"]

    

    if request.method == 'POST':
        form = AttributeSampleForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.AttributeSample(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/AttributeSample/list/"
            return HttpResponseRedirect(link)
    else:
        form = AttributeSampleForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "AttributeSampleCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ExperimentalRuleCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ExperimentalRule"]

    

    if request.method == 'POST':
        form = ExperimentalRuleForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ExperimentalRule(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/ExperimentalRule/list/"
            return HttpResponseRedirect(link)
    else:
        form = ExperimentalRuleForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ExperimentalRuleCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def RobustnessCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Robustness"]

    

    if request.method == 'POST':
        form = RobustnessForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Robustness(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/Robustness/list/"
            return HttpResponseRedirect(link)
    else:
        form = RobustnessForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "RobustnessCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ValidityCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Validity"]

    

    if request.method == 'POST':
        form = ValidityForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Validity(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/Validity/list/"
            return HttpResponseRedirect(link)
    else:
        form = ValidityForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ValidityCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def AttributeOfDocumentCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["AttributeOfDocument"]

    

    if request.method == 'POST':
        form = AttributeOfDocumentForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.AttributeOfDocument(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/AttributeOfDocument/list/"
            return HttpResponseRedirect(link)
    else:
        form = AttributeOfDocumentForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "AttributeOfDocumentCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def PhysicalExperimentCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["PhysicalExperiment"]

    

    if request.method == 'POST':
        form = PhysicalExperimentForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.PhysicalExperiment(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/PhysicalExperiment/list/"
            return HttpResponseRedirect(link)
    else:
        form = PhysicalExperimentForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "PhysicalExperimentCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ComputationalDataCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ComputationalData"]

    

    if request.method == 'POST':
        form = ComputationalDataForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ComputationalData(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/ComputationalData/list/"
            return HttpResponseRedirect(link)
    else:
        form = ComputationalDataForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ComputationalDataCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def PredicateCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Predicate"]

    

    if request.method == 'POST':
        form = PredicateForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Predicate(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/Predicate/list/"
            return HttpResponseRedirect(link)
    else:
        form = PredicateForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "PredicateCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def SelfConnectedObjectCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["SelfConnectedObject"]

    

    if request.method == 'POST':
        form = SelfConnectedObjectForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.SelfConnectedObject(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/SelfConnectedObject/list/"
            return HttpResponseRedirect(link)
    else:
        form = SelfConnectedObjectForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "SelfConnectedObjectCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ScientificActivityCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ScientificActivity"]

    

    if request.method == 'POST':
        form = ScientificActivityForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ScientificActivity(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/ScientificActivity/list/"
            return HttpResponseRedirect(link)
    else:
        form = ScientificActivityForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ScientificActivityCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def FormingClassificationSystemCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["FormingClassificationSystem"]

    

    if request.method == 'POST':
        form = FormingClassificationSystemForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.FormingClassificationSystem(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/FormingClassificationSystem/list/"
            return HttpResponseRedirect(link)
    else:
        form = FormingClassificationSystemForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "FormingClassificationSystemCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def HypothesisFormingCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["HypothesisForming"]

    

    if request.method == 'POST':
        form = HypothesisFormingForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.HypothesisForming(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/HypothesisForming/list/"
            return HttpResponseRedirect(link)
    else:
        form = HypothesisFormingForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "HypothesisFormingCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def InterpretingResultCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["InterpretingResult"]

    

    if request.method == 'POST':
        form = InterpretingResultForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.InterpretingResult(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/InterpretingResult/list/"
            return HttpResponseRedirect(link)
    else:
        form = InterpretingResultForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "InterpretingResultCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ProcessProblemAnalysisCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ProcessProblemAnalysis"]

    

    if request.method == 'POST':
        form = ProcessProblemAnalysisForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ProcessProblemAnalysis(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/ProcessProblemAnalysis/list/"
            return HttpResponseRedirect(link)
    else:
        form = ProcessProblemAnalysisForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ProcessProblemAnalysisCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ResultEvaluatingCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ResultEvaluating"]

    

    if request.method == 'POST':
        form = ResultEvaluatingForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ResultEvaluating(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/ResultEvaluating/list/"
            return HttpResponseRedirect(link)
    else:
        form = ResultEvaluatingForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ResultEvaluatingCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def AttributeOfModelCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["AttributeOfModel"]

    

    if request.method == 'POST':
        form = AttributeOfModelForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.AttributeOfModel(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/AttributeOfModel/list/"
            return HttpResponseRedirect(link)
    else:
        form = AttributeOfModelForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "AttributeOfModelCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def AttributeOfVariableCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["AttributeOfVariable"]

    

    if request.method == 'POST':
        form = AttributeOfVariableForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.AttributeOfVariable(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/AttributeOfVariable/list/"
            return HttpResponseRedirect(link)
    else:
        form = AttributeOfVariableForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "AttributeOfVariableCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def AgentCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Agent"]

    

    if request.method == 'POST':
        form = AgentForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Agent(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/Agent/list/"
            return HttpResponseRedirect(link)
    else:
        form = AgentForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "AgentCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def CollectionCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Collection"]

    

    if request.method == 'POST':
        form = CollectionForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Collection(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/Collection/list/"
            return HttpResponseRedirect(link)
    else:
        form = CollectionForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "CollectionCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def EperimentalDesignTaskCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["EperimentalDesignTask"]

    

    if request.method == 'POST':
        form = EperimentalDesignTaskForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.EperimentalDesignTask(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/EperimentalDesignTask/list/"
            return HttpResponseRedirect(link)
    else:
        form = EperimentalDesignTaskForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "EperimentalDesignTaskCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def PhysicalCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Physical"]

    

    if request.method == 'POST':
        form = PhysicalForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Physical(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/Physical/list/"
            return HttpResponseRedirect(link)
    else:
        form = PhysicalForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "PhysicalCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ObjectCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Object"]

    

    if request.method == 'POST':
        form = ObjectForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Object(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/Object/list/"
            return HttpResponseRedirect(link)
    else:
        form = ObjectForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ObjectCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ProcessCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Process"]

    

    if request.method == 'POST':
        form = ProcessForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Process(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/Process/list/"
            return HttpResponseRedirect(link)
    else:
        form = ProcessForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ProcessCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def TaskRoleCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["TaskRole"]

    

    if request.method == 'POST':
        form = TaskRoleForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.TaskRole(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/TaskRole/list/"
            return HttpResponseRedirect(link)
    else:
        form = TaskRoleForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "TaskRoleCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def TimeMeasureCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["TimeMeasure"]

    

    if request.method == 'POST':
        form = TimeMeasureForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.TimeMeasure(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/TimeMeasure/list/"
            return HttpResponseRedirect(link)
    else:
        form = TimeMeasureForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "TimeMeasureCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ActionrelatedRoleCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ActionrelatedRole"]

    

    if request.method == 'POST':
        form = ActionrelatedRoleForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ActionrelatedRole(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/ActionrelatedRole/list/"
            return HttpResponseRedirect(link)
    else:
        form = ActionrelatedRoleForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ActionrelatedRoleCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def AbductiveHypothesisCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["AbductiveHypothesis"]

    

    if request.method == 'POST':
        form = AbductiveHypothesisForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.AbductiveHypothesis(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/AbductiveHypothesis/list/"
            return HttpResponseRedirect(link)
    else:
        form = AbductiveHypothesisForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "AbductiveHypothesisCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def InductiveHypothesisCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["InductiveHypothesis"]

    

    if request.method == 'POST':
        form = InductiveHypothesisForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.InductiveHypothesis(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/InductiveHypothesis/list/"
            return HttpResponseRedirect(link)
    else:
        form = InductiveHypothesisForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "InductiveHypothesisCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def AdequacyCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Adequacy"]

    

    if request.method == 'POST':
        form = AdequacyForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Adequacy(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/Adequacy/list/"
            return HttpResponseRedirect(link)
    else:
        form = AdequacyForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "AdequacyCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def AlternativeHypothesisCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["AlternativeHypothesis"]

    

    if request.method == 'POST':
        form = AlternativeHypothesisForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.AlternativeHypothesis(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/AlternativeHypothesis/list/"
            return HttpResponseRedirect(link)
    else:
        form = AlternativeHypothesisForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "AlternativeHypothesisCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def NullHypothesisCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["NullHypothesis"]

    

    if request.method == 'POST':
        form = NullHypothesisForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.NullHypothesis(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/NullHypothesis/list/"
            return HttpResponseRedirect(link)
    else:
        form = NullHypothesisForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "NullHypothesisCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ResearchHypothesisCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ResearchHypothesis"]

    

    if request.method == 'POST':
        form = ResearchHypothesisForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ResearchHypothesis(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/ResearchHypothesis/list/"
            return HttpResponseRedirect(link)
    else:
        form = ResearchHypothesisForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ResearchHypothesisCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ArticleCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Article"]

    

    if request.method == 'POST':
        form = ArticleForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Article(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/Article/list/"
            return HttpResponseRedirect(link)
    else:
        form = ArticleForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ArticleCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def TextCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Text"]

    

    if request.method == 'POST':
        form = TextForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Text(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/Text/list/"
            return HttpResponseRedirect(link)
    else:
        form = TextForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "TextCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ArtificialLanguageCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ArtificialLanguage"]

    

    if request.method == 'POST':
        form = ArtificialLanguageForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ArtificialLanguage(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/ArtificialLanguage/list/"
            return HttpResponseRedirect(link)
    else:
        form = ArtificialLanguageForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ArtificialLanguageCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def LanguageCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Language"]

    

    if request.method == 'POST':
        form = LanguageForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Language(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/Language/list/"
            return HttpResponseRedirect(link)
    else:
        form = LanguageForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "LanguageCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def HumanLanguageCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["HumanLanguage"]

    

    if request.method == 'POST':
        form = HumanLanguageForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.HumanLanguage(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/HumanLanguage/list/"
            return HttpResponseRedirect(link)
    else:
        form = HumanLanguageForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "HumanLanguageCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def SentenceCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Sentence"]

    

    if request.method == 'POST':
        form = SentenceForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Sentence(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/Sentence/list/"
            return HttpResponseRedirect(link)
    else:
        form = SentenceForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "SentenceCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def AtomicActionCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["AtomicAction"]

    

    if request.method == 'POST':
        form = AtomicActionForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.AtomicAction(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/AtomicAction/list/"
            return HttpResponseRedirect(link)
    else:
        form = AtomicActionForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "AtomicActionCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ComplexActionCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ComplexAction"]

    

    if request.method == 'POST':
        form = ComplexActionForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ComplexAction(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/ComplexAction/list/"
            return HttpResponseRedirect(link)
    else:
        form = ComplexActionForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ComplexActionCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def AuthorProtocolCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["AuthorProtocol"]

    

    if request.method == 'POST':
        form = AuthorProtocolForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.AuthorProtocol(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/AuthorProtocol/list/"
            return HttpResponseRedirect(link)
    else:
        form = AuthorProtocolForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "AuthorProtocolCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def BookCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Book"]

    

    if request.method == 'POST':
        form = BookForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Book(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/Book/list/"
            return HttpResponseRedirect(link)
    else:
        form = BookForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "BookCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def CalculableVariableCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["CalculableVariable"]

    

    if request.method == 'POST':
        form = CalculableVariableForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.CalculableVariable(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/CalculableVariable/list/"
            return HttpResponseRedirect(link)
    else:
        form = CalculableVariableForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "CalculableVariableCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def CapacityRequirementsCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["CapacityRequirements"]

    

    if request.method == 'POST':
        form = CapacityRequirementsForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.CapacityRequirements(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/CapacityRequirements/list/"
            return HttpResponseRedirect(link)
    else:
        form = CapacityRequirementsForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "CapacityRequirementsCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def EnvironmentalRequirementsCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["EnvironmentalRequirements"]

    

    if request.method == 'POST':
        form = EnvironmentalRequirementsForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.EnvironmentalRequirements(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/EnvironmentalRequirements/list/"
            return HttpResponseRedirect(link)
    else:
        form = EnvironmentalRequirementsForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "EnvironmentalRequirementsCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def FinancialRequirementsCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["FinancialRequirements"]

    

    if request.method == 'POST':
        form = FinancialRequirementsForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.FinancialRequirements(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/FinancialRequirements/list/"
            return HttpResponseRedirect(link)
    else:
        form = FinancialRequirementsForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "FinancialRequirementsCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ClassificationByDomainCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ClassificationByDomain"]

    

    if request.method == 'POST':
        form = ClassificationByDomainForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ClassificationByDomain(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/ClassificationByDomain/list/"
            return HttpResponseRedirect(link)
    else:
        form = ClassificationByDomainForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ClassificationByDomainCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ClassifyingCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Classifying"]

    

    if request.method == 'POST':
        form = ClassifyingForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Classifying(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/Classifying/list/"
            return HttpResponseRedirect(link)
    else:
        form = ClassifyingForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ClassifyingCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def DesigningExperimentCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["DesigningExperiment"]

    

    if request.method == 'POST':
        form = DesigningExperimentForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.DesigningExperiment(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/DesigningExperiment/list/"
            return HttpResponseRedirect(link)
    else:
        form = DesigningExperimentForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "DesigningExperimentCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def CoarseErrorCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["CoarseError"]

    

    if request.method == 'POST':
        form = CoarseErrorForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.CoarseError(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/CoarseError/list/"
            return HttpResponseRedirect(link)
    else:
        form = CoarseErrorForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "CoarseErrorCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def MeasurementErrorCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["MeasurementError"]

    

    if request.method == 'POST':
        form = MeasurementErrorForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.MeasurementError(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/MeasurementError/list/"
            return HttpResponseRedirect(link)
    else:
        form = MeasurementErrorForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "MeasurementErrorCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ComparisonControl_TargetGroupsCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ComparisonControl_TargetGroups"]

    

    if request.method == 'POST':
        form = ComparisonControl_TargetGroupsForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ComparisonControl_TargetGroups(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/ComparisonControl_TargetGroups/list/"
            return HttpResponseRedirect(link)
    else:
        form = ComparisonControl_TargetGroupsForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ComparisonControl_TargetGroupsCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def PairedComparisonCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["PairedComparison"]

    

    if request.method == 'POST':
        form = PairedComparisonForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.PairedComparison(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/PairedComparison/list/"
            return HttpResponseRedirect(link)
    else:
        form = PairedComparisonForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "PairedComparisonCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def PairedComparisonOfMatchingGroupsCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["PairedComparisonOfMatchingGroups"]

    

    if request.method == 'POST':
        form = PairedComparisonOfMatchingGroupsForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.PairedComparisonOfMatchingGroups(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/PairedComparisonOfMatchingGroups/list/"
            return HttpResponseRedirect(link)
    else:
        form = PairedComparisonOfMatchingGroupsForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "PairedComparisonOfMatchingGroupsCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def PairedComparisonOfSingleSampleGroupsCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["PairedComparisonOfSingleSampleGroups"]

    

    if request.method == 'POST':
        form = PairedComparisonOfSingleSampleGroupsForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.PairedComparisonOfSingleSampleGroups(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/PairedComparisonOfSingleSampleGroups/list/"
            return HttpResponseRedirect(link)
    else:
        form = PairedComparisonOfSingleSampleGroupsForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "PairedComparisonOfSingleSampleGroupsCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def QualityControlCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["QualityControl"]

    

    if request.method == 'POST':
        form = QualityControlForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.QualityControl(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/QualityControl/list/"
            return HttpResponseRedirect(link)
    else:
        form = QualityControlForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "QualityControlCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ComplexHypothesisCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ComplexHypothesis"]

    

    if request.method == 'POST':
        form = ComplexHypothesisForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ComplexHypothesis(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/ComplexHypothesis/list/"
            return HttpResponseRedirect(link)
    else:
        form = ComplexHypothesisForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ComplexHypothesisCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def SingleHypothesisCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["SingleHypothesis"]

    

    if request.method == 'POST':
        form = SingleHypothesisForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.SingleHypothesis(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/SingleHypothesis/list/"
            return HttpResponseRedirect(link)
    else:
        form = SingleHypothesisForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "SingleHypothesisCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ComputationalExperimentCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ComputationalExperiment"]

    

    if request.method == 'POST':
        form = ComputationalExperimentForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ComputationalExperiment(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/ComputationalExperiment/list/"
            return HttpResponseRedirect(link)
    else:
        form = ComputationalExperimentForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ComputationalExperimentCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ComputeGoalCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ComputeGoal"]

    

    if request.method == 'POST':
        form = ComputeGoalForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ComputeGoal(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/ComputeGoal/list/"
            return HttpResponseRedirect(link)
    else:
        form = ComputeGoalForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ComputeGoalCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ConfirmGoalCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ConfirmGoal"]

    

    if request.method == 'POST':
        form = ConfirmGoalForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ConfirmGoal(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/ConfirmGoal/list/"
            return HttpResponseRedirect(link)
    else:
        form = ConfirmGoalForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ConfirmGoalCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ExplainGoalCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ExplainGoal"]

    

    if request.method == 'POST':
        form = ExplainGoalForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ExplainGoal(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/ExplainGoal/list/"
            return HttpResponseRedirect(link)
    else:
        form = ExplainGoalForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ExplainGoalCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def InvestigateGoalCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["InvestigateGoal"]

    

    if request.method == 'POST':
        form = InvestigateGoalForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.InvestigateGoal(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/InvestigateGoal/list/"
            return HttpResponseRedirect(link)
    else:
        form = InvestigateGoalForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "InvestigateGoalCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ComputerSimulationCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ComputerSimulation"]

    

    if request.method == 'POST':
        form = ComputerSimulationForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ComputerSimulation(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/ComputerSimulation/list/"
            return HttpResponseRedirect(link)
    else:
        form = ComputerSimulationForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ComputerSimulationCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ConstantQuantityCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ConstantQuantity"]

    

    if request.method == 'POST':
        form = ConstantQuantityForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ConstantQuantity(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/ConstantQuantity/list/"
            return HttpResponseRedirect(link)
    else:
        form = ConstantQuantityForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ConstantQuantityCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ControllabilityCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Controllability"]

    

    if request.method == 'POST':
        form = ControllabilityForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Controllability(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/Controllability/list/"
            return HttpResponseRedirect(link)
    else:
        form = ControllabilityForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ControllabilityCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def IndependenceCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Independence"]

    

    if request.method == 'POST':
        form = IndependenceForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Independence(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/Independence/list/"
            return HttpResponseRedirect(link)
    else:
        form = IndependenceForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "IndependenceCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def DBReferenceCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["DBReference"]

    

    if request.method == 'POST':
        form = DBReferenceForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.DBReference(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/DBReference/list/"
            return HttpResponseRedirect(link)
    else:
        form = DBReferenceForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "DBReferenceCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def DDC_Dewey_ClassificationCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["DDC_Dewey_Classification"]

    

    if request.method == 'POST':
        form = DDC_Dewey_ClassificationForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.DDC_Dewey_Classification(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/DDC_Dewey_Classification/list/"
            return HttpResponseRedirect(link)
    else:
        form = DDC_Dewey_ClassificationForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "DDC_Dewey_ClassificationCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def LibraryOfCongressClassificationCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["LibraryOfCongressClassification"]

    

    if request.method == 'POST':
        form = LibraryOfCongressClassificationForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.LibraryOfCongressClassification(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/LibraryOfCongressClassification/list/"
            return HttpResponseRedirect(link)
    else:
        form = LibraryOfCongressClassificationForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "LibraryOfCongressClassificationCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def NLMClassificationCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["NLMClassification"]

    

    if request.method == 'POST':
        form = NLMClassificationForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.NLMClassification(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/NLMClassification/list/"
            return HttpResponseRedirect(link)
    else:
        form = NLMClassificationForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "NLMClassificationCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ResearchCouncilsUKClassificationCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ResearchCouncilsUKClassification"]

    

    if request.method == 'POST':
        form = ResearchCouncilsUKClassificationForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ResearchCouncilsUKClassification(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/ResearchCouncilsUKClassification/list/"
            return HttpResponseRedirect(link)
    else:
        form = ResearchCouncilsUKClassificationForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ResearchCouncilsUKClassificationCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def DataRepresentationStandardCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["DataRepresentationStandard"]

    

    if request.method == 'POST':
        form = DataRepresentationStandardForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.DataRepresentationStandard(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/DataRepresentationStandard/list/"
            return HttpResponseRedirect(link)
    else:
        form = DataRepresentationStandardForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "DataRepresentationStandardCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def DegreeOfModelCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["DegreeOfModel"]

    

    if request.method == 'POST':
        form = DegreeOfModelForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.DegreeOfModel(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/DegreeOfModel/list/"
            return HttpResponseRedirect(link)
    else:
        form = DegreeOfModelForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "DegreeOfModelCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def DynamismOfModelCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["DynamismOfModel"]

    

    if request.method == 'POST':
        form = DynamismOfModelForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.DynamismOfModel(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/DynamismOfModel/list/"
            return HttpResponseRedirect(link)
    else:
        form = DynamismOfModelForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "DynamismOfModelCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def DependentVariableCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["DependentVariable"]

    

    if request.method == 'POST':
        form = DependentVariableForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.DependentVariable(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/DependentVariable/list/"
            return HttpResponseRedirect(link)
    else:
        form = DependentVariableForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "DependentVariableCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def DeviceCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Device"]

    

    if request.method == 'POST':
        form = DeviceForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Device(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/Device/list/"
            return HttpResponseRedirect(link)
    else:
        form = DeviceForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "DeviceCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def DomainActionCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["DomainAction"]

    

    if request.method == 'POST':
        form = DomainActionForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.DomainAction(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/DomainAction/list/"
            return HttpResponseRedirect(link)
    else:
        form = DomainActionForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "DomainActionCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def DoseResponseCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["DoseResponse"]

    

    if request.method == 'POST':
        form = DoseResponseForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.DoseResponse(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/DoseResponse/list/"
            return HttpResponseRedirect(link)
    else:
        form = DoseResponseForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "DoseResponseCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def GeneKnockinCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["GeneKnockin"]

    

    if request.method == 'POST':
        form = GeneKnockinForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.GeneKnockin(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/GeneKnockin/list/"
            return HttpResponseRedirect(link)
    else:
        form = GeneKnockinForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "GeneKnockinCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def GeneKnockoutCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["GeneKnockout"]

    

    if request.method == 'POST':
        form = GeneKnockoutForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.GeneKnockout(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/GeneKnockout/list/"
            return HttpResponseRedirect(link)
    else:
        form = GeneKnockoutForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "GeneKnockoutCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def Normal_DiseaseCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Normal_Disease"]

    

    if request.method == 'POST':
        form = Normal_DiseaseForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Normal_Disease(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/Normal_Disease/list/"
            return HttpResponseRedirect(link)
    else:
        form = Normal_DiseaseForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "Normal_DiseaseCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def TimeCourseCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["TimeCourse"]

    

    if request.method == 'POST':
        form = TimeCourseForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.TimeCourse(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/TimeCourse/list/"
            return HttpResponseRedirect(link)
    else:
        form = TimeCourseForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "TimeCourseCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def Treated_UntreatedCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Treated_Untreated"]

    

    if request.method == 'POST':
        form = Treated_UntreatedForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Treated_Untreated(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/Treated_Untreated/list/"
            return HttpResponseRedirect(link)
    else:
        form = Treated_UntreatedForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "Treated_UntreatedCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def DraftStatusCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["DraftStatus"]

    

    if request.method == 'POST':
        form = DraftStatusForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.DraftStatus(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/DraftStatus/list/"
            return HttpResponseRedirect(link)
    else:
        form = DraftStatusForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "DraftStatusCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def DuhemEffectCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["DuhemEffect"]

    

    if request.method == 'POST':
        form = DuhemEffectForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.DuhemEffect(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/DuhemEffect/list/"
            return HttpResponseRedirect(link)
    else:
        form = DuhemEffectForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "DuhemEffectCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ExperimentalDesignEffectCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ExperimentalDesignEffect"]

    

    if request.method == 'POST':
        form = ExperimentalDesignEffectForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ExperimentalDesignEffect(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/ExperimentalDesignEffect/list/"
            return HttpResponseRedirect(link)
    else:
        form = ExperimentalDesignEffectForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ExperimentalDesignEffectCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def InstrumentationEffectCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["InstrumentationEffect"]

    

    if request.method == 'POST':
        form = InstrumentationEffectForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.InstrumentationEffect(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/InstrumentationEffect/list/"
            return HttpResponseRedirect(link)
    else:
        form = InstrumentationEffectForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "InstrumentationEffectCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ObjectEffectCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ObjectEffect"]

    

    if request.method == 'POST':
        form = ObjectEffectForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ObjectEffect(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/ObjectEffect/list/"
            return HttpResponseRedirect(link)
    else:
        form = ObjectEffectForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ObjectEffectCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def SubjectEffectCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["SubjectEffect"]

    

    if request.method == 'POST':
        form = SubjectEffectForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.SubjectEffect(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/SubjectEffect/list/"
            return HttpResponseRedirect(link)
    else:
        form = SubjectEffectForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "SubjectEffectCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def TimeEffectCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["TimeEffect"]

    

    if request.method == 'POST':
        form = TimeEffectForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.TimeEffect(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/TimeEffect/list/"
            return HttpResponseRedirect(link)
    else:
        form = TimeEffectForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "TimeEffectCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def DynamicModelCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["DynamicModel"]

    

    if request.method == 'POST':
        form = DynamicModelForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.DynamicModel(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/DynamicModel/list/"
            return HttpResponseRedirect(link)
    else:
        form = DynamicModelForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "DynamicModelCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def StaticModelCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["StaticModel"]

    

    if request.method == 'POST':
        form = StaticModelForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.StaticModel(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/StaticModel/list/"
            return HttpResponseRedirect(link)
    else:
        form = StaticModelForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "StaticModelCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ErrorOfConclusionCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ErrorOfConclusion"]

    

    if request.method == 'POST':
        form = ErrorOfConclusionForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ErrorOfConclusion(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/ErrorOfConclusion/list/"
            return HttpResponseRedirect(link)
    else:
        form = ErrorOfConclusionForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ErrorOfConclusionCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ExperimentalActionsPlanningCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ExperimentalActionsPlanning"]

    

    if request.method == 'POST':
        form = ExperimentalActionsPlanningForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ExperimentalActionsPlanning(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/ExperimentalActionsPlanning/list/"
            return HttpResponseRedirect(link)
    else:
        form = ExperimentalActionsPlanningForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ExperimentalActionsPlanningCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ExperimentalEquipmentSelectingCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ExperimentalEquipmentSelecting"]

    

    if request.method == 'POST':
        form = ExperimentalEquipmentSelectingForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ExperimentalEquipmentSelecting(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/ExperimentalEquipmentSelecting/list/"
            return HttpResponseRedirect(link)
    else:
        form = ExperimentalEquipmentSelectingForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ExperimentalEquipmentSelectingCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ExperimentalModelDesignCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ExperimentalModelDesign"]

    

    if request.method == 'POST':
        form = ExperimentalModelDesignForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ExperimentalModelDesign(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/ExperimentalModelDesign/list/"
            return HttpResponseRedirect(link)
    else:
        form = ExperimentalModelDesignForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ExperimentalModelDesignCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ExperimentalObjectSelectingCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ExperimentalObjectSelecting"]

    

    if request.method == 'POST':
        form = ExperimentalObjectSelectingForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ExperimentalObjectSelecting(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/ExperimentalObjectSelecting/list/"
            return HttpResponseRedirect(link)
    else:
        form = ExperimentalObjectSelectingForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ExperimentalObjectSelectingCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ExperimentalConclusionCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ExperimentalConclusion"]

    

    if request.method == 'POST':
        form = ExperimentalConclusionForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ExperimentalConclusion(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/ExperimentalConclusion/list/"
            return HttpResponseRedirect(link)
    else:
        form = ExperimentalConclusionForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ExperimentalConclusionCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ExperimentalProtocolCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ExperimentalProtocol"]

    

    if request.method == 'POST':
        form = ExperimentalProtocolForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ExperimentalProtocol(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/ExperimentalProtocol/list/"
            return HttpResponseRedirect(link)
    else:
        form = ExperimentalProtocolForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ExperimentalProtocolCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ExperimenteeBiasCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ExperimenteeBias"]

    

    if request.method == 'POST':
        form = ExperimenteeBiasForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ExperimenteeBias(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/ExperimenteeBias/list/"
            return HttpResponseRedirect(link)
    else:
        form = ExperimenteeBiasForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ExperimenteeBiasCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def HawthorneEffectCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["HawthorneEffect"]

    

    if request.method == 'POST':
        form = HawthorneEffectForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.HawthorneEffect(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/HawthorneEffect/list/"
            return HttpResponseRedirect(link)
    else:
        form = HawthorneEffectForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "HawthorneEffectCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def MortalityEffectCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["MortalityEffect"]

    

    if request.method == 'POST':
        form = MortalityEffectForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.MortalityEffect(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/MortalityEffect/list/"
            return HttpResponseRedirect(link)
    else:
        form = MortalityEffectForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "MortalityEffectCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ExperimenterBiasCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ExperimenterBias"]

    

    if request.method == 'POST':
        form = ExperimenterBiasForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ExperimenterBias(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/ExperimenterBias/list/"
            return HttpResponseRedirect(link)
    else:
        form = ExperimenterBiasForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ExperimenterBiasCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def TestingEffectCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["TestingEffect"]

    

    if request.method == 'POST':
        form = TestingEffectForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.TestingEffect(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/TestingEffect/list/"
            return HttpResponseRedirect(link)
    else:
        form = TestingEffectForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "TestingEffectCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ExplicitHypothesisCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ExplicitHypothesis"]

    

    if request.method == 'POST':
        form = ExplicitHypothesisForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ExplicitHypothesis(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/ExplicitHypothesis/list/"
            return HttpResponseRedirect(link)
    else:
        form = ExplicitHypothesisForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ExplicitHypothesisCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ImplicitHypothesisCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ImplicitHypothesis"]

    

    if request.method == 'POST':
        form = ImplicitHypothesisForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ImplicitHypothesis(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/ImplicitHypothesis/list/"
            return HttpResponseRedirect(link)
    else:
        form = ImplicitHypothesisForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ImplicitHypothesisCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def FactorLevelCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["FactorLevel"]

    

    if request.method == 'POST':
        form = FactorLevelForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.FactorLevel(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/FactorLevel/list/"
            return HttpResponseRedirect(link)
    else:
        form = FactorLevelForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "FactorLevelCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def FactorialDesignCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["FactorialDesign"]

    

    if request.method == 'POST':
        form = FactorialDesignForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.FactorialDesign(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/FactorialDesign/list/"
            return HttpResponseRedirect(link)
    else:
        form = FactorialDesignForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "FactorialDesignCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def FalseNegativeCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["FalseNegative"]

    

    if request.method == 'POST':
        form = FalseNegativeForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.FalseNegative(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/FalseNegative/list/"
            return HttpResponseRedirect(link)
    else:
        form = FalseNegativeForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "FalseNegativeCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def HypothesisAcceptanceMistakeCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["HypothesisAcceptanceMistake"]

    

    if request.method == 'POST':
        form = HypothesisAcceptanceMistakeForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.HypothesisAcceptanceMistake(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/HypothesisAcceptanceMistake/list/"
            return HttpResponseRedirect(link)
    else:
        form = HypothesisAcceptanceMistakeForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "HypothesisAcceptanceMistakeCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def FalsePpositiveCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["FalsePpositive"]

    

    if request.method == 'POST':
        form = FalsePpositiveForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.FalsePpositive(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/FalsePpositive/list/"
            return HttpResponseRedirect(link)
    else:
        form = FalsePpositiveForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "FalsePpositiveCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def FaultyComparisonCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["FaultyComparison"]

    

    if request.method == 'POST':
        form = FaultyComparisonForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.FaultyComparison(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/FaultyComparison/list/"
            return HttpResponseRedirect(link)
    else:
        form = FaultyComparisonForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "FaultyComparisonCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def FormulaCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Formula"]

    

    if request.method == 'POST':
        form = FormulaForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Formula(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/Formula/list/"
            return HttpResponseRedirect(link)
    else:
        form = FormulaForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "FormulaCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def GalileanExperimentCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["GalileanExperiment"]

    

    if request.method == 'POST':
        form = GalileanExperimentForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.GalileanExperiment(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/GalileanExperiment/list/"
            return HttpResponseRedirect(link)
    else:
        form = GalileanExperimentForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "GalileanExperimentCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def HandToolCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["HandTool"]

    

    if request.method == 'POST':
        form = HandToolForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.HandTool(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/HandTool/list/"
            return HttpResponseRedirect(link)
    else:
        form = HandToolForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "HandToolCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ToolCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Tool"]

    

    if request.method == 'POST':
        form = ToolForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Tool(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/Tool/list/"
            return HttpResponseRedirect(link)
    else:
        form = ToolForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ToolCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def HardwareCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Hardware"]

    

    if request.method == 'POST':
        form = HardwareForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Hardware(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/Hardware/list/"
            return HttpResponseRedirect(link)
    else:
        form = HardwareForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "HardwareCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def HistoryEffectCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["HistoryEffect"]

    

    if request.method == 'POST':
        form = HistoryEffectForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.HistoryEffect(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/HistoryEffect/list/"
            return HttpResponseRedirect(link)
    else:
        form = HistoryEffectForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "HistoryEffectCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def MaturationEffectCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["MaturationEffect"]

    

    if request.method == 'POST':
        form = MaturationEffectForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.MaturationEffect(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/MaturationEffect/list/"
            return HttpResponseRedirect(link)
    else:
        form = MaturationEffectForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "MaturationEffectCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def HypothesisdrivenExperimentCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["HypothesisdrivenExperiment"]

    

    if request.method == 'POST':
        form = HypothesisdrivenExperimentForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.HypothesisdrivenExperiment(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/HypothesisdrivenExperiment/list/"
            return HttpResponseRedirect(link)
    else:
        form = HypothesisdrivenExperimentForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "HypothesisdrivenExperimentCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def HypothesisformingExperimentCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["HypothesisformingExperiment"]

    

    if request.method == 'POST':
        form = HypothesisformingExperimentForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.HypothesisformingExperiment(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/HypothesisformingExperiment/list/"
            return HttpResponseRedirect(link)
    else:
        form = HypothesisformingExperimentForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "HypothesisformingExperimentCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ImageCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Image"]

    

    if request.method == 'POST':
        form = ImageForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Image(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/Image/list/"
            return HttpResponseRedirect(link)
    else:
        form = ImageForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ImageCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ImproperSamplingCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ImproperSampling"]

    

    if request.method == 'POST':
        form = ImproperSamplingForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ImproperSampling(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/ImproperSampling/list/"
            return HttpResponseRedirect(link)
    else:
        form = ImproperSamplingForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ImproperSamplingCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def IncompleteDataErrorCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["IncompleteDataError"]

    

    if request.method == 'POST':
        form = IncompleteDataErrorForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.IncompleteDataError(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/IncompleteDataError/list/"
            return HttpResponseRedirect(link)
    else:
        form = IncompleteDataErrorForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "IncompleteDataErrorCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def IndependentVariableCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["IndependentVariable"]

    

    if request.method == 'POST':
        form = IndependentVariableForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.IndependentVariable(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/IndependentVariable/list/"
            return HttpResponseRedirect(link)
    else:
        form = IndependentVariableForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "IndependentVariableCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def InferableVariableCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["InferableVariable"]

    

    if request.method == 'POST':
        form = InferableVariableForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.InferableVariable(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/InferableVariable/list/"
            return HttpResponseRedirect(link)
    else:
        form = InferableVariableForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "InferableVariableCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def InternalStatusCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["InternalStatus"]

    

    if request.method == 'POST':
        form = InternalStatusForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.InternalStatus(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/InternalStatus/list/"
            return HttpResponseRedirect(link)
    else:
        form = InternalStatusForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "InternalStatusCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def RestrictedStatusCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["RestrictedStatus"]

    

    if request.method == 'POST':
        form = RestrictedStatusForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.RestrictedStatus(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/RestrictedStatus/list/"
            return HttpResponseRedirect(link)
    else:
        form = RestrictedStatusForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "RestrictedStatusCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def JournalCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Journal"]

    

    if request.method == 'POST':
        form = JournalForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Journal(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/Journal/list/"
            return HttpResponseRedirect(link)
    else:
        form = JournalForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "JournalCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def LinearModelCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["LinearModel"]

    

    if request.method == 'POST':
        form = LinearModelForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.LinearModel(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/LinearModel/list/"
            return HttpResponseRedirect(link)
    else:
        form = LinearModelForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "LinearModelCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def NonlinearModelCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["NonlinearModel"]

    

    if request.method == 'POST':
        form = NonlinearModelForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.NonlinearModel(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/NonlinearModel/list/"
            return HttpResponseRedirect(link)
    else:
        form = NonlinearModelForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "NonlinearModelCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def LogicalModelRepresentationCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["LogicalModelRepresentation"]

    

    if request.method == 'POST':
        form = LogicalModelRepresentationForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.LogicalModelRepresentation(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/LogicalModelRepresentation/list/"
            return HttpResponseRedirect(link)
    else:
        form = LogicalModelRepresentationForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "LogicalModelRepresentationCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def MachineCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Machine"]

    

    if request.method == 'POST':
        form = MachineForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Machine(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/Machine/list/"
            return HttpResponseRedirect(link)
    else:
        form = MachineForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "MachineCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def MachineToolCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["MachineTool"]

    

    if request.method == 'POST':
        form = MachineToolForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.MachineTool(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/MachineTool/list/"
            return HttpResponseRedirect(link)
    else:
        form = MachineToolForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "MachineToolCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def MagazineCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Magazine"]

    

    if request.method == 'POST':
        form = MagazineForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Magazine(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/Magazine/list/"
            return HttpResponseRedirect(link)
    else:
        form = MagazineForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "MagazineCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def MaterialsCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Materials"]

    

    if request.method == 'POST':
        form = MaterialsForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Materials(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/Materials/list/"
            return HttpResponseRedirect(link)
    else:
        form = MaterialsForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "MaterialsCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def MathematicalModelRepresentationCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["MathematicalModelRepresentation"]

    

    if request.method == 'POST':
        form = MathematicalModelRepresentationForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.MathematicalModelRepresentation(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/MathematicalModelRepresentation/list/"
            return HttpResponseRedirect(link)
    else:
        form = MathematicalModelRepresentationForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "MathematicalModelRepresentationCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def MetabolomicExperimentCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["MetabolomicExperiment"]

    

    if request.method == 'POST':
        form = MetabolomicExperimentForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.MetabolomicExperiment(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/MetabolomicExperiment/list/"
            return HttpResponseRedirect(link)
    else:
        form = MetabolomicExperimentForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "MetabolomicExperimentCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def MethodsComparisonCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["MethodsComparison"]

    

    if request.method == 'POST':
        form = MethodsComparisonForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.MethodsComparison(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/MethodsComparison/list/"
            return HttpResponseRedirect(link)
    else:
        form = MethodsComparisonForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "MethodsComparisonCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def SubjectsComparisonCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["SubjectsComparison"]

    

    if request.method == 'POST':
        form = SubjectsComparisonForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.SubjectsComparison(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/SubjectsComparison/list/"
            return HttpResponseRedirect(link)
    else:
        form = SubjectsComparisonForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "SubjectsComparisonCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def MicroarrayExperimentCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["MicroarrayExperiment"]

    

    if request.method == 'POST':
        form = MicroarrayExperimentForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.MicroarrayExperiment(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/MicroarrayExperiment/list/"
            return HttpResponseRedirect(link)
    else:
        form = MicroarrayExperimentForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "MicroarrayExperimentCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def MultifactorExperimentCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["MultifactorExperiment"]

    

    if request.method == 'POST':
        form = MultifactorExperimentForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.MultifactorExperiment(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/MultifactorExperiment/list/"
            return HttpResponseRedirect(link)
    else:
        form = MultifactorExperimentForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "MultifactorExperimentCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def OnefactorExperimentCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["OnefactorExperiment"]

    

    if request.method == 'POST':
        form = OnefactorExperimentForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.OnefactorExperiment(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/OnefactorExperiment/list/"
            return HttpResponseRedirect(link)
    else:
        form = OnefactorExperimentForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "OnefactorExperimentCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def TwofactorExperimentCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["TwofactorExperiment"]

    

    if request.method == 'POST':
        form = TwofactorExperimentForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.TwofactorExperiment(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/TwofactorExperiment/list/"
            return HttpResponseRedirect(link)
    else:
        form = TwofactorExperimentForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "TwofactorExperimentCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def NaturalLanguageCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["NaturalLanguage"]

    

    if request.method == 'POST':
        form = NaturalLanguageForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.NaturalLanguage(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/NaturalLanguage/list/"
            return HttpResponseRedirect(link)
    else:
        form = NaturalLanguageForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "NaturalLanguageCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def NonRestrictedStatusCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["NonRestrictedStatus"]

    

    if request.method == 'POST':
        form = NonRestrictedStatusForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.NonRestrictedStatus(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/NonRestrictedStatus/list/"
            return HttpResponseRedirect(link)
    else:
        form = NonRestrictedStatusForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "NonRestrictedStatusCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def NormalizationStrategyCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["NormalizationStrategy"]

    

    if request.method == 'POST':
        form = NormalizationStrategyForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.NormalizationStrategy(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/NormalizationStrategy/list/"
            return HttpResponseRedirect(link)
    else:
        form = NormalizationStrategyForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "NormalizationStrategyCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ObjectMethodCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ObjectMethod"]

    

    if request.method == 'POST':
        form = ObjectMethodForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ObjectMethod(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/ObjectMethod/list/"
            return HttpResponseRedirect(link)
    else:
        form = ObjectMethodForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ObjectMethodCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ObjectOfActionCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ObjectOfAction"]

    

    if request.method == 'POST':
        form = ObjectOfActionForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ObjectOfAction(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/ObjectOfAction/list/"
            return HttpResponseRedirect(link)
    else:
        form = ObjectOfActionForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ObjectOfActionCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ObservableVariableCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ObservableVariable"]

    

    if request.method == 'POST':
        form = ObservableVariableForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ObservableVariable(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/ObservableVariable/list/"
            return HttpResponseRedirect(link)
    else:
        form = ObservableVariableForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ObservableVariableCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def PaperCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Paper"]

    

    if request.method == 'POST':
        form = PaperForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Paper(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/Paper/list/"
            return HttpResponseRedirect(link)
    else:
        form = PaperForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "PaperCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ParticlePhysicsExperimentCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ParticlePhysicsExperiment"]

    

    if request.method == 'POST':
        form = ParticlePhysicsExperimentForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ParticlePhysicsExperiment(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/ParticlePhysicsExperiment/list/"
            return HttpResponseRedirect(link)
    else:
        form = ParticlePhysicsExperimentForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ParticlePhysicsExperimentCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def PresentingSamplingCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["PresentingSampling"]

    

    if request.method == 'POST':
        form = PresentingSamplingForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.PresentingSampling(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/PresentingSampling/list/"
            return HttpResponseRedirect(link)
    else:
        form = PresentingSamplingForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "PresentingSamplingCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ProceedingsCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Proceedings"]

    

    if request.method == 'POST':
        form = ProceedingsForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Proceedings(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/Proceedings/list/"
            return HttpResponseRedirect(link)
    else:
        form = ProceedingsForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ProceedingsCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ProgramCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Program"]

    

    if request.method == 'POST':
        form = ProgramForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Program(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/Program/list/"
            return HttpResponseRedirect(link)
    else:
        form = ProgramForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ProgramCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ProteomicExperimentCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["ProteomicExperiment"]

    

    if request.method == 'POST':
        form = ProteomicExperimentForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.ProteomicExperiment(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/ProteomicExperiment/list/"
            return HttpResponseRedirect(link)
    else:
        form = ProteomicExperimentForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ProteomicExperimentCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def ProviderCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Provider"]

    
    foreign_keys['Has_admin_info'] = 'EXPO_app:AdminInfoProviderList'
    
    foreign_keys['Has_admin_info'] = 'EXPO_app:ThingList'
    

    if request.method == 'POST':
        form = ProviderForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Provider(instance.name)
            onto.save()
            

            
            onto.save()

            
            fkstr = 'Has_admin_info'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.AdminInfoProvider(fkinstanceinput[0])
            onto.save()
            onto_instance.has_admin_info.append(fkinstance) 
            
            fkstr = 'Has_admin_info'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Thing(fkinstanceinput[0])
            onto.save()
            onto_instance.has_admin_info.append(fkinstance) 
            
            onto.save()
            link = "/EXPO_app/Provider/list/"
            return HttpResponseRedirect(link)
    else:
        form = ProviderForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "ProviderCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def PublicAcademicStatusCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["PublicAcademicStatus"]

    

    if request.method == 'POST':
        form = PublicAcademicStatusForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.PublicAcademicStatus(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/PublicAcademicStatus/list/"
            return HttpResponseRedirect(link)
    else:
        form = PublicAcademicStatusForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "PublicAcademicStatusCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def PublicStatusCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["PublicStatus"]

    

    if request.method == 'POST':
        form = PublicStatusForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.PublicStatus(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/PublicStatus/list/"
            return HttpResponseRedirect(link)
    else:
        form = PublicStatusForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "PublicStatusCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def RandomErrorCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["RandomError"]

    

    if request.method == 'POST':
        form = RandomErrorForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.RandomError(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/RandomError/list/"
            return HttpResponseRedirect(link)
    else:
        form = RandomErrorForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "RandomErrorCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def RandomSamplingCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["RandomSampling"]

    

    if request.method == 'POST':
        form = RandomSamplingForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.RandomSampling(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/RandomSampling/list/"
            return HttpResponseRedirect(link)
    else:
        form = RandomSamplingForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "RandomSamplingCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def RecommendationSatusCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["RecommendationSatus"]

    

    if request.method == 'POST':
        form = RecommendationSatusForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.RecommendationSatus(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/RecommendationSatus/list/"
            return HttpResponseRedirect(link)
    else:
        form = RecommendationSatusForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "RecommendationSatusCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def RecordingActionCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["RecordingAction"]

    

    if request.method == 'POST':
        form = RecordingActionForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.RecordingAction(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/RecordingAction/list/"
            return HttpResponseRedirect(link)
    else:
        form = RecordingActionForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "RecordingActionCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def RegionCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Region"]

    

    if request.method == 'POST':
        form = RegionForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Region(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/Region/list/"
            return HttpResponseRedirect(link)
    else:
        form = RegionForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "RegionCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def RepresenationalMediumCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["RepresenationalMedium"]

    

    if request.method == 'POST':
        form = RepresenationalMediumForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.RepresenationalMedium(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/RepresenationalMedium/list/"
            return HttpResponseRedirect(link)
    else:
        form = RepresenationalMediumForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "RepresenationalMediumCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def SUMOSynonymCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["SUMOSynonym"]

    

    if request.method == 'POST':
        form = SUMOSynonymForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.SUMOSynonym(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/SUMOSynonym/list/"
            return HttpResponseRedirect(link)
    else:
        form = SUMOSynonymForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "SUMOSynonymCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def SampleFormingCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["SampleForming"]

    

    if request.method == 'POST':
        form = SampleFormingForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.SampleForming(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/SampleForming/list/"
            return HttpResponseRedirect(link)
    else:
        form = SampleFormingForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "SampleFormingCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def SequentialDesignCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["SequentialDesign"]

    

    if request.method == 'POST':
        form = SequentialDesignForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.SequentialDesign(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/SequentialDesign/list/"
            return HttpResponseRedirect(link)
    else:
        form = SequentialDesignForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "SequentialDesignCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def SoftwareCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Software"]

    

    if request.method == 'POST':
        form = SoftwareForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Software(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/Software/list/"
            return HttpResponseRedirect(link)
    else:
        form = SoftwareForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "SoftwareCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def SoundCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Sound"]

    

    if request.method == 'POST':
        form = SoundForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Sound(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/Sound/list/"
            return HttpResponseRedirect(link)
    else:
        form = SoundForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "SoundCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def SplitSamplesCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["SplitSamples"]

    

    if request.method == 'POST':
        form = SplitSamplesForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.SplitSamples(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/SplitSamples/list/"
            return HttpResponseRedirect(link)
    else:
        form = SplitSamplesForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "SplitSamplesCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def SymmetricalMatchingCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["SymmetricalMatching"]

    

    if request.method == 'POST':
        form = SymmetricalMatchingForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.SymmetricalMatching(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/SymmetricalMatching/list/"
            return HttpResponseRedirect(link)
    else:
        form = SymmetricalMatchingForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "SymmetricalMatchingCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def StatisticalErrorCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["StatisticalError"]

    

    if request.method == 'POST':
        form = StatisticalErrorForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.StatisticalError(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/StatisticalError/list/"
            return HttpResponseRedirect(link)
    else:
        form = StatisticalErrorForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "StatisticalErrorCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def StratifiedSamplingCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["StratifiedSampling"]

    

    if request.method == 'POST':
        form = StratifiedSamplingForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.StratifiedSampling(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/StratifiedSampling/list/"
            return HttpResponseRedirect(link)
    else:
        form = StratifiedSamplingForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "StratifiedSamplingCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def SubjectOfActionCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["SubjectOfAction"]

    

    if request.method == 'POST':
        form = SubjectOfActionForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.SubjectOfAction(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/SubjectOfAction/list/"
            return HttpResponseRedirect(link)
    else:
        form = SubjectOfActionForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "SubjectOfActionCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def SubjectOfExperimentCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["SubjectOfExperiment"]

    
    foreign_keys['Has_admin_info'] = 'EXPO_app:AdminInfoSubjectOfExperimentList'
    
    foreign_keys['Has_admin_info'] = 'EXPO_app:ThingList'
    

    if request.method == 'POST':
        form = SubjectOfExperimentForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.SubjectOfExperiment(instance.name)
            onto.save()
            

            
            onto.save()

            
            fkstr = 'Has_admin_info'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.AdminInfoSubjectOfExperiment(fkinstanceinput[0])
            onto.save()
            onto_instance.has_admin_info.append(fkinstance) 
            
            fkstr = 'Has_admin_info'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Thing(fkinstanceinput[0])
            onto.save()
            onto_instance.has_admin_info.append(fkinstance) 
            
            onto.save()
            link = "/EXPO_app/SubjectOfExperiment/list/"
            return HttpResponseRedirect(link)
    else:
        form = SubjectOfExperimentForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "SubjectOfExperimentCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def SubmitterCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Submitter"]

    
    foreign_keys['Has_admin_info'] = 'EXPO_app:AdminInfoSubmitterList'
    
    foreign_keys['Has_admin_info'] = 'EXPO_app:ThingList'
    

    if request.method == 'POST':
        form = SubmitterForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Submitter(instance.name)
            onto.save()
            

            
            onto.save()

            
            fkstr = 'Has_admin_info'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.AdminInfoSubmitter(fkinstanceinput[0])
            onto.save()
            onto_instance.has_admin_info.append(fkinstance) 
            
            fkstr = 'Has_admin_info'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Thing(fkinstanceinput[0])
            onto.save()
            onto_instance.has_admin_info.append(fkinstance) 
            
            onto.save()
            link = "/EXPO_app/Submitter/list/"
            return HttpResponseRedirect(link)
    else:
        form = SubmitterForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "SubmitterCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def SummaryCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["Summary"]

    

    if request.method == 'POST':
        form = SummaryForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.Summary(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/Summary/list/"
            return HttpResponseRedirect(link)
    else:
        form = SummaryForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "SummaryCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def SystematicErrorCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["SystematicError"]

    

    if request.method == 'POST':
        form = SystematicErrorForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.SystematicError(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/SystematicError/list/"
            return HttpResponseRedirect(link)
    else:
        form = SystematicErrorForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "SystematicErrorCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def TechnicalReportCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["TechnicalReport"]

    

    if request.method == 'POST':
        form = TechnicalReportForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.TechnicalReport(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/TechnicalReport/list/"
            return HttpResponseRedirect(link)
    else:
        form = TechnicalReportForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "TechnicalReportCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def TimeDurationCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["TimeDuration"]

    

    if request.method == 'POST':
        form = TimeDurationForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.TimeDuration(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/TimeDuration/list/"
            return HttpResponseRedirect(link)
    else:
        form = TimeDurationForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "TimeDurationCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def TimeIntervalCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["TimeInterval"]

    

    if request.method == 'POST':
        form = TimeIntervalForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.TimeInterval(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/TimeInterval/list/"
            return HttpResponseRedirect(link)
    else:
        form = TimeIntervalForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "TimeIntervalCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})

def URLCreate(request):
    foreign_keys = dict()
    class_namespace =  namespaces["URL"]

    

    if request.method == 'POST':
        form = URLForm(request.POST)
        if form.is_valid():
            instance = form.save()

            onto_instance = class_namespace.URL(instance.name)
            onto.save()
            

            
            onto.save()

            
            onto.save()
            link = "/EXPO_app/URL/list/"
            return HttpResponseRedirect(link)
    else:
        form = URLForm()
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    return render(request, "URLCreate.html",{'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys})
