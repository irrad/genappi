from django.db import models

class Thing(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class RepresentationForm(models.Model):
    
    has=models.OneToOneField("Artifact", related_name = "%(class)s_has")
    
    has_expression=models.OneToOneField("LinguisticExpression", related_name = "%(class)s_has_expression")
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Artifact(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class SynonymousExternalConcept(models.Model):
    
    has_EXPO_concept=models.OneToOneField("Name", related_name = "%(class)s_has_EXPO_concept")
    
    has_synonymousExternalConcept=models.OneToOneField("Name", related_name = "%(class)s_has_synonymousExternalConcept")
    
    has_EXPO_concept=models.ForeignKey("Thing", related_name = "%(class)s_has_EXPO_concept")
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Name(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ScientificExperiment(models.Model):
    
    has_ExperimentalDesign=models.OneToOneField("ExperimentalDesign", related_name = "%(class)s_has_ExperimentalDesign")
    
    has_admin_info=models.OneToOneField("AdminInfoExperiment", related_name = "%(class)s_has_admin_info")
    
    has_classification=models.OneToOneField("ClassificationOfExperiments", related_name = "%(class)s_has_classification")
    
    has_goal=models.OneToOneField("ExperimentalGoal", related_name = "%(class)s_has_goal")
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ExperimentalDesign(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class AdminInfoExperiment(models.Model):
    
    has_ID=models.ManyToManyField("Thing", related_name = "%(class)s_has_ID")
    
    has_ID=models.OneToOneField("IDExperiment", related_name = "%(class)s_has_ID")
    
    has_author=models.OneToOneField("Author", related_name = "%(class)s_has_author")
    
    has_organization=models.OneToOneField("Organization", related_name = "%(class)s_has_organization")
    
    has_reference=models.OneToOneField("BiblioReference", related_name = "%(class)s_has_reference")
    
    has_status=models.OneToOneField("StatusExperimentalDocument", related_name = "%(class)s_has_status")
    
    has_title=models.OneToOneField("TitleExperiment", related_name = "%(class)s_has_title")
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class IDExperiment(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class DomainOfExperiment(models.Model):
    
    has_SUMO_sinonym=models.OneToOneField("SynonymousExternalConcept", related_name = "%(class)s_has_SUMO_sinonym")
    
    has_classification=models.OneToOneField("ClassificationOfDomains", related_name = "%(class)s_has_classification")
    
    has_model=models.OneToOneField("DomainModel", related_name = "%(class)s_has_model")
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ExperimentalObservation(models.Model):
    
    has_interpretation=models.OneToOneField("ResultsInterpretation", related_name = "%(class)s_has_interpretation")
    
    has_error=models.OneToOneField("ObservationalError", related_name = "%(class)s_has_error")
    
    has_observation_characteristic=models.OneToOneField("StatisticsCharacteristic", related_name = "%(class)s_has_observation_characteristic")
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ResultsInterpretation(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ExperimentalAction(models.Model):
    
    has_name=models.OneToOneField("ActionName", related_name = "%(class)s_has_name")
    
    has_action_complexity=models.OneToOneField("ActionComplexity", related_name = "%(class)s_has_action_complexity")
    
    has_goal=models.OneToOneField("ActionGoal", related_name = "%(class)s_has_goal")
    
    has_instrument=models.OneToOneField("ExperimentalEquipment", related_name = "%(class)s_has_instrument")
    
    has_method=models.OneToOneField("ExperimentalMethod", related_name = "%(class)s_has_method")
    
    has_timing=models.OneToOneField("TimePoint", related_name = "%(class)s_has_timing")
    
    has_action_complexity=models.ForeignKey("Thing", related_name = "%(class)s_has_action_complexity")
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ActionName(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ActionComplexity(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Human(models.Model):
    
    has_organization=models.OneToOneField("Organization", related_name = "%(class)s_has_organization")
    
    has_telephone_number=models.OneToOneField("Number", related_name = "%(class)s_has_telephone_number")
    
    has_title=models.OneToOneField("Title", related_name = "%(class)s_has_title")
    
    has_name=models.ForeignKey("Thing", related_name = "%(class)s_has_name")
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Organization(models.Model):
    
    has_fax_number=models.OneToOneField("Number", related_name = "%(class)s_has_fax_number")
    
    has_name=models.OneToOneField("Name", related_name = "%(class)s_has_name")
    
    has_telephone_number=models.OneToOneField("Number", related_name = "%(class)s_has_telephone_number")
    
    has_name=models.ForeignKey("Entity", related_name = "%(class)s_has_name")
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class AdminInfoAuthor(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class AdminInfoObjectOfExperiment(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class AdminInfoProvider(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class AdminInfoSubjectOfExperiment(models.Model):
    
    has_name=models.OneToOneField("Name", related_name = "%(class)s_has_name")
    
    has_organization=models.OneToOneField("Organization", related_name = "%(class)s_has_organization")
    
    has_qualification=models.OneToOneField("SubjectQualification", related_name = "%(class)s_has_qualification")
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class AdminInfoSubmitter(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class AdminInfoUser(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ExperimentalMethod(models.Model):
    
    has_applicability=models.OneToOneField("MethodApplicabilityCondition", related_name = "%(class)s_has_applicability")
    
    has_goal=models.OneToOneField("MethodGoal", related_name = "%(class)s_has_goal")
    
    has_plan=models.OneToOneField("PlanOfExperimentalActions", related_name = "%(class)s_has_plan")
    
    has_subject=models.OneToOneField("SubjectMethod", related_name = "%(class)s_has_subject")
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class MethodApplicabilityCondition(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class StandardOperatingProcedure(models.Model):
    
    has_author=models.OneToOneField("AuthorSOP", related_name = "%(class)s_has_author")
    
    has_latest_modification=models.OneToOneField("TimePoint", related_name = "%(class)s_has_latest_modification")
    
    has_procedure=models.OneToOneField("ProcedureExecuteExperiment", related_name = "%(class)s_has_procedure")
    
    has_representation=models.OneToOneField("BiblioReference", related_name = "%(class)s_has_representation")
    
    has_latest_modification=models.ForeignKey("Thing", related_name = "%(class)s_has_latest_modification")
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Author(models.Model):
    
    has_admin_info=models.ManyToManyField("Thing", related_name = "%(class)s_has_admin_info")
    
    has_admin_info=models.OneToOneField("AdminInfoAuthor", related_name = "%(class)s_has_admin_info")
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ClassificationOfDomains(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ClassificationOfExperiments(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ExperimentalHypothesis(models.Model):
    
    has_explicitness=models.ManyToManyField("Thing", related_name = "%(class)s_has_explicitness")
    
    has_complexity=models.OneToOneField("HypothesisComplexity", related_name = "%(class)s_has_complexity")
    
    has_explicitness=models.OneToOneField("HypothesisExplicitness", related_name = "%(class)s_has_explicitness")
    
    has_generality=models.OneToOneField("GeneralityOfHypothesis", related_name = "%(class)s_has_generality")
    
    has_hypothesis_representation=models.OneToOneField("HypothesisRepresentation", related_name = "%(class)s_has_hypothesis_representation")
    
    has_target_variable=models.OneToOneField("TargetVariable", related_name = "%(class)s_has_target_variable")
    
    has_complexity=models.ForeignKey("Thing", related_name = "%(class)s_has_complexity")
    
    has_generality=models.ForeignKey("Thing", related_name = "%(class)s_has_generality")
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class HypothesisComplexity(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Representation(models.Model):
    
    has_content=models.OneToOneField("Proposition", related_name = "%(class)s_has_content")
    
    has_representation_style=models.OneToOneField("RepresentationForm", related_name = "%(class)s_has_representation_style")
    
    has_content=models.ForeignKey("Thing", related_name = "%(class)s_has_content")
    
    has_representation_style=models.ForeignKey("Thing", related_name = "%(class)s_has_representation_style")
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Proposition(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ExperimentalEquipment(models.Model):
    
    has_description=models.OneToOneField("TechnicalDescription", related_name = "%(class)s_has_description")
    
    has_name=models.OneToOneField("NameEquipment", related_name = "%(class)s_has_name")
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class TechnicalDescription(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ExperimentalResults(models.Model):
    
    has_error=models.OneToOneField("ResultError", related_name = "%(class)s_has_error")
    
    has_evaluation=models.OneToOneField("ResultsEvaluation", related_name = "%(class)s_has_evaluation")
    
    has_fact=models.OneToOneField("FactRejectResearchHypothesis", related_name = "%(class)s_has_fact")
    
    has_fact=models.OneToOneField("FactSupportResearchHypothesis", related_name = "%(class)s_has_fact")
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ObservationalError(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ResultError(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ResultsEvaluation(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ScientificInvestigation(models.Model):
    
    has_experiment=models.OneToOneField("ScientificExperiment", related_name = "%(class)s_has_experiment")
    
    has_experimental_design=models.OneToOneField("ExperimentalDesign", related_name = "%(class)s_has_experimental_design")
    
    has_hypothesis_formation=models.OneToOneField("HypothesisFormation", related_name = "%(class)s_has_hypothesis_formation")
    
    has_information_gethering=models.OneToOneField("InformationGethering", related_name = "%(class)s_has_information_gethering")
    
    has_problem_analysis=models.OneToOneField("PoblemAnalysis", related_name = "%(class)s_has_problem_analysis")
    
    has_result_evaluation=models.OneToOneField("ResultsEvaluation", related_name = "%(class)s_has_result_evaluation")
    
    has_result_interpretation=models.OneToOneField("ResultsInterpretation", related_name = "%(class)s_has_result_interpretation")
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ExperimentalModel(models.Model):
    
    has_experimental_factor=models.OneToOneField("ExperimentalFactor", related_name = "%(class)s_has_experimental_factor")
    
    has_unknown_variable=models.OneToOneField("TargetVariable", related_name = "%(class)s_has_unknown_variable")
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ExperimentalFactor(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ExperimentalTechnology(models.Model):
    
    has_experimental_requirements=models.OneToOneField("ExperimentalRequirements", related_name = "%(class)s_has_experimental_requirements")
    
    has_standard=models.OneToOneField("ExperimentalStandard", related_name = "%(class)s_has_standard")
    
    has_strategy=models.OneToOneField("ExperimentalDesignStrategy", related_name = "%(class)s_has_strategy")
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ExperimentalRequirements(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class HypothesisExplicitness(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class LinguisticExpression(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class FactRejectResearchHypothesis(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class FactSupportResearchHypothesis(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Number(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class GeneralityOfHypothesis(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ActionGoal(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ExperimentalGoal(models.Model):
    
    has_representation=models.OneToOneField("RepresentationExperimentalGoal", related_name = "%(class)s_has_representation")
    
    has_subgoal=models.OneToOneField("ExperimentalSubgoal", related_name = "%(class)s_has_subgoal")
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class MethodGoal(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class HypothesisFormation(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class HypothesisRepresentation(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class InformationGethering(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Sample(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Variability(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class TimePoint(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class User(models.Model):
    
    has_admin_info=models.OneToOneField("AdminInfoUser", related_name = "%(class)s_has_admin_info")
    
    has_login=models.OneToOneField("LoginName", related_name = "%(class)s_has_login")
    
    has_pasword=models.OneToOneField("Password", related_name = "%(class)s_has_pasword")
    
    has_admin_info=models.ForeignKey("Thing", related_name = "%(class)s_has_admin_info")
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class LoginName(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class DomainModel(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class GroupExperimentalObject(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ObjectOfExperiment(models.Model):
    
    has_admin_info=models.ForeignKey("Thing", related_name = "%(class)s_has_admin_info")
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ClassificationByModel(models.Model):
    
    has_number_of_factors=models.OneToOneField("Number", related_name = "%(class)s_has_number_of_factors")
    
    has_number_of_factors=models.ForeignKey("Thing", related_name = "%(class)s_has_number_of_factors")
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class StatisticsCharacteristic(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ParentGroup(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Password(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ProcedureExecuteExperiment(models.Model):
    
    has_plan=models.OneToOneField("PlanOfExperimentalActions", related_name = "%(class)s_has_plan")
    
    has_representation=models.OneToOneField("RepresentationExperimentalExecutionProcedure", related_name = "%(class)s_has_representation")
    
    has_status=models.OneToOneField("DocumentStage", related_name = "%(class)s_has_status")
    
    has_status=models.OneToOneField("PermissionStatus", related_name = "%(class)s_has_status")
    
    has_status=models.ForeignKey("Thing", related_name = "%(class)s_has_status")
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class PlanOfExperimentalActions(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class PoblemAnalysis(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class SubjectQualification(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BiblioReference(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Model(models.Model):
    
    has_representation=models.OneToOneField("ModelRepresentation", related_name = "%(class)s_has_representation")
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ModelRepresentation(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class RepresentationExperimentalExecutionProcedure(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class RepresentationExperimentalGoal(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class SampleRepresentativeness(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class SampleSize(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class SamplingMethod(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class SystematicSampling(models.Model):
    
    has_sampling_rule=models.OneToOneField("SamplingRule", related_name = "%(class)s_has_sampling_rule")
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class SamplingRule(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ExperimentalStandard(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Error(models.Model):
    
    has_statistics_characteristic=models.OneToOneField("Dispersion", related_name = "%(class)s_has_statistics_characteristic")
    
    has_statistics_characteristic=models.OneToOneField("LevelOfSignificance", related_name = "%(class)s_has_statistics_characteristic")
    
    has_statistics_characteristic=models.ForeignKey("Thing", related_name = "%(class)s_has_statistics_characteristic")
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Dispersion(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class LevelOfSignificance(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class StatusExperimentalDocument(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ExperimentalDesignStrategy(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ExperimentalSubgoal(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class SubjectMethod(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class BaconianExperiment(models.Model):
    
    has_synonym=models.OneToOneField("SynonymousExternalConcept", related_name = "%(class)s_has_synonym")
    
    has_synonym=models.ForeignKey("Thing", related_name = "%(class)s_has_synonym")
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class TargetVariable(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Title(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ValueEstimate(models.Model):
    
    has_value_of_variable=models.OneToOneField("ValueOfVariable", related_name = "%(class)s_has_value_of_variable")
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ValueOfVariable(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Role(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class CorpuscularObject(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class AttributeRole(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Attribute(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Procedure(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class AdministrativeInformation(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class TitleExperiment(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class NameEquipment(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class PersonalName(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class FieldOfStudy(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class RelatedDomain(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ProductRole(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ScientificTask(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ExecutionOfExperiment(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class CorporateName(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class AttributeOfAction(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class SentientAgent(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Robot(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Group(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Entity(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ResearchMethod(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Requirements(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class AuthorSOP(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class SubjectRole(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Classification(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Fact(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ModelAssumption(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Variable(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class AttributeOfHypothesis(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ContentBearingObject(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Abstract(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Quantity(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Relation(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ProcessrelatedRole(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ContentBearingPhysical(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class PhysicalQuantity(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Goal(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class RepresentationExperimentalObservation(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class RepresentationExperimentalResults(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class AttributeGroup(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class TimePosition(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ActorRole(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class DocumentStage(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class PermissionStatus(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Plan(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Reference(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class AttributeSample(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ExperimentalRule(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Robustness(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Validity(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class AttributeOfDocument(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class PhysicalExperiment(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ComputationalData(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Predicate(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class SelfConnectedObject(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ScientificActivity(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class FormingClassificationSystem(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class HypothesisForming(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class InterpretingResult(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ProcessProblemAnalysis(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ResultEvaluating(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class AttributeOfModel(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class AttributeOfVariable(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Agent(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Collection(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class EperimentalDesignTask(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Physical(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Object(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Process(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class TaskRole(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class TimeMeasure(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ActionrelatedRole(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class AbductiveHypothesis(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class InductiveHypothesis(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Adequacy(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class AlternativeHypothesis(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class NullHypothesis(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ResearchHypothesis(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Article(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Text(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ArtificialLanguage(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Language(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class HumanLanguage(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Sentence(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class AtomicAction(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ComplexAction(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class AuthorProtocol(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Book(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class CalculableVariable(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class CapacityRequirements(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class EnvironmentalRequirements(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class FinancialRequirements(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ClassificationByDomain(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Classifying(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class DesigningExperiment(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class CoarseError(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class MeasurementError(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ComparisonControl_TargetGroups(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class PairedComparison(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class PairedComparisonOfMatchingGroups(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class PairedComparisonOfSingleSampleGroups(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class QualityControl(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ComplexHypothesis(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class SingleHypothesis(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ComputationalExperiment(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ComputeGoal(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ConfirmGoal(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ExplainGoal(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class InvestigateGoal(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ComputerSimulation(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ConstantQuantity(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Controllability(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Independence(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class DBReference(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class DDC_Dewey_Classification(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class LibraryOfCongressClassification(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class NLMClassification(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ResearchCouncilsUKClassification(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class DataRepresentationStandard(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class DegreeOfModel(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class DynamismOfModel(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class DependentVariable(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Device(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class DomainAction(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class DoseResponse(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class GeneKnockin(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class GeneKnockout(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Normal_Disease(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class TimeCourse(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Treated_Untreated(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class DraftStatus(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class DuhemEffect(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ExperimentalDesignEffect(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class InstrumentationEffect(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ObjectEffect(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class SubjectEffect(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class TimeEffect(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class DynamicModel(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class StaticModel(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ErrorOfConclusion(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ExperimentalActionsPlanning(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ExperimentalEquipmentSelecting(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ExperimentalModelDesign(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ExperimentalObjectSelecting(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ExperimentalConclusion(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ExperimentalProtocol(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ExperimenteeBias(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class HawthorneEffect(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class MortalityEffect(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ExperimenterBias(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class TestingEffect(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ExplicitHypothesis(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ImplicitHypothesis(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class FactorLevel(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class FactorialDesign(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class FalseNegative(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class HypothesisAcceptanceMistake(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class FalsePpositive(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class FaultyComparison(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Formula(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class GalileanExperiment(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class HandTool(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Tool(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Hardware(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class HistoryEffect(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class MaturationEffect(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class HypothesisdrivenExperiment(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class HypothesisformingExperiment(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Image(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ImproperSampling(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class IncompleteDataError(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class IndependentVariable(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class InferableVariable(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class InternalStatus(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class RestrictedStatus(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Journal(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class LinearModel(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class NonlinearModel(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class LogicalModelRepresentation(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Machine(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class MachineTool(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Magazine(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Materials(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class MathematicalModelRepresentation(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class MetabolomicExperiment(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class MethodsComparison(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class SubjectsComparison(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class MicroarrayExperiment(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class MultifactorExperiment(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class OnefactorExperiment(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class TwofactorExperiment(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class NaturalLanguage(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class NonRestrictedStatus(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class NormalizationStrategy(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ObjectMethod(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ObjectOfAction(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ObservableVariable(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Paper(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ParticlePhysicsExperiment(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class PresentingSampling(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Proceedings(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Program(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class ProteomicExperiment(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Provider(models.Model):
    
    has_admin_info=models.OneToOneField("AdminInfoProvider", related_name = "%(class)s_has_admin_info")
    
    has_admin_info=models.ForeignKey("Thing", related_name = "%(class)s_has_admin_info")
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class PublicAcademicStatus(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class PublicStatus(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class RandomError(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class RandomSampling(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class RecommendationSatus(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class RecordingAction(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Region(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class RepresenationalMedium(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class SUMOSynonym(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class SampleForming(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class SequentialDesign(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Software(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Sound(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class SplitSamples(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class SymmetricalMatching(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class StatisticalError(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class StratifiedSampling(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class SubjectOfAction(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class SubjectOfExperiment(models.Model):
    
    has_admin_info=models.OneToOneField("AdminInfoSubjectOfExperiment", related_name = "%(class)s_has_admin_info")
    
    has_admin_info=models.ForeignKey("Thing", related_name = "%(class)s_has_admin_info")
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Submitter(models.Model):
    
    has_admin_info=models.OneToOneField("AdminInfoSubmitter", related_name = "%(class)s_has_admin_info")
    
    has_admin_info=models.ForeignKey("Thing", related_name = "%(class)s_has_admin_info")
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Summary(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class SystematicError(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class TechnicalReport(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class TimeDuration(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class TimeInterval(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class URL(models.Model):
    
    name=models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name
