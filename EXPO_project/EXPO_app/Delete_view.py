from django.db import models
from django.template.loader import render_to_string
from .models import *
from .forms import *
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseNotFound
from django.template import loader
from django.core.urlresolvers import reverse
import datetime
from .forms import *
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.core.files.storage import FileSystemStorage
from django.utils.safestring import mark_safe


def ThingDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Thing,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Thing/list/"
        return HttpResponseRedirect(link)
    else:
         form = ThingForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ThingDelete.html",context)

def RepresentationFormDelete(request, pk):
    foreign_keys = dict()
    
    foreign_keys['Has_'] = 'EXPO_app:ArtifactList'
    
    foreign_keys['Has_expression'] = 'EXPO_app:LinguisticExpressionList'
    
    object_to_delete = get_object_or_404(RepresentationForm,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/RepresentationForm/list/"
        return HttpResponseRedirect(link)
    else:
         form = RepresentationForm1(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "RepresentationFormDelete.html",context)

def ArtifactDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Artifact,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Artifact/list/"
        return HttpResponseRedirect(link)
    else:
         form = ArtifactForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ArtifactDelete.html",context)

def SynonymousExternalConceptDelete(request, pk):
    foreign_keys = dict()
    
    foreign_keys['Has_EXPO_concept'] = 'EXPO_app:NameList'
    
    foreign_keys['Has_synonymousExternalConcept'] = 'EXPO_app:NameList'
    
    foreign_keys['Has_EXPO_concept'] = 'EXPO_app:ThingList'
    
    object_to_delete = get_object_or_404(SynonymousExternalConcept,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/SynonymousExternalConcept/list/"
        return HttpResponseRedirect(link)
    else:
         form = SynonymousExternalConceptForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SynonymousExternalConceptDelete.html",context)

def NameDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Name,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Name/list/"
        return HttpResponseRedirect(link)
    else:
         form = NameForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "NameDelete.html",context)

def ScientificExperimentDelete(request, pk):
    foreign_keys = dict()
    
    foreign_keys['Has_ExperimentalDesign'] = 'EXPO_app:ExperimentalDesignList'
    
    foreign_keys['Has_admin_info'] = 'EXPO_app:AdminInfoExperimentList'
    
    foreign_keys['Has_classification'] = 'EXPO_app:ClassificationOfExperimentsList'
    
    foreign_keys['Has_goal'] = 'EXPO_app:ExperimentalGoalList'
    
    object_to_delete = get_object_or_404(ScientificExperiment,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ScientificExperiment/list/"
        return HttpResponseRedirect(link)
    else:
         form = ScientificExperimentForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ScientificExperimentDelete.html",context)

def ExperimentalDesignDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ExperimentalDesign,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ExperimentalDesign/list/"
        return HttpResponseRedirect(link)
    else:
         form = ExperimentalDesignForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ExperimentalDesignDelete.html",context)

def AdminInfoExperimentDelete(request, pk):
    foreign_keys = dict()
    
    foreign_keys['Has_ID'] = 'EXPO_app:ThingList'
    
    foreign_keys['Has_ID'] = 'EXPO_app:IDExperimentList'
    
    foreign_keys['Has_author'] = 'EXPO_app:AuthorList'
    
    foreign_keys['Has_organization'] = 'EXPO_app:OrganizationList'
    
    foreign_keys['Has_reference'] = 'EXPO_app:BiblioReferenceList'
    
    foreign_keys['Has_status'] = 'EXPO_app:StatusExperimentalDocumentList'
    
    foreign_keys['Has_title'] = 'EXPO_app:TitleExperimentList'
    
    object_to_delete = get_object_or_404(AdminInfoExperiment,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/AdminInfoExperiment/list/"
        return HttpResponseRedirect(link)
    else:
         form = AdminInfoExperimentForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AdminInfoExperimentDelete.html",context)

def IDExperimentDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(IDExperiment,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/IDExperiment/list/"
        return HttpResponseRedirect(link)
    else:
         form = IDExperimentForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "IDExperimentDelete.html",context)

def DomainOfExperimentDelete(request, pk):
    foreign_keys = dict()
    
    foreign_keys['Has_SUMO_sinonym'] = 'EXPO_app:SynonymousExternalConceptList'
    
    foreign_keys['Has_classification'] = 'EXPO_app:ClassificationOfDomainsList'
    
    foreign_keys['Has_model'] = 'EXPO_app:DomainModelList'
    
    object_to_delete = get_object_or_404(DomainOfExperiment,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/DomainOfExperiment/list/"
        return HttpResponseRedirect(link)
    else:
         form = DomainOfExperimentForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "DomainOfExperimentDelete.html",context)

def ExperimentalObservationDelete(request, pk):
    foreign_keys = dict()
    
    foreign_keys['Has__interpretation'] = 'EXPO_app:ResultsInterpretationList'
    
    foreign_keys['Has_error'] = 'EXPO_app:ObservationalErrorList'
    
    foreign_keys['Has_observation_characteristic'] = 'EXPO_app:StatisticsCharacteristicList'
    
    object_to_delete = get_object_or_404(ExperimentalObservation,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ExperimentalObservation/list/"
        return HttpResponseRedirect(link)
    else:
         form = ExperimentalObservationForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ExperimentalObservationDelete.html",context)

def ResultsInterpretationDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ResultsInterpretation,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ResultsInterpretation/list/"
        return HttpResponseRedirect(link)
    else:
         form = ResultsInterpretationForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ResultsInterpretationDelete.html",context)

def ExperimentalActionDelete(request, pk):
    foreign_keys = dict()
    
    foreign_keys['Has__name'] = 'EXPO_app:ActionNameList'
    
    foreign_keys['Has_action_complexity'] = 'EXPO_app:ActionComplexityList'
    
    foreign_keys['Has_goal'] = 'EXPO_app:ActionGoalList'
    
    foreign_keys['Has_instrument'] = 'EXPO_app:ExperimentalEquipmentList'
    
    foreign_keys['Has_method'] = 'EXPO_app:ExperimentalMethodList'
    
    foreign_keys['Has_timing'] = 'EXPO_app:TimePointList'
    
    foreign_keys['Has_action_complexity'] = 'EXPO_app:ThingList'
    
    object_to_delete = get_object_or_404(ExperimentalAction,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ExperimentalAction/list/"
        return HttpResponseRedirect(link)
    else:
         form = ExperimentalActionForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ExperimentalActionDelete.html",context)

def ActionNameDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ActionName,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ActionName/list/"
        return HttpResponseRedirect(link)
    else:
         form = ActionNameForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ActionNameDelete.html",context)

def ActionComplexityDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ActionComplexity,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ActionComplexity/list/"
        return HttpResponseRedirect(link)
    else:
         form = ActionComplexityForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ActionComplexityDelete.html",context)

def HumanDelete(request, pk):
    foreign_keys = dict()
    
    foreign_keys['Has_organization'] = 'EXPO_app:OrganizationList'
    
    foreign_keys['Has_telephone_number'] = 'EXPO_app:NumberList'
    
    foreign_keys['Has_title'] = 'EXPO_app:TitleList'
    
    foreign_keys['Has_name'] = 'EXPO_app:ThingList'
    
    object_to_delete = get_object_or_404(Human,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Human/list/"
        return HttpResponseRedirect(link)
    else:
         form = HumanForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "HumanDelete.html",context)

def OrganizationDelete(request, pk):
    foreign_keys = dict()
    
    foreign_keys['Has_fax_number'] = 'EXPO_app:NumberList'
    
    foreign_keys['Has_name'] = 'EXPO_app:NameList'
    
    foreign_keys['Has_telephone_number'] = 'EXPO_app:NumberList'
    
    foreign_keys['Has_name'] = 'EXPO_app:EntityList'
    
    object_to_delete = get_object_or_404(Organization,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Organization/list/"
        return HttpResponseRedirect(link)
    else:
         form = OrganizationForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "OrganizationDelete.html",context)

def AdminInfoAuthorDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(AdminInfoAuthor,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/AdminInfoAuthor/list/"
        return HttpResponseRedirect(link)
    else:
         form = AdminInfoAuthorForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AdminInfoAuthorDelete.html",context)

def AdminInfoObjectOfExperimentDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(AdminInfoObjectOfExperiment,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/AdminInfoObjectOfExperiment/list/"
        return HttpResponseRedirect(link)
    else:
         form = AdminInfoObjectOfExperimentForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AdminInfoObjectOfExperimentDelete.html",context)

def AdminInfoProviderDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(AdminInfoProvider,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/AdminInfoProvider/list/"
        return HttpResponseRedirect(link)
    else:
         form = AdminInfoProviderForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AdminInfoProviderDelete.html",context)

def AdminInfoSubjectOfExperimentDelete(request, pk):
    foreign_keys = dict()
    
    foreign_keys['Has_name'] = 'EXPO_app:NameList'
    
    foreign_keys['Has_organization'] = 'EXPO_app:OrganizationList'
    
    foreign_keys['Has_qualification'] = 'EXPO_app:SubjectQualificationList'
    
    object_to_delete = get_object_or_404(AdminInfoSubjectOfExperiment,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/AdminInfoSubjectOfExperiment/list/"
        return HttpResponseRedirect(link)
    else:
         form = AdminInfoSubjectOfExperimentForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AdminInfoSubjectOfExperimentDelete.html",context)

def AdminInfoSubmitterDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(AdminInfoSubmitter,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/AdminInfoSubmitter/list/"
        return HttpResponseRedirect(link)
    else:
         form = AdminInfoSubmitterForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AdminInfoSubmitterDelete.html",context)

def AdminInfoUserDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(AdminInfoUser,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/AdminInfoUser/list/"
        return HttpResponseRedirect(link)
    else:
         form = AdminInfoUserForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AdminInfoUserDelete.html",context)

def ExperimentalMethodDelete(request, pk):
    foreign_keys = dict()
    
    foreign_keys['Has_applicability'] = 'EXPO_app:MethodApplicabilityConditionList'
    
    foreign_keys['Has_goal'] = 'EXPO_app:MethodGoalList'
    
    foreign_keys['Has_plan'] = 'EXPO_app:PlanOfExperimentalActionsList'
    
    foreign_keys['Has_subject'] = 'EXPO_app:SubjectMethodList'
    
    object_to_delete = get_object_or_404(ExperimentalMethod,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ExperimentalMethod/list/"
        return HttpResponseRedirect(link)
    else:
         form = ExperimentalMethodForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ExperimentalMethodDelete.html",context)

def MethodApplicabilityConditionDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(MethodApplicabilityCondition,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/MethodApplicabilityCondition/list/"
        return HttpResponseRedirect(link)
    else:
         form = MethodApplicabilityConditionForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "MethodApplicabilityConditionDelete.html",context)

def StandardOperatingProcedureDelete(request, pk):
    foreign_keys = dict()
    
    foreign_keys['Has_author'] = 'EXPO_app:AuthorSOPList'
    
    foreign_keys['Has_latest_modification'] = 'EXPO_app:TimePointList'
    
    foreign_keys['Has_procedure'] = 'EXPO_app:ProcedureExecuteExperimentList'
    
    foreign_keys['Has_representation'] = 'EXPO_app:BiblioReferenceList'
    
    foreign_keys['Has_latest_modification'] = 'EXPO_app:ThingList'
    
    object_to_delete = get_object_or_404(StandardOperatingProcedure,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/StandardOperatingProcedure/list/"
        return HttpResponseRedirect(link)
    else:
         form = StandardOperatingProcedureForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "StandardOperatingProcedureDelete.html",context)

def AuthorDelete(request, pk):
    foreign_keys = dict()
    
    foreign_keys['Has_admin_info'] = 'EXPO_app:ThingList'
    
    foreign_keys['Has_admin_info'] = 'EXPO_app:AdminInfoAuthorList'
    
    object_to_delete = get_object_or_404(Author,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Author/list/"
        return HttpResponseRedirect(link)
    else:
         form = AuthorForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AuthorDelete.html",context)

def ClassificationOfDomainsDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ClassificationOfDomains,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ClassificationOfDomains/list/"
        return HttpResponseRedirect(link)
    else:
         form = ClassificationOfDomainsForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ClassificationOfDomainsDelete.html",context)

def ClassificationOfExperimentsDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ClassificationOfExperiments,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ClassificationOfExperiments/list/"
        return HttpResponseRedirect(link)
    else:
         form = ClassificationOfExperimentsForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ClassificationOfExperimentsDelete.html",context)

def ExperimentalHypothesisDelete(request, pk):
    foreign_keys = dict()
    
    foreign_keys['Has_explicitness'] = 'EXPO_app:ThingList'
    
    foreign_keys['Has_complexity'] = 'EXPO_app:HypothesisComplexityList'
    
    foreign_keys['Has_explicitness'] = 'EXPO_app:HypothesisExplicitnessList'
    
    foreign_keys['Has_generality'] = 'EXPO_app:GeneralityOfHypothesisList'
    
    foreign_keys['Has_hypothesis_representation'] = 'EXPO_app:HypothesisRepresentationList'
    
    foreign_keys['Has_target_variable'] = 'EXPO_app:TargetVariableList'
    
    foreign_keys['Has_complexity'] = 'EXPO_app:ThingList'
    
    foreign_keys['Has_generality'] = 'EXPO_app:ThingList'
    
    object_to_delete = get_object_or_404(ExperimentalHypothesis,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ExperimentalHypothesis/list/"
        return HttpResponseRedirect(link)
    else:
         form = ExperimentalHypothesisForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ExperimentalHypothesisDelete.html",context)

def HypothesisComplexityDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(HypothesisComplexity,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/HypothesisComplexity/list/"
        return HttpResponseRedirect(link)
    else:
         form = HypothesisComplexityForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "HypothesisComplexityDelete.html",context)

def RepresentationDelete(request, pk):
    foreign_keys = dict()
    
    foreign_keys['Has_content'] = 'EXPO_app:PropositionList'
    
    foreign_keys['Has_representation_style'] = 'EXPO_app:RepresentationFormList'
    
    foreign_keys['Has_content'] = 'EXPO_app:ThingList'
    
    foreign_keys['Has_representation_style'] = 'EXPO_app:ThingList'
    
    object_to_delete = get_object_or_404(Representation,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Representation/list/"
        return HttpResponseRedirect(link)
    else:
         form = RepresentationForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "RepresentationDelete.html",context)

def PropositionDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Proposition,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Proposition/list/"
        return HttpResponseRedirect(link)
    else:
         form = PropositionForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "PropositionDelete.html",context)

def ExperimentalEquipmentDelete(request, pk):
    foreign_keys = dict()
    
    foreign_keys['Has_description'] = 'EXPO_app:TechnicalDescriptionList'
    
    foreign_keys['Has_name'] = 'EXPO_app:NameEquipmentList'
    
    object_to_delete = get_object_or_404(ExperimentalEquipment,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ExperimentalEquipment/list/"
        return HttpResponseRedirect(link)
    else:
         form = ExperimentalEquipmentForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ExperimentalEquipmentDelete.html",context)

def TechnicalDescriptionDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(TechnicalDescription,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/TechnicalDescription/list/"
        return HttpResponseRedirect(link)
    else:
         form = TechnicalDescriptionForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "TechnicalDescriptionDelete.html",context)

def ExperimentalResultsDelete(request, pk):
    foreign_keys = dict()
    
    foreign_keys['Has_error'] = 'EXPO_app:ResultErrorList'
    
    foreign_keys['Has_evaluation'] = 'EXPO_app:ResultsEvaluationList'
    
    foreign_keys['Has_fact'] = 'EXPO_app:FactRejectResearchHypothesisList'
    
    foreign_keys['Has_fact'] = 'EXPO_app:FactSupportResearchHypothesisList'
    
    object_to_delete = get_object_or_404(ExperimentalResults,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ExperimentalResults/list/"
        return HttpResponseRedirect(link)
    else:
         form = ExperimentalResultsForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ExperimentalResultsDelete.html",context)

def ObservationalErrorDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ObservationalError,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ObservationalError/list/"
        return HttpResponseRedirect(link)
    else:
         form = ObservationalErrorForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ObservationalErrorDelete.html",context)

def ResultErrorDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ResultError,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ResultError/list/"
        return HttpResponseRedirect(link)
    else:
         form = ResultErrorForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ResultErrorDelete.html",context)

def ResultsEvaluationDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ResultsEvaluation,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ResultsEvaluation/list/"
        return HttpResponseRedirect(link)
    else:
         form = ResultsEvaluationForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ResultsEvaluationDelete.html",context)

def ScientificInvestigationDelete(request, pk):
    foreign_keys = dict()
    
    foreign_keys['Has_experiment'] = 'EXPO_app:ScientificExperimentList'
    
    foreign_keys['Has_experimental_design'] = 'EXPO_app:ExperimentalDesignList'
    
    foreign_keys['Has_hypothesis_formation'] = 'EXPO_app:HypothesisFormationList'
    
    foreign_keys['Has_information_gethering'] = 'EXPO_app:InformationGetheringList'
    
    foreign_keys['Has_problem_analysis'] = 'EXPO_app:PoblemAnalysisList'
    
    foreign_keys['Has_result_evaluation'] = 'EXPO_app:ResultsEvaluationList'
    
    foreign_keys['Has_result_interpretation'] = 'EXPO_app:ResultsInterpretationList'
    
    object_to_delete = get_object_or_404(ScientificInvestigation,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ScientificInvestigation/list/"
        return HttpResponseRedirect(link)
    else:
         form = ScientificInvestigationForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ScientificInvestigationDelete.html",context)

def ExperimentalModelDelete(request, pk):
    foreign_keys = dict()
    
    foreign_keys['Has_experimental_factor'] = 'EXPO_app:ExperimentalFactorList'
    
    foreign_keys['Has_unknown_variable'] = 'EXPO_app:TargetVariableList'
    
    object_to_delete = get_object_or_404(ExperimentalModel,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ExperimentalModel/list/"
        return HttpResponseRedirect(link)
    else:
         form = ExperimentalModelForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ExperimentalModelDelete.html",context)

def ExperimentalFactorDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ExperimentalFactor,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ExperimentalFactor/list/"
        return HttpResponseRedirect(link)
    else:
         form = ExperimentalFactorForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ExperimentalFactorDelete.html",context)

def ExperimentalTechnologyDelete(request, pk):
    foreign_keys = dict()
    
    foreign_keys['Has_experimental_requirements'] = 'EXPO_app:ExperimentalRequirementsList'
    
    foreign_keys['Has_standard'] = 'EXPO_app:ExperimentalStandardList'
    
    foreign_keys['Has_strategy'] = 'EXPO_app:ExperimentalDesignStrategyList'
    
    object_to_delete = get_object_or_404(ExperimentalTechnology,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ExperimentalTechnology/list/"
        return HttpResponseRedirect(link)
    else:
         form = ExperimentalTechnologyForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ExperimentalTechnologyDelete.html",context)

def ExperimentalRequirementsDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ExperimentalRequirements,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ExperimentalRequirements/list/"
        return HttpResponseRedirect(link)
    else:
         form = ExperimentalRequirementsForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ExperimentalRequirementsDelete.html",context)

def HypothesisExplicitnessDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(HypothesisExplicitness,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/HypothesisExplicitness/list/"
        return HttpResponseRedirect(link)
    else:
         form = HypothesisExplicitnessForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "HypothesisExplicitnessDelete.html",context)

def LinguisticExpressionDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(LinguisticExpression,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/LinguisticExpression/list/"
        return HttpResponseRedirect(link)
    else:
         form = LinguisticExpressionForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "LinguisticExpressionDelete.html",context)

def FactRejectResearchHypothesisDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(FactRejectResearchHypothesis,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/FactRejectResearchHypothesis/list/"
        return HttpResponseRedirect(link)
    else:
         form = FactRejectResearchHypothesisForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "FactRejectResearchHypothesisDelete.html",context)

def FactSupportResearchHypothesisDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(FactSupportResearchHypothesis,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/FactSupportResearchHypothesis/list/"
        return HttpResponseRedirect(link)
    else:
         form = FactSupportResearchHypothesisForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "FactSupportResearchHypothesisDelete.html",context)

def NumberDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Number,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Number/list/"
        return HttpResponseRedirect(link)
    else:
         form = NumberForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "NumberDelete.html",context)

def GeneralityOfHypothesisDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(GeneralityOfHypothesis,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/GeneralityOfHypothesis/list/"
        return HttpResponseRedirect(link)
    else:
         form = GeneralityOfHypothesisForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "GeneralityOfHypothesisDelete.html",context)

def ActionGoalDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ActionGoal,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ActionGoal/list/"
        return HttpResponseRedirect(link)
    else:
         form = ActionGoalForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ActionGoalDelete.html",context)

def ExperimentalGoalDelete(request, pk):
    foreign_keys = dict()
    
    foreign_keys['Has_representation'] = 'EXPO_app:RepresentationExperimentalGoalList'
    
    foreign_keys['Has_subgoal'] = 'EXPO_app:ExperimentalSubgoalList'
    
    object_to_delete = get_object_or_404(ExperimentalGoal,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ExperimentalGoal/list/"
        return HttpResponseRedirect(link)
    else:
         form = ExperimentalGoalForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ExperimentalGoalDelete.html",context)

def MethodGoalDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(MethodGoal,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/MethodGoal/list/"
        return HttpResponseRedirect(link)
    else:
         form = MethodGoalForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "MethodGoalDelete.html",context)

def HypothesisFormationDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(HypothesisFormation,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/HypothesisFormation/list/"
        return HttpResponseRedirect(link)
    else:
         form = HypothesisFormationForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "HypothesisFormationDelete.html",context)

def HypothesisRepresentationDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(HypothesisRepresentation,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/HypothesisRepresentation/list/"
        return HttpResponseRedirect(link)
    else:
         form = HypothesisRepresentationForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "HypothesisRepresentationDelete.html",context)

def InformationGetheringDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(InformationGethering,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/InformationGethering/list/"
        return HttpResponseRedirect(link)
    else:
         form = InformationGetheringForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "InformationGetheringDelete.html",context)

def SampleDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Sample,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Sample/list/"
        return HttpResponseRedirect(link)
    else:
         form = SampleForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SampleDelete.html",context)

def VariabilityDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Variability,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Variability/list/"
        return HttpResponseRedirect(link)
    else:
         form = VariabilityForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "VariabilityDelete.html",context)

def TimePointDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(TimePoint,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/TimePoint/list/"
        return HttpResponseRedirect(link)
    else:
         form = TimePointForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "TimePointDelete.html",context)

def UserDelete(request, pk):
    foreign_keys = dict()
    
    foreign_keys['Has_admin_info'] = 'EXPO_app:AdminInfoUserList'
    
    foreign_keys['Has_login'] = 'EXPO_app:LoginNameList'
    
    foreign_keys['Has_pasword'] = 'EXPO_app:PasswordList'
    
    foreign_keys['Has_admin_info'] = 'EXPO_app:ThingList'
    
    object_to_delete = get_object_or_404(User,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/User/list/"
        return HttpResponseRedirect(link)
    else:
         form = UserForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "UserDelete.html",context)

def LoginNameDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(LoginName,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/LoginName/list/"
        return HttpResponseRedirect(link)
    else:
         form = LoginNameForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "LoginNameDelete.html",context)

def DomainModelDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(DomainModel,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/DomainModel/list/"
        return HttpResponseRedirect(link)
    else:
         form = DomainModelForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "DomainModelDelete.html",context)

def GroupExperimentalObjectDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(GroupExperimentalObject,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/GroupExperimentalObject/list/"
        return HttpResponseRedirect(link)
    else:
         form = GroupExperimentalObjectForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "GroupExperimentalObjectDelete.html",context)

def ObjectOfExperimentDelete(request, pk):
    foreign_keys = dict()
    
    foreign_keys['Has_admin_info'] = 'EXPO_app:ThingList'
    
    object_to_delete = get_object_or_404(ObjectOfExperiment,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ObjectOfExperiment/list/"
        return HttpResponseRedirect(link)
    else:
         form = ObjectOfExperimentForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ObjectOfExperimentDelete.html",context)

def ClassificationByModelDelete(request, pk):
    foreign_keys = dict()
    
    foreign_keys['Has_number_of_factors'] = 'EXPO_app:NumberList'
    
    foreign_keys['Has_number_of_factors'] = 'EXPO_app:ThingList'
    
    object_to_delete = get_object_or_404(ClassificationByModel,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ClassificationByModel/list/"
        return HttpResponseRedirect(link)
    else:
         form = ClassificationByModelForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ClassificationByModelDelete.html",context)

def StatisticsCharacteristicDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(StatisticsCharacteristic,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/StatisticsCharacteristic/list/"
        return HttpResponseRedirect(link)
    else:
         form = StatisticsCharacteristicForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "StatisticsCharacteristicDelete.html",context)

def ParentGroupDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ParentGroup,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ParentGroup/list/"
        return HttpResponseRedirect(link)
    else:
         form = ParentGroupForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ParentGroupDelete.html",context)

def PasswordDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Password,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Password/list/"
        return HttpResponseRedirect(link)
    else:
         form = PasswordForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "PasswordDelete.html",context)

def ProcedureExecuteExperimentDelete(request, pk):
    foreign_keys = dict()
    
    foreign_keys['Has_plan'] = 'EXPO_app:PlanOfExperimentalActionsList'
    
    foreign_keys['Has_representation'] = 'EXPO_app:RepresentationExperimentalExecutionProcedureList'
    
    foreign_keys['Has_status'] = 'EXPO_app:DocumentStageList'
    
    foreign_keys['Has_status'] = 'EXPO_app:PermissionStatusList'
    
    foreign_keys['Has_status'] = 'EXPO_app:ThingList'
    
    object_to_delete = get_object_or_404(ProcedureExecuteExperiment,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ProcedureExecuteExperiment/list/"
        return HttpResponseRedirect(link)
    else:
         form = ProcedureExecuteExperimentForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ProcedureExecuteExperimentDelete.html",context)

def PlanOfExperimentalActionsDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(PlanOfExperimentalActions,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/PlanOfExperimentalActions/list/"
        return HttpResponseRedirect(link)
    else:
         form = PlanOfExperimentalActionsForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "PlanOfExperimentalActionsDelete.html",context)

def PoblemAnalysisDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(PoblemAnalysis,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/PoblemAnalysis/list/"
        return HttpResponseRedirect(link)
    else:
         form = PoblemAnalysisForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "PoblemAnalysisDelete.html",context)

def SubjectQualificationDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(SubjectQualification,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/SubjectQualification/list/"
        return HttpResponseRedirect(link)
    else:
         form = SubjectQualificationForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SubjectQualificationDelete.html",context)

def BiblioReferenceDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(BiblioReference,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/BiblioReference/list/"
        return HttpResponseRedirect(link)
    else:
         form = BiblioReferenceForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BiblioReferenceDelete.html",context)

def ModelDelete(request, pk):
    foreign_keys = dict()
    
    foreign_keys['Has_representation'] = 'EXPO_app:ModelRepresentationList'
    
    object_to_delete = get_object_or_404(Model,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Model/list/"
        return HttpResponseRedirect(link)
    else:
         form = ModelForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ModelDelete.html",context)

def ModelRepresentationDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ModelRepresentation,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ModelRepresentation/list/"
        return HttpResponseRedirect(link)
    else:
         form = ModelRepresentationForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ModelRepresentationDelete.html",context)

def RepresentationExperimentalExecutionProcedureDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(RepresentationExperimentalExecutionProcedure,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/RepresentationExperimentalExecutionProcedure/list/"
        return HttpResponseRedirect(link)
    else:
         form = RepresentationExperimentalExecutionProcedureForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "RepresentationExperimentalExecutionProcedureDelete.html",context)

def RepresentationExperimentalGoalDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(RepresentationExperimentalGoal,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/RepresentationExperimentalGoal/list/"
        return HttpResponseRedirect(link)
    else:
         form = RepresentationExperimentalGoalForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "RepresentationExperimentalGoalDelete.html",context)

def SampleRepresentativenessDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(SampleRepresentativeness,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/SampleRepresentativeness/list/"
        return HttpResponseRedirect(link)
    else:
         form = SampleRepresentativenessForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SampleRepresentativenessDelete.html",context)

def SampleSizeDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(SampleSize,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/SampleSize/list/"
        return HttpResponseRedirect(link)
    else:
         form = SampleSizeForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SampleSizeDelete.html",context)

def SamplingMethodDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(SamplingMethod,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/SamplingMethod/list/"
        return HttpResponseRedirect(link)
    else:
         form = SamplingMethodForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SamplingMethodDelete.html",context)

def SystematicSamplingDelete(request, pk):
    foreign_keys = dict()
    
    foreign_keys['Has_sampling_rule'] = 'EXPO_app:SamplingRuleList'
    
    object_to_delete = get_object_or_404(SystematicSampling,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/SystematicSampling/list/"
        return HttpResponseRedirect(link)
    else:
         form = SystematicSamplingForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SystematicSamplingDelete.html",context)

def SamplingRuleDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(SamplingRule,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/SamplingRule/list/"
        return HttpResponseRedirect(link)
    else:
         form = SamplingRuleForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SamplingRuleDelete.html",context)

def ExperimentalStandardDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ExperimentalStandard,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ExperimentalStandard/list/"
        return HttpResponseRedirect(link)
    else:
         form = ExperimentalStandardForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ExperimentalStandardDelete.html",context)

def ErrorDelete(request, pk):
    foreign_keys = dict()
    
    foreign_keys['Has_statistics_characteristic'] = 'EXPO_app:DispersionList'
    
    foreign_keys['Has_statistics_characteristic'] = 'EXPO_app:LevelOfSignificanceList'
    
    foreign_keys['Has_statistics_characteristic'] = 'EXPO_app:ThingList'
    
    object_to_delete = get_object_or_404(Error,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Error/list/"
        return HttpResponseRedirect(link)
    else:
         form = ErrorForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ErrorDelete.html",context)

def DispersionDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Dispersion,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Dispersion/list/"
        return HttpResponseRedirect(link)
    else:
         form = DispersionForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "DispersionDelete.html",context)

def LevelOfSignificanceDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(LevelOfSignificance,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/LevelOfSignificance/list/"
        return HttpResponseRedirect(link)
    else:
         form = LevelOfSignificanceForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "LevelOfSignificanceDelete.html",context)

def StatusExperimentalDocumentDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(StatusExperimentalDocument,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/StatusExperimentalDocument/list/"
        return HttpResponseRedirect(link)
    else:
         form = StatusExperimentalDocumentForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "StatusExperimentalDocumentDelete.html",context)

def ExperimentalDesignStrategyDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ExperimentalDesignStrategy,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ExperimentalDesignStrategy/list/"
        return HttpResponseRedirect(link)
    else:
         form = ExperimentalDesignStrategyForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ExperimentalDesignStrategyDelete.html",context)

def ExperimentalSubgoalDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ExperimentalSubgoal,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ExperimentalSubgoal/list/"
        return HttpResponseRedirect(link)
    else:
         form = ExperimentalSubgoalForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ExperimentalSubgoalDelete.html",context)

def SubjectMethodDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(SubjectMethod,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/SubjectMethod/list/"
        return HttpResponseRedirect(link)
    else:
         form = SubjectMethodForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SubjectMethodDelete.html",context)

def BaconianExperimentDelete(request, pk):
    foreign_keys = dict()
    
    foreign_keys['Has_synonym'] = 'EXPO_app:SynonymousExternalConceptList'
    
    foreign_keys['Has_synonym'] = 'EXPO_app:ThingList'
    
    object_to_delete = get_object_or_404(BaconianExperiment,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/BaconianExperiment/list/"
        return HttpResponseRedirect(link)
    else:
         form = BaconianExperimentForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BaconianExperimentDelete.html",context)

def TargetVariableDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(TargetVariable,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/TargetVariable/list/"
        return HttpResponseRedirect(link)
    else:
         form = TargetVariableForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "TargetVariableDelete.html",context)

def TitleDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Title,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Title/list/"
        return HttpResponseRedirect(link)
    else:
         form = TitleForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "TitleDelete.html",context)

def ValueEstimateDelete(request, pk):
    foreign_keys = dict()
    
    foreign_keys['Has_value_of_variable_'] = 'EXPO_app:ValueOfVariableList'
    
    object_to_delete = get_object_or_404(ValueEstimate,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ValueEstimate/list/"
        return HttpResponseRedirect(link)
    else:
         form = ValueEstimateForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ValueEstimateDelete.html",context)

def ValueOfVariableDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ValueOfVariable,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ValueOfVariable/list/"
        return HttpResponseRedirect(link)
    else:
         form = ValueOfVariableForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ValueOfVariableDelete.html",context)

def RoleDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Role,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Role/list/"
        return HttpResponseRedirect(link)
    else:
         form = RoleForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "RoleDelete.html",context)

def CorpuscularObjectDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(CorpuscularObject,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/CorpuscularObject/list/"
        return HttpResponseRedirect(link)
    else:
         form = CorpuscularObjectForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "CorpuscularObjectDelete.html",context)

def AttributeRoleDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(AttributeRole,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/AttributeRole/list/"
        return HttpResponseRedirect(link)
    else:
         form = AttributeRoleForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AttributeRoleDelete.html",context)

def AttributeDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Attribute,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Attribute/list/"
        return HttpResponseRedirect(link)
    else:
         form = AttributeForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AttributeDelete.html",context)

def ProcedureDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Procedure,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Procedure/list/"
        return HttpResponseRedirect(link)
    else:
         form = ProcedureForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ProcedureDelete.html",context)

def AdministrativeInformationDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(AdministrativeInformation,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/AdministrativeInformation/list/"
        return HttpResponseRedirect(link)
    else:
         form = AdministrativeInformationForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AdministrativeInformationDelete.html",context)

def TitleExperimentDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(TitleExperiment,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/TitleExperiment/list/"
        return HttpResponseRedirect(link)
    else:
         form = TitleExperimentForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "TitleExperimentDelete.html",context)

def NameEquipmentDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(NameEquipment,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/NameEquipment/list/"
        return HttpResponseRedirect(link)
    else:
         form = NameEquipmentForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "NameEquipmentDelete.html",context)

def PersonalNameDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(PersonalName,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/PersonalName/list/"
        return HttpResponseRedirect(link)
    else:
         form = PersonalNameForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "PersonalNameDelete.html",context)

def FieldOfStudyDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(FieldOfStudy,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/FieldOfStudy/list/"
        return HttpResponseRedirect(link)
    else:
         form = FieldOfStudyForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "FieldOfStudyDelete.html",context)

def RelatedDomainDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(RelatedDomain,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/RelatedDomain/list/"
        return HttpResponseRedirect(link)
    else:
         form = RelatedDomainForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "RelatedDomainDelete.html",context)

def ProductRoleDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ProductRole,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ProductRole/list/"
        return HttpResponseRedirect(link)
    else:
         form = ProductRoleForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ProductRoleDelete.html",context)

def ScientificTaskDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ScientificTask,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ScientificTask/list/"
        return HttpResponseRedirect(link)
    else:
         form = ScientificTaskForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ScientificTaskDelete.html",context)

def ExecutionOfExperimentDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ExecutionOfExperiment,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ExecutionOfExperiment/list/"
        return HttpResponseRedirect(link)
    else:
         form = ExecutionOfExperimentForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ExecutionOfExperimentDelete.html",context)

def CorporateNameDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(CorporateName,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/CorporateName/list/"
        return HttpResponseRedirect(link)
    else:
         form = CorporateNameForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "CorporateNameDelete.html",context)

def AttributeOfActionDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(AttributeOfAction,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/AttributeOfAction/list/"
        return HttpResponseRedirect(link)
    else:
         form = AttributeOfActionForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AttributeOfActionDelete.html",context)

def SentientAgentDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(SentientAgent,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/SentientAgent/list/"
        return HttpResponseRedirect(link)
    else:
         form = SentientAgentForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SentientAgentDelete.html",context)

def RobotDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Robot,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Robot/list/"
        return HttpResponseRedirect(link)
    else:
         form = RobotForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "RobotDelete.html",context)

def GroupDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Group,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Group/list/"
        return HttpResponseRedirect(link)
    else:
         form = GroupForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "GroupDelete.html",context)

def EntityDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Entity,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Entity/list/"
        return HttpResponseRedirect(link)
    else:
         form = EntityForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "EntityDelete.html",context)

def ResearchMethodDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ResearchMethod,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ResearchMethod/list/"
        return HttpResponseRedirect(link)
    else:
         form = ResearchMethodForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ResearchMethodDelete.html",context)

def RequirementsDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Requirements,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Requirements/list/"
        return HttpResponseRedirect(link)
    else:
         form = RequirementsForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "RequirementsDelete.html",context)

def AuthorSOPDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(AuthorSOP,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/AuthorSOP/list/"
        return HttpResponseRedirect(link)
    else:
         form = AuthorSOPForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AuthorSOPDelete.html",context)

def SubjectRoleDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(SubjectRole,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/SubjectRole/list/"
        return HttpResponseRedirect(link)
    else:
         form = SubjectRoleForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SubjectRoleDelete.html",context)

def ClassificationDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Classification,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Classification/list/"
        return HttpResponseRedirect(link)
    else:
         form = ClassificationForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ClassificationDelete.html",context)

def FactDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Fact,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Fact/list/"
        return HttpResponseRedirect(link)
    else:
         form = FactForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "FactDelete.html",context)

def ModelAssumptionDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ModelAssumption,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ModelAssumption/list/"
        return HttpResponseRedirect(link)
    else:
         form = ModelAssumptionForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ModelAssumptionDelete.html",context)

def VariableDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Variable,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Variable/list/"
        return HttpResponseRedirect(link)
    else:
         form = VariableForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "VariableDelete.html",context)

def AttributeOfHypothesisDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(AttributeOfHypothesis,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/AttributeOfHypothesis/list/"
        return HttpResponseRedirect(link)
    else:
         form = AttributeOfHypothesisForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AttributeOfHypothesisDelete.html",context)

def ContentBearingObjectDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ContentBearingObject,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ContentBearingObject/list/"
        return HttpResponseRedirect(link)
    else:
         form = ContentBearingObjectForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ContentBearingObjectDelete.html",context)

def AbstractDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Abstract,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Abstract/list/"
        return HttpResponseRedirect(link)
    else:
         form = AbstractForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AbstractDelete.html",context)

def QuantityDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Quantity,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Quantity/list/"
        return HttpResponseRedirect(link)
    else:
         form = QuantityForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "QuantityDelete.html",context)

def RelationDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Relation,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Relation/list/"
        return HttpResponseRedirect(link)
    else:
         form = RelationForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "RelationDelete.html",context)

def ProcessrelatedRoleDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ProcessrelatedRole,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ProcessrelatedRole/list/"
        return HttpResponseRedirect(link)
    else:
         form = ProcessrelatedRoleForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ProcessrelatedRoleDelete.html",context)

def ContentBearingPhysicalDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ContentBearingPhysical,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ContentBearingPhysical/list/"
        return HttpResponseRedirect(link)
    else:
         form = ContentBearingPhysicalForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ContentBearingPhysicalDelete.html",context)

def PhysicalQuantityDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(PhysicalQuantity,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/PhysicalQuantity/list/"
        return HttpResponseRedirect(link)
    else:
         form = PhysicalQuantityForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "PhysicalQuantityDelete.html",context)

def GoalDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Goal,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Goal/list/"
        return HttpResponseRedirect(link)
    else:
         form = GoalForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "GoalDelete.html",context)

def RepresentationExperimentalObservationDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(RepresentationExperimentalObservation,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/RepresentationExperimentalObservation/list/"
        return HttpResponseRedirect(link)
    else:
         form = RepresentationExperimentalObservationForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "RepresentationExperimentalObservationDelete.html",context)

def RepresentationExperimentalResultsDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(RepresentationExperimentalResults,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/RepresentationExperimentalResults/list/"
        return HttpResponseRedirect(link)
    else:
         form = RepresentationExperimentalResultsForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "RepresentationExperimentalResultsDelete.html",context)

def AttributeGroupDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(AttributeGroup,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/AttributeGroup/list/"
        return HttpResponseRedirect(link)
    else:
         form = AttributeGroupForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AttributeGroupDelete.html",context)

def TimePositionDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(TimePosition,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/TimePosition/list/"
        return HttpResponseRedirect(link)
    else:
         form = TimePositionForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "TimePositionDelete.html",context)

def ActorRoleDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ActorRole,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ActorRole/list/"
        return HttpResponseRedirect(link)
    else:
         form = ActorRoleForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ActorRoleDelete.html",context)

def DocumentStageDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(DocumentStage,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/DocumentStage/list/"
        return HttpResponseRedirect(link)
    else:
         form = DocumentStageForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "DocumentStageDelete.html",context)

def PermissionStatusDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(PermissionStatus,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/PermissionStatus/list/"
        return HttpResponseRedirect(link)
    else:
         form = PermissionStatusForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "PermissionStatusDelete.html",context)

def PlanDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Plan,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Plan/list/"
        return HttpResponseRedirect(link)
    else:
         form = PlanForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "PlanDelete.html",context)

def ReferenceDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Reference,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Reference/list/"
        return HttpResponseRedirect(link)
    else:
         form = ReferenceForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ReferenceDelete.html",context)

def AttributeSampleDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(AttributeSample,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/AttributeSample/list/"
        return HttpResponseRedirect(link)
    else:
         form = AttributeSampleForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AttributeSampleDelete.html",context)

def ExperimentalRuleDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ExperimentalRule,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ExperimentalRule/list/"
        return HttpResponseRedirect(link)
    else:
         form = ExperimentalRuleForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ExperimentalRuleDelete.html",context)

def RobustnessDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Robustness,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Robustness/list/"
        return HttpResponseRedirect(link)
    else:
         form = RobustnessForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "RobustnessDelete.html",context)

def ValidityDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Validity,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Validity/list/"
        return HttpResponseRedirect(link)
    else:
         form = ValidityForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ValidityDelete.html",context)

def AttributeOfDocumentDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(AttributeOfDocument,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/AttributeOfDocument/list/"
        return HttpResponseRedirect(link)
    else:
         form = AttributeOfDocumentForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AttributeOfDocumentDelete.html",context)

def PhysicalExperimentDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(PhysicalExperiment,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/PhysicalExperiment/list/"
        return HttpResponseRedirect(link)
    else:
         form = PhysicalExperimentForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "PhysicalExperimentDelete.html",context)

def ComputationalDataDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ComputationalData,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ComputationalData/list/"
        return HttpResponseRedirect(link)
    else:
         form = ComputationalDataForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ComputationalDataDelete.html",context)

def PredicateDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Predicate,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Predicate/list/"
        return HttpResponseRedirect(link)
    else:
         form = PredicateForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "PredicateDelete.html",context)

def SelfConnectedObjectDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(SelfConnectedObject,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/SelfConnectedObject/list/"
        return HttpResponseRedirect(link)
    else:
         form = SelfConnectedObjectForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SelfConnectedObjectDelete.html",context)

def ScientificActivityDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ScientificActivity,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ScientificActivity/list/"
        return HttpResponseRedirect(link)
    else:
         form = ScientificActivityForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ScientificActivityDelete.html",context)

def FormingClassificationSystemDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(FormingClassificationSystem,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/FormingClassificationSystem/list/"
        return HttpResponseRedirect(link)
    else:
         form = FormingClassificationSystemForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "FormingClassificationSystemDelete.html",context)

def HypothesisFormingDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(HypothesisForming,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/HypothesisForming/list/"
        return HttpResponseRedirect(link)
    else:
         form = HypothesisFormingForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "HypothesisFormingDelete.html",context)

def InterpretingResultDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(InterpretingResult,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/InterpretingResult/list/"
        return HttpResponseRedirect(link)
    else:
         form = InterpretingResultForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "InterpretingResultDelete.html",context)

def ProcessProblemAnalysisDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ProcessProblemAnalysis,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ProcessProblemAnalysis/list/"
        return HttpResponseRedirect(link)
    else:
         form = ProcessProblemAnalysisForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ProcessProblemAnalysisDelete.html",context)

def ResultEvaluatingDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ResultEvaluating,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ResultEvaluating/list/"
        return HttpResponseRedirect(link)
    else:
         form = ResultEvaluatingForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ResultEvaluatingDelete.html",context)

def AttributeOfModelDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(AttributeOfModel,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/AttributeOfModel/list/"
        return HttpResponseRedirect(link)
    else:
         form = AttributeOfModelForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AttributeOfModelDelete.html",context)

def AttributeOfVariableDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(AttributeOfVariable,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/AttributeOfVariable/list/"
        return HttpResponseRedirect(link)
    else:
         form = AttributeOfVariableForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AttributeOfVariableDelete.html",context)

def AgentDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Agent,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Agent/list/"
        return HttpResponseRedirect(link)
    else:
         form = AgentForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AgentDelete.html",context)

def CollectionDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Collection,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Collection/list/"
        return HttpResponseRedirect(link)
    else:
         form = CollectionForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "CollectionDelete.html",context)

def EperimentalDesignTaskDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(EperimentalDesignTask,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/EperimentalDesignTask/list/"
        return HttpResponseRedirect(link)
    else:
         form = EperimentalDesignTaskForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "EperimentalDesignTaskDelete.html",context)

def PhysicalDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Physical,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Physical/list/"
        return HttpResponseRedirect(link)
    else:
         form = PhysicalForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "PhysicalDelete.html",context)

def ObjectDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Object,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Object/list/"
        return HttpResponseRedirect(link)
    else:
         form = ObjectForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ObjectDelete.html",context)

def ProcessDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Process,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Process/list/"
        return HttpResponseRedirect(link)
    else:
         form = ProcessForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ProcessDelete.html",context)

def TaskRoleDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(TaskRole,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/TaskRole/list/"
        return HttpResponseRedirect(link)
    else:
         form = TaskRoleForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "TaskRoleDelete.html",context)

def TimeMeasureDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(TimeMeasure,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/TimeMeasure/list/"
        return HttpResponseRedirect(link)
    else:
         form = TimeMeasureForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "TimeMeasureDelete.html",context)

def ActionrelatedRoleDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ActionrelatedRole,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ActionrelatedRole/list/"
        return HttpResponseRedirect(link)
    else:
         form = ActionrelatedRoleForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ActionrelatedRoleDelete.html",context)

def AbductiveHypothesisDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(AbductiveHypothesis,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/AbductiveHypothesis/list/"
        return HttpResponseRedirect(link)
    else:
         form = AbductiveHypothesisForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AbductiveHypothesisDelete.html",context)

def InductiveHypothesisDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(InductiveHypothesis,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/InductiveHypothesis/list/"
        return HttpResponseRedirect(link)
    else:
         form = InductiveHypothesisForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "InductiveHypothesisDelete.html",context)

def AdequacyDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Adequacy,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Adequacy/list/"
        return HttpResponseRedirect(link)
    else:
         form = AdequacyForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AdequacyDelete.html",context)

def AlternativeHypothesisDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(AlternativeHypothesis,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/AlternativeHypothesis/list/"
        return HttpResponseRedirect(link)
    else:
         form = AlternativeHypothesisForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AlternativeHypothesisDelete.html",context)

def NullHypothesisDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(NullHypothesis,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/NullHypothesis/list/"
        return HttpResponseRedirect(link)
    else:
         form = NullHypothesisForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "NullHypothesisDelete.html",context)

def ResearchHypothesisDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ResearchHypothesis,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ResearchHypothesis/list/"
        return HttpResponseRedirect(link)
    else:
         form = ResearchHypothesisForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ResearchHypothesisDelete.html",context)

def ArticleDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Article,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Article/list/"
        return HttpResponseRedirect(link)
    else:
         form = ArticleForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ArticleDelete.html",context)

def TextDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Text,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Text/list/"
        return HttpResponseRedirect(link)
    else:
         form = TextForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "TextDelete.html",context)

def ArtificialLanguageDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ArtificialLanguage,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ArtificialLanguage/list/"
        return HttpResponseRedirect(link)
    else:
         form = ArtificialLanguageForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ArtificialLanguageDelete.html",context)

def LanguageDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Language,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Language/list/"
        return HttpResponseRedirect(link)
    else:
         form = LanguageForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "LanguageDelete.html",context)

def HumanLanguageDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(HumanLanguage,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/HumanLanguage/list/"
        return HttpResponseRedirect(link)
    else:
         form = HumanLanguageForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "HumanLanguageDelete.html",context)

def SentenceDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Sentence,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Sentence/list/"
        return HttpResponseRedirect(link)
    else:
         form = SentenceForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SentenceDelete.html",context)

def AtomicActionDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(AtomicAction,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/AtomicAction/list/"
        return HttpResponseRedirect(link)
    else:
         form = AtomicActionForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AtomicActionDelete.html",context)

def ComplexActionDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ComplexAction,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ComplexAction/list/"
        return HttpResponseRedirect(link)
    else:
         form = ComplexActionForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ComplexActionDelete.html",context)

def AuthorProtocolDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(AuthorProtocol,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/AuthorProtocol/list/"
        return HttpResponseRedirect(link)
    else:
         form = AuthorProtocolForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AuthorProtocolDelete.html",context)

def BookDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Book,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Book/list/"
        return HttpResponseRedirect(link)
    else:
         form = BookForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BookDelete.html",context)

def CalculableVariableDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(CalculableVariable,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/CalculableVariable/list/"
        return HttpResponseRedirect(link)
    else:
         form = CalculableVariableForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "CalculableVariableDelete.html",context)

def CapacityRequirementsDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(CapacityRequirements,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/CapacityRequirements/list/"
        return HttpResponseRedirect(link)
    else:
         form = CapacityRequirementsForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "CapacityRequirementsDelete.html",context)

def EnvironmentalRequirementsDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(EnvironmentalRequirements,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/EnvironmentalRequirements/list/"
        return HttpResponseRedirect(link)
    else:
         form = EnvironmentalRequirementsForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "EnvironmentalRequirementsDelete.html",context)

def FinancialRequirementsDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(FinancialRequirements,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/FinancialRequirements/list/"
        return HttpResponseRedirect(link)
    else:
         form = FinancialRequirementsForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "FinancialRequirementsDelete.html",context)

def ClassificationByDomainDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ClassificationByDomain,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ClassificationByDomain/list/"
        return HttpResponseRedirect(link)
    else:
         form = ClassificationByDomainForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ClassificationByDomainDelete.html",context)

def ClassifyingDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Classifying,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Classifying/list/"
        return HttpResponseRedirect(link)
    else:
         form = ClassifyingForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ClassifyingDelete.html",context)

def DesigningExperimentDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(DesigningExperiment,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/DesigningExperiment/list/"
        return HttpResponseRedirect(link)
    else:
         form = DesigningExperimentForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "DesigningExperimentDelete.html",context)

def CoarseErrorDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(CoarseError,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/CoarseError/list/"
        return HttpResponseRedirect(link)
    else:
         form = CoarseErrorForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "CoarseErrorDelete.html",context)

def MeasurementErrorDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(MeasurementError,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/MeasurementError/list/"
        return HttpResponseRedirect(link)
    else:
         form = MeasurementErrorForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "MeasurementErrorDelete.html",context)

def ComparisonControl_TargetGroupsDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ComparisonControl_TargetGroups,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ComparisonControl_TargetGroups/list/"
        return HttpResponseRedirect(link)
    else:
         form = ComparisonControl_TargetGroupsForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ComparisonControl_TargetGroupsDelete.html",context)

def PairedComparisonDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(PairedComparison,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/PairedComparison/list/"
        return HttpResponseRedirect(link)
    else:
         form = PairedComparisonForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "PairedComparisonDelete.html",context)

def PairedComparisonOfMatchingGroupsDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(PairedComparisonOfMatchingGroups,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/PairedComparisonOfMatchingGroups/list/"
        return HttpResponseRedirect(link)
    else:
         form = PairedComparisonOfMatchingGroupsForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "PairedComparisonOfMatchingGroupsDelete.html",context)

def PairedComparisonOfSingleSampleGroupsDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(PairedComparisonOfSingleSampleGroups,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/PairedComparisonOfSingleSampleGroups/list/"
        return HttpResponseRedirect(link)
    else:
         form = PairedComparisonOfSingleSampleGroupsForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "PairedComparisonOfSingleSampleGroupsDelete.html",context)

def QualityControlDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(QualityControl,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/QualityControl/list/"
        return HttpResponseRedirect(link)
    else:
         form = QualityControlForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "QualityControlDelete.html",context)

def ComplexHypothesisDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ComplexHypothesis,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ComplexHypothesis/list/"
        return HttpResponseRedirect(link)
    else:
         form = ComplexHypothesisForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ComplexHypothesisDelete.html",context)

def SingleHypothesisDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(SingleHypothesis,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/SingleHypothesis/list/"
        return HttpResponseRedirect(link)
    else:
         form = SingleHypothesisForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SingleHypothesisDelete.html",context)

def ComputationalExperimentDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ComputationalExperiment,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ComputationalExperiment/list/"
        return HttpResponseRedirect(link)
    else:
         form = ComputationalExperimentForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ComputationalExperimentDelete.html",context)

def ComputeGoalDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ComputeGoal,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ComputeGoal/list/"
        return HttpResponseRedirect(link)
    else:
         form = ComputeGoalForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ComputeGoalDelete.html",context)

def ConfirmGoalDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ConfirmGoal,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ConfirmGoal/list/"
        return HttpResponseRedirect(link)
    else:
         form = ConfirmGoalForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ConfirmGoalDelete.html",context)

def ExplainGoalDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ExplainGoal,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ExplainGoal/list/"
        return HttpResponseRedirect(link)
    else:
         form = ExplainGoalForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ExplainGoalDelete.html",context)

def InvestigateGoalDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(InvestigateGoal,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/InvestigateGoal/list/"
        return HttpResponseRedirect(link)
    else:
         form = InvestigateGoalForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "InvestigateGoalDelete.html",context)

def ComputerSimulationDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ComputerSimulation,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ComputerSimulation/list/"
        return HttpResponseRedirect(link)
    else:
         form = ComputerSimulationForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ComputerSimulationDelete.html",context)

def ConstantQuantityDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ConstantQuantity,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ConstantQuantity/list/"
        return HttpResponseRedirect(link)
    else:
         form = ConstantQuantityForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ConstantQuantityDelete.html",context)

def ControllabilityDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Controllability,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Controllability/list/"
        return HttpResponseRedirect(link)
    else:
         form = ControllabilityForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ControllabilityDelete.html",context)

def IndependenceDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Independence,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Independence/list/"
        return HttpResponseRedirect(link)
    else:
         form = IndependenceForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "IndependenceDelete.html",context)

def DBReferenceDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(DBReference,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/DBReference/list/"
        return HttpResponseRedirect(link)
    else:
         form = DBReferenceForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "DBReferenceDelete.html",context)

def DDC_Dewey_ClassificationDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(DDC_Dewey_Classification,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/DDC_Dewey_Classification/list/"
        return HttpResponseRedirect(link)
    else:
         form = DDC_Dewey_ClassificationForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "DDC_Dewey_ClassificationDelete.html",context)

def LibraryOfCongressClassificationDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(LibraryOfCongressClassification,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/LibraryOfCongressClassification/list/"
        return HttpResponseRedirect(link)
    else:
         form = LibraryOfCongressClassificationForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "LibraryOfCongressClassificationDelete.html",context)

def NLMClassificationDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(NLMClassification,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/NLMClassification/list/"
        return HttpResponseRedirect(link)
    else:
         form = NLMClassificationForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "NLMClassificationDelete.html",context)

def ResearchCouncilsUKClassificationDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ResearchCouncilsUKClassification,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ResearchCouncilsUKClassification/list/"
        return HttpResponseRedirect(link)
    else:
         form = ResearchCouncilsUKClassificationForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ResearchCouncilsUKClassificationDelete.html",context)

def DataRepresentationStandardDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(DataRepresentationStandard,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/DataRepresentationStandard/list/"
        return HttpResponseRedirect(link)
    else:
         form = DataRepresentationStandardForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "DataRepresentationStandardDelete.html",context)

def DegreeOfModelDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(DegreeOfModel,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/DegreeOfModel/list/"
        return HttpResponseRedirect(link)
    else:
         form = DegreeOfModelForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "DegreeOfModelDelete.html",context)

def DynamismOfModelDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(DynamismOfModel,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/DynamismOfModel/list/"
        return HttpResponseRedirect(link)
    else:
         form = DynamismOfModelForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "DynamismOfModelDelete.html",context)

def DependentVariableDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(DependentVariable,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/DependentVariable/list/"
        return HttpResponseRedirect(link)
    else:
         form = DependentVariableForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "DependentVariableDelete.html",context)

def DeviceDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Device,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Device/list/"
        return HttpResponseRedirect(link)
    else:
         form = DeviceForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "DeviceDelete.html",context)

def DomainActionDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(DomainAction,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/DomainAction/list/"
        return HttpResponseRedirect(link)
    else:
         form = DomainActionForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "DomainActionDelete.html",context)

def DoseResponseDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(DoseResponse,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/DoseResponse/list/"
        return HttpResponseRedirect(link)
    else:
         form = DoseResponseForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "DoseResponseDelete.html",context)

def GeneKnockinDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(GeneKnockin,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/GeneKnockin/list/"
        return HttpResponseRedirect(link)
    else:
         form = GeneKnockinForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "GeneKnockinDelete.html",context)

def GeneKnockoutDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(GeneKnockout,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/GeneKnockout/list/"
        return HttpResponseRedirect(link)
    else:
         form = GeneKnockoutForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "GeneKnockoutDelete.html",context)

def Normal_DiseaseDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Normal_Disease,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Normal_Disease/list/"
        return HttpResponseRedirect(link)
    else:
         form = Normal_DiseaseForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "Normal_DiseaseDelete.html",context)

def TimeCourseDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(TimeCourse,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/TimeCourse/list/"
        return HttpResponseRedirect(link)
    else:
         form = TimeCourseForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "TimeCourseDelete.html",context)

def Treated_UntreatedDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Treated_Untreated,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Treated_Untreated/list/"
        return HttpResponseRedirect(link)
    else:
         form = Treated_UntreatedForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "Treated_UntreatedDelete.html",context)

def DraftStatusDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(DraftStatus,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/DraftStatus/list/"
        return HttpResponseRedirect(link)
    else:
         form = DraftStatusForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "DraftStatusDelete.html",context)

def DuhemEffectDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(DuhemEffect,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/DuhemEffect/list/"
        return HttpResponseRedirect(link)
    else:
         form = DuhemEffectForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "DuhemEffectDelete.html",context)

def ExperimentalDesignEffectDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ExperimentalDesignEffect,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ExperimentalDesignEffect/list/"
        return HttpResponseRedirect(link)
    else:
         form = ExperimentalDesignEffectForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ExperimentalDesignEffectDelete.html",context)

def InstrumentationEffectDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(InstrumentationEffect,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/InstrumentationEffect/list/"
        return HttpResponseRedirect(link)
    else:
         form = InstrumentationEffectForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "InstrumentationEffectDelete.html",context)

def ObjectEffectDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ObjectEffect,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ObjectEffect/list/"
        return HttpResponseRedirect(link)
    else:
         form = ObjectEffectForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ObjectEffectDelete.html",context)

def SubjectEffectDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(SubjectEffect,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/SubjectEffect/list/"
        return HttpResponseRedirect(link)
    else:
         form = SubjectEffectForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SubjectEffectDelete.html",context)

def TimeEffectDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(TimeEffect,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/TimeEffect/list/"
        return HttpResponseRedirect(link)
    else:
         form = TimeEffectForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "TimeEffectDelete.html",context)

def DynamicModelDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(DynamicModel,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/DynamicModel/list/"
        return HttpResponseRedirect(link)
    else:
         form = DynamicModelForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "DynamicModelDelete.html",context)

def StaticModelDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(StaticModel,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/StaticModel/list/"
        return HttpResponseRedirect(link)
    else:
         form = StaticModelForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "StaticModelDelete.html",context)

def ErrorOfConclusionDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ErrorOfConclusion,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ErrorOfConclusion/list/"
        return HttpResponseRedirect(link)
    else:
         form = ErrorOfConclusionForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ErrorOfConclusionDelete.html",context)

def ExperimentalActionsPlanningDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ExperimentalActionsPlanning,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ExperimentalActionsPlanning/list/"
        return HttpResponseRedirect(link)
    else:
         form = ExperimentalActionsPlanningForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ExperimentalActionsPlanningDelete.html",context)

def ExperimentalEquipmentSelectingDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ExperimentalEquipmentSelecting,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ExperimentalEquipmentSelecting/list/"
        return HttpResponseRedirect(link)
    else:
         form = ExperimentalEquipmentSelectingForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ExperimentalEquipmentSelectingDelete.html",context)

def ExperimentalModelDesignDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ExperimentalModelDesign,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ExperimentalModelDesign/list/"
        return HttpResponseRedirect(link)
    else:
         form = ExperimentalModelDesignForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ExperimentalModelDesignDelete.html",context)

def ExperimentalObjectSelectingDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ExperimentalObjectSelecting,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ExperimentalObjectSelecting/list/"
        return HttpResponseRedirect(link)
    else:
         form = ExperimentalObjectSelectingForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ExperimentalObjectSelectingDelete.html",context)

def ExperimentalConclusionDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ExperimentalConclusion,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ExperimentalConclusion/list/"
        return HttpResponseRedirect(link)
    else:
         form = ExperimentalConclusionForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ExperimentalConclusionDelete.html",context)

def ExperimentalProtocolDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ExperimentalProtocol,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ExperimentalProtocol/list/"
        return HttpResponseRedirect(link)
    else:
         form = ExperimentalProtocolForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ExperimentalProtocolDelete.html",context)

def ExperimenteeBiasDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ExperimenteeBias,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ExperimenteeBias/list/"
        return HttpResponseRedirect(link)
    else:
         form = ExperimenteeBiasForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ExperimenteeBiasDelete.html",context)

def HawthorneEffectDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(HawthorneEffect,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/HawthorneEffect/list/"
        return HttpResponseRedirect(link)
    else:
         form = HawthorneEffectForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "HawthorneEffectDelete.html",context)

def MortalityEffectDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(MortalityEffect,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/MortalityEffect/list/"
        return HttpResponseRedirect(link)
    else:
         form = MortalityEffectForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "MortalityEffectDelete.html",context)

def ExperimenterBiasDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ExperimenterBias,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ExperimenterBias/list/"
        return HttpResponseRedirect(link)
    else:
         form = ExperimenterBiasForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ExperimenterBiasDelete.html",context)

def TestingEffectDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(TestingEffect,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/TestingEffect/list/"
        return HttpResponseRedirect(link)
    else:
         form = TestingEffectForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "TestingEffectDelete.html",context)

def ExplicitHypothesisDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ExplicitHypothesis,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ExplicitHypothesis/list/"
        return HttpResponseRedirect(link)
    else:
         form = ExplicitHypothesisForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ExplicitHypothesisDelete.html",context)

def ImplicitHypothesisDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ImplicitHypothesis,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ImplicitHypothesis/list/"
        return HttpResponseRedirect(link)
    else:
         form = ImplicitHypothesisForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ImplicitHypothesisDelete.html",context)

def FactorLevelDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(FactorLevel,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/FactorLevel/list/"
        return HttpResponseRedirect(link)
    else:
         form = FactorLevelForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "FactorLevelDelete.html",context)

def FactorialDesignDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(FactorialDesign,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/FactorialDesign/list/"
        return HttpResponseRedirect(link)
    else:
         form = FactorialDesignForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "FactorialDesignDelete.html",context)

def FalseNegativeDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(FalseNegative,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/FalseNegative/list/"
        return HttpResponseRedirect(link)
    else:
         form = FalseNegativeForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "FalseNegativeDelete.html",context)

def HypothesisAcceptanceMistakeDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(HypothesisAcceptanceMistake,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/HypothesisAcceptanceMistake/list/"
        return HttpResponseRedirect(link)
    else:
         form = HypothesisAcceptanceMistakeForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "HypothesisAcceptanceMistakeDelete.html",context)

def FalsePpositiveDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(FalsePpositive,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/FalsePpositive/list/"
        return HttpResponseRedirect(link)
    else:
         form = FalsePpositiveForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "FalsePpositiveDelete.html",context)

def FaultyComparisonDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(FaultyComparison,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/FaultyComparison/list/"
        return HttpResponseRedirect(link)
    else:
         form = FaultyComparisonForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "FaultyComparisonDelete.html",context)

def FormulaDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Formula,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Formula/list/"
        return HttpResponseRedirect(link)
    else:
         form = FormulaForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "FormulaDelete.html",context)

def GalileanExperimentDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(GalileanExperiment,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/GalileanExperiment/list/"
        return HttpResponseRedirect(link)
    else:
         form = GalileanExperimentForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "GalileanExperimentDelete.html",context)

def HandToolDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(HandTool,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/HandTool/list/"
        return HttpResponseRedirect(link)
    else:
         form = HandToolForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "HandToolDelete.html",context)

def ToolDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Tool,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Tool/list/"
        return HttpResponseRedirect(link)
    else:
         form = ToolForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ToolDelete.html",context)

def HardwareDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Hardware,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Hardware/list/"
        return HttpResponseRedirect(link)
    else:
         form = HardwareForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "HardwareDelete.html",context)

def HistoryEffectDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(HistoryEffect,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/HistoryEffect/list/"
        return HttpResponseRedirect(link)
    else:
         form = HistoryEffectForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "HistoryEffectDelete.html",context)

def MaturationEffectDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(MaturationEffect,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/MaturationEffect/list/"
        return HttpResponseRedirect(link)
    else:
         form = MaturationEffectForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "MaturationEffectDelete.html",context)

def HypothesisdrivenExperimentDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(HypothesisdrivenExperiment,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/HypothesisdrivenExperiment/list/"
        return HttpResponseRedirect(link)
    else:
         form = HypothesisdrivenExperimentForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "HypothesisdrivenExperimentDelete.html",context)

def HypothesisformingExperimentDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(HypothesisformingExperiment,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/HypothesisformingExperiment/list/"
        return HttpResponseRedirect(link)
    else:
         form = HypothesisformingExperimentForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "HypothesisformingExperimentDelete.html",context)

def ImageDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Image,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Image/list/"
        return HttpResponseRedirect(link)
    else:
         form = ImageForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ImageDelete.html",context)

def ImproperSamplingDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ImproperSampling,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ImproperSampling/list/"
        return HttpResponseRedirect(link)
    else:
         form = ImproperSamplingForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ImproperSamplingDelete.html",context)

def IncompleteDataErrorDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(IncompleteDataError,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/IncompleteDataError/list/"
        return HttpResponseRedirect(link)
    else:
         form = IncompleteDataErrorForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "IncompleteDataErrorDelete.html",context)

def IndependentVariableDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(IndependentVariable,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/IndependentVariable/list/"
        return HttpResponseRedirect(link)
    else:
         form = IndependentVariableForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "IndependentVariableDelete.html",context)

def InferableVariableDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(InferableVariable,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/InferableVariable/list/"
        return HttpResponseRedirect(link)
    else:
         form = InferableVariableForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "InferableVariableDelete.html",context)

def InternalStatusDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(InternalStatus,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/InternalStatus/list/"
        return HttpResponseRedirect(link)
    else:
         form = InternalStatusForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "InternalStatusDelete.html",context)

def RestrictedStatusDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(RestrictedStatus,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/RestrictedStatus/list/"
        return HttpResponseRedirect(link)
    else:
         form = RestrictedStatusForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "RestrictedStatusDelete.html",context)

def JournalDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Journal,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Journal/list/"
        return HttpResponseRedirect(link)
    else:
         form = JournalForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "JournalDelete.html",context)

def LinearModelDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(LinearModel,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/LinearModel/list/"
        return HttpResponseRedirect(link)
    else:
         form = LinearModelForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "LinearModelDelete.html",context)

def NonlinearModelDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(NonlinearModel,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/NonlinearModel/list/"
        return HttpResponseRedirect(link)
    else:
         form = NonlinearModelForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "NonlinearModelDelete.html",context)

def LogicalModelRepresentationDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(LogicalModelRepresentation,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/LogicalModelRepresentation/list/"
        return HttpResponseRedirect(link)
    else:
         form = LogicalModelRepresentationForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "LogicalModelRepresentationDelete.html",context)

def MachineDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Machine,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Machine/list/"
        return HttpResponseRedirect(link)
    else:
         form = MachineForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "MachineDelete.html",context)

def MachineToolDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(MachineTool,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/MachineTool/list/"
        return HttpResponseRedirect(link)
    else:
         form = MachineToolForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "MachineToolDelete.html",context)

def MagazineDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Magazine,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Magazine/list/"
        return HttpResponseRedirect(link)
    else:
         form = MagazineForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "MagazineDelete.html",context)

def MaterialsDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Materials,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Materials/list/"
        return HttpResponseRedirect(link)
    else:
         form = MaterialsForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "MaterialsDelete.html",context)

def MathematicalModelRepresentationDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(MathematicalModelRepresentation,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/MathematicalModelRepresentation/list/"
        return HttpResponseRedirect(link)
    else:
         form = MathematicalModelRepresentationForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "MathematicalModelRepresentationDelete.html",context)

def MetabolomicExperimentDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(MetabolomicExperiment,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/MetabolomicExperiment/list/"
        return HttpResponseRedirect(link)
    else:
         form = MetabolomicExperimentForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "MetabolomicExperimentDelete.html",context)

def MethodsComparisonDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(MethodsComparison,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/MethodsComparison/list/"
        return HttpResponseRedirect(link)
    else:
         form = MethodsComparisonForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "MethodsComparisonDelete.html",context)

def SubjectsComparisonDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(SubjectsComparison,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/SubjectsComparison/list/"
        return HttpResponseRedirect(link)
    else:
         form = SubjectsComparisonForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SubjectsComparisonDelete.html",context)

def MicroarrayExperimentDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(MicroarrayExperiment,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/MicroarrayExperiment/list/"
        return HttpResponseRedirect(link)
    else:
         form = MicroarrayExperimentForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "MicroarrayExperimentDelete.html",context)

def MultifactorExperimentDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(MultifactorExperiment,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/MultifactorExperiment/list/"
        return HttpResponseRedirect(link)
    else:
         form = MultifactorExperimentForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "MultifactorExperimentDelete.html",context)

def OnefactorExperimentDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(OnefactorExperiment,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/OnefactorExperiment/list/"
        return HttpResponseRedirect(link)
    else:
         form = OnefactorExperimentForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "OnefactorExperimentDelete.html",context)

def TwofactorExperimentDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(TwofactorExperiment,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/TwofactorExperiment/list/"
        return HttpResponseRedirect(link)
    else:
         form = TwofactorExperimentForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "TwofactorExperimentDelete.html",context)

def NaturalLanguageDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(NaturalLanguage,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/NaturalLanguage/list/"
        return HttpResponseRedirect(link)
    else:
         form = NaturalLanguageForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "NaturalLanguageDelete.html",context)

def NonRestrictedStatusDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(NonRestrictedStatus,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/NonRestrictedStatus/list/"
        return HttpResponseRedirect(link)
    else:
         form = NonRestrictedStatusForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "NonRestrictedStatusDelete.html",context)

def NormalizationStrategyDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(NormalizationStrategy,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/NormalizationStrategy/list/"
        return HttpResponseRedirect(link)
    else:
         form = NormalizationStrategyForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "NormalizationStrategyDelete.html",context)

def ObjectMethodDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ObjectMethod,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ObjectMethod/list/"
        return HttpResponseRedirect(link)
    else:
         form = ObjectMethodForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ObjectMethodDelete.html",context)

def ObjectOfActionDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ObjectOfAction,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ObjectOfAction/list/"
        return HttpResponseRedirect(link)
    else:
         form = ObjectOfActionForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ObjectOfActionDelete.html",context)

def ObservableVariableDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ObservableVariable,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ObservableVariable/list/"
        return HttpResponseRedirect(link)
    else:
         form = ObservableVariableForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ObservableVariableDelete.html",context)

def PaperDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Paper,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Paper/list/"
        return HttpResponseRedirect(link)
    else:
         form = PaperForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "PaperDelete.html",context)

def ParticlePhysicsExperimentDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ParticlePhysicsExperiment,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ParticlePhysicsExperiment/list/"
        return HttpResponseRedirect(link)
    else:
         form = ParticlePhysicsExperimentForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ParticlePhysicsExperimentDelete.html",context)

def PresentingSamplingDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(PresentingSampling,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/PresentingSampling/list/"
        return HttpResponseRedirect(link)
    else:
         form = PresentingSamplingForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "PresentingSamplingDelete.html",context)

def ProceedingsDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Proceedings,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Proceedings/list/"
        return HttpResponseRedirect(link)
    else:
         form = ProceedingsForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ProceedingsDelete.html",context)

def ProgramDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Program,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Program/list/"
        return HttpResponseRedirect(link)
    else:
         form = ProgramForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ProgramDelete.html",context)

def ProteomicExperimentDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(ProteomicExperiment,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/ProteomicExperiment/list/"
        return HttpResponseRedirect(link)
    else:
         form = ProteomicExperimentForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ProteomicExperimentDelete.html",context)

def ProviderDelete(request, pk):
    foreign_keys = dict()
    
    foreign_keys['Has_admin_info'] = 'EXPO_app:AdminInfoProviderList'
    
    foreign_keys['Has_admin_info'] = 'EXPO_app:ThingList'
    
    object_to_delete = get_object_or_404(Provider,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Provider/list/"
        return HttpResponseRedirect(link)
    else:
         form = ProviderForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ProviderDelete.html",context)

def PublicAcademicStatusDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(PublicAcademicStatus,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/PublicAcademicStatus/list/"
        return HttpResponseRedirect(link)
    else:
         form = PublicAcademicStatusForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "PublicAcademicStatusDelete.html",context)

def PublicStatusDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(PublicStatus,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/PublicStatus/list/"
        return HttpResponseRedirect(link)
    else:
         form = PublicStatusForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "PublicStatusDelete.html",context)

def RandomErrorDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(RandomError,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/RandomError/list/"
        return HttpResponseRedirect(link)
    else:
         form = RandomErrorForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "RandomErrorDelete.html",context)

def RandomSamplingDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(RandomSampling,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/RandomSampling/list/"
        return HttpResponseRedirect(link)
    else:
         form = RandomSamplingForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "RandomSamplingDelete.html",context)

def RecommendationSatusDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(RecommendationSatus,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/RecommendationSatus/list/"
        return HttpResponseRedirect(link)
    else:
         form = RecommendationSatusForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "RecommendationSatusDelete.html",context)

def RecordingActionDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(RecordingAction,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/RecordingAction/list/"
        return HttpResponseRedirect(link)
    else:
         form = RecordingActionForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "RecordingActionDelete.html",context)

def RegionDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Region,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Region/list/"
        return HttpResponseRedirect(link)
    else:
         form = RegionForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "RegionDelete.html",context)

def RepresenationalMediumDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(RepresenationalMedium,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/RepresenationalMedium/list/"
        return HttpResponseRedirect(link)
    else:
         form = RepresenationalMediumForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "RepresenationalMediumDelete.html",context)

def SUMOSynonymDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(SUMOSynonym,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/SUMOSynonym/list/"
        return HttpResponseRedirect(link)
    else:
         form = SUMOSynonymForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SUMOSynonymDelete.html",context)

def SampleFormingDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(SampleForming,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/SampleForming/list/"
        return HttpResponseRedirect(link)
    else:
         form = SampleFormingForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SampleFormingDelete.html",context)

def SequentialDesignDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(SequentialDesign,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/SequentialDesign/list/"
        return HttpResponseRedirect(link)
    else:
         form = SequentialDesignForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SequentialDesignDelete.html",context)

def SoftwareDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Software,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Software/list/"
        return HttpResponseRedirect(link)
    else:
         form = SoftwareForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SoftwareDelete.html",context)

def SoundDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Sound,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Sound/list/"
        return HttpResponseRedirect(link)
    else:
         form = SoundForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SoundDelete.html",context)

def SplitSamplesDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(SplitSamples,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/SplitSamples/list/"
        return HttpResponseRedirect(link)
    else:
         form = SplitSamplesForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SplitSamplesDelete.html",context)

def SymmetricalMatchingDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(SymmetricalMatching,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/SymmetricalMatching/list/"
        return HttpResponseRedirect(link)
    else:
         form = SymmetricalMatchingForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SymmetricalMatchingDelete.html",context)

def StatisticalErrorDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(StatisticalError,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/StatisticalError/list/"
        return HttpResponseRedirect(link)
    else:
         form = StatisticalErrorForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "StatisticalErrorDelete.html",context)

def StratifiedSamplingDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(StratifiedSampling,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/StratifiedSampling/list/"
        return HttpResponseRedirect(link)
    else:
         form = StratifiedSamplingForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "StratifiedSamplingDelete.html",context)

def SubjectOfActionDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(SubjectOfAction,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/SubjectOfAction/list/"
        return HttpResponseRedirect(link)
    else:
         form = SubjectOfActionForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SubjectOfActionDelete.html",context)

def SubjectOfExperimentDelete(request, pk):
    foreign_keys = dict()
    
    foreign_keys['Has_admin_info'] = 'EXPO_app:AdminInfoSubjectOfExperimentList'
    
    foreign_keys['Has_admin_info'] = 'EXPO_app:ThingList'
    
    object_to_delete = get_object_or_404(SubjectOfExperiment,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/SubjectOfExperiment/list/"
        return HttpResponseRedirect(link)
    else:
         form = SubjectOfExperimentForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SubjectOfExperimentDelete.html",context)

def SubmitterDelete(request, pk):
    foreign_keys = dict()
    
    foreign_keys['Has_admin_info'] = 'EXPO_app:AdminInfoSubmitterList'
    
    foreign_keys['Has_admin_info'] = 'EXPO_app:ThingList'
    
    object_to_delete = get_object_or_404(Submitter,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Submitter/list/"
        return HttpResponseRedirect(link)
    else:
         form = SubmitterForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SubmitterDelete.html",context)

def SummaryDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(Summary,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/Summary/list/"
        return HttpResponseRedirect(link)
    else:
         form = SummaryForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SummaryDelete.html",context)

def SystematicErrorDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(SystematicError,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/SystematicError/list/"
        return HttpResponseRedirect(link)
    else:
         form = SystematicErrorForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SystematicErrorDelete.html",context)

def TechnicalReportDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(TechnicalReport,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/TechnicalReport/list/"
        return HttpResponseRedirect(link)
    else:
         form = TechnicalReportForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "TechnicalReportDelete.html",context)

def TimeDurationDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(TimeDuration,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/TimeDuration/list/"
        return HttpResponseRedirect(link)
    else:
         form = TimeDurationForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "TimeDurationDelete.html",context)

def TimeIntervalDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(TimeInterval,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/TimeInterval/list/"
        return HttpResponseRedirect(link)
    else:
         form = TimeIntervalForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "TimeIntervalDelete.html",context)

def URLDelete(request, pk):
    foreign_keys = dict()
    
    object_to_delete = get_object_or_404(URL,pk=pk)
    if request.method == 'POST':
        object_to_delete.delete()
        link = "/EXPO_app/URL/list/"
        return HttpResponseRedirect(link)
    else:
         form = URLForm(instance =  object_to_delete)
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "URLDelete.html",context)
