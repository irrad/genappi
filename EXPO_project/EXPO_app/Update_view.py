from owlready2 import *
from django.db import models
from django.template.loader import render_to_string
from .models import *
from .forms import *
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseNotFound
from django.template import loader
from django.core.urlresolvers import reverse
import datetime
from .forms import *
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.core.files.storage import FileSystemStorage
from django.utils.safestring import mark_safe
from django.conf import settings

onto_path.append(settings.BASE_DIR)
onto = get_ontology("EXPO.owl").load()
ontoclasses = onto.classes()
namespaces = dict() 
for clas in ontoclasses:
    namespaces[clas.name] = clas.namespace


def ThingUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Thing"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Thing, pk=pk)
    if request.method == 'POST':
        form = ThingForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Thing(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/Thing/list/"
            return HttpResponseRedirect(link)
    else:
        form = ThingForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ThingUpdate.html",context)

def RepresentationFormUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["RepresentationForm"]
    foreign_keys = dict()
    
    foreign_keys['Has_'] = 'EXPO_app:ArtifactList'
    
    foreign_keys['Has_expression'] = 'EXPO_app:LinguisticExpressionList'
    
    updated_instance =  get_object_or_404(RepresentationForm, pk=pk)
    if request.method == 'POST':
        form = RepresentationForm1(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.RepresentationForm(form.cleaned_data['name'])
            
            onto.save()

            
            fkstr = 'Has_'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Artifact(fkinstanceinput[0])
            onto.save()
            onto_instance.has_.append(fkinstance) 
            
            fkstr = 'Has_expression'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.LinguisticExpression(fkinstanceinput[0])
            onto.save()
            onto_instance.has_expression.append(fkinstance) 
            
            onto.save()


            link = "/EXPO_app/RepresentationForm/list/"
            return HttpResponseRedirect(link)
    else:
        form = RepresentationForm1(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "RepresentationFormUpdate.html",context)

def ArtifactUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Artifact"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Artifact, pk=pk)
    if request.method == 'POST':
        form = ArtifactForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Artifact(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/Artifact/list/"
            return HttpResponseRedirect(link)
    else:
        form = ArtifactForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ArtifactUpdate.html",context)

def SynonymousExternalConceptUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["SynonymousExternalConcept"]
    foreign_keys = dict()
    
    foreign_keys['Has_EXPO_concept'] = 'EXPO_app:NameList'
    
    foreign_keys['Has_synonymousExternalConcept'] = 'EXPO_app:NameList'
    
    foreign_keys['Has_EXPO_concept'] = 'EXPO_app:ThingList'
    
    updated_instance =  get_object_or_404(SynonymousExternalConcept, pk=pk)
    if request.method == 'POST':
        form = SynonymousExternalConceptForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.SynonymousExternalConcept(form.cleaned_data['name'])
            
            onto.save()

            
            fkstr = 'Has_EXPO_concept'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Name(fkinstanceinput[0])
            onto.save()
            onto_instance.has_EXPO_concept.append(fkinstance) 
            
            fkstr = 'Has_synonymousExternalConcept'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Name(fkinstanceinput[0])
            onto.save()
            onto_instance.has_synonymousExternalConcept.append(fkinstance) 
            
            fkstr = 'Has_EXPO_concept'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Thing(fkinstanceinput[0])
            onto.save()
            onto_instance.has_EXPO_concept.append(fkinstance) 
            
            onto.save()


            link = "/EXPO_app/SynonymousExternalConcept/list/"
            return HttpResponseRedirect(link)
    else:
        form = SynonymousExternalConceptForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SynonymousExternalConceptUpdate.html",context)

def NameUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Name"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Name, pk=pk)
    if request.method == 'POST':
        form = NameForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Name(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/Name/list/"
            return HttpResponseRedirect(link)
    else:
        form = NameForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "NameUpdate.html",context)

def ScientificExperimentUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ScientificExperiment"]
    foreign_keys = dict()
    
    foreign_keys['Has_ExperimentalDesign'] = 'EXPO_app:ExperimentalDesignList'
    
    foreign_keys['Has_admin_info'] = 'EXPO_app:AdminInfoExperimentList'
    
    foreign_keys['Has_classification'] = 'EXPO_app:ClassificationOfExperimentsList'
    
    foreign_keys['Has_goal'] = 'EXPO_app:ExperimentalGoalList'
    
    updated_instance =  get_object_or_404(ScientificExperiment, pk=pk)
    if request.method == 'POST':
        form = ScientificExperimentForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ScientificExperiment(form.cleaned_data['name'])
            
            onto.save()

            
            fkstr = 'Has_ExperimentalDesign'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.ExperimentalDesign(fkinstanceinput[0])
            onto.save()
            onto_instance.has_ExperimentalDesign.append(fkinstance) 
            
            fkstr = 'Has_admin_info'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.AdminInfoExperiment(fkinstanceinput[0])
            onto.save()
            onto_instance.has_admin_info.append(fkinstance) 
            
            fkstr = 'Has_classification'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.ClassificationOfExperiments(fkinstanceinput[0])
            onto.save()
            onto_instance.has_classification.append(fkinstance) 
            
            fkstr = 'Has_goal'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.ExperimentalGoal(fkinstanceinput[0])
            onto.save()
            onto_instance.has_goal.append(fkinstance) 
            
            onto.save()


            link = "/EXPO_app/ScientificExperiment/list/"
            return HttpResponseRedirect(link)
    else:
        form = ScientificExperimentForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ScientificExperimentUpdate.html",context)

def ExperimentalDesignUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ExperimentalDesign"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ExperimentalDesign, pk=pk)
    if request.method == 'POST':
        form = ExperimentalDesignForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ExperimentalDesign(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/ExperimentalDesign/list/"
            return HttpResponseRedirect(link)
    else:
        form = ExperimentalDesignForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ExperimentalDesignUpdate.html",context)

def AdminInfoExperimentUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["AdminInfoExperiment"]
    foreign_keys = dict()
    
    foreign_keys['Has_ID'] = 'EXPO_app:ThingList'
    
    foreign_keys['Has_ID'] = 'EXPO_app:IDExperimentList'
    
    foreign_keys['Has_author'] = 'EXPO_app:AuthorList'
    
    foreign_keys['Has_organization'] = 'EXPO_app:OrganizationList'
    
    foreign_keys['Has_reference'] = 'EXPO_app:BiblioReferenceList'
    
    foreign_keys['Has_status'] = 'EXPO_app:StatusExperimentalDocumentList'
    
    foreign_keys['Has_title'] = 'EXPO_app:TitleExperimentList'
    
    updated_instance =  get_object_or_404(AdminInfoExperiment, pk=pk)
    if request.method == 'POST':
        form = AdminInfoExperimentForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.AdminInfoExperiment(form.cleaned_data['name'])
            
            onto.save()

            
            fkstr = 'Has_ID'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Thing(fkinstanceinput[0])
            onto.save()
            onto_instance.has_ID.append(fkinstance) 
            
            fkstr = 'Has_ID'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.IDExperiment(fkinstanceinput[0])
            onto.save()
            onto_instance.has_ID.append(fkinstance) 
            
            fkstr = 'Has_author'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Author(fkinstanceinput[0])
            onto.save()
            onto_instance.has_author.append(fkinstance) 
            
            fkstr = 'Has_organization'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Organization(fkinstanceinput[0])
            onto.save()
            onto_instance.has_organization.append(fkinstance) 
            
            fkstr = 'Has_reference'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.BiblioReference(fkinstanceinput[0])
            onto.save()
            onto_instance.has_reference.append(fkinstance) 
            
            fkstr = 'Has_status'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.StatusExperimentalDocument(fkinstanceinput[0])
            onto.save()
            onto_instance.has_status.append(fkinstance) 
            
            fkstr = 'Has_title'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.TitleExperiment(fkinstanceinput[0])
            onto.save()
            onto_instance.has_title.append(fkinstance) 
            
            onto.save()


            link = "/EXPO_app/AdminInfoExperiment/list/"
            return HttpResponseRedirect(link)
    else:
        form = AdminInfoExperimentForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AdminInfoExperimentUpdate.html",context)

def IDExperimentUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["IDExperiment"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(IDExperiment, pk=pk)
    if request.method == 'POST':
        form = IDExperimentForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.IDExperiment(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/IDExperiment/list/"
            return HttpResponseRedirect(link)
    else:
        form = IDExperimentForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "IDExperimentUpdate.html",context)

def DomainOfExperimentUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["DomainOfExperiment"]
    foreign_keys = dict()
    
    foreign_keys['Has_SUMO_sinonym'] = 'EXPO_app:SynonymousExternalConceptList'
    
    foreign_keys['Has_classification'] = 'EXPO_app:ClassificationOfDomainsList'
    
    foreign_keys['Has_model'] = 'EXPO_app:DomainModelList'
    
    updated_instance =  get_object_or_404(DomainOfExperiment, pk=pk)
    if request.method == 'POST':
        form = DomainOfExperimentForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.DomainOfExperiment(form.cleaned_data['name'])
            
            onto.save()

            
            fkstr = 'Has_SUMO_sinonym'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.SynonymousExternalConcept(fkinstanceinput[0])
            onto.save()
            onto_instance.has_SUMO_sinonym.append(fkinstance) 
            
            fkstr = 'Has_classification'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.ClassificationOfDomains(fkinstanceinput[0])
            onto.save()
            onto_instance.has_classification.append(fkinstance) 
            
            fkstr = 'Has_model'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.DomainModel(fkinstanceinput[0])
            onto.save()
            onto_instance.has_model.append(fkinstance) 
            
            onto.save()


            link = "/EXPO_app/DomainOfExperiment/list/"
            return HttpResponseRedirect(link)
    else:
        form = DomainOfExperimentForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "DomainOfExperimentUpdate.html",context)

def ExperimentalObservationUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ExperimentalObservation"]
    foreign_keys = dict()
    
    foreign_keys['Has__interpretation'] = 'EXPO_app:ResultsInterpretationList'
    
    foreign_keys['Has_error'] = 'EXPO_app:ObservationalErrorList'
    
    foreign_keys['Has_observation_characteristic'] = 'EXPO_app:StatisticsCharacteristicList'
    
    updated_instance =  get_object_or_404(ExperimentalObservation, pk=pk)
    if request.method == 'POST':
        form = ExperimentalObservationForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ExperimentalObservation(form.cleaned_data['name'])
            
            onto.save()

            
            fkstr = 'Has__interpretation'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.ResultsInterpretation(fkinstanceinput[0])
            onto.save()
            onto_instance.has__interpretation.append(fkinstance) 
            
            fkstr = 'Has_error'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.ObservationalError(fkinstanceinput[0])
            onto.save()
            onto_instance.has_error.append(fkinstance) 
            
            fkstr = 'Has_observation_characteristic'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.StatisticsCharacteristic(fkinstanceinput[0])
            onto.save()
            onto_instance.has_observation_characteristic.append(fkinstance) 
            
            onto.save()


            link = "/EXPO_app/ExperimentalObservation/list/"
            return HttpResponseRedirect(link)
    else:
        form = ExperimentalObservationForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ExperimentalObservationUpdate.html",context)

def ResultsInterpretationUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ResultsInterpretation"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ResultsInterpretation, pk=pk)
    if request.method == 'POST':
        form = ResultsInterpretationForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ResultsInterpretation(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/ResultsInterpretation/list/"
            return HttpResponseRedirect(link)
    else:
        form = ResultsInterpretationForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ResultsInterpretationUpdate.html",context)

def ExperimentalActionUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ExperimentalAction"]
    foreign_keys = dict()
    
    foreign_keys['Has__name'] = 'EXPO_app:ActionNameList'
    
    foreign_keys['Has_action_complexity'] = 'EXPO_app:ActionComplexityList'
    
    foreign_keys['Has_goal'] = 'EXPO_app:ActionGoalList'
    
    foreign_keys['Has_instrument'] = 'EXPO_app:ExperimentalEquipmentList'
    
    foreign_keys['Has_method'] = 'EXPO_app:ExperimentalMethodList'
    
    foreign_keys['Has_timing'] = 'EXPO_app:TimePointList'
    
    foreign_keys['Has_action_complexity'] = 'EXPO_app:ThingList'
    
    updated_instance =  get_object_or_404(ExperimentalAction, pk=pk)
    if request.method == 'POST':
        form = ExperimentalActionForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ExperimentalAction(form.cleaned_data['name'])
            
            onto.save()

            
            fkstr = 'Has__name'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.ActionName(fkinstanceinput[0])
            onto.save()
            onto_instance.has__name.append(fkinstance) 
            
            fkstr = 'Has_action_complexity'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.ActionComplexity(fkinstanceinput[0])
            onto.save()
            onto_instance.has_action_complexity.append(fkinstance) 
            
            fkstr = 'Has_goal'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.ActionGoal(fkinstanceinput[0])
            onto.save()
            onto_instance.has_goal.append(fkinstance) 
            
            fkstr = 'Has_instrument'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.ExperimentalEquipment(fkinstanceinput[0])
            onto.save()
            onto_instance.has_instrument.append(fkinstance) 
            
            fkstr = 'Has_method'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.ExperimentalMethod(fkinstanceinput[0])
            onto.save()
            onto_instance.has_method.append(fkinstance) 
            
            fkstr = 'Has_timing'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.TimePoint(fkinstanceinput[0])
            onto.save()
            onto_instance.has_timing.append(fkinstance) 
            
            fkstr = 'Has_action_complexity'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Thing(fkinstanceinput[0])
            onto.save()
            onto_instance.has_action_complexity.append(fkinstance) 
            
            onto.save()


            link = "/EXPO_app/ExperimentalAction/list/"
            return HttpResponseRedirect(link)
    else:
        form = ExperimentalActionForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ExperimentalActionUpdate.html",context)

def ActionNameUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ActionName"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ActionName, pk=pk)
    if request.method == 'POST':
        form = ActionNameForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ActionName(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/ActionName/list/"
            return HttpResponseRedirect(link)
    else:
        form = ActionNameForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ActionNameUpdate.html",context)

def ActionComplexityUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ActionComplexity"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ActionComplexity, pk=pk)
    if request.method == 'POST':
        form = ActionComplexityForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ActionComplexity(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/ActionComplexity/list/"
            return HttpResponseRedirect(link)
    else:
        form = ActionComplexityForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ActionComplexityUpdate.html",context)

def HumanUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Human"]
    foreign_keys = dict()
    
    foreign_keys['Has_organization'] = 'EXPO_app:OrganizationList'
    
    foreign_keys['Has_telephone_number'] = 'EXPO_app:NumberList'
    
    foreign_keys['Has_title'] = 'EXPO_app:TitleList'
    
    foreign_keys['Has_name'] = 'EXPO_app:ThingList'
    
    updated_instance =  get_object_or_404(Human, pk=pk)
    if request.method == 'POST':
        form = HumanForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Human(form.cleaned_data['name'])
            
            onto.save()

            
            fkstr = 'Has_organization'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Organization(fkinstanceinput[0])
            onto.save()
            onto_instance.has_organization.append(fkinstance) 
            
            fkstr = 'Has_telephone_number'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Number(fkinstanceinput[0])
            onto.save()
            onto_instance.has_telephone_number.append(fkinstance) 
            
            fkstr = 'Has_title'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Title(fkinstanceinput[0])
            onto.save()
            onto_instance.has_title.append(fkinstance) 
            
            fkstr = 'Has_name'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Thing(fkinstanceinput[0])
            onto.save()
            onto_instance.has_name.append(fkinstance) 
            
            onto.save()


            link = "/EXPO_app/Human/list/"
            return HttpResponseRedirect(link)
    else:
        form = HumanForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "HumanUpdate.html",context)

def OrganizationUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Organization"]
    foreign_keys = dict()
    
    foreign_keys['Has_fax_number'] = 'EXPO_app:NumberList'
    
    foreign_keys['Has_name'] = 'EXPO_app:NameList'
    
    foreign_keys['Has_telephone_number'] = 'EXPO_app:NumberList'
    
    foreign_keys['Has_name'] = 'EXPO_app:EntityList'
    
    updated_instance =  get_object_or_404(Organization, pk=pk)
    if request.method == 'POST':
        form = OrganizationForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Organization(form.cleaned_data['name'])
            
            onto.save()

            
            fkstr = 'Has_fax_number'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Number(fkinstanceinput[0])
            onto.save()
            onto_instance.has_fax_number.append(fkinstance) 
            
            fkstr = 'Has_name'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Name(fkinstanceinput[0])
            onto.save()
            onto_instance.has_name.append(fkinstance) 
            
            fkstr = 'Has_telephone_number'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Number(fkinstanceinput[0])
            onto.save()
            onto_instance.has_telephone_number.append(fkinstance) 
            
            fkstr = 'Has_name'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Entity(fkinstanceinput[0])
            onto.save()
            onto_instance.has_name.append(fkinstance) 
            
            onto.save()


            link = "/EXPO_app/Organization/list/"
            return HttpResponseRedirect(link)
    else:
        form = OrganizationForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "OrganizationUpdate.html",context)

def AdminInfoAuthorUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["AdminInfoAuthor"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(AdminInfoAuthor, pk=pk)
    if request.method == 'POST':
        form = AdminInfoAuthorForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.AdminInfoAuthor(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/AdminInfoAuthor/list/"
            return HttpResponseRedirect(link)
    else:
        form = AdminInfoAuthorForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AdminInfoAuthorUpdate.html",context)

def AdminInfoObjectOfExperimentUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["AdminInfoObjectOfExperiment"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(AdminInfoObjectOfExperiment, pk=pk)
    if request.method == 'POST':
        form = AdminInfoObjectOfExperimentForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.AdminInfoObjectOfExperiment(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/AdminInfoObjectOfExperiment/list/"
            return HttpResponseRedirect(link)
    else:
        form = AdminInfoObjectOfExperimentForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AdminInfoObjectOfExperimentUpdate.html",context)

def AdminInfoProviderUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["AdminInfoProvider"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(AdminInfoProvider, pk=pk)
    if request.method == 'POST':
        form = AdminInfoProviderForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.AdminInfoProvider(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/AdminInfoProvider/list/"
            return HttpResponseRedirect(link)
    else:
        form = AdminInfoProviderForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AdminInfoProviderUpdate.html",context)

def AdminInfoSubjectOfExperimentUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["AdminInfoSubjectOfExperiment"]
    foreign_keys = dict()
    
    foreign_keys['Has_name'] = 'EXPO_app:NameList'
    
    foreign_keys['Has_organization'] = 'EXPO_app:OrganizationList'
    
    foreign_keys['Has_qualification'] = 'EXPO_app:SubjectQualificationList'
    
    updated_instance =  get_object_or_404(AdminInfoSubjectOfExperiment, pk=pk)
    if request.method == 'POST':
        form = AdminInfoSubjectOfExperimentForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.AdminInfoSubjectOfExperiment(form.cleaned_data['name'])
            
            onto.save()

            
            fkstr = 'Has_name'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Name(fkinstanceinput[0])
            onto.save()
            onto_instance.has_name.append(fkinstance) 
            
            fkstr = 'Has_organization'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Organization(fkinstanceinput[0])
            onto.save()
            onto_instance.has_organization.append(fkinstance) 
            
            fkstr = 'Has_qualification'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.SubjectQualification(fkinstanceinput[0])
            onto.save()
            onto_instance.has_qualification.append(fkinstance) 
            
            onto.save()


            link = "/EXPO_app/AdminInfoSubjectOfExperiment/list/"
            return HttpResponseRedirect(link)
    else:
        form = AdminInfoSubjectOfExperimentForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AdminInfoSubjectOfExperimentUpdate.html",context)

def AdminInfoSubmitterUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["AdminInfoSubmitter"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(AdminInfoSubmitter, pk=pk)
    if request.method == 'POST':
        form = AdminInfoSubmitterForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.AdminInfoSubmitter(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/AdminInfoSubmitter/list/"
            return HttpResponseRedirect(link)
    else:
        form = AdminInfoSubmitterForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AdminInfoSubmitterUpdate.html",context)

def AdminInfoUserUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["AdminInfoUser"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(AdminInfoUser, pk=pk)
    if request.method == 'POST':
        form = AdminInfoUserForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.AdminInfoUser(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/AdminInfoUser/list/"
            return HttpResponseRedirect(link)
    else:
        form = AdminInfoUserForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AdminInfoUserUpdate.html",context)

def ExperimentalMethodUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ExperimentalMethod"]
    foreign_keys = dict()
    
    foreign_keys['Has_applicability'] = 'EXPO_app:MethodApplicabilityConditionList'
    
    foreign_keys['Has_goal'] = 'EXPO_app:MethodGoalList'
    
    foreign_keys['Has_plan'] = 'EXPO_app:PlanOfExperimentalActionsList'
    
    foreign_keys['Has_subject'] = 'EXPO_app:SubjectMethodList'
    
    updated_instance =  get_object_or_404(ExperimentalMethod, pk=pk)
    if request.method == 'POST':
        form = ExperimentalMethodForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ExperimentalMethod(form.cleaned_data['name'])
            
            onto.save()

            
            fkstr = 'Has_applicability'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.MethodApplicabilityCondition(fkinstanceinput[0])
            onto.save()
            onto_instance.has_applicability.append(fkinstance) 
            
            fkstr = 'Has_goal'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.MethodGoal(fkinstanceinput[0])
            onto.save()
            onto_instance.has_goal.append(fkinstance) 
            
            fkstr = 'Has_plan'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.PlanOfExperimentalActions(fkinstanceinput[0])
            onto.save()
            onto_instance.has_plan.append(fkinstance) 
            
            fkstr = 'Has_subject'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.SubjectMethod(fkinstanceinput[0])
            onto.save()
            onto_instance.has_subject.append(fkinstance) 
            
            onto.save()


            link = "/EXPO_app/ExperimentalMethod/list/"
            return HttpResponseRedirect(link)
    else:
        form = ExperimentalMethodForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ExperimentalMethodUpdate.html",context)

def MethodApplicabilityConditionUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["MethodApplicabilityCondition"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(MethodApplicabilityCondition, pk=pk)
    if request.method == 'POST':
        form = MethodApplicabilityConditionForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.MethodApplicabilityCondition(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/MethodApplicabilityCondition/list/"
            return HttpResponseRedirect(link)
    else:
        form = MethodApplicabilityConditionForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "MethodApplicabilityConditionUpdate.html",context)

def StandardOperatingProcedureUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["StandardOperatingProcedure"]
    foreign_keys = dict()
    
    foreign_keys['Has_author'] = 'EXPO_app:AuthorSOPList'
    
    foreign_keys['Has_latest_modification'] = 'EXPO_app:TimePointList'
    
    foreign_keys['Has_procedure'] = 'EXPO_app:ProcedureExecuteExperimentList'
    
    foreign_keys['Has_representation'] = 'EXPO_app:BiblioReferenceList'
    
    foreign_keys['Has_latest_modification'] = 'EXPO_app:ThingList'
    
    updated_instance =  get_object_or_404(StandardOperatingProcedure, pk=pk)
    if request.method == 'POST':
        form = StandardOperatingProcedureForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.StandardOperatingProcedure(form.cleaned_data['name'])
            
            onto.save()

            
            fkstr = 'Has_author'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.AuthorSOP(fkinstanceinput[0])
            onto.save()
            onto_instance.has_author.append(fkinstance) 
            
            fkstr = 'Has_latest_modification'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.TimePoint(fkinstanceinput[0])
            onto.save()
            onto_instance.has_latest_modification.append(fkinstance) 
            
            fkstr = 'Has_procedure'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.ProcedureExecuteExperiment(fkinstanceinput[0])
            onto.save()
            onto_instance.has_procedure.append(fkinstance) 
            
            fkstr = 'Has_representation'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.BiblioReference(fkinstanceinput[0])
            onto.save()
            onto_instance.has_representation.append(fkinstance) 
            
            fkstr = 'Has_latest_modification'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Thing(fkinstanceinput[0])
            onto.save()
            onto_instance.has_latest_modification.append(fkinstance) 
            
            onto.save()


            link = "/EXPO_app/StandardOperatingProcedure/list/"
            return HttpResponseRedirect(link)
    else:
        form = StandardOperatingProcedureForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "StandardOperatingProcedureUpdate.html",context)

def AuthorUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Author"]
    foreign_keys = dict()
    
    foreign_keys['Has_admin_info'] = 'EXPO_app:ThingList'
    
    foreign_keys['Has_admin_info'] = 'EXPO_app:AdminInfoAuthorList'
    
    updated_instance =  get_object_or_404(Author, pk=pk)
    if request.method == 'POST':
        form = AuthorForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Author(form.cleaned_data['name'])
            
            onto.save()

            
            fkstr = 'Has_admin_info'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Thing(fkinstanceinput[0])
            onto.save()
            onto_instance.has_admin_info.append(fkinstance) 
            
            fkstr = 'Has_admin_info'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.AdminInfoAuthor(fkinstanceinput[0])
            onto.save()
            onto_instance.has_admin_info.append(fkinstance) 
            
            onto.save()


            link = "/EXPO_app/Author/list/"
            return HttpResponseRedirect(link)
    else:
        form = AuthorForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AuthorUpdate.html",context)

def ClassificationOfDomainsUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ClassificationOfDomains"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ClassificationOfDomains, pk=pk)
    if request.method == 'POST':
        form = ClassificationOfDomainsForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ClassificationOfDomains(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/ClassificationOfDomains/list/"
            return HttpResponseRedirect(link)
    else:
        form = ClassificationOfDomainsForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ClassificationOfDomainsUpdate.html",context)

def ClassificationOfExperimentsUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ClassificationOfExperiments"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ClassificationOfExperiments, pk=pk)
    if request.method == 'POST':
        form = ClassificationOfExperimentsForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ClassificationOfExperiments(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/ClassificationOfExperiments/list/"
            return HttpResponseRedirect(link)
    else:
        form = ClassificationOfExperimentsForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ClassificationOfExperimentsUpdate.html",context)

def ExperimentalHypothesisUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ExperimentalHypothesis"]
    foreign_keys = dict()
    
    foreign_keys['Has_explicitness'] = 'EXPO_app:ThingList'
    
    foreign_keys['Has_complexity'] = 'EXPO_app:HypothesisComplexityList'
    
    foreign_keys['Has_explicitness'] = 'EXPO_app:HypothesisExplicitnessList'
    
    foreign_keys['Has_generality'] = 'EXPO_app:GeneralityOfHypothesisList'
    
    foreign_keys['Has_hypothesis_representation'] = 'EXPO_app:HypothesisRepresentationList'
    
    foreign_keys['Has_target_variable'] = 'EXPO_app:TargetVariableList'
    
    foreign_keys['Has_complexity'] = 'EXPO_app:ThingList'
    
    foreign_keys['Has_generality'] = 'EXPO_app:ThingList'
    
    updated_instance =  get_object_or_404(ExperimentalHypothesis, pk=pk)
    if request.method == 'POST':
        form = ExperimentalHypothesisForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ExperimentalHypothesis(form.cleaned_data['name'])
            
            onto.save()

            
            fkstr = 'Has_explicitness'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Thing(fkinstanceinput[0])
            onto.save()
            onto_instance.has_explicitness.append(fkinstance) 
            
            fkstr = 'Has_complexity'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.HypothesisComplexity(fkinstanceinput[0])
            onto.save()
            onto_instance.has_complexity.append(fkinstance) 
            
            fkstr = 'Has_explicitness'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.HypothesisExplicitness(fkinstanceinput[0])
            onto.save()
            onto_instance.has_explicitness.append(fkinstance) 
            
            fkstr = 'Has_generality'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.GeneralityOfHypothesis(fkinstanceinput[0])
            onto.save()
            onto_instance.has_generality.append(fkinstance) 
            
            fkstr = 'Has_hypothesis_representation'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.HypothesisRepresentation(fkinstanceinput[0])
            onto.save()
            onto_instance.has_hypothesis_representation.append(fkinstance) 
            
            fkstr = 'Has_target_variable'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.TargetVariable(fkinstanceinput[0])
            onto.save()
            onto_instance.has_target_variable.append(fkinstance) 
            
            fkstr = 'Has_complexity'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Thing(fkinstanceinput[0])
            onto.save()
            onto_instance.has_complexity.append(fkinstance) 
            
            fkstr = 'Has_generality'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Thing(fkinstanceinput[0])
            onto.save()
            onto_instance.has_generality.append(fkinstance) 
            
            onto.save()


            link = "/EXPO_app/ExperimentalHypothesis/list/"
            return HttpResponseRedirect(link)
    else:
        form = ExperimentalHypothesisForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ExperimentalHypothesisUpdate.html",context)

def HypothesisComplexityUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["HypothesisComplexity"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(HypothesisComplexity, pk=pk)
    if request.method == 'POST':
        form = HypothesisComplexityForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.HypothesisComplexity(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/HypothesisComplexity/list/"
            return HttpResponseRedirect(link)
    else:
        form = HypothesisComplexityForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "HypothesisComplexityUpdate.html",context)

def RepresentationUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Representation"]
    foreign_keys = dict()
    
    foreign_keys['Has_content'] = 'EXPO_app:PropositionList'
    
    foreign_keys['Has_representation_style'] = 'EXPO_app:RepresentationFormList'
    
    foreign_keys['Has_content'] = 'EXPO_app:ThingList'
    
    foreign_keys['Has_representation_style'] = 'EXPO_app:ThingList'
    
    updated_instance =  get_object_or_404(Representation, pk=pk)
    if request.method == 'POST':
        form = RepresentationForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Representation(form.cleaned_data['name'])
            
            onto.save()

            
            fkstr = 'Has_content'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Proposition(fkinstanceinput[0])
            onto.save()
            onto_instance.has_content.append(fkinstance) 
            
            fkstr = 'Has_representation_style'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.RepresentationForm(fkinstanceinput[0])
            onto.save()
            onto_instance.has_representation_style.append(fkinstance) 
            
            fkstr = 'Has_content'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Thing(fkinstanceinput[0])
            onto.save()
            onto_instance.has_content.append(fkinstance) 
            
            fkstr = 'Has_representation_style'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Thing(fkinstanceinput[0])
            onto.save()
            onto_instance.has_representation_style.append(fkinstance) 
            
            onto.save()


            link = "/EXPO_app/Representation/list/"
            return HttpResponseRedirect(link)
    else:
        form = RepresentationForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "RepresentationUpdate.html",context)

def PropositionUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Proposition"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Proposition, pk=pk)
    if request.method == 'POST':
        form = PropositionForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Proposition(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/Proposition/list/"
            return HttpResponseRedirect(link)
    else:
        form = PropositionForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "PropositionUpdate.html",context)

def ExperimentalEquipmentUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ExperimentalEquipment"]
    foreign_keys = dict()
    
    foreign_keys['Has_description'] = 'EXPO_app:TechnicalDescriptionList'
    
    foreign_keys['Has_name'] = 'EXPO_app:NameEquipmentList'
    
    updated_instance =  get_object_or_404(ExperimentalEquipment, pk=pk)
    if request.method == 'POST':
        form = ExperimentalEquipmentForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ExperimentalEquipment(form.cleaned_data['name'])
            
            onto.save()

            
            fkstr = 'Has_description'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.TechnicalDescription(fkinstanceinput[0])
            onto.save()
            onto_instance.has_description.append(fkinstance) 
            
            fkstr = 'Has_name'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.NameEquipment(fkinstanceinput[0])
            onto.save()
            onto_instance.has_name.append(fkinstance) 
            
            onto.save()


            link = "/EXPO_app/ExperimentalEquipment/list/"
            return HttpResponseRedirect(link)
    else:
        form = ExperimentalEquipmentForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ExperimentalEquipmentUpdate.html",context)

def TechnicalDescriptionUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["TechnicalDescription"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(TechnicalDescription, pk=pk)
    if request.method == 'POST':
        form = TechnicalDescriptionForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.TechnicalDescription(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/TechnicalDescription/list/"
            return HttpResponseRedirect(link)
    else:
        form = TechnicalDescriptionForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "TechnicalDescriptionUpdate.html",context)

def ExperimentalResultsUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ExperimentalResults"]
    foreign_keys = dict()
    
    foreign_keys['Has_error'] = 'EXPO_app:ResultErrorList'
    
    foreign_keys['Has_evaluation'] = 'EXPO_app:ResultsEvaluationList'
    
    foreign_keys['Has_fact'] = 'EXPO_app:FactRejectResearchHypothesisList'
    
    foreign_keys['Has_fact'] = 'EXPO_app:FactSupportResearchHypothesisList'
    
    updated_instance =  get_object_or_404(ExperimentalResults, pk=pk)
    if request.method == 'POST':
        form = ExperimentalResultsForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ExperimentalResults(form.cleaned_data['name'])
            
            onto.save()

            
            fkstr = 'Has_error'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.ResultError(fkinstanceinput[0])
            onto.save()
            onto_instance.has_error.append(fkinstance) 
            
            fkstr = 'Has_evaluation'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.ResultsEvaluation(fkinstanceinput[0])
            onto.save()
            onto_instance.has_evaluation.append(fkinstance) 
            
            fkstr = 'Has_fact'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.FactRejectResearchHypothesis(fkinstanceinput[0])
            onto.save()
            onto_instance.has_fact.append(fkinstance) 
            
            fkstr = 'Has_fact'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.FactSupportResearchHypothesis(fkinstanceinput[0])
            onto.save()
            onto_instance.has_fact.append(fkinstance) 
            
            onto.save()


            link = "/EXPO_app/ExperimentalResults/list/"
            return HttpResponseRedirect(link)
    else:
        form = ExperimentalResultsForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ExperimentalResultsUpdate.html",context)

def ObservationalErrorUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ObservationalError"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ObservationalError, pk=pk)
    if request.method == 'POST':
        form = ObservationalErrorForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ObservationalError(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/ObservationalError/list/"
            return HttpResponseRedirect(link)
    else:
        form = ObservationalErrorForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ObservationalErrorUpdate.html",context)

def ResultErrorUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ResultError"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ResultError, pk=pk)
    if request.method == 'POST':
        form = ResultErrorForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ResultError(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/ResultError/list/"
            return HttpResponseRedirect(link)
    else:
        form = ResultErrorForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ResultErrorUpdate.html",context)

def ResultsEvaluationUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ResultsEvaluation"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ResultsEvaluation, pk=pk)
    if request.method == 'POST':
        form = ResultsEvaluationForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ResultsEvaluation(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/ResultsEvaluation/list/"
            return HttpResponseRedirect(link)
    else:
        form = ResultsEvaluationForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ResultsEvaluationUpdate.html",context)

def ScientificInvestigationUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ScientificInvestigation"]
    foreign_keys = dict()
    
    foreign_keys['Has_experiment'] = 'EXPO_app:ScientificExperimentList'
    
    foreign_keys['Has_experimental_design'] = 'EXPO_app:ExperimentalDesignList'
    
    foreign_keys['Has_hypothesis_formation'] = 'EXPO_app:HypothesisFormationList'
    
    foreign_keys['Has_information_gethering'] = 'EXPO_app:InformationGetheringList'
    
    foreign_keys['Has_problem_analysis'] = 'EXPO_app:PoblemAnalysisList'
    
    foreign_keys['Has_result_evaluation'] = 'EXPO_app:ResultsEvaluationList'
    
    foreign_keys['Has_result_interpretation'] = 'EXPO_app:ResultsInterpretationList'
    
    updated_instance =  get_object_or_404(ScientificInvestigation, pk=pk)
    if request.method == 'POST':
        form = ScientificInvestigationForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ScientificInvestigation(form.cleaned_data['name'])
            
            onto.save()

            
            fkstr = 'Has_experiment'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.ScientificExperiment(fkinstanceinput[0])
            onto.save()
            onto_instance.has_experiment.append(fkinstance) 
            
            fkstr = 'Has_experimental_design'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.ExperimentalDesign(fkinstanceinput[0])
            onto.save()
            onto_instance.has_experimental_design.append(fkinstance) 
            
            fkstr = 'Has_hypothesis_formation'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.HypothesisFormation(fkinstanceinput[0])
            onto.save()
            onto_instance.has_hypothesis_formation.append(fkinstance) 
            
            fkstr = 'Has_information_gethering'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.InformationGethering(fkinstanceinput[0])
            onto.save()
            onto_instance.has_information_gethering.append(fkinstance) 
            
            fkstr = 'Has_problem_analysis'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.PoblemAnalysis(fkinstanceinput[0])
            onto.save()
            onto_instance.has_problem_analysis.append(fkinstance) 
            
            fkstr = 'Has_result_evaluation'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.ResultsEvaluation(fkinstanceinput[0])
            onto.save()
            onto_instance.has_result_evaluation.append(fkinstance) 
            
            fkstr = 'Has_result_interpretation'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.ResultsInterpretation(fkinstanceinput[0])
            onto.save()
            onto_instance.has_result_interpretation.append(fkinstance) 
            
            onto.save()


            link = "/EXPO_app/ScientificInvestigation/list/"
            return HttpResponseRedirect(link)
    else:
        form = ScientificInvestigationForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ScientificInvestigationUpdate.html",context)

def ExperimentalModelUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ExperimentalModel"]
    foreign_keys = dict()
    
    foreign_keys['Has_experimental_factor'] = 'EXPO_app:ExperimentalFactorList'
    
    foreign_keys['Has_unknown_variable'] = 'EXPO_app:TargetVariableList'
    
    updated_instance =  get_object_or_404(ExperimentalModel, pk=pk)
    if request.method == 'POST':
        form = ExperimentalModelForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ExperimentalModel(form.cleaned_data['name'])
            
            onto.save()

            
            fkstr = 'Has_experimental_factor'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.ExperimentalFactor(fkinstanceinput[0])
            onto.save()
            onto_instance.has_experimental_factor.append(fkinstance) 
            
            fkstr = 'Has_unknown_variable'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.TargetVariable(fkinstanceinput[0])
            onto.save()
            onto_instance.has_unknown_variable.append(fkinstance) 
            
            onto.save()


            link = "/EXPO_app/ExperimentalModel/list/"
            return HttpResponseRedirect(link)
    else:
        form = ExperimentalModelForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ExperimentalModelUpdate.html",context)

def ExperimentalFactorUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ExperimentalFactor"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ExperimentalFactor, pk=pk)
    if request.method == 'POST':
        form = ExperimentalFactorForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ExperimentalFactor(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/ExperimentalFactor/list/"
            return HttpResponseRedirect(link)
    else:
        form = ExperimentalFactorForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ExperimentalFactorUpdate.html",context)

def ExperimentalTechnologyUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ExperimentalTechnology"]
    foreign_keys = dict()
    
    foreign_keys['Has_experimental_requirements'] = 'EXPO_app:ExperimentalRequirementsList'
    
    foreign_keys['Has_standard'] = 'EXPO_app:ExperimentalStandardList'
    
    foreign_keys['Has_strategy'] = 'EXPO_app:ExperimentalDesignStrategyList'
    
    updated_instance =  get_object_or_404(ExperimentalTechnology, pk=pk)
    if request.method == 'POST':
        form = ExperimentalTechnologyForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ExperimentalTechnology(form.cleaned_data['name'])
            
            onto.save()

            
            fkstr = 'Has_experimental_requirements'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.ExperimentalRequirements(fkinstanceinput[0])
            onto.save()
            onto_instance.has_experimental_requirements.append(fkinstance) 
            
            fkstr = 'Has_standard'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.ExperimentalStandard(fkinstanceinput[0])
            onto.save()
            onto_instance.has_standard.append(fkinstance) 
            
            fkstr = 'Has_strategy'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.ExperimentalDesignStrategy(fkinstanceinput[0])
            onto.save()
            onto_instance.has_strategy.append(fkinstance) 
            
            onto.save()


            link = "/EXPO_app/ExperimentalTechnology/list/"
            return HttpResponseRedirect(link)
    else:
        form = ExperimentalTechnologyForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ExperimentalTechnologyUpdate.html",context)

def ExperimentalRequirementsUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ExperimentalRequirements"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ExperimentalRequirements, pk=pk)
    if request.method == 'POST':
        form = ExperimentalRequirementsForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ExperimentalRequirements(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/ExperimentalRequirements/list/"
            return HttpResponseRedirect(link)
    else:
        form = ExperimentalRequirementsForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ExperimentalRequirementsUpdate.html",context)

def HypothesisExplicitnessUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["HypothesisExplicitness"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(HypothesisExplicitness, pk=pk)
    if request.method == 'POST':
        form = HypothesisExplicitnessForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.HypothesisExplicitness(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/HypothesisExplicitness/list/"
            return HttpResponseRedirect(link)
    else:
        form = HypothesisExplicitnessForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "HypothesisExplicitnessUpdate.html",context)

def LinguisticExpressionUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["LinguisticExpression"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(LinguisticExpression, pk=pk)
    if request.method == 'POST':
        form = LinguisticExpressionForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.LinguisticExpression(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/LinguisticExpression/list/"
            return HttpResponseRedirect(link)
    else:
        form = LinguisticExpressionForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "LinguisticExpressionUpdate.html",context)

def FactRejectResearchHypothesisUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["FactRejectResearchHypothesis"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(FactRejectResearchHypothesis, pk=pk)
    if request.method == 'POST':
        form = FactRejectResearchHypothesisForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.FactRejectResearchHypothesis(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/FactRejectResearchHypothesis/list/"
            return HttpResponseRedirect(link)
    else:
        form = FactRejectResearchHypothesisForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "FactRejectResearchHypothesisUpdate.html",context)

def FactSupportResearchHypothesisUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["FactSupportResearchHypothesis"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(FactSupportResearchHypothesis, pk=pk)
    if request.method == 'POST':
        form = FactSupportResearchHypothesisForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.FactSupportResearchHypothesis(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/FactSupportResearchHypothesis/list/"
            return HttpResponseRedirect(link)
    else:
        form = FactSupportResearchHypothesisForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "FactSupportResearchHypothesisUpdate.html",context)

def NumberUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Number"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Number, pk=pk)
    if request.method == 'POST':
        form = NumberForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Number(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/Number/list/"
            return HttpResponseRedirect(link)
    else:
        form = NumberForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "NumberUpdate.html",context)

def GeneralityOfHypothesisUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["GeneralityOfHypothesis"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(GeneralityOfHypothesis, pk=pk)
    if request.method == 'POST':
        form = GeneralityOfHypothesisForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.GeneralityOfHypothesis(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/GeneralityOfHypothesis/list/"
            return HttpResponseRedirect(link)
    else:
        form = GeneralityOfHypothesisForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "GeneralityOfHypothesisUpdate.html",context)

def ActionGoalUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ActionGoal"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ActionGoal, pk=pk)
    if request.method == 'POST':
        form = ActionGoalForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ActionGoal(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/ActionGoal/list/"
            return HttpResponseRedirect(link)
    else:
        form = ActionGoalForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ActionGoalUpdate.html",context)

def ExperimentalGoalUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ExperimentalGoal"]
    foreign_keys = dict()
    
    foreign_keys['Has_representation'] = 'EXPO_app:RepresentationExperimentalGoalList'
    
    foreign_keys['Has_subgoal'] = 'EXPO_app:ExperimentalSubgoalList'
    
    updated_instance =  get_object_or_404(ExperimentalGoal, pk=pk)
    if request.method == 'POST':
        form = ExperimentalGoalForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ExperimentalGoal(form.cleaned_data['name'])
            
            onto.save()

            
            fkstr = 'Has_representation'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.RepresentationExperimentalGoal(fkinstanceinput[0])
            onto.save()
            onto_instance.has_representation.append(fkinstance) 
            
            fkstr = 'Has_subgoal'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.ExperimentalSubgoal(fkinstanceinput[0])
            onto.save()
            onto_instance.has_subgoal.append(fkinstance) 
            
            onto.save()


            link = "/EXPO_app/ExperimentalGoal/list/"
            return HttpResponseRedirect(link)
    else:
        form = ExperimentalGoalForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ExperimentalGoalUpdate.html",context)

def MethodGoalUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["MethodGoal"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(MethodGoal, pk=pk)
    if request.method == 'POST':
        form = MethodGoalForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.MethodGoal(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/MethodGoal/list/"
            return HttpResponseRedirect(link)
    else:
        form = MethodGoalForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "MethodGoalUpdate.html",context)

def HypothesisFormationUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["HypothesisFormation"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(HypothesisFormation, pk=pk)
    if request.method == 'POST':
        form = HypothesisFormationForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.HypothesisFormation(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/HypothesisFormation/list/"
            return HttpResponseRedirect(link)
    else:
        form = HypothesisFormationForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "HypothesisFormationUpdate.html",context)

def HypothesisRepresentationUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["HypothesisRepresentation"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(HypothesisRepresentation, pk=pk)
    if request.method == 'POST':
        form = HypothesisRepresentationForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.HypothesisRepresentation(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/HypothesisRepresentation/list/"
            return HttpResponseRedirect(link)
    else:
        form = HypothesisRepresentationForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "HypothesisRepresentationUpdate.html",context)

def InformationGetheringUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["InformationGethering"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(InformationGethering, pk=pk)
    if request.method == 'POST':
        form = InformationGetheringForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.InformationGethering(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/InformationGethering/list/"
            return HttpResponseRedirect(link)
    else:
        form = InformationGetheringForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "InformationGetheringUpdate.html",context)

def SampleUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Sample"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Sample, pk=pk)
    if request.method == 'POST':
        form = SampleForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Sample(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/Sample/list/"
            return HttpResponseRedirect(link)
    else:
        form = SampleForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SampleUpdate.html",context)

def VariabilityUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Variability"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Variability, pk=pk)
    if request.method == 'POST':
        form = VariabilityForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Variability(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/Variability/list/"
            return HttpResponseRedirect(link)
    else:
        form = VariabilityForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "VariabilityUpdate.html",context)

def TimePointUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["TimePoint"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(TimePoint, pk=pk)
    if request.method == 'POST':
        form = TimePointForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.TimePoint(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/TimePoint/list/"
            return HttpResponseRedirect(link)
    else:
        form = TimePointForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "TimePointUpdate.html",context)

def UserUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["User"]
    foreign_keys = dict()
    
    foreign_keys['Has_admin_info'] = 'EXPO_app:AdminInfoUserList'
    
    foreign_keys['Has_login'] = 'EXPO_app:LoginNameList'
    
    foreign_keys['Has_pasword'] = 'EXPO_app:PasswordList'
    
    foreign_keys['Has_admin_info'] = 'EXPO_app:ThingList'
    
    updated_instance =  get_object_or_404(User, pk=pk)
    if request.method == 'POST':
        form = UserForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.User(form.cleaned_data['name'])
            
            onto.save()

            
            fkstr = 'Has_admin_info'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.AdminInfoUser(fkinstanceinput[0])
            onto.save()
            onto_instance.has_admin_info.append(fkinstance) 
            
            fkstr = 'Has_login'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.LoginName(fkinstanceinput[0])
            onto.save()
            onto_instance.has_login.append(fkinstance) 
            
            fkstr = 'Has_pasword'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Password(fkinstanceinput[0])
            onto.save()
            onto_instance.has_pasword.append(fkinstance) 
            
            fkstr = 'Has_admin_info'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Thing(fkinstanceinput[0])
            onto.save()
            onto_instance.has_admin_info.append(fkinstance) 
            
            onto.save()


            link = "/EXPO_app/User/list/"
            return HttpResponseRedirect(link)
    else:
        form = UserForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "UserUpdate.html",context)

def LoginNameUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["LoginName"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(LoginName, pk=pk)
    if request.method == 'POST':
        form = LoginNameForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.LoginName(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/LoginName/list/"
            return HttpResponseRedirect(link)
    else:
        form = LoginNameForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "LoginNameUpdate.html",context)

def DomainModelUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["DomainModel"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(DomainModel, pk=pk)
    if request.method == 'POST':
        form = DomainModelForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.DomainModel(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/DomainModel/list/"
            return HttpResponseRedirect(link)
    else:
        form = DomainModelForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "DomainModelUpdate.html",context)

def GroupExperimentalObjectUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["GroupExperimentalObject"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(GroupExperimentalObject, pk=pk)
    if request.method == 'POST':
        form = GroupExperimentalObjectForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.GroupExperimentalObject(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/GroupExperimentalObject/list/"
            return HttpResponseRedirect(link)
    else:
        form = GroupExperimentalObjectForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "GroupExperimentalObjectUpdate.html",context)

def ObjectOfExperimentUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ObjectOfExperiment"]
    foreign_keys = dict()
    
    foreign_keys['Has_admin_info'] = 'EXPO_app:ThingList'
    
    updated_instance =  get_object_or_404(ObjectOfExperiment, pk=pk)
    if request.method == 'POST':
        form = ObjectOfExperimentForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ObjectOfExperiment(form.cleaned_data['name'])
            
            onto.save()

            
            fkstr = 'Has_admin_info'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Thing(fkinstanceinput[0])
            onto.save()
            onto_instance.has_admin_info.append(fkinstance) 
            
            onto.save()


            link = "/EXPO_app/ObjectOfExperiment/list/"
            return HttpResponseRedirect(link)
    else:
        form = ObjectOfExperimentForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ObjectOfExperimentUpdate.html",context)

def ClassificationByModelUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ClassificationByModel"]
    foreign_keys = dict()
    
    foreign_keys['Has_number_of_factors'] = 'EXPO_app:NumberList'
    
    foreign_keys['Has_number_of_factors'] = 'EXPO_app:ThingList'
    
    updated_instance =  get_object_or_404(ClassificationByModel, pk=pk)
    if request.method == 'POST':
        form = ClassificationByModelForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ClassificationByModel(form.cleaned_data['name'])
            
            onto.save()

            
            fkstr = 'Has_number_of_factors'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Number(fkinstanceinput[0])
            onto.save()
            onto_instance.has_number_of_factors.append(fkinstance) 
            
            fkstr = 'Has_number_of_factors'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Thing(fkinstanceinput[0])
            onto.save()
            onto_instance.has_number_of_factors.append(fkinstance) 
            
            onto.save()


            link = "/EXPO_app/ClassificationByModel/list/"
            return HttpResponseRedirect(link)
    else:
        form = ClassificationByModelForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ClassificationByModelUpdate.html",context)

def StatisticsCharacteristicUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["StatisticsCharacteristic"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(StatisticsCharacteristic, pk=pk)
    if request.method == 'POST':
        form = StatisticsCharacteristicForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.StatisticsCharacteristic(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/StatisticsCharacteristic/list/"
            return HttpResponseRedirect(link)
    else:
        form = StatisticsCharacteristicForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "StatisticsCharacteristicUpdate.html",context)

def ParentGroupUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ParentGroup"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ParentGroup, pk=pk)
    if request.method == 'POST':
        form = ParentGroupForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ParentGroup(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/ParentGroup/list/"
            return HttpResponseRedirect(link)
    else:
        form = ParentGroupForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ParentGroupUpdate.html",context)

def PasswordUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Password"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Password, pk=pk)
    if request.method == 'POST':
        form = PasswordForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Password(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/Password/list/"
            return HttpResponseRedirect(link)
    else:
        form = PasswordForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "PasswordUpdate.html",context)

def ProcedureExecuteExperimentUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ProcedureExecuteExperiment"]
    foreign_keys = dict()
    
    foreign_keys['Has_plan'] = 'EXPO_app:PlanOfExperimentalActionsList'
    
    foreign_keys['Has_representation'] = 'EXPO_app:RepresentationExperimentalExecutionProcedureList'
    
    foreign_keys['Has_status'] = 'EXPO_app:DocumentStageList'
    
    foreign_keys['Has_status'] = 'EXPO_app:PermissionStatusList'
    
    foreign_keys['Has_status'] = 'EXPO_app:ThingList'
    
    updated_instance =  get_object_or_404(ProcedureExecuteExperiment, pk=pk)
    if request.method == 'POST':
        form = ProcedureExecuteExperimentForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ProcedureExecuteExperiment(form.cleaned_data['name'])
            
            onto.save()

            
            fkstr = 'Has_plan'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.PlanOfExperimentalActions(fkinstanceinput[0])
            onto.save()
            onto_instance.has_plan.append(fkinstance) 
            
            fkstr = 'Has_representation'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.RepresentationExperimentalExecutionProcedure(fkinstanceinput[0])
            onto.save()
            onto_instance.has_representation.append(fkinstance) 
            
            fkstr = 'Has_status'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.DocumentStage(fkinstanceinput[0])
            onto.save()
            onto_instance.has_status.append(fkinstance) 
            
            fkstr = 'Has_status'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.PermissionStatus(fkinstanceinput[0])
            onto.save()
            onto_instance.has_status.append(fkinstance) 
            
            fkstr = 'Has_status'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Thing(fkinstanceinput[0])
            onto.save()
            onto_instance.has_status.append(fkinstance) 
            
            onto.save()


            link = "/EXPO_app/ProcedureExecuteExperiment/list/"
            return HttpResponseRedirect(link)
    else:
        form = ProcedureExecuteExperimentForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ProcedureExecuteExperimentUpdate.html",context)

def PlanOfExperimentalActionsUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["PlanOfExperimentalActions"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(PlanOfExperimentalActions, pk=pk)
    if request.method == 'POST':
        form = PlanOfExperimentalActionsForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.PlanOfExperimentalActions(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/PlanOfExperimentalActions/list/"
            return HttpResponseRedirect(link)
    else:
        form = PlanOfExperimentalActionsForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "PlanOfExperimentalActionsUpdate.html",context)

def PoblemAnalysisUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["PoblemAnalysis"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(PoblemAnalysis, pk=pk)
    if request.method == 'POST':
        form = PoblemAnalysisForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.PoblemAnalysis(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/PoblemAnalysis/list/"
            return HttpResponseRedirect(link)
    else:
        form = PoblemAnalysisForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "PoblemAnalysisUpdate.html",context)

def SubjectQualificationUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["SubjectQualification"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(SubjectQualification, pk=pk)
    if request.method == 'POST':
        form = SubjectQualificationForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.SubjectQualification(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/SubjectQualification/list/"
            return HttpResponseRedirect(link)
    else:
        form = SubjectQualificationForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SubjectQualificationUpdate.html",context)

def BiblioReferenceUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["BiblioReference"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(BiblioReference, pk=pk)
    if request.method == 'POST':
        form = BiblioReferenceForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.BiblioReference(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/BiblioReference/list/"
            return HttpResponseRedirect(link)
    else:
        form = BiblioReferenceForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BiblioReferenceUpdate.html",context)

def ModelUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Model"]
    foreign_keys = dict()
    
    foreign_keys['Has_representation'] = 'EXPO_app:ModelRepresentationList'
    
    updated_instance =  get_object_or_404(Model, pk=pk)
    if request.method == 'POST':
        form = ModelForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Model(form.cleaned_data['name'])
            
            onto.save()

            
            fkstr = 'Has_representation'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.ModelRepresentation(fkinstanceinput[0])
            onto.save()
            onto_instance.has_representation.append(fkinstance) 
            
            onto.save()


            link = "/EXPO_app/Model/list/"
            return HttpResponseRedirect(link)
    else:
        form = ModelForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ModelUpdate.html",context)

def ModelRepresentationUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ModelRepresentation"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ModelRepresentation, pk=pk)
    if request.method == 'POST':
        form = ModelRepresentationForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ModelRepresentation(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/ModelRepresentation/list/"
            return HttpResponseRedirect(link)
    else:
        form = ModelRepresentationForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ModelRepresentationUpdate.html",context)

def RepresentationExperimentalExecutionProcedureUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["RepresentationExperimentalExecutionProcedure"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(RepresentationExperimentalExecutionProcedure, pk=pk)
    if request.method == 'POST':
        form = RepresentationExperimentalExecutionProcedureForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.RepresentationExperimentalExecutionProcedure(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/RepresentationExperimentalExecutionProcedure/list/"
            return HttpResponseRedirect(link)
    else:
        form = RepresentationExperimentalExecutionProcedureForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "RepresentationExperimentalExecutionProcedureUpdate.html",context)

def RepresentationExperimentalGoalUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["RepresentationExperimentalGoal"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(RepresentationExperimentalGoal, pk=pk)
    if request.method == 'POST':
        form = RepresentationExperimentalGoalForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.RepresentationExperimentalGoal(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/RepresentationExperimentalGoal/list/"
            return HttpResponseRedirect(link)
    else:
        form = RepresentationExperimentalGoalForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "RepresentationExperimentalGoalUpdate.html",context)

def SampleRepresentativenessUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["SampleRepresentativeness"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(SampleRepresentativeness, pk=pk)
    if request.method == 'POST':
        form = SampleRepresentativenessForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.SampleRepresentativeness(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/SampleRepresentativeness/list/"
            return HttpResponseRedirect(link)
    else:
        form = SampleRepresentativenessForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SampleRepresentativenessUpdate.html",context)

def SampleSizeUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["SampleSize"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(SampleSize, pk=pk)
    if request.method == 'POST':
        form = SampleSizeForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.SampleSize(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/SampleSize/list/"
            return HttpResponseRedirect(link)
    else:
        form = SampleSizeForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SampleSizeUpdate.html",context)

def SamplingMethodUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["SamplingMethod"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(SamplingMethod, pk=pk)
    if request.method == 'POST':
        form = SamplingMethodForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.SamplingMethod(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/SamplingMethod/list/"
            return HttpResponseRedirect(link)
    else:
        form = SamplingMethodForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SamplingMethodUpdate.html",context)

def SystematicSamplingUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["SystematicSampling"]
    foreign_keys = dict()
    
    foreign_keys['Has_sampling_rule'] = 'EXPO_app:SamplingRuleList'
    
    updated_instance =  get_object_or_404(SystematicSampling, pk=pk)
    if request.method == 'POST':
        form = SystematicSamplingForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.SystematicSampling(form.cleaned_data['name'])
            
            onto.save()

            
            fkstr = 'Has_sampling_rule'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.SamplingRule(fkinstanceinput[0])
            onto.save()
            onto_instance.has_sampling_rule.append(fkinstance) 
            
            onto.save()


            link = "/EXPO_app/SystematicSampling/list/"
            return HttpResponseRedirect(link)
    else:
        form = SystematicSamplingForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SystematicSamplingUpdate.html",context)

def SamplingRuleUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["SamplingRule"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(SamplingRule, pk=pk)
    if request.method == 'POST':
        form = SamplingRuleForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.SamplingRule(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/SamplingRule/list/"
            return HttpResponseRedirect(link)
    else:
        form = SamplingRuleForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SamplingRuleUpdate.html",context)

def ExperimentalStandardUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ExperimentalStandard"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ExperimentalStandard, pk=pk)
    if request.method == 'POST':
        form = ExperimentalStandardForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ExperimentalStandard(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/ExperimentalStandard/list/"
            return HttpResponseRedirect(link)
    else:
        form = ExperimentalStandardForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ExperimentalStandardUpdate.html",context)

def ErrorUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Error"]
    foreign_keys = dict()
    
    foreign_keys['Has_statistics_characteristic'] = 'EXPO_app:DispersionList'
    
    foreign_keys['Has_statistics_characteristic'] = 'EXPO_app:LevelOfSignificanceList'
    
    foreign_keys['Has_statistics_characteristic'] = 'EXPO_app:ThingList'
    
    updated_instance =  get_object_or_404(Error, pk=pk)
    if request.method == 'POST':
        form = ErrorForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Error(form.cleaned_data['name'])
            
            onto.save()

            
            fkstr = 'Has_statistics_characteristic'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Dispersion(fkinstanceinput[0])
            onto.save()
            onto_instance.has_statistics_characteristic.append(fkinstance) 
            
            fkstr = 'Has_statistics_characteristic'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.LevelOfSignificance(fkinstanceinput[0])
            onto.save()
            onto_instance.has_statistics_characteristic.append(fkinstance) 
            
            fkstr = 'Has_statistics_characteristic'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Thing(fkinstanceinput[0])
            onto.save()
            onto_instance.has_statistics_characteristic.append(fkinstance) 
            
            onto.save()


            link = "/EXPO_app/Error/list/"
            return HttpResponseRedirect(link)
    else:
        form = ErrorForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ErrorUpdate.html",context)

def DispersionUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Dispersion"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Dispersion, pk=pk)
    if request.method == 'POST':
        form = DispersionForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Dispersion(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/Dispersion/list/"
            return HttpResponseRedirect(link)
    else:
        form = DispersionForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "DispersionUpdate.html",context)

def LevelOfSignificanceUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["LevelOfSignificance"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(LevelOfSignificance, pk=pk)
    if request.method == 'POST':
        form = LevelOfSignificanceForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.LevelOfSignificance(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/LevelOfSignificance/list/"
            return HttpResponseRedirect(link)
    else:
        form = LevelOfSignificanceForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "LevelOfSignificanceUpdate.html",context)

def StatusExperimentalDocumentUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["StatusExperimentalDocument"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(StatusExperimentalDocument, pk=pk)
    if request.method == 'POST':
        form = StatusExperimentalDocumentForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.StatusExperimentalDocument(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/StatusExperimentalDocument/list/"
            return HttpResponseRedirect(link)
    else:
        form = StatusExperimentalDocumentForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "StatusExperimentalDocumentUpdate.html",context)

def ExperimentalDesignStrategyUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ExperimentalDesignStrategy"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ExperimentalDesignStrategy, pk=pk)
    if request.method == 'POST':
        form = ExperimentalDesignStrategyForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ExperimentalDesignStrategy(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/ExperimentalDesignStrategy/list/"
            return HttpResponseRedirect(link)
    else:
        form = ExperimentalDesignStrategyForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ExperimentalDesignStrategyUpdate.html",context)

def ExperimentalSubgoalUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ExperimentalSubgoal"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ExperimentalSubgoal, pk=pk)
    if request.method == 'POST':
        form = ExperimentalSubgoalForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ExperimentalSubgoal(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/ExperimentalSubgoal/list/"
            return HttpResponseRedirect(link)
    else:
        form = ExperimentalSubgoalForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ExperimentalSubgoalUpdate.html",context)

def SubjectMethodUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["SubjectMethod"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(SubjectMethod, pk=pk)
    if request.method == 'POST':
        form = SubjectMethodForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.SubjectMethod(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/SubjectMethod/list/"
            return HttpResponseRedirect(link)
    else:
        form = SubjectMethodForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SubjectMethodUpdate.html",context)

def BaconianExperimentUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["BaconianExperiment"]
    foreign_keys = dict()
    
    foreign_keys['Has_synonym'] = 'EXPO_app:SynonymousExternalConceptList'
    
    foreign_keys['Has_synonym'] = 'EXPO_app:ThingList'
    
    updated_instance =  get_object_or_404(BaconianExperiment, pk=pk)
    if request.method == 'POST':
        form = BaconianExperimentForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.BaconianExperiment(form.cleaned_data['name'])
            
            onto.save()

            
            fkstr = 'Has_synonym'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.SynonymousExternalConcept(fkinstanceinput[0])
            onto.save()
            onto_instance.has_synonym.append(fkinstance) 
            
            fkstr = 'Has_synonym'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Thing(fkinstanceinput[0])
            onto.save()
            onto_instance.has_synonym.append(fkinstance) 
            
            onto.save()


            link = "/EXPO_app/BaconianExperiment/list/"
            return HttpResponseRedirect(link)
    else:
        form = BaconianExperimentForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BaconianExperimentUpdate.html",context)

def TargetVariableUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["TargetVariable"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(TargetVariable, pk=pk)
    if request.method == 'POST':
        form = TargetVariableForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.TargetVariable(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/TargetVariable/list/"
            return HttpResponseRedirect(link)
    else:
        form = TargetVariableForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "TargetVariableUpdate.html",context)

def TitleUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Title"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Title, pk=pk)
    if request.method == 'POST':
        form = TitleForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Title(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/Title/list/"
            return HttpResponseRedirect(link)
    else:
        form = TitleForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "TitleUpdate.html",context)

def ValueEstimateUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ValueEstimate"]
    foreign_keys = dict()
    
    foreign_keys['Has_value_of_variable_'] = 'EXPO_app:ValueOfVariableList'
    
    updated_instance =  get_object_or_404(ValueEstimate, pk=pk)
    if request.method == 'POST':
        form = ValueEstimateForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ValueEstimate(form.cleaned_data['name'])
            
            onto.save()

            
            fkstr = 'Has_value_of_variable_'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.ValueOfVariable(fkinstanceinput[0])
            onto.save()
            onto_instance.has_value_of_variable_.append(fkinstance) 
            
            onto.save()


            link = "/EXPO_app/ValueEstimate/list/"
            return HttpResponseRedirect(link)
    else:
        form = ValueEstimateForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ValueEstimateUpdate.html",context)

def ValueOfVariableUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ValueOfVariable"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ValueOfVariable, pk=pk)
    if request.method == 'POST':
        form = ValueOfVariableForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ValueOfVariable(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/ValueOfVariable/list/"
            return HttpResponseRedirect(link)
    else:
        form = ValueOfVariableForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ValueOfVariableUpdate.html",context)

def RoleUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Role"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Role, pk=pk)
    if request.method == 'POST':
        form = RoleForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Role(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/Role/list/"
            return HttpResponseRedirect(link)
    else:
        form = RoleForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "RoleUpdate.html",context)

def CorpuscularObjectUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["CorpuscularObject"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(CorpuscularObject, pk=pk)
    if request.method == 'POST':
        form = CorpuscularObjectForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.CorpuscularObject(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/CorpuscularObject/list/"
            return HttpResponseRedirect(link)
    else:
        form = CorpuscularObjectForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "CorpuscularObjectUpdate.html",context)

def AttributeRoleUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["AttributeRole"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(AttributeRole, pk=pk)
    if request.method == 'POST':
        form = AttributeRoleForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.AttributeRole(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/AttributeRole/list/"
            return HttpResponseRedirect(link)
    else:
        form = AttributeRoleForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AttributeRoleUpdate.html",context)

def AttributeUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Attribute"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Attribute, pk=pk)
    if request.method == 'POST':
        form = AttributeForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Attribute(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/Attribute/list/"
            return HttpResponseRedirect(link)
    else:
        form = AttributeForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AttributeUpdate.html",context)

def ProcedureUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Procedure"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Procedure, pk=pk)
    if request.method == 'POST':
        form = ProcedureForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Procedure(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/Procedure/list/"
            return HttpResponseRedirect(link)
    else:
        form = ProcedureForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ProcedureUpdate.html",context)

def AdministrativeInformationUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["AdministrativeInformation"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(AdministrativeInformation, pk=pk)
    if request.method == 'POST':
        form = AdministrativeInformationForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.AdministrativeInformation(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/AdministrativeInformation/list/"
            return HttpResponseRedirect(link)
    else:
        form = AdministrativeInformationForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AdministrativeInformationUpdate.html",context)

def TitleExperimentUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["TitleExperiment"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(TitleExperiment, pk=pk)
    if request.method == 'POST':
        form = TitleExperimentForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.TitleExperiment(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/TitleExperiment/list/"
            return HttpResponseRedirect(link)
    else:
        form = TitleExperimentForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "TitleExperimentUpdate.html",context)

def NameEquipmentUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["NameEquipment"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(NameEquipment, pk=pk)
    if request.method == 'POST':
        form = NameEquipmentForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.NameEquipment(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/NameEquipment/list/"
            return HttpResponseRedirect(link)
    else:
        form = NameEquipmentForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "NameEquipmentUpdate.html",context)

def PersonalNameUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["PersonalName"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(PersonalName, pk=pk)
    if request.method == 'POST':
        form = PersonalNameForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.PersonalName(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/PersonalName/list/"
            return HttpResponseRedirect(link)
    else:
        form = PersonalNameForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "PersonalNameUpdate.html",context)

def FieldOfStudyUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["FieldOfStudy"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(FieldOfStudy, pk=pk)
    if request.method == 'POST':
        form = FieldOfStudyForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.FieldOfStudy(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/FieldOfStudy/list/"
            return HttpResponseRedirect(link)
    else:
        form = FieldOfStudyForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "FieldOfStudyUpdate.html",context)

def RelatedDomainUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["RelatedDomain"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(RelatedDomain, pk=pk)
    if request.method == 'POST':
        form = RelatedDomainForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.RelatedDomain(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/RelatedDomain/list/"
            return HttpResponseRedirect(link)
    else:
        form = RelatedDomainForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "RelatedDomainUpdate.html",context)

def ProductRoleUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ProductRole"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ProductRole, pk=pk)
    if request.method == 'POST':
        form = ProductRoleForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ProductRole(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/ProductRole/list/"
            return HttpResponseRedirect(link)
    else:
        form = ProductRoleForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ProductRoleUpdate.html",context)

def ScientificTaskUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ScientificTask"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ScientificTask, pk=pk)
    if request.method == 'POST':
        form = ScientificTaskForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ScientificTask(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/ScientificTask/list/"
            return HttpResponseRedirect(link)
    else:
        form = ScientificTaskForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ScientificTaskUpdate.html",context)

def ExecutionOfExperimentUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ExecutionOfExperiment"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ExecutionOfExperiment, pk=pk)
    if request.method == 'POST':
        form = ExecutionOfExperimentForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ExecutionOfExperiment(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/ExecutionOfExperiment/list/"
            return HttpResponseRedirect(link)
    else:
        form = ExecutionOfExperimentForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ExecutionOfExperimentUpdate.html",context)

def CorporateNameUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["CorporateName"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(CorporateName, pk=pk)
    if request.method == 'POST':
        form = CorporateNameForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.CorporateName(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/CorporateName/list/"
            return HttpResponseRedirect(link)
    else:
        form = CorporateNameForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "CorporateNameUpdate.html",context)

def AttributeOfActionUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["AttributeOfAction"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(AttributeOfAction, pk=pk)
    if request.method == 'POST':
        form = AttributeOfActionForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.AttributeOfAction(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/AttributeOfAction/list/"
            return HttpResponseRedirect(link)
    else:
        form = AttributeOfActionForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AttributeOfActionUpdate.html",context)

def SentientAgentUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["SentientAgent"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(SentientAgent, pk=pk)
    if request.method == 'POST':
        form = SentientAgentForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.SentientAgent(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/SentientAgent/list/"
            return HttpResponseRedirect(link)
    else:
        form = SentientAgentForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SentientAgentUpdate.html",context)

def RobotUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Robot"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Robot, pk=pk)
    if request.method == 'POST':
        form = RobotForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Robot(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/Robot/list/"
            return HttpResponseRedirect(link)
    else:
        form = RobotForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "RobotUpdate.html",context)

def GroupUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Group"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Group, pk=pk)
    if request.method == 'POST':
        form = GroupForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Group(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/Group/list/"
            return HttpResponseRedirect(link)
    else:
        form = GroupForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "GroupUpdate.html",context)

def EntityUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Entity"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Entity, pk=pk)
    if request.method == 'POST':
        form = EntityForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Entity(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/Entity/list/"
            return HttpResponseRedirect(link)
    else:
        form = EntityForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "EntityUpdate.html",context)

def ResearchMethodUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ResearchMethod"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ResearchMethod, pk=pk)
    if request.method == 'POST':
        form = ResearchMethodForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ResearchMethod(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/ResearchMethod/list/"
            return HttpResponseRedirect(link)
    else:
        form = ResearchMethodForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ResearchMethodUpdate.html",context)

def RequirementsUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Requirements"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Requirements, pk=pk)
    if request.method == 'POST':
        form = RequirementsForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Requirements(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/Requirements/list/"
            return HttpResponseRedirect(link)
    else:
        form = RequirementsForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "RequirementsUpdate.html",context)

def AuthorSOPUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["AuthorSOP"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(AuthorSOP, pk=pk)
    if request.method == 'POST':
        form = AuthorSOPForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.AuthorSOP(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/AuthorSOP/list/"
            return HttpResponseRedirect(link)
    else:
        form = AuthorSOPForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AuthorSOPUpdate.html",context)

def SubjectRoleUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["SubjectRole"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(SubjectRole, pk=pk)
    if request.method == 'POST':
        form = SubjectRoleForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.SubjectRole(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/SubjectRole/list/"
            return HttpResponseRedirect(link)
    else:
        form = SubjectRoleForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SubjectRoleUpdate.html",context)

def ClassificationUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Classification"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Classification, pk=pk)
    if request.method == 'POST':
        form = ClassificationForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Classification(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/Classification/list/"
            return HttpResponseRedirect(link)
    else:
        form = ClassificationForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ClassificationUpdate.html",context)

def FactUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Fact"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Fact, pk=pk)
    if request.method == 'POST':
        form = FactForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Fact(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/Fact/list/"
            return HttpResponseRedirect(link)
    else:
        form = FactForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "FactUpdate.html",context)

def ModelAssumptionUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ModelAssumption"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ModelAssumption, pk=pk)
    if request.method == 'POST':
        form = ModelAssumptionForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ModelAssumption(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/ModelAssumption/list/"
            return HttpResponseRedirect(link)
    else:
        form = ModelAssumptionForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ModelAssumptionUpdate.html",context)

def VariableUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Variable"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Variable, pk=pk)
    if request.method == 'POST':
        form = VariableForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Variable(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/Variable/list/"
            return HttpResponseRedirect(link)
    else:
        form = VariableForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "VariableUpdate.html",context)

def AttributeOfHypothesisUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["AttributeOfHypothesis"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(AttributeOfHypothesis, pk=pk)
    if request.method == 'POST':
        form = AttributeOfHypothesisForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.AttributeOfHypothesis(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/AttributeOfHypothesis/list/"
            return HttpResponseRedirect(link)
    else:
        form = AttributeOfHypothesisForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AttributeOfHypothesisUpdate.html",context)

def ContentBearingObjectUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ContentBearingObject"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ContentBearingObject, pk=pk)
    if request.method == 'POST':
        form = ContentBearingObjectForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ContentBearingObject(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/ContentBearingObject/list/"
            return HttpResponseRedirect(link)
    else:
        form = ContentBearingObjectForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ContentBearingObjectUpdate.html",context)

def AbstractUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Abstract"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Abstract, pk=pk)
    if request.method == 'POST':
        form = AbstractForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Abstract(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/Abstract/list/"
            return HttpResponseRedirect(link)
    else:
        form = AbstractForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AbstractUpdate.html",context)

def QuantityUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Quantity"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Quantity, pk=pk)
    if request.method == 'POST':
        form = QuantityForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Quantity(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/Quantity/list/"
            return HttpResponseRedirect(link)
    else:
        form = QuantityForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "QuantityUpdate.html",context)

def RelationUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Relation"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Relation, pk=pk)
    if request.method == 'POST':
        form = RelationForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Relation(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/Relation/list/"
            return HttpResponseRedirect(link)
    else:
        form = RelationForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "RelationUpdate.html",context)

def ProcessrelatedRoleUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ProcessrelatedRole"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ProcessrelatedRole, pk=pk)
    if request.method == 'POST':
        form = ProcessrelatedRoleForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ProcessrelatedRole(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/ProcessrelatedRole/list/"
            return HttpResponseRedirect(link)
    else:
        form = ProcessrelatedRoleForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ProcessrelatedRoleUpdate.html",context)

def ContentBearingPhysicalUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ContentBearingPhysical"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ContentBearingPhysical, pk=pk)
    if request.method == 'POST':
        form = ContentBearingPhysicalForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ContentBearingPhysical(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/ContentBearingPhysical/list/"
            return HttpResponseRedirect(link)
    else:
        form = ContentBearingPhysicalForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ContentBearingPhysicalUpdate.html",context)

def PhysicalQuantityUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["PhysicalQuantity"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(PhysicalQuantity, pk=pk)
    if request.method == 'POST':
        form = PhysicalQuantityForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.PhysicalQuantity(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/PhysicalQuantity/list/"
            return HttpResponseRedirect(link)
    else:
        form = PhysicalQuantityForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "PhysicalQuantityUpdate.html",context)

def GoalUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Goal"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Goal, pk=pk)
    if request.method == 'POST':
        form = GoalForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Goal(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/Goal/list/"
            return HttpResponseRedirect(link)
    else:
        form = GoalForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "GoalUpdate.html",context)

def RepresentationExperimentalObservationUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["RepresentationExperimentalObservation"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(RepresentationExperimentalObservation, pk=pk)
    if request.method == 'POST':
        form = RepresentationExperimentalObservationForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.RepresentationExperimentalObservation(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/RepresentationExperimentalObservation/list/"
            return HttpResponseRedirect(link)
    else:
        form = RepresentationExperimentalObservationForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "RepresentationExperimentalObservationUpdate.html",context)

def RepresentationExperimentalResultsUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["RepresentationExperimentalResults"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(RepresentationExperimentalResults, pk=pk)
    if request.method == 'POST':
        form = RepresentationExperimentalResultsForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.RepresentationExperimentalResults(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/RepresentationExperimentalResults/list/"
            return HttpResponseRedirect(link)
    else:
        form = RepresentationExperimentalResultsForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "RepresentationExperimentalResultsUpdate.html",context)

def AttributeGroupUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["AttributeGroup"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(AttributeGroup, pk=pk)
    if request.method == 'POST':
        form = AttributeGroupForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.AttributeGroup(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/AttributeGroup/list/"
            return HttpResponseRedirect(link)
    else:
        form = AttributeGroupForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AttributeGroupUpdate.html",context)

def TimePositionUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["TimePosition"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(TimePosition, pk=pk)
    if request.method == 'POST':
        form = TimePositionForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.TimePosition(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/TimePosition/list/"
            return HttpResponseRedirect(link)
    else:
        form = TimePositionForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "TimePositionUpdate.html",context)

def ActorRoleUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ActorRole"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ActorRole, pk=pk)
    if request.method == 'POST':
        form = ActorRoleForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ActorRole(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/ActorRole/list/"
            return HttpResponseRedirect(link)
    else:
        form = ActorRoleForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ActorRoleUpdate.html",context)

def DocumentStageUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["DocumentStage"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(DocumentStage, pk=pk)
    if request.method == 'POST':
        form = DocumentStageForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.DocumentStage(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/DocumentStage/list/"
            return HttpResponseRedirect(link)
    else:
        form = DocumentStageForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "DocumentStageUpdate.html",context)

def PermissionStatusUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["PermissionStatus"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(PermissionStatus, pk=pk)
    if request.method == 'POST':
        form = PermissionStatusForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.PermissionStatus(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/PermissionStatus/list/"
            return HttpResponseRedirect(link)
    else:
        form = PermissionStatusForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "PermissionStatusUpdate.html",context)

def PlanUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Plan"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Plan, pk=pk)
    if request.method == 'POST':
        form = PlanForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Plan(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/Plan/list/"
            return HttpResponseRedirect(link)
    else:
        form = PlanForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "PlanUpdate.html",context)

def ReferenceUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Reference"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Reference, pk=pk)
    if request.method == 'POST':
        form = ReferenceForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Reference(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/Reference/list/"
            return HttpResponseRedirect(link)
    else:
        form = ReferenceForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ReferenceUpdate.html",context)

def AttributeSampleUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["AttributeSample"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(AttributeSample, pk=pk)
    if request.method == 'POST':
        form = AttributeSampleForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.AttributeSample(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/AttributeSample/list/"
            return HttpResponseRedirect(link)
    else:
        form = AttributeSampleForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AttributeSampleUpdate.html",context)

def ExperimentalRuleUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ExperimentalRule"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ExperimentalRule, pk=pk)
    if request.method == 'POST':
        form = ExperimentalRuleForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ExperimentalRule(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/ExperimentalRule/list/"
            return HttpResponseRedirect(link)
    else:
        form = ExperimentalRuleForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ExperimentalRuleUpdate.html",context)

def RobustnessUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Robustness"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Robustness, pk=pk)
    if request.method == 'POST':
        form = RobustnessForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Robustness(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/Robustness/list/"
            return HttpResponseRedirect(link)
    else:
        form = RobustnessForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "RobustnessUpdate.html",context)

def ValidityUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Validity"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Validity, pk=pk)
    if request.method == 'POST':
        form = ValidityForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Validity(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/Validity/list/"
            return HttpResponseRedirect(link)
    else:
        form = ValidityForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ValidityUpdate.html",context)

def AttributeOfDocumentUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["AttributeOfDocument"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(AttributeOfDocument, pk=pk)
    if request.method == 'POST':
        form = AttributeOfDocumentForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.AttributeOfDocument(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/AttributeOfDocument/list/"
            return HttpResponseRedirect(link)
    else:
        form = AttributeOfDocumentForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AttributeOfDocumentUpdate.html",context)

def PhysicalExperimentUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["PhysicalExperiment"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(PhysicalExperiment, pk=pk)
    if request.method == 'POST':
        form = PhysicalExperimentForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.PhysicalExperiment(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/PhysicalExperiment/list/"
            return HttpResponseRedirect(link)
    else:
        form = PhysicalExperimentForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "PhysicalExperimentUpdate.html",context)

def ComputationalDataUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ComputationalData"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ComputationalData, pk=pk)
    if request.method == 'POST':
        form = ComputationalDataForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ComputationalData(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/ComputationalData/list/"
            return HttpResponseRedirect(link)
    else:
        form = ComputationalDataForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ComputationalDataUpdate.html",context)

def PredicateUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Predicate"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Predicate, pk=pk)
    if request.method == 'POST':
        form = PredicateForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Predicate(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/Predicate/list/"
            return HttpResponseRedirect(link)
    else:
        form = PredicateForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "PredicateUpdate.html",context)

def SelfConnectedObjectUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["SelfConnectedObject"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(SelfConnectedObject, pk=pk)
    if request.method == 'POST':
        form = SelfConnectedObjectForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.SelfConnectedObject(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/SelfConnectedObject/list/"
            return HttpResponseRedirect(link)
    else:
        form = SelfConnectedObjectForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SelfConnectedObjectUpdate.html",context)

def ScientificActivityUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ScientificActivity"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ScientificActivity, pk=pk)
    if request.method == 'POST':
        form = ScientificActivityForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ScientificActivity(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/ScientificActivity/list/"
            return HttpResponseRedirect(link)
    else:
        form = ScientificActivityForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ScientificActivityUpdate.html",context)

def FormingClassificationSystemUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["FormingClassificationSystem"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(FormingClassificationSystem, pk=pk)
    if request.method == 'POST':
        form = FormingClassificationSystemForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.FormingClassificationSystem(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/FormingClassificationSystem/list/"
            return HttpResponseRedirect(link)
    else:
        form = FormingClassificationSystemForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "FormingClassificationSystemUpdate.html",context)

def HypothesisFormingUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["HypothesisForming"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(HypothesisForming, pk=pk)
    if request.method == 'POST':
        form = HypothesisFormingForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.HypothesisForming(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/HypothesisForming/list/"
            return HttpResponseRedirect(link)
    else:
        form = HypothesisFormingForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "HypothesisFormingUpdate.html",context)

def InterpretingResultUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["InterpretingResult"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(InterpretingResult, pk=pk)
    if request.method == 'POST':
        form = InterpretingResultForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.InterpretingResult(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/InterpretingResult/list/"
            return HttpResponseRedirect(link)
    else:
        form = InterpretingResultForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "InterpretingResultUpdate.html",context)

def ProcessProblemAnalysisUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ProcessProblemAnalysis"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ProcessProblemAnalysis, pk=pk)
    if request.method == 'POST':
        form = ProcessProblemAnalysisForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ProcessProblemAnalysis(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/ProcessProblemAnalysis/list/"
            return HttpResponseRedirect(link)
    else:
        form = ProcessProblemAnalysisForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ProcessProblemAnalysisUpdate.html",context)

def ResultEvaluatingUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ResultEvaluating"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ResultEvaluating, pk=pk)
    if request.method == 'POST':
        form = ResultEvaluatingForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ResultEvaluating(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/ResultEvaluating/list/"
            return HttpResponseRedirect(link)
    else:
        form = ResultEvaluatingForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ResultEvaluatingUpdate.html",context)

def AttributeOfModelUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["AttributeOfModel"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(AttributeOfModel, pk=pk)
    if request.method == 'POST':
        form = AttributeOfModelForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.AttributeOfModel(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/AttributeOfModel/list/"
            return HttpResponseRedirect(link)
    else:
        form = AttributeOfModelForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AttributeOfModelUpdate.html",context)

def AttributeOfVariableUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["AttributeOfVariable"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(AttributeOfVariable, pk=pk)
    if request.method == 'POST':
        form = AttributeOfVariableForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.AttributeOfVariable(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/AttributeOfVariable/list/"
            return HttpResponseRedirect(link)
    else:
        form = AttributeOfVariableForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AttributeOfVariableUpdate.html",context)

def AgentUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Agent"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Agent, pk=pk)
    if request.method == 'POST':
        form = AgentForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Agent(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/Agent/list/"
            return HttpResponseRedirect(link)
    else:
        form = AgentForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AgentUpdate.html",context)

def CollectionUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Collection"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Collection, pk=pk)
    if request.method == 'POST':
        form = CollectionForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Collection(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/Collection/list/"
            return HttpResponseRedirect(link)
    else:
        form = CollectionForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "CollectionUpdate.html",context)

def EperimentalDesignTaskUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["EperimentalDesignTask"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(EperimentalDesignTask, pk=pk)
    if request.method == 'POST':
        form = EperimentalDesignTaskForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.EperimentalDesignTask(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/EperimentalDesignTask/list/"
            return HttpResponseRedirect(link)
    else:
        form = EperimentalDesignTaskForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "EperimentalDesignTaskUpdate.html",context)

def PhysicalUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Physical"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Physical, pk=pk)
    if request.method == 'POST':
        form = PhysicalForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Physical(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/Physical/list/"
            return HttpResponseRedirect(link)
    else:
        form = PhysicalForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "PhysicalUpdate.html",context)

def ObjectUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Object"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Object, pk=pk)
    if request.method == 'POST':
        form = ObjectForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Object(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/Object/list/"
            return HttpResponseRedirect(link)
    else:
        form = ObjectForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ObjectUpdate.html",context)

def ProcessUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Process"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Process, pk=pk)
    if request.method == 'POST':
        form = ProcessForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Process(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/Process/list/"
            return HttpResponseRedirect(link)
    else:
        form = ProcessForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ProcessUpdate.html",context)

def TaskRoleUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["TaskRole"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(TaskRole, pk=pk)
    if request.method == 'POST':
        form = TaskRoleForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.TaskRole(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/TaskRole/list/"
            return HttpResponseRedirect(link)
    else:
        form = TaskRoleForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "TaskRoleUpdate.html",context)

def TimeMeasureUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["TimeMeasure"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(TimeMeasure, pk=pk)
    if request.method == 'POST':
        form = TimeMeasureForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.TimeMeasure(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/TimeMeasure/list/"
            return HttpResponseRedirect(link)
    else:
        form = TimeMeasureForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "TimeMeasureUpdate.html",context)

def ActionrelatedRoleUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ActionrelatedRole"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ActionrelatedRole, pk=pk)
    if request.method == 'POST':
        form = ActionrelatedRoleForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ActionrelatedRole(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/ActionrelatedRole/list/"
            return HttpResponseRedirect(link)
    else:
        form = ActionrelatedRoleForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ActionrelatedRoleUpdate.html",context)

def AbductiveHypothesisUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["AbductiveHypothesis"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(AbductiveHypothesis, pk=pk)
    if request.method == 'POST':
        form = AbductiveHypothesisForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.AbductiveHypothesis(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/AbductiveHypothesis/list/"
            return HttpResponseRedirect(link)
    else:
        form = AbductiveHypothesisForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AbductiveHypothesisUpdate.html",context)

def InductiveHypothesisUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["InductiveHypothesis"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(InductiveHypothesis, pk=pk)
    if request.method == 'POST':
        form = InductiveHypothesisForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.InductiveHypothesis(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/InductiveHypothesis/list/"
            return HttpResponseRedirect(link)
    else:
        form = InductiveHypothesisForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "InductiveHypothesisUpdate.html",context)

def AdequacyUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Adequacy"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Adequacy, pk=pk)
    if request.method == 'POST':
        form = AdequacyForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Adequacy(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/Adequacy/list/"
            return HttpResponseRedirect(link)
    else:
        form = AdequacyForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AdequacyUpdate.html",context)

def AlternativeHypothesisUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["AlternativeHypothesis"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(AlternativeHypothesis, pk=pk)
    if request.method == 'POST':
        form = AlternativeHypothesisForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.AlternativeHypothesis(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/AlternativeHypothesis/list/"
            return HttpResponseRedirect(link)
    else:
        form = AlternativeHypothesisForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AlternativeHypothesisUpdate.html",context)

def NullHypothesisUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["NullHypothesis"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(NullHypothesis, pk=pk)
    if request.method == 'POST':
        form = NullHypothesisForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.NullHypothesis(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/NullHypothesis/list/"
            return HttpResponseRedirect(link)
    else:
        form = NullHypothesisForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "NullHypothesisUpdate.html",context)

def ResearchHypothesisUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ResearchHypothesis"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ResearchHypothesis, pk=pk)
    if request.method == 'POST':
        form = ResearchHypothesisForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ResearchHypothesis(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/ResearchHypothesis/list/"
            return HttpResponseRedirect(link)
    else:
        form = ResearchHypothesisForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ResearchHypothesisUpdate.html",context)

def ArticleUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Article"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Article, pk=pk)
    if request.method == 'POST':
        form = ArticleForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Article(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/Article/list/"
            return HttpResponseRedirect(link)
    else:
        form = ArticleForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ArticleUpdate.html",context)

def TextUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Text"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Text, pk=pk)
    if request.method == 'POST':
        form = TextForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Text(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/Text/list/"
            return HttpResponseRedirect(link)
    else:
        form = TextForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "TextUpdate.html",context)

def ArtificialLanguageUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ArtificialLanguage"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ArtificialLanguage, pk=pk)
    if request.method == 'POST':
        form = ArtificialLanguageForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ArtificialLanguage(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/ArtificialLanguage/list/"
            return HttpResponseRedirect(link)
    else:
        form = ArtificialLanguageForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ArtificialLanguageUpdate.html",context)

def LanguageUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Language"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Language, pk=pk)
    if request.method == 'POST':
        form = LanguageForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Language(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/Language/list/"
            return HttpResponseRedirect(link)
    else:
        form = LanguageForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "LanguageUpdate.html",context)

def HumanLanguageUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["HumanLanguage"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(HumanLanguage, pk=pk)
    if request.method == 'POST':
        form = HumanLanguageForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.HumanLanguage(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/HumanLanguage/list/"
            return HttpResponseRedirect(link)
    else:
        form = HumanLanguageForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "HumanLanguageUpdate.html",context)

def SentenceUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Sentence"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Sentence, pk=pk)
    if request.method == 'POST':
        form = SentenceForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Sentence(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/Sentence/list/"
            return HttpResponseRedirect(link)
    else:
        form = SentenceForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SentenceUpdate.html",context)

def AtomicActionUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["AtomicAction"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(AtomicAction, pk=pk)
    if request.method == 'POST':
        form = AtomicActionForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.AtomicAction(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/AtomicAction/list/"
            return HttpResponseRedirect(link)
    else:
        form = AtomicActionForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AtomicActionUpdate.html",context)

def ComplexActionUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ComplexAction"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ComplexAction, pk=pk)
    if request.method == 'POST':
        form = ComplexActionForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ComplexAction(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/ComplexAction/list/"
            return HttpResponseRedirect(link)
    else:
        form = ComplexActionForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ComplexActionUpdate.html",context)

def AuthorProtocolUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["AuthorProtocol"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(AuthorProtocol, pk=pk)
    if request.method == 'POST':
        form = AuthorProtocolForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.AuthorProtocol(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/AuthorProtocol/list/"
            return HttpResponseRedirect(link)
    else:
        form = AuthorProtocolForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "AuthorProtocolUpdate.html",context)

def BookUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Book"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Book, pk=pk)
    if request.method == 'POST':
        form = BookForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Book(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/Book/list/"
            return HttpResponseRedirect(link)
    else:
        form = BookForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "BookUpdate.html",context)

def CalculableVariableUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["CalculableVariable"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(CalculableVariable, pk=pk)
    if request.method == 'POST':
        form = CalculableVariableForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.CalculableVariable(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/CalculableVariable/list/"
            return HttpResponseRedirect(link)
    else:
        form = CalculableVariableForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "CalculableVariableUpdate.html",context)

def CapacityRequirementsUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["CapacityRequirements"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(CapacityRequirements, pk=pk)
    if request.method == 'POST':
        form = CapacityRequirementsForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.CapacityRequirements(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/CapacityRequirements/list/"
            return HttpResponseRedirect(link)
    else:
        form = CapacityRequirementsForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "CapacityRequirementsUpdate.html",context)

def EnvironmentalRequirementsUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["EnvironmentalRequirements"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(EnvironmentalRequirements, pk=pk)
    if request.method == 'POST':
        form = EnvironmentalRequirementsForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.EnvironmentalRequirements(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/EnvironmentalRequirements/list/"
            return HttpResponseRedirect(link)
    else:
        form = EnvironmentalRequirementsForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "EnvironmentalRequirementsUpdate.html",context)

def FinancialRequirementsUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["FinancialRequirements"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(FinancialRequirements, pk=pk)
    if request.method == 'POST':
        form = FinancialRequirementsForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.FinancialRequirements(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/FinancialRequirements/list/"
            return HttpResponseRedirect(link)
    else:
        form = FinancialRequirementsForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "FinancialRequirementsUpdate.html",context)

def ClassificationByDomainUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ClassificationByDomain"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ClassificationByDomain, pk=pk)
    if request.method == 'POST':
        form = ClassificationByDomainForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ClassificationByDomain(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/ClassificationByDomain/list/"
            return HttpResponseRedirect(link)
    else:
        form = ClassificationByDomainForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ClassificationByDomainUpdate.html",context)

def ClassifyingUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Classifying"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Classifying, pk=pk)
    if request.method == 'POST':
        form = ClassifyingForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Classifying(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/Classifying/list/"
            return HttpResponseRedirect(link)
    else:
        form = ClassifyingForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ClassifyingUpdate.html",context)

def DesigningExperimentUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["DesigningExperiment"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(DesigningExperiment, pk=pk)
    if request.method == 'POST':
        form = DesigningExperimentForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.DesigningExperiment(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/DesigningExperiment/list/"
            return HttpResponseRedirect(link)
    else:
        form = DesigningExperimentForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "DesigningExperimentUpdate.html",context)

def CoarseErrorUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["CoarseError"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(CoarseError, pk=pk)
    if request.method == 'POST':
        form = CoarseErrorForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.CoarseError(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/CoarseError/list/"
            return HttpResponseRedirect(link)
    else:
        form = CoarseErrorForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "CoarseErrorUpdate.html",context)

def MeasurementErrorUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["MeasurementError"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(MeasurementError, pk=pk)
    if request.method == 'POST':
        form = MeasurementErrorForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.MeasurementError(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/MeasurementError/list/"
            return HttpResponseRedirect(link)
    else:
        form = MeasurementErrorForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "MeasurementErrorUpdate.html",context)

def ComparisonControl_TargetGroupsUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ComparisonControl_TargetGroups"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ComparisonControl_TargetGroups, pk=pk)
    if request.method == 'POST':
        form = ComparisonControl_TargetGroupsForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ComparisonControl_TargetGroups(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/ComparisonControl_TargetGroups/list/"
            return HttpResponseRedirect(link)
    else:
        form = ComparisonControl_TargetGroupsForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ComparisonControl_TargetGroupsUpdate.html",context)

def PairedComparisonUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["PairedComparison"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(PairedComparison, pk=pk)
    if request.method == 'POST':
        form = PairedComparisonForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.PairedComparison(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/PairedComparison/list/"
            return HttpResponseRedirect(link)
    else:
        form = PairedComparisonForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "PairedComparisonUpdate.html",context)

def PairedComparisonOfMatchingGroupsUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["PairedComparisonOfMatchingGroups"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(PairedComparisonOfMatchingGroups, pk=pk)
    if request.method == 'POST':
        form = PairedComparisonOfMatchingGroupsForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.PairedComparisonOfMatchingGroups(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/PairedComparisonOfMatchingGroups/list/"
            return HttpResponseRedirect(link)
    else:
        form = PairedComparisonOfMatchingGroupsForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "PairedComparisonOfMatchingGroupsUpdate.html",context)

def PairedComparisonOfSingleSampleGroupsUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["PairedComparisonOfSingleSampleGroups"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(PairedComparisonOfSingleSampleGroups, pk=pk)
    if request.method == 'POST':
        form = PairedComparisonOfSingleSampleGroupsForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.PairedComparisonOfSingleSampleGroups(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/PairedComparisonOfSingleSampleGroups/list/"
            return HttpResponseRedirect(link)
    else:
        form = PairedComparisonOfSingleSampleGroupsForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "PairedComparisonOfSingleSampleGroupsUpdate.html",context)

def QualityControlUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["QualityControl"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(QualityControl, pk=pk)
    if request.method == 'POST':
        form = QualityControlForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.QualityControl(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/QualityControl/list/"
            return HttpResponseRedirect(link)
    else:
        form = QualityControlForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "QualityControlUpdate.html",context)

def ComplexHypothesisUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ComplexHypothesis"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ComplexHypothesis, pk=pk)
    if request.method == 'POST':
        form = ComplexHypothesisForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ComplexHypothesis(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/ComplexHypothesis/list/"
            return HttpResponseRedirect(link)
    else:
        form = ComplexHypothesisForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ComplexHypothesisUpdate.html",context)

def SingleHypothesisUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["SingleHypothesis"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(SingleHypothesis, pk=pk)
    if request.method == 'POST':
        form = SingleHypothesisForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.SingleHypothesis(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/SingleHypothesis/list/"
            return HttpResponseRedirect(link)
    else:
        form = SingleHypothesisForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SingleHypothesisUpdate.html",context)

def ComputationalExperimentUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ComputationalExperiment"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ComputationalExperiment, pk=pk)
    if request.method == 'POST':
        form = ComputationalExperimentForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ComputationalExperiment(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/ComputationalExperiment/list/"
            return HttpResponseRedirect(link)
    else:
        form = ComputationalExperimentForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ComputationalExperimentUpdate.html",context)

def ComputeGoalUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ComputeGoal"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ComputeGoal, pk=pk)
    if request.method == 'POST':
        form = ComputeGoalForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ComputeGoal(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/ComputeGoal/list/"
            return HttpResponseRedirect(link)
    else:
        form = ComputeGoalForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ComputeGoalUpdate.html",context)

def ConfirmGoalUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ConfirmGoal"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ConfirmGoal, pk=pk)
    if request.method == 'POST':
        form = ConfirmGoalForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ConfirmGoal(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/ConfirmGoal/list/"
            return HttpResponseRedirect(link)
    else:
        form = ConfirmGoalForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ConfirmGoalUpdate.html",context)

def ExplainGoalUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ExplainGoal"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ExplainGoal, pk=pk)
    if request.method == 'POST':
        form = ExplainGoalForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ExplainGoal(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/ExplainGoal/list/"
            return HttpResponseRedirect(link)
    else:
        form = ExplainGoalForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ExplainGoalUpdate.html",context)

def InvestigateGoalUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["InvestigateGoal"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(InvestigateGoal, pk=pk)
    if request.method == 'POST':
        form = InvestigateGoalForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.InvestigateGoal(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/InvestigateGoal/list/"
            return HttpResponseRedirect(link)
    else:
        form = InvestigateGoalForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "InvestigateGoalUpdate.html",context)

def ComputerSimulationUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ComputerSimulation"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ComputerSimulation, pk=pk)
    if request.method == 'POST':
        form = ComputerSimulationForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ComputerSimulation(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/ComputerSimulation/list/"
            return HttpResponseRedirect(link)
    else:
        form = ComputerSimulationForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ComputerSimulationUpdate.html",context)

def ConstantQuantityUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ConstantQuantity"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ConstantQuantity, pk=pk)
    if request.method == 'POST':
        form = ConstantQuantityForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ConstantQuantity(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/ConstantQuantity/list/"
            return HttpResponseRedirect(link)
    else:
        form = ConstantQuantityForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ConstantQuantityUpdate.html",context)

def ControllabilityUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Controllability"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Controllability, pk=pk)
    if request.method == 'POST':
        form = ControllabilityForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Controllability(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/Controllability/list/"
            return HttpResponseRedirect(link)
    else:
        form = ControllabilityForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ControllabilityUpdate.html",context)

def IndependenceUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Independence"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Independence, pk=pk)
    if request.method == 'POST':
        form = IndependenceForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Independence(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/Independence/list/"
            return HttpResponseRedirect(link)
    else:
        form = IndependenceForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "IndependenceUpdate.html",context)

def DBReferenceUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["DBReference"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(DBReference, pk=pk)
    if request.method == 'POST':
        form = DBReferenceForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.DBReference(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/DBReference/list/"
            return HttpResponseRedirect(link)
    else:
        form = DBReferenceForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "DBReferenceUpdate.html",context)

def DDC_Dewey_ClassificationUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["DDC_Dewey_Classification"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(DDC_Dewey_Classification, pk=pk)
    if request.method == 'POST':
        form = DDC_Dewey_ClassificationForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.DDC_Dewey_Classification(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/DDC_Dewey_Classification/list/"
            return HttpResponseRedirect(link)
    else:
        form = DDC_Dewey_ClassificationForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "DDC_Dewey_ClassificationUpdate.html",context)

def LibraryOfCongressClassificationUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["LibraryOfCongressClassification"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(LibraryOfCongressClassification, pk=pk)
    if request.method == 'POST':
        form = LibraryOfCongressClassificationForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.LibraryOfCongressClassification(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/LibraryOfCongressClassification/list/"
            return HttpResponseRedirect(link)
    else:
        form = LibraryOfCongressClassificationForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "LibraryOfCongressClassificationUpdate.html",context)

def NLMClassificationUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["NLMClassification"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(NLMClassification, pk=pk)
    if request.method == 'POST':
        form = NLMClassificationForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.NLMClassification(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/NLMClassification/list/"
            return HttpResponseRedirect(link)
    else:
        form = NLMClassificationForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "NLMClassificationUpdate.html",context)

def ResearchCouncilsUKClassificationUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ResearchCouncilsUKClassification"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ResearchCouncilsUKClassification, pk=pk)
    if request.method == 'POST':
        form = ResearchCouncilsUKClassificationForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ResearchCouncilsUKClassification(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/ResearchCouncilsUKClassification/list/"
            return HttpResponseRedirect(link)
    else:
        form = ResearchCouncilsUKClassificationForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ResearchCouncilsUKClassificationUpdate.html",context)

def DataRepresentationStandardUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["DataRepresentationStandard"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(DataRepresentationStandard, pk=pk)
    if request.method == 'POST':
        form = DataRepresentationStandardForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.DataRepresentationStandard(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/DataRepresentationStandard/list/"
            return HttpResponseRedirect(link)
    else:
        form = DataRepresentationStandardForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "DataRepresentationStandardUpdate.html",context)

def DegreeOfModelUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["DegreeOfModel"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(DegreeOfModel, pk=pk)
    if request.method == 'POST':
        form = DegreeOfModelForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.DegreeOfModel(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/DegreeOfModel/list/"
            return HttpResponseRedirect(link)
    else:
        form = DegreeOfModelForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "DegreeOfModelUpdate.html",context)

def DynamismOfModelUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["DynamismOfModel"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(DynamismOfModel, pk=pk)
    if request.method == 'POST':
        form = DynamismOfModelForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.DynamismOfModel(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/DynamismOfModel/list/"
            return HttpResponseRedirect(link)
    else:
        form = DynamismOfModelForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "DynamismOfModelUpdate.html",context)

def DependentVariableUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["DependentVariable"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(DependentVariable, pk=pk)
    if request.method == 'POST':
        form = DependentVariableForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.DependentVariable(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/DependentVariable/list/"
            return HttpResponseRedirect(link)
    else:
        form = DependentVariableForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "DependentVariableUpdate.html",context)

def DeviceUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Device"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Device, pk=pk)
    if request.method == 'POST':
        form = DeviceForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Device(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/Device/list/"
            return HttpResponseRedirect(link)
    else:
        form = DeviceForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "DeviceUpdate.html",context)

def DomainActionUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["DomainAction"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(DomainAction, pk=pk)
    if request.method == 'POST':
        form = DomainActionForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.DomainAction(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/DomainAction/list/"
            return HttpResponseRedirect(link)
    else:
        form = DomainActionForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "DomainActionUpdate.html",context)

def DoseResponseUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["DoseResponse"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(DoseResponse, pk=pk)
    if request.method == 'POST':
        form = DoseResponseForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.DoseResponse(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/DoseResponse/list/"
            return HttpResponseRedirect(link)
    else:
        form = DoseResponseForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "DoseResponseUpdate.html",context)

def GeneKnockinUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["GeneKnockin"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(GeneKnockin, pk=pk)
    if request.method == 'POST':
        form = GeneKnockinForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.GeneKnockin(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/GeneKnockin/list/"
            return HttpResponseRedirect(link)
    else:
        form = GeneKnockinForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "GeneKnockinUpdate.html",context)

def GeneKnockoutUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["GeneKnockout"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(GeneKnockout, pk=pk)
    if request.method == 'POST':
        form = GeneKnockoutForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.GeneKnockout(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/GeneKnockout/list/"
            return HttpResponseRedirect(link)
    else:
        form = GeneKnockoutForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "GeneKnockoutUpdate.html",context)

def Normal_DiseaseUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Normal_Disease"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Normal_Disease, pk=pk)
    if request.method == 'POST':
        form = Normal_DiseaseForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Normal_Disease(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/Normal_Disease/list/"
            return HttpResponseRedirect(link)
    else:
        form = Normal_DiseaseForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "Normal_DiseaseUpdate.html",context)

def TimeCourseUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["TimeCourse"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(TimeCourse, pk=pk)
    if request.method == 'POST':
        form = TimeCourseForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.TimeCourse(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/TimeCourse/list/"
            return HttpResponseRedirect(link)
    else:
        form = TimeCourseForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "TimeCourseUpdate.html",context)

def Treated_UntreatedUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Treated_Untreated"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Treated_Untreated, pk=pk)
    if request.method == 'POST':
        form = Treated_UntreatedForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Treated_Untreated(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/Treated_Untreated/list/"
            return HttpResponseRedirect(link)
    else:
        form = Treated_UntreatedForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "Treated_UntreatedUpdate.html",context)

def DraftStatusUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["DraftStatus"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(DraftStatus, pk=pk)
    if request.method == 'POST':
        form = DraftStatusForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.DraftStatus(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/DraftStatus/list/"
            return HttpResponseRedirect(link)
    else:
        form = DraftStatusForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "DraftStatusUpdate.html",context)

def DuhemEffectUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["DuhemEffect"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(DuhemEffect, pk=pk)
    if request.method == 'POST':
        form = DuhemEffectForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.DuhemEffect(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/DuhemEffect/list/"
            return HttpResponseRedirect(link)
    else:
        form = DuhemEffectForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "DuhemEffectUpdate.html",context)

def ExperimentalDesignEffectUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ExperimentalDesignEffect"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ExperimentalDesignEffect, pk=pk)
    if request.method == 'POST':
        form = ExperimentalDesignEffectForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ExperimentalDesignEffect(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/ExperimentalDesignEffect/list/"
            return HttpResponseRedirect(link)
    else:
        form = ExperimentalDesignEffectForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ExperimentalDesignEffectUpdate.html",context)

def InstrumentationEffectUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["InstrumentationEffect"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(InstrumentationEffect, pk=pk)
    if request.method == 'POST':
        form = InstrumentationEffectForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.InstrumentationEffect(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/InstrumentationEffect/list/"
            return HttpResponseRedirect(link)
    else:
        form = InstrumentationEffectForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "InstrumentationEffectUpdate.html",context)

def ObjectEffectUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ObjectEffect"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ObjectEffect, pk=pk)
    if request.method == 'POST':
        form = ObjectEffectForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ObjectEffect(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/ObjectEffect/list/"
            return HttpResponseRedirect(link)
    else:
        form = ObjectEffectForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ObjectEffectUpdate.html",context)

def SubjectEffectUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["SubjectEffect"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(SubjectEffect, pk=pk)
    if request.method == 'POST':
        form = SubjectEffectForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.SubjectEffect(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/SubjectEffect/list/"
            return HttpResponseRedirect(link)
    else:
        form = SubjectEffectForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SubjectEffectUpdate.html",context)

def TimeEffectUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["TimeEffect"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(TimeEffect, pk=pk)
    if request.method == 'POST':
        form = TimeEffectForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.TimeEffect(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/TimeEffect/list/"
            return HttpResponseRedirect(link)
    else:
        form = TimeEffectForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "TimeEffectUpdate.html",context)

def DynamicModelUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["DynamicModel"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(DynamicModel, pk=pk)
    if request.method == 'POST':
        form = DynamicModelForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.DynamicModel(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/DynamicModel/list/"
            return HttpResponseRedirect(link)
    else:
        form = DynamicModelForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "DynamicModelUpdate.html",context)

def StaticModelUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["StaticModel"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(StaticModel, pk=pk)
    if request.method == 'POST':
        form = StaticModelForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.StaticModel(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/StaticModel/list/"
            return HttpResponseRedirect(link)
    else:
        form = StaticModelForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "StaticModelUpdate.html",context)

def ErrorOfConclusionUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ErrorOfConclusion"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ErrorOfConclusion, pk=pk)
    if request.method == 'POST':
        form = ErrorOfConclusionForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ErrorOfConclusion(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/ErrorOfConclusion/list/"
            return HttpResponseRedirect(link)
    else:
        form = ErrorOfConclusionForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ErrorOfConclusionUpdate.html",context)

def ExperimentalActionsPlanningUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ExperimentalActionsPlanning"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ExperimentalActionsPlanning, pk=pk)
    if request.method == 'POST':
        form = ExperimentalActionsPlanningForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ExperimentalActionsPlanning(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/ExperimentalActionsPlanning/list/"
            return HttpResponseRedirect(link)
    else:
        form = ExperimentalActionsPlanningForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ExperimentalActionsPlanningUpdate.html",context)

def ExperimentalEquipmentSelectingUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ExperimentalEquipmentSelecting"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ExperimentalEquipmentSelecting, pk=pk)
    if request.method == 'POST':
        form = ExperimentalEquipmentSelectingForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ExperimentalEquipmentSelecting(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/ExperimentalEquipmentSelecting/list/"
            return HttpResponseRedirect(link)
    else:
        form = ExperimentalEquipmentSelectingForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ExperimentalEquipmentSelectingUpdate.html",context)

def ExperimentalModelDesignUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ExperimentalModelDesign"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ExperimentalModelDesign, pk=pk)
    if request.method == 'POST':
        form = ExperimentalModelDesignForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ExperimentalModelDesign(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/ExperimentalModelDesign/list/"
            return HttpResponseRedirect(link)
    else:
        form = ExperimentalModelDesignForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ExperimentalModelDesignUpdate.html",context)

def ExperimentalObjectSelectingUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ExperimentalObjectSelecting"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ExperimentalObjectSelecting, pk=pk)
    if request.method == 'POST':
        form = ExperimentalObjectSelectingForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ExperimentalObjectSelecting(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/ExperimentalObjectSelecting/list/"
            return HttpResponseRedirect(link)
    else:
        form = ExperimentalObjectSelectingForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ExperimentalObjectSelectingUpdate.html",context)

def ExperimentalConclusionUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ExperimentalConclusion"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ExperimentalConclusion, pk=pk)
    if request.method == 'POST':
        form = ExperimentalConclusionForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ExperimentalConclusion(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/ExperimentalConclusion/list/"
            return HttpResponseRedirect(link)
    else:
        form = ExperimentalConclusionForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ExperimentalConclusionUpdate.html",context)

def ExperimentalProtocolUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ExperimentalProtocol"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ExperimentalProtocol, pk=pk)
    if request.method == 'POST':
        form = ExperimentalProtocolForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ExperimentalProtocol(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/ExperimentalProtocol/list/"
            return HttpResponseRedirect(link)
    else:
        form = ExperimentalProtocolForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ExperimentalProtocolUpdate.html",context)

def ExperimenteeBiasUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ExperimenteeBias"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ExperimenteeBias, pk=pk)
    if request.method == 'POST':
        form = ExperimenteeBiasForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ExperimenteeBias(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/ExperimenteeBias/list/"
            return HttpResponseRedirect(link)
    else:
        form = ExperimenteeBiasForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ExperimenteeBiasUpdate.html",context)

def HawthorneEffectUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["HawthorneEffect"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(HawthorneEffect, pk=pk)
    if request.method == 'POST':
        form = HawthorneEffectForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.HawthorneEffect(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/HawthorneEffect/list/"
            return HttpResponseRedirect(link)
    else:
        form = HawthorneEffectForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "HawthorneEffectUpdate.html",context)

def MortalityEffectUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["MortalityEffect"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(MortalityEffect, pk=pk)
    if request.method == 'POST':
        form = MortalityEffectForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.MortalityEffect(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/MortalityEffect/list/"
            return HttpResponseRedirect(link)
    else:
        form = MortalityEffectForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "MortalityEffectUpdate.html",context)

def ExperimenterBiasUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ExperimenterBias"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ExperimenterBias, pk=pk)
    if request.method == 'POST':
        form = ExperimenterBiasForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ExperimenterBias(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/ExperimenterBias/list/"
            return HttpResponseRedirect(link)
    else:
        form = ExperimenterBiasForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ExperimenterBiasUpdate.html",context)

def TestingEffectUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["TestingEffect"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(TestingEffect, pk=pk)
    if request.method == 'POST':
        form = TestingEffectForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.TestingEffect(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/TestingEffect/list/"
            return HttpResponseRedirect(link)
    else:
        form = TestingEffectForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "TestingEffectUpdate.html",context)

def ExplicitHypothesisUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ExplicitHypothesis"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ExplicitHypothesis, pk=pk)
    if request.method == 'POST':
        form = ExplicitHypothesisForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ExplicitHypothesis(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/ExplicitHypothesis/list/"
            return HttpResponseRedirect(link)
    else:
        form = ExplicitHypothesisForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ExplicitHypothesisUpdate.html",context)

def ImplicitHypothesisUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ImplicitHypothesis"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ImplicitHypothesis, pk=pk)
    if request.method == 'POST':
        form = ImplicitHypothesisForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ImplicitHypothesis(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/ImplicitHypothesis/list/"
            return HttpResponseRedirect(link)
    else:
        form = ImplicitHypothesisForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ImplicitHypothesisUpdate.html",context)

def FactorLevelUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["FactorLevel"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(FactorLevel, pk=pk)
    if request.method == 'POST':
        form = FactorLevelForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.FactorLevel(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/FactorLevel/list/"
            return HttpResponseRedirect(link)
    else:
        form = FactorLevelForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "FactorLevelUpdate.html",context)

def FactorialDesignUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["FactorialDesign"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(FactorialDesign, pk=pk)
    if request.method == 'POST':
        form = FactorialDesignForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.FactorialDesign(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/FactorialDesign/list/"
            return HttpResponseRedirect(link)
    else:
        form = FactorialDesignForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "FactorialDesignUpdate.html",context)

def FalseNegativeUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["FalseNegative"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(FalseNegative, pk=pk)
    if request.method == 'POST':
        form = FalseNegativeForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.FalseNegative(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/FalseNegative/list/"
            return HttpResponseRedirect(link)
    else:
        form = FalseNegativeForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "FalseNegativeUpdate.html",context)

def HypothesisAcceptanceMistakeUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["HypothesisAcceptanceMistake"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(HypothesisAcceptanceMistake, pk=pk)
    if request.method == 'POST':
        form = HypothesisAcceptanceMistakeForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.HypothesisAcceptanceMistake(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/HypothesisAcceptanceMistake/list/"
            return HttpResponseRedirect(link)
    else:
        form = HypothesisAcceptanceMistakeForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "HypothesisAcceptanceMistakeUpdate.html",context)

def FalsePpositiveUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["FalsePpositive"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(FalsePpositive, pk=pk)
    if request.method == 'POST':
        form = FalsePpositiveForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.FalsePpositive(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/FalsePpositive/list/"
            return HttpResponseRedirect(link)
    else:
        form = FalsePpositiveForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "FalsePpositiveUpdate.html",context)

def FaultyComparisonUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["FaultyComparison"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(FaultyComparison, pk=pk)
    if request.method == 'POST':
        form = FaultyComparisonForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.FaultyComparison(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/FaultyComparison/list/"
            return HttpResponseRedirect(link)
    else:
        form = FaultyComparisonForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "FaultyComparisonUpdate.html",context)

def FormulaUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Formula"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Formula, pk=pk)
    if request.method == 'POST':
        form = FormulaForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Formula(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/Formula/list/"
            return HttpResponseRedirect(link)
    else:
        form = FormulaForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "FormulaUpdate.html",context)

def GalileanExperimentUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["GalileanExperiment"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(GalileanExperiment, pk=pk)
    if request.method == 'POST':
        form = GalileanExperimentForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.GalileanExperiment(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/GalileanExperiment/list/"
            return HttpResponseRedirect(link)
    else:
        form = GalileanExperimentForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "GalileanExperimentUpdate.html",context)

def HandToolUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["HandTool"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(HandTool, pk=pk)
    if request.method == 'POST':
        form = HandToolForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.HandTool(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/HandTool/list/"
            return HttpResponseRedirect(link)
    else:
        form = HandToolForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "HandToolUpdate.html",context)

def ToolUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Tool"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Tool, pk=pk)
    if request.method == 'POST':
        form = ToolForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Tool(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/Tool/list/"
            return HttpResponseRedirect(link)
    else:
        form = ToolForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ToolUpdate.html",context)

def HardwareUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Hardware"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Hardware, pk=pk)
    if request.method == 'POST':
        form = HardwareForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Hardware(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/Hardware/list/"
            return HttpResponseRedirect(link)
    else:
        form = HardwareForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "HardwareUpdate.html",context)

def HistoryEffectUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["HistoryEffect"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(HistoryEffect, pk=pk)
    if request.method == 'POST':
        form = HistoryEffectForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.HistoryEffect(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/HistoryEffect/list/"
            return HttpResponseRedirect(link)
    else:
        form = HistoryEffectForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "HistoryEffectUpdate.html",context)

def MaturationEffectUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["MaturationEffect"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(MaturationEffect, pk=pk)
    if request.method == 'POST':
        form = MaturationEffectForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.MaturationEffect(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/MaturationEffect/list/"
            return HttpResponseRedirect(link)
    else:
        form = MaturationEffectForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "MaturationEffectUpdate.html",context)

def HypothesisdrivenExperimentUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["HypothesisdrivenExperiment"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(HypothesisdrivenExperiment, pk=pk)
    if request.method == 'POST':
        form = HypothesisdrivenExperimentForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.HypothesisdrivenExperiment(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/HypothesisdrivenExperiment/list/"
            return HttpResponseRedirect(link)
    else:
        form = HypothesisdrivenExperimentForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "HypothesisdrivenExperimentUpdate.html",context)

def HypothesisformingExperimentUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["HypothesisformingExperiment"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(HypothesisformingExperiment, pk=pk)
    if request.method == 'POST':
        form = HypothesisformingExperimentForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.HypothesisformingExperiment(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/HypothesisformingExperiment/list/"
            return HttpResponseRedirect(link)
    else:
        form = HypothesisformingExperimentForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "HypothesisformingExperimentUpdate.html",context)

def ImageUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Image"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Image, pk=pk)
    if request.method == 'POST':
        form = ImageForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Image(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/Image/list/"
            return HttpResponseRedirect(link)
    else:
        form = ImageForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ImageUpdate.html",context)

def ImproperSamplingUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ImproperSampling"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ImproperSampling, pk=pk)
    if request.method == 'POST':
        form = ImproperSamplingForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ImproperSampling(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/ImproperSampling/list/"
            return HttpResponseRedirect(link)
    else:
        form = ImproperSamplingForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ImproperSamplingUpdate.html",context)

def IncompleteDataErrorUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["IncompleteDataError"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(IncompleteDataError, pk=pk)
    if request.method == 'POST':
        form = IncompleteDataErrorForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.IncompleteDataError(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/IncompleteDataError/list/"
            return HttpResponseRedirect(link)
    else:
        form = IncompleteDataErrorForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "IncompleteDataErrorUpdate.html",context)

def IndependentVariableUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["IndependentVariable"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(IndependentVariable, pk=pk)
    if request.method == 'POST':
        form = IndependentVariableForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.IndependentVariable(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/IndependentVariable/list/"
            return HttpResponseRedirect(link)
    else:
        form = IndependentVariableForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "IndependentVariableUpdate.html",context)

def InferableVariableUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["InferableVariable"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(InferableVariable, pk=pk)
    if request.method == 'POST':
        form = InferableVariableForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.InferableVariable(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/InferableVariable/list/"
            return HttpResponseRedirect(link)
    else:
        form = InferableVariableForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "InferableVariableUpdate.html",context)

def InternalStatusUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["InternalStatus"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(InternalStatus, pk=pk)
    if request.method == 'POST':
        form = InternalStatusForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.InternalStatus(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/InternalStatus/list/"
            return HttpResponseRedirect(link)
    else:
        form = InternalStatusForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "InternalStatusUpdate.html",context)

def RestrictedStatusUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["RestrictedStatus"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(RestrictedStatus, pk=pk)
    if request.method == 'POST':
        form = RestrictedStatusForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.RestrictedStatus(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/RestrictedStatus/list/"
            return HttpResponseRedirect(link)
    else:
        form = RestrictedStatusForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "RestrictedStatusUpdate.html",context)

def JournalUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Journal"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Journal, pk=pk)
    if request.method == 'POST':
        form = JournalForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Journal(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/Journal/list/"
            return HttpResponseRedirect(link)
    else:
        form = JournalForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "JournalUpdate.html",context)

def LinearModelUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["LinearModel"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(LinearModel, pk=pk)
    if request.method == 'POST':
        form = LinearModelForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.LinearModel(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/LinearModel/list/"
            return HttpResponseRedirect(link)
    else:
        form = LinearModelForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "LinearModelUpdate.html",context)

def NonlinearModelUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["NonlinearModel"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(NonlinearModel, pk=pk)
    if request.method == 'POST':
        form = NonlinearModelForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.NonlinearModel(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/NonlinearModel/list/"
            return HttpResponseRedirect(link)
    else:
        form = NonlinearModelForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "NonlinearModelUpdate.html",context)

def LogicalModelRepresentationUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["LogicalModelRepresentation"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(LogicalModelRepresentation, pk=pk)
    if request.method == 'POST':
        form = LogicalModelRepresentationForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.LogicalModelRepresentation(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/LogicalModelRepresentation/list/"
            return HttpResponseRedirect(link)
    else:
        form = LogicalModelRepresentationForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "LogicalModelRepresentationUpdate.html",context)

def MachineUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Machine"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Machine, pk=pk)
    if request.method == 'POST':
        form = MachineForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Machine(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/Machine/list/"
            return HttpResponseRedirect(link)
    else:
        form = MachineForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "MachineUpdate.html",context)

def MachineToolUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["MachineTool"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(MachineTool, pk=pk)
    if request.method == 'POST':
        form = MachineToolForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.MachineTool(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/MachineTool/list/"
            return HttpResponseRedirect(link)
    else:
        form = MachineToolForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "MachineToolUpdate.html",context)

def MagazineUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Magazine"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Magazine, pk=pk)
    if request.method == 'POST':
        form = MagazineForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Magazine(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/Magazine/list/"
            return HttpResponseRedirect(link)
    else:
        form = MagazineForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "MagazineUpdate.html",context)

def MaterialsUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Materials"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Materials, pk=pk)
    if request.method == 'POST':
        form = MaterialsForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Materials(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/Materials/list/"
            return HttpResponseRedirect(link)
    else:
        form = MaterialsForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "MaterialsUpdate.html",context)

def MathematicalModelRepresentationUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["MathematicalModelRepresentation"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(MathematicalModelRepresentation, pk=pk)
    if request.method == 'POST':
        form = MathematicalModelRepresentationForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.MathematicalModelRepresentation(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/MathematicalModelRepresentation/list/"
            return HttpResponseRedirect(link)
    else:
        form = MathematicalModelRepresentationForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "MathematicalModelRepresentationUpdate.html",context)

def MetabolomicExperimentUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["MetabolomicExperiment"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(MetabolomicExperiment, pk=pk)
    if request.method == 'POST':
        form = MetabolomicExperimentForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.MetabolomicExperiment(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/MetabolomicExperiment/list/"
            return HttpResponseRedirect(link)
    else:
        form = MetabolomicExperimentForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "MetabolomicExperimentUpdate.html",context)

def MethodsComparisonUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["MethodsComparison"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(MethodsComparison, pk=pk)
    if request.method == 'POST':
        form = MethodsComparisonForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.MethodsComparison(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/MethodsComparison/list/"
            return HttpResponseRedirect(link)
    else:
        form = MethodsComparisonForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "MethodsComparisonUpdate.html",context)

def SubjectsComparisonUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["SubjectsComparison"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(SubjectsComparison, pk=pk)
    if request.method == 'POST':
        form = SubjectsComparisonForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.SubjectsComparison(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/SubjectsComparison/list/"
            return HttpResponseRedirect(link)
    else:
        form = SubjectsComparisonForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SubjectsComparisonUpdate.html",context)

def MicroarrayExperimentUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["MicroarrayExperiment"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(MicroarrayExperiment, pk=pk)
    if request.method == 'POST':
        form = MicroarrayExperimentForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.MicroarrayExperiment(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/MicroarrayExperiment/list/"
            return HttpResponseRedirect(link)
    else:
        form = MicroarrayExperimentForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "MicroarrayExperimentUpdate.html",context)

def MultifactorExperimentUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["MultifactorExperiment"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(MultifactorExperiment, pk=pk)
    if request.method == 'POST':
        form = MultifactorExperimentForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.MultifactorExperiment(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/MultifactorExperiment/list/"
            return HttpResponseRedirect(link)
    else:
        form = MultifactorExperimentForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "MultifactorExperimentUpdate.html",context)

def OnefactorExperimentUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["OnefactorExperiment"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(OnefactorExperiment, pk=pk)
    if request.method == 'POST':
        form = OnefactorExperimentForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.OnefactorExperiment(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/OnefactorExperiment/list/"
            return HttpResponseRedirect(link)
    else:
        form = OnefactorExperimentForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "OnefactorExperimentUpdate.html",context)

def TwofactorExperimentUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["TwofactorExperiment"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(TwofactorExperiment, pk=pk)
    if request.method == 'POST':
        form = TwofactorExperimentForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.TwofactorExperiment(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/TwofactorExperiment/list/"
            return HttpResponseRedirect(link)
    else:
        form = TwofactorExperimentForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "TwofactorExperimentUpdate.html",context)

def NaturalLanguageUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["NaturalLanguage"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(NaturalLanguage, pk=pk)
    if request.method == 'POST':
        form = NaturalLanguageForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.NaturalLanguage(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/NaturalLanguage/list/"
            return HttpResponseRedirect(link)
    else:
        form = NaturalLanguageForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "NaturalLanguageUpdate.html",context)

def NonRestrictedStatusUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["NonRestrictedStatus"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(NonRestrictedStatus, pk=pk)
    if request.method == 'POST':
        form = NonRestrictedStatusForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.NonRestrictedStatus(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/NonRestrictedStatus/list/"
            return HttpResponseRedirect(link)
    else:
        form = NonRestrictedStatusForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "NonRestrictedStatusUpdate.html",context)

def NormalizationStrategyUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["NormalizationStrategy"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(NormalizationStrategy, pk=pk)
    if request.method == 'POST':
        form = NormalizationStrategyForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.NormalizationStrategy(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/NormalizationStrategy/list/"
            return HttpResponseRedirect(link)
    else:
        form = NormalizationStrategyForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "NormalizationStrategyUpdate.html",context)

def ObjectMethodUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ObjectMethod"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ObjectMethod, pk=pk)
    if request.method == 'POST':
        form = ObjectMethodForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ObjectMethod(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/ObjectMethod/list/"
            return HttpResponseRedirect(link)
    else:
        form = ObjectMethodForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ObjectMethodUpdate.html",context)

def ObjectOfActionUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ObjectOfAction"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ObjectOfAction, pk=pk)
    if request.method == 'POST':
        form = ObjectOfActionForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ObjectOfAction(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/ObjectOfAction/list/"
            return HttpResponseRedirect(link)
    else:
        form = ObjectOfActionForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ObjectOfActionUpdate.html",context)

def ObservableVariableUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ObservableVariable"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ObservableVariable, pk=pk)
    if request.method == 'POST':
        form = ObservableVariableForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ObservableVariable(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/ObservableVariable/list/"
            return HttpResponseRedirect(link)
    else:
        form = ObservableVariableForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ObservableVariableUpdate.html",context)

def PaperUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Paper"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Paper, pk=pk)
    if request.method == 'POST':
        form = PaperForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Paper(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/Paper/list/"
            return HttpResponseRedirect(link)
    else:
        form = PaperForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "PaperUpdate.html",context)

def ParticlePhysicsExperimentUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ParticlePhysicsExperiment"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ParticlePhysicsExperiment, pk=pk)
    if request.method == 'POST':
        form = ParticlePhysicsExperimentForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ParticlePhysicsExperiment(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/ParticlePhysicsExperiment/list/"
            return HttpResponseRedirect(link)
    else:
        form = ParticlePhysicsExperimentForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ParticlePhysicsExperimentUpdate.html",context)

def PresentingSamplingUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["PresentingSampling"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(PresentingSampling, pk=pk)
    if request.method == 'POST':
        form = PresentingSamplingForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.PresentingSampling(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/PresentingSampling/list/"
            return HttpResponseRedirect(link)
    else:
        form = PresentingSamplingForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "PresentingSamplingUpdate.html",context)

def ProceedingsUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Proceedings"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Proceedings, pk=pk)
    if request.method == 'POST':
        form = ProceedingsForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Proceedings(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/Proceedings/list/"
            return HttpResponseRedirect(link)
    else:
        form = ProceedingsForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ProceedingsUpdate.html",context)

def ProgramUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Program"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Program, pk=pk)
    if request.method == 'POST':
        form = ProgramForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Program(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/Program/list/"
            return HttpResponseRedirect(link)
    else:
        form = ProgramForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ProgramUpdate.html",context)

def ProteomicExperimentUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["ProteomicExperiment"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(ProteomicExperiment, pk=pk)
    if request.method == 'POST':
        form = ProteomicExperimentForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.ProteomicExperiment(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/ProteomicExperiment/list/"
            return HttpResponseRedirect(link)
    else:
        form = ProteomicExperimentForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ProteomicExperimentUpdate.html",context)

def ProviderUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Provider"]
    foreign_keys = dict()
    
    foreign_keys['Has_admin_info'] = 'EXPO_app:AdminInfoProviderList'
    
    foreign_keys['Has_admin_info'] = 'EXPO_app:ThingList'
    
    updated_instance =  get_object_or_404(Provider, pk=pk)
    if request.method == 'POST':
        form = ProviderForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Provider(form.cleaned_data['name'])
            
            onto.save()

            
            fkstr = 'Has_admin_info'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.AdminInfoProvider(fkinstanceinput[0])
            onto.save()
            onto_instance.has_admin_info.append(fkinstance) 
            
            fkstr = 'Has_admin_info'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Thing(fkinstanceinput[0])
            onto.save()
            onto_instance.has_admin_info.append(fkinstance) 
            
            onto.save()


            link = "/EXPO_app/Provider/list/"
            return HttpResponseRedirect(link)
    else:
        form = ProviderForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "ProviderUpdate.html",context)

def PublicAcademicStatusUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["PublicAcademicStatus"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(PublicAcademicStatus, pk=pk)
    if request.method == 'POST':
        form = PublicAcademicStatusForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.PublicAcademicStatus(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/PublicAcademicStatus/list/"
            return HttpResponseRedirect(link)
    else:
        form = PublicAcademicStatusForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "PublicAcademicStatusUpdate.html",context)

def PublicStatusUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["PublicStatus"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(PublicStatus, pk=pk)
    if request.method == 'POST':
        form = PublicStatusForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.PublicStatus(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/PublicStatus/list/"
            return HttpResponseRedirect(link)
    else:
        form = PublicStatusForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "PublicStatusUpdate.html",context)

def RandomErrorUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["RandomError"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(RandomError, pk=pk)
    if request.method == 'POST':
        form = RandomErrorForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.RandomError(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/RandomError/list/"
            return HttpResponseRedirect(link)
    else:
        form = RandomErrorForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "RandomErrorUpdate.html",context)

def RandomSamplingUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["RandomSampling"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(RandomSampling, pk=pk)
    if request.method == 'POST':
        form = RandomSamplingForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.RandomSampling(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/RandomSampling/list/"
            return HttpResponseRedirect(link)
    else:
        form = RandomSamplingForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "RandomSamplingUpdate.html",context)

def RecommendationSatusUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["RecommendationSatus"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(RecommendationSatus, pk=pk)
    if request.method == 'POST':
        form = RecommendationSatusForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.RecommendationSatus(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/RecommendationSatus/list/"
            return HttpResponseRedirect(link)
    else:
        form = RecommendationSatusForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "RecommendationSatusUpdate.html",context)

def RecordingActionUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["RecordingAction"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(RecordingAction, pk=pk)
    if request.method == 'POST':
        form = RecordingActionForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.RecordingAction(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/RecordingAction/list/"
            return HttpResponseRedirect(link)
    else:
        form = RecordingActionForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "RecordingActionUpdate.html",context)

def RegionUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Region"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Region, pk=pk)
    if request.method == 'POST':
        form = RegionForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Region(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/Region/list/"
            return HttpResponseRedirect(link)
    else:
        form = RegionForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "RegionUpdate.html",context)

def RepresenationalMediumUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["RepresenationalMedium"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(RepresenationalMedium, pk=pk)
    if request.method == 'POST':
        form = RepresenationalMediumForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.RepresenationalMedium(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/RepresenationalMedium/list/"
            return HttpResponseRedirect(link)
    else:
        form = RepresenationalMediumForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "RepresenationalMediumUpdate.html",context)

def SUMOSynonymUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["SUMOSynonym"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(SUMOSynonym, pk=pk)
    if request.method == 'POST':
        form = SUMOSynonymForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.SUMOSynonym(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/SUMOSynonym/list/"
            return HttpResponseRedirect(link)
    else:
        form = SUMOSynonymForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SUMOSynonymUpdate.html",context)

def SampleFormingUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["SampleForming"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(SampleForming, pk=pk)
    if request.method == 'POST':
        form = SampleFormingForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.SampleForming(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/SampleForming/list/"
            return HttpResponseRedirect(link)
    else:
        form = SampleFormingForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SampleFormingUpdate.html",context)

def SequentialDesignUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["SequentialDesign"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(SequentialDesign, pk=pk)
    if request.method == 'POST':
        form = SequentialDesignForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.SequentialDesign(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/SequentialDesign/list/"
            return HttpResponseRedirect(link)
    else:
        form = SequentialDesignForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SequentialDesignUpdate.html",context)

def SoftwareUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Software"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Software, pk=pk)
    if request.method == 'POST':
        form = SoftwareForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Software(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/Software/list/"
            return HttpResponseRedirect(link)
    else:
        form = SoftwareForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SoftwareUpdate.html",context)

def SoundUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Sound"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Sound, pk=pk)
    if request.method == 'POST':
        form = SoundForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Sound(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/Sound/list/"
            return HttpResponseRedirect(link)
    else:
        form = SoundForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SoundUpdate.html",context)

def SplitSamplesUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["SplitSamples"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(SplitSamples, pk=pk)
    if request.method == 'POST':
        form = SplitSamplesForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.SplitSamples(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/SplitSamples/list/"
            return HttpResponseRedirect(link)
    else:
        form = SplitSamplesForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SplitSamplesUpdate.html",context)

def SymmetricalMatchingUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["SymmetricalMatching"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(SymmetricalMatching, pk=pk)
    if request.method == 'POST':
        form = SymmetricalMatchingForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.SymmetricalMatching(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/SymmetricalMatching/list/"
            return HttpResponseRedirect(link)
    else:
        form = SymmetricalMatchingForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SymmetricalMatchingUpdate.html",context)

def StatisticalErrorUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["StatisticalError"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(StatisticalError, pk=pk)
    if request.method == 'POST':
        form = StatisticalErrorForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.StatisticalError(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/StatisticalError/list/"
            return HttpResponseRedirect(link)
    else:
        form = StatisticalErrorForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "StatisticalErrorUpdate.html",context)

def StratifiedSamplingUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["StratifiedSampling"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(StratifiedSampling, pk=pk)
    if request.method == 'POST':
        form = StratifiedSamplingForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.StratifiedSampling(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/StratifiedSampling/list/"
            return HttpResponseRedirect(link)
    else:
        form = StratifiedSamplingForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "StratifiedSamplingUpdate.html",context)

def SubjectOfActionUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["SubjectOfAction"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(SubjectOfAction, pk=pk)
    if request.method == 'POST':
        form = SubjectOfActionForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.SubjectOfAction(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/SubjectOfAction/list/"
            return HttpResponseRedirect(link)
    else:
        form = SubjectOfActionForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SubjectOfActionUpdate.html",context)

def SubjectOfExperimentUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["SubjectOfExperiment"]
    foreign_keys = dict()
    
    foreign_keys['Has_admin_info'] = 'EXPO_app:AdminInfoSubjectOfExperimentList'
    
    foreign_keys['Has_admin_info'] = 'EXPO_app:ThingList'
    
    updated_instance =  get_object_or_404(SubjectOfExperiment, pk=pk)
    if request.method == 'POST':
        form = SubjectOfExperimentForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.SubjectOfExperiment(form.cleaned_data['name'])
            
            onto.save()

            
            fkstr = 'Has_admin_info'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.AdminInfoSubjectOfExperiment(fkinstanceinput[0])
            onto.save()
            onto_instance.has_admin_info.append(fkinstance) 
            
            fkstr = 'Has_admin_info'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Thing(fkinstanceinput[0])
            onto.save()
            onto_instance.has_admin_info.append(fkinstance) 
            
            onto.save()


            link = "/EXPO_app/SubjectOfExperiment/list/"
            return HttpResponseRedirect(link)
    else:
        form = SubjectOfExperimentForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SubjectOfExperimentUpdate.html",context)

def SubmitterUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Submitter"]
    foreign_keys = dict()
    
    foreign_keys['Has_admin_info'] = 'EXPO_app:AdminInfoSubmitterList'
    
    foreign_keys['Has_admin_info'] = 'EXPO_app:ThingList'
    
    updated_instance =  get_object_or_404(Submitter, pk=pk)
    if request.method == 'POST':
        form = SubmitterForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Submitter(form.cleaned_data['name'])
            
            onto.save()

            
            fkstr = 'Has_admin_info'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.AdminInfoSubmitter(fkinstanceinput[0])
            onto.save()
            onto_instance.has_admin_info.append(fkinstance) 
            
            fkstr = 'Has_admin_info'
            fklc = fkstr[0].lower() + fkstr[1:]
            fkinstanceinput = form.cleaned_data[fklc]
            fkinstance = class_namespace.Thing(fkinstanceinput[0])
            onto.save()
            onto_instance.has_admin_info.append(fkinstance) 
            
            onto.save()


            link = "/EXPO_app/Submitter/list/"
            return HttpResponseRedirect(link)
    else:
        form = SubmitterForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SubmitterUpdate.html",context)

def SummaryUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["Summary"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(Summary, pk=pk)
    if request.method == 'POST':
        form = SummaryForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.Summary(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/Summary/list/"
            return HttpResponseRedirect(link)
    else:
        form = SummaryForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SummaryUpdate.html",context)

def SystematicErrorUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["SystematicError"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(SystematicError, pk=pk)
    if request.method == 'POST':
        form = SystematicErrorForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.SystematicError(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/SystematicError/list/"
            return HttpResponseRedirect(link)
    else:
        form = SystematicErrorForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "SystematicErrorUpdate.html",context)

def TechnicalReportUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["TechnicalReport"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(TechnicalReport, pk=pk)
    if request.method == 'POST':
        form = TechnicalReportForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.TechnicalReport(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/TechnicalReport/list/"
            return HttpResponseRedirect(link)
    else:
        form = TechnicalReportForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "TechnicalReportUpdate.html",context)

def TimeDurationUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["TimeDuration"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(TimeDuration, pk=pk)
    if request.method == 'POST':
        form = TimeDurationForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.TimeDuration(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/TimeDuration/list/"
            return HttpResponseRedirect(link)
    else:
        form = TimeDurationForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "TimeDurationUpdate.html",context)

def TimeIntervalUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["TimeInterval"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(TimeInterval, pk=pk)
    if request.method == 'POST':
        form = TimeIntervalForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.TimeInterval(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/TimeInterval/list/"
            return HttpResponseRedirect(link)
    else:
        form = TimeIntervalForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "TimeIntervalUpdate.html",context)

def URLUpdate(request, pk):
    data = dict() 
    class_namespace =  namespaces["URL"]
    foreign_keys = dict()
    
    updated_instance =  get_object_or_404(URL, pk=pk)
    if request.method == 'POST':
        form = URLForm(request.POST,instance = updated_instance)
        if form.is_valid():
            form.save()

            onto_instance = class_namespace.URL(form.cleaned_data['name'])
            
            onto.save()

            
            onto.save()


            link = "/EXPO_app/URL/list/"
            return HttpResponseRedirect(link)
    else:
        form = URLForm(instance = updated_instance)
    field_types = dict()
    for field in form:
        field_type_object = str(field.field.widget).split(' ')
        field_type = field_type_object[0].split('.')[3]
        field_types[field.field.label] = field_type
    context = {'form': form, 'field_types' : field_types, 'foreign_keys': foreign_keys}
    return render(request, "URLUpdate.html",context)
